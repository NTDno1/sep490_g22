USE [master]
GO
/****** Object:  Database [sep490_g22]    Script Date: 2023-12-11 02:02:36 ******/
CREATE DATABASE [sep490_g22]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sep490_g22', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\sep490_g22.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'sep490_g22_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\sep490_g22_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [sep490_g22] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sep490_g22].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sep490_g22] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [sep490_g22] SET ARITHABORT OFF 
GO
ALTER DATABASE [sep490_g22] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [sep490_g22] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [sep490_g22] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [sep490_g22] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [sep490_g22] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [sep490_g22] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [sep490_g22] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [sep490_g22] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [sep490_g22] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [sep490_g22] SET  ENABLE_BROKER 
GO
ALTER DATABASE [sep490_g22] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [sep490_g22] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [sep490_g22] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [sep490_g22] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [sep490_g22] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [sep490_g22] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [sep490_g22] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [sep490_g22] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [sep490_g22] SET  MULTI_USER 
GO
ALTER DATABASE [sep490_g22] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [sep490_g22] SET DB_CHAINING OFF 
GO
ALTER DATABASE [sep490_g22] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [sep490_g22] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [sep490_g22] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [sep490_g22] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [sep490_g22] SET QUERY_STORE = ON
GO
ALTER DATABASE [sep490_g22] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [sep490_g22]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[id] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[RoleId] [int] NOT NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[refreshToken] [text] NULL,
	[Verfify] [nvarchar](max) NULL,
	[refreshTokenExpired] [datetime] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Atendence]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Atendence](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[SchedualId] [int] NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_Atendence] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cate_id] [int] NOT NULL,
	[center_id] [uniqueidentifier] NULL,
	[blog_code] [uniqueidentifier] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[short_description] [nvarchar](500) NULL,
	[status] [int] NULL,
	[created_date] [date] NULL,
	[update_date] [date] NULL,
	[created_by] [nvarchar](50) NULL,
	[update_by] [nvarchar](50) NULL,
	[images] [nvarchar](200) NULL,
	[position] [int] NULL,
	[url] [nvarchar](500) NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CateBlog]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CateBlog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](500) NULL,
	[level] [int] NULL,
	[parent_id] [int] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_CateBlog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Center]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Center](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[Adress] [nvarchar](500) NULL,
	[CodeCenter] [nvarchar](500) NULL,
	[AdminId] [uniqueidentifier] NULL,
	[NumberPhone] [nvarchar](500) NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Center_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Code] [nvarchar](500) NULL,
	[CourceId] [int] NULL,
	[AdminCenterId] [uniqueidentifier] NULL,
	[TeacherId] [uniqueidentifier] NULL,
	[CenterId] [uniqueidentifier] NULL,
	[NotificationId] [int] NULL,
	[Status] [int] NULL,
	[SlotId] [int] NULL,
	[SlotNumber] [int] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Class_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClassRoom]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Code] [nvarchar](500) NULL,
	[TypeId] [int] NULL,
	[CenterId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ClassRoom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClassSlotDate]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassSlotDate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NOT NULL,
	[SlotId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[TeacherId] [uniqueidentifier] NOT NULL,
	[Uid] [int] NULL,
 CONSTRAINT [PK_ClassSlotDate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Code] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[Price] [money] NULL,
	[ClassId] [int] NULL,
	[ImageSrc] [nvarchar](max) NULL,
	[BillId] [int] NULL,
	[CenterId] [uniqueidentifier] NULL,
	[EmployeeId] [uniqueidentifier] NULL,
	[TypeId] [int] NULL,
	[SyllabusId] [int] NULL,
	[Status] [int] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[Rating] [int] NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Adress] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](500) NULL,
	[Dob] [date] NULL,
	[CenterId] [uniqueidentifier] NULL,
	[AccountId] [uniqueidentifier] NULL,
	[RoleId] [uniqueidentifier] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[DateCreate] [date] NULL,
	[refreshTokenExpired] [datetime] NULL,
	[refreshToken] [text] NULL,
	[Verfify] [nvarchar](max) NULL,
	[DateUpdate] [date] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exam](
	[id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Desc] [nvarchar](50) NULL,
	[average] [float] NOT NULL,
	[CourceId] [int] NULL,
 CONSTRAINT [PK_Exam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamClass]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamClass](
	[ExamId] [int] NOT NULL,
	[Mark] [float] NULL,
	[Rank] [int] NULL,
	[student_class_id] [int] NOT NULL,
	[Evaluate] [text] NULL,
 CONSTRAINT [PK_ExamClass] PRIMARY KEY CLUSTERED 
(
	[ExamId] ASC,
	[student_class_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[id] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[EmployeeId] [uniqueidentifier] NULL,
	[ViewCount] [float] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationClases]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationClases](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[ClassId] [int] NULL,
	[StudentId] [uniqueidentifier] NULL,
	[ViewCount] [float] NULL,
	[Datecreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_NotificationClass] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Role_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SAQ]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SAQ](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Desc] [nvarchar](500) NULL,
	[image] [nvarchar](500) NULL,
	[EmployeeId] [uniqueidentifier] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_SAQ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedual]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedual](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NOT NULL,
	[SlotNum] [int] NOT NULL,
	[StatusOfSchedule] [nvarchar](50) NULL,
	[dateOffSlot] [date] NOT NULL,
	[Staus] [int] NOT NULL,
	[SlotDateId] [int] NOT NULL,
	[RoomIdUpDate] [int] NULL,
 CONSTRAINT [PK_SlotClass_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SlotDate]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlotDate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeStart] [time](7) NOT NULL,
	[TimeEnd] [time](7) NOT NULL,
	[Day] [nvarchar](50) NOT NULL,
	[CenterId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Slot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentClass]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentClass](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NULL,
	[StudentId] [uniqueidentifier] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_StudentClass_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentProfile]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentProfile](
	[id] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](500) NULL,
	[MidName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Image] [nvarchar](500) NULL,
	[Dob] [date] NULL,
	[Status] [int] NULL,
	[Gender] [int] NULL,
	[Cccd] [nvarchar](50) NULL,
	[CenterId] [uniqueidentifier] NULL,
	[EmoloyeeId] [uniqueidentifier] NULL,
	[DateCreate] [date] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[refreshToken] [text] NULL,
	[refreshTokenExpired] [datetime] NULL,
	[Verfify] [nvarchar](max) NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_StudentProfile] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Syllabus]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Syllabus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SyllabusName] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[TimeAllocation] [nvarchar](max) NULL,
	[StudentTasks] [nvarchar](max) NULL,
	[CourseObjectives] [nvarchar](max) NULL,
	[Exam] [nvarchar](max) NULL,
	[SubjectCode] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[CourseLearningOutcome] [nvarchar](max) NULL,
	[MainTopics] [nvarchar](max) NULL,
	[RequiredKnowledge] [nvarchar](max) NULL,
	[LinkBook] [nvarchar](500) NULL,
	[LinkDocument] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CenterId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Syllabus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherProfile]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherProfile](
	[Id] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](500) NULL,
	[MidName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](500) NULL,
	[Dob] [date] NULL,
	[Image] [nvarchar](500) NULL,
	[CenterId] [uniqueidentifier] NULL,
	[CertificateName] [nvarchar](500) NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[Release date] [date] NULL,
	[ImageCertificateName] [nvarchar](500) NULL,
	[CreateDate] [date] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[UpdateDate] [date] NULL,
	[refreshToken] [text] NULL,
	[Verfify] [nvarchar](max) NULL,
	[refreshTokenExpired] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_TeacherProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherTeachingHours]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherTeachingHours](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeacherId] [uniqueidentifier] NULL,
	[ScheduleId] [int] NULL,
	[TimeTeaching] [int] NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_TeacherTeachingHours] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeCourse]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeCourse](
	[Id] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[CourseId] [int] NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeRoom]    Script Date: 2023-12-11 02:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[status] [int] NULL,
	[created_date] [date] NULL,
	[update_date] [date] NULL,
	[created_by] [nvarchar](50) NULL,
	[update_by] [nvarchar](50) NULL,
 CONSTRAINT [PK_TypeRoom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([id], [UserName], [PassWord], [RoleId], [DateCreate], [DateUpdate], [refreshToken], [Verfify], [refreshTokenExpired]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'Admin', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', 1, CAST(N'2023-10-10' AS Date), CAST(N'2023-10-10' AS Date), N'cWdpV3uhkGHiOzZBlH2nGL9I4mnMfhBYzzDGtqb3Gtw=', NULL, CAST(N'2023-12-14T23:22:07.723' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Atendence] ON 

INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (779, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', 1, 368, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (780, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', 2, 368, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (781, N'2a0761e6-d4c8-46af-b451-21556534da08', 2, 368, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (782, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', 0, 369, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (783, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', 0, 369, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (784, N'2a0761e6-d4c8-46af-b451-21556534da08', 0, 369, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (785, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', 0, 370, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (786, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', 0, 370, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (787, N'2a0761e6-d4c8-46af-b451-21556534da08', 0, 370, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (788, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', 0, 371, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (789, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', 0, 371, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (790, N'2a0761e6-d4c8-46af-b451-21556534da08', 0, 371, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (791, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', 0, 372, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (792, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', 0, 372, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (793, N'2a0761e6-d4c8-46af-b451-21556534da08', 0, 372, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (794, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 383, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (795, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 383, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (796, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 383, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (797, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 384, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (798, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 384, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (799, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 384, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (800, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 385, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (801, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 385, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (802, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 385, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (803, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 386, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (804, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 386, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (805, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 386, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (806, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 387, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (807, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 387, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (808, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 387, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (809, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 388, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (810, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 388, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (811, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 388, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (812, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 389, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (813, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 389, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (814, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 389, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (815, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 390, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (816, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 390, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (817, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 390, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (818, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 391, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (819, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 391, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (820, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 391, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (821, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', 0, 392, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (822, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', 0, 392, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (823, N'9ea611ce-702f-4e12-8861-495b42f70d0f', 0, 392, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (824, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 403, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (825, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 403, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (826, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 403, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (827, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 404, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (828, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 404, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (829, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 404, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (830, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 405, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (831, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 405, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (832, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 405, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (833, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 406, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (834, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 406, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (835, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 406, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (836, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 407, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (837, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 407, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (838, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 407, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (839, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 408, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (840, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 408, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (841, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 408, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (842, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 409, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (843, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 409, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (844, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 409, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (845, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 410, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (846, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 410, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (847, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 410, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (848, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 411, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (849, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 411, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (850, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 411, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (851, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', 0, 412, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (852, N'f8a8cbf2-2546-4378-b01a-56272adbc419', 0, 412, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (853, N'29d1a49c-d290-42ce-8336-5a132fc40883', 0, 412, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (854, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 433, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (855, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 433, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (856, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 434, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (857, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 434, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (858, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 435, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (859, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 435, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (860, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 436, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (861, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 436, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (862, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 437, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (863, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 437, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (864, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 438, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (865, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 438, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (866, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 439, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (867, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 439, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (868, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 440, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (869, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 440, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (870, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 441, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (871, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 441, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (872, N'70b47530-8d88-428c-9ae8-67750d95bb66', 0, 442, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (873, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', 0, 442, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (874, N'46369be6-e8ad-4ff7-a322-8e8384bc1959', 1, 461, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (875, N'46369be6-e8ad-4ff7-a322-8e8384bc1959', 1, 462, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (876, N'46369be6-e8ad-4ff7-a322-8e8384bc1959', 1, 463, NULL)
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (877, N'46369be6-e8ad-4ff7-a322-8e8384bc1959', 1, 464, NULL)
GO
INSERT [dbo].[Atendence] ([Id], [StudentId], [Status], [SchedualId], [Note]) VALUES (882, N'8c7e7d71-788a-4181-85db-edc41dbd9459', 1, 469, NULL)
SET IDENTITY_INSERT [dbo].[Atendence] OFF
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (1, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'6f9619ff-8b86-d011-b42d-00c04fc964f2', N'Today Is Very Good', N'<p>Tech Crunch là một trong những ví dụ blog thành công nhất để lấy cảm hứng từ đó. Được thành lập vào năm 2005 bởi Michael Arrington, nó bao gồm các tin tức và câu chuyện về các công nghệ mới nhất và các công ty khởi nghiệp.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/techcrunch.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/techcrunch.png?resize=884%2C432&amp;ssl=1'' alt=''ví dụ tốt nhất về blog'' width=''884'' height=''432''></a></p><p>Vào năm 2010, AOL đã mua lại công ty với giá khoảng 25 triệu USD và hiện tại nó đã được sáp nhập với Yahoo Inc.</p><p><strong>Bí quyết thành công của TechCrunch là gì?</strong></p><p>Dưới đây là một số lý do chính tại sao TechCrunch là một trang web công nghệ trị giá hàng triệu đô la.</p><ul><li><strong>Niềm đam mê : </strong>Lý do số một cho sự thành công của TechCrunch là niềm đam mê của Michael Arrington. Mặc dù Arrington không còn là Giám đốc điều hành của TechCrunch (trang web hiện đang được điều hành bởi Yahoo), nhưng phần lớn thành công của nó là do ông. Anh ấy có niềm đam mê tuyệt đối với công nghệ, khởi nghiệp và tinh thần kinh doanh.</li><li><strong>Nhiệm vụ: </strong>Nhiệm vụ của TechCrunch là trợ giúp những người sáng lập công ty khởi nghiệp bằng cách cung cấp các phân tích tiếp thị, dữ liệu khởi động, thông tin chi tiết về công nghệ cập nhật từng phút, v.v.</li><li><strong>Sự kiện thường niên: </strong>TechCrunch còn được biết đến với hội nghị khởi nghiệp thường niên mang tên “TechCrunch Disrupt”, được tổ chức tại một số thành phố ở Hoa Kỳ, Châu Âu và Trung Quốc. Cô ấy tập trung vào các tin tức và sự phát triển về công nghệ với các nhà lãnh đạo có tư tưởng có ảnh hưởng và những người sáng lập công ty khởi nghiệp.</li></ul><p>Nếu bạn cần lời khuyên về việc bắt đầu kinh doanh hoặc tin tức công nghệ mới nhất, TechCrunch là một trong những trang web tốt nhất bạn có thể theo dõi.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (2, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'af6237dd-2c94-b055-a63c-00c04fc964f2', N'Today Is Very Bad', N'<p>The Verge là website công nghệ nổi tiếng của Mỹ được thành lập năm 2011 và vận hành bởi Vox Media. Bạn sẽ tìm thấy mọi thứ từ tin tức công nghệ mới nhất, đánh giá sản phẩm, khoa học, giải trí, v.v.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/verge.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/verge.png?resize=884%2C430&amp;ssl=1'' alt='''' width=''884'' height=''430''></a></p><p>Joshua Topolsky, đồng sáng lập và biên tập viên của The Verge, là một nhà báo công nghệ người Mỹ.</p><p><strong>Tại sao trang web The Verge lại thành công như vậy?</strong></p><p>Dưới đây là một số lý do đằng sau sự thành công của The Verge.</p><ul><li><strong>Giá trị duy nhất: </strong>Cung cấp cho mọi người giá trị độc đáo giúp các công ty nổi bật so với các đối thủ cạnh tranh của họ. The Verge được biết đến với sự độc đáo của nó. Nó xuất bản một loạt các chủ đề bao gồm tin tức, bài báo nổi bật, hướng dẫn, đánh giá sản phẩm, podcast, v.v. mà không ảnh hưởng đến chất lượng.</li><li><strong>Tin mới nhất : </strong>The Verge chủ yếu bao gồm các tin tức liên quan đến công nghệ. Từ câu chuyện về Twitter của Elon Musk đến tin tức công nghệ mới nhất về các thiết bị và ứng dụng tốt nhất thế giới, tất cả đều có ở đây.</li></ul><p>Nếu bạn đang tìm kiếm các mẹo mua sắm tiện ích công nghệ và tin tức công nghệ cập nhật từng phút, bạn cần đọc The Verge.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (3, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'2e373a91-baf4-4a84-95f7-8118ebacd0af', N'Mastering Data Science: A Comprehensive Guide for Beginners', N'<p>Một trong những ví dụ tốt nhất về blog WordPress là Mashable, được thành lập vào năm 2005 bởi Pete Cashmore. Anh ấy đã tăng lưu lượng truy cập blog công nghệ này lên hơn hai triệu người đọc trong khoảng thời gian 18 tháng.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/Mashable.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/Mashable.png?resize=884%2C424&amp;ssl=1'' alt='''' width=''880'' height=''422''></a></p><p><strong>Các yếu tố khiến Mashable trở nên phổ biến là gì?</strong></p><p><strong>Tính nhất quán của các bài đăng trên blog:</strong> Ngay từ đầu, Mashable đã xuất bản 2-3 bài báo mỗi ngày về công nghệ và phương tiện kỹ thuật số. Hơn nữa, các tác giả đã đăng tất cả các tin tức thịnh hành và lan truyền, giúp thu hút lượng truy cập khổng lồ từ Google và mạng xã hội.</p><p><strong>Một loạt các chủ đề: </strong>Có hai cách để xây dựng một trang web thành công. Một là bao gồm vô số chủ đề, hai là tập trung vào một hoặc hai chủ đề. Pete Cashmore của Mashable muốn có được một số lượng lớn người đăng ký, vì vậy anh ấy đảm bảo đưa tin về những chủ đề phổ biến nhất.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1233204933.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (4, 4, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'9f7438cc-3b23-d044-e53f-00c04fb964f2', N'This is our center', NULL, N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-29' AS Date), N'hieuht', N'hieuht', N'2589274231229637.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (5, 4, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'e8f8390d-c01b-4a24-a00d-d88c2f79107b', N'ABOUT US', N'<p>Tạp chí Đàn ông là một blog phong cách sống nổi tiếng tập trung vào sức khỏe, thể lực, phong cách, thời trang và hơn thế nữa. Nó được thành lập bởi Jann Wenner, một ông trùm tạp chí người Mỹ.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/mens-journal.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/mens-journal.png?resize=884%2C500&amp;ssl=1'' alt='''' width=''884'' height=''500''></a></p><p><strong>Tại sao thành công này?</strong></p><p>Dưới đây là một vài lý do dẫn đến sự thành công của Tạp chí Đàn ông.</p><p><strong>Tập trung vào nghiên cứu trường hợp: </strong>Nếu bạn muốn xây dựng một blog tốt hơn trong một thị trường ngách đông đúc như thể hình, bạn cần tập trung vào việc xây dựng độc giả và Men''s Journal làm điều đó rất tốt. Tạp chí Đàn ông thường xuất bản các nghiên cứu điển hình về người dùng dưới dạng ''Câu chuyện thành công'', nơi bạn sẽ tìm thấy sự biến đổi của mọi người.</p><p><strong>Được tập trung: </strong>Ngành thể hình là một ngành KHỔNG LỒ với rất nhiều sự cạnh tranh. Tạp chí Đàn ông nổi bật so với các đối thủ cạnh tranh bằng cách tập trung vào các chủ đề liên quan đến nam giới như mẹo tập thể dục, mẹo du lịch, mẹo thời trang, v.v. cho nam giới.</p>', N'Over 10 Years in Distant learning for Skill Development', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-29' AS Date), N'hieuht', N'hieuht', N'11cacd38d6232748232.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (6, 4, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'9f7438cc-3b23-d044-e53f-00c04fb964f2', N'The Best Program to Enroll for Exchange', N'Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit.', NULL, 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'hero-banner-1.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (7, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'8b99d7a2-8678-4daa-9a10-c07997b5a510', N'Notification1', N'<p>Tạp chí Đàn ông là một blog phong cách sống nổi tiếng tập trung vào sức khỏe, thể lực, phong cách, thời trang và hơn thế nữa. Nó được thành lập bởi Jann Wenner, một ông trùm tạp chí người Mỹ.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/mens-journal.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/mens-journal.png?resize=884%2C500&amp;ssl=1'' alt='''' width=''884'' height=''500''></a></p><p><strong>Tại sao thành công này?</strong></p><p>Dưới đây là một vài lý do dẫn đến sự thành công của Tạp chí Đàn ông.</p><p><strong>Tập trung vào nghiên cứu trường hợp: </strong>Nếu bạn muốn xây dựng một blog tốt hơn trong một thị trường ngách đông đúc như thể hình, bạn cần tập trung vào việc xây dựng độc giả và Men''s Journal làm điều đó rất tốt. Tạp chí Đàn ông thường xuất bản các nghiên cứu điển hình về người dùng dưới dạng ''Câu chuyện thành công'', nơi bạn sẽ tìm thấy sự biến đổi của mọi người.</p><p><strong>Được tập trung: </strong>Ngành thể hình là một ngành KHỔNG LỒ với rất nhiều sự cạnh tranh. Tạp chí Đàn ông nổi bật so với các đối thủ cạnh tranh bằng cách tập trung vào các chủ đề liên quan đến nam giới như mẹo tập thể dục, mẹo du lịch, mẹo thời trang, v.v. cho nam giới.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (8, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'4752abea-e82a-4071-a00c-0be52c646909', N'Unlock Your Creativity: A Writing Workshop for Aspiring Authors', N'<p>Treehugger là một trang web bền vững và phong cách sống về thiết kế, nhà cửa và khu vườn thân thiện với môi trường. Được tạo bởi Graham Hill vào năm 2005, nó được Nielsen Netratings xếp hạng blog bền vững tốt nhất vào năm 2007 và cũng được đăng trên Tạp chí Time.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/treehugger.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/treehugger.png?resize=884%2C488&amp;ssl=1'' alt='''' width=''884'' height=''488''></a></p><p><strong>Tại sao thành công này?</strong></p><p>Dưới đây là một số lý do tại sao Treehugger là một ví dụ về một blog thành công.</p><p><strong>Có một nhiệm vụ:</strong> Nhiệm vụ của Treehugger là giúp mọi người thiết kế các sản phẩm thân thiện với môi trường. Có một nhiệm vụ mạnh mẽ giúp bạn tập trung và có động lực khi bạn cố gắng đạt được mục tiêu viết blog của mình.</p><p>Quan trọng hơn, sứ mệnh của bạn sẽ truyền cảm hứng cho bạn hành động và dẫn bạn đến mục tiêu cuối cùng của mình. Đó là cách Treehugger trở thành một blog phổ biến với hơn 2,5 triệu độc giả hàng tháng trên khắp thế giới.</p><p><strong>Củng cố uy tín của bạn: </strong>Treehugger có rất nhiều uy tín khi nói đến thiết kế xanh. Để thiết lập uy tín của bạn, điều cần thiết là phải thiết lập bản thân như một chuyên gia trong lĩnh vực của bạn.</p><p>Bất kể bạn đang ở lĩnh vực nào, bạn có thể xây dựng uy tín của mình bằng cách xuất bản các bài báo, sách, video, v.v. có chất lượng tuyệt vời. Bằng cách chia sẻ kiến ​​thức của mình với người khác, bạn sẽ không chỉ xây dựng được uy tín của mình mà còn thu hút được nhiều người theo dõi.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (10, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'55538d0d-3a29-4da0-af5e-4f04c2729a76', N'Hôm qua rất vuia', N'<p>Treehugger là một trang web bền vững và phong cách sống về thiết kế, nhà cửa và khu vườn thân thiện với môi trường. Được tạo bởi Graham Hill vào năm 2005, nó được Nielsen Netratings xếp hạng blog bền vững tốt nhất vào năm 2007 và cũng được đăng trên Tạp chí Time.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/treehugger.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/treehugger.png?resize=884%2C488&amp;ssl=1'' alt='''' width=''884'' height=''488''></a></p><p><strong>Tại sao thành công này?</strong></p><p>Dưới đây là một số lý do tại sao Treehugger là một ví dụ về một blog thành công.</p><p><strong>Có một nhiệm vụ:</strong> Nhiệm vụ của Treehugger là giúp mọi người thiết kế các sản phẩm thân thiện với môi trường. Có một nhiệm vụ mạnh mẽ giúp bạn tập trung và có động lực khi bạn cố gắng đạt được mục tiêu viết blog của mình.</p><p>Quan trọng hơn, sứ mệnh của bạn sẽ truyền cảm hứng cho bạn hành động và dẫn bạn đến mục tiêu cuối cùng của mình. Đó là cách Treehugger trở thành một blog phổ biến với hơn 2,5 triệu độc giả hàng tháng trên khắp thế giới.</p><p><strong>Củng cố uy tín của bạn: </strong>Treehugger có rất nhiều uy tín khi nói đến thiết kế xanh. Để thiết lập uy tín của bạn, điều cần thiết là phải thiết lập bản thân như một chuyên gia trong lĩnh vực của bạn.</p><p>Bất kể bạn đang ở lĩnh vực nào, bạn có thể xây dựng uy tín của mình bằng cách xuất bản các bài báo, sách, video, v.v. có chất lượng tuyệt vời. Bằng cách chia sẻ kiến ​​thức của mình với người khác, bạn sẽ không chỉ xây dựng được uy tín của mình mà còn thu hút được nhiều người theo dõi.</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-12-05' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (11, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'745fa309-a91e-4374-889e-fa274e606cf1', N'Mindfulness Meditation: Cultivating Inner Peace in a Busy World', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (12, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'a05d26af-f8d7-4f2a-a8a5-4471368b4bd2', N'Photography Fundamentals: Capturing Stunning Images with Any Camera', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication5', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (13, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'ef0434ab-0397-436e-9181-32a851362154', N'Financial Literacy Bootcamp: Managing Your Money Like a Pro', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication6', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (14, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'a4476f4b-26f5-4ce5-a553-e0bda499a3a0', N'Learn a New Language: A Step-by-Step Guide to Fluency', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication7', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (15, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5ee14258-fcf5-4cbb-b4f6-6a9aab540bac', N'Excel Mastery for Business: Boost Your Productivity and Efficiency', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication8', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (16, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'4e1f32af-9cc3-4a5e-bcd2-2bd9def6339c', N'Fitness Foundations: Building a Sustainable Exercise Routine', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication9', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (17, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'712eecaf-15ce-4ad2-9f06-91b418927611', N'Mastering Data Science: A Comprehensive Guide for Beginners', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication10', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (18, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'07763bc3-680f-48ea-a661-d5ef72f31550', N'The Art of Digital Marketing: Strategies for Success in 2023', N'<p>The Verge là website công nghệ nổi tiếng của Mỹ được thành lập năm 2011 và vận hành bởi Vox Media. Bạn sẽ tìm thấy mọi thứ từ tin tức công nghệ mới nhất, đánh giá sản phẩm, khoa học, giải trí, v.v.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/verge.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/verge.png?resize=884%2C430&amp;ssl=1'' alt='''' width=''884'' height=''430''></a></p><p>Joshua Topolsky, đồng sáng lập và biên tập viên của The Verge, là một nhà báo công nghệ người Mỹ.</p><p><strong>Tại sao trang web The Verge lại thành công như vậy?</strong></p><p>Dưới đây là một số lý do đằng sau sự thành công của The Verge.</p><ul><li><strong>Giá trị duy nhất: </strong>Cung cấp cho mọi người giá trị độc đáo giúp các công ty nổi bật so với các đối thủ cạnh tranh của họ. The Verge được biết đến với sự độc đáo của nó. Nó xuất bản một loạt các chủ đề bao gồm tin tức, bài báo nổi bật, hướng dẫn, đánh giá sản phẩm, podcast, v.v. mà không ảnh hưởng đến chất lượng.</li><li><strong>Tin mới nhất : </strong>The Verge chủ yếu bao gồm các tin tức liên quan đến công nghệ. Từ câu chuyện về Twitter của Elon Musk đến tin tức công nghệ mới nhất về các thiết bị và ứng dụng tốt nhất thế giới, tất cả đều có ở đây.</li></ul><p>Nếu bạn đang tìm kiếm các mẹo mua sắm tiện ích công nghệ và tin tức công nghệ cập nhật từng phút, bạn cần đọc The Verge.</p>', N'This is notication11', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (19, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'835ea212-3840-47c1-9192-7b81e045018f', N'Mastering Data Science: A Comprehensive Guide for Beginners', N'<p>Một trong những ví dụ tốt nhất về blog WordPress là Mashable, được thành lập vào năm 2005 bởi Pete Cashmore. Anh ấy đã tăng lưu lượng truy cập blog công nghệ này lên hơn hai triệu người đọc trong khoảng thời gian 18 tháng.</p><p><a href=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/Mashable.png?ssl=1''><img src=''https://i0.wp.com/blogpascher.com/wp-content/uploads/2023/05/Mashable.png?resize=884%2C424&amp;ssl=1'' alt='''' width=''884'' height=''424''></a></p><p><strong>Các yếu tố khiến Mashable trở nên phổ biến là gì?</strong></p><p><strong>Tính nhất quán của các bài đăng trên blog:</strong> Ngay từ đầu, Mashable đã xuất bản 2-3 bài báo mỗi ngày về công nghệ và phương tiện kỹ thuật số. Hơn nữa, các tác giả đã đăng tất cả các tin tức thịnh hành và lan truyền, giúp thu hút lượng truy cập khổng lồ từ Google và mạng xã hội.</p><p><strong>Một loạt các chủ đề: </strong>Có hai cách để xây dựng một trang web thành công. Một là bao gồm vô số chủ đề, hai là tập trung vào một hoặc hai chủ đề. Pete Cashmore của Mashable muốn có được một số lượng lớn người đăng ký, vì vậy anh ấy đảm bảo đưa tin về những chủ đề phổ biến nhất.</p>', N'This is notication12', 1, CAST(N'2001-09-20' AS Date), CAST(N'2023-11-28' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (20, 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'74a99479-bbc1-40b1-9736-0c8f02dc000c', N'Home', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication12', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', 1, N'/#')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (21, 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'e34e5217-c64d-4fd5-983f-eead895a55c6', N'News', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication12', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', 2, N'/notification')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (22, 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'33335e6b-9ed5-45fa-a828-a0342f564b72', N'FAQ', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication12', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', 3, N'/faq')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (23, 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'eb6d5e67-b474-4a20-afcc-f16b940f96ae', N'About Us', N'A small river named Duden flows by their place and supplies it with the necessary regelialia', N'This is notication12', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', 4, N'/aboutus')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (24, 6, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'966e3adc-02c6-455b-b1a3-592f8d2c4ee2', N'Blog', N'<p>Hôm nay tôi bu?n</p><figure class=''image''><img src=''https://localhost:7241/Images/than bai.jpg''></figure><p>nên tôi r?t vui</p>', N'I dont know what do you want to go and run', 1, CAST(N'2001-09-20' AS Date), CAST(N'2001-09-20' AS Date), N'hieuht', N'hieuht', N'nhat_cay-1232538650.jpg', NULL, N'/notification')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (25, 6, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'9f7438cc-3b23-d044-e53f-00c04fb964f2', N'Events', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'/notification')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (30, 5, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'cb1b9720-1648-4950-9957-62b271c9e4f8', N'About Us', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'/')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (31, 7, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'72318650-9a95-477d-88c0-c1640f163ea8', N'Math', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'/')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (32, 7, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'dd979597-d111-43cc-8c9f-29b4671ddb8c', N'English', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'/')
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (33, 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'c54c914e-c9ef-4492-b055-b1af5a090f32', N'Mastering Data Science: A Comprehensive Guide for Beginners', N'<p><strong>UI/UX design là gì?</strong></p><p>Khái niệm về UI/UX dưới đây sẽ giúp bạn phân biệt giữa 2 hình thức:</p><figure class=''image''><img style=''aspect-ratio:2560/1700;'' src=''https://localhost:7241/Images/2589274.jpg'' width=''2560'' height=''1700''></figure><h3>1. Khái niệm UI design</h3><p><strong>UI</strong> (<strong>User Interface</strong> - <strong>giao diện người dùng</strong>) liên quan đến cách mà người dùng tương tác với một sản phẩm hoặc dịch vụ trên một thiết bị hoặc trang web. Giao diện người dùng có thể bao gồm các thành phần như nút, thanh công cụ, bảng điều khiển, menu và các phần tử tương tác khác, nhằm giúp người dùng dễ dàng và hiệu quả hơn trong việc tương tác với sản phẩm hoặc dịch vụ đó.</p><h3>2. Khái niệm UX design</h3><p><strong>UX</strong> (<strong>User Experience</strong> - <strong>trải nghiệm người dùng</strong>) liên quan đến cách mà người dùng tương tác với trang web, ứng dụng hoặc sản phẩm điện tử khác. Điều này bao gồm các yếu tố như thiết kế giao diện, trải nghiệm người dùng, khả năng tương tác và tính thân thiện với người dùng. Mục tiêu của UX là cung cấp trải nghiệm tốt nhất có thể cho người dùng, giúp họ tìm kiếm thông tin hoặc hoàn thành một tác vụ một cách dễ dàng và nhanh chóng. Tối ưu hóa UX là một yếu tố rất quan trọng trong việc tăng cường hiệu quả của một trang web hoặc ứng dụng và thu hút người dùng trung thực</p>', N'đây là 1 bài viết mới', 1, CAST(N'2023-11-17' AS Date), NULL, NULL, NULL, N'192004230207321.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (34, 1, NULL, N'bfc86abf-cac7-463c-bdab-f0c40175183c', N'The Art of Digital Marketing: Strategies for Success in 2023', N'<p><strong>UI/UX design là gì?</strong></p><p>Khái niệm về UI/UX dưới đây sẽ giúp bạn phân biệt giữa 2 hình thức:</p><figure class=''image''><img style=''aspect-ratio:800/416;'' src=''https://resources.mindx.edu.vn/uploads/images/uiux%20l%C3%A0%20g%C3%AC%20.png'' alt=''UI và UX design'' width=''800'' height=''416''><figcaption>UI/UX là viết tắt của từ User Interface/User Experience</figcaption></figure><h3>1. Khái niệm UI design</h3><p><strong>UI</strong> (<strong>User Interface</strong> - <strong>giao diện người dùng</strong>) liên quan đến cách mà người dùng tương tác với một sản phẩm hoặc dịch vụ trên một thiết bị hoặc trang web. Giao diện người dùng có thể bao gồm các thành phần như nút, thanh công cụ, bảng điều khiển, menu và các phần tử tương tác khác, nhằm giúp người dùng dễ dàng và hiệu quả hơn trong việc tương tác với sản phẩm hoặc dịch vụ đó.</p><h3>2. Khái niệm UX design</h3><p><strong>UX</strong> (<strong>User Experience</strong> - <strong>trải nghiệm người dùng</strong>) liên quan đến cách mà người dùng tương tác với trang web, ứng dụng hoặc sản phẩm điện tử khác. Điều này bao gồm các yếu tố như thiết kế giao diện, trải nghiệm người dùng, khả năng tương tác và tính thân thiện với người dùng. Mục tiêu của UX là cung cấp trải nghiệm tốt nhất có thể cho người dùng, giúp họ tìm kiếm thông tin hoặc hoàn thành một tác vụ một cách dễ dàng và nhanh chóng. Tối ưu hóa UX là một yếu tố rất quan trọng trong việc tăng cường hiệu quả của một trang web hoặc ứng dụng và thu hút người dùng trung thực.</p>', N'đây là 1 bài viết mới', 1, CAST(N'2023-11-17' AS Date), CAST(N'2023-12-01' AS Date), NULL, NULL, N'5d6bad2a76233819833.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (35, 9, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'7cdff132-c4c0-4a06-b148-f8b9aff18bdc', N'Về chúng tôi', N'<figure class=''image''><img style=''aspect-ratio:392/257;'' src=''https://smac.com.vn/assets/img/gioithieu3.png'' alt='''' width=''392'' height=''257''></figure><h2><strong>Chúng tôi là ai?</strong></h2><p>Ngày 27/03/2017: Pitech được thành lập bởi những nhà quản lý, chuyên gia công nghệ của các tập đoàn hàng đầu Việt Nam như FPT, Viettel, Mobifone... Pitech là một trong những công ty công nghệ hàng đầu với đội ngũ chuyên môn có kinh nghiệm, tâm huyết, sáng tạo và tài năng; chuyên cung cấp các sản phẩm, giải pháp và dịch vụ công nghệ thông tin cho các khách hàng thuộc khối như Viễn thông, Tài chính, Vận tải, Doanh nghiệp...</p><h2><strong>Sứ mệnh</strong></h2><figure class=''image image-style-side''><img style=''aspect-ratio:2280/1588;'' src=''https://smac.com.vn/assets/img/gioithieu1.png'' alt='''' width=''2280'' height=''1588''></figure><p>Chúng tôi tận tâm, tận lực, sáng tạo để tạo ra các sản phẩm và dịch vụ tốt và hiện đại để làm hài lòng khách hàng, cải thiện chất lượng cuộc sống; mang đến cho mỗi thành viên một môi trường phát huy tối đa năng lực của bản thân và cuộc sống sung túc, hạnh phúc</p><h2><strong>Tầm nhìn</strong></h2><p>Chúng tôi không ngừng mở rộng mạng lưới khách hàng và đối tác trên khắp thế giới để trao đổi, áp dụng, nghiên cứu các giải pháp, xu hướng công nghệ trong tương lai và trở thành công ty hàng đầu thế giới trong việc cung cấp các giải pháp kỹ thuật số</p>', N'Là địa điểm học tập đa dạng và thân thiện, nơi học viên không chỉ nắm vững kiến thức ngôn ngữ mà còn trải nghiệm sự hứng khởi trong quá trình học.', 1, CAST(N'2023-11-26' AS Date), CAST(N'2023-12-01' AS Date), N'hieuht', NULL, N'11cacd38d6234117318.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (36, 10, NULL, N'58b794e1-e419-48ee-b9b9-1f121e1a9707', N'GIÁO VIÊN GIỎI - GIỜ HỌC LINH HOAT', N'<p>a</p>', N'Đội ngũ giáo viên tất cả trình độ Cử nhân Đại học trở nên và có đầy đủ chứng chỉ nghiệp vụ sư phạm dạy nghề của Tổng cục đường bộ VN cấp', 1, CAST(N'2023-12-01' AS Date), NULL, NULL, NULL, N'cat-1230959128.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (37, 10, NULL, N'7fa10364-3065-4748-993d-c55785bcc656', N'Các lớp học online', N'<p>a</p>', N'Có những lớp học online để giúp đỡ tận tình học sinh, sinh viên', 1, CAST(N'2023-12-01' AS Date), NULL, NULL, NULL, N'cat-2231122915.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (38, 10, NULL, N'dc9615d4-c55c-4e3e-9a64-9e75b6f36842', N'Những bài học mới lạ', N'<p>a</p>', N'Có sãn những project hay những bài tập tiếng anh đầy lý thú', 1, CAST(N'2023-12-01' AS Date), NULL, NULL, NULL, N'cat-3231218254.jpg', NULL, NULL)
INSERT [dbo].[Blog] ([id], [cate_id], [center_id], [blog_code], [title], [description], [short_description], [status], [created_date], [update_date], [created_by], [update_by], [images], [position], [url]) VALUES (39, 10, NULL, N'30ec8f0f-c9d5-4bc3-9038-5f81a9194ccc', N'Thư viện sách', N'<p>a</p>', N'Cung cấp dịch vụ mượn sách, giáo cho các sinh viên để có thể dễ dàng tiếp cận những điều mới', 1, CAST(N'2023-12-01' AS Date), NULL, NULL, NULL, N'cat-4231337172.jpg', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[CateBlog] ON 

INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (1, N'Blog', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (2, N'Footer', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (3, N'Header', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (4, N'Banner', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (5, N'Contact', NULL, NULL, 2, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (6, N'About Pitech', NULL, NULL, 2, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (7, N'Course', NULL, NULL, 2, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (8, N'Notification', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (9, N'About Us', NULL, NULL, NULL, 1)
INSERT [dbo].[CateBlog] ([Id], [name], [description], [level], [parent_id], [status]) VALUES (10, N'Information', NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[CateBlog] OFF
GO
INSERT [dbo].[Center] ([Id], [Name], [Description], [Adress], [CodeCenter], [AdminId], [NumberPhone], [CreateDate], [UpdateDate], [Status]) VALUES (N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'PiTechLab', N'IELTS Fighter - trung tâm luy?n thi IELTS s? 1 Vi?t Nam. V?i s? m?nh giúp hàng tri?u ngu?i Vi?t d?t 6.5-7.0 IELTS, IELTS Fighter n? l?c m?i ngày d? cùng các b?n h?c IELTS d? nhu an bánh, chinh ph?c du?c d?nh cao IELTS!', N'107 Xuân La - Số nhà D21, Bắc Từ Liêm', N'PTL', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'0123456789', CAST(N'2020-10-10' AS Date), CAST(N'2020-10-10' AS Date), 1)
INSERT [dbo].[Center] ([Id], [Name], [Description], [Adress], [CodeCenter], [AdminId], [NumberPhone], [CreateDate], [UpdateDate], [Status]) VALUES (N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'Alaska', N'Trung tâm Anh ng? Qu?c t? Alaska Vi?t Nam (Vi?t t?t là ALASKA ENGLISH) là thuong hi?u b?n quy?n c?a Công ty C? ph?n Thuong m?i & Giáo d?c Alaska Vi?t Nam.  Alaska English dã và dang d?n tr? thành m?t trong nh?ng t? ch?c giáo d?c Anh ng? uy tín hàng d?u t?i Vi?t Nam. V?i d?i ngu 100% giáo viên nu?c ngoài có trình d? chuyên môn su ph?m cao, t?n tâm yêu ngh?, cùng d?i ngu qu?n lý có nhi?u nam kinh nghi?m ho?t d?ng trong ngành giáo d?c cung nhu cán b?, nhân viên chuyên nghi?p, nhi?t tình dã giúp chúng tôi t?o d?ng du?c ni?m tin noi khách hàng và giúp thuong hi?u Alaska English dã du?c nhi?u ph? huynh bi?t d?n. Chúng tôi cam k?t mang d?n m?t môi tru?ng h?c t?p chuyên nghi?p, mang tính qu?c t? cho t?t c? các h?c viên ? m?i l?a tu?i khác nhau t? M?u giáo, Thi?u nhi, Thi?u niên và Ngu?i l?n.', N'L1 – LKV 13 Khu Đô Thị HUD Sơn Tây, Trung Hưng, Sơn Tây, Hà Nội', N'ALK', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'0987654321', CAST(N'2020-10-10' AS Date), CAST(N'2020-10-10' AS Date), 1)
INSERT [dbo].[Center] ([Id], [Name], [Description], [Adress], [CodeCenter], [AdminId], [NumberPhone], [CreateDate], [UpdateDate], [Status]) VALUES (N'9c8d8c3d-8b27-4c87-a236-4d0ce2d9606b', N'UUUAUUUAUAUU', N'string', N'string', N'string', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'string', CAST(N'2023-11-14' AS Date), CAST(N'2023-11-14' AS Date), 2)
GO
SET IDENTITY_INSERT [dbo].[Class] ON 

INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (47, N'IELTS5.0A1Phi', N'IELTS5A1', 2, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 1, NULL, 5, CAST(N'2023-12-11' AS Date), CAST(N'2023-12-26' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (48, N'IELTS4B1', N'IELTS4B1Phil', 3, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 1, NULL, 10, CAST(N'2023-12-11' AS Date), CAST(N'2024-01-10' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (49, N'IELTS7A1', N'IELTS7A1Phil', 2, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 1, NULL, 10, CAST(N'2023-12-11' AS Date), CAST(N'2024-01-10' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (50, N'IELTS5B1', N'IELTS5B1Phil', 4, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 3, NULL, 10, CAST(N'2023-12-11' AS Date), CAST(N'2024-01-10' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (51, N'IELTS5C1', N'IELTS5C1Phil', 2, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 0, NULL, 5, CAST(N'2024-01-27' AS Date), CAST(N'2024-02-16' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (52, N'IELTS7A2', N'IELTS7A2Phil', 17, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 2, NULL, 4, CAST(N'2023-11-27' AS Date), CAST(N'2023-12-08' AS Date), CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[Class] ([id], [Name], [Code], [CourceId], [AdminCenterId], [TeacherId], [CenterId], [NotificationId], [Status], [SlotId], [SlotNumber], [StartDate], [EndDate], [DateCreate], [DateUpdate]) VALUES (53, N'IELTS7A3', N'IELTS7A2Phil', 2, N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, 2, NULL, 1, CAST(N'2023-12-12' AS Date), CAST(N'2023-12-13' AS Date), CAST(N'2023-12-11' AS Date), CAST(N'2023-12-11' AS Date))
SET IDENTITY_INSERT [dbo].[Class] OFF
GO
SET IDENTITY_INSERT [dbo].[ClassRoom] ON 

INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (1, N'Room 01', N'Al01', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (2, N'Room02', N'Al02', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (3, N'Room01', N'Be01', 2, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (4, N'Room02', N'Be02', 2, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (5, N'Room01', N'Ga01', 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (6, N'Room02', N'Ga02', 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (7, N'Room03', N'Al03', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (8, N'Room03', N'Be03', 2, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (9, N'Room03', N'Ga03', 3, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (10, N'Room 01', N'Al01', 1, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (11, N'Room02', N'Al02', 1, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (12, N'Room01', N'Be01', 2, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (13, N'Room02', N'Be02', 2, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (14, N'Room01', N'Ga01', 3, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[ClassRoom] ([Id], [Name], [Code], [TypeId], [CenterId]) VALUES (15, N'Room02', N'Ga02', 3, N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
SET IDENTITY_INSERT [dbo].[ClassRoom] OFF
GO
SET IDENTITY_INSERT [dbo].[ClassSlotDate] ON 

INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (60, 47, 1, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (61, 47, 6, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (62, 48, 7, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (63, 48, 12, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (64, 49, 8, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (65, 49, 13, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (66, 50, 6, 2, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (67, 50, 11, 1, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (68, 51, 21, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (69, 51, 26, 1, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (71, 52, 23, 2, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (72, 52, 18, 2, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
INSERT [dbo].[ClassSlotDate] ([Id], [ClassId], [SlotId], [RoomId], [TeacherId], [Uid]) VALUES (73, 53, 15, 6, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', NULL)
SET IDENTITY_INSERT [dbo].[ClassSlotDate] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (2, N'IELTS5APhil', N'IELTS5.0A-Phil(24b)', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 2995000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', 1, 2, 1, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (3, N'IELTS40BPhil', N'IELTS4.0B-Phil', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 3600000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', 1, 3, 1, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (4, N'IELTS5BPhil', N'IELTS5.0B-Phil(24b)', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 2995000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', 1, 3, 1, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (5, N'Khóa Học 4 ', N'KH3', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 3000.0000, NULL, N'DefaultImg.png', NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'76e78cbf-aa68-466b-9846-c3242faa00e8', 1, 4, 1, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (9, N'IELTS5CPhil', N'IELTS5.0C-Phil(24b)', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 2995000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', NULL, 5, 1, NULL, CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (13, N'IELTS40APhil', N'IELTS4.0A-Phil', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 3600000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', NULL, 2, 1, CAST(N'2023-10-18' AS Date), CAST(N'2023-12-10' AS Date), 5)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (15, N'IELTS40CPhil', N'IELTS4.0C-Phil', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 3600000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', NULL, 2, 1, CAST(N'2023-11-14' AS Date), CAST(N'2023-12-10' AS Date), 4)
INSERT [dbo].[Course] ([id], [Name], [Code], [Description], [Price], [ClassId], [ImageSrc], [BillId], [CenterId], [EmployeeId], [TypeId], [SyllabusId], [Status], [DateCreate], [DateUpdate], [Rating]) VALUES (17, N'IELTS7APhil', N'IELTS7.0A-Phil', N'Embark on a transformative learning journey with our [Course Name]! This dynamic online course caters to diverse learners seeking [mention key benefits]. Delve into engaging modules, master practical skills, and interact with a global community. Our expert instructors ensure a rich and collaborative learning experience. Whether you''re a novice or expert, [Course Name] equips you with essential tools for success. Elevate your knowledge, broaden your perspectives, and unlock new opportunities. Join us in shaping a brighter future—enroll today!', 3895000.0000, NULL, N'DefaultImg.png', NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', NULL, 3, 1, CAST(N'2023-11-22' AS Date), CAST(N'2023-12-10' AS Date), 4)
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [Verfify], [DateUpdate], [Status]) VALUES (N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', N'Admin Hoàng Hiếu Alaska', NULL, NULL, NULL, NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'36cb6da0-72af-4381-8b9b-38fe047de6fd', N'AdminCenter2', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-12' AS Date), NULL, NULL, NULL, CAST(N'2023-10-12' AS Date), 1)
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [Verfify], [DateUpdate], [Status]) VALUES (N'43cf8980-96f3-4b4d-bd7b-3146547b4eba', N'Nguyễn Văn An', NULL, N'datt19112001@gmail.com', NULL, NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'36cb6da0-72af-4381-8b9b-38fe047de6fd', N'testthemmoi', N'2A589442164C92DA70C28F3C869D01F912294F1D6BD7847DDC243CA8DE2EAD23', CAST(N'2023-12-07' AS Date), CAST(N'2023-12-15T01:19:11.997' AS DateTime), N'eNVo9P2zTHqeGUgNwqKOtFKjpgJZFBBtM9CanuryELE=', NULL, CAST(N'2023-12-07' AS Date), 1)
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [Verfify], [DateUpdate], [Status]) VALUES (N'a9b8bcd3-f26c-4853-8c42-6243e850b3ba', N'BackOffice2', NULL, NULL, NULL, NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'bc2c1ee4-d94f-4923-a036-8c3ca1894569', N'BackOffice2', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, NULL, CAST(N'2023-10-12' AS Date), 1)
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [Verfify], [DateUpdate], [Status]) VALUES (N'76e78cbf-aa68-466b-9846-c3242faa00e8', N'Nguyễn Tiến Đạt (Admin)', N'123', N'datt19112001@gmail.com', N'1234567899', CAST(N'1970-01-01' AS Date), N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'36cb6da0-72af-4381-8b9b-38fe047de6fd', N'AdminCenter', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-12' AS Date), CAST(N'2023-12-17T22:09:26.200' AS DateTime), N'gwzfNDPOiKqIavZqJAlSN0w9Ya/Oe/sgWiCyAbc0DW8=', NULL, CAST(N'2023-12-10' AS Date), 1)
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [Verfify], [DateUpdate], [Status]) VALUES (N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', N'BackOffice', NULL, NULL, NULL, NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'bc2c1ee4-d94f-4923-a036-8c3ca1894569', N'BackOffice', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-12' AS Date), CAST(N'2023-10-19T17:13:53.383' AS DateTime), N'YpJ1YXQ3oelfpTcxMoXl2fK3Xk5LZa0t0OGZ3TUY0Mw=', NULL, CAST(N'2023-10-12' AS Date), 1)
GO
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (1, N'Read', NULL, 30, 2)
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (2, N'Write', NULL, 30, 2)
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (3, N'Translate', NULL, 40, 2)
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (4, N'Speaking', NULL, 20, 3)
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (5, N'Communicate', NULL, 40, 3)
INSERT [dbo].[Exam] ([id], [Name], [Desc], [average], [CourceId]) VALUES (6, N'Final', NULL, 40, 3)
GO
SET IDENTITY_INSERT [dbo].[NotificationClases] ON 

INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (11, N'Announce the start date of classes', N'Class IELTS5.0A1Phi will start on: 12-12-2023 Students please check the timetable for detailed information.', 47, NULL, NULL, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (12, N'Announce the start date of classes', N'Class IELTS4B1 will start on: 12-12-2023 Students please check the timetable for detailed information.', 48, NULL, NULL, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (13, N'Announce the start date of classes', N'Class IELTS7A1 will start on: 12-12-2023 Students please check the timetable for detailed information.', 49, NULL, NULL, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (14, N'Announce the start date of classes', N'Class IELTS5B1 will start on: 12-12-2023 Students please check the timetable for detailed information.', 50, NULL, NULL, CAST(N'2023-12-10' AS Date), CAST(N'2023-12-10' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (15, N'Announce the start date of classes', N'Class IELTS7A2 will start on: 30-11-2023 Students please check the timetable for detailed information.', 52, NULL, NULL, CAST(N'2023-12-11' AS Date), CAST(N'2023-12-11' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (16, N'Announce the start date of classes', N'Class IELTS7A2 will start on: 13-12-2023 Students please check the timetable for detailed information.', 53, NULL, NULL, CAST(N'2023-12-11' AS Date), CAST(N'2023-12-11' AS Date))
INSERT [dbo].[NotificationClases] ([id], [Title], [Description], [ClassId], [StudentId], [ViewCount], [Datecreate], [DateUpdate]) VALUES (17, N'Announce the start date of classes', N'Class IELTS7A3 will start on: 13-12-2023 Students please check the timetable for detailed information.', 53, NULL, NULL, CAST(N'2023-12-11' AS Date), CAST(N'2023-12-11' AS Date))
SET IDENTITY_INSERT [dbo].[NotificationClases] OFF
GO
INSERT [dbo].[Role] ([id], [Name], [Description], [DateCreate], [DateUpdate]) VALUES (N'36cb6da0-72af-4381-8b9b-38fe047de6fd', N'AdminCenter', NULL, NULL, NULL)
INSERT [dbo].[Role] ([id], [Name], [Description], [DateCreate], [DateUpdate]) VALUES (N'bc2c1ee4-d94f-4923-a036-8c3ca1894569', N'BackOffice', NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[SAQ] ON 

INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (1, N'How long has your English language center been in operation?', N'NULLThe duration of operation of an English language center often reflects its stability and experience. A center that has been operating for an extended period may have accumulated knowledge and improved its teaching methods.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (2, N'Does your English language center employ any special teaching methods to help students enhance their English language skills?', N'Each language center may have unique teaching methods. They might employ creative learning approaches, emphasize communication skills, or integrate technology into their teaching.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (3, N'NULLAre the teachers at your center certified and experienced in teaching English?', N'It''s crucial to ensure the quality of education by having teachers with relevant qualifications and teaching experience to support students in developing their English language skills.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (4, N'Does your English language center offer flexible learning programs to cater to individual student learning goals?', N'Flexible learning programs allow students to choose courses that align with their personal objectives. These programs accommodate a diverse range of needs.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (5, N'How is student progress assessed at your English language center?', N'Language centers typically conduct regular or periodic assessments to evaluate student progress. Attendance in classes and completion of assignments are common indicators of progress.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (6, N'NULLDo you offer English language courses in specialized fields or preparation for international exams like IELTS and TOEFL?', N'NULLIf you have a specific field of interest or need to prepare for international exams, it''s important to check whether the center offers courses or schedules that cater to these requirements.', NULL, NULL, CAST(N'2023-11-11' AS Date), CAST(N'2023-11-11' AS Date))
INSERT [dbo].[SAQ] ([Id], [Title], [Desc], [image], [EmployeeId], [DateCreate], [DateUpdate]) VALUES (13, N'ab', N'a', NULL, NULL, CAST(N'2023-11-22' AS Date), CAST(N'2023-11-22' AS Date))
SET IDENTITY_INSERT [dbo].[SAQ] OFF
GO
SET IDENTITY_INSERT [dbo].[Schedual] ON 

INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (368, 47, 1, NULL, CAST(N'2023-12-11' AS Date), 1, 1, 6)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (369, 47, 2, NULL, CAST(N'2023-12-13' AS Date), 5, 14, 9)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (370, 47, 3, NULL, CAST(N'2023-12-19' AS Date), 0, 6, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (371, 47, 4, NULL, CAST(N'2023-12-25' AS Date), 0, 1, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (372, 47, 5, NULL, CAST(N'2023-12-26' AS Date), 0, 6, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (383, 48, 1, NULL, CAST(N'2023-12-12' AS Date), 0, 7, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (384, 48, 2, NULL, CAST(N'2023-12-13' AS Date), 0, 12, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (385, 48, 3, NULL, CAST(N'2023-12-19' AS Date), 0, 7, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (386, 48, 4, NULL, CAST(N'2023-12-20' AS Date), 0, 12, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (387, 48, 5, NULL, CAST(N'2023-12-26' AS Date), 0, 7, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (388, 48, 6, NULL, CAST(N'2023-12-27' AS Date), 0, 12, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (389, 48, 7, NULL, CAST(N'2024-01-02' AS Date), 0, 7, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (390, 48, 8, NULL, CAST(N'2024-01-03' AS Date), 0, 12, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (391, 48, 9, NULL, CAST(N'2024-01-09' AS Date), 0, 7, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (392, 48, 10, NULL, CAST(N'2024-01-10' AS Date), 0, 12, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (403, 49, 1, NULL, CAST(N'2023-12-12' AS Date), 0, 8, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (404, 49, 2, NULL, CAST(N'2023-12-13' AS Date), 0, 13, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (405, 49, 3, NULL, CAST(N'2023-12-19' AS Date), 0, 8, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (406, 49, 4, NULL, CAST(N'2023-12-20' AS Date), 0, 13, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (407, 49, 5, NULL, CAST(N'2023-12-26' AS Date), 0, 8, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (408, 49, 6, NULL, CAST(N'2023-12-27' AS Date), 0, 13, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (409, 49, 7, NULL, CAST(N'2024-01-02' AS Date), 0, 8, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (410, 49, 8, NULL, CAST(N'2024-01-03' AS Date), 0, 13, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (411, 49, 9, NULL, CAST(N'2024-01-09' AS Date), 0, 8, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (412, 49, 10, NULL, CAST(N'2024-01-10' AS Date), 0, 13, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (433, 50, 1, NULL, CAST(N'2023-12-12' AS Date), 4, 6, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (434, 50, 2, NULL, CAST(N'2023-12-13' AS Date), 4, 11, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (435, 50, 3, NULL, CAST(N'2023-12-19' AS Date), 4, 6, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (436, 50, 4, NULL, CAST(N'2023-12-20' AS Date), 4, 11, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (437, 50, 5, NULL, CAST(N'2023-12-26' AS Date), 4, 6, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (438, 50, 6, NULL, CAST(N'2023-12-27' AS Date), 4, 11, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (439, 50, 7, NULL, CAST(N'2024-01-02' AS Date), 4, 6, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (440, 50, 8, NULL, CAST(N'2024-01-03' AS Date), 4, 11, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (441, 50, 9, NULL, CAST(N'2024-01-09' AS Date), 4, 6, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (442, 50, 10, NULL, CAST(N'2024-01-10' AS Date), 4, 11, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (448, 51, 1, NULL, CAST(N'2024-02-02' AS Date), 3, 21, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (449, 51, 2, NULL, CAST(N'2024-02-03' AS Date), 3, 26, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (450, 51, 3, NULL, CAST(N'2024-02-09' AS Date), 3, 21, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (451, 51, 4, NULL, CAST(N'2024-02-10' AS Date), 3, 26, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (452, 51, 5, NULL, CAST(N'2024-02-16' AS Date), 3, 21, 1)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (461, 52, 1, NULL, CAST(N'2023-11-30' AS Date), 2, 18, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (462, 52, 2, NULL, CAST(N'2023-12-01' AS Date), 2, 23, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (463, 52, 3, NULL, CAST(N'2023-12-07' AS Date), 2, 18, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (464, 52, 4, NULL, CAST(N'2023-12-08' AS Date), 2, 23, 2)
INSERT [dbo].[Schedual] ([Id], [ClassId], [SlotNum], [StatusOfSchedule], [dateOffSlot], [Staus], [SlotDateId], [RoomIdUpDate]) VALUES (469, 53, 1, NULL, CAST(N'2023-12-11' AS Date), 2, 15, 6)
SET IDENTITY_INSERT [dbo].[Schedual] OFF
GO
SET IDENTITY_INSERT [dbo].[SlotDate] ON 

INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (1, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Mon', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (2, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Mon', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (3, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Mon', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (4, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Mon', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (5, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Mon', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (6, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Tue', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (7, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Tue', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (8, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Tue', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (9, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Tue', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (10, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Tue', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (11, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Wed', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (12, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Wed', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (13, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Wed', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (14, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Wed', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (15, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Wed', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (16, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Thu', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (17, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Thu', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (18, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Thu', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (19, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Thu', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (20, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Thu', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (21, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Fri', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (22, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Fri', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (23, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Fri', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (24, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Fri', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (25, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Fri', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (26, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Sat', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (27, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Sat', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (28, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Sat', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (29, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Sat', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (30, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Sat', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (31, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Sun', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (32, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Sun', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (33, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Sun', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (34, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Sun', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (35, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Sun', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (36, CAST(N'07:10:00' AS Time), CAST(N'09:00:00' AS Time), N'Mon', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (37, CAST(N'09:10:00' AS Time), CAST(N'11:00:00' AS Time), N'Mon', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (38, CAST(N'11:10:00' AS Time), CAST(N'13:00:00' AS Time), N'Mon', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (39, CAST(N'13:10:00' AS Time), CAST(N'15:00:00' AS Time), N'Mon', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
INSERT [dbo].[SlotDate] ([Id], [TimeStart], [TimeEnd], [Day], [CenterId]) VALUES (40, CAST(N'15:10:00' AS Time), CAST(N'17:00:00' AS Time), N'Mon', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
SET IDENTITY_INSERT [dbo].[SlotDate] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentClass] ON 

INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (47, 47, N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', NULL, NULL, N'$Học 5 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (48, 47, N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL, NULL, N'$Học 5 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (49, 47, N'2a0761e6-d4c8-46af-b451-21556534da08', NULL, NULL, N'$Học 5 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (50, 48, N'fc7d472a-be81-481b-ae77-408b6bb02ab0', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (51, 48, N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (52, 48, N'9ea611ce-702f-4e12-8861-495b42f70d0f', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (53, 49, N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (54, 49, N'f8a8cbf2-2546-4378-b01a-56272adbc419', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (55, 49, N'29d1a49c-d290-42ce-8336-5a132fc40883', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (56, 50, N'70b47530-8d88-428c-9ae8-67750d95bb66', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (57, 50, N'f6431713-89e4-4436-8f2b-6a72c67f12c8', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (58, 51, N'4936489c-7fbb-499c-8b2f-73b0bbfacb5b', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (59, 51, N'14bedc28-2f96-4687-aa9b-7d93dfc9948e', NULL, NULL, N'$Học 10 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (60, 52, N'46369be6-e8ad-4ff7-a322-8e8384bc1959', NULL, NULL, N'$Học 4 Buổi')
INSERT [dbo].[StudentClass] ([Id], [ClassId], [StudentId], [DateCreate], [DateUpdate], [Note]) VALUES (61, 53, N'8c7e7d71-788a-4181-85db-edc41dbd9459', NULL, NULL, N'$1')
SET IDENTITY_INSERT [dbo].[StudentClass] OFF
GO
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'f02f5f89-68ba-4e58-a94e-08456c772168', N'Bùi', N'Đình', N'Thông', N'Na Mao Huyện Đại Từ', N'ispringer2@e-recht24.de', N'0927.47.8866', NULL, CAST(N'2001-04-04' AS Date), 1, 1, N'292239584574', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Jannelle', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'7ecf8b63-737b-4fa1-bb2c-19346b93c672', N'Nguyễn', N'Văn', N'Thuận', N'Di Trạch Huyện Hoài Đức', N'tsq35064@zbock.com', N'0917.08.8686', NULL, CAST(N'2001-02-05' AS Date), 1, 1, N'871316349389', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Gherardo', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', N'AgkJSSpfcBbY8gqjLoG026/em2uG+EBTX5EGqUOq+1k=', CAST(N'2023-11-17T14:32:52.410' AS DateTime), NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'Nguyễn', N'Mạnh ', N'Đạt', N'Ha Noi', N'uls72513@nezid.com', N'2312321312', NULL, CAST(N'2001-02-06' AS Date), 1, 1, N'213213213123', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'student', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', N'/kkdQo3Oul5Rzuv67HmibVWFvfZZBYp8oSf9VtVDeF0=', CAST(N'2023-12-16T01:10:06.940' AS DateTime), NULL, CAST(N'2023-12-05' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'2a0761e6-d4c8-46af-b451-21556534da08', N'Khuất ', N'Mạnh ', N'Quân', N'Dân Hòa Huyện Thanh Oai', N'vmp17308@omeie.com', N'0941.32.8866', NULL, CAST(N'2001-03-05' AS Date), 1, 1, N'332556024659', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Matthus', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', N'4uZNAAYYCiwHi+swHhTB9LlqNnq5dGr4ACeOx2F5LOQ=', CAST(N'2023-11-16T23:14:32.967' AS DateTime), NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'9ab9f996-a525-42ab-854c-3df16f0d574f', N'Nguyễn', N'Thị', N'Loan', N'Minh Tiến Huyện Đại Từ', N'dkohter0@nba.com', N'0941.94.8866', NULL, CAST(N'2001-02-02' AS Date), 1, 2, N'533544504007', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Miles', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'fc7d472a-be81-481b-ae77-408b6bb02ab0', N'Nguyễn', N'Thái', N'Quân', N'Cự Khê Huyện Thanh Oai', N'hgg28899@omeie.com', N'0983.872.068', NULL, CAST(N'2001-04-05' AS Date), 1, 1, N'525615167558', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'quan', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-11-11' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'fa2b4cd8-3efb-4ec5-b770-48ff9ea2c313', N'Nguyễn', N'Văn', N'Nam', N'Sơn Tây, Hà Nội', N'wmd25257@nezid.com', N'0941.32.6688', NULL, CAST(N'2001-04-04' AS Date), 1, 1, N'230024386854', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Nelli', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'9ea611ce-702f-4e12-8861-495b42f70d0f', N'Khuất', N'Duy', N'Hải', N'Thịnh Thành, Thái Nguyên', N'nuz45649@zslsz.com', N'0563.06.8866', NULL, CAST(N'2001-09-09' AS Date), 1, 1, N'355900880617', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Nelson', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'3e07e5ac-efbb-4491-90b2-49cdb5a7525d', N'qưewqeqweqweqweqweqwe', N'qưewqeqweqweqweqweqwe', N'qưewqeqweqweqweqweqwe', N'qưewqeqweqweqweqweqwe', N'qưewqeqweqweqweqweqwe', N'123123123123', NULL, CAST(N'2022-11-11' AS Date), 1, 1, N'1111111111111111111111111', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', CAST(N'2023-11-22' AS Date), N'string', N'990B6F5F7AF9EC7816DA20E6FB03C0A9EFEE78D0B6AC25C61A4AE02EC607176B', NULL, NULL, NULL, CAST(N'2023-11-22' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'ead74c0e-bbf9-45f4-b4a8-4c7616c869d9', N'Lê', N'Minh', N'Hinh', N'Thạch Thất, Hà Nội', N'bsc17603@nezid.com', N'08.6996.1368', NULL, CAST(N'2001-08-08' AS Date), 1, 1, N'284974075761', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Theodora', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'f8a8cbf2-2546-4378-b01a-56272adbc419', N'Lê', N'Thanh', N'Minh', N'Phúc Thọ, Hà Nội', N'llx02267@omeie.com', N'0834.01.8866', NULL, CAST(N'2000-01-03' AS Date), 1, 1, N'301593892883', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Darn', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'5b13adf1-5ab5-42d0-9d65-59038295cba1', N'Bùi', N'Thanh', N'Lam', N'Minh Lập Huyện Đồng Hỷ', N'iakhurstk@cbsnews.com', N'0971.766.168', NULL, CAST(N'2001-01-01' AS Date), 1, 2, N'304188673173', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Carlye', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'29d1a49c-d290-42ce-8336-5a132fc40883', N'Nguyễn', N'Hoài', N'An', N'An Khánh	Huyện Hoài Đức', N'kka70017@zbock.com', N'0943.70.6868', NULL, CAST(N'2000-01-02' AS Date), 1, 2, N'831191287363', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Rosalinda', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'70b47530-8d88-428c-9ae8-67750d95bb66', N'Nguyễn', N'Thúy', N'An', N'Ba Trại Huyện Ba Vì', N'qqt13550@zslsz.com', N'0963.827.068', NULL, CAST(N'2000-01-04' AS Date), 1, 2, N'650405749456', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Elie', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'f6431713-89e4-4436-8f2b-6a72c67f12c8', N'Nguyễn', N'Gia', N'An', N'Ba Vì Huyện Ba Vì', N'ktd30005@zbock.com', N'0868.79.6886', NULL, CAST(N'2000-01-06' AS Date), 1, 2, N'330971356153', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Chev', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'4936489c-7fbb-499c-8b2f-73b0bbfacb5b', N'Hoàng', N'Trọng', N'Hiếu', N'Bạch Hạ Huyện Phú Xuyên', N'unu51513@zbock.comunu51513@zbock.com', N'0567.35.8866', NULL, CAST(N'2001-07-07' AS Date), 1, 1, N'105121824078', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'hieu', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-11-11' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'd45a5d8e-1e16-43c0-a887-7d68414a41d3', N'Dương', N'Công', N'Thành', N'Nam Hòa Huyện Đồng Hỷ', N'hraff4@liveinternet.ru', N'0981.77.8286', NULL, CAST(N'2001-05-05' AS Date), 1, 1, N'346472474013', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Bellina', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'14bedc28-2f96-4687-aa9b-7d93dfc9948e', N'Nguyễn', N'Hải', N'Băng', N'Cẩm Lĩnh Huyện Ba Vì ', N'email6@example.com', N'085.444.6886', NULL, CAST(N'2000-01-06' AS Date), 1, 2, N'760514245386', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Marc', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'9f22cd2e-3a30-4df5-ba59-819f47e0fe95', N'Nguyễn', N'Thanh', N'Thúy', N'Cẩm Yên Huyện Thạch Thất    ', N'ast30393@zbock.com', N'0989.931.086', NULL, CAST(N'2000-01-05' AS Date), 1, 2, N'366131561731', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Elihu', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'7129b1ee-7179-4674-a19c-81efc11078bc', N'Nguyễn', N'Linh', N'Chi', N'Chàng Sơn Huyện Thạch Thất', N'wuq04831@zslsz.com', N'0583.85.8866', NULL, CAST(N'2000-01-01' AS Date), 1, 2, N'777956994840', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Ansell', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'37b5a824-d3c6-449e-8145-883adae4a967', N'Nguyễn', N'Văn', N'Test', NULL, N'datt19112001@gmail.com', NULL, NULL, NULL, 0, NULL, NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'76e78cbf-aa68-466b-9846-c3242faa00e8', CAST(N'2023-12-06' AS Date), N'testStudent', N'E6032E7F389A8E7F7026082022F5EF1C179942304C2B2C6336422D5FB551E1A4', NULL, NULL, NULL, CAST(N'2023-12-06' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'46369be6-e8ad-4ff7-a322-8e8384bc1959', N'Đoàn', N'Hải', N'Phong', N'Châu Can Huyện Phú Xuyên', N'yju60615@zslsz.com', N'0834.91.8668', NULL, CAST(N'2001-06-06' AS Date), 1, 1, N'776323755232', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'Benji', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', N'wCdrVvIoM0SerTqpTqP9kgDY9Mp+IXj+ezXqjIGROs8=', CAST(N'2023-12-16T01:12:36.127' AS DateTime), NULL, CAST(N'2023-11-11' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'5a9fa6a9-1209-4ab5-8281-8ee75511b822', N'Đinh', N'Bảo', N'Thiện', N'Nga My Huyện Phú Bình', N'garonovich5@mtv.com', N'0917.50.8866', NULL, CAST(N'2001-06-06' AS Date), 1, 1, N'435232909741', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Aldous', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'696695d7-d74b-4cef-9c7a-97b017fa5ef4', N'Nguyễn', N'Khắc', N'Thành', N'Mỹ Yên Huyện Đại Từ', N'etwyning1@vkontakte.ru', N'0918.65.8866', NULL, CAST(N'2001-03-03' AS Date), 1, 1, N'571198225083', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Cheston', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'07be1092-3992-4bfb-8481-9c67ea87cf56', N'Nguyễn', N'Thiên', N'Ân', N'Châu Sơn Huyện Ba Vì', N'eol16601@nezid.com', N'0916.41.8866', NULL, CAST(N'2000-01-04' AS Date), 1, 2, N'984178505561', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Mel', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'b5b9364a-e3cf-4b1f-be25-a4b6a0a87678', N'Lê', N'Trà', N'My', N'Nhã Lộng Huyện Phú Bình', N'jlindenblatt7@over-blog.com', N'0947.51.6688', NULL, CAST(N'2001-08-08' AS Date), 1, 2, N'575161023787', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Horton', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'4a8d4061-b35c-46c7-b654-a54fdf0017a9', N'Đỗ', N'Quang', N'Huy', N'Chu Phan Huyện Mê Linh', N'yjq85698@nezid.com', N'0889.61.6868', NULL, CAST(N'2001-05-05' AS Date), 1, 1, N'225799368302', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'huy', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-11-11' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'0e1739cc-5c84-4e53-ae5e-b98b46a1dfd5', N'Đào', N'Trung', N'Dũng', N'Chu Minh Huyện Ba Vì', N'jhx06591@nezid.com', N'091.793.6688', NULL, CAST(N'2000-01-06' AS Date), 1, 1, N'617169519191', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Prentiss', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'10d381dd-5403-4133-8527-bbe6c40e9710', N'Phan', N'Hải', N'Đăng', N'Chuyên Mỹ Huyện Phú Xuyên', N'email5@example.com', N'0942.40.8866', NULL, CAST(N'2000-01-05' AS Date), 1, 1, N'474431105558', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Ardelia', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'32cc0150-3701-41d2-a611-cdebd27d15be', N'Phạm', N'Thành', N'Đạt', N'Cổ Bi Huyện Gia Lâm', N'email30@example.com', N'0969.00.1686', NULL, CAST(N'2000-01-30' AS Date), 1, 1, N'535386092436', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Averill', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'051e590c-38ff-4301-bf9a-d5faa4015d15', N'Đào', N'Thanh', N'Tuyết', N'Phấn Mễ Huyện Phú Lương', N'lmonan9@army.mil', N'0562.50.8866', NULL, CAST(N'2001-10-10' AS Date), 1, 2, N'475568461084', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Rufe', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'2b884694-13e8-4519-b80e-dcc99e8cb21d', N'Nguyễn', N'Diệu', N'Ngân', N'Ôn Lương Huyện Phú Lương', N'ugillum8@sbwire.com', N'0969.582.086', NULL, CAST(N'2001-09-09' AS Date), 1, 2, N'253546250948', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Rafaelita', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'95d0df9f-af9b-4cab-aa26-de0890e3e15e', N'Khuất', N'Gia', N'Bảo', N'Cổ Đô Huyện Ba Vì', N'email6@example.com', N'0965.88.1368', NULL, CAST(N'2000-01-06' AS Date), 1, 1, N'900457317138', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Raff', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'6137e4f4-edde-4177-98de-e0676fb9c286', N'Hoangs', N'Hai', N'Nam', N'a', N'a', N'0123123', NULL, CAST(N'2023-11-23' AS Date), 1, 1, N'0123123123', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-11-11' AS Date), N'Joice', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-11-23' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'18bbd2c6-4771-4921-9fbd-e1a8a289a502', N'Nguyễn', N'Anh', N'Thư', N'Cổ Loa Huyện Đông Anh', N'email4@example.com', N'0563.23.8866', NULL, CAST(N'2000-01-04' AS Date), 1, 2, N'953379947144', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Yvette', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'8c7e7d71-788a-4181-85db-edc41dbd9459', N'Lê', N'Thị Đoan', N'Trang', N'Cộng Hòa Huyện Quốc Oai', N'ganafor467@glalen.com', N'0963.37.8668', NULL, CAST(N'2000-01-05' AS Date), 1, 2, N'597547835524', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'5861360d-5e71-46f9-bee3-e42ebb8c27ff', CAST(N'2023-10-10' AS Date), N'Ivy', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', N'VLVUjBDbLsdVLS3GbyU/T3pd7ScYwqTZjuaRJOCSeNw=', CAST(N'2023-12-16T22:17:10.617' AS DateTime), NULL, CAST(N'2023-10-10' AS Date))
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Image], [Dob], [Status], [Gender], [Cccd], [CenterId], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [Verfify], [DateUpdate]) VALUES (N'4ddccbf2-39d7-444d-9b63-f1bdcfe2317f', N'Nguyễn', N'Huy', N'Hùng', N'Nghinh Tường Huyện Võ Nhai', N'fcoultish6@devhub.com', N'08.6996.6886', NULL, CAST(N'2001-07-07' AS Date), 2, 1, N'455819325153', N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'0b4fe348-dbc4-411e-bee3-0d252e72c43a', CAST(N'2023-10-10' AS Date), N'Sampson', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', NULL, NULL, NULL, CAST(N'2023-11-23' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Syllabus] ON 

INSERT [dbo].[Syllabus] ([Id], [SyllabusName], [Description], [TimeAllocation], [StudentTasks], [CourseObjectives], [Exam], [SubjectCode], [Contact], [CourseLearningOutcome], [MainTopics], [RequiredKnowledge], [LinkBook], [LinkDocument], [Status], [CenterId]) VALUES (2, N'English 1 (University success)_Tiếng Anh 1', N'This course is designed to further develop the learner''s confidence and fluency in using the English language and to enrich their learning experience through interactive lessons and communicative exercises.

_Upon completion of the course, students should be able to:
_- Skim for gist & scan for details
_- Identify sentences functions, Identify topics & main ideas, Identify supporting details
_- Recognize patterns of cohesion: cause/effect, compare/contrast, problem/solution, Understand cohesion in description.
_- Apply different methods to Increase fluency, and tolerance ambiguity in reading
_- Understand text references to visuals, Interprete the information is visuals
_- Recognize and interprete statements of opinion, and fact
_- Make strong inferences and avoid weak ones, Distinguish between deliberate implications and direct statement.
_- Identify and evaluate evidence, recognize and deal with faulty rhetoric
_- Understand multiple perspectives, Evaluate the credibility and motives of sources
_- Recognize and understand definitions within a text, Work with classifications

_Students are also expected to:
_- Be cooperative, supportive, and responsible in group work. Be able to desomontrate lateral and critical thinking. Be professsional in demonstrating academic presentation and writing.
_- Take advantages of online applications, language labs, and online platforms in performing learning tasks_
_Be ethic and adhere to plagiarism rules in performing learning tasks. Be an active reader in acquiring and selecting knowledge', N'Study hour (300h) = 105 contact hours (70 slots) + 2 hours final exam + 193 hours self-study
Interactive Classroom Teaching
1 slot =90''', N'_ Attend more than 80% of contact hours in order to be accepted to the final examination
_ Actively participate in class activities
_ Fulfil tasks given by the instructor after class
_ Use their own laptop in class only for learning purposes
_ Use the Student Access Code provided with the textbook
_ Access the course website for up-to-date information and material of the course, for online support from teachers and other students, and for practicing and assessment.', N'Understand multiple perspectives, Evaluate the credibility and motives of sources', N'1) On-going assessment
_- Progress test: 02*20% : 40%
_- Presentation: : 20 %
_2) Final Examination (FE) 40%
_3) Final Result 100%
_4) Completion Criteria:
_a) On-going assessment >0, Final Exam Score >=4/10 & Final Result >=5/10
_b) Each part of final exam must be >0.
_- If FE < 4/10, student must take resit exam (all parts);
_- If have a) but not b), student must take resit exam for the failed parts only', N'TRS601', N'Lawrence Zwier and Maggie Vosters', N'Contents Covered in the Course Book', N'Diagnostic Test (Reading & Writing Skills)', N'Passed ENT403 or Level 5 LUK or TRS501', N'Link sách 2', N'Link tài liệu 2', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[Syllabus] ([Id], [SyllabusName], [Description], [TimeAllocation], [StudentTasks], [CourseObjectives], [Exam], [SubjectCode], [Contact], [CourseLearningOutcome], [MainTopics], [RequiredKnowledge], [LinkBook], [LinkDocument], [Status], [CenterId]) VALUES (3, N'English 2 (University success)_Tiếng Anh 2', N'This course is designed to further develop the learner''s confidence and fluency in using the English language and to enrich their learning experience through interactive lessons and communicative exercises.

_Upon completion of the course, students should be able to:
_- Skim for gist & scan for details
_- Identify sentences functions, Identify topics & main ideas, Identify supporting details
_- Recognize patterns of cohesion: cause/effect, compare/contrast, problem/solution, Understand cohesion in description.
_- Apply different methods to Increase fluency, and tolerance ambiguity in reading
_- Understand text references to visuals, Interprete the information is visuals
_- Recognize and interprete statements of opinion, and fact
_- Make strong inferences and avoid weak ones, Distinguish between deliberate implications and direct statement.
_- Identify and evaluate evidence, recognize and deal with faulty rhetoric
_- Understand multiple perspectives, Evaluate the credibility and motives of sources
_- Recognize and understand definitions within a text, Work with classifications

_Students are also expected to:
_- Be cooperative, supportive, and responsible in group work. Be able to desomontrate lateral and critical thinking. Be professsional in demonstrating academic presentation and writing.
_- Take advantages of online applications, language labs, and online platforms in performing learning tasks_
_Be ethic and adhere to plagiarism rules in performing learning tasks. Be an active reader in acquiring and selecting knowledge', N'Study hour (300h) = 105 contact hours (70 slots) + 2 hours final exam + 193 hours self-study
Interactive Classroom Teaching
1 slot =90''', N'_ Attend more than 80% of contact hours in order to be accepted to the final examination
_ Actively participate in class activities
_ Fulfil tasks given by the instructor after class
_ Use their own laptop in class only for learning purposes
_ Use the Student Access Code provided with the textbook
_ Access the course website for up-to-date information and material of the course, for online support from teachers and other students, and for practicing and assessment.', N'Understand multiple perspectives, Evaluate the credibility and motives of sources', N'1) On-going assessment
_- Progress test: 02*20% : 40%
_- Presentation: : 20 %
_2) Final Examination (FE) 40%
_3) Final Result 100%
_4) Completion Criteria:
_a) On-going assessment >0, Final Exam Score >=4/10 & Final Result >=5/10
_b) Each part of final exam must be >0.
_- If FE < 4/10, student must take resit exam (all parts);
_- If have a) but not b), student must take resit exam for the failed parts only', N'TRS602', N'Lawrence Zwier and Maggie Vosters', N'Contents Covered in the Course Book', N'Diagnostic Test (Reading & Writing Skills)', N'Passed ENT403 or Level 5 LUK or TRS501', N'Link sách 3', N'Link tài liệu 3', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[Syllabus] ([Id], [SyllabusName], [Description], [TimeAllocation], [StudentTasks], [CourseObjectives], [Exam], [SubjectCode], [Contact], [CourseLearningOutcome], [MainTopics], [RequiredKnowledge], [LinkBook], [LinkDocument], [Status], [CenterId]) VALUES (4, N'English 3 (University success)_Tiếng Anh 3', N'This course is designed to further develop the learner''s confidence and fluency in using the English language and to enrich their learning experience through interactive lessons and communicative exercises.

_Upon completion of the course, students should be able to:
_- Skim for gist & scan for details
_- Identify sentences functions, Identify topics & main ideas, Identify supporting details
_- Recognize patterns of cohesion: cause/effect, compare/contrast, problem/solution, Understand cohesion in description.
_- Apply different methods to Increase fluency, and tolerance ambiguity in reading
_- Understand text references to visuals, Interprete the information is visuals
_- Recognize and interprete statements of opinion, and fact
_- Make strong inferences and avoid weak ones, Distinguish between deliberate implications and direct statement.
_- Identify and evaluate evidence, recognize and deal with faulty rhetoric
_- Understand multiple perspectives, Evaluate the credibility and motives of sources
_- Recognize and understand definitions within a text, Work with classifications

_Students are also expected to:
_- Be cooperative, supportive, and responsible in group work. Be able to desomontrate lateral and critical thinking. Be professsional in demonstrating academic presentation and writing.
_- Take advantages of online applications, language labs, and online platforms in performing learning tasks_
_Be ethic and adhere to plagiarism rules in performing learning tasks. Be an active reader in acquiring and selecting knowledge', N'Study hour (300h) = 105 contact hours (70 slots) + 2 hours final exam + 193 hours self-study
Interactive Classroom Teaching
1 slot =90''', N'_ Attend more than 80% of contact hours in order to be accepted to the final examination
_ Actively participate in class activities
_ Fulfil tasks given by the instructor after class
_ Use their own laptop in class only for learning purposes
_ Use the Student Access Code provided with the textbook
_ Access the course website for up-to-date information and material of the course, for online support from teachers and other students, and for practicing and assessment.', N'Understand multiple perspectives, Evaluate the credibility and motives of sources', N'1) On-going assessment
_- Progress test: 02*20% : 40%
_- Presentation: : 20 %
_2) Final Examination (FE) 40%
_3) Final Result 100%
_4) Completion Criteria:
_a) On-going assessment >0, Final Exam Score >=4/10 & Final Result >=5/10
_b) Each part of final exam must be >0.
_- If FE < 4/10, student must take resit exam (all parts);
_- If have a) but not b), student must take resit exam for the failed parts only', N'TRS603', N'Lawrence Zwier and Maggie VostersLawrence Zwier and Maggie Vosters', N'Contents Covered in the Course Book', N'Diagnostic Test (Reading & Writing Skills)', N'Passed ENT403 or Level 5 LUK or TRS501', N'Link sách 4', N'Link tài liệu 4', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[Syllabus] ([Id], [SyllabusName], [Description], [TimeAllocation], [StudentTasks], [CourseObjectives], [Exam], [SubjectCode], [Contact], [CourseLearningOutcome], [MainTopics], [RequiredKnowledge], [LinkBook], [LinkDocument], [Status], [CenterId]) VALUES (5, N'English 4 (University success)_Tiếng Anh 4', N'This course is designed to further develop the learner''s confidence and fluency in using the English language and to enrich their learning experience through interactive lessons and communicative exercises.

_Upon completion of the course, students should be able to:
_- Skim for gist & scan for details
_- Identify sentences functions, Identify topics & main ideas, Identify supporting details
_- Recognize patterns of cohesion: cause/effect, compare/contrast, problem/solution, Understand cohesion in description.
_- Apply different methods to Increase fluency, and tolerance ambiguity in reading
_- Understand text references to visuals, Interprete the information is visuals
_- Recognize and interprete statements of opinion, and fact
_- Make strong inferences and avoid weak ones, Distinguish between deliberate implications and direct statement.
_- Identify and evaluate evidence, recognize and deal with faulty rhetoric
_- Understand multiple perspectives, Evaluate the credibility and motives of sources
_- Recognize and understand definitions within a text, Work with classifications

_Students are also expected to:
_- Be cooperative, supportive, and responsible in group work. Be able to desomontrate lateral and critical thinking. Be professsional in demonstrating academic presentation and writing.
_- Take advantages of online applications, language labs, and online platforms in performing learning tasks_
_Be ethic and adhere to plagiarism rules in performing learning tasks. Be an active reader in acquiring and selecting knowledge', N'Study hour (300h) = 105 contact hours (70 slots) + 2 hours final exam + 193 hours self-study
Interactive Classroom Teaching
1 slot =90''', N'_ Attend more than 80% of contact hours in order to be accepted to the final examination
_ Actively participate in class activities
_ Fulfil tasks given by the instructor after class
_ Use their own laptop in class only for learning purposes
_ Use the Student Access Code provided with the textbook
_ Access the course website for up-to-date information and material of the course, for online support from teachers and other students, and for practicing and assessment.', N'Understand multiple perspectives, Evaluate the credibility and motives of sources', N'1) On-going assessment
_- Progress test: 02*20% : 40%
_- Presentation: : 20 %
_2) Final Examination (FE) 40%
_3) Final Result 100%
_4) Completion Criteria:
_a) On-going assessment >0, Final Exam Score >=4/10 & Final Result >=5/10
_b) Each part of final exam must be >0.
_- If FE < 4/10, student must take resit exam (all parts);
_- If have a) but not b), student must take resit exam for the failed parts only', N'TRS604', N'Lawrence Zwier and Maggie Vosters', N'Contents Covered in the Course Book', N'Diagnostic Test (Reading & Writing Skills)', N'Passed ENT403 or Level 5 LUK or TRS501', N'Link sách 5', N'Link tài liệu 5', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
INSERT [dbo].[Syllabus] ([Id], [SyllabusName], [Description], [TimeAllocation], [StudentTasks], [CourseObjectives], [Exam], [SubjectCode], [Contact], [CourseLearningOutcome], [MainTopics], [RequiredKnowledge], [LinkBook], [LinkDocument], [Status], [CenterId]) VALUES (6, N'English 5 (University success)_Tiếng Anh 5', N'This course is designed to further develop the learner''s confidence and fluency in using the English language and to enrich their learning experience through interactive lessons and communicative exercises.

_Upon completion of the course, students should be able to:
_- Skim for gist & scan for details
_- Identify sentences functions, Identify topics & main ideas, Identify supporting details
_- Recognize patterns of cohesion: cause/effect, compare/contrast, problem/solution, Understand cohesion in description.
_- Apply different methods to Increase fluency, and tolerance ambiguity in reading
_- Understand text references to visuals, Interprete the information is visuals
_- Recognize and interprete statements of opinion, and fact
_- Make strong inferences and avoid weak ones, Distinguish between deliberate implications and direct statement.
_- Identify and evaluate evidence, recognize and deal with faulty rhetoric
_- Understand multiple perspectives, Evaluate the credibility and motives of sources
_- Recognize and understand definitions within a text, Work with classifications

_Students are also expected to:
_- Be cooperative, supportive, and responsible in group work. Be able to desomontrate lateral and critical thinking. Be professsional in demonstrating academic presentation and writing.
_- Take advantages of online applications, language labs, and online platforms in performing learning tasks_
_Be ethic and adhere to plagiarism rules in performing learning tasks. Be an active reader in acquiring and selecting knowledge', N'Study hour (300h) = 105 contact hours (70 slots) + 2 hours final exam + 193 hours self-study
Interactive Classroom Teaching
1 slot =90''', N'_ Attend more than 80% of contact hours in order to be accepted to the final examination
_ Actively participate in class activities
_ Fulfil tasks given by the instructor after class
_ Use their own laptop in class only for learning purposes
_ Use the Student Access Code provided with the textbook
_ Access the course website for up-to-date information and material of the course, for online support from teachers and other students, and for practicing and assessment.', N'Understand multiple perspectives, Evaluate the credibility and motives of sources', N'1) On-going assessment
_- Progress test: 02*20% : 40%
_- Presentation: : 20 %
_2) Final Examination (FE) 40%
_3) Final Result 100%
_4) Completion Criteria:
_a) On-going assessment >0, Final Exam Score >=4/10 & Final Result >=5/10
_b) Each part of final exam must be >0.
_- If FE < 4/10, student must take resit exam (all parts);
_- If have a) but not b), student must take resit exam for the failed parts only', N'TRS605', N'Lawrence Zwier and Maggie Vosters', N'Contents Covered in the Course Book', N'Diagnostic Test (Reading & Writing Skills)', N'Passed ENT403 or Level 5 LUK or TRS501', N'Link sách 6', N'Link tài liệu 6', 1, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823')
SET IDENTITY_INSERT [dbo].[Syllabus] OFF
GO
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'8b2ba772-5e2c-4395-8353-0dd814cc8750', N'Nguyễns', N'Thái', N'Quân', N'Duyên Hà Huyện Thanh Trì', N'jmeadenf@irs.gov', N'1232312321', CAST(N'1989-01-01' AS Date), N'background234007267.jpg', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'IELTS 8.0 certificate', N'5 years of experience teaching IELTS.', N'It has been ages since the day I decided to pursue a career in education, I suppose it was about ten years ago. Back then, I was so eager to be a person who could help inspire and motivate the young learners as I understood the difficulties that they might face when they wanted to learn a language from my experiences at schools. Especially when English is so prevalent among the modern citizens, students really need some mentors giving help and advice, or maybe just simply giving them a new light. These all drove me to become a teacher and I can say I have never regretted my decision.', CAST(N'2020-01-01' AS Date), N'wallpaperf234007273.jpg', CAST(N'2023-10-10' AS Date), N'teacher1', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-12-05' AS Date), N'zjGiZ3ZSuLUFmwDjhsuJDvtiYNtU1veLLxLllbwhi98=', NULL, CAST(N'2023-12-18T00:52:07.560' AS DateTime), 1)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'2ca5905c-87e8-4a30-93da-179c710a142e', N'Test', N'Test', N'Test', N'Test', N'Test', N'00000000000', CAST(N'2000-01-01' AS Date), NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'Test', N'Test', N'Test', CAST(N'2000-01-01' AS Date), NULL, CAST(N'2023-10-10' AS Date), N'teachertestCenter2', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-10' AS Date), NULL, NULL, NULL, 1)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'Nguyễn', N'Thành', N'Ý', N'Dũng Tiến Huyện Thường Tín', N'dlivoire@networkadvertising.org', N'0941.60.8866', CAST(N'1989-02-02' AS Date), N'Email-mark232538407.png', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, NULL, NULL, NULL, NULL, CAST(N'2023-10-10' AS Date), N'teacher', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-12-05' AS Date), N'avhoIU2WfnG9sZF6WjmcDRaIworEfR3WdrLizvVHBdw=', NULL, CAST(N'2023-12-18T00:08:49.513' AS DateTime), 1)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'3d1a2bc0-e7be-4059-b8d7-2094322bea38', N'Nguyễn', N'Văn', N'Nam', N'Minh Lập Huyện Đồng Hỷ', N'jasseld@clickbank.net', N'0947.04.8866', CAST(N'1989-03-03' AS Date), NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'IELTS certificate 8.0, 9.0 Speaking', N'Bachelor graduated with honors from RMIT University', N'Passionate about design, production, communication, and events, but fate brought Mr. Jim to another profession: teaching. Having spent time standing on the podium since 2018 or via Zoom in 2020, Mr. Jim has always thought: "The world is always changing, we also have to change to bring better lessons, along with More students are confident in conquering IELTS." That''s why over the past 3 years, he has always put his best effort into every class, participating in classroom experience initiatives, contributing to creating interesting teaching hours with his team of IELTS Fighter teachers.', CAST(N'2020-03-03' AS Date), NULL, CAST(N'2023-10-10' AS Date), N'teacherCenter2', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-10' AS Date), N'DU3tisfmEvXGR4QSNlAGK04M7q6CWpsWAxXGxAXL724=', NULL, CAST(N'2023-12-15T15:01:38.130' AS DateTime), 1)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'a3d39d79-f67a-498a-8021-2b125de2e5bc', N'testthem', N'testthem', N'testthem', NULL, N'datt19112001@gmail.com', NULL, CAST(N'2000-02-02' AS Date), N'DefaultAvatar.png', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, NULL, NULL, NULL, N'DefaultCetificate.png', CAST(N'2023-12-09' AS Date), N'testthem', N'C67E9C4A964FF7CDFD8FE6DB431B9AC00216016D79090AFD8D311C272C27F652', CAST(N'2023-12-09' AS Date), N'Q3bdmOb8BuZvLLYUlEYNwTCQzHq2zyqEShsgLeADUZM=', NULL, CAST(N'2023-12-16T17:41:58.220' AS DateTime), 2)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'2d11b40c-31e6-447f-a2da-633d8e999fca', N'Nguyên', N'Thủy', N'Tiên', N'Minh Tiến Huyện Đại Từ', N'fteligac@guardian.co.uk', N'0968.320.168', CAST(N'1989-04-04' AS Date), NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'IELTS 8.5 certificate', N'Graduated from Gustavus Adolphus College', N'NUPassionate about English since childhood, Mr. Van Anh chose to study English at Polytechnic University and won a scholarship to study abroad in the UK to further improve his qualifications. And he got 8.0 IELTS in just the first test. In addition, he also excellently won many awards such as first prize in the competition to learn about the culture of English-speaking countries organized by the Institute of Foreign Languages, Hanoi University of Science and Technology and the Australian Embassy in 2016. Second prize in the competition. Searching for Polytechnic talent 2015. Therefore, Mr. Van Anh always has useful shares for students in studying abroad and changing themselves, participating in extracurricular activities, and developing soft skills outside of school. edge strengthens the English foundation.LL', CAST(N'2020-04-04' AS Date), NULL, CAST(N'2023-10-10' AS Date), N'teacher1Center2', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-10-10' AS Date), NULL, NULL, NULL, 1)
INSERT [dbo].[TeacherProfile] ([Id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Image], [CenterId], [CertificateName], [Title], [Description], [Release date], [ImageCertificateName], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [Verfify], [refreshTokenExpired], [Status]) VALUES (N'8827321b-1289-41bd-9c69-ebe1ef86bd49', N'as', N'as', N'as', N'as', N'as', N'213213', CAST(N'2022-11-11' AS Date), N'thoi-khoa-235516612.png', N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', N'a', N'a', N'a', CAST(N'2022-11-11' AS Date), N'nhat_cay-1235516616.jpg', CAST(N'2023-10-10' AS Date), N'teachertest', N'1395507E447359E5FD6389C5BCBCD8EE8E2AAC5654803C96CE14FBF62DA413C7', CAST(N'2023-11-23' AS Date), N'DuFQHwf36dLxqRySIG/fDXaEAsa9ay1/sEuH6P1xTf8=', NULL, CAST(N'2023-11-22T15:53:25.097' AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[TeacherTeachingHours] ON 

INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (4, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 461, 110, N'Add through attendance')
INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (5, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 462, 110, N'Add through attendance')
INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (6, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 463, 110, N'Add through attendance')
INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (7, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 464, 110, N'Add through attendance')
INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (10, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 469, 110, N'Add through attendance ')
INSERT [dbo].[TeacherTeachingHours] ([Id], [TeacherId], [ScheduleId], [TimeTeaching], [Note]) VALUES (11, N'8b2ba772-5e2c-4395-8353-0dd814cc8750', 368, 110, N'Add through attendance ')
SET IDENTITY_INSERT [dbo].[TeacherTeachingHours] OFF
GO
INSERT [dbo].[TypeCourse] ([Id], [Name], [Description], [CourseId], [CreateDate], [UpdateDate]) VALUES (1, N'Tiếng Anh', NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeCourse] ([Id], [Name], [Description], [CourseId], [CreateDate], [UpdateDate]) VALUES (2, N'Toán', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TypeRoom] ON 

INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (1, N'Alpha', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (2, N'Beta', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (3, N'Grama', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (4, N'Delta', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (5, N'Epsilon', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (6, N'Zeta', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (7, N'Eta', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (8, N'Theta', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[TypeRoom] ([Id], [Name], [status], [created_date], [update_date], [created_by], [update_by]) VALUES (16, N'Phong hoc 1', 1, CAST(N'2023-12-04' AS Date), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TypeRoom] OFF
GO
ALTER TABLE [dbo].[Atendence]  WITH CHECK ADD  CONSTRAINT [FK_Atendence_SlotClass] FOREIGN KEY([SchedualId])
REFERENCES [dbo].[Schedual] ([Id])
GO
ALTER TABLE [dbo].[Atendence] CHECK CONSTRAINT [FK_Atendence_SlotClass]
GO
ALTER TABLE [dbo].[Atendence]  WITH CHECK ADD  CONSTRAINT [FK_Atendence_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[Atendence] CHECK CONSTRAINT [FK_Atendence_StudentProfile]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_BLOG_ID] FOREIGN KEY([cate_id])
REFERENCES [dbo].[CateBlog] ([Id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_BLOG_ID]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Center_ID] FOREIGN KEY([center_id])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Center_ID]
GO
ALTER TABLE [dbo].[Center]  WITH CHECK ADD  CONSTRAINT [FK_Center_Account] FOREIGN KEY([AdminId])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[Center] CHECK CONSTRAINT [FK_Center_Account]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Center]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Course1] FOREIGN KEY([CourceId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Course1]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Employee] FOREIGN KEY([AdminCenterId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Employee]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_TeacherProfile1] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[TeacherProfile] ([Id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_TeacherProfile1]
GO
ALTER TABLE [dbo].[ClassRoom]  WITH CHECK ADD  CONSTRAINT [FK_ClassRoom_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[ClassRoom] CHECK CONSTRAINT [FK_ClassRoom_Center]
GO
ALTER TABLE [dbo].[ClassRoom]  WITH CHECK ADD  CONSTRAINT [FK_ClassRoom_TypeRoom] FOREIGN KEY([TypeId])
REFERENCES [dbo].[TypeRoom] ([Id])
GO
ALTER TABLE [dbo].[ClassRoom] CHECK CONSTRAINT [FK_ClassRoom_TypeRoom]
GO
ALTER TABLE [dbo].[ClassSlotDate]  WITH CHECK ADD  CONSTRAINT [FK_ClassSlotDate_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[ClassSlotDate] CHECK CONSTRAINT [FK_ClassSlotDate_Class]
GO
ALTER TABLE [dbo].[ClassSlotDate]  WITH CHECK ADD  CONSTRAINT [FK_ClassSlotDate_ClassRoom] FOREIGN KEY([RoomId])
REFERENCES [dbo].[ClassRoom] ([Id])
GO
ALTER TABLE [dbo].[ClassSlotDate] CHECK CONSTRAINT [FK_ClassSlotDate_ClassRoom]
GO
ALTER TABLE [dbo].[ClassSlotDate]  WITH CHECK ADD  CONSTRAINT [FK_ClassSlotDate_Slot] FOREIGN KEY([SlotId])
REFERENCES [dbo].[SlotDate] ([Id])
GO
ALTER TABLE [dbo].[ClassSlotDate] CHECK CONSTRAINT [FK_ClassSlotDate_Slot]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Center]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Center1] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Center1]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Employee]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Syllabus] FOREIGN KEY([SyllabusId])
REFERENCES [dbo].[Syllabus] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Syllabus]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Account]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Center]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Role]
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD  CONSTRAINT [FK_Exam_Course] FOREIGN KEY([CourceId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Exam] CHECK CONSTRAINT [FK_Exam_Course]
GO
ALTER TABLE [dbo].[ExamClass]  WITH CHECK ADD  CONSTRAINT [FK_ExamClass_Exam1] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([id])
GO
ALTER TABLE [dbo].[ExamClass] CHECK CONSTRAINT [FK_ExamClass_Exam1]
GO
ALTER TABLE [dbo].[ExamClass]  WITH CHECK ADD  CONSTRAINT [FK_ExamClass_StudentClass] FOREIGN KEY([student_class_id])
REFERENCES [dbo].[StudentClass] ([Id])
GO
ALTER TABLE [dbo].[ExamClass] CHECK CONSTRAINT [FK_ExamClass_StudentClass]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_Employee]
GO
ALTER TABLE [dbo].[NotificationClases]  WITH CHECK ADD  CONSTRAINT [FK_NotificationClases_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[NotificationClases] CHECK CONSTRAINT [FK_NotificationClases_Class]
GO
ALTER TABLE [dbo].[SAQ]  WITH CHECK ADD  CONSTRAINT [FK_SAQ_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[SAQ] CHECK CONSTRAINT [FK_SAQ_Employee]
GO
ALTER TABLE [dbo].[Schedual]  WITH CHECK ADD  CONSTRAINT [FK_Schedual_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[Schedual] CHECK CONSTRAINT [FK_Schedual_Class]
GO
ALTER TABLE [dbo].[Schedual]  WITH CHECK ADD  CONSTRAINT [FK_Schedual_ClassRoom] FOREIGN KEY([RoomIdUpDate])
REFERENCES [dbo].[ClassRoom] ([Id])
GO
ALTER TABLE [dbo].[Schedual] CHECK CONSTRAINT [FK_Schedual_ClassRoom]
GO
ALTER TABLE [dbo].[Schedual]  WITH CHECK ADD  CONSTRAINT [FK_SlotClass_SlotDate] FOREIGN KEY([SlotDateId])
REFERENCES [dbo].[SlotDate] ([Id])
GO
ALTER TABLE [dbo].[Schedual] CHECK CONSTRAINT [FK_SlotClass_SlotDate]
GO
ALTER TABLE [dbo].[SlotDate]  WITH CHECK ADD  CONSTRAINT [FK_SlotDate_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[SlotDate] CHECK CONSTRAINT [FK_SlotDate_Center]
GO
ALTER TABLE [dbo].[StudentClass]  WITH CHECK ADD  CONSTRAINT [FK_StudentClass_Class1] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[StudentClass] CHECK CONSTRAINT [FK_StudentClass_Class1]
GO
ALTER TABLE [dbo].[StudentClass]  WITH CHECK ADD  CONSTRAINT [FK_StudentClass_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[StudentClass] CHECK CONSTRAINT [FK_StudentClass_StudentProfile]
GO
ALTER TABLE [dbo].[StudentProfile]  WITH CHECK ADD  CONSTRAINT [FK_StudentProfile_Employee] FOREIGN KEY([EmoloyeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[StudentProfile] CHECK CONSTRAINT [FK_StudentProfile_Employee]
GO
ALTER TABLE [dbo].[Syllabus]  WITH CHECK ADD  CONSTRAINT [FK_Syllabus_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Syllabus] CHECK CONSTRAINT [FK_Syllabus_Center]
GO
ALTER TABLE [dbo].[TeacherProfile]  WITH CHECK ADD  CONSTRAINT [FK_TeacherProfile_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[TeacherProfile] CHECK CONSTRAINT [FK_TeacherProfile_Center]
GO
ALTER TABLE [dbo].[TeacherTeachingHours]  WITH CHECK ADD  CONSTRAINT [FK_TeacherTeachingHours_Schedual] FOREIGN KEY([ScheduleId])
REFERENCES [dbo].[Schedual] ([Id])
GO
ALTER TABLE [dbo].[TeacherTeachingHours] CHECK CONSTRAINT [FK_TeacherTeachingHours_Schedual]
GO
ALTER TABLE [dbo].[TeacherTeachingHours]  WITH CHECK ADD  CONSTRAINT [FK_TeacherTeachingHours_TeacherProfile] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[TeacherProfile] ([Id])
GO
ALTER TABLE [dbo].[TeacherTeachingHours] CHECK CONSTRAINT [FK_TeacherTeachingHours_TeacherProfile]
GO
ALTER TABLE [dbo].[TypeCourse]  WITH CHECK ADD  CONSTRAINT [FK_TypeCourse_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[TypeCourse] CHECK CONSTRAINT [FK_TypeCourse_Course]
GO
USE [master]
GO
ALTER DATABASE [sep490_g22] SET  READ_WRITE 
GO
