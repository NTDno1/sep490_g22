USE [master]
GO
/****** Object:  Database [sep490_g22]    Script Date: 12 1:31:33 PM ******/
CREATE DATABASE [sep490_g22]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sep490_g22', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\sep490_g22.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'sep490_g22_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\sep490_g22_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [sep490_g22] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sep490_g22].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sep490_g22] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [sep490_g22] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [sep490_g22] SET ARITHABORT OFF 
GO
ALTER DATABASE [sep490_g22] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [sep490_g22] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [sep490_g22] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [sep490_g22] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [sep490_g22] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [sep490_g22] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [sep490_g22] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [sep490_g22] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [sep490_g22] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [sep490_g22] SET  ENABLE_BROKER 
GO
ALTER DATABASE [sep490_g22] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [sep490_g22] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [sep490_g22] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [sep490_g22] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [sep490_g22] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [sep490_g22] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [sep490_g22] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [sep490_g22] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [sep490_g22] SET  MULTI_USER 
GO
ALTER DATABASE [sep490_g22] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [sep490_g22] SET DB_CHAINING OFF 
GO
ALTER DATABASE [sep490_g22] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [sep490_g22] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [sep490_g22] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [sep490_g22] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'sep490_g22', N'ON'
GO
ALTER DATABASE [sep490_g22] SET QUERY_STORE = OFF
GO
USE [sep490_g22]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[id] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[RoleId] [int] NOT NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[refreshToken] [text] NULL,
	[refreshTokenExpired] [datetime] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[student_in_class] [int] NULL,
	[schedule_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[StudentId] [uniqueidentifier] NOT NULL,
	[BillId] [int] NOT NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC,
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillDetail]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[id] [int] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[TotalPrice] [money] NULL,
	[Userid] [int] NULL,
	[CourceId] [int] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[ClassId] [int] NULL,
 CONSTRAINT [PK_BillDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[IsComment] [int] NULL,
	[ViewCount] [float] NULL,
	[Status] [int] NULL,
	[UserId] [int] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Center]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Center](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[Adress] [nvarchar](500) NULL,
	[CodeCenter] [nvarchar](500) NULL,
	[AdminId] [uniqueidentifier] NULL,
	[NumberPhone] [nvarchar](500) NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL,
 CONSTRAINT [PK_Center_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chat]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chat](
	[id] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Type] [nvarchar](500) NULL,
	[Data] [nvarchar](500) NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL,
 CONSTRAINT [PK_Chat] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[id] [int] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Code] [nvarchar](500) NULL,
	[CourceId] [int] NULL,
	[CenterId] [int] NULL,
	[MentorId] [int] NULL,
	[ManageId] [int] NULL,
	[Status] [int] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommnentBlog]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommnentBlog](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Content] [nvarchar](50) NULL,
	[Star] [int] NULL,
	[UserRate] [int] NULL,
	[ReplyComment] [bigint] NULL,
	[UserCommnent] [int] NULL,
	[BlogId] [int] NULL,
 CONSTRAINT [PK_CommnentBlog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Code] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[Price] [money] NULL,
	[Image] [image] NULL,
	[ClassId] [int] NULL,
	[ImageSrc] [nvarchar](500) NULL,
	[BillId] [int] NULL,
	[CenterId] [uniqueidentifier] NULL,
	[EmployeeId] [uniqueidentifier] NULL,
	[TypeId] [int] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Date_Of_Class]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Date_Of_Class](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[day_in_week] [int] NULL,
	[class_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Adress] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](500) NULL,
	[Dob] [date] NULL,
	[CenterId] [uniqueidentifier] NULL,
	[AccountId] [uniqueidentifier] NULL,
	[RoleId] [uniqueidentifier] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[DateCreate] [date] NULL,
	[refreshTokenExpired] [datetime] NULL,
	[refreshToken] [text] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventChat]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventChat](
	[id] [int] NULL,
	[ChatId] [int] NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exam](
	[id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Desc] [nvarchar](50) NULL,
	[CourceId] [int] NULL,
 CONSTRAINT [PK_Exam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamClass]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamClass](
	[ExamId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[StudentId] [uniqueidentifier] NOT NULL,
	[Mark] [int] NOT NULL,
	[Rank] [int] NULL,
	[Evaluate] [text] NULL,
 CONSTRAINT [PK_ExamClass_1] PRIMARY KEY CLUSTERED 
(
	[ExamId] ASC,
	[ClassId] ASC,
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Image]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Image](
	[id] [int] NOT NULL,
	[Src] [nvarchar](50) NULL,
	[image] [image] NULL,
 CONSTRAINT [PK_Image] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImageBlog]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImageBlog](
	[BlogId] [int] NOT NULL,
	[ImageId] [int] NOT NULL,
 CONSTRAINT [PK_ImageBlog] PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC,
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImageUser]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImageUser](
	[StudentId] [uniqueidentifier] NOT NULL,
	[ImageId] [int] NOT NULL,
 CONSTRAINT [PK_ImageUser_1] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC,
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemberChat]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberChat](
	[id] [int] NOT NULL,
	[UserId] [int] NULL,
	[ChatId] [int] NULL,
	[Type] [nvarchar](500) NULL,
	[Data] [text] NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL,
 CONSTRAINT [PK_MemberChat] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[id] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[EmployeeId] [uniqueidentifier] NULL,
	[ViewCount] [float] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationClass]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationClass](
	[id] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[ClassId] [int] NULL,
	[StudentId] [uniqueidentifier] NULL,
	[ViewCount] [float] NULL,
	[Datecreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_NotificationClass] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_Role_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_schedule] [date] NULL,
	[slot] [int] NULL,
	[class_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentCenter]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentCenter](
	[CenterId] [uniqueidentifier] NOT NULL,
	[StudentId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_StudentCenter] PRIMARY KEY CLUSTERED 
(
	[CenterId] ASC,
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentClass]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentClass](
	[ClassId] [int] NOT NULL,
	[StudentId] [uniqueidentifier] NOT NULL,
	[DateCreate] [date] NULL,
	[DateUpdate] [date] NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_StudentClass_1] PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC,
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentProfile]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentProfile](
	[id] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](500) NULL,
	[MidName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Dob] [date] NULL,
	[Status] [int] NULL,
	[EmoloyeeId] [uniqueidentifier] NULL,
	[DateCreate] [date] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[refreshToken] [text] NULL,
	[refreshTokenExpired] [datetime] NULL,
	[DateUpdate] [date] NULL,
 CONSTRAINT [PK_StudentProfile] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherCenter]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherCenter](
	[CenterId] [uniqueidentifier] NOT NULL,
	[Teacherid] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TeacherCenter] PRIMARY KEY CLUSTERED 
(
	[CenterId] ASC,
	[Teacherid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeacherProfile]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherProfile](
	[Id] [uniqueidentifier] NOT NULL,
	[FirtsName] [nvarchar](500) NULL,
	[MidName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Adress] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[PhoneNumber] [float] NULL,
	[Dob] [date] NULL,
	[RoleId] [int] NOT NULL,
	[CertificateId] [int] NULL,
	[CreateDate] [date] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[UpdateDate] [date] NULL,
	[refreshToken] [text] NULL,
	[refreshTokenExpired] [datetime] NULL,
 CONSTRAINT [PK_TeacherProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeCourse]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeCourse](
	[Id] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [text] NULL,
	[CourseId] [int] NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Waiting list]    Script Date: 12 1:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Waiting list](
	[StudentId] [uniqueidentifier] NOT NULL,
	[EmployeeId] [uniqueidentifier] NOT NULL,
	[CourseId] [int] NOT NULL,
	[CreateDate] [date] NULL,
	[UpdateDate] [date] NULL,
	[Note] [text] NULL,
 CONSTRAINT [PK_Waiting list_1] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC,
	[EmployeeId] ASC,
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Account] ([id], [UserName], [PassWord], [RoleId], [DateCreate], [DateUpdate], [refreshToken], [refreshTokenExpired]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', N'a', N'a', 1, NULL, NULL, NULL, CAST(N'2023-10-19T03:13:02.517' AS DateTime))
GO
INSERT [dbo].[Center] ([Id], [Name], [Description], [Adress], [CodeCenter], [AdminId], [NumberPhone], [CreateDate], [UpdateDate]) VALUES (N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Center] ([Id], [Name], [Description], [Adress], [CodeCenter], [AdminId], [NumberPhone], [CreateDate], [UpdateDate]) VALUES (N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'Center1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [DateUpdate]) VALUES (N'55c318d0-196e-4b10-9085-0f333ff1fc81', NULL, NULL, NULL, NULL, NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', NULL, NULL, N'aaaa', N'990B6F5F7AF9EC7816DA20E6FB03C0A9EFEE78D0B6AC25C61A4AE02EC607176B', CAST(N'2023-10-04' AS Date), NULL, NULL, CAST(N'2023-10-04' AS Date))
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [DateUpdate]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'e', N'e', NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [DateUpdate]) VALUES (N'59f21d20-0ab7-46f4-a62a-2084b2b0676b', NULL, NULL, NULL, NULL, NULL, N'3fa85f64-5717-4562-b3fc-2c963f66afa6', NULL, NULL, N'string', N'990B6F5F7AF9EC7816DA20E6FB03C0A9EFEE78D0B6AC25C61A4AE02EC607176B', CAST(N'2023-10-04' AS Date), NULL, NULL, CAST(N'2023-10-04' AS Date))
INSERT [dbo].[Employee] ([id], [Name], [Adress], [Email], [PhoneNumber], [Dob], [CenterId], [AccountId], [RoleId], [UserName], [PassWord], [DateCreate], [refreshTokenExpired], [refreshToken], [DateUpdate]) VALUES (N'915dda93-8e70-4b6e-bcac-493a30b2d932', NULL, NULL, NULL, NULL, NULL, N'd6f9d24f-07b9-46b5-ab27-2a42553cd823', NULL, NULL, N'string', N'990B6F5F7AF9EC7816DA20E6FB03C0A9EFEE78D0B6AC25C61A4AE02EC607176B', CAST(N'2023-10-04' AS Date), NULL, NULL, CAST(N'2023-10-04' AS Date))
GO
INSERT [dbo].[StudentProfile] ([id], [FirstName], [MidName], [LastName], [Address], [Email], [PhoneNumber], [Dob], [Status], [EmoloyeeId], [DateCreate], [UserName], [PassWord], [refreshToken], [refreshTokenExpired], [DateUpdate]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N's', N's', NULL, CAST(N'2023-10-19T03:14:21.100' AS DateTime), NULL)
GO
INSERT [dbo].[TeacherProfile] ([Id], [FirtsName], [MidName], [LastName], [Adress], [Email], [PhoneNumber], [Dob], [RoleId], [CertificateId], [CreateDate], [UserName], [PassWord], [UpdateDate], [refreshToken], [refreshTokenExpired]) VALUES (N'e45e13d8-cff0-4fc7-b7c9-1d53e95c502d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, N't', N't', NULL, NULL, NULL)
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Schedule] FOREIGN KEY([schedule_id])
REFERENCES [dbo].[Schedule] ([id])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Schedule]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_BillDetail] FOREIGN KEY([BillId])
REFERENCES [dbo].[BillDetail] ([id])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_BillDetail]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_StudentProfile]
GO
ALTER TABLE [dbo].[BillDetail]  WITH CHECK ADD  CONSTRAINT [FK_BillDetail_Course] FOREIGN KEY([CourceId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[BillDetail] CHECK CONSTRAINT [FK_BillDetail_Course]
GO
ALTER TABLE [dbo].[Center]  WITH CHECK ADD  CONSTRAINT [FK_Center_Account] FOREIGN KEY([AdminId])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[Center] CHECK CONSTRAINT [FK_Center_Account]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Course] FOREIGN KEY([CourceId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Course]
GO
ALTER TABLE [dbo].[CommnentBlog]  WITH CHECK ADD  CONSTRAINT [FK_CommnentBlog_Blog] FOREIGN KEY([BlogId])
REFERENCES [dbo].[Blog] ([id])
GO
ALTER TABLE [dbo].[CommnentBlog] CHECK CONSTRAINT [FK_CommnentBlog_Blog]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Center1] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Center1]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Employee]
GO
ALTER TABLE [dbo].[Date_Of_Class]  WITH CHECK ADD  CONSTRAINT [FK_Date_Of_Class_Class] FOREIGN KEY([class_id])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[Date_Of_Class] CHECK CONSTRAINT [FK_Date_Of_Class_Class]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Account]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Center]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Role]
GO
ALTER TABLE [dbo].[EventChat]  WITH CHECK ADD  CONSTRAINT [FK_EventChat_Chat] FOREIGN KEY([ChatId])
REFERENCES [dbo].[Chat] ([id])
GO
ALTER TABLE [dbo].[EventChat] CHECK CONSTRAINT [FK_EventChat_Chat]
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD  CONSTRAINT [FK_Exam_Course] FOREIGN KEY([CourceId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Exam] CHECK CONSTRAINT [FK_Exam_Course]
GO
ALTER TABLE [dbo].[ExamClass]  WITH CHECK ADD  CONSTRAINT [FK_ExamClass_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[ExamClass] CHECK CONSTRAINT [FK_ExamClass_Class]
GO
ALTER TABLE [dbo].[ExamClass]  WITH CHECK ADD  CONSTRAINT [FK_ExamClass_Exam] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([id])
GO
ALTER TABLE [dbo].[ExamClass] CHECK CONSTRAINT [FK_ExamClass_Exam]
GO
ALTER TABLE [dbo].[ExamClass]  WITH CHECK ADD  CONSTRAINT [FK_ExamClass_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[ExamClass] CHECK CONSTRAINT [FK_ExamClass_StudentProfile]
GO
ALTER TABLE [dbo].[ImageBlog]  WITH CHECK ADD  CONSTRAINT [FK_ImageBlog_Blog] FOREIGN KEY([BlogId])
REFERENCES [dbo].[Blog] ([id])
GO
ALTER TABLE [dbo].[ImageBlog] CHECK CONSTRAINT [FK_ImageBlog_Blog]
GO
ALTER TABLE [dbo].[ImageBlog]  WITH CHECK ADD  CONSTRAINT [FK_ImageBlog_Image] FOREIGN KEY([ImageId])
REFERENCES [dbo].[Image] ([id])
GO
ALTER TABLE [dbo].[ImageBlog] CHECK CONSTRAINT [FK_ImageBlog_Image]
GO
ALTER TABLE [dbo].[ImageUser]  WITH CHECK ADD  CONSTRAINT [FK_ImageUser_Image] FOREIGN KEY([ImageId])
REFERENCES [dbo].[Image] ([id])
GO
ALTER TABLE [dbo].[ImageUser] CHECK CONSTRAINT [FK_ImageUser_Image]
GO
ALTER TABLE [dbo].[ImageUser]  WITH CHECK ADD  CONSTRAINT [FK_ImageUser_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[ImageUser] CHECK CONSTRAINT [FK_ImageUser_StudentProfile]
GO
ALTER TABLE [dbo].[MemberChat]  WITH CHECK ADD  CONSTRAINT [FK_MemberChat_Chat] FOREIGN KEY([ChatId])
REFERENCES [dbo].[Chat] ([id])
GO
ALTER TABLE [dbo].[MemberChat] CHECK CONSTRAINT [FK_MemberChat_Chat]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_Employee]
GO
ALTER TABLE [dbo].[NotificationClass]  WITH CHECK ADD  CONSTRAINT [FK_NotificationClass_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[NotificationClass] CHECK CONSTRAINT [FK_NotificationClass_Class]
GO
ALTER TABLE [dbo].[NotificationClass]  WITH CHECK ADD  CONSTRAINT [FK_NotificationClass_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[NotificationClass] CHECK CONSTRAINT [FK_NotificationClass_StudentProfile]
GO
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Class] FOREIGN KEY([class_id])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Class]
GO
ALTER TABLE [dbo].[StudentCenter]  WITH CHECK ADD  CONSTRAINT [FK_StudentCenter_Center] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[StudentCenter] CHECK CONSTRAINT [FK_StudentCenter_Center]
GO
ALTER TABLE [dbo].[StudentCenter]  WITH CHECK ADD  CONSTRAINT [FK_StudentCenter_StudentProfile1] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[StudentCenter] CHECK CONSTRAINT [FK_StudentCenter_StudentProfile1]
GO
ALTER TABLE [dbo].[StudentClass]  WITH CHECK ADD  CONSTRAINT [FK_StudentClass_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([id])
GO
ALTER TABLE [dbo].[StudentClass] CHECK CONSTRAINT [FK_StudentClass_Class]
GO
ALTER TABLE [dbo].[StudentClass]  WITH CHECK ADD  CONSTRAINT [FK_StudentClass_StudentProfile] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[StudentClass] CHECK CONSTRAINT [FK_StudentClass_StudentProfile]
GO
ALTER TABLE [dbo].[StudentProfile]  WITH CHECK ADD  CONSTRAINT [FK_StudentProfile_Employee] FOREIGN KEY([EmoloyeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[StudentProfile] CHECK CONSTRAINT [FK_StudentProfile_Employee]
GO
ALTER TABLE [dbo].[TeacherCenter]  WITH CHECK ADD  CONSTRAINT [FK_TeacherCenter_Center1] FOREIGN KEY([CenterId])
REFERENCES [dbo].[Center] ([Id])
GO
ALTER TABLE [dbo].[TeacherCenter] CHECK CONSTRAINT [FK_TeacherCenter_Center1]
GO
ALTER TABLE [dbo].[TeacherCenter]  WITH CHECK ADD  CONSTRAINT [FK_TeacherCenter_TeacherProfile1] FOREIGN KEY([Teacherid])
REFERENCES [dbo].[TeacherProfile] ([Id])
GO
ALTER TABLE [dbo].[TeacherCenter] CHECK CONSTRAINT [FK_TeacherCenter_TeacherProfile1]
GO
ALTER TABLE [dbo].[TypeCourse]  WITH CHECK ADD  CONSTRAINT [FK_TypeCourse_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[TypeCourse] CHECK CONSTRAINT [FK_TypeCourse_Course]
GO
ALTER TABLE [dbo].[Waiting list]  WITH CHECK ADD  CONSTRAINT [FK_Waiting list_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Waiting list] CHECK CONSTRAINT [FK_Waiting list_Course]
GO
ALTER TABLE [dbo].[Waiting list]  WITH CHECK ADD  CONSTRAINT [FK_Waiting list_Course1] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([id])
GO
ALTER TABLE [dbo].[Waiting list] CHECK CONSTRAINT [FK_Waiting list_Course1]
GO
ALTER TABLE [dbo].[Waiting list]  WITH CHECK ADD  CONSTRAINT [FK_Waiting list_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[Waiting list] CHECK CONSTRAINT [FK_Waiting list_Employee]
GO
ALTER TABLE [dbo].[Waiting list]  WITH CHECK ADD  CONSTRAINT [FK_Waiting list_StudentProfile1] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentProfile] ([id])
GO
ALTER TABLE [dbo].[Waiting list] CHECK CONSTRAINT [FK_Waiting list_StudentProfile1]
GO
USE [master]
GO
ALTER DATABASE [sep490_g22] SET  READ_WRITE 
GO
