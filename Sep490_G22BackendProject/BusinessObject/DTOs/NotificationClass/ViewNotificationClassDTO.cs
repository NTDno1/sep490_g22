﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.NotificationClass
{
    public class ViewNotificationClassDTO
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ClassName { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}
