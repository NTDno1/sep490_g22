﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.NotificationClass
{
    public class AddNotificationClass
    {
        public string? Title { get; set; }
        public string? Description { get; set; }
        public int? ClassId { get; set; }
    }
}
