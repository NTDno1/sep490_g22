﻿using BusinessObject.Models;
using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SchedualDTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.AttendenceDTO;
using BusinessObject.DTOs.TeacherTeachingHourDTO;
using BusinessObject.DTOs.SyllabusDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.DTOs.NotificationClass;
using Newtonsoft.Json.Linq;
using BusinessObject.DTOs.OrderDTO;

namespace BusinessObject.DTOs
{
    public class MappingProfile : Profile
    {
        public MappingProfile() {
            CreateMap<Center,AddNewCenterDTOs>().ReverseMap();

            CreateMap<Center, ListCenterDTOs>().ForMember(dest => dest.Editor, opt => opt.MapFrom(src => src.Admin.UserName)).ReverseMap();

            CreateMap<Employee, AddEmployeeDTO>().ReverseMap();

            CreateMap<Course, ListCourseDTO>().ReverseMap();

            CreateMap<Course, DetailCourseDTO>().ReverseMap();

            CreateMap<Course, UpdateCourseDTO>().ReverseMap();
            CreateMap<TeacherProfile, ViewTeacherDTO>()
                .ForMember(dest => dest.Updater, opt => opt.MapFrom(src => "Admin"))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.MidName + " " + src.LastName + " #" + src.Id.ToString().Substring(src.Id.ToString().Length - 6)))
                .ReverseMap();


            CreateMap<SlotDate, ViewSlotdateDTO>()
                //.ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.ClassSlotDates.))
                .ReverseMap();

            CreateMap<Employee, ListEmployeeDTO>()
                .ForMember(dest => dest.CenterName, opt => opt.MapFrom(src => src.Center.Name))
                .ForMember(dest => dest.SuperAdminName, opt => opt.MapFrom(src => src.Account.UserName))
                .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Role.Name))
                .ReverseMap();
            CreateMap<Saq, ViewSqaDTOs>();
            //int CountClassesWithTrueStatus(ICollection<Class> src)
            //{
            //    return src.Count(c => c.);
            //} 
            CreateMap<Class, ViewClassDTO>()
                .ForMember(dest => dest.TeacherName, opt => opt.MapFrom(src => src.Teacher.FirstName+ " " + src.Teacher.MidName +" "+ src.Teacher.LastName))
                .ForMember(dest => dest.AdminCenterName, opt => opt.MapFrom(src => src.AdminCenter.Name))
                .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Cource.Name))
                .ForMember(dest => dest.SlotProcess, opt => opt.MapFrom(src => src.Scheduals != null ? src.Scheduals.Count(u => (u.Staus == 1 || u.Staus == 2)) : 1))
                //.ForMember(dest => dest.StudentNumber, opt => opt.MapFrom(src => src.StudentClasses != null ? src.StudentClasses.Count() : 0))
                .ForMember(dest => dest.StudentNumber, opt => opt.MapFrom(src => src.StudentClasses.Count()))
                .ForMember(dest => dest.classSlotDates, opt => opt.MapFrom(src => src.ClassSlotDates.Select(cs => new ViewSlotdateDTO
                {
                    Id= cs.SlotId,
                    TimeEnd= cs.Slot.TimeEnd,
                    TimeStart= cs.Slot.TimeStart,
                    Day = cs.Slot.Day,
                    RoomId = cs.Room.Id,
                    RoomName = cs.Room.Name + " " +cs.Room.Type.Name,
                    RoomCode = cs.Room.Code
                }).ToList()))
                //.ForMember(dest => dest.CourseName, opt =>     opt.MapFrom(src => src.Cource.Name))
                .ReverseMap();

            CreateMap<Class, AddClassDTOs>().ReverseMap();

          
            
            CreateMap<ClassSlotDate, AddClassSlotDateDTO>()
                .ForMember(dest => dest.SlotName, opt => opt.MapFrom(src => src.Slot.TimeStart))
                .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.Room.Name))
                .ReverseMap();

            CreateMap<ClassSlotDate, ViewClassSlotDateDTO>()
                .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Class.Name))
                .ForMember(dest => dest.ClassCode, opt => opt.MapFrom(src => src.Class.Code))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Class.Status))
                .ForMember(dest => dest.DateCreate, opt => opt.MapFrom(src => src.Class.DateCreate))
                .ForMember(dest => dest.DateUpdate, opt => opt.MapFrom(src => src.Class.DateUpdate))
                .ForMember(dest => dest.TimeStart, opt => opt.MapFrom(src => src.Slot.TimeStart))
                .ForMember(dest => dest.TimeEnd, opt => opt.MapFrom(src => src.Slot.TimeEnd))
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.Slot.Day))
                .ForMember(dest => dest.CourseID, opt => opt.MapFrom(src => src.Class.Cource.Id))
                .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Class.Cource.Name))
                .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.Room.Name +" "+ src.Room.Type.Name))
                .ForMember(dest => dest.RoomCode, opt => opt.MapFrom(src => src.Room.Code))
                .ForMember(dest => dest.TeacherName, opt => opt.MapFrom(src => src.Class.Teacher.FirstName+" "+ src.Class.Teacher.MidName+" "+src.Class.Teacher.LastName))
                .ReverseMap();

            CreateMap<Schedual, ViewSchedualDTO>()
                .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Class.Name))
                .ForMember(dest => dest.TimeStart, opt => opt.MapFrom(src => src.SlotDate.TimeStart))
                .ForMember(dest => dest.TimeEnd, opt => opt.MapFrom(src => src.SlotDate.TimeEnd))
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.SlotDate.Day))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Class.Code))
                .ForMember(dest => dest.CourceId, opt => opt.MapFrom(src => src.Class.Cource.Id))
                .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Class.Cource.Name))
                .ForMember(dest => dest.TeacherId, opt => opt.MapFrom(src => src.Class.TeacherId))
                .ForMember(dest => dest.TeacherName, opt => opt.MapFrom(src => src.Class.Teacher.FirstName+" "+ src.Class.Teacher.MidName +" "+ src.Class.Teacher.LastName))
                .ForMember(dest => dest.AdminCenterName, opt => opt.MapFrom(src => src.Class.AdminCenter.Name))
                .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.RoomIdUpDateNavigation.Name +" "+ src.RoomIdUpDateNavigation.Type.Name))

                .ForMember(dest => dest.TotalSlot, opt => opt.MapFrom(src => src.Class.SlotNumber))
                .ReverseMap();

            CreateMap<ExtendedStudentClass, ViewStudentClassDTO>()
                .ForMember(dest => dest.StudentName, opt => opt.MapFrom(src => (src.Student.FirstName + " " +src.Student.MidName +" "+ src.Student.LastName)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Class.Status))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.Class.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.Class.EndDate))
                .ReverseMap();

            CreateMap<ClassRoom, ViewClassRoomDTO>()
               .ForMember(dest => dest.TypeName, opt => opt.MapFrom(src => src.Type.Name))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name + " " + src.Type.Name))
               .ReverseMap();

            CreateMap<ExtendedCourse, ViewSyllabusOfCourseDTO>()
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Syllabus.Id))
               .ForMember(dest => dest.SyllabusName, opt => opt.MapFrom(src => src.Syllabus.SyllabusName))
               .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Syllabus.Description))
               .ForMember(dest => dest.TimeAllocation, opt => opt.MapFrom(src => src.Syllabus.TimeAllocation))
               .ForMember(dest => dest.StudentTasks, opt => opt.MapFrom(src => src.Syllabus.StudentTasks))
               .ForMember(dest => dest.CourseObjectives, opt => opt.MapFrom(src => src.Syllabus.CourseObjectives))
               .ForMember(dest => dest.Exam, opt => opt.MapFrom(src => src.Syllabus.Exam))
               .ForMember(dest => dest.SubjectCode, opt => opt.MapFrom(src => src.Syllabus.SubjectCode))
               .ForMember(dest => dest.Contact, opt => opt.MapFrom(src => src.Syllabus.Contact))
               .ForMember(dest => dest.CourseLearningOutcome, opt => opt.MapFrom(src => src.Syllabus.CourseLearningOutcome))
               .ForMember(dest => dest.MainTopics, opt => opt.MapFrom(src => src.Syllabus.MainTopics))
               .ForMember(dest => dest.RequiredKnowledge, opt => opt.MapFrom(src => src.Syllabus.RequiredKnowledge))
               .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Syllabus.Status))
               .ForMember(dest => dest.LinkBook, opt => opt.MapFrom(src => src.Syllabus.LinkBook))
               .ForMember(dest => dest.LinkDocument, opt => opt.MapFrom(src => src.Syllabus.LinkDocument))
               .ReverseMap();

            CreateMap<StudentProfile, ViewListStudentDTO>()
                .ForMember(dest => dest.Updater, opt => opt.MapFrom(src => src.Emoloyee.Name))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.MidName + " " + src.LastName + " #" + src.Id.ToString().Substring(src.Id.ToString().Length - 6)))
               .ReverseMap();

            CreateMap<Atendence, LOSNAttendanceDTO>()
            .ForMember(dest => dest.StudentName, opt => opt.MapFrom(src => src.Student.FirstName +" "+src.Student.MidName+" "+src.Student.LastName +" #" + src.Student.Id.ToString().Substring(src.Student.Id.ToString().Length - 6)))
            .ForMember(dest => dest.StudentImage, opt => opt.MapFrom(src => src.Student.Image))
            .ReverseMap();
            CreateMap<Atendence, ViewAttendenceDTO>()
                .ForMember(dest => dest.ClassId, opt => opt.MapFrom(src => src.Schedual.Class.Id))
                .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Schedual.Class.Name))
                .ForMember(dest => dest.ClassCode, opt => opt.MapFrom(src => src.Schedual.Class.Code))
                .ForMember(dest => dest.TotalSlot, opt => opt.MapFrom(src => src.Schedual.Class.SlotNumber))
                .ForMember(dest => dest.StatusSchedule, opt => opt.MapFrom(src => src.Schedual.Staus))
                .ForMember(dest => dest.SlotNum, opt => opt.MapFrom(src => src.Schedual.SlotNum))
                .ForMember(dest => dest.DateOffSlot, opt => opt.MapFrom(src => src.Schedual.DateOffSlot))
                .ForMember(dest => dest.TimeStart, opt => opt.MapFrom(src => src.Schedual.SlotDate.TimeStart))
                .ForMember(dest => dest.TimeEnd, opt => opt.MapFrom(src => src.Schedual.SlotDate.TimeEnd))
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.Schedual.SlotDate.Day))
                .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.Schedual.RoomIdUpDateNavigation.Name +" " + src.Schedual.RoomIdUpDateNavigation.Type.Name))
               .ReverseMap();

            CreateMap<TeacherTeachingHour, ViewTeacherTeachingHourDTO>()
                .ForMember(dest => dest.TeacherName, opt => opt.MapFrom(src => src.Teacher.FirstName +" "+ src.Teacher.MidName+" "+src.Teacher.LastName))
                .ForMember(dest => dest.ClassId, opt => opt.MapFrom(src => src.Schedule.ClassId))
                .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Schedule.Class.Name))
                .ForMember(dest => dest.SlotNum, opt => opt.MapFrom(src => src.Schedule.SlotNum))
                .ForMember(dest => dest.DateOffSlot, opt => opt.MapFrom(src => src.Schedule.DateOffSlot))
                .ForMember(dest => dest.SlotDateId, opt => opt.MapFrom(src => src.Schedule.SlotDateId))
                .ForMember(dest => dest.TimeStart, opt => opt.MapFrom(src => src.Schedule.SlotDate.TimeStart))
                .ForMember(dest => dest.TimeEnd, opt => opt.MapFrom(src => src.Schedule.SlotDate.TimeEnd))
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.Schedule.SlotDate.Day))
               .ReverseMap();

            CreateMap<Syllabus, ViewSyllabusDTO>()
                //.ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Courses. + " " + src.Class.Code))
                .ReverseMap();
            CreateMap<NotificationClase, ViewNotificationClassDTO>()
                .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Class.Name +" " + src.Class.Code))
                .ReverseMap();

            CreateMap<Order, ViewOrderDTO>()
                 .ForMember(dest => dest.StudentName, opt => opt.MapFrom(src => src.Student.FirstName))
               .ForMember(dest => dest.EmployeeName, opt => opt.MapFrom(src => src.Employee.Name))
               .ForMember(dest => dest.TotalCourse, opt => opt.MapFrom(src => src.OrderDetails.Sum(od => od.Price)))
               .ReverseMap();
            CreateMap<OrderDetail, ViewOrderDetailDTO>()
               .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Course.Name))
               .ForMember(dest => dest.ClassName, opt => opt.MapFrom(src => src.Class.Name))
            .ReverseMap();

            CreateMap<OrderDetail, OrderDetailView>()
                .ForMember(dest => dest.StudentID, opt => opt.MapFrom(src => src.Order.Student.Id))
           .ReverseMap();


            CreateMap<Order, AddOrderDTO>()
               .ReverseMap();
        }
    }
}
