﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;


namespace BusinessObject.DTOs.ClassSlotDateDTO
{
    public class AddClassSlotDateDTO
    {
        public int ClassId { get; set; }
        public int SlotId { get; set; } 
        public int RoomIds { get; set; }
        [JsonIgnore]
        public string? SlotName { get; set; }
        [JsonIgnore]
        public string? RoomName { get; set; }

    }
}
