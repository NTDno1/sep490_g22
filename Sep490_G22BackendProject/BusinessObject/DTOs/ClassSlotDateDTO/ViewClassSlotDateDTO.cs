﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassSlotDateDTO
{
    public class ViewClassSlotDateDTO
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int SlotId { get; set; }
        public int? Uid { get; set; }
        public string? ClassName { get; set; }
        public string? TeacherName { get; set; }
        public string? ClassCode { get; set; }
        public int? Status { get; set; }
        public int? SlotNumber { get; set; }
        public string? DateCreate { get; set; }
        public string? DateUpdate { get; set; }
        public string? TimeStart { get; set; }
        public string? TimeEnd { get; set; }
        public string? Day { get; set; }
        public string? CourseID { get; set; }
        public string? CourseName { get; set; }
        public string? RoomName { get; set; }
        public string? RoomCode { get; set; }

    }
}
