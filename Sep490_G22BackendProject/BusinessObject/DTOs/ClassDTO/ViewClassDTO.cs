﻿using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassDTO
{
    public class ViewClassDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public int? CourceId { get; set; }
        public string? CourseName { get; set; }
        public Guid TeacherId { get; set; }
        public string? TeacherName { get; set; }
        public string? AdminCenterName { get; set; }
        public int? Status { get; set; }
        public int? NotificationId { get; set; }
        public int? SlotId { get; set; }
        public int SlotNumber { get; set; }
        public int SlotProcess { get; set; }
        public int StudentNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<ViewSlotdateDTO> classSlotDates { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}
