﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassDTO
{
    public class ClassFilter
    {
        public int ClassID { get; set; }

        public string? ClassName { get; set; }
    }
}
