﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace BusinessObject.DTOs.ClassDTO
{
    public class UpdateClassDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        //public string? Code { get; set; }
        public int? CourceId { get; set; }
        [JsonIgnore]
        public Guid? AdminCenterId { get; set; }
        public Guid? TeacherId { get; set; }
        [JsonIgnore]
        public Guid? CenterId { get; set; }
        [JsonIgnore]
        public int? NotificationId { get; set; }
        public int? SlotNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DateUpdate { get; set; } 
    }
}
