﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassRoomDTO
{
    public class ViewRoomAdminDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public int? TypeId { get; set; }
        public int? Status { get; set; }
        public string? CreatedBy { get; set; }
        public string? CreatedAt { get; set; }
        public string? LastModifiedBy { get; set; }
        public string? LastModifiedAt { get; set; }
        public string? TypeName { get; set; }
    }
}
