﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassRoomDTO
{
    public class AddClassRoomDTO
    {
        public string? Name { get; set; }
        public int? TypeId { get; set; }
        public Guid? CenterId { get; set; }
    }
}
