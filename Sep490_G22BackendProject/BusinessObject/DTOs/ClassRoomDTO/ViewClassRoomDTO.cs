﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ClassRoomDTO
{
    public class ViewClassRoomDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public int? TypeId { get; set; }
        public string? TypeName { get; set; }
    }
}
