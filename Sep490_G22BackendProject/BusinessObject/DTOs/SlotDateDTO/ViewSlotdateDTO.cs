﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.SlotDateDTO
{
    public class ViewSlotdateDTO
    {
        public int Id { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; }
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomCode { get; set; }
    }
}
