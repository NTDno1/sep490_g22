﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class ProfileDTO
    {
        public string firtsName { get; set; }
        public string lastName { get; set; }
        public string phoneNumber { get; set; }
        public string adress { get; set; }
        public string image { get; set; }
        public string email { get; set; }
        public string dob { get; set; }
        public string userName { get; set; }


    }
}
