﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.CateBlogDTO
{
    public class ViewCateBlogDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Description { get; set; }
        public int? Level { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }
        public virtual ICollection<Blog>? Blogs { get; set; }
    }
}
