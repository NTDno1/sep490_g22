﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class UpdateExamDTOs
    {
        public int courseID { get; set; }

        public List<ExamDTOsRequest> listExam { get; set; }
    }
}
