﻿using BusinessObject.DTOs.ClassDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class ExamClassFilter
    {
        public List<ExamFilter>? ListExam { get; set; }  

        public List<ClassFilter>? ListClass { get; set; }

    }
}
