﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class FilterOfCourse
    {
        public int? courseID { get; set; }

        public string courseName { get; set; }

        public int? classID { get; set; }

        public string? className { get; set; }
    }
}
