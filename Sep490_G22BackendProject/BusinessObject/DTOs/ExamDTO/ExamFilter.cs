﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class ExamFilter
    {
        public int ExamID { get; set; }

        public string? ExamName { get; set; }

        public double? Average { get; set; }
    }
}
