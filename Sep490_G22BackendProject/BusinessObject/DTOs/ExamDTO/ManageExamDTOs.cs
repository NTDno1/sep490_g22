﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class ManageExamDTOs
    {
        public int? courseID { get; set; }

        public string courseName { get; set; }

        public List<ExamDTOs> exams { get; set; } 
    }
}
