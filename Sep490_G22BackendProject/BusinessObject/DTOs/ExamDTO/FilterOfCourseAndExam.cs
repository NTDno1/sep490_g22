﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class FilterOfCourseAndExam
    {
        public int ExamID { get; set; }

        public string CourseExam { get; set; }
    }
}
