﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class CourseClassExamResponse
    {
        public List<FilterOfClass> ListClassFilter { get; set; }

        public List<FilterOfCourseAndExam> ListCourseExamFilter { get; set; }
    }
}
