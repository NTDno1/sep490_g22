﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class ExamDTOsRequest
    {
        public int examID { get; set; }
        public string? examName { get; set; }
        public double? average { get; set; }
    }
}
