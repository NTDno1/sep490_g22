﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class FilterOfClass
    {
        public int ClassID { get; set; }

        public string Name { get; set; }
    }
}
