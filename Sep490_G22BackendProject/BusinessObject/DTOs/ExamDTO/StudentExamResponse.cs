﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.ExamDTO
{
    public class StudentExamResponse
    {
        public int ExamID { get; set; }

        public int StudentClassID { get; set; }

        public double? Mark { get; set; }

        public string? Evaluate { get; set; }

        public string StudentName { get; set; }


    }
}
