﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.AttendenceDTO
{
    public class LOSNAttendanceDTO
    {
        public int Id { get; set; }
        public Guid? StudentId { get; set; }
        public string? StudentName { get; set; }
        public string? StudentImage { get; set; }
        public int? Status { get; set; }
        public int? SchedualId { get; set; }
        public string? Note { get; set; }
    }
}
