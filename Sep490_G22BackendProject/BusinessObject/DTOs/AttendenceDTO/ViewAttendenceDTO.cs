﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.AttendenceDTO
{
    public class ViewAttendenceDTO
    {
        public int Id { get; set; }
        public Guid? StudentId { get; set; }
        public int? Status { get; set; }
        public int? SchedualId { get; set; }
        public string? Note { get; set; }
        public int ClassId { get; set; }
        public string? ClassName { get; set; }
        public string? ClassCode { get; set; }
        public int? StatusSchedule { get; set; }
        public int TotalSlot { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; } = null!;
        public int? SlotNum { get; set; }
        public DateTime? DateOffSlot { get; set; }
        public string? RoomName { get; set; }
    }
}
