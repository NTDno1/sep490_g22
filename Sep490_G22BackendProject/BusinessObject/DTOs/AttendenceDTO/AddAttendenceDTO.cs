﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.AttendenceDTO
{
    public class AddAttendenceDTO
    {
        public int Id { get; set; }
        public Guid? StudentId { get; set; }
        public int? Status { get; set; }
        public int? SchedualId { get; set; }
        public int SlotNum { get; set; }
        public DateTime DateOffSlot { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; } = null!;
    }
}
