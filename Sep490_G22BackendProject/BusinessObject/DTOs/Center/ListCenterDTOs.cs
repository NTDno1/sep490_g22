﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace BusinessObject.DTOs
{
    public class ListCenterDTOs
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Adress { get; set; }
        public string? CodeCenter { get; set; }
        public Guid? AdminId { get; set; }
        public string? Editor { get; set; }
        public string? NumberPhone { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Status { get; set; }
    }
}
