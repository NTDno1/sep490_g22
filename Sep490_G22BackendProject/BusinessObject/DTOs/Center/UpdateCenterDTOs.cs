﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace BusinessObject.DTOs
{
    public class UpdateCenterDTOs
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Adress { get; set; }
        public string? CodeCenter { get; set; }
        [JsonIgnore]
        public string? AdminId { get; set; }
        public string? NumberPhone { get; set; }
    }
}
