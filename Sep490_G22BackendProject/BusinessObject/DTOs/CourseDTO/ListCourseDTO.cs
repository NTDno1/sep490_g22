﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.CourseDTO
{
    public class ListCourseDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public string? Description { get; set; }
        public decimal? Price { get; set; }
        public int? ClassId { get; set; }
        public string? ImageSrc { get; set; }
        public int? BillId { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? EmployeeId { get; set; }
        public int? TypeId { get; set; }
        public int? SyllabusId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? Rating { get; set; }
    }
}
