﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.CourseDTO
{
    public class ViewSyllabusOfCourseDTO
    {
        public int Id { get; set; }
        public string? SyllabusName { get; set; }
        public string? Description { get; set; }
        public string? TimeAllocation { get; set; }
        public string? StudentTasks { get; set; }
        public string? CourseObjectives { get; set; }
        public string? Exam { get; set; }
        public string? SubjectCode { get; set; }
        public string? Contact { get; set; }
        public string? CourseLearningOutcome { get; set; }
        public string? MainTopics { get; set; }
        public string? RequiredKnowledge { get; set; }
        public string? LinkBook { get; set; }
        public string? LinkDocument { get; set; }
        public int? Status { get; set; }
        public string? ClassName { get; set; }

        ///////////////
    }
    public class ExtendedCourse : Course
    {
        public string? ClassName { get; set; }
    }
}
