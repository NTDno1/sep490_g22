﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace BusinessObject.DTOs.CourseDTO
{
    public class UpdateCourseDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public string? Description { get; set; }
        public decimal? Price { get; set; }
        [JsonIgnore]
        public string? ImageSrc { get; set; }
        [JsonIgnore]
        public Guid? EmployeeId { get; set; }
        public int? SyllabusId { get; set; }
    }
}
