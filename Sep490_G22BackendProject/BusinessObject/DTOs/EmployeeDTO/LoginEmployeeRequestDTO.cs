﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.EmployeeDTO
{
    public class LoginEmployeeRequestDTO
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}
