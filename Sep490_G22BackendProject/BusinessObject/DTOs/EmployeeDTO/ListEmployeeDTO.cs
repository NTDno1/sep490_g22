﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.EmployeeDTO
{
    public class ListEmployeeDTO
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Adress { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public string? CenterName { get; set; }
        public string? SuperAdminName { get; set; }
        public string? RoleName { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
         public int? Status { get; set; }
    }
}
