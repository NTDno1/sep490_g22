﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CateBlogDTO;
using BusinessObject.DTOs.RoomDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AutoMapper
{
    public class AutoMapperRoom : Profile
    {
        public AutoMapperRoom()
        {
            CreateMap<TypeRoom, ViewRoomDTO>()
                 .ForMember(x => x.Id,
            options => options.MapFrom(source => source.Id))
                .ForMember(x => x.Name,
            options => options.MapFrom(source => source.Name))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ForMember(x => x.CreatedAt,
            options => options.MapFrom(source => source.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.CreatedBy,
            options => options.MapFrom(source => source.CreatedBy))
                .ForMember(x => x.LastModifiedAt,
            options => options.MapFrom(source => source.UpdateDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.LastModifiedBy,
            options => options.MapFrom(source => source.UpdateBy))
                .ReverseMap();


            CreateMap<TypeRoom, AddRoomDTO>()
                 .ForMember(x => x.Name,
            options => options.MapFrom(source => source.Name))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ReverseMap();

            CreateMap<TypeRoom, UpdateRoomDTO>()
                 .ForMember(x => x.Name,
            options => options.MapFrom(source => source.Name))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ReverseMap();
        }
    }
}
