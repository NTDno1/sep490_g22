﻿using AutoMapper;
using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.RoomDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.AutoMapper
{
    public class AutoMapperClassRoom : Profile
    {
        public AutoMapperClassRoom()
        {
            CreateMap<ClassRoom, ViewRoomAdminDTO>()
                 .ForMember(x => x.Id,
            options => options.MapFrom(source => source.Id))
                .ForMember(x => x.Name,
            options => options.MapFrom(source => source.Name))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ForMember(x => x.CreatedAt,
            options => options.MapFrom(source => source.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.CreatedBy,
            options => options.MapFrom(source => source.CreatedBy))
                .ForMember(x => x.LastModifiedAt,
            options => options.MapFrom(source => source.UpdateDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.LastModifiedBy,
            options => options.MapFrom(source => source.UpdateBy))
                .ForMember(dest => dest.TypeName, opt => opt.MapFrom(src => src.Type.Name))
                .ReverseMap();

            CreateMap<ClassRoom, AddClassRoomDTO>()
                .ReverseMap();

            CreateMap<ClassRoom, UpdateBlogDTO>()
                .ReverseMap();

            CreateMap<ClassRoom, ViewRoomDetailDTO>()
                .ReverseMap();
        }
    }
}
