﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CateBlogDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AutoMapper
{
    public class AutoMapperBlog : Profile
    {
        public AutoMapperBlog()
        {
            CreateMap<Blog, ViewBlogDTO>()
                .ForMember(x => x.BlogCode,
            options => options.MapFrom(source => source.BlogCode))
                 .ForMember(x => x.Id,
            options => options.MapFrom(source => source.Id))
                .ForMember(x => x.CateId,
            options => options.MapFrom(source => source.CateId))
                .ForMember(x => x.Title,
            options => options.MapFrom(source => source.Title))
                .ForMember(x => x.Description,
            options => options.MapFrom(source => source.Description))
                .ForMember(x => x.ShortDescription,
            options => options.MapFrom(source => source.ShortDescription))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ForMember(x => x.Img,
            options => options.MapFrom(source => source.Images))
                .ForMember(x => x.CreatedAt,
            options => options.MapFrom(source => source.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.CreatedBy,
            options => options.MapFrom(source => source.CreatedBy))
                .ForMember(x => x.LastModifiedAt,
            options => options.MapFrom(source => source.UpdateDate.GetValueOrDefault().ToString("dd/MM/yyyy")))
                .ForMember(x => x.LastModifiedBy,
            options => options.MapFrom(source => source.UpdateBy))
                .ReverseMap();


            CreateMap<Blog, CreateBlogDTO>()
                .ForMember(x => x.ShortDescription,
            options => options.MapFrom(source => source.ShortDescription))
                .ForMember(x => x.Title,
            options => options.MapFrom(source => source.Title))
                .ForMember(x => x.Description,
            options => options.MapFrom(source => source.Description))
                .ForMember(x => x.Img,
            options => options.MapFrom(source => source.Images))
                .ReverseMap();

            CreateMap<Blog, UpdateBlogDTO>()
                 .ForMember(x => x.ShortDescription,
            options => options.MapFrom(source => source.ShortDescription))
                .ForMember(x => x.Title,
            options => options.MapFrom(source => source.Title))
                .ForMember(x => x.Description,
            options => options.MapFrom(source => source.Description))
                .ForMember(x => x.Img,
            options => options.MapFrom(source => source.Images))
                .ForMember(x => x.Status,
            options => options.MapFrom(source => source.Status))
                .ReverseMap();

            CreateMap<CateBlog, ViewCateBlogDTO>()
                .ReverseMap();
        }
    }
}
