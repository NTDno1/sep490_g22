﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class ReportDTO
    {
        public int TotalClass { get; set; }
        public int ClassInProgess { get; set; }
        public int ClassIsFinished { get; set; }
        public int ClassIsClosed { get; set; }

        public int TotalClassOffMonth { get; set; }
        public int ClassInProgessOffMonth { get; set; }
        public int ClassIsFinishedOffMonth { get; set; }
        public int ClassIsClosedOffMonth { get; set; }

        public int TotalStudent { get; set; }   
        public int StudentInProgess { get; set; }
        public int StudentIsFinished { get; set; }
        public int StudentIsClosed { get; set; }

        public int TotalStudentOffMonth { get; set; }
        public int StudentInProgessOffMonth { get; set; }
        public int StudentIsFinishedOffMonth { get; set; }
        public int StudentIsClosedOffMonth { get; set; }
        
        public int TeacherInProgess { get; set; }
        public int TeacherCanceled { get; set; }

        public int EmployeeInProgess { get; set; }
        public int EmployeeCanceled { get; set; }
    }
}
