﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{

    public class CreateBlogDTO
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        public string? Description { get; set; }
        public int CateId { get; set; }
        public string? Img { get; set; }
        public int? Position { get; set; }
        public string? Url { get; set; }
    }
}
