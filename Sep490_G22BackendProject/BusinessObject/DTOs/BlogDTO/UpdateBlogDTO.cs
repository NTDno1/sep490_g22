﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class UpdateBlogDTO
    {
        public string? Title { get; set; }
        public string? ShortDescription { get; set; }
        public string? Description { get; set; }
        public string? Img { get; set; }
        public int? Status { get; set; } = 1;
        public int? Position { get; set; }
        public string? Url { get; set; }
    }
}
