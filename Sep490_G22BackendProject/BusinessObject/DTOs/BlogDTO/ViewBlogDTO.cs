﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class ViewBlogDTO
    {
        public int Id { get; set; }
        public Guid? BlogCode { get; set; }
        public int? CateId { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ShortDescription { get; set; }
        public int? Status { get; set; }
        public string? Img { get; set; }
        public string? CreatedBy { get; set; }
        public string? CreatedAt { get; set; }
        public string? LastModifiedBy { get; set; }
        public string? LastModifiedAt { get; set; }
        public int? Position { get; set; }
        public string? Url { get; set; }
    }
}
