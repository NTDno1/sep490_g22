﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class ApiResponse
    {

        public ApiResponse()
        {
            ErrorMessages = new List<string>();
        }
        public HttpStatusCode StatusCode { get; set; }
        public bool IsSuccess { get; set; } = true;
        public List<string> ErrorMessages { get; set; }
        public object Result { get; set; }

        public String Message { get; set; }
        public string? SuccessMessage { get; set; }


        public TokenModel TokenInformation { get; set; }

    }
}
