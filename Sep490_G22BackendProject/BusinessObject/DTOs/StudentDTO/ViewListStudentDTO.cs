﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.Student
{
    public class ViewListStudentDTO
    {
        private Guid _id;
        public string StudentCode { get; private set; }
        public Guid Id
        {
            get => _id;
            set
            {
                _id = value;
                StudentCode = value.ToString().Substring(value.ToString().Length - 6);
            }
        }
        public string? FirstName { get; set; }
        public string? MidName { get; set; }
        public string? LastName { get; set; }
        public string? FullName { get; set; }
        public string? Updater { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Image { get; set; }
        public DateTime? Dob { get; set; }
        public int? Status { get; set; }
        public int? Gender { get; set; }
        public string? Cccd { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? EmoloyeeId { get; set; }
        public DateTime? DateCreate { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }
        public string? Verfify { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}
