﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.Student
{
    public class UpdateStudentDTOs
    {
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? MidName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string? Cccd { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? EmoloyeeId { get; set; }
    }
}
