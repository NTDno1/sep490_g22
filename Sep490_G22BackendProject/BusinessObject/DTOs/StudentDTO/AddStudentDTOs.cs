﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
namespace BusinessObject.DTOs.Student
{
    public class AddStudentDTOs
    {
        public string? UserName { get; set; }
        public string? FirstName { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string? Cccd { get; set; }
    }
}
