﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.RoomDTO
{
    public class AddRoomDTO
    {
        public string? Name { get; set; }
        public int? Status { get; set; } = 1;
    }
}
