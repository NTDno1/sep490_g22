﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.StudentClassDTO
{
    public class ViewStudentClassDTO
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public int Status { get; set; }
        public int TotalSlot { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public DateTime EndDate { get; set; }
        public string? Note { get; set; }
        public string? ClassName { get; set; }
    }
    public class ExtendedStudentClass : StudentClass
    {
        public string? ClassName { get; set; }
    }
}
