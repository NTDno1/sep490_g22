﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.TeacherDTO
{
    public class ViewTeacherDTO
    {
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? MidName { get; set; }
        public string? LastName { get; set; }
        public string? FullName { get; set; }
        public string? Updater { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public string? Image { get; set; }
        public Guid? CenterId { get; set; }
        public string? CertificateName { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string? ImageCertificateName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Status { get; set; }
    }
}
