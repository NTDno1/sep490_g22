﻿using BusinessObject.DTOs.SlotDateDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.SchedualDTO
{
    public class ViewSchedualDTO
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public string? ClassName { get; set; }
        public string? Code { get; set; }
        public int? CourceId { get; set; }
        public string? CourseName { get; set; }
        public Guid? TeacherId { get; set; }
        public string? TeacherName { get; set; }
        public string? AdminCenterName { get; set; }
        public int? Status { get; set; }
        public int TotalSlot { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; } = null!;
        public int? SlotNum { get; set; }
        public DateTime? DateOffSlot { get; set; }
        public int? Staus { get; set; }
        public string? RoomName { get; set; }
        public string? StatusOfSchedule { get; set; }
    }
}
