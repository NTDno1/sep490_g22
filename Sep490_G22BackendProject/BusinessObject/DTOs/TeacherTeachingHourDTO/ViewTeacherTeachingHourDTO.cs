﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.TeacherTeachingHourDTO
{
    public class ViewTeacherTeachingHourDTO
    {
        public int Id { get; set; }
        public Guid? TeacherId { get; set; }
        public int? ScheduleId { get; set; }
        public int? TimeTeaching { get; set; }
        public string? Note { get; set; }
        public string? TeacherName { get; set; }
        public int ClassId { get; set; }
        public string? ClassName { get; set; }
        public int SlotNum { get; set; }
        public DateTime DateOffSlot { get; set; }
        public int SlotDateId { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; } = null!;
    }
}
