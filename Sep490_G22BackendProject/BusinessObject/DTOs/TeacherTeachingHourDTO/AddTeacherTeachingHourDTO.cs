﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.TeacherTeachingHourDTO
{
    public class AddTeacherTeachingHourDTO
    {
        public int Id { get; set; }
        public Guid? TeacherId { get; set; }
        public int? ScheduleId { get; set; }
        public int? TimeTeaching { get; set; }
        public string? Note { get; set; }
    }
}
