﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.SQADTO
{
    public class UpdateSqaDTOs
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Desc { get; set; }
        //[JsonIgnore]
        //public string? Image { get; set; }
    }
}
