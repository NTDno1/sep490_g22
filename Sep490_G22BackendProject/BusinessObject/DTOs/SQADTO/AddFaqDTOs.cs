﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Text.Json.Serialization;


namespace BusinessObject.DTOs.SQADTO
{
    public class AddFaqDTOs
    {
        public string? Title { get; set; }
        public string? Desc { get; set; }
        //[JsonIgnore]
        //public string? Image { get; set; }
    }
}
