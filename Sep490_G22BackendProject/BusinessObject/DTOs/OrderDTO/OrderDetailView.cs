﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.OrderDTO
{
    public class OrderDetailView
    {
        public int Id { get; set; }
        public int? CourseId { get; set; }
        public int? OrderDetailId { get; set; }
        public Guid? StudentID { get; set; }
    }
}
