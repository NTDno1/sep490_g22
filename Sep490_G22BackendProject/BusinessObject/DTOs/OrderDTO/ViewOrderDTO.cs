﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.OrderDTO
{
    public class ViewOrderDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Title { get; set; }
        public string? OrderCode { get; set; }
        public string? Note { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public Guid? StudentId { get; set; }
        public string? StudentName { get; set; }
        public Guid? EmployeeId { get; set; }
        public string? EmployeeName { get; set; }
        public double? TotalCourse { get; set; }
    }
}
