﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.OrderDTO
{
    public class ViewOrderDetailDTO
    {
        public int Id { get; set; }
        public int? CourseId { get; set; }
        public string? CourseName { get; set; }
        public double? Price { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Discount { get; set; }
        public string? Note { get; set; }
        public int? OrderDetailId { get; set; }
        public int? Status { get; set; }
        public int? ClassId { get; set; }
        public string? ClassName { get; set; }
        public int? StatusPass { get; set; }
    }
}
