﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.OrderDTO
{
    public class AddOrderDTO
    {
        public string? Name { get; set; }
        public string? Title { get; set; }
        public string? Note { get; set; }
        public Guid? StudentId { get; set; }
    }
}
