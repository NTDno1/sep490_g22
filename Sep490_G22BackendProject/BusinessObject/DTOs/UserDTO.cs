﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace BusinessObject.DTOs
{
    public class UserDTO
    {
        public String username { get; set; }
        public String password { get; set; }
        [JsonIgnore]
        public String? centerId { get; set; }
        public String role { get; set; }
    }
}
