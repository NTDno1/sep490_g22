﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Syllabus
    {
        public Syllabus()
        {
            Courses = new HashSet<Course>();
        }

        public int Id { get; set; }
        public string? SyllabusName { get; set; }
        public string? Description { get; set; }
        public string? TimeAllocation { get; set; }
        public string? StudentTasks { get; set; }
        public string? CourseObjectives { get; set; }
        public string? Exam { get; set; }
        public string? SubjectCode { get; set; }
        public string? Contact { get; set; }
        public string? CourseLearningOutcome { get; set; }
        public string? MainTopics { get; set; }
        public string? RequiredKnowledge { get; set; }
        public string? LinkBook { get; set; }
        public string? LinkDocument { get; set; }
        public int? Status { get; set; }
        public Guid CenterId { get; set; }

        public virtual Center Center { get; set; } = null!;
        public virtual ICollection<Course> Courses { get; set; }
    }
}
