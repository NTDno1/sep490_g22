﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class TeacherTeachingHour
    {
        public int Id { get; set; }
        public Guid? TeacherId { get; set; }
        public int? ScheduleId { get; set; }
        public int? TimeTeaching { get; set; }
        public string? Note { get; set; }

        public virtual Schedual? Schedule { get; set; }
        public virtual TeacherProfile? Teacher { get; set; }
    }
}
