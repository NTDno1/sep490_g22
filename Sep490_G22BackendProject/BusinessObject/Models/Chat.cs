﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Chat
    {
        public Chat()
        {
            MemberChats = new HashSet<MemberChat>();
        }

        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Type { get; set; }
        public string? Data { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<MemberChat> MemberChats { get; set; }
    }
}
