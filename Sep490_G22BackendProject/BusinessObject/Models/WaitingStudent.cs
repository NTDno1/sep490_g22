﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class WaitingStudent
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Guid? StudentId { get; set; }
        public Guid? EmployeeId { get; set; }

        public virtual Employee? Employee { get; set; }
        public virtual Order? Order { get; set; }
        public virtual StudentProfile? Student { get; set; }
    }
}
