﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class TeacherProfile
    {
        public TeacherProfile()
        {
            Classes = new HashSet<Class>();
            TeacherTeachingHours = new HashSet<TeacherTeachingHour>();
        }

        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? MidName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public string? Image { get; set; }
        public Guid? CenterId { get; set; }
        public string? CertificateName { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string? ImageCertificateName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? RefreshToken { get; set; }
        public string? Verfify { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }
        public int? Status { get; set; }

        public virtual Center? Center { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<TeacherTeachingHour> TeacherTeachingHours { get; set; }
    }
}
