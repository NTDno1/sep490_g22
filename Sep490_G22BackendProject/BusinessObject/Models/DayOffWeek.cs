﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class DayOffWeek
    {
        public int Id { get; set; }
        public string? Day { get; set; }
    }
}
