﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Attendance
    {
        public int Id { get; set; }
        public int? StudentInClass { get; set; }
        public int? ScheduleId { get; set; }

    }
}
