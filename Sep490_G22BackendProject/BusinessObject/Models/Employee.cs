﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Classes = new HashSet<Class>();
            Courses = new HashSet<Course>();
            Notifications = new HashSet<Notification>();
            Orders = new HashSet<Order>();
            Saqs = new HashSet<Saq>();
            StudentProfiles = new HashSet<StudentProfile>();
        }

        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Adress { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? AccountId { get; set; }
        public Guid? RoleId { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }
        public string? RefreshToken { get; set; }
        public string? Verfify { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? Status { get; set; }

        public virtual Account? Account { get; set; }
        public virtual Center? Center { get; set; }
        public virtual Role? Role { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Saq> Saqs { get; set; }
        public virtual ICollection<StudentProfile> StudentProfiles { get; set; }
    }
}
