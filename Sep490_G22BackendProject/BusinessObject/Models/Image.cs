﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Image
    {
        public Image()
        {
            Blogs = new HashSet<Blog>();
            Students = new HashSet<StudentProfile>();
        }

        public int Id { get; set; }
        public string? Src { get; set; }
        public byte[]? Image1 { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<StudentProfile> Students { get; set; }
    }
}
