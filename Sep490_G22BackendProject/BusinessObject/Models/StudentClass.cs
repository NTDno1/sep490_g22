﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class StudentClass
    {
        public StudentClass()
        {
            ExamClasses = new HashSet<ExamClass>();
        }

        public int Id { get; set; }
        public int? ClassId { get; set; }
        public Guid? StudentId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string? Note { get; set; }

        public virtual Class? Class { get; set; }
        public virtual StudentProfile? Student { get; set; }
        public virtual ICollection<ExamClass> ExamClasses { get; set; }
    }
}
