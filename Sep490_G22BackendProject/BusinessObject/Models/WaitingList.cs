﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class WaitingList
    {
        public Guid StudentId { get; set; }
        public Guid EmployeeId { get; set; }
        public int CourseId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? Note { get; set; }

        public virtual Course Course { get; set; } = null!;
        public virtual Employee Employee { get; set; } = null!;
        public virtual StudentProfile Student { get; set; } = null!;
    }
}
