﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Saq
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Desc { get; set; }
        public string? Image { get; set; }
        public Guid? EmployeeId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual Employee? Employee { get; set; }
    }
}
