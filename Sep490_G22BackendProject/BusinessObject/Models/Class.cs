﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Class
    {
        public Class()
        {
            ClassSlotDates = new HashSet<ClassSlotDate>();
            NotificationClases = new HashSet<NotificationClase>();
            OrderDetails = new HashSet<OrderDetail>();
            Scheduals = new HashSet<Schedual>();
            StudentClasses = new HashSet<StudentClass>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public int? CourceId { get; set; }
        public Guid? AdminCenterId { get; set; }
        public Guid? TeacherId { get; set; }
        public Guid? CenterId { get; set; }
        public int? NotificationId { get; set; }
        public int? Status { get; set; }
        public int? SlotId { get; set; }
        public int? SlotNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual Employee? AdminCenter { get; set; }
        public virtual Center? Center { get; set; }
        public virtual Course? Cource { get; set; }
        public virtual TeacherProfile? Teacher { get; set; }
        public virtual ICollection<ClassSlotDate> ClassSlotDates { get; set; }
        public virtual ICollection<NotificationClase> NotificationClases { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<Schedual> Scheduals { get; set; }
        public virtual ICollection<StudentClass> StudentClasses { get; set; }
    }
}
