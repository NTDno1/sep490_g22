﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class OrderDetail
    {
        public int Id { get; set; }
        public int? CourseId { get; set; }
        public double? Price { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Discount { get; set; }
        public string? Note { get; set; }
        public int? OrderId { get; set; }
        public int? Status { get; set; }
        public int? ClassId { get; set; }
        public int? StatusPass { get; set; }

        public virtual Class? Class { get; set; }
        public virtual Course? Course { get; set; }
        public virtual Order? Order { get; set; }
    }
}
