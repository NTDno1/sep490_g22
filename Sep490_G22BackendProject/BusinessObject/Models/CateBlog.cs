﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class CateBlog
    {
        public CateBlog()
        {
            Blogs = new HashSet<Blog>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int? Level { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
    }
}
