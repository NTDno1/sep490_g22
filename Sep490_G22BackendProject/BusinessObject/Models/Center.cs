﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Center
    {
        public Center()
        {
            Blogs = new HashSet<Blog>();
            ClassRooms = new HashSet<ClassRoom>();
            Classes = new HashSet<Class>();
            Courses = new HashSet<Course>();
            Employees = new HashSet<Employee>();
            SlotDates = new HashSet<SlotDate>();
            Syllabi = new HashSet<Syllabus>();
            TeacherProfiles = new HashSet<TeacherProfile>();
        }

        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Adress { get; set; }
        public string? CodeCenter { get; set; }
        public Guid? AdminId { get; set; }
        public string? NumberPhone { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Status { get; set; }

        public virtual Account? Admin { get; set; }
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<ClassRoom> ClassRooms { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<SlotDate> SlotDates { get; set; }
        public virtual ICollection<Syllabus> Syllabi { get; set; }
        public virtual ICollection<TeacherProfile> TeacherProfiles { get; set; }
    }
}
