﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BusinessObject.Models
{
    public partial class sep490_g22Context : DbContext
    {
        public sep490_g22Context()
        {
        }

        public sep490_g22Context(DbContextOptions<sep490_g22Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Atendence> Atendences { get; set; } = null!;
        public virtual DbSet<Blog> Blogs { get; set; } = null!;
        public virtual DbSet<CateBlog> CateBlogs { get; set; } = null!;
        public virtual DbSet<Center> Centers { get; set; } = null!;
        public virtual DbSet<Class> Classes { get; set; } = null!;
        public virtual DbSet<ClassRoom> ClassRooms { get; set; } = null!;
        public virtual DbSet<ClassSlotDate> ClassSlotDates { get; set; } = null!;
        public virtual DbSet<Course> Courses { get; set; } = null!;
        public virtual DbSet<Employee> Employees { get; set; } = null!;
        public virtual DbSet<Exam> Exams { get; set; } = null!;
        public virtual DbSet<ExamClass> ExamClasses { get; set; } = null!;
        public virtual DbSet<Notification> Notifications { get; set; } = null!;
        public virtual DbSet<NotificationClase> NotificationClases { get; set; } = null!;
        public virtual DbSet<Order> Orders { get; set; } = null!;
        public virtual DbSet<OrderDetail> OrderDetails { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Saq> Saqs { get; set; } = null!;
        public virtual DbSet<Schedual> Scheduals { get; set; } = null!;
        public virtual DbSet<SlotDate> SlotDates { get; set; } = null!;
        public virtual DbSet<StudentClass> StudentClasses { get; set; } = null!;
        public virtual DbSet<StudentProfile> StudentProfiles { get; set; } = null!;
        public virtual DbSet<Syllabus> Syllabi { get; set; } = null!;
        public virtual DbSet<TeacherProfile> TeacherProfiles { get; set; } = null!;
        public virtual DbSet<TeacherTeachingHour> TeacherTeachingHours { get; set; } = null!;
        public virtual DbSet<TypeCourse> TypeCourses { get; set; } = null!;
        public virtual DbSet<TypeRoom> TypeRooms { get; set; } = null!;
        public virtual DbSet<WaitingStudent> WaitingStudents { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("server=MSI\\SQLEXPRESS; database=sep490_g22;uid=sa; pwd=123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("Account");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.PassWord).HasMaxLength(500);

                entity.Property(e => e.RefreshToken)
                    .HasColumnType("text")
                    .HasColumnName("refreshToken");

                entity.Property(e => e.RefreshTokenExpired)
                    .HasColumnType("datetime")
                    .HasColumnName("refreshTokenExpired");

                entity.Property(e => e.UserName).HasMaxLength(500);
            });

            modelBuilder.Entity<Atendence>(entity =>
            {
                entity.ToTable("Atendence");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Schedual)
                    .WithMany(p => p.Atendences)
                    .HasForeignKey(d => d.SchedualId)
                    .HasConstraintName("FK_Atendence_SlotClass");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Atendences)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_Atendence_StudentProfile");
            });

            modelBuilder.Entity<Blog>(entity =>
            {
                entity.ToTable("Blog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BlogCode).HasColumnName("blog_code");

                entity.Property(e => e.CateId).HasColumnName("cate_id");

                entity.Property(e => e.CenterId).HasColumnName("center_id");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("date")
                    .HasColumnName("created_date");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Images)
                    .HasMaxLength(200)
                    .HasColumnName("images");

                entity.Property(e => e.Position).HasColumnName("position");

                entity.Property(e => e.ShortDescription)
                    .HasMaxLength(500)
                    .HasColumnName("short_description");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Title)
                    .HasMaxLength(500)
                    .HasColumnName("title");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("date")
                    .HasColumnName("update_date");

                entity.Property(e => e.Url)
                    .HasMaxLength(500)
                    .HasColumnName("url");

                entity.HasOne(d => d.Cate)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.CateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BLOG_ID");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_Center_ID");
            });

            modelBuilder.Entity<CateBlog>(entity =>
            {
                entity.ToTable("CateBlog");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasColumnName("description");

                entity.Property(e => e.Level).HasColumnName("level");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name");

                entity.Property(e => e.ParentId).HasColumnName("parent_id");

                entity.Property(e => e.Status).HasColumnName("status");
            });

            modelBuilder.Entity<Center>(entity =>
            {
                entity.ToTable("Center");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Adress).HasMaxLength(500);

                entity.Property(e => e.CodeCenter).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.NumberPhone).HasMaxLength(500);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.Centers)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_Center_Account");
            });

            modelBuilder.Entity<Class>(entity =>
            {
                entity.ToTable("Class");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasMaxLength(500);

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.AdminCenter)
                    .WithMany(p => p.Classes)
                    .HasForeignKey(d => d.AdminCenterId)
                    .HasConstraintName("FK_Class_Employee");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Classes)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_Class_Center");

                entity.HasOne(d => d.Cource)
                    .WithMany(p => p.Classes)
                    .HasForeignKey(d => d.CourceId)
                    .HasConstraintName("FK_Class_Course1");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.Classes)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_Class_TeacherProfile1");
            });

            modelBuilder.Entity<ClassRoom>(entity =>
            {
                entity.ToTable("ClassRoom");

                entity.Property(e => e.Code).HasMaxLength(500);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.UpdateBy).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.ClassRooms)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_ClassRoom_Center");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.ClassRooms)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_ClassRoom_TypeRoom");
            });

            modelBuilder.Entity<ClassSlotDate>(entity =>
            {
                entity.ToTable("ClassSlotDate");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.ClassSlotDates)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassSlotDate_Class");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.ClassSlotDates)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassSlotDate_ClassRoom");

                entity.HasOne(d => d.Slot)
                    .WithMany(p => p.ClassSlotDates)
                    .HasForeignKey(d => d.SlotId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassSlotDate_Slot");
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("Course");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasMaxLength(500);

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_Course_Center");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_Course_Employee");

                entity.HasOne(d => d.Syllabus)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.SyllabusId)
                    .HasConstraintName("FK_Course_Syllabus");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Adress).HasMaxLength(500);

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.PassWord).HasMaxLength(500);

                entity.Property(e => e.PhoneNumber).HasMaxLength(500);

                entity.Property(e => e.RefreshToken)
                    .HasColumnType("text")
                    .HasColumnName("refreshToken");

                entity.Property(e => e.RefreshTokenExpired)
                    .HasColumnType("datetime")
                    .HasColumnName("refreshTokenExpired");

                entity.Property(e => e.UserName).HasMaxLength(500);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_Employee_Account");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_Employee_Center");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Employee_Role");
            });

            modelBuilder.Entity<Exam>(entity =>
            {
                entity.ToTable("Exam");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Average).HasColumnName("average");

                entity.Property(e => e.Desc).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Cource)
                    .WithMany(p => p.Exams)
                    .HasForeignKey(d => d.CourceId)
                    .HasConstraintName("FK_Exam_Course");
            });

            modelBuilder.Entity<ExamClass>(entity =>
            {
                entity.HasKey(e => new { e.ExamId, e.StudentClassId });

                entity.ToTable("ExamClass");

                entity.Property(e => e.StudentClassId).HasColumnName("student_class_id");

                entity.Property(e => e.Evaluate).HasColumnType("text");

                entity.HasOne(d => d.Exam)
                    .WithMany(p => p.ExamClasses)
                    .HasForeignKey(d => d.ExamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExamClass_Exam1");

                entity.HasOne(d => d.StudentClass)
                    .WithMany(p => p.ExamClasses)
                    .HasForeignKey(d => d.StudentClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExamClass_StudentClass");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("Notification");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_Notification_Employee");
            });

            modelBuilder.Entity<NotificationClase>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Datecreate).HasColumnType("date");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.NotificationClases)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_NotificationClases_Class");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OrderCode).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_OrderDetail_Employee");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_OrderDetail_StudentProfile");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.ToTable("OrderDetail");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_Order_Class");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_Order_Course");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_Order_OrderDetail");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<Saq>(entity =>
            {
                entity.ToTable("SAQ");

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Desc).HasMaxLength(500);

                entity.Property(e => e.Image)
                    .HasMaxLength(500)
                    .HasColumnName("image");

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Saqs)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_SAQ_Employee");
            });

            modelBuilder.Entity<Schedual>(entity =>
            {
                entity.ToTable("Schedual");

                entity.Property(e => e.DateOffSlot)
                    .HasColumnType("date")
                    .HasColumnName("dateOffSlot");

                entity.Property(e => e.StatusOfSchedule).HasMaxLength(50);

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Scheduals)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Schedual_Class");

                entity.HasOne(d => d.RoomIdUpDateNavigation)
                    .WithMany(p => p.Scheduals)
                    .HasForeignKey(d => d.RoomIdUpDate)
                    .HasConstraintName("FK_Schedual_ClassRoom");

                entity.HasOne(d => d.SlotDate)
                    .WithMany(p => p.Scheduals)
                    .HasForeignKey(d => d.SlotDateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SlotClass_SlotDate");
            });

            modelBuilder.Entity<SlotDate>(entity =>
            {
                entity.ToTable("SlotDate");

                entity.Property(e => e.Day).HasMaxLength(50);

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.SlotDates)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_SlotDate_Center");
            });

            modelBuilder.Entity<StudentClass>(entity =>
            {
                entity.ToTable("StudentClass");

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.StudentClasses)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_StudentClass_Class1");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentClasses)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_StudentClass_StudentProfile");
            });

            modelBuilder.Entity<StudentProfile>(entity =>
            {
                entity.ToTable("StudentProfile");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Cccd).HasMaxLength(50);

                entity.Property(e => e.DateCreate).HasColumnType("date");

                entity.Property(e => e.DateUpdate).HasColumnType("date");

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.FirstName).HasMaxLength(500);

                entity.Property(e => e.Image).HasMaxLength(500);

                entity.Property(e => e.LastName).HasMaxLength(500);

                entity.Property(e => e.MidName).HasMaxLength(500);

                entity.Property(e => e.PassWord).HasMaxLength(500);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.RefreshToken)
                    .HasColumnType("text")
                    .HasColumnName("refreshToken");

                entity.Property(e => e.RefreshTokenExpired)
                    .HasColumnType("datetime")
                    .HasColumnName("refreshTokenExpired");

                entity.Property(e => e.UserName).HasMaxLength(500);

                entity.HasOne(d => d.Emoloyee)
                    .WithMany(p => p.StudentProfiles)
                    .HasForeignKey(d => d.EmoloyeeId)
                    .HasConstraintName("FK_StudentProfile_Employee");
            });

            modelBuilder.Entity<Syllabus>(entity =>
            {
                entity.ToTable("Syllabus");

                entity.Property(e => e.LinkBook).HasMaxLength(500);

                entity.Property(e => e.LinkDocument).HasMaxLength(500);

                entity.Property(e => e.SyllabusName).HasMaxLength(500);

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Syllabi)
                    .HasForeignKey(d => d.CenterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Syllabus_Center");
            });

            modelBuilder.Entity<TeacherProfile>(entity =>
            {
                entity.ToTable("TeacherProfile");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CertificateName).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.FirstName).HasMaxLength(500);

                entity.Property(e => e.Image).HasMaxLength(500);

                entity.Property(e => e.ImageCertificateName).HasMaxLength(500);

                entity.Property(e => e.LastName).HasMaxLength(500);

                entity.Property(e => e.MidName).HasMaxLength(500);

                entity.Property(e => e.PassWord).HasMaxLength(500);

                entity.Property(e => e.PhoneNumber).HasMaxLength(500);

                entity.Property(e => e.RefreshToken)
                    .HasColumnType("text")
                    .HasColumnName("refreshToken");

                entity.Property(e => e.RefreshTokenExpired)
                    .HasColumnType("datetime")
                    .HasColumnName("refreshTokenExpired");

                entity.Property(e => e.ReleaseDate)
                    .HasColumnType("date")
                    .HasColumnName("Release date");

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.Property(e => e.UserName).HasMaxLength(500);

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.TeacherProfiles)
                    .HasForeignKey(d => d.CenterId)
                    .HasConstraintName("FK_TeacherProfile_Center");
            });

            modelBuilder.Entity<TeacherTeachingHour>(entity =>
            {
                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Schedule)
                    .WithMany(p => p.TeacherTeachingHours)
                    .HasForeignKey(d => d.ScheduleId)
                    .HasConstraintName("FK_TeacherTeachingHours_Schedual");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.TeacherTeachingHours)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_TeacherTeachingHours_TeacherProfile");
            });

            modelBuilder.Entity<TypeCourse>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TypeCourse");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Course)
                    .WithMany()
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_TypeCourse_Course");
            });

            modelBuilder.Entity<TypeRoom>(entity =>
            {
                entity.ToTable("TypeRoom");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("date")
                    .HasColumnName("created_date");

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("date")
                    .HasColumnName("update_date");
            });

            modelBuilder.Entity<WaitingStudent>(entity =>
            {
                

                entity.ToTable("WaitingStudent");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Employee)
                    .WithMany()
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_WaitingStudent_Employee");

                entity.HasOne(d => d.Order)
                    .WithMany()
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_WaitingStudentt_OrderDetail");

                entity.HasOne(d => d.Student)
                    .WithMany()
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_WaitingStudent_StudentProfile");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
