﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class TypeCourse
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int? CourseId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual Course? Course { get; set; }
    }
}
