﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Atendence
    {
        public int Id { get; set; }
        public Guid? StudentId { get; set; }
        public int? Status { get; set; }
        public int? SchedualId { get; set; }
        public string? Note { get; set; }

        public virtual Schedual? Schedual { get; set; }
        public virtual StudentProfile? Student { get; set; }
    }
}
