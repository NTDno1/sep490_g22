﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class EventChat
    {
        public int? Id { get; set; }
        public int? ChatId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual Chat? Chat { get; set; }
    }
}
