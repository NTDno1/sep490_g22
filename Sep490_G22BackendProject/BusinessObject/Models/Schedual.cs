﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Schedual
    {
        public Schedual()
        {
            Atendences = new HashSet<Atendence>();
            TeacherTeachingHours = new HashSet<TeacherTeachingHour>();
        }

        public int Id { get; set; }
        public int ClassId { get; set; }
        public int SlotNum { get; set; }
        public string? StatusOfSchedule { get; set; }
        public DateTime DateOffSlot { get; set; }
        public int Staus { get; set; }
        public int SlotDateId { get; set; }
        public int? RoomIdUpDate { get; set; }

        public virtual Class Class { get; set; } = null!;
        public virtual ClassRoom? RoomIdUpDateNavigation { get; set; }
        public virtual SlotDate SlotDate { get; set; } = null!;
        public virtual ICollection<Atendence> Atendences { get; set; }
        public virtual ICollection<TeacherTeachingHour> TeacherTeachingHours { get; set; }
    }
}
