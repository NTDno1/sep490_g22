﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class CommnentBlog
    {
        public long Id { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public int? Star { get; set; }
        public int? UserRate { get; set; }
        public long? ReplyComment { get; set; }
        public int? UserCommnent { get; set; }
        public int? BlogId { get; set; }

        public virtual Blog? Blog { get; set; }
    }
}
