﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class SlotDate
    {
        public SlotDate()
        {
            ClassSlotDates = new HashSet<ClassSlotDate>();
            Scheduals = new HashSet<Schedual>();
        }

        public int Id { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public string Day { get; set; } = null!;
        public Guid? CenterId { get; set; }

        public virtual Center? Center { get; set; }
        public virtual ICollection<ClassSlotDate> ClassSlotDates { get; set; }
        public virtual ICollection<Schedual> Scheduals { get; set; }
    }
}
