﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class MemberChat
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ChatId { get; set; }
        public string? Type { get; set; }
        public string? Data { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual Chat? Chat { get; set; }
    }
}
