﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class TypeRoom
    {
        public TypeRoom()
        {
            ClassRooms = new HashSet<ClassRoom>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? CreatedBy { get; set; }
        public string? UpdateBy { get; set; }

        public virtual ICollection<ClassRoom> ClassRooms { get; set; }
    }
}
