﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class BackOfficeProfile
    {
        public BackOfficeProfile()
        {
            WaitingLists = new HashSet<WaitingList>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }

        public virtual Account IdNavigation { get; set; } = null!;
        public virtual ICollection<WaitingList> WaitingLists { get; set; }
    }
}
