﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Bill
    {
        public int UserId { get; set; }
        public int BillId { get; set; }

        public virtual BillDetail BillNavigation { get; set; } = null!;
        public virtual StudentProfile User { get; set; } = null!;
    }
}
