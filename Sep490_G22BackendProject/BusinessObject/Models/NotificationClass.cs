﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class NotificationClass
    {
        public NotificationClass()
        {
            Classes = new HashSet<Class>();
        }

        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public int? ClassId { get; set; }
        public Guid? StudentId { get; set; }
        public double? ViewCount { get; set; }
        public DateTime? Datecreate { get; set; }
        public DateTime? DateUpdate { get; set; }
public virtual ICollection<Class> Classes { get; set; }
    }
}
