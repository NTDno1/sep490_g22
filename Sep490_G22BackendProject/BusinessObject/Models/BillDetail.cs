﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class BillDetail
    {
        public BillDetail()
        {
            Students = new HashSet<StudentProfile>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? Userid { get; set; }
        public int? CourceId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? ClassId { get; set; }

        public virtual Course? Cource { get; set; }

        public virtual ICollection<StudentProfile> Students { get; set; }
    }
}
