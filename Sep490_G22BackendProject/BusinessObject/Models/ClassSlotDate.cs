﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class ClassSlotDate
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int SlotId { get; set; }
        public int RoomId { get; set; }
        public Guid TeacherId { get; set; }
        public int? Uid { get; set; }

        public virtual Class Class { get; set; } = null!;
        public virtual ClassRoom Room { get; set; } = null!;
        public virtual SlotDate Slot { get; set; } = null!;
    }
}
