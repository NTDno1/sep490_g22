﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Transaction
    {
        public Transaction()
        {
            Wallets = new HashSet<Wallet>();
            Images = new HashSet<Image>();
        }

        public int Id { get; set; }
        public decimal? Price { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual ICollection<Wallet> Wallets { get; set; }

        public virtual ICollection<Image> Images { get; set; }
    }
}
