﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Account
    {
        public Account()
        {
            Centers = new HashSet<Center>();
            Employees = new HashSet<Employee>();
        }

        public Guid Id { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public int RoleId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string? RefreshToken { get; set; }
        public string? Verfify { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }

        public virtual ICollection<Center> Centers { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
