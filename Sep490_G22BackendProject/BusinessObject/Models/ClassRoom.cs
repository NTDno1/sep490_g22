﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class ClassRoom
    {
        public ClassRoom()
        {
            ClassSlotDates = new HashSet<ClassSlotDate>();
            Scheduals = new HashSet<Schedual>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public int? TypeId { get; set; }
        public Guid? CenterId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? CreatedBy { get; set; }
        public string? UpdateBy { get; set; }
        public int? Status { get; set; }

        public virtual Center? Center { get; set; }
        public virtual TypeRoom? Type { get; set; }
        public virtual ICollection<ClassSlotDate> ClassSlotDates { get; set; }
        public virtual ICollection<Schedual> Scheduals { get; set; }
    }
}
