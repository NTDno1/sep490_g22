﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Course
    {
        public Course()
        {
            Classes = new HashSet<Class>();
            Exams = new HashSet<Exam>();
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Code { get; set; }
        public string? Description { get; set; }
        public decimal? Price { get; set; }
        public int? ClassId { get; set; }
        public string? ImageSrc { get; set; }
        public int? BillId { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? EmployeeId { get; set; }
        public int? TypeId { get; set; }
        public int? SyllabusId { get; set; }
        public int? Status { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? Rating { get; set; }

        public virtual Center? Center { get; set; }
        public virtual Employee? Employee { get; set; }
        public virtual Syllabus? Syllabus { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
