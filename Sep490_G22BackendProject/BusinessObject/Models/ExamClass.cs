﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class ExamClass
    {
        public int ExamId { get; set; }
        public double? Mark { get; set; }
        public int? Rank { get; set; }
        public int StudentClassId { get; set; }
        public string? Evaluate { get; set; }

        public virtual Exam Exam { get; set; } = null!;
        public virtual StudentClass StudentClass { get; set; } = null!;
    }
}
