﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class AccountCenter
    {
        public int StudentId { get; set; }
        public int CenterId { get; set; }
        public int? Status { get; set; }
        public string? Note { get; set; }

        public virtual Center Center { get; set; } = null!;
        public virtual Account Student { get; set; } = null!;
    }
}
