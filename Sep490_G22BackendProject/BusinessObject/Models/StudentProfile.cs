﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class StudentProfile
    {
        public StudentProfile()
        {
            Atendences = new HashSet<Atendence>();
            Orders = new HashSet<Order>();
            StudentClasses = new HashSet<StudentClass>();
        }

        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? MidName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Image { get; set; }
        public DateTime? Dob { get; set; }
        public int? Status { get; set; }
        public int? Gender { get; set; }
        public string? Cccd { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? EmoloyeeId { get; set; }
        public DateTime? DateCreate { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }
        public string? Verfify { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual Employee? Emoloyee { get; set; }
        public virtual ICollection<Atendence> Atendences { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<StudentClass> StudentClasses { get; set; }
    }
}
