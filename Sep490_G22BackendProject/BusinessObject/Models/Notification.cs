﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Notification
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public Guid? EmployeeId { get; set; }
        public double? ViewCount { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual Employee? Employee { get; set; }
    }
}
