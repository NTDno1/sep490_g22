﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Blog
    {
        public int Id { get; set; }
        public int CateId { get; set; }
        public Guid? CenterId { get; set; }
        public Guid? BlogCode { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ShortDescription { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? CreatedBy { get; set; }
        public string? UpdateBy { get; set; }
        public string? Images { get; set; }
        public int? Position { get; set; }
        public string? Url { get; set; }

        public virtual CateBlog Cate { get; set; } = null!;
        public virtual Center? Center { get; set; }
    }
}
