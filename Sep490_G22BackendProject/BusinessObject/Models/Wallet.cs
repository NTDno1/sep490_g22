﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Wallet
    {
        public int UserId { get; set; }
        public string? Code { get; set; }
        public decimal? Ballance { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? BillWalletId { get; set; }

        public virtual Transaction? BillWallet { get; set; }
        public virtual StudentProfile? StudentProfile { get; set; }
    }
}
