﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Title { get; set; }
        public string? OrderCode { get; set; }
        public string? Note { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public Guid? StudentId { get; set; }
        public Guid? EmployeeId { get; set; }

        public virtual Employee? Employee { get; set; }
        public virtual StudentProfile? Student { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
