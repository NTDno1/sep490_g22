﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Exam
    {
        public Exam()
        {
            ExamClasses = new HashSet<ExamClass>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Desc { get; set; }
        public double? Average { get; set; }
        public int? CourceId { get; set; }

        public virtual Course? Cource { get; set; }
        public virtual ICollection<ExamClass> ExamClasses { get; set; }
    }
}
