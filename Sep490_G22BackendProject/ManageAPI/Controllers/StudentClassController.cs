﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.StudentClassDTO;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentClassController : ControllerBase
    {
        private readonly IStudentClassRepositorys _studentClassRepositorys;
        protected ApiResponse _response;

        public StudentClassController(IStudentClassRepositorys studentClassRepositorys)
        {
            _response = new();
            _studentClassRepositorys = studentClassRepositorys;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost("AddStudentClass")]
        //[Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> AddStudentClass(string studentId, string classId, string note)
        {
            try
            {
                (bool success, List<string> messages) check = await _studentClassRepositorys.AddStudentClass(classId, studentId, note);
                if(check.success == false && check.messages != null)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest; 
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.messages;
                    _response.Result = check;
                    return BadRequest(_response);
                }
                _response.Result = $"Added successfully: StudentID = {studentId}, Note = {note}";
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess =true;
                _response.ErrorMessages = check.messages;
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return _response;
            }
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListStudentOffClass")]
        [Authorize(Roles = "Teacher, Employee, Super Admin, Student")]
        public async Task<ActionResult<ApiResponse>> ListStudentOffClass(string classId)
        {
            try
            {
                Console.WriteLine("Đã vào trong danh sách học sinh của lớp học");
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _studentClassRepositorys.ListStudentOffClass(classId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpDelete("DeletedStudentOffClass")]
        // [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeletedStudentOffClass(string classId, string studentId)
        {
            try
            {
                
                (bool success, List<string> message) check = await _studentClassRepositorys.deleteStudentOffClass(classId, studentId);
                if(check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true; 
                    _response.ErrorMessages = check.message;
                    return Ok(_response);
                }
                else
                {
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.StatusCode=HttpStatusCode.BadRequest;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteListStudentOffClass")]
        // [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeleteListStudentOffClass(List<ViewStudentClassDTO> listStudentClass)
        {
            try
            {

                (bool success, List<string> message) check = await _studentClassRepositorys.deleteListStudentClass(listStudentClass);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
    }
}
