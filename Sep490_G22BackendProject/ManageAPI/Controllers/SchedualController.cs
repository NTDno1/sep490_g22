﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchedualController : ControllerBase
    {

        private readonly ISchedualRepositorys _schedualRepositorys;
        protected ApiResponse _response;
        private readonly IClassRepositorys _classRepositorys;

        public SchedualController(IClassRepositorys classRepositorys, ISchedualRepositorys schedualRepositorys)
        {
            _response = new();
            _schedualRepositorys= schedualRepositorys;
            _classRepositorys = classRepositorys;
        }
        [HttpGet("ListSchedual")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> ListSchedual(string? MM_yyyy)
        {
            try
            {
                var centerId =  HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                _response.Result = await _schedualRepositorys.ViewSchedualByMonth(MM_yyyy, centerId);
                _response.StatusCode = HttpStatusCode.OK;
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ClassSchedual")]
        [Authorize(Roles = "Teacher, Super Admin, Employee, Student")]
        public async Task<ActionResult<ApiResponse>> ClassSchedual(string classId)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _schedualRepositorys.ViewClassSchedual(classId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("TeacherClassSchedual")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> TeacherClassSchedual()
        {
            try
            {
                //addClassDTOs.AdminCenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                //addClassDTOs.CenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                var teacherID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                if(teacherID == null)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = new List<string>() { "Chưa Đăng Nhập" }; 
                }
                _response.Result = await _schedualRepositorys.ViewTeacherClassSchedual(teacherID);
                _response.StatusCode = HttpStatusCode.OK;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("StudenClassSchedual")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> StudenClassSchedual()
        {
            try
            {
                var teacherID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                if (teacherID == null)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = new List<string>() { "Chưa Đăng Nhập" };
                }
                _response.Result = await _schedualRepositorys.ViewStudentClassSchedual(teacherID);
                _response.StatusCode = HttpStatusCode.OK;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("GenerateSchedual")]
        [Authorize(Roles = "Teacher, Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> GenerateSchedual(string classId)
        {
            try
            {
                ViewClassDTO viewClassDTO = await _classRepositorys.ClassDetail(classId, null);
                var addClassLostDate = await _schedualRepositorys.GenerateSchedule(viewClassDTO.classSlotDates, viewClassDTO.SlotNumber, viewClassDTO.StartDate, viewClassDTO.Id);
                await _schedualRepositorys.CreateSlotClass(addClassLostDate);
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = addClassLostDate;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("UpdateSchedule")]
        [Authorize(Roles = "Teacher, Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateSchedule(string scheduleId, string slotDate, string dateChange, string roomId)
        {
            try
            {
                (bool success, List<string> message) addClassLostDate = await _schedualRepositorys.UpdateSchedule(scheduleId, slotDate, dateChange, roomId);
                if(addClassLostDate.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = addClassLostDate.message;
                    _response.Message = addClassLostDate.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Result = addClassLostDate.message;
                    _response.Message = addClassLostDate.message.First();
                    _response.ErrorMessages = addClassLostDate.message;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
    }
}
