﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlotDateController : ControllerBase
    {
        private readonly ISlotDateRepositorys _slotDateRepositorys;
        protected ApiResponse _response;

        public SlotDateController(ISlotDateRepositorys slotDateRepositorys)
        {
            _response = new();
            _slotDateRepositorys= slotDateRepositorys;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListSlotDate")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListSlotDate()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _slotDateRepositorys.ListSlotDate();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
    }
}
