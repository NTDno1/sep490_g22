﻿using AutoMapper;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using DataAccess.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.CourseDTO;
using Microsoft.Extensions.Hosting;
using FluentValidation;
//using Telerik.WinControls.UI;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SAQController : ControllerBase
    {
        private readonly IAuthenticationRepository _authenticationRepository;
        private readonly ISAQRepositorys _sAQRepositorys;
        protected ApiResponse _response;
        protected UpdateFaqValidator _updateFaqValidator;
        protected AddFaqValidator _addFaqValidator;
        private readonly IWebHostEnvironment _hostEnvironment;
        readonly DeleteExitImage _deleteExitImage;

        public SAQController(IAuthenticationRepository authenticationRepository, 
            ISAQRepositorys sAQRepositorys, IWebHostEnvironment hostEnvironment, 
            DeleteExitImage deleteExitImage, UpdateFaqValidator updateFaqValidator,
            AddFaqValidator addFaqValidator)
        {
            _authenticationRepository = authenticationRepository;
            _sAQRepositorys = sAQRepositorys;
            _response = new();
            _hostEnvironment = hostEnvironment;
            _deleteExitImage = deleteExitImage;
            _updateFaqValidator = updateFaqValidator;
            _addFaqValidator = addFaqValidator;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListSAQ")]
        //[Authorize(Roles = "Super Admin, Teacher")]
        public async Task<ActionResult<ApiResponse>> ListSAQ()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _sAQRepositorys.GetListSQA();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("FaqDetail")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> FaqDetail(string id)
        {
            try
            {
                var faqDetail = await _sAQRepositorys.GetFaqById(int.Parse(id));
                if(faqDetail!= null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = faqDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess=false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
        [HttpPut("UpdateFaq")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateFaq(UpdateSqaDTOs updateSqaDTO)
        {
            try
            {
                //if (imageFile != null)
                //{
                //    // Kiểm tra dung lượng ảnh
                //    if (imageFile.Length > 2 * 1024 * 1024) // 2MB
                //    {
                //        _response.StatusCode = HttpStatusCode.BadRequest;
                //        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                //        return BadRequest();
                //    }

                //    // Kiểm tra định dạng ảnh
                //    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                //    var fileExtension = System.IO.Path.GetExtension(imageFile.FileName).ToLower();

                //    if (!allowedExtensions.Contains(fileExtension))
                //    {
                //        _response.StatusCode = HttpStatusCode.BadRequest;
                //        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                //        return BadRequest();
                //    }
                //}
                if (!_updateFaqValidator.Validate(updateSqaDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateFaqValidator.Validate(updateSqaDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                //ViewSqaDTOs faq = await _sAQRepositorys.GetFaqById(updateSqaDTO.Id);
                //if (faq.Image != null)
                //{
                //    _deleteExitImage.DeleteImageFaq(faq.Image);
                //}
                //updateSqaDTO.Image = await SaveImage(imageFile);
                (bool success, List<string> message) check = await _sAQRepositorys.UpdateFaq(updateSqaDTO);
                if(check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateSqaDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateSqaDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddFaq")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> AddFaq(AddFaqDTOs addFaqDTOs)
        {
            try
            {
                //if (imageFile != null)
                //{
                //    // Kiểm tra dung lượng ảnh
                //    if (imageFile.Length > 2 * 1024 * 1024) // 2MB
                //    {
                //        _response.StatusCode = HttpStatusCode.BadRequest;
                //        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                //        return BadRequest();
                //    }

                //    // Kiểm tra định dạng ảnh
                //    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                //    var fileExtension = System.IO.Path.GetExtension(imageFile.FileName).ToLower();

                //    if (!allowedExtensions.Contains(fileExtension))
                //    {
                //        _response.StatusCode = HttpStatusCode.BadRequest;
                //        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                //        return BadRequest();
                //    }
                //}
                if (!_addFaqValidator.Validate(addFaqDTOs).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addFaqValidator.Validate(addFaqDTOs).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                //addFaqDTOs.Image = await SaveImage(imageFile);
                (bool success, List<string> message) check = await _sAQRepositorys.AddFaq(addFaqDTOs);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addFaqDTOs;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addFaqDTOs;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteFaq")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteFaq(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _sAQRepositorys.DeleteFaq(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        //[HttpPost("AddImage")]
        //public async Task<string> SaveImage(IFormFile imageFile)
        //{
        //    string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
        //    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
        //    var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images/Faq", imageName);
        //    Console.WriteLine(imagePath);
        //    Console.WriteLine(imagePath);

        //    using (var fileStream = new FileStream(imagePath, FileMode.Create))
        //    {
        //        await imageFile.CopyToAsync(fileStream);
        //    }
        //    return imageName;
        //}
    }
}
