﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using static DataAccess.Repositories.IReportmplement;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportRepositorys _reportRepositorys;
        protected ApiResponse _response;
        public ReportController(IReportRepositorys reportRepositorys)
        {
            _reportRepositorys = reportRepositorys;
            _response = new();
        }
        [HttpGet("ReportMember")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ReportMember(string? date) {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                Dictionary<string, Dictionary<string, Count>> list = new Dictionary<string, Dictionary<string, Count>>();
                if (date!= null)
                {
                     list = await _reportRepositorys.ReportMember(roleName, centerID, int.Parse(date));
                }
                else
                {
                    list = await _reportRepositorys.ReportMember(roleName, centerID, 0);
                }
                if (list != null)
                {
                    _response.Result = list;
                    return Ok(_response);
                }
                return BadRequest(_response);
            }
            catch
            (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.ErrorMessages = new List<string> { ex.Message };
                return BadRequest(_response);
            }
        }
        [HttpGet("ReportCol")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ReportCol()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var list = await _reportRepositorys.ReportCol(roleName, centerID);
                if (list != null)
                {
                    _response.Result = list;
                    return Ok(_response);
                }
                return BadRequest(_response);
            }
            catch
            (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.ErrorMessages = new List<string> { ex.Message };
                return BadRequest(_response);
            }
        }
        [HttpGet("ListTeacher")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListTeacher()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var list = await _reportRepositorys.ListTeacherAdminSide(roleName, centerID);
                if (list != null)
                {
                    _response.Result = list;
                    return Ok(_response);
                }
                return BadRequest(_response);
            }
            catch
            (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.ErrorMessages = new List<string> { ex.Message };
                return BadRequest(_response);
            }
        }
        [HttpGet("TotalMember")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> TotalMember()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var list = await _reportRepositorys.ReportTotalMember(roleName, centerID);
                if (list != null)
                {
                    _response.Result = list;
                    return Ok(_response);
                }
                return BadRequest(_response);
            }
            catch
            (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.ErrorMessages = new List<string> { ex.Message };
                return BadRequest(_response);
            }
        }
    }
}
