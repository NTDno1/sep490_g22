﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using ManageAPI.Token;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationRepository _authenticationRepository;
        private readonly ManageTokenRepository _manageToken;
        private readonly IAccountRepository _accountRepository;
        private readonly IEmployeeRepository _employeeRepository;
        public AuthenticationController(IAuthenticationRepository authenticationRepository, ManageTokenRepository manageToken, IAccountRepository accountRepository, IEmployeeRepository employeeRepository)
        {
            _authenticationRepository = authenticationRepository;
            _manageToken = manageToken;
            _accountRepository = accountRepository;
            _employeeRepository = employeeRepository;
        }

        [HttpPost("Login")]
        public IActionResult Login(UserDTO userRequest)
        {
            if(_authenticationRepository.checkLoginAccount(userRequest, false) == null) {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            ErrorMessages = new List<string>() { "Wrong account" },
                            Message = "Wrong account"
                        }
                    );;
            }
            else
            {
                var user = _authenticationRepository.checkLoginAccount(userRequest,false);
                var accessToken = _manageToken.generateToken(userRequest,false);
                var refreshToken = _manageToken.GenerateRefreshToken();
                var tokenInformation = new TokenModel
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                };
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpired = DateTime.Now.AddDays(7);
                _accountRepository.UpdateAccount(user);
                return Ok(
                    new ApiResponse
                    {
                        StatusCode = HttpStatusCode.OK,
                        IsSuccess = true,
                        ErrorMessages = new List<string>() { "Login successfully" },
                        Message = "Login successfully",
                        TokenInformation = tokenInformation
                    }
                    ); ;
            }

        }
        [HttpPost("LoginStudent")]
        public IActionResult LoginStudent(UserDTO userRequest)
        {
            if (_authenticationRepository.checkLoginStudent(userRequest, false).Item1 == 1)
            {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Center Currently Under Maintenance",
                            ErrorMessages = new List<string>{ "Center Currently Under Maintenance"}
                        }
                    );
            }
            else if (_authenticationRepository.checkLoginStudent(userRequest, false).Item1 == 2)
            {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Wrong account",
                            ErrorMessages = new List<string>{ "Wrong account"}
                        }
                    );
            }
            else
            {
                var user = _authenticationRepository.checkLoginStudent(userRequest, false).Item2;
                var accessToken = _manageToken.generateToken(userRequest, false);
                var refreshToken = _manageToken.GenerateRefreshToken();
                var tokenInformation = new TokenModel
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                };
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpired = DateTime.Now.AddDays(7);
                _accountRepository.UpdateStudent(user);

                return Ok(
                    new ApiResponse
                    {
                        StatusCode = HttpStatusCode.OK,
                        IsSuccess = true,
                        ErrorMessages = new List<string> { "Login successfully" },
                        Message = "Login successfully",
                        TokenInformation = tokenInformation
                    }
                    ); ;
            }

        }
        [HttpPost("LoginTeacher")]
        public IActionResult LoginTeacher(UserDTO userRequest)
        {
            if (_authenticationRepository.checkLoginTeacher(userRequest, false).Item1 == 1)
            {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Center Currently Under Maintenance",
                            ErrorMessages = new List<string>{ "Center Currently Under Maintenance"}
                        }
                    );
            }
            else if (_authenticationRepository.checkLoginTeacher(userRequest, false).Item1 == 2)
            {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Wrong account",
                             ErrorMessages = new List<string>{ "Wrong account"}
                        }
                    );
            }
            else
            {
                var user = _authenticationRepository.checkLoginTeacher(userRequest, false).Item2;
                var accessToken = _manageToken.generateToken(userRequest, false);
                var refreshToken = _manageToken.GenerateRefreshToken();
                var tokenInformation = new TokenModel
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                };
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpired = DateTime.Now.AddDays(7);
                _accountRepository.UpdateTeacher(user);

                return Ok(
                    new ApiResponse
                    {
                        StatusCode = HttpStatusCode.OK,
                        IsSuccess = true,
                        Message = "Login successfully",
                        TokenInformation = tokenInformation
                    }
                    ); ;
            }

        }
        [HttpPost("LoginEmployee")]
        public IActionResult LoginEmployee(UserDTO userRequest)
        {
            if (_authenticationRepository.checkLoginEmployee(userRequest, false).Item1 == 1)
            {
                return NotFound(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Center Currently Under Maintenance",
                            ErrorMessages = new List<string>{ "Center Currently Under Maintenance"}
                        }
                    );
            }
            else if (_authenticationRepository.checkLoginEmployee(userRequest, false).Item1 == 2)
            {
                return BadRequest(
                        new ApiResponse
                        {
                            StatusCode = HttpStatusCode.NotFound,
                            IsSuccess = false,
                            Message = "Wrong account",
                            ErrorMessages = new List<string>{ "Wrong account"}
                        }
                    );
            }
            else
            {
                var user = _authenticationRepository.checkLoginEmployee(userRequest, false).Item2;
                var accessToken = _manageToken.generateToken(userRequest, false);
                var refreshToken = _manageToken.GenerateRefreshToken();
                var tokenInformation = new TokenModel
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                };
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpired = DateTime.Now.AddDays(7);
                _accountRepository.UpdateEmployee(user);
                if(user.Status == 0)
                {
                    _employeeRepository.ActivateEmployee(user.Id.ToString());
                }
                return Ok(
                    new ApiResponse
                    {
                        //StatusCode = HttpStatusCode.OK,
                        IsSuccess = true,
                        Message = "Login successfully",
                        TokenInformation = tokenInformation
                    }
                    ); ;
            }

        }

    }
}
