﻿using Microsoft.AspNetCore.Http;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using ManageAPI.Token;
using BusinessObject.DTOs;
using BusinessObject.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using BusinessObject.DTOs.SyllabusDTO;
using DataAccess.Validation.ValidationInput;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   
    public class CentersController : ControllerBase
    {   
        private readonly ICenterRepository _ICenteRepository;
        protected UpdateCenterValidator _updateCenterValidator;
        protected AddCenterValidator _addCenterValidator;

        protected ApiResponse _response;
        public CentersController(ICenterRepository icenteRepository, UpdateCenterValidator updateCenterValidator, AddCenterValidator addCenterValidator)
        {
            _ICenteRepository = icenteRepository;
            _response = new();
            _updateCenterValidator = updateCenterValidator;
            _addCenterValidator = addCenterValidator;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPut("CloseCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> CloseCenter(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _ICenteRepository.CloseCenter(id);
                if(check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = check.message;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("ListCenterClientSide")]
        public async Task<ActionResult<ApiResponse>> ListCenterClientSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _ICenteRepository.ListCenter();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        [HttpPut("OpenCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> OpenCenter(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _ICenteRepository.OpenCenter(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = check.message;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("ListCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListCenter()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _ICenteRepository.ListCenter();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("UpdateCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> UpdateCenter(UpdateCenterDTOs updateCenterDTO)
        {
            try
            {
                if (!_updateCenterValidator.Validate(updateCenterDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateCenterValidator.Validate(updateCenterDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                updateCenterDTO.AdminId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool success, List<string> message) check = await _ICenteRepository.UpdateCenter(updateCenterDTO);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateCenterDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateCenterDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> AddCenter(AddNewCenterDTOs addCenterDTO)
        {
            try
            {
                if (!_addCenterValidator.Validate(addCenterDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addCenterValidator.Validate(addCenterDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                addCenterDTO.AdminId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                addCenterDTO.AdminId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool success, List<string> message) check = await _ICenteRepository.AddCenter(addCenterDTO);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addCenterDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addCenterDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteCenter")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeleteCenter(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _ICenteRepository.DeleteCenter(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("CenterDetail")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> CenterDetail(string id)
        {
            try
            {
                var faqDetail = await _ICenteRepository.CenterDetail(id);
                if (faqDetail != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = faqDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
    }
}
