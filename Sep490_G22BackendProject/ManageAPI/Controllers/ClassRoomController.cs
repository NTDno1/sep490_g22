﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassRoomController : ControllerBase
    {
        private readonly IClassRoomRepositorys _classRoomRepositorys;
        protected ApiResponse _response;

        public ClassRoomController(IClassRoomRepositorys classRoomRepositorys)
        {
            _response = new();
            _classRoomRepositorys= classRoomRepositorys;
        }
        [HttpGet("ListAllRoom")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListAllRoom()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classRoomRepositorys.ListAllRoom(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
    }
}
