﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.ExamDTO;
using BusinessObject.Models;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        private readonly IExamRepository _iExamRepositorys;
        protected ApiResponse _response;

        public ExamController(IExamRepository iExamRepositorys)
        {
            _iExamRepositorys = iExamRepositorys;
            _response = new();
        }
        [HttpGet("EvaluateExam")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<List<StudentExamResponse>>> EvaluateExam(int examID,int classID)
        {
            try
            {
                return Ok(_iExamRepositorys.FilterExamClass(examID,classID));
            }catch(Exception ex)
            {
                return BadRequest(new ApiResponse()
                {

                    Message = ex.Message,
                    IsSuccess = false
                });
            }
        }
        [HttpGet("GetFilterExamPage")]
        //[Authorize(Roles = "Teacher")]
        public async Task<ActionResult<CourseClassExamResponse>> GetFilterExamPage(string teacherName)
        {
            try
            {
                return Ok(_iExamRepositorys.GetCourseClassExamFilter(teacherName));
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {

                    Message = ex.Message,
                    IsSuccess = false
                });
            }
        }
        [HttpGet("GetFilterOfCourse")]
        //[Authorize(Roles = "Teacher")]
        public async Task<ActionResult<List<CourseFilter>>> GetFilterOfCourse(string teacherName)
        {
            try
            {
                return Ok(_iExamRepositorys.GetListCourseOfTeacher(teacherName));
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {

                    Message = ex.Message,
                    IsSuccess = false
                });
            }
        }
        [HttpGet("GetFilterOfClassAndExam")]
        //[Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ExamClassFilter>> GetFilterOfClassAndExam(int CourseID,string teacherName)
        {
            try
            {
                return Ok(_iExamRepositorys.GetListExamAndClassOfCourse(CourseID,teacherName));
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {

                    Message = ex.Message,
                    IsSuccess = false
                });
            }
        }

        [HttpPut("UpdateEvaluate")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> UpdateEvaluate(List<StudentExamResponse> exams)
        {
             try
            {
                _iExamRepositorys.UpdateEvaluate(exams);
                return Ok(new ApiResponse()
                {
                    Message = "Update Success",
                    IsSuccess = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {

                    Message = ex.Message,
                    IsSuccess = false
                });
            }
        }
        [HttpGet("ListExam")]
        //[Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListExam(int courseID)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _iExamRepositorys.ListExam(courseID);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        
        [HttpPut("EditExam")]
        //[Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> EditExam(UpdateExamDTOs dtoRequest)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                if(_iExamRepositorys.ValidateExam(dtoRequest.listExam).Result.Count > 0)
                {
                    _response.IsSuccess = false;
                    _response.ErrorMessages = await _iExamRepositorys.ValidateExam(dtoRequest.listExam);
                }
                else
                {
                    _response.IsSuccess = await _iExamRepositorys.UpdateExam(dtoRequest.listExam, dtoRequest.courseID); ;
                    
                }
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("GetAllCourseOfStudent")]
        //[Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> GetAllCourseOfStudent(string username)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _iExamRepositorys.GetCourseOfStudent(username);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("GetMarkOfStudent")]
        //[Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> GetMarkOfStudent(string username,int courseID,int classID)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _iExamRepositorys.GetMarkOfStudent(username, courseID,classID);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
    }
}
