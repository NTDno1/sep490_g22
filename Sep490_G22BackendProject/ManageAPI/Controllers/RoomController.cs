﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using ManageAPI.Token;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.Metrics;
using BusinessObject.Models;
using AutoMapper;
using System.Net;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using System.IO;
using BusinessObject.DTOs.RoomDTO;
using BusinessObject.DTOs.ClassRoomDTO;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public RoomController(IMapper mapper, IRoomRepository roomRepository, IWebHostEnvironment webHostEnvironment)
        {
            _roomRepository = roomRepository;
            _mapper = mapper;
            _response = new();
            _webHostEnvironment = webHostEnvironment;
        }
        [HttpGet("listRoom")]
        public IActionResult Get(int page = 1, int limit = 10, string? searchText = null)
        {
            try
            {
                var data = _roomRepository.ViewRooms(page, limit, searchText);
                if (data == null)
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.Message = "Data not found";
                    return BadRequest(_response);
                }
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("viewById")]
        public IActionResult GetRoomById(int id)
        {
            try
            {
                var data = _roomRepository.FindRoomById(id);
                if (data == null)
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.Message = "Data not found";
                    return BadRequest(_response);
                }
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("viewClassRoomById")]
        public async Task<ActionResult<ApiResponse>> GetClassRoomById(int id)
        {
            try
            {
                var data = await _roomRepository.FindClassRoomById(id);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }
        [HttpPost("addRoom")]
        public async Task<IActionResult> Post(AddRoomDTO createRoomDTO)
        {
            if (createRoomDTO == null)
            {
                return BadRequest("Invalid data. Ensure you send valid JSON data.");
            }

            try
            {
                _roomRepository.AddRoom(createRoomDTO);
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Create blog successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    ErrorMessages = new List<string> { ex.Message }
                };

                return BadRequest(response);
            }
        }

        [HttpPut("updateRoom")]
        public async Task<IActionResult> Put(int id, UpdateRoomDTO? updateRoomDTO)
        {
            try
            {
                if (updateRoomDTO != null)
                {
                    _roomRepository.UpdateRoom(id, updateRoomDTO);
                }
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Update Room successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    ErrorMessages = new List<string> { ex.Message }
                };

                return BadRequest(response);
            }
        }


        [HttpDelete("deleteRoom")]
        public IActionResult Delete(List<int> ids)
        {
            try
            {
                _roomRepository.DeleteRoom(ids);
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                _response.Message = "Delete room successfully";
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("listRoomAdminSide")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> GetRoomAdminSide()
        {
            try
            {
                var data = await _roomRepository.ViewClassRoomsDTO(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost("addClassRoom")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> AddClassRoom(AddClassRoomDTO createRoomDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                createRoomDTO.CenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value);
                await _roomRepository.AddClassRoom(createRoomDTO, Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value));
                var response = new ApiResponse
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Class room created successfully"
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new ApiResponse
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    Message = ex.Message,
                };

                return BadRequest(response);
            }
        }

        [HttpPut("updateClassRoom")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<IActionResult> UpdateClassRoom(int id, UpdateClassRoomDTO? updateRoomDTO)
        {
            try
            {
                if (updateRoomDTO != null)
                {
                  await  _roomRepository.UpdateClassRoom(id, updateRoomDTO,Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value));
                }
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Update Room successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    Message = ex.Message,
                };

                return BadRequest(response);
            }
        }


        [HttpDelete("deleteClassRoom")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteClassRoom(int id)
        {
            try
            {
                await _roomRepository.DeleteClassRoom(id);
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                _response.Message = "Delete room successfully";
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

    }
}