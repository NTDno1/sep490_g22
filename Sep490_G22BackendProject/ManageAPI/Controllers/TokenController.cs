﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using ManageAPI.Token;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ManageTokenRepository _manageTokenRepository;
        public TokenController(IAccountRepository accountRepository, ManageTokenRepository manageTokenRepository) {
            _accountRepository = accountRepository;
            _manageTokenRepository = manageTokenRepository;
        }
        [HttpPost("Refresh")]
        public IActionResult Refresh(TokenModel token) 
        {
            if(token == null)
            {
                return BadRequest("Invalid Request");
            }
            String accessToken = token.AccessToken;
            String refreshToken = token.RefreshToken;
            
            var principle = _manageTokenRepository.GetPrincipalFromExpiredToken(accessToken);
            var username = principle.Identity.Name;

            var user = _accountRepository.getAccountByUsername(username);

            if(user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpired <= DateTime.Now)
            {
                return BadRequest("Invalid Request");
            }

            var newAcessToken = _manageTokenRepository.generateToken(new UserDTO()
            {
                username = user.UserName,
                password = user.PassWord,
                role = "Super Admin"
            },true);
            var newRefreshToken = _manageTokenRepository.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            _accountRepository.UpdateAccount(user);
            return Ok(
                new TokenModel()
                {
                    AccessToken = newAcessToken,
                    RefreshToken = newRefreshToken
                }
                );


        }
        [HttpPost("RefreshStudent")]
        public IActionResult RefreshStudent(TokenModel token)
        {
            if (token == null)
            {
                return BadRequest("Invalid Request");
            }
            String accessToken = token.AccessToken;
            String refreshToken = token.RefreshToken;

            var principle = _manageTokenRepository.GetPrincipalFromExpiredToken(accessToken);
            var username = principle.Identity.Name;

            var user = _accountRepository.getStudentByUsername(username);

            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpired <= DateTime.Now)
            {
                return BadRequest("Invalid Request");
            }

            var newAcessToken = _manageTokenRepository.generateToken(new UserDTO()
            {
                username = user.UserName,
                password = user.PassWord,
                role = "Student"
            }, true);
            var newRefreshToken = _manageTokenRepository.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            _accountRepository.UpdateStudent(user);
            return Ok(
                new TokenModel()
                {
                    AccessToken = newAcessToken,
                    RefreshToken = newRefreshToken
                }
                );


        }
        [HttpPost("RefreshTeacher")]
        public IActionResult RefreshTeacher(TokenModel token)
        {
            if (token == null)
            {
                return BadRequest("Invalid Request");
            }
            String accessToken = token.AccessToken;
            String refreshToken = token.RefreshToken;

            var principle = _manageTokenRepository.GetPrincipalFromExpiredToken(accessToken);
            var username = principle.Identity.Name;

            var user = _accountRepository.getTeacherByUsername(username);

            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpired <= DateTime.Now)
            {
                return BadRequest("Invalid Request");
            }

            var newAcessToken = _manageTokenRepository.generateToken(new UserDTO()
            {
                username = user.UserName,
                password = user.PassWord,
                role = "Teacher"
            }, true);
            var newRefreshToken = _manageTokenRepository.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            _accountRepository.UpdateTeacher(user);
            return Ok(
                new TokenModel()
                {
                    AccessToken = newAcessToken,
                    RefreshToken = newRefreshToken
                }
                );


        }
        [HttpPost("RefreshEmployee")]
        public IActionResult RefreshEmployee(TokenModel token)
        {
            if (token == null)
            {
                return BadRequest("Invalid Request");
            }
            String accessToken = token.AccessToken;
            String refreshToken = token.RefreshToken;

            var principle = _manageTokenRepository.GetPrincipalFromExpiredToken(accessToken);
            var username = principle.Identity.Name;

            var user = _accountRepository.getEmployeeByUsername(username);

            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpired <= DateTime.Now)
            {
                return BadRequest("Invalid Request");
            }

            var newAcessToken = _manageTokenRepository.generateToken(new UserDTO()
            {
                username = user.UserName,
                password = user.PassWord,
                role = "Employee"
            }, true);
            var newRefreshToken = _manageTokenRepository.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            _accountRepository.UpdateEmployee(user);
            return Ok(
                new TokenModel()
                {
                    AccessToken = newAcessToken,
                    RefreshToken = newRefreshToken
                }
                );


        }
        [HttpPost("Revoke")]
        [Authorize]
        public IActionResult Revoke()
        {
            var username = User.Identity.Name;
            var user = _accountRepository.getAccountByUsername(username);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _accountRepository.UpdateAccount(user);
            return NoContent();
        }
        [HttpPost("RevokeStudent")]
        [Authorize]
        public IActionResult RevokeStudent()
        {
            var username = User.Identity.Name;
            var user = _accountRepository.getStudentByUsername(username);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _accountRepository.UpdateStudent(user);
            return NoContent();
        }
        [HttpPost("RevokeTeacher")]
        [Authorize]
        public IActionResult RevokeTeacher()
        {
            var username = User.Identity.Name;
            var user = _accountRepository.getTeacherByUsername(username);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _accountRepository.UpdateTeacher(user);
            return NoContent();
        }
        [HttpPost("RevokeEmployee")]
        [Authorize]
        public IActionResult RevokeEmployee()
        {
            var username = User.Identity.Name;
            var user = _accountRepository.getEmployeeByUsername(username);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _accountRepository.UpdateEmployee(user);
            return NoContent();
        }
    }
}
