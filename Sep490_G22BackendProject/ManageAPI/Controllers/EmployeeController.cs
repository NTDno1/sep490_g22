﻿using Microsoft.AspNetCore.Http;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using ManageAPI.Token;
using BusinessObject.DTOs;
using BusinessObject.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using BusinessObject.DTOs.SyllabusDTO;
using DataAccess.Validation.ValidationInput;
using System.Net;
using BusinessObject.DTOs.EmployeeDTO;

namespace ManageAPI.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeRepository _IemployeeRepositorys;
        protected UpdateEmployeeValidator _updateEmployeeValidator;
        protected AddEmployeeValidator _addEmloyeeValidator;
        protected ApiResponse _response;

        public EmployeeController(IEmployeeRepository employeeRepositorys,
            AddEmployeeValidator addEmployeeValidator, UpdateEmployeeValidator updateEmployeeValidator)
        {
            _IemployeeRepositorys = employeeRepositorys;
            _response = new();
            _addEmloyeeValidator = addEmployeeValidator;
            _updateEmployeeValidator = updateEmployeeValidator;
        }
        [HttpGet("ListEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListEmployee(string? centerId, string? name)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _IemployeeRepositorys.ListEmployee(centerId, name);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("UpdateEmployee")]
          [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateEmployee(UpdateEmployeeDTO updateEmployeeValidator)
        {
            try
            {
                if (!_updateEmployeeValidator.Validate(updateEmployeeValidator).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateEmployeeValidator.Validate(updateEmployeeValidator).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                (bool success, List<string> message) check = await _IemployeeRepositorys.UpdateEmployee(updateEmployeeValidator);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateEmployeeValidator;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> AddEmployee([FromBody]AddEmployeeDTO addEmployeeDTO)
        {
            try
            {
                if (!_addEmloyeeValidator.Validate(addEmployeeDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addEmloyeeValidator.Validate(addEmployeeDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool success, List<string> message) check = await _IemployeeRepositorys.AddEmployee(addEmployeeDTO, userId);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addEmployeeDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addEmployeeDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeleteEmployee(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _IemployeeRepositorys.DeleteEmployee(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteListEmployee")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteListEmployee([FromBody]List<string> id)
        {
            try
            {
                (bool success, List<string> message) check = await _IemployeeRepositorys.DeleteListEmployee(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("DeactivateEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeactivateEmployee(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _IemployeeRepositorys.DeactivateEmployee(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("ActivateEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> ActivateEmployee(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _IemployeeRepositorys.ActivateEmployee(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("EmployeeDetail")]
         [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> EmployeeDetail(string id)
        {
            try
            {
                var faqDetail = await _IemployeeRepositorys.EmployeeDetail(id);
                if (faqDetail != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = faqDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
    }
}
