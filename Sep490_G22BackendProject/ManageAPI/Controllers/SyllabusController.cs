﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.SyllabusDTO;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyllabusController : ControllerBase
    {
        private readonly ISyllabusRepositorys _syllabusRepositorys;
        protected ApiResponse _response;
        protected UpdateSyllabusValidator _updateSyllabusValidator;
        protected AddSyllabusValidator _addSyllabusValidator;

        public SyllabusController(ISyllabusRepositorys syllabusRepositorys, UpdateSyllabusValidator updateSyllabusValidator, AddSyllabusValidator addSyllabusValidator)
        {
            _response = new();
            _syllabusRepositorys = syllabusRepositorys;
            _updateSyllabusValidator = updateSyllabusValidator;
            _addSyllabusValidator = addSyllabusValidator;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListSylabusByClassId")]
        //[Authorize(Roles = "Employee, Super Admin, Teacher, Student")]
        public async Task<ActionResult<ApiResponse>> ListSylabusByClassId(string classId)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _syllabusRepositorys.ListSylabusByClassId(classId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListSyllabus")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> ListSyllabus()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                var centerId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                _response.Result = await _syllabusRepositorys.ListSylabus(centerId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("UpdateSyllabus")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateSyllabus(UpdateSyllabusDTO updateSyllabusDTO)
        {
            try
            {
                if (!_updateSyllabusValidator.Validate(updateSyllabusDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateSyllabusValidator.Validate(updateSyllabusDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                (bool success, List<string> message) check = await _syllabusRepositorys.UpdateSylabus(updateSyllabusDTO);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateSyllabusDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateSyllabusDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddSyllabus")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> AddSyllabus(AddSyllabusDTO addSyllabusDTO)
        {
            try
            {
                if (!_addSyllabusValidator.Validate(addSyllabusDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addSyllabusValidator.Validate(addSyllabusDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                //addFaqDTOs.Image = await SaveImage(imageFile);
                //updateClassDTO.AdminCenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                addSyllabusDTO.CenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                (bool success, List<string> message) check = await _syllabusRepositorys.AddSyllabus(addSyllabusDTO);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addSyllabusDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addSyllabusDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteSyllabus")]
         [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteSyllabus(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _syllabusRepositorys.DeleteSyllabus(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("SyllabusDetail")]
         //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> SyllabusDetail(string id)
        {
            try
            {
                var faqDetail = await _syllabusRepositorys.SyllabusDetail(id);
                if (faqDetail != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = faqDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
    }
}
