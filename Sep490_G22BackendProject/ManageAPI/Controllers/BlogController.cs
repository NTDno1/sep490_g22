﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using ManageAPI.Token;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.Metrics;
using BusinessObject.Models;
using AutoMapper;
using System.Net;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using System.IO;
using BusinessObject.DTOs.CourseDTO;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public BlogController(IMapper mapper, IBlogRepository blogRepository, IWebHostEnvironment webHostEnvironment)
        {
            _blogRepository = blogRepository;
            _mapper = mapper;
            _response = new();
            _webHostEnvironment = webHostEnvironment;
        }
        [HttpGet]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<IActionResult> Get(string? searchText = null, int? cateId = null)
        {
            try
            {
                var data = await _blogRepository.viewBlogs(searchText, cateId);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.ToString();
                return BadRequest(_response);
            }
        }
        [HttpGet("viewBlogById")]
        public async Task<IActionResult> GetBlogById(string code)
        {
            try
            {
                var data =await _blogRepository.findBlogById(code);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.ToString();
                return BadRequest(_response);
            }
        }
        [HttpGet("viewBlog")]
        public async Task<IActionResult>  Get()
        {
            try
            {
                var data = await _blogRepository.viewBlogHP();
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<IActionResult> Post([FromForm] CreateBlogDTO createBlogDTO, IFormFile? imageFile)
        {
            if (createBlogDTO == null)
            {
                return BadRequest("Invalid data. Ensure you send valid JSON data.");
            }

            try
            {
                if (imageFile != null)
                {
                    createBlogDTO.Img = await SaveImage(imageFile);
                }
               await _blogRepository.addBlog(createBlogDTO, Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value));
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Create blog successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    Message = ex.Message
                };

                return BadRequest(response);
            }
        }

        [HttpPost("AddImage")]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Images", imageName);

            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile upload)
        {
            if (upload != null && upload.Length > 0)
            {
                try
                {
                    string rootPath = _webHostEnvironment.ContentRootPath;

                    // Thêm đường dẫn tương đối tới thư mục lưu trữ ảnh
                    string imageFolderPath = Path.Combine(rootPath, "Uploads");
                    if (!Directory.Exists(imageFolderPath))
                    {
                        Directory.CreateDirectory(imageFolderPath);
                    }

                    string fileName = upload.FileName;
                    // Check if the file extension is either .png or .jpg
                    string fileExtension = Path.GetExtension(fileName);
                    if (fileExtension != ".png" && fileExtension != ".jpg")
                    {
                        return BadRequest("Invalid file format. Only .png and .jpg are allowed.");
                    }

                    string filePath = Path.Combine(imageFolderPath, upload.FileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        upload.CopyTo(stream);
                    }

                    var urlRoot = $"{Request.Scheme}://{Request.Host}/Uploads/{upload.FileName}";
                    return Ok(new
                    {
                        uploaded = true,
                        url = urlRoot,
                    }); ;
                }
                catch (Exception ex)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.Message = ex.Message;
                    return BadRequest(_response);
                }
            }
            else
            {
                return BadRequest(new { error = "No file uploaded or file is empty." });
            }
        }

        [HttpPut]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<IActionResult> Put(string code, [FromForm] UpdateBlogDTO? updateBlogDTO, IFormFile? imageFile)
        {
            if (updateBlogDTO == null)
            {
                return BadRequest("Invalid data. Ensure you send valid JSON data.");
            }

            try
            {
                if (imageFile != null)
                {
                    updateBlogDTO.Img = await SaveImage(imageFile);
                }
                if (updateBlogDTO != null)
                {
                   await _blogRepository.updateBlog(code, updateBlogDTO, Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value));
                }
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Update blog successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    Message = ex.Message
                };

                return BadRequest(response);
            }
        }


        [HttpDelete]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<IActionResult> Delete(List<string> ids)
        {
            try
            {
               await _blogRepository.deleteBlog(ids);
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                _response.Message = "Xóa bài đăng thành công";
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("viewCate")]
        public async Task<IActionResult> ViewCateBlog()
        {
            try
            {
                var data = await _blogRepository.getCateBlog();
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("viewBlogByCate")]
        public async Task<IActionResult> ViewBlogByCate(int? cateId)
        {
            try
            {
                var data = await _blogRepository.getBlogByCate(cateId);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("viewCateBlogChild")]
        public async Task<IActionResult> ViewBlogCate(int? cateId)
        {
            try
            {
                var data = await _blogRepository.getCateBlogById(cateId);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("viewBlogClientSide")]
        public async Task<IActionResult> GetBlogClientSide(int page = 1, int limit = 10, string? searchText = null, int? cateId = null)
        {
            try
            {
                var data =await _blogRepository.viewBlogsForClientSide(page, limit, searchText, cateId);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("viewRelatedBlog")]
        public async Task<IActionResult> GetRelatedBlog(int? cateId = 1, string? blogCode = null)
        {
            try
            {
                var data = await _blogRepository.getRelatedBlog(cateId, blogCode);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }
        [HttpPut("changeStatus")]
        public async Task<IActionResult> UpdateStatus(string? code = null, int? status = 1)
        {
            try
            {
               await _blogRepository.updateStatusBlog(status, code);
                var response = new
                {
                    StatusCode = HttpStatusCode.Created,
                    IsSuccess = true,
                    Message = "Update blog successfully"
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    IsSuccess = false,
                    Message = ex.Message
                };

                return BadRequest(response);
            }
        }
    }
}
