﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.Models;
using DataAccess.Repositories;
using DataAccess.Validation;
using DataAccess.Validation.ValidationInput;
using ManageAPI.Token;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System.Data;
using System.Globalization;
using System.Net;
using System.Security.Claims;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class AccountController : ControllerBase
    {
        private readonly IAuthenticationRepository _authenticationRepository;
        private readonly IAccountRepository _IaccountRepository;
         private readonly IHttpContextAccessor _httpContextAccessor;
        readonly CheckUser _checkUser;
        protected ApiResponse _response;
        private readonly IWebHostEnvironment _webHostEnvironment;
        protected AddEmployeeValidator _addEmployeeValidator;
        protected UpdateEmployeeValidator _updateEmployeeValidator;
        readonly HashPassword _hashPassword;

        public AccountController(HashPassword hashPassword, IWebHostEnvironment webHostEnvironment,IAuthenticationRepository authenticationRepository, IAccountRepository accountRepository,
            CheckUser checkUser, AddEmployeeValidator addEmployeeValidator, UpdateEmployeeValidator updateEmployeeValidator, IHttpContextAccessor iHttpContextAccessor)
        {
            _authenticationRepository = authenticationRepository;
            _checkUser = checkUser;
            _IaccountRepository = accountRepository;
            _response = new();
            _addEmployeeValidator = addEmployeeValidator;
            _updateEmployeeValidator = updateEmployeeValidator;
            _httpContextAccessor = iHttpContextAccessor;
            _webHostEnvironment = webHostEnvironment;
            _hashPassword = hashPassword;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<List<Account>>>> GetAccount()
        {
            if (_authenticationRepository.GetAccount == null)
            {
                return NotFound();
            }
            else
            {
                List<Account> list = _authenticationRepository.GetAccount();
                return Ok(list);
            }

        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListEmployee")]
        [Authorize(Roles = "Super Admin, Teacher")]
        public async Task<ActionResult<ApiResponse>> ListEmployeeAccount()
        {
            if (_checkUser.CheckAccountAdmin(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString()) == false)
            {
                return Unauthorized();
            }
            List<ListEmployeeDTO> employees = await _IaccountRepository.GetEmployees();
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _IaccountRepository.GetEmployees();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("EmployeeDetail")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> EmployeeDetail()
        {
            if (_checkUser.CheckAccountAdmin(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString()) == false)
            {
                return Unauthorized();
            }
            List<ListEmployeeDTO> employees = await _IaccountRepository.GetEmployees();
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _IaccountRepository.GetEmployees();
                return Ok(employees);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("AddEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> AddEmployee([FromBody] AddEmployeeDTO addEmployeeDTO)
        {
            if (_checkUser.CheckAccountAdmin(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString()) == false)
            {
                return Unauthorized();
            }
            if (!_addEmployeeValidator.Validate(addEmployeeDTO).IsValid)
            {
                var errorMessages = new List<string>();
                foreach (var error in _addEmployeeValidator.Validate(addEmployeeDTO).Errors)
                {
                    errorMessages.Add(error.ErrorMessage);
                }
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.Created;
                _response.ErrorMessages = errorMessages;
                return BadRequest(_response);
            }
            List<ListEmployeeDTO> list = await _IaccountRepository.GetEmployees();
            try
            {
                // await _IaccountRepository.AddEmployee(addEmployeeDTO, HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                _response.Result = addEmployeeDTO;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("UpdateEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> UpdateEmployee([FromBody] UpdateEmployeeDTO updateEmployeeDTO)
        {
            if (_checkUser.CheckAccountAdmin(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString()) == false)
            {
                return Unauthorized();
            }
            try
            {
                if (!_updateEmployeeValidator.Validate(updateEmployeeDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateEmployeeValidator.Validate(updateEmployeeDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                if (updateEmployeeDTO == null)
                {
                    ModelState.AddModelError("ErrorMessages", "Employee is Not Null!");
                    return BadRequest(ModelState);
                }
                await _IaccountRepository.UpdateEmployee(updateEmployeeDTO);
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                _response.Result = updateEmployeeDTO;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("DeleteEmployee")]
        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult<ApiResponse>> DeleteEmployee(string id)
        {
            if (_checkUser.CheckAccountAdmin(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString()) == false)
            {
                return Unauthorized();
            }
            try
            {
                if (id == null)
                {
                    return BadRequest();
                }
                List<ListEmployeeDTO> employees = await _IaccountRepository.GetEmployees();
                var check = employees.FirstOrDefault(u => u.Id.ToString().ToLower() == id.ToLower());
                if (check == null)
                {
                    return NotFound();
                }
                await _IaccountRepository.DeleteEmployee(id);
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("StudentProfile")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> StudentProfile(string username)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            try
            {
                if (username == null)
                {
                    return BadRequest();
                }
                
                var student = _IaccountRepository.getStudentByUsername(username);
                student.Image = $"{baseUrl}/Images/Student/{student.Image}";
                if (student == null)
                {
                    return NotFound();
                }
                
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                return Ok(student);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("TeacherProfile")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> TeacherProfile(string username)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            
            try
            {
                if (username == null)
                {
                    return BadRequest();
                }

                var teacher = _IaccountRepository.getTeacherByUsername(username);
                teacher.Image = $"{baseUrl}/Images/Teacher/{teacher.Image}";

                if (teacher == null)
                {
                    return NotFound();
                }

                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                return Ok(teacher);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("UpdateProfileStudent")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> UpdateProfileStudent([FromForm] ProfileDTO profileRequest, IFormFile? imageFile)
        {

            try
            {
                if (profileRequest == null)
                {
                    return BadRequest();
                }
                CultureInfo provider = CultureInfo.InvariantCulture;
                var student = _IaccountRepository.getStudentByUsername(profileRequest.userName);
                student.FirstName = profileRequest.firtsName;
                student.LastName = profileRequest.lastName;
                student.Email = profileRequest.email;
                student.Dob = String.IsNullOrEmpty(profileRequest.dob) ? student.Dob : DateTime.ParseExact(profileRequest.dob, "yyyy-MM-dd", provider);
                student.PhoneNumber = profileRequest.phoneNumber;
                student.Address = profileRequest.adress;
                if (imageFile != null)
                {
                    var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Images", student.Image);

                    System.IO.File.Delete(imagePath);
                    student.Image = await SaveImage(imageFile);
                }
                if (student == null)
                {
                    return NotFound();
                }
                _IaccountRepository.UpdateStudent(student);
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                return Ok();

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("UpdateProfileTeacher")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> UpdateProfileTeacher([FromForm]ProfileDTO profileRequest,IFormFile? imageFile)
        {

            try
            {
                if (profileRequest == null)
                {
                    return BadRequest();
                }
                CultureInfo provider = CultureInfo.InvariantCulture;
                var teacher = _IaccountRepository.getTeacherByUsername(profileRequest.userName);
                teacher.FirstName = profileRequest.firtsName;
                teacher.LastName = profileRequest.lastName;
                teacher.Email = profileRequest.email;
                if (!String.IsNullOrEmpty(profileRequest.dob) && !profileRequest.dob.Equals(""))
                {
                    teacher.Dob = DateTime.ParseExact(profileRequest.dob, "yyyy-MM-dd", provider);
                }
                
                teacher.PhoneNumber = profileRequest.phoneNumber;
               

                teacher.Address = profileRequest.adress;

                if (imageFile != null)
                {
                    var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Images", teacher.Image);

                    System.IO.File.Delete(imagePath);
                    teacher.Image = await SaveImage(imageFile);
                }

                if (teacher == null)
                {
                    return NotFound();
                }
                _IaccountRepository.UpdateTeacher(teacher);
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                return Ok();

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("AddImage")]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Images", imageName);
            Console.WriteLine(imagePath);
            Console.WriteLine(imagePath);

            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("UploadFolder")]
        [Authorize]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            string rootPath = _webHostEnvironment.ContentRootPath;

            // Thêm đường dẫn tương đối tới thư mục lưu trữ ảnh
            string imageFolderPath = Path.Combine(rootPath, "Images");
            if (file != null && file.Length > 0)
            {
                string uploadPath = imageFolderPath; 

                using (var stream = new FileStream(Path.Combine(uploadPath, file.FileName), FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                return Ok("File uploaded successfully");
            }

            return BadRequest("No file uploaded");
        }
        [HttpPut("ChangePassword")]
        [Authorize(Roles = "Teacher, Employee, Student, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ChangePasswordTeacher(ChangePasswordDTO formRequest)
        {

            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool check, string message) checks = await _IaccountRepository.ChangePassWord(formRequest, roleName, centerID, userId);
                if(checks.check == true)
                {
                    _response.IsSuccess = true;
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = checks.message;
                    _response.ErrorMessages = new List<string>() { checks.message };
                    return (Ok(_response));
                }
                else
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Message = checks.message;
                    _response.ErrorMessages = new List<string>() { checks.message };
                    return (BadRequest(_response));
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
    }
    
}
