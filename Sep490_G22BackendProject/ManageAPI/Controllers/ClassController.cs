﻿using AutoMapper;
using BusinessObject.DTOs;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using BusinessObject.DTOs.ClassDTO;
using FluentValidation;
using BusinessObject.Models;
using System.Security.Claims;
using BusinessObject.DTOs.CourseDTO;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly IClassRepositorys _classRepositorys;
        protected ApiResponse _response;
        protected AddClassValidator _addClassValidator;
        protected UpdateClassValidator _updateClassValidator;
        readonly CheckValidationInDb _checkValidationInDb;
        public ClassController(IClassRepositorys classRepositorys, AddClassValidator addClassValidator, UpdateClassValidator updateClassValidator, CheckValidationInDb checkValidationInDb)
        {
            _response = new();
            _classRepositorys = classRepositorys;
            _addClassValidator = addClassValidator;
            _updateClassValidator = updateClassValidator;
            _checkValidationInDb = checkValidationInDb;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListClassTeacherSide")]
        [Authorize (Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> ListClassTeacherSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classRepositorys.GetListClassTeachertSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListClassStudentSide")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> ListClassStudentSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                List<ViewClassDTO> check = await _classRepositorys.GetListClassStudentSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                if (check.Count < 0 || check == null)
                {
                    check = new List<ViewClassDTO>(); 
                }
                _response.Result = check;
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListClassAdminSide")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> ListClassAdminSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classRepositorys.GetListClassAdminSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("GetListClassToAttend")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> GetListClassToAttend(string status)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classRepositorys.GetListClassToAttend(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString(), int.Parse(status));
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }

        [HttpGet("ListStudentOfClass")]
        [Authorize(Roles = "Teacher, Student, Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListStudentOfClass(string classId)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classRepositorys.ListStudentInClass(classId);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ClassDetail")]
        [Authorize(Roles = "Teacher, Teacher, Student, Employee")]
        public async Task<ActionResult<ApiResponse>> ClassDetail(string id, string? chekcUpdate)
        {
            try
            {
                //var roleName = User.FindFirstValue(ClaimTypes.Role);
                _response.StatusCode = HttpStatusCode.OK;
                var classDetail = await _classRepositorys.ClassDetail(id, chekcUpdate);
                if(classDetail == null)
                {
                    return BadRequest(_response);
                }
                _response.Result = classDetail;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("AddClass")]
        // [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> AddClass(AddClassDTOs addClassDTOs)
        {
            try
            {
                if (!_addClassValidator.Validate(addClassDTOs).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addClassValidator.Validate(addClassDTOs).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                addClassDTOs.AdminCenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                //addClassDTOs.CenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());

                var centerId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                addClassDTOs.CenterId = Guid.Parse(centerId);
                addClassDTOs.Status = 0;
                if (!_checkValidationInDb.BeUniqueClassCodeAsync(addClassDTOs.Code, null, centerId))
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.ErrorMessages = new List<string> { "Đã Tồn Tại Mã Lớp Học" };
                    _response.Message = "Đã Tồn Tại Mã Lớp Học";
                    return BadRequest(_response);
                }
                bool addClass = await _classRepositorys.AddClass(addClassDTOs);
                if (addClass == false)
                {
                    throw new Exception("Lỗi khi thêm lớp học");
                }
                var newClass = await _classRepositorys.GetNewClass();
                _response.Result = newClass;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode=HttpStatusCode.BadRequest;
            }
            return _response;
        }
        [HttpPut("UpdateClass")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateClass(UpdateClassDTO updateClassDTO)
        {
            try
            {
                if (!_updateClassValidator.Validate(updateClassDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateClassValidator.Validate(updateClassDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                updateClassDTO.AdminCenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                updateClassDTO.CenterId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                (bool success, List<string> messgae) updateClass = await _classRepositorys.UpdateClass(updateClassDTO);
                if (updateClass.success == false)
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = updateClass.messgae;
                    return BadRequest(_response);
                }
                _response.Result = updateClassDTO;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
            
        }
        [HttpPut("CheckAdd")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> CheckAdd(string classId)
        {
            try
            {
                (bool success, List<string> messgae) updateClass = await _classRepositorys.CheckAdd(classId);
                if (updateClass.success == false)
                {
                    throw new Exception("Lỗi khi thêm lớp học");
                }
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }

        }
        [HttpPut("ClosedClass")]
        //[Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> ClosedClass(string classId)
        {
            try
            {
                (bool success, List<string> messgae) closedClass = await _classRepositorys.CloseClass(classId);
                if (closedClass.success == false)
                {
                    throw new Exception("Lỗi khi Đóng lớp học");
                }
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }

        }
        [HttpPut("OpenClass")]
        //[Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> OpenClass(string classId)
        {
            try
            {
                bool closedClass = await _classRepositorys.ChangeStatusClass(int.Parse(classId),1);
                if (closedClass == false)
                {
                    throw new Exception("Lỗi khi Mở lớp học");
                }
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteClass")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteClass(string classId)
        {
            try
            {
                (bool success, List<string> messgae) deleteClass = await _classRepositorys.DeleteClass(classId);
                if (deleteClass.success == false)
                {
                _response.IsSuccess = false;
                _response.ErrorMessages = deleteClass.messgae;
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
                }
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }

        }
    }
}
