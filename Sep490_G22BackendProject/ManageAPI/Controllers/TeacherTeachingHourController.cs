﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherTeachingHourController : ControllerBase
    {
        private readonly ITeacherTeachingHourRepositorys _teacherTeachingHourRepositorys;
        protected ApiResponse _response;

        public TeacherTeachingHourController(ITeacherTeachingHourRepositorys teacherTeachingHourRepositorys)
        {
            _response = new();
            _teacherTeachingHourRepositorys= teacherTeachingHourRepositorys;   
        }
        [HttpGet("ListTeacherTeachingHour")]
        //[Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> ListTeacherTeachingHour()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                //_response.Result = await _teacherTeachingHourRepositorys.ListTeacherTeachingHour("8b2ba772-5e2c-4395-8353-0dd814cc8750");
                _response.Result = await _teacherTeachingHourRepositorys.ListTeacherTeachingHour(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.Message };
                return BadRequest(_response);
            }
        }
        [HttpGet("ListTeacherTeachingHourAdminSide")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> ListTeacherTeachingHourAdminSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _teacherTeachingHourRepositorys.ListTeacherTeachingHourAdminSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                //_response.Result = await _teacherTeachingHourRepositorys.ListTeacherTeachingHour(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.Message };
                return BadRequest(_response);
            }
        }
    }
}
