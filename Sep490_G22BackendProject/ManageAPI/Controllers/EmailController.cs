﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.EmailDTO;
using BusinessObject.Models;
using DataAccess.Repositories;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using MimeKit;
using MimeKit.Text;
using System.Net;
using System.Security.Claims;

namespace SimpleEmailApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailRepositorys _emailRepository;
        private readonly IAccountRepository _accountRepository;
        protected ApiResponse _response;
        private readonly IMemoryCache _memoryCache;

        public EmailController(IEmailRepositorys emailRepositorys, IAccountRepository accountRepository, IMemoryCache memoryCache)
        {
            _emailRepository = emailRepositorys;
            _accountRepository = accountRepository;
            _response = new();
            _memoryCache = memoryCache;
        }

        [HttpPost("ForgotPassWord")]
        public async Task<ActionResult<ApiResponse>> ForgotPassWord(string userName, string email, string roleName)
        {
            string userKey = $"{userName}_{roleName}";

            // Lấy thời gian cuối cùng yêu cầu được gửi
            if (_memoryCache.TryGetValue<DateTime?>(userKey + "_LastRequestTime", out var lastRequestTime))
            {
                var currentTime = DateTime.Now;

                // Kiểm tra nếu đã qua 5 phút kể từ lần gửi cuối cùng
                if (currentTime - lastRequestTime >= TimeSpan.FromMinutes(5))
                {
                    // Reset số lần yêu cầu và cập nhật thời gian cuối cùng yêu cầu
                    _memoryCache.Set(userKey, 0);
                    _memoryCache.Set(userKey + "_LastRequestTime", currentTime);
                }
            }

            if (!_memoryCache.TryGetValue(userKey, out int requestCount))
            {
                // Nếu không có trong cache, đặt giá trị mặc định là 1 và thiết lập thời gian hết hạn
                var cacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5) // Thời gian hết hạn sau 5 phút
                };

                _memoryCache.Set(userKey, 1, cacheEntryOptions);
                _memoryCache.Set(userKey + "_LastRequestTime", DateTime.Now); // Lưu thời gian cuối cùng yêu cầu
            }
            else
            {
                // Nếu có trong cache, tăng giá trị
                _memoryCache.Set(userKey, requestCount + 1);
            }

            if (requestCount < 2) // Giới hạn số lần gọi là 2 trong 5 phút
            {
                (bool success, string message) check = await _accountRepository.GetUser(roleName, userName, email);
                if (check.success == true)
                {
                    SendEamilDto request = new SendEamilDto
                    {
                        To = email,
                        Subject = "Update New Password PI ONLINE TRAINING WEBSITE",
                        //Body = $"<body><h2>A NEW PASSWORD</h2> </br> <h3>New Pass:{check.message}</h3></body>"
                        Body = $"<!DOCTYPE html>\r\n<html>\r\n<head>\r\n<style>\r\nbody {{\r\n  font-family: Arial, sans-serif;\r\n}}\r\n\r\n.notification {{\r\n  background-color: #f9f9f9;\r\n  border: 1px solid #ddd;\r\n  padding: 10px;\r\n  margin: 10px 0;\r\n}}\r\n\r\n.notification-header {{\r\n  background-color: #4CAF50;\r\n  color: white;\r\n  padding: 10px;\r\n  text-align: center;\r\n  font-size: 20px;\r\n}}\r\n\r\n.notification button {{\r\n  background-color: #4CAF50;\r\n  color: white;\r\n  border: none;\r\n  padding: 10px 20px;\r\n  text-align: center;\r\n  text-decoration: none;\r\n  display: inline-block;\r\n  font-size: 16px;\r\n  margin: 10px 2px;  \r\n  /* Thêm các thuộc tính sau để căn giữa button */\r\n  display: block;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}}\r\n</style>\r\n</head>\r\n<body>\r\n\r\n<div class=\"notification\">\r\n  <div class=\"notification-header\">\r\n    POTMS Tranning Platform\r\n  </div>\r\n  <p>Hey, {userName} This is your new password:</p>\r\n  <button>{check.message}</button>\r\n  <p>You can log in here: <a href=\"http://pitech.edu.vn/login\">Login</a>.</p>\r\n  <p>Please do not reply directly to this email.\r\nCopyright © 2023 POTMS. All rights reserved.\r\nContact Us | Legal Notices and Terms of Use | Privacy Statement.</p>\r\n</div>\r\n\r\n</body>\r\n</html>\r\n"
                    };
                    _emailRepository.SendEmail(request);
                    _response.ErrorMessages = new List<string> { "Successful, please check your email" };
                    _response.Message = "Successful, please check your email";
                    _response.StatusCode = HttpStatusCode.OK;
                    return Ok(_response);
                }
                else
                {
                    _response.ErrorMessages = new List<string> { check.message };
                    _response.Message = check.message;
                    _response.StatusCode = HttpStatusCode.OK;
                    return BadRequest(_response);
                }
            }
            else
            {
                _response.ErrorMessages = new List<string> { "The 5-minute Limit request limit has been exceeded at one time" };
                _response.Message = "The 5-minute Limit request limit has been exceeded at one time";
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
    }
}