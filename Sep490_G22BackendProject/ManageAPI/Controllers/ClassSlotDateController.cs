﻿using AutoMapper;
using BusinessObject.DTOs;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BusinessObject.DTOs.ClassDTO;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using BusinessObject.DTOs.ClassSlotDateDTO;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassSlotDateController : ControllerBase
    {
        protected ApiResponse _response;
        private readonly IClassSlotDateRepositorys _classSlotDateRepositorys;
        private readonly ISchedualRepositorys _schedualRepositorys;
        private readonly IClassRepositorys _classRepositorys;
        public ClassSlotDateController(IClassSlotDateRepositorys classSlotDateRepositorys,
            IClassRepositorys classRepositorys, 
            ISchedualRepositorys slotClassRepositorys)
        {
            _response = new();
            _classSlotDateRepositorys = classSlotDateRepositorys;
            _classRepositorys = classRepositorys;
            _schedualRepositorys = slotClassRepositorys;
        }
        [HttpGet("ListClassSlotDate")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult<ApiResponse>> ListClassSlotDate()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classSlotDateRepositorys.ViewListClassSlotDate();
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListClassSlotDateByClass")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> ListClassSlotDateByClass(string id)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _classSlotDateRepositorys.ViewListClassSlotDateByClass(id);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddClassSlotDate")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> AddClassSlotDate([FromBody] AddClassSlotDateDTO listAddClassDateDTO)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                (bool success, List<string> errorMessage) addClassDateDTO =  await _classSlotDateRepositorys.CreateClassSlotDate(listAddClassDateDTO);
                if(addClassDateDTO.success == false)
                {
                    return BadRequest(new ApiResponse()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        ErrorMessages = addClassDateDTO.errorMessage
                });
                    
                }
                if (addClassDateDTO.success == true)
                {
                    ViewClassDTO viewClassDTO = await _classRepositorys.ClassDetail(listAddClassDateDTO.ClassId.ToString(), null);
                    var addClassLostDate = _schedualRepositorys.GenerateSchedule(viewClassDTO.classSlotDates, viewClassDTO.SlotNumber, viewClassDTO.StartDate, viewClassDTO.Id);
                    //await _slotClassRepositorys.CreateSlotClass(viewClassDTO.classSlotDates);
                    _response.Result = addClassDateDTO;
                    return Ok(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpDelete("DeleteClassSlotDate")]
        //[Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteClassSlotDate(string id)
        {
            try
            {
                (bool success, List<string> message) checkDelete =await _classSlotDateRepositorys.DeleteClassSlotDate(id);
                if(checkDelete.success== true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess=true;
                    _response.ErrorMessages = checkDelete.message;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = checkDelete.message;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.Message };
                return BadRequest(_response);
            }
        }
    }
}
