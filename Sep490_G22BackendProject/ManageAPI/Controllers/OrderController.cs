﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.OrderDTO;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using PdfSharpCore;
using PdfSharpCore.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using BusinessObject.Models;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        protected ApiResponse _response;
        private readonly IWebHostEnvironment environment;
        public OrderController(IOrderRepository orderRepository, IWebHostEnvironment webHostEnvironment)
        {
            _response = new();
            _orderRepository = orderRepository;
            this.environment = webHostEnvironment;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("Order")]
        //[Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> Order(string orderId)
        {   
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _orderRepository.Order(orderId, userId, roleName);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListOrder")]
        [Authorize(Roles = "Student, Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListOrder(string? toMonth, string? fromMonth, string? year)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                _response.Result = await _orderRepository.ListOrder(roleName, userId, toMonth, fromMonth, year, centerID);
                _response.StatusCode = HttpStatusCode.OK;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListOrderDetail")]
        public async Task<ActionResult<ApiResponse>> ListOrderDetail(string orderDetailId)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _orderRepository.ListOrderDetail(orderDetailId, roleName , userId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("AddOrderDetail")]
        public async Task<ActionResult<ApiResponse>> AddOrderDetail(AddOrderDetailDTO addOrderDetailDTO)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool status, List<string> message, ViewOrderDetailDTO respone) check = await _orderRepository.AddOrderDetail(addOrderDetailDTO, userId);
                if (check.status == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = check.message.First();
                    _response.ErrorMessages = check.message;
                    _response.Result = check.respone;
                    return Ok(_response);
                }
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.Message = check.message.First();
                _response.ErrorMessages = check.message;
                return BadRequest(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("AddOrder")]

        public async Task<ActionResult<ApiResponse>> AddOrder(AddOrderDTO addOrderDTO)
        {
            try
            {
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool status, List<string> message, ViewOrderDTO respone) check = await _orderRepository.AddOrder(addOrderDTO, userId);
                if (check.status == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = check.respone;
                    _response.Message = check.message.First();
                    _response.ErrorMessages = check.message;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Result = null;
                    _response.Message = "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: ";
                    _response.ErrorMessages = new List<string> { "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " };
                    Console.WriteLine("Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: "+ check.message.First());
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }

        [HttpPost("Payment")]

        public async Task<ActionResult<ApiResponse>> Payment([FromBody] string orderId)
        {
            try
            {
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool status, List<string> message) check = await _orderRepository.Payment(orderId, userId);
                if (check.status == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = check.message.First();
                    _response.ErrorMessages = check.message;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Result = null;
                    _response.Message = "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: ";
                    _response.ErrorMessages = new List<string> { "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " };
                    Console.WriteLine("Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " + check.message.First());
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("DeleteOrder")]
        public async Task<ActionResult<ApiResponse>> DeleteOrder(string orderId)
        {
            try
            {
                (bool status, List<string> message) check = await _orderRepository.DeteleOrder(orderId);
                if (check.status == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = check.message.First();
                    _response.ErrorMessages = check.message;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Result = null;
                    _response.Message = "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: ";
                    _response.ErrorMessages = new List<string> { "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " };
                    Console.WriteLine("Đã sảy ra lỗi khi thê m mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " + check.message.First());
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("DeleteOrderDetail")]
        public async Task<ActionResult<ApiResponse>> DeleteOrderDetail(string orderDetailId)
        {
            try
            {
                (bool status, List<string> message) check = await _orderRepository.DeleteOrderDetail(orderDetailId);
                if (check.status == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = check.message.First();
                    _response.ErrorMessages = check.message;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.Result = null;
                    _response.Message = "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: ";
                    _response.ErrorMessages = new List<string> { "Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " };
                    Console.WriteLine("Đã sảy ra lỗi khi thêm mới hóa đơn vui lòng liên hệ với bên quản trị dự án để khắc phục: " + check.message.First());
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [Authorize(Roles = "Employee, Super Admin")]
        [HttpGet("generatepdf")]
        public async Task<IActionResult> GeneratePDF(string orderId)
        {
            var document = new PdfDocument();
            //string imgeurl = "data:image/png;base64, " + Getbase64string() + "";
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
            var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
            var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
            var orderDetail = await _orderRepository.ListOrderDetail(orderId, roleName ,userId);
            var order = await _orderRepository.Order(orderId, userId, roleName);
            var orderMore = await _orderRepository.OrderMore(orderId);
            var totalPrice = orderMore.OrderDetails;
            float price = 0;
            foreach(var item in totalPrice)
            {
                if (item != null && item.Price.HasValue)
                {
                    string priceAsString = item.Price.Value.ToString(); // Convert nullable double to string
                    if (float.TryParse(priceAsString, out float parsedPrice))
                    {
                        price += parsedPrice;
                    }
                }
            }
            string[] copies = { "Customer copy", "Comapny Copy" };

            string htmlcontent = "<div style='width:100%; text-align:center; background-color: #f2f2f2; padding: 10px;'>";
            htmlcontent += "<h2 style='color: #333;'>Hóa Đơn Đăng Ký Khóa Học </h2>";
            htmlcontent += "<h2 style='color: #007bff;'>Phần Mềm Đào Tạo Trực Tuyến PI Online Training</h2>";

            if (order != null)
            {
                htmlcontent += "<h2 style='color: #333;'> Số hóa đơn: " + order.OrderCode + " & Ngày hóa đơn:" + order.CreateDate?.ToString("dd/MM/yyyy") + "</h2>";
                htmlcontent += "<h3 style='color: #333;'> Khách hàng : " + order.StudentName + "</h3>";
                htmlcontent += "<p> Tiêu Đề : " + order.Name + "</p>";
                htmlcontent += "<p> Mô Tả: " + order.Title + "</p>";
                htmlcontent += "<h3 style='color: #333;'> Mã Khách Hàng:  # " + order.StudentId.ToString().Substring(order.StudentId.ToString().Length - 6) + " </h3>";
                htmlcontent += "</div>";
            }

            htmlcontent += "<table style ='width:100%; border-collapse: collapse; border: 1px solid #000; margin-top: 10px;'>";
            htmlcontent += "<thead style='font-weight:bold; background-color: #007bff; color: #fff;'>";
            htmlcontent += "<tr>";
            htmlcontent += "<td style='border:1px solid #333'> Mã sản phẩm </td>";
            htmlcontent += "<td style='border:1px solid #333'> Mô tả </td>";
            htmlcontent += "<td style='border:1px solid #333'>Số lượng</td>";
            htmlcontent += "<td style='border:1px solid #333'>Giá</td >";
            htmlcontent += "<td style='border:1px solid #333'>Tổng cộng</td>";
            htmlcontent += "</tr>";
            htmlcontent += "</thead >";

            htmlcontent += "<tbody>";
            if (orderDetail != null && orderDetail.Count > 0)
            {
                orderDetail.ForEach(item =>
                {
                    htmlcontent += "<tr>";
                    htmlcontent += "<td>" + item.CourseName + "</td>";
                    htmlcontent += "<td>" + item.Note + "</td>";
                    htmlcontent += "<td>" + item.Discount + "</td >";
                    htmlcontent += "<td>" + item.Price + "</td>";
                    htmlcontent += "</tr>";
                });
            }
            htmlcontent += "</tbody>";

            htmlcontent += "</table>";
            htmlcontent += "<div style='text-align:right; margin-top: 10px;'>";
            htmlcontent += "<h1 style='color: #333;'> Thông tin tóm tắt </h1>";
            htmlcontent += "<table style='border-collapse: collapse; border: 1px solid #000; float:right;'>";
            htmlcontent += "<tr>";
            htmlcontent += "<td style='border:1px solid #000; background-color: #f2f2f2;'> Tổng cộng tóm tắt </td>";
            htmlcontent += "<td style='border:1px solid #000; background-color: #f2f2f2;'> Thuế tóm tắt </td>";
            htmlcontent += "<td style='border:1px solid #000; background-color: #f2f2f2;'> Tổng cộng Net </td>";
            htmlcontent += "</tr>";
            if (order != null)
            {
                htmlcontent += "<tr>";
                htmlcontent += "<td style='border: 1px solid #000'> " + price + " </td>";
                //htmlcontent += "<td style='border: 1px solid #000'>" + header.Tax + "</td>";
                //htmlcontent += "<td style='border: 1px solid #000'> " + header.NetTotal + "</td>";
                htmlcontent += "</tr>";
            }
            htmlcontent += "</table>";
            htmlcontent += "</div>";

            htmlcontent += "</div>";

            // Phần ký xác nhận giữa hai bên
            htmlcontent += "<div style='text-align:center; margin-top:20px;'>";
            htmlcontent += "<p style='text-align:left; display:inline-block; margin-right:auto; margin-bottom: 10px;'>Xác nhận từ bên A--------------------------</p>";
            htmlcontent += "<p style='text-align:right; display:inline-block; margin-left:auto; margin-bottom: 10px;'>Xác nhận từ bên B--------------------------</p>";
            htmlcontent += "</div>";

            PdfGenerator.AddPdfPages(document, htmlcontent, PageSize.A4);

            
            byte[]? response = null;
            using (MemoryStream ms = new MemoryStream())
            {
                document.Save(ms);
                response = ms.ToArray();
            }
            string Filename = "Invoice_" + orderId + ".pdf";
            return File(response, "application/pdf", Filename);
        }
        //[NonAction]
        //public string Getbase64string()
        //{
        //    string filepath = this.environment.WebRootPath + "\\Uploads\\common\\logo.jpeg";
        //    byte[] imgarray = System.IO.File.ReadAllBytes(filepath);
        //    string base64 = Convert.ToBase64String(imgarray);
        //    return base64;
        //}
    }
}
