﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.Models;
using DataAccess.Repositories;
using DataAccess.Validation;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseRepository _IcourseRepository;
        protected ApiResponse _response;
        protected AddCourseValidator _addCourseValidator;
        protected UpdateCourseValidator _updateCourseValidator;
        private readonly IWebHostEnvironment _hostEnvironment;
        readonly DeleteExitImage _deleteExitImage;
        readonly CheckValidationInDb _checkValidationInDb;
        public CourseController( ICourseRepository courseRepository,AddCourseValidator 
            addCourseValidator, UpdateCourseValidator updateCourseValidator, 
            IWebHostEnvironment hostEnvironment, DeleteExitImage deleteExitImage, CheckValidationInDb checkValidationInDb)
        {
            _IcourseRepository = courseRepository;
            _response = new();
            _addCourseValidator = addCourseValidator;
            _updateCourseValidator = updateCourseValidator;
            _hostEnvironment = hostEnvironment;
            _deleteExitImage = deleteExitImage;
            _checkValidationInDb= checkValidationInDb;
        }
[HttpGet("viewCourseClientSide")]
        public async Task<IActionResult> GetCourseClientSide(int page = 1, int limit = 10, string? searchText = null, Guid? centerCode = null)
        {
            try
            {
                var data = await _IcourseRepository.ViewCourseForClientSide(page, limit, searchText, centerCode);
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.Message = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("ListCourse")]
        public async Task<ActionResult<IEnumerable<List<ListCourseDTO>>>> GetCourseDTOs()
        {
            try
            {
                List<ListCourseDTO> courses = new List<ListCourseDTO>();
                if (HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString() != null)
                {
                    courses = await _IcourseRepository.GetCourseDTOs(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                }
                else
                {
                    courses = await _IcourseRepository.GetCourseDTOs("");
                }
                if (courses != null)
                {
                    _response.Result = courses;
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    return Ok(_response);
                }
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("DetailCourse")]
        public async Task<ActionResult<IEnumerable<List<ListCourseDTO>>>> GetCourseDetailDTOById(int courseId)
        {
            try
            {
                ListCourseDTO course = await _IcourseRepository.GetCourseDetailDTOById(courseId);

                if (course != null)
                {
                    return Ok(course);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Super Admin, Employee")]
        [HttpPost("AddCourse")]
        public async Task<ActionResult<ApiResponse>> AddCourse([FromForm] AddCourseDTO addCourseDTO, IFormFile? imageFile)
        {
            if (imageFile != null)
            {
                // Kiểm tra dung lượng ảnh
                if (imageFile.Length > 2 * 1024 * 1024) // 2MB
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                    return BadRequest(_response);
                }

                // Kiểm tra định dạng ảnh
                var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                var fileExtension = System.IO.Path.GetExtension(imageFile.FileName).ToLower();

                if (!allowedExtensions.Contains(fileExtension))
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                    return BadRequest(_response);
                }
            }
            if (!_addCourseValidator.Validate(addCourseDTO).IsValid)
            {
                var errorMessages = new List<string>();
                foreach (var error in _addCourseValidator.Validate(addCourseDTO).Errors)
                {
                    errorMessages.Add(error.ErrorMessage);
                }
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.Created;
                _response.ErrorMessages = errorMessages;
                return BadRequest(_response);
            }
            var centerId =  HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
            if (!_checkValidationInDb.BeUniqueCourseCode(addCourseDTO.Code, centerId))
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.Created;
                _response.ErrorMessages = new List<string> { "Đã Tồn Tại Mã Khóa Học" };
                _response.Message = "Đã Tồn Tại Mã Khóa Học";
                return BadRequest(_response);
            }
            try
            {
                if (addCourseDTO == null)
                {
                    return BadRequest(addCourseDTO);
                }
                if(imageFile == null && addCourseDTO!= null)
                {
                    await _IcourseRepository.AddCourse(addCourseDTO, HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                    _response.Result = addCourseDTO;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.IsSuccess = true;
                    return Ok(_response);
                }
                addCourseDTO.ImageSrc = await SaveImage(imageFile);
                await _IcourseRepository.AddCourse(addCourseDTO, HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                _response.Result = addCourseDTO;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode=HttpStatusCode.BadRequest; 
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("DeleteCourse")]
         //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteCourse(int courseId)
        {
            try
            {
                (bool success, List<string> message) check = await _IcourseRepository.DeleteCourse(courseId);
                if(check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("UpdateCourse")]
         [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateCourse([FromForm] UpdateCourseDTO updateCourseDTO, IFormFile? imageFile)
        {
            try
            {
                if (imageFile != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (imageFile.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest(_response);
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(imageFile.FileName).ToLower();

                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest(_response);
                    }
                }
                if (!_updateCourseValidator.Validate(updateCourseDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateCourseValidator.Validate(updateCourseDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                var course = await _IcourseRepository.GetCourseByID(updateCourseDTO.Id);
                if (imageFile != null)
                {
                    if(course.ImageSrc!= null)
                    {
                        _deleteExitImage.DeleteImageCourse(course.ImageSrc);
                    }
                    updateCourseDTO.ImageSrc = await SaveImage(imageFile);
                    updateCourseDTO.EmployeeId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                    await _IcourseRepository.UpdateCourse(updateCourseDTO, HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString(), "true");
                    return Ok(_response);
                }
                //updateCourseDTO.ImageSrc = await SaveImage(imageFile);
                updateCourseDTO.EmployeeId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                await _IcourseRepository.UpdateCourse(updateCourseDTO, HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString(), null);
                _response.StatusCode = HttpStatusCode.NoContent;
                _response.IsSuccess = true;
                _response.Result = updateCourseDTO;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
            
        }
        [HttpPost("ConvertImageToBase64")]
        //public ActionResult ConvertImageToBase64(IFormFile imageFile)
        //{
        //    if (imageFile != null && imageFile.Length > 0)
        //    {
        //        long maxFileSizeInBytes = 2 * 1024 * 1024; // 2MB
        //        if (imageFile.Length > maxFileSizeInBytes)
        //        {
        //            return BadRequest("Dung lượng tệp ảnh quá lớn.");
        //        }
        //        using (var stream = new MemoryStream())
        //        {
        //            imageFile.CopyTo(stream);
        //            var imageBytes = stream.GetBuffer();

        //            try
        //            {
        //                using (var image = System.Drawing.Image.FromStream(new MemoryStream(imageBytes)))
        //                {
        //                    int desiredWidth = 200; // Độ phân giải mong muốn (ví dụ: 800 pixel)
        //                    int desiredHeight = 300;

        //                    if (image.Width > desiredWidth || image.Height > desiredHeight)
        //                    {
        //                        // Độ phân giải của ảnh lớn hơn mong muốn, nén ảnh.
        //                        var resizedImage = _checkUser.ResizeImage(image, desiredWidth, desiredHeight);
        //                        using (var resizedStream = new MemoryStream())
        //                        {
        //                            resizedImage.Save(resizedStream, ImageFormat.Jpeg); // Lưu ảnh dưới dạng JPEG
        //                            var base64String = Convert.ToBase64String(resizedStream.ToArray());
        //                            _IcourseRepository.TestAddImgToDb(base64String);
        //                            return Ok(base64String);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // Độ phân giải của ảnh đã thấp hơn hoặc bằng độ phân giải mong muốn.
        //                        var base64String = Convert.ToBase64String(imageBytes);
        //                        _IcourseRepository.TestAddImgToDb(base64String);
        //                        return Ok(base64String);
        //                    }
        //                }
        //            }
        //            catch (Exception)
        //            {
        //                return BadRequest("Tệp không phải là ảnh.");
        //            }
        //        }
        //    }
        //    return BadRequest("Không có tệp hình ảnh được tải lên.");
        //}
        //public async Task<ActionResult> ConvertImageToBase64([FromForm] AddCourseDTO addCourseDTO, IFormFile imageFile)
        //{            
        //        using (var stream = new MemoryStream())
        //        {
        //        addCourseDTO.ImageSrc = await SaveImage(imageFile);
        //        _IcourseRepository.TestAddImgToDb(addCourseDTO);
        //            return Ok(addCourseDTO.Name);
        //        }
        //}
        [HttpPost("AddImage")]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images/Course", imageName);
            Console.WriteLine(imagePath);
            Console.WriteLine(imagePath);

            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }
        //[NonAction]
        //public void DeleteImage(string imageName)
        //{
        //    var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);
        //    if (System.IO.File.Exists(imagePath))
        //        System.IO.File.Delete(imagePath);
        //}
        [HttpGet("getCourse")]
        public IActionResult Get()
        {
            try
            {
                var data = _IcourseRepository.GetPopularCourse();
                if (data == null)
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.Created;
                    _response.Message = "Data not found";
                    return BadRequest(_response);
                }
                _response.Result = data;
                _response.StatusCode = HttpStatusCode.Created;
                _response.IsSuccess = true;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
