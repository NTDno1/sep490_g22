﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.TeacherDTO;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using DataAccess.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System.Net;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.ClassDTO;
using System.Security.Claims;
using System.Linq;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepositorys _studentRepositorys;
        protected ApiResponse _response;
        readonly DeleteExitImage _deleteExitImage;
        protected UpdateStudentValidator _updateStudentValidator;
        protected AddStudentValidator _addStudentValidator;
        private readonly IWebHostEnvironment _hostEnvironment;

        public StudentController(IStudentRepositorys studentRepositorys, 
            DeleteExitImage deleteExitImage, 
            UpdateStudentValidator updateStudentValidator, 
            AddStudentValidator addStudentValidator, 
            IWebHostEnvironment webHostEnvironment)
        {
            _response = new();
            _studentRepositorys = studentRepositorys;
            _deleteExitImage = deleteExitImage;
            _updateStudentValidator = updateStudentValidator;
            _addStudentValidator = addStudentValidator;
            _hostEnvironment = webHostEnvironment;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListAllStudentAdminSide")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListAllStudentAdminSide(string? classId)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _studentRepositorys.ListAllStudentAdminSide(roleName, centerID, classId);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListWattingStudent")]
        [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListWattingStudent()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                var centerID = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _studentRepositorys.ListWaitingStudent(roleName, centerID);
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPut("UpdateStudent")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> UpdateStudent([FromForm] UpdateStudentDTOs updateStudentDTO, IFormFile? Image)
        {
            try
            {
                bool checkava = false;
                string image = "";
                if ((Image) != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (Image.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(Image.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    var teacher = await _studentRepositorys.StudentDetail(updateStudentDTO.Id.ToString());
                    if (teacher.Image != null)
                    {
                        _deleteExitImage.DeleteImageStudent(teacher.Image);
                    }
                    image = await SaveImage(Image);
                    checkava = true;
                }
                if (!_updateStudentValidator.Validate(updateStudentDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateStudentValidator.Validate(updateStudentDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                string userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var roleName = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                if(roleName == "Student") {
                    userId = null;
                }
                (bool success, List<string> message) check = await _studentRepositorys.UpdateStudent(updateStudentDTO, checkava, userId, image);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateStudentDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddStudent")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> AddStudent([FromForm] AddStudentDTOs addStudentDTO, IFormFile? ImageStudent)
        {
            try
            {
                bool checkava = false;
                string image = "";
                if ((ImageStudent) != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (ImageStudent.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(ImageStudent.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    image = await SaveImage(ImageStudent);
                    checkava = true;
                }
                if (!_addStudentValidator.Validate(addStudentDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addStudentValidator.Validate(addStudentDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                var centerId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();

                (bool success, List<string> message) check = await _studentRepositorys.AddStudent(addStudentDTO, centerId, userId, checkava, image);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addStudentDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addStudentDTO;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteStudent")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteStudent(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _studentRepositorys.DeleteStudent(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteListStudent")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteListStudent(List<string> id)
        {
            try
            {
                (bool success, List<string> message) check = await _studentRepositorys.DeleteListStudent(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("DeactivateStudent")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeactivateStudent(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _studentRepositorys.DeactivateStudent(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("ActivateStudent")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> ActivateStudent(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _studentRepositorys.ActivateStudent(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        [HttpGet("StudentDetail")]
        //[Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> StudentDetail(string id)
        {
            try
            {
                var studentDetail = await _studentRepositorys.StudentDetail(id);
                if (studentDetail != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = studentDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
        [HttpPost("AddImage")]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images/Student", imageName);
            Console.WriteLine(imagePath);
            Console.WriteLine(imagePath);

            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }
    }
}
