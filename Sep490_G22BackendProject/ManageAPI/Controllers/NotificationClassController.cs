﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SyllabusDTO;
using DataAccess.Repositories;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationClassController : ControllerBase
    {
        private readonly INotificationClassRepositorys _notificationClassRepositorys;
        protected ApiResponse _response;
        protected AddNotificationClassValidator _addNotificationClassValidator;

        public NotificationClassController(INotificationClassRepositorys notificationClassRepositorys, AddNotificationClassValidator addNotificationClassValidator)
        {
            _response = new();
            _notificationClassRepositorys = notificationClassRepositorys;
            _addNotificationClassValidator = addNotificationClassValidator;
        }
        [HttpGet("ListNotificationClass")]
        [Authorize(Roles = "Teacher, Student, Employee")]
        public async Task<ActionResult<ApiResponse>> ListNotificationClass()
        {
            try
            {
                
                var roleName = User.FindFirstValue(ClaimTypes.Role);
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                var notiClass = await _notificationClassRepositorys.ListNotificationClass(userId, roleName);
                if(notiClass != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = notiClass;
                    return Ok(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPost("AddNotificationClass")]
        [Authorize(Roles = "Super Admin, Employee, Teacher")]
        public async Task<ActionResult<ApiResponse>> AddNotificationClass(AddNotificationClass addNotificationClass)
        {
            try
            {
                if (!_addNotificationClassValidator.Validate(addNotificationClass).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addNotificationClassValidator.Validate(addNotificationClass).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                (bool success, List<string> message) check = await _notificationClassRepositorys.AddNotificationClass(addNotificationClass);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addNotificationClass;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addNotificationClass;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
    }
}
