﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.TeacherDTO;
using DataAccess.Repositories;
using DataAccess.Validation;
using DataAccess.Validation.ValidationInput;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherRepositorys _teacherRepositorys;
        protected ApiResponse _response;
        protected AddTeacherValidator _addTeacherValidator;
        protected UpdateTeacherValidator _updateTeacherValidator;
        private readonly IWebHostEnvironment _hostEnvironment;
        readonly DeleteExitImage _deleteExitImage;

        public TeacherController(ITeacherRepositorys teacherRepositorys, 
            AddTeacherValidator addTeacherValidator, 
            UpdateTeacherValidator updateTeacherValidator, 
            IWebHostEnvironment hostEnvironment, 
            DeleteExitImage deleteExitImage)
        {
            _response = new();
            _teacherRepositorys = teacherRepositorys;
            _addTeacherValidator = addTeacherValidator;
            _updateTeacherValidator = updateTeacherValidator;
            _hostEnvironment = hostEnvironment;
            _deleteExitImage = deleteExitImage;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("ListTeacherAdminSide")]
        //  [Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListTeacherAdminSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _teacherRepositorys.ListTeacherAdminSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpGet("ListTeacherClientSide")]
        //[Authorize(Roles = "Employee, Super Admin")]
        public async Task<ActionResult<ApiResponse>> ListTeacherClientSide()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.Result = await _teacherRepositorys.ListTeacherClientSide(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString());
                return Ok(_response);

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }
            return _response;
        }
        [HttpPut("UpdateTeaher")]
        [Authorize(Roles = "Super Admin, Employee, Teacher")]
        public async Task<ActionResult<ApiResponse>> UpdateTeaher([FromForm]UpdateProfileTeacherDTO updateProfileTeacherDTO, IFormFile? image, IFormFile? imageCertificateName)
        {
            try
            {
                bool checkava = false;
                bool checkceti = false;
                string cetiimg ="";
                string avasimg="";
                if ((image) != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (image.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(image.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    var teacher = await _teacherRepositorys.TeacherDetail(updateProfileTeacherDTO.Id.ToString());
                    if (teacher.Image != null)
                    {
                        _deleteExitImage.DeleteImageTeacher(teacher.Image);
                    }
                     avasimg = await SaveImage(image);
                    checkava= true;
                }
                if (imageCertificateName != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (imageCertificateName.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(imageCertificateName.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    var teacher = await _teacherRepositorys.TeacherDetail(updateProfileTeacherDTO.Id.ToString());
                    if (teacher.ImageCertificateName != null)
                    {
                        _deleteExitImage.DeleteImageTeacher(teacher.ImageCertificateName);
                    }
                     cetiimg = await SaveImage(imageCertificateName);
                    checkceti= true;
                }
                if (!_updateTeacherValidator.Validate(updateProfileTeacherDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _updateTeacherValidator.Validate(updateProfileTeacherDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                (bool success, List<string> message) check = await _teacherRepositorys.UpdateTeacher(updateProfileTeacherDTO, checkava, checkceti, avasimg, cetiimg);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = updateProfileTeacherDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPost("AddTeacher")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> AddTeacher([FromForm]AddTeacherDTOs addTeacherDTO, IFormFile? image, IFormFile? imageCertificateName)
        {
            try
            {
                bool checkava = false;
                bool checkceti = false;
                string cetiimg = "";
                string avasimg = "";
                if ((image) != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (image.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(image.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    avasimg = await SaveImage(image);
                    checkava = true;
                }
                if (imageCertificateName != null)
                {
                    // Kiểm tra dung lượng ảnh
                    if (imageCertificateName.Length > 2 * 1024 * 1024) // 2MB
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Dung lượng ảnh không được vượt quá 2MB." };
                        return BadRequest();
                    }

                    // Kiểm tra định dạng ảnh
                    var allowedExtensions = new[] { ".png", ".jpg", ".jpeg" };
                    var fileExtension = System.IO.Path.GetExtension(imageCertificateName.FileName).ToLower();
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        _response.StatusCode = HttpStatusCode.BadRequest;
                        _response.ErrorMessages = new List<string> { "Định dạng ảnh không hợp lệ. Chỉ chấp nhận định dạng PNG, JPG, JPEG." };
                        return BadRequest();
                    }
                    cetiimg = await SaveImage(imageCertificateName);
                    checkceti = true;
                }
                if (!_addTeacherValidator.Validate(addTeacherDTO).IsValid)
                {
                    var errorMessages = new List<string>();
                    foreach (var error in _addTeacherValidator.Validate(addTeacherDTO).Errors)
                    {
                        errorMessages.Add(error.ErrorMessage);
                    }
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = errorMessages;
                    return BadRequest(_response);
                }
                var centerId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CenterID")?.Value.ToString();
                var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString();
                (bool success, List<string> message) check = await _teacherRepositorys.AddTeacher(addTeacherDTO, checkava, checkceti, avasimg, cetiimg, centerId, userId);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    _response.Result = addTeacherDTO;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteTeacher")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteTeacher(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _teacherRepositorys.DeleteTeacher(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpDelete("DeleteListTeacher")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeleteListTeacher(List<string> id)
        {
            try
            {
                (bool success, List<string> message) check = await _teacherRepositorys.DeleteListTeacher(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
            [HttpPut("DeactivateTeacher")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> DeactivateTeacher(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _teacherRepositorys.DeactivateTeacher(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpPut("ActivateTeacher")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> ActivateTeacher(string id)
        {
            try
            {
                (bool success, List<string> message) check = await _teacherRepositorys.ActivateTeacher(id);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    _response.Message = check.message.First();
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        [HttpGet("TeacherDetail")]
        [Authorize(Roles = "Super Admin, Employee")]
        public async Task<ActionResult<ApiResponse>> TeacherDetail(string id)
        {
            try
            {
                var faqDetail = await _teacherRepositorys.TeacherDetail(id);
                if (faqDetail != null)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Result = faqDetail;
                    return Ok(_response);
                }
                else
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    return BadRequest(_response);
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                _response.StatusCode = HttpStatusCode.BadRequest;
                return BadRequest(_response);
            }
        }
        [HttpPost("AddImage")]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images/Teacher", imageName);
            Console.WriteLine(imagePath);
            Console.WriteLine(imagePath);

            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }
    }
}
