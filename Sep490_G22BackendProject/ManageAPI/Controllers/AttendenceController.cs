﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.AttendenceDTO;
using BusinessObject.Models;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ManageAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendenceController : ControllerBase
    {
        private readonly IAttendenceRepositorys _attendenceDateRepositorys;
        protected ApiResponse _response;

        public AttendenceController(IAttendenceRepositorys attendenceDateRepositorys)
        {
            _response = new();
            _attendenceDateRepositorys= attendenceDateRepositorys;
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost("GenerateAttendence")]
        //[Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> GenerateAttendence(string classId)
        {
            try
            {
                (bool success, List<string> message) checks =  await _attendenceDateRepositorys.GenerateAttendence(classId);
                if(checks.success == true )
                {
                    _response.IsSuccess = true;
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.Message = $"Successfully created attendance for classId: {classId}";
                    _response.ErrorMessages = checks.message;
                    return Ok(_response);
                }
                else
                {
                    _response.IsSuccess = false;
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.ErrorMessages = checks.message;
                    return BadRequest(_response);
                }
                
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode=HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost("TakeAttendence")]
        [Authorize(Roles = "Employee, Teacher")]
        public async Task<ActionResult<ApiResponse>> TakeAttendence(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs)
        {
            try
            {
                (bool success, List<string> message) check = await _attendenceDateRepositorys.TakeAttendence(listlOSNAttendanceDTOs);
                if(check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.SuccessMessage = check.message.First();
                    return Ok(_response);
                }
                else if (check.success == false)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    return BadRequest(_response);
                }
                else
                {
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost("TakeAttendenceAdmin")]
        [Authorize(Roles = "Employee")]
        public async Task<ActionResult<ApiResponse>> TakeAttendenceAdmin(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs)
        {
            try
            {
                (bool success, List<string> message) check = await _attendenceDateRepositorys.TakeAttendenceAdmin(listlOSNAttendanceDTOs);
                if (check.success == true)
                {
                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.SuccessMessage = check.message.First();
                    return Ok(_response);
                }
                else if (check.success == false)
                {
                    _response.StatusCode = HttpStatusCode.BadRequest;
                    _response.IsSuccess = false;
                    _response.ErrorMessages = check.message;
                    return BadRequest(_response);
                }
                else
                {
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
        [HttpGet("ListAttendenceBySchedule")]
        [Authorize(Roles = "Employee, Super Admin, Teacher")]
        public async Task<ActionResult<ApiResponse>> ListAttendenceBySchedule(string scheduleID)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _attendenceDateRepositorys.ListAttendenceBySchedualId(scheduleID);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        [HttpGet("ListAttendenceByStudent")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> ListAttendenceByStudent()
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _attendenceDateRepositorys.ListAttendenceByStudentId(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString());
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }

        [HttpGet("ListAttendenceByClassId")]
        [Authorize(Roles = "Student")]
        public async Task<ActionResult<ApiResponse>> ListAttendenceByClassId(string ClassId)
        {
            try
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.Result = await _attendenceDateRepositorys.ListAttendenceByClassId(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ID")?.Value.ToString(), ClassId);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
                return BadRequest(_response);
            }
        }
    }
}
