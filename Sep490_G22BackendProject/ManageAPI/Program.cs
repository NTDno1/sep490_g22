using BusinessObject.DTOs;
using BusinessObject.Models;
using DataAccess.Repositories;
using ManageAPI.Token;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.Json.Serialization;
using AutoMapper;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.FileProviders;
using AspNetCoreRateLimit;
using Microsoft.EntityFrameworkCore;

namespace ManageAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllersWithViews().AddJsonOptions(option => option.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            builder.Services.AddHttpContextAccessor(); // This registers IHttpContextAccessor
            var secretKey = builder.Configuration["Config:SecretKey"];
            var secretKeyEncrypt = Encoding.UTF8.GetBytes(secretKey);
            builder.Services.AddScoped<ICourseRepository, ICourseImplement>();
            builder.Services.AddScoped<IExamRepository, IExamImplement>();
            builder.Services.AddScoped<IAuthenticationRepository, IAuthenticationImplement>();
            builder.Services.AddScoped<ICenterRepository, ICenterImplement>();
            builder.Services.AddScoped<ManageTokenRepository, ManageToken>();
            builder.Services.AddScoped<IAccountRepository, IAccountImplement>();
            builder.Services.AddScoped<IEmployeeRepository, IEmployeeImplement>();  
            builder.Services.AddScoped<IBlogRepository, IBlogImplement>();
            builder.Services.AddScoped<IRoomRepository, IRoomImplement>();
            builder.Services.AddScoped<ISAQRepositorys, ISAQImplement>();
            builder.Services.AddScoped<IClassRepositorys, IClassImplement>();
            builder.Services.AddScoped<IClassSlotDateRepositorys, IClassSlotDateImplement>();
            builder.Services.AddScoped<IClassRoomRepositorys, IClassRoomImplement>();
            builder.Services.AddScoped<ISchedualRepositorys, ISchedualImplement>();
            builder.Services.AddScoped<ISlotDateRepositorys, ISlotDateImplement>();
            builder.Services.AddScoped<IAttendenceRepositorys, IAttendenceImplement>();
            builder.Services.AddScoped<IStudentClassRepositorys, IStudentClassImplement>();
            builder.Services.AddScoped<IStudentRepositorys, IStudentImplement>();
            builder.Services.AddScoped<ISyllabusRepositorys, ISyllabusImplement>();
            builder.Services.AddScoped<ITeacherRepositorys, ITeacherImplement>();
            builder.Services.AddScoped<ITeacherTeachingHourRepositorys, ITeacherTeachingHourImplement>();
            builder.Services.AddScoped<INotificationClassRepositorys, INotificationClassmplement>();
            builder.Services.AddScoped<IReportRepositorys, IReportmplement>();
            builder.Services.AddScoped<IEmailRepositorys, IEmailImplement>();
            builder.Services.AddScoped<IOrderRepository, IOrderImplement>();
            builder.Services.AddScoped<DataAccess.Validation.HashPassword>();
            builder.Services.AddScoped<DataAccess.Validation.CheckUser>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.CheckValidationInDb>();
            builder.Services.AddScoped<DataAccess.Validation.DeleteExitImage>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationAddClass>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddEmployeeValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateEmployeeValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddCourseValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateCourseValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateClassValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddClassValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateFaqValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddFaqValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateSyllabusValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddSyllabusValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateCenterValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddCenterValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddTeacherValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateTeacherValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddNotificationClassValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.AddStudentValidator>();
            builder.Services.AddScoped<DataAccess.Validation.ValidationInput.UpdateStudentValidator>();

            builder.Services.AddMemoryCache();
            builder.Services.Configure<IpRateLimitOptions>(builder.Configuration.GetSection("IpRateLimiting"));
            builder.Services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            builder.Services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            builder.Services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            builder.Services.AddAutoMapper(typeof(MappingProfile));
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddDbContext<sep490_g22Context>(options => options.UseSqlServer(
            builder.Configuration.GetConnectionString("DefaultConnection")
            ));
            builder.Services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(option =>
                {
                    option.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(secretKeyEncrypt),
                        ClockSkew = TimeSpan.Zero
                    };
                });
            builder.Services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition(
                    "Bearer",
                    new OpenApiSecurityScheme
                    {
                        Description =
                            "JWT Authorization header using the Bearer scheme. \r\n\r\n "
                            + "Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\n"
                            + "Example: \"Bearer 12345abcdef\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Scheme = "Bearer"
                    }
                );
                options.AddSecurityRequirement(
                    new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header
                            },
                            new List<string>()
                        }
                    }
                );
            });
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseCors(builder =>
            {
                builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
            });
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "Images")),
                RequestPath = "/Images"
            });
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "Uploads")),
                RequestPath = "/Uploads"
            });
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
