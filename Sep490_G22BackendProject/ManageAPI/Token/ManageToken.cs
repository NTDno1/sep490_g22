﻿using BusinessObject.DTOs;
using DataAccess.Repositories;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using System.Security.Cryptography;
using BusinessObject.Models;

namespace ManageAPI.Token
{
    public class ManageToken : ManageTokenRepository
    {
        private readonly IConfiguration _configuration;

        private readonly IAuthenticationRepository _authenticationRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ManageToken(IConfiguration configuration,IAuthenticationRepository authenticationRepository, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _authenticationRepository = authenticationRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        public String generateToken(UserDTO userRequest,Boolean isRefresh)
        {
            
            String Id = null;
            String UserName = null;
            String centerID = null;
            String Role = null;
            String CenterName = null;
            String Avatar = null;
             var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            if(userRequest.role.Equals("Super Admin"))
            {
                var account = _authenticationRepository.checkLoginAccount(userRequest, isRefresh);
                Id = account.Id.ToString();
                UserName = account.UserName;
                centerID = "";
                Role = "Super Admin";
                CenterName = ""; 
                Avatar = "https://localhost:7241/Images/Teacher/landscape-232052088.jpg";
            }
            else if (userRequest.role.Equals("Teacher"))
            {
                var teacher = _authenticationRepository.checkLoginTeacher(userRequest, isRefresh).Item2;
                Id = teacher.Id.ToString();
                UserName = teacher.UserName;
                centerID = teacher.CenterId.ToString();
                Role = "Teacher";
                var center = _authenticationRepository.GetCenter(teacher.CenterId.ToString());
                CenterName = center.Name;
                Avatar = $"{baseUrl}/Images/Teacher/{teacher.Image}";
            }
            else if (userRequest.role.Equals("Student"))
            {
                var student = _authenticationRepository.checkLoginStudent(userRequest, isRefresh).Item2;
                Id = student.Id.ToString();
                UserName = student.UserName;
                centerID= student.CenterId.ToString();
                Role = "Student";
                var center = _authenticationRepository.GetCenter(student.CenterId.ToString());
                CenterName = center.Name;
                Avatar = $"{baseUrl}/Images/Student/{student.Image}";;
            }
            else
            {
                var employee = _authenticationRepository.checkLoginEmployee(userRequest, isRefresh).Item2;
                Id = employee.Id.ToString();
                UserName = employee.UserName;
                centerID= employee.CenterId.ToString();
                Role = "Employee";
                var center = _authenticationRepository.GetCenter(employee.CenterId.ToString());
                CenterName = center.Name;
                Avatar = "https://localhost:7241/Images/Teacher/landscape-232052088.jpg";
            }

            var jwtToken = new JwtSecurityTokenHandler();

            var secretKeyEncrypt = Encoding.UTF8.GetBytes(_configuration.GetSection("Config").GetSection("SecretKey").Value);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("ID",Id),
                    new Claim(ClaimTypes.Name,UserName),
                    new Claim("TokenId",Guid.NewGuid().ToString()),
                    new Claim("RoleID",userRequest.role),
                    new Claim("CenterID",centerID),
                    new Claim("CenterName",CenterName),
                    new Claim("Avatar",Avatar),
                    new Claim(ClaimTypes.Role, Role)
        }),
                Expires = DateTime.UtcNow.AddDays(40),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKeyEncrypt),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = jwtToken.CreateToken(tokenDescription);
            return jwtToken.WriteToken(token);
        }
        
        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, 
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("Config").GetSection("SecretKey").Value)),
                ValidateLifetime = false 
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        }
    }
}
