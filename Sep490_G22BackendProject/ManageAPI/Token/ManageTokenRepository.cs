﻿using BusinessObject.DTOs;
using System.Security.Claims;

namespace ManageAPI.Token
{
    public interface ManageTokenRepository
    {
        public String generateToken(UserDTO userRequest,Boolean isRefresh);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}
