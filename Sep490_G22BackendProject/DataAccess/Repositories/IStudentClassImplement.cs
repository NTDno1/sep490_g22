﻿    using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IStudentClassImplement : IStudentClassRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        private readonly IStudentRepositorys _studentRepositorys;
        public IStudentClassImplement(IMapper mapper, IStudentRepositorys studentRepositorys, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context;
            _studentRepositorys = studentRepositorys;
        }
        public async Task<(bool success, List<string> message)> AddStudentClass(string classId, string studentId, string note)
        {
            List<ClassSlotDate> classSlotDates = new List<ClassSlotDate>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                //var checkSchedual = _context.Scheduals.
                var checkStatusClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id.ToString() == classId); 
                if(checkStatusClass.Status !=0)
                {
                    return (false, new List<string> { $"Lớp học đang trong quá trình không thể cập nhật" });
                }
                var classSlotDate = await _context.ClassSlotDates.FirstOrDefaultAsync(u=>u.ClassId.ToString() == classId);
                if(classSlotDate == null)
                {
                    return (false, new List<string> { $"Hiện Tại Lớp Học Đang Chưa Có Lịch Học" });
                }
                    var check = await _context.StudentClasses.Where(u=>u.StudentId.ToString() == studentId).ToListAsync();
                    if (check == null || check.Count <=0 )
                    {
                        var studentClass = new StudentClass
                        {
                            StudentId = Guid.Parse(studentId),
                            ClassId = int.Parse(classId),
                            Note = note
                        };
                        await _context.StudentClasses.AddAsync(studentClass);
                        await _context.SaveChangesAsync();
                        int id = studentClass.Id;
                        var course = await _context.Classes.FirstOrDefaultAsync(x => x.Id.ToString() == classId);
                        var exams = await _context.Exams.Where(x => x.CourceId == course.CourceId).ToListAsync();
                        foreach(var item in exams)
                        {
                        ExamClass examClass = new ExamClass()
                        {
                            StudentClassId = id,
                            ExamId = item.Id
                        };
                            await _context.ExamClasses.AddAsync(examClass);
                            await _context.SaveChangesAsync();  

                        }
                        await _studentRepositorys.ActivateStudent(studentId);
                        return (true, new List<string> { $"Added successfully:  StudentName {await _context.StudentProfiles.Where(u => u.Id == studentClass.StudentId).Select(u=>u.LastName+" "+ u.MidName+" "+u.FirstName).FirstOrDefaultAsync()}; ClassId {studentClass.ClassId}" });
                    }
                    var getSlotByClass = await _context.ClassSlotDates.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                    List<int> slotIds = getSlotByClass.Select(slot => slot.SlotId).ToList();
                    if (check != null)
                    {
                        foreach(var item in check)
                        {
                            var addClassSlotDate = await _context.ClassSlotDates.Include(u=>u.Class).Where(u => u.ClassId == item.ClassId).ToListAsync();
                            classSlotDates.AddRange(addClassSlotDate);
                        }
                        bool existSlotIds = classSlotDates.Any(slotDate => slotIds.Contains(slotDate.SlotId));
                        if (existSlotIds == false )
                        {
                            var studentClass = new StudentClass
                            {
                                StudentId = Guid.Parse(studentId),
                                ClassId = int.Parse(classId),
                                DateCreate = DateTime.Now,
                                DateUpdate= DateTime.Now,
                                Note = note
                            };
                            await _context.StudentClasses.AddAsync(studentClass);

                            await _context.SaveChangesAsync();
                        int id = studentClass.Id;
                        var course = await _context.Classes.FirstOrDefaultAsync(x => x.Id.ToString() == classId);
                        var exams = await _context.Exams.Where(x => x.CourceId == course.CourceId).ToListAsync();
                        foreach (var item in exams)
                        {
                            ExamClass examClass = new ExamClass()
                            {
                                StudentClassId = id,
                                ExamId = item.Id
                            };
                            await _context.ExamClasses.AddAsync(examClass);
                            await _context.SaveChangesAsync();

                        }
                        await _studentRepositorys.ActivateStudent(studentId);
                            Console.WriteLine("đã vào đây");
                            return (true, new List<string> { $"Added successfully:  " +
                                $"StudentName {await _context.StudentProfiles.Where(u => u.Id == studentClass.StudentId).Select(u => u.LastName + " " + u.MidName + " " + u.FirstName).FirstOrDefaultAsync()}" +
                                $"; ClassName {await _context.Classes.Where(u=>u.Id == studentClass.ClassId).Select(u=>u.Name).FirstOrDefaultAsync()}" });
                        }
                        if (existSlotIds == true)
                        {
                            int existingSlotId = 0;
                            int classIdExit = 0;
                            existingSlotId = classSlotDates.First(slotDate => slotIds.Contains(slotDate.SlotId)).SlotId;
                            classIdExit = classSlotDates.First(slotDate => slotIds.Contains(slotDate.SlotId)).ClassId;
                            return (false, new List<string> { $"Học sinh: " +
                                $"{await _context.StudentProfiles.Where(u => u.Id.ToString() == studentId).Select(u =>u.FirstName).FirstOrDefaultAsync()}" +
                                $" không thể thêm do đang học lớp {await _context.Classes.Where(u=>u.Id == classIdExit).Select(u=>u.Name).FirstOrDefaultAsync()}" +
                                $"; vào ca học từ {await _context.SlotDates.Where(u=>u.Id == existingSlotId).Select(u=>u.TimeStart.ToString(@"hh\:mm")+"-"+u.TimeEnd.ToString(@"hh\:mm") +" vào "+u.Day+" Day").FirstOrDefaultAsync()}"});
                        }
                    }
                //}
            }
            catch (Exception ex )
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return (true, new List<string> { "ERROR" });
        }
        public async Task<List<ViewStudentClassDTO>> ListStudentOffClass(string classId)
        {
            List<StudentClass> studentClasses = new List<StudentClass>();
            List<ExtendedStudentClass> extendedStudentClasses = new List<ExtendedStudentClass>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    studentClasses = await _context.StudentClasses.Where(u=>u.ClassId.ToString() == classId).Include(u=>u.Student).Include(u=>u.Class).ThenInclude(u=>u.Scheduals).ToListAsync();
                //}
                var classes = await _context.Classes.FirstOrDefaultAsync(u => u.Id.ToString() == classId);
                    if(studentClasses != null)
                {
                    foreach(var item in studentClasses)
                    {
                        var studentClass = new ExtendedStudentClass
                        {
                            Id = item.Id,
                            ClassId = item.ClassId,
                            StudentId = item.StudentId,
                            DateCreate= item.DateCreate,
                            DateUpdate = item.DateUpdate,
                            Note= item.Note,
                            Class = item.Class,
                            Student = item.Student,
                            ClassName = classes.Name,
                        };
                        extendedStudentClasses.Add(studentClass);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            foreach(var items in extendedStudentClasses)
            {
                Console.WriteLine(items.ClassName);
                Console.WriteLine(items.Student.LastName);

            }
            return _mapper.Map<List<ViewStudentClassDTO>>(extendedStudentClasses);
        }
        public async Task<(bool, List<string>)> deleteStudentOffClass(string classId, string studentId)
        {
            List<StudentClass> studentClasses = new List<StudentClass>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var studentDelete = await _context.StudentClasses.FirstOrDefaultAsync(u=>u.ClassId == int.Parse(classId) && u.StudentId.ToString() == studentId);
                int id = studentDelete.Id;
                var examClassess = await _context.ExamClasses.Where(x => x.StudentClassId == id).ToListAsync();
                _context.ExamClasses.RemoveRange(examClassess);
                await _context.SaveChangesAsync();
                if (studentDelete!= null)
                    {
                         _context.StudentClasses.Remove(studentDelete);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Deleted Is Successfully" });
                    }
                    else
                    {
                        return (false, new List<string> { "Error When Delete Student" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> deleteListStudentClass(List<ViewStudentClassDTO> listStudentClass)
        {
            List<StudentClass> studentClasses = new List<StudentClass>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    foreach (var item in listStudentClass)
                    {
                        var studentDelete = await _context.StudentClasses.FirstOrDefaultAsync(u => u.ClassId == item.ClassId && u.StudentId == item.StudentId);
                    if (studentDelete != null)
                        {
                            studentClasses.Add(studentDelete);
                        }
                    }
                    if (studentClasses != null)
                    {
                        _context.StudentClasses.RemoveRange(studentClasses);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Deleted Is Successfully" });
                    }
                    else
                    {
                        return (false, new List<string> { "Error When Delete Student" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
