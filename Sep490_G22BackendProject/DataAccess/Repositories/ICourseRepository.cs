﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Pagination.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ICourseRepository
    {
        Task<List<ListCourseDTO>> GetCourseDTOs(string employeeId);

        Task<ListCourseDTO> GetCourseDetailDTOById(int courseId);

        public Task AddCourse(AddCourseDTO addCourseDTO, string employeeId);

        public Task<(bool, List<string> message)> DeleteCourse(int courseId);

        public Task UpdateCourse(UpdateCourseDTO updatedCourse, string centerId, string? checkimg);
        public  Task TestAddImgToDb(AddCourseDTO addCourseDTO);
        Task<Course> GetCourseByID(int courseId);
        DetailCourseDTO GetPopularCourse();
       Task<Pagination<ListCourseDTO>> ViewCourseForClientSide(int page, int limit, string? searchText, Guid? cateId);
    }
}
