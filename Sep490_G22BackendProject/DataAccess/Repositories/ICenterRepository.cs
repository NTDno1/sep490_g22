using BusinessObject.Models;
using BusinessObject.DTOs;
using BusinessObject.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ICenterRepository
    {
        Task<List<ListCenterDTOs>?> ListCenter();
        Task<(bool, List<string>)> UpdateCenter(UpdateCenterDTOs updateCenterDTOs);
        Task<(bool, List<string>)> AddCenter(AddNewCenterDTOs addNewCenterDTOs);
        Task<(bool, List<string>)> DeleteCenter(string id);
        Task<(bool, List<string>)> CloseCenter(string id);
        Task<ListCenterDTOs?> CenterDetail(string id);
        Task<(bool, List<string>)> OpenCenter(string id);

    }
}
