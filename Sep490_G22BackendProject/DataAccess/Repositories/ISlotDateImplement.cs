﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ISlotDateImplement : ISlotDateRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ISlotDateImplement(HashPassword hashPassword, IMapper mapper, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context; 
            _response = new();
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<List<ViewSlotdateDTO>> ListSlotDate()
        {
            List<SlotDate> slotDates = new List<SlotDate>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    slotDates = await _context.SlotDates.ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSlotdateDTO>>(slotDates);
        }
    }
}
