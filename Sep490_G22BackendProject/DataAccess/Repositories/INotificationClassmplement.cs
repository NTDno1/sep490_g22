﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class INotificationClassmplement : INotificationClassRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        readonly HashPassword _hashPassword;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public INotificationClassmplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _hashPassword = hashPassword;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }
        public async Task<List<ViewNotificationClassDTO>?> ListNotificationClass(string userId,string roleName)
        {
            List<NotificationClase> viewNotificationClassDTOs = new List<NotificationClase>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if(roleName != null && roleName == "Teacher")
                    {
                        var listClass = await _context.Classes.Where(u => u.TeacherId.ToString() == userId).ToListAsync();
                        foreach (var item in listClass)
                        {
                            var notiClass = await _context.NotificationClases.Where(u => u.ClassId == item.Id).ToListAsync();
                            viewNotificationClassDTOs.AddRange(notiClass);
                        }
                    }
                    if (roleName != null && roleName == "Student")
                    {
                        var listStudentClass = await _context.StudentClasses.Where(u => u.StudentId.ToString() == userId).ToListAsync();
                        var uniqueClassIds = listStudentClass.Select(c => c.ClassId).Distinct().ToList();
                        foreach (var item in uniqueClassIds)
                        {
                            var notiClass = await _context.NotificationClases.Where(u => u.ClassId == item.Value).ToListAsync();
                            viewNotificationClassDTOs.AddRange(notiClass);
                        }
                    }
                    if (roleName == "Employee")
                    {
                            var notiClass = await _context.NotificationClases.ToListAsync();
                            viewNotificationClassDTOs.AddRange(notiClass);
                    }
                //}
                return _mapper.Map<List<ViewNotificationClassDTO>>(viewNotificationClassDTOs.OrderByDescending(x => x.DateUpdate));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddNotificationClass(AddNotificationClass addNotificationClass)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (addNotificationClass != null)
                    {
                        var notificationClase = new NotificationClase
                        {
                          Title = addNotificationClass.Title,
                          Description = addNotificationClass.Description,
                          ClassId= addNotificationClass.ClassId,
                          Datecreate = DateTime.Now,
                          DateUpdate = DateTime.Now
                        };
                        await _context.AddAsync(notificationClase);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Add Sucess" });
                    }
                    else
                    {
                        return (false, new List<string> { "Add False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
