﻿using AutoMapper;
using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.RoomDTO;
using BusinessObject.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Pagination.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;

namespace DataAccess.Repositories
{
    public class IRoomImplement : IRoomRepository
    {
        private readonly sep490_g22Context _context = new();
        public static readonly int STATUS_ACTIVE = 1;
        public static readonly int STATUS_NOT_ACTIVE = 0;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IRoomImplement(IMapper mapper, IWebHostEnvironment hostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _hostEnvironment = hostEnvironment;
            _httpContextAccessor = httpContextAccessor;
        }
        private readonly IMapper _mapper;

        Pagination<ViewRoomDTO> IRoomRepository.ViewRooms(int page, int limit, string? searchText)
        {
            var query = _context.TypeRooms.AsQueryable();
            var totalItems = query.Count();

            if (totalItems == 0)
            {
                throw new Exception("No data found");
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                query = query.Where(x => x.Name.Contains(searchText));
            }

            query = query.Skip((page - 1) * limit).Take(limit);

            List<TypeRoom> paginatedData = query.ToList();
            IEnumerable<ViewRoomDTO> listRoomDTOs = _mapper.Map<IEnumerable<ViewRoomDTO>>(paginatedData);
            return new Pagination<ViewRoomDTO>(listRoomDTOs, totalItems, page, limit);
        }

        ViewRoomDTO IRoomRepository.FindRoomById(int id)
        {
            var data = _context.TypeRooms.FirstOrDefault(c => c.Id == id);
            if (data == null)
            {
                throw new Exception("err" + id + "not be found");
            }
            ViewRoomDTO roomDTO = _mapper.Map<ViewRoomDTO>(data);
            return roomDTO;
        }

        void IRoomRepository.AddRoom(AddRoomDTO addRoomDTO)
        {
            try
            {
                if (string.IsNullOrEmpty(addRoomDTO.Name.Trim()))
                {
                    throw new Exception("err name can not be null");
                }
                TypeRoom room = _mapper.Map<TypeRoom>(addRoomDTO);
                room.Name = addRoomDTO.Name;
                room.CreatedDate = DateTime.Now;
                room.Status = STATUS_ACTIVE;
                _context.TypeRooms.Add(room);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        void IRoomRepository.UpdateRoom(int id, UpdateRoomDTO updateRoomDTO)
        {

            try
            {
                var data = _context.TypeRooms.FirstOrDefault(c => c.Id == id);

                if (data == null)
                {
                    throw new Exception("err" + id + "not be found");
                }
                if (updateRoomDTO.Name.Trim() != null)
                {
                    data.Name = updateRoomDTO.Name;
                }
                data.Status = STATUS_ACTIVE;
                data.Status = updateRoomDTO.Status;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        void IRoomRepository.DeleteRoom(List<int> ids)
        {
            if (ids is null)
            {
                throw new Exception("Please choose blog!");
            }
            foreach (int id in ids)
            {
                var data = _context.TypeRooms.FirstOrDefault(c => c.Id == id);
                if (data is null)
                {
                    throw new Exception("Cannot found " + id + " in this system");
                }
                _context.Remove(data);
                _context.SaveChanges();
            }
        }
        private void changeStatusInClassRoom()
        {
            var roomIds = _context.ClassSlotDates.Select(s => s.RoomId).ToList();
            foreach (var roomId in roomIds)
            {
                var data = _context.ClassRooms.Where(u => u.Id == roomId).FirstOrDefault();
                data.Status = 2;
                _context.SaveChanges();
            }
            var roomSchedualIds = _context.Scheduals.Select(s => s.RoomIdUpDate).ToList();
            foreach (var roomId in roomSchedualIds)
            {
                var data = _context.ClassRooms.Where(u => u.Id == roomId).FirstOrDefault();
                data.Status = 2;
                _context.SaveChanges();
            }
        }
        public async Task<List<ViewRoomAdminDTO>> ViewClassRoomsDTO(string centerId)
        {
            var data = await _context.ClassRooms.Include(u => u.Type).Where(u=>u.CenterId.ToString() == centerId).OrderByDescending(article => article.UpdateDate.HasValue ? article.UpdateDate : article.CreatedDate).ToListAsync();

            if (data == null || data.Count == 0)
            {
                throw new Exception("Không tìm thấy dữ liệu");
            }

            List<ViewRoomAdminDTO> dataRoomDTO = _mapper.Map<List<ViewRoomAdminDTO>>(data);
            return dataRoomDTO;
        }

        public async Task<ViewRoomDetailDTO> FindClassRoomById(int id)
        {
            var data = await _context.ClassRooms.SingleOrDefaultAsync(c => c.Id == id);

            if (data == null)
            {
                throw new Exception("Không tìm thấy dữ liệu");
            }

            ViewRoomDetailDTO roomDTO = _mapper.Map<ViewRoomDetailDTO>(data);
            return roomDTO;
        }

        public async Task AddClassRoom(AddClassRoomDTO addRoomDTO, Guid employeeId)
        {
            try
            {
                if (string.IsNullOrEmpty(addRoomDTO.Name.Trim()))
                {
                    throw new Exception("Tên lớp không được bỏ trống");
                }

                bool roomExists = await _context.ClassRooms
                    .AnyAsync(classRoom =>
                        classRoom.Name.ToLower().Trim() == addRoomDTO.Name.Trim().ToLower() && classRoom.TypeId == addRoomDTO.TypeId);

                if (roomExists)
                {
                    throw new Exception("Phòng đã tồn tại");
                }

                var userName = await _context.Employees.Where(s => s.Id == employeeId).Select(x => x.Name).FirstOrDefaultAsync();

                ClassRoom room = _mapper.Map<ClassRoom>(addRoomDTO);
                room.Name = addRoomDTO.Name;
                room.TypeId = addRoomDTO.TypeId;
                room.CreatedDate = DateTime.Now;
                room.CreatedBy = userName.ToString();
                room.Status = STATUS_ACTIVE;

                _context.ClassRooms.Add(room);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task UpdateClassRoom(int id, UpdateClassRoomDTO updateRoomDTO, Guid employeeId)
        {
            if (id <= 0)
            {
                throw new Exception("Mã id phai lon hon 0");
            }
            var data = await _context.ClassRooms.FindAsync(id);
            if (data is null)
            {
                throw new Exception("Không tìm thấy thông tin với id: " + id);
            }

            if (await _context.ClassSlotDates.AnyAsync(s => s.RoomId == id) || await _context.Scheduals.AnyAsync(s => s.RoomIdUpDate == id))
            {
                throw new Exception("Lớp đang trong quá trình hoạt động");
            }

            if (await _context.ClassRooms.AnyAsync(s => s.Id != id && s.Name.ToLower().Trim() == updateRoomDTO.Name && s.TypeId == updateRoomDTO.TypeId))
            {
                throw new Exception("Phòng đã tồn tại");
            }

            var userName = await _context.Employees.Where(s => s.Id == employeeId).Select(x => x.Name).FirstOrDefaultAsync();
            data.Name = updateRoomDTO.Name;
            data.TypeId = updateRoomDTO.TypeId;
            data.UpdateDate = DateTime.Now;
            data.UpdateBy = userName.ToString();
            await _context.SaveChangesAsync();
        }

        public async Task DeleteClassRoom(int id)
        {
                if (id <= 0)
                {
                    throw new Exception("Id phải lớn hơn 0");
                }

                var data = await _context.ClassRooms.FindAsync(id);

                if (data is null)
                {
                    throw new Exception("Không tìm thấy thông tin với id: " + id);
                }

                bool classInProcess = await _context.ClassSlotDates.AnyAsync(s => s.RoomId == id);

                if (classInProcess)
                {
                    throw new Exception("Lớp đang trong quá trình hoạt động");
                }

                bool schedualInProcess = await _context.Scheduals.AnyAsync(s => s.RoomIdUpDate == id);

                if (schedualInProcess)
                {
                    throw new Exception("Lớp đang trong quá trình hoạt động");
                }

                _context.Remove(data);

            await _context.SaveChangesAsync();
        }


    }
}
