﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.AttendenceDTO;
using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SchedualDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IAttendenceImplement : IAttendenceRepositorys
    {
        //readonly sep490_g22Context _context = new();
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly ISchedualRepositorys _schedualRepositorys;
        private readonly ITeacherTeachingHourRepositorys _teacherTeachingHourRepositorys;
        private readonly IClassRepositorys _classRepositorys;
        private readonly INotificationClassRepositorys _notificationClassRepositorys;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IAttendenceImplement(IMapper mapper, IConfiguration configuration,ISchedualRepositorys schedualRepositorys,ITeacherTeachingHourRepositorys teacherTeachingHourRepositorys,
            IClassRepositorys IClassRepositorys, IHttpContextAccessor httpContextAccessor, INotificationClassRepositorys notificationClassRepositorys, sep490_g22Context context)
        {   
            _mapper = mapper;
            _schedualRepositorys = schedualRepositorys;
            _teacherTeachingHourRepositorys = teacherTeachingHourRepositorys;
            _response = new();
            _classRepositorys = IClassRepositorys;
            _httpContextAccessor = httpContextAccessor;
            _notificationClassRepositorys = notificationClassRepositorys;
            _context = context;
        } 
        public async Task<(bool success, List<string> message)> GenerateAttendence(string classId)
        {
            Dictionary<List<Schedual>, List<StudentClass>> mapSchedualAttendence = new Dictionary<List<Schedual>, List<StudentClass>>();
            List<Atendence> atendences = new List<Atendence>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var classSlotdate = await _context.StudentClasses.FirstOrDefaultAsync(u=>u.ClassId.ToString() == classId);
                    if (classSlotdate == null)
                {
                    return (true, new List<string> { "Hiện Tại Lớp Học Đang Không Học Sinh" });
                }
                    var checkStatusClass = await _context.Classes.FirstOrDefaultAsync(u=>u.Id == int.Parse(classId)); 
                    if(checkStatusClass!= null && checkStatusClass.Status == 0)
                    {
                        List<Schedual> listSchedual = await _context.Scheduals.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                        foreach (var item in listSchedual)
                        {
                            var checkAttendence = await _context.Atendences.Where(u => u.SchedualId == item.Id).ToListAsync();
                            if (checkAttendence != null || checkAttendence.Count > 0)
                            {
                                _context.Atendences.RemoveRange(checkAttendence); // Xóa tất cả các schedules tìm thấy
                                await _context.SaveChangesAsync(); // Lưu thay đổi vào cơ sở dữ liệu
                            }
                        }
                        //ClassSlotDate = await _context.ClassSlotDates.Include(u => u.Class).ThenInclude(u => u.Cource).Include(u => u.Slot).ToListAsync();
                        List<StudentClass> studentClasses = await _context.StudentClasses.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                        // Kiểm tra xem có key nào trong mapSchedualAttendence chưa
                        bool keyExists = mapSchedualAttendence.Any(pair => pair.Key.SequenceEqual(listSchedual));

                        if (!keyExists)
                        {
                            mapSchedualAttendence[listSchedual] = studentClasses;
                        }
                        //int count = 1;
                        List<Atendence> attendances = new List<Atendence>();
                        //Atendence addAttendances = new Atendence();
                        // Vòng lặp đã bên trong try/catch
                        foreach (var pair in mapSchedualAttendence)
                        {
                            List<Schedual> schedualList = pair.Key;
                            List<StudentClass> associatedAttendence = pair.Value;
                            foreach (var item in schedualList)
                            {
                                foreach (var items in associatedAttendence)
                                {
                                    var addAttendances = new Atendence
                                    {
                                        StudentId = items.StudentId,
                                        SchedualId = item.Id,
                                        Status = 0,
                                    };
                                    attendances.Add(addAttendances);
                                }
                            }
                        }
                        _context.Atendences.AddRange(attendances);
                        await _context.SaveChangesAsync();
                        await _classRepositorys.ChangeStatusClass(int.Parse(classId), 1);
                        await _context.Scheduals.Where(u => u.ClassId.ToString() == classId).ForEachAsync(s => s.Staus = 0);
                        await _context.SaveChangesAsync();
                    foreach (var studentClass in studentClasses)
                    {
                        // Kiểm tra xem có OrderDetail nào có CourseId giống với studentClass.CourceId và StudentId giống với studentClass.StudentId không
                        var hasMatchingOrder = await _context.OrderDetails
                            .FirstOrDefaultAsync(u => u.CourseId == checkStatusClass.CourceId && u.Order.StudentId == studentClass.StudentId);

                        if (hasMatchingOrder != null)
                        {
                            hasMatchingOrder.ClassId = int.Parse(classId);
                        }
                    }
                        var className = await _context.Classes.Where(u => u.Id == listSchedual.First().ClassId).Select(u => u.Name).FirstOrDefaultAsync();
                        AddNotificationClass addNotificationClass = new AddNotificationClass();
                        addNotificationClass.Title = "Thông Báo Bắt Đầu lớp học";
                        addNotificationClass.Description = $"Lớp Học: {className} " +
                            $"Sẽ Bắt Đầu vào: {listSchedual.First().DateOffSlot.ToString("dd/MM/yyyy")} Học Sinh Xem Lịch học";
                        addNotificationClass.ClassId = listSchedual.First().ClassId;
                        await _notificationClassRepositorys.AddNotificationClass(addNotificationClass);
                        return (true, new List<string> { "Items added successfully." });
                    }
                    else
                    {
                        return (false, new List<string> { "Classes in progress cannot be edited" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Error: " + ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool success, List<string> message)> TakeAttendence(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    int maxSlot = 0;
                    int scheduleid = 0;
                    scheduleid = listlOSNAttendanceDTOs.First().SchedualId.Value;
                    if (scheduleid == 0)
                    {
                        throw new Exception("SchedualId is null or the list is empty.");
                    }
                    var listSched =  _context.Scheduals.FirstOrDefault(u=>u.Id == scheduleid);
                    var classIdBySche = await _context.Scheduals
                    .Where(u => u.ClassId == listSched.ClassId)
                    .Select(u => u.SlotNum)
                    .ToListAsync();
                    if (classIdBySche.Any())
                    {
                        maxSlot = classIdBySche.Max();
                    }
                    (bool success, ViewSchedualDTO viewScheduals) check = await _schedualRepositorys.GetScheduleByScheduleId(scheduleid);
                    bool checkTeacherTime = await _teacherTeachingHourRepositorys.CheckScheduleInTeachHour(check.viewScheduals.Id.ToString());
                    DateTime dateOffPlusOneDay = check.viewScheduals.DateOffSlot.Value.AddDays(2);
                    DateTime today = DateTime.Today;
                    TimeSpan timeDifference = check.viewScheduals.TimeEnd - check.viewScheduals.TimeStart;
                    int timeDifferenceInMinutes = (int)timeDifference.TotalMinutes; // Chuyển đổi sang phút
                    if (today >= dateOffPlusOneDay)
                        {
                            return (false, new List<string> { $"Class attendance is only allowed within 48 hours!!" });
                        }
                        else if(checkTeacherTime == true && check.viewScheduals.DateOffSlot <= today && today <=dateOffPlusOneDay)
                        {
                            foreach (var item in listlOSNAttendanceDTOs)
                            {
                                var attendece = await _context.Atendences.FirstOrDefaultAsync(u => u.Id == item.Id);
                                if (attendece == null)
                                {
                                    return (false, new List<string> { $"Not Found Student Name: {item.StudentName}" });
                                }
                                else if (attendece != null)
                            {
                                attendece.Status = item.Status;
                                attendece.Note = item.Note;
                                await _context.SaveChangesAsync();
                            }
                        }
                        await _teacherTeachingHourRepositorys.AddTeacherTeachingHour(check.viewScheduals, timeDifferenceInMinutes);
                        var schedule = await _context.Scheduals.FirstOrDefaultAsync(u => u.Id == scheduleid);
                        if (schedule != null)
                        {
                            schedule.Staus = 1;
                            await _context.SaveChangesAsync();
                        }
                        listSched.Staus = 1;
                        if (listSched.SlotNum == maxSlot)
                        {
                            var classUpdate = await _context.Classes.FirstOrDefaultAsync(u => u.Id == listSched.ClassId);
                            classUpdate.Status = 2;
                            listSched.Staus = 2;
                        await _context.SaveChangesAsync();
                        }
                    await _context.SaveChangesAsync(); 
                        return (true, new List<string> { "Taking Attendance and Adding Successful Teaching Hours" });
                    }
                    else if (checkTeacherTime == false && check.viewScheduals.DateOffSlot <= today &&   today <= dateOffPlusOneDay)
                    {
                        foreach (var item in listlOSNAttendanceDTOs)
                        {
                            var attendece = await _context.Atendences.FirstOrDefaultAsync(u => u.Id == item.Id);
                            if (attendece == null)
                            {
                                return (false, new List<string> { $"Not Found Student Name: {item.StudentName}" });
                            }
                            else if (attendece != null)
                            {
                                attendece.Status = item.Status;
                                attendece.Note = item.Note;
                            }
                            await _context.SaveChangesAsync();
                        }
                    listSched.Staus = 1;
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Update Classroom Attendance Success" });
                    }
                    else if (today <= check.viewScheduals.DateOffSlot &&  checkTeacherTime == true)
                    {
                        return (false, new List<string> { "The class has not started yet" });
                    }
                    else
                    {
                        throw new Exception("An error occurs when taking attendance and reports to the Admin department");
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Error: " + ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool success, List<string> message)> TakeAttendenceAdmin(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    int scheduleid = 0;
                    scheduleid = listlOSNAttendanceDTOs.First().SchedualId.Value;
                    if (scheduleid == 0)
                    {
                        throw new Exception("SchedualId is null or the list is empty.");
                    }
                    (bool success, ViewSchedualDTO viewScheduals) check = await _schedualRepositorys.GetScheduleByScheduleId(scheduleid);
                    DateTime today = DateTime.Today;
                    TimeSpan timeDifference = check.viewScheduals.TimeEnd - check.viewScheduals.TimeStart;
                    int timeDifferenceInMinutes = (int)timeDifference.TotalMinutes; // Chuyển đổi sang phút
                    if (check.viewScheduals.DateOffSlot <= today)
                    {
                        foreach (var item in listlOSNAttendanceDTOs)
                        {
                            var attendece = await _context.Atendences.FirstOrDefaultAsync(u => u.Id == item.Id);
                            if (attendece == null)
                            {
                                return (false, new List<string> { $"Not Found Student Name: {item.StudentName}" });
                            }
                            else if (attendece != null)
                            {
                                attendece.Status = item.Status;
                                attendece.Note = item.Note;
                            }
                            await _context.SaveChangesAsync();
                        }
                        return (true, new List<string> { "Update Classroom Attendance Success" });
                    }
                    else if (today <= check.viewScheduals.DateOffSlot)
                    {
                        return (false, new List<string> { "The class has not started yet" });
                    }
                    else
                    {
                        throw new Exception("An error occurs when taking attendance and reports to the Admin department");
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Error: " + ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<List<ViewAttendenceDTO>> ListAttendenceByStudentId(string studentId)
        {
            List<Atendence> atendences = new List<Atendence>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    Console.WriteLine(studentId);
                    atendences = await _context.Atendences.Where(u => u.StudentId.ToString() == studentId).Include(u => u.Schedual).ThenInclude(u=>u.Class)
                        .Where(u=>u.Schedual.Class.Status == 1 || u.Schedual.Class.Status == 2)
                        .Include(u=>u.Schedual).ThenInclude(u=>u.SlotDate).Include(u=>u.Schedual).ThenInclude(u=>u.RoomIdUpDateNavigation).ThenInclude(u=>u.Type).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewAttendenceDTO>>(atendences);
        }
        public async Task<List<ViewAttendenceDTO>> ListAttendenceByClassId(string studentId, string classId)
        {
            List<Atendence> atendences = new List<Atendence>();
            List<Schedual> schedules = new List<Schedual> ();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getattendence = await _context.Atendences.Where(u => u.StudentId.ToString() == studentId).ToListAsync();
                    foreach(var item in getattendence)
                    {
                        var schedule = await _context.Scheduals.FirstOrDefaultAsync(u => u.Id == item.SchedualId && u.ClassId.ToString() == classId);
                        schedules.Add(schedule);
                    }
                    foreach(var item in schedules)
                    {
                       var atendence = await _context.Atendences.Where(u => u.SchedualId == item.Id && u.StudentId.ToString() == studentId).Include(u => u.Schedual).ThenInclude(u => u.Class)
                        .Where(u => u.Schedual.Class.Status == 1 || u.Schedual.Class.Status == 2)
                        .Include(u => u.Schedual).ThenInclude(u => u.SlotDate).Include(u => u.Schedual).ThenInclude(u => u.RoomIdUpDateNavigation).ThenInclude(u=>u.Type).FirstOrDefaultAsync();
                        atendences.Add(atendence);
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewAttendenceDTO>>(atendences);
        }
        public async Task<List<LOSNAttendanceDTO>> ListAttendenceBySchedualId(string schedualId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            
            List<Atendence> atendences= new List<Atendence>(); 
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    atendences = await _context.Atendences.Where(u => u.SchedualId.ToString() == schedualId).Include(u => u.Student).ToListAsync();
                    foreach (var attendance in atendences)
                    {
                        // Update the Image property by appending the desired string
                        attendance.Student.Image = $"{baseUrl}/Images/Student/{attendance.Student.Image}";
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return  _mapper.Map<List<LOSNAttendanceDTO>>(atendences);
        }
    }
}
