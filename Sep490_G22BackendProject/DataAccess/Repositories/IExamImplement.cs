﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.ExamDTO;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IExamImplement : IExamRepository
    {
        private readonly sep490_g22Context _context;
        public IExamImplement(sep490_g22Context context)
        {
            _context = context;
        }
        public List<StudentExamResponse> FilterExamClass(int examID, int classID)
        {
            List<StudentExamResponse> list = new();
            try
            {


                var listIdOfStudentClass = _context.StudentClasses.Include(x => x.Student).Where(x => x.ClassId == classID).ToList();
                foreach (var item in listIdOfStudentClass)
                {
                    var examClass = _context.ExamClasses.FirstOrDefault(x => x.StudentClassId == item.Id && x.ExamId == examID);
                    if (examClass != null)
                    {
                        list.Add(new StudentExamResponse()
                        {
                            ExamID = examID,
                            StudentClassID = item.Id,
                            Mark = examClass.Mark,
                            Evaluate = examClass.Evaluate,
                            StudentName = item.Student.FirstName + " " + item.Student.MidName + " " + item.Student.LastName
                        });
                    }


                }

            }
            catch (Exception ex)
            {

            }
            return list;


        }

        public CourseClassExamResponse GetCourseClassExamFilter(string TeacherName)
        {
            List<FilterOfCourseAndExam> listCourseExam = new();
            List<FilterOfClass> listClass = new();
            try
            {


                var teacher = _context.TeacherProfiles.FirstOrDefault(x => x.UserName.Equals(TeacherName));
                var ListCLass = _context.Classes.Where(x => x.TeacherId.Equals(teacher.Id)).ToList();
                var ListCourse = _context.Classes.Where(x => x.TeacherId.Equals(teacher.Id)).ToList().DistinctBy(x => x.CourceId);
                foreach (var itemCourseExam in ListCourse)
                {
                    var ListExamOfCourse = _context.Exams.Include(x => x.Cource).Where(x => x.CourceId == itemCourseExam.CourceId).ToList();


                    foreach (var itemExam in ListExamOfCourse)
                    {

                        FilterOfCourseAndExam filterCourseExam = new FilterOfCourseAndExam()
                        {
                            ExamID = itemExam.Id,
                            CourseExam = itemExam.Cource.Name + " - " + itemExam.Name
                        };
                        listCourseExam.Add(filterCourseExam);

                    }
                }

                foreach (var itemClass in ListCLass)
                {

                    FilterOfClass filterClass = new FilterOfClass()
                    {
                        ClassID = itemClass.Id,
                        Name = itemClass.Name
                    };
                    listClass.Add(filterClass);


                }

            }
            catch (Exception ex)
            {

            }
            return new CourseClassExamResponse()
            {
                ListClassFilter = listClass,
                ListCourseExamFilter = listCourseExam
            };
        }

        public List<CourseFilter> GetListCourseOfTeacher(string TeacherName)
        {
            List<CourseFilter> courseFilters = new List<CourseFilter>();
            try
            {


                var teacher = _context.TeacherProfiles.FirstOrDefault(x => x.UserName.Equals(TeacherName));
                var courseIDs = _context.Classes.Where(x => x.TeacherId == teacher.Id).ToList().Select(x => x.CourceId).Distinct();
                foreach (var item in courseIDs)
                {
                    var course = _context.Courses.FirstOrDefault(x => x.Id == item);
                    courseFilters.Add(new CourseFilter
                    {
                        CourseID = course.Id,
                        CourseName = course.Name
                    });
                }


            }
            catch (Exception ex)
            {

            }
            return courseFilters;

        }
        public ExamClassFilter GetListExamAndClassOfCourse(int CourseID, string teacherName)
        {
            ExamClassFilter examClassFilter = new();
            List<ExamFilter> examFilters = new List<ExamFilter>();
            List<ClassFilter> classFilters = new List<ClassFilter>();
            try
            {


                var teacher = _context.TeacherProfiles.FirstOrDefault(x => x.UserName.Equals(teacherName));
                var exams = _context.Exams.Where(x => x.CourceId == CourseID).ToList();
                var classes = _context.Classes.Where(x => x.CourceId == CourseID && x.TeacherId.Equals(teacher.Id) && x.Status == 1 ).ToList();
                foreach (var item in exams)
                {
                    ExamFilter examFilter = new ExamFilter
                    {
                        ExamID = item.Id,
                        ExamName = item.Name,
                        Average = item.Average
                    };
                    examFilters.Add(examFilter);
                }
                foreach (var item in classes)
                {
                    ClassFilter classFilter = new ClassFilter
                    {
                        ClassID = item.Id,
                        ClassName = item.Name
                    };
                    classFilters.Add(classFilter);
                }
                examClassFilter = new ExamClassFilter
                {
                    ListClass = classFilters,
                    ListExam = examFilters
                };


            }
            catch (Exception ex)
            {

            };
            return examClassFilter;

        }

        public void UpdateEvaluate(List<StudentExamResponse> exams)
        {

            foreach (var updatedStudent in exams)
            {
                var existingStudent = _context.ExamClasses.SingleOrDefault(x => x.StudentClassId == updatedStudent.StudentClassID && x.ExamId == updatedStudent.ExamID);
                if (existingStudent != null)
                {
                    existingStudent.Mark = updatedStudent.Mark;
                    existingStudent.Evaluate = updatedStudent.Evaluate;
                }
                _context.ExamClasses.Update(existingStudent);
                _context.SaveChanges();
            }

        }
        public async Task<ManageExamDTOs> ListExam(int? courseID)
        {
            List<ExamDTOs> listExamDTOs = new List<ExamDTOs>();
            string courseName = "";
            try
            {

                var listExam = await _context.Exams.Include(u => u.Cource).Where(x => x.CourceId == courseID).ToListAsync();
                var course = await _context.Courses.FirstOrDefaultAsync(x => x.Id == courseID);
                courseName = course.Name;
                if (listExam.Count != 0)
                {
                    
                    foreach (var exam in listExam)
                    {
                        ExamDTOs item = new ExamDTOs
                        {
                            examID = exam.Id,
                            examName = exam.Name,
                            average = exam.Average,
                        };
                        listExamDTOs.Add(item);
                    }
                }
                
                


            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

            }
            return new ManageExamDTOs
            {
                courseID = courseID,
                courseName = courseName,
                exams = listExamDTOs
            };


        }
        public async Task<bool> UpdateExam(List<ExamDTOsRequest> exams, int courseID)
        {

            try
            {
                List<int> IdsOfNewExam = new List<int>();
                List<int> IdsOfUpdateExam = new List<int>();

                foreach (var item in exams)
                {
                    Exam exam = await _context.Exams.FirstOrDefaultAsync(x => x.Id == item.examID && x.CourceId == courseID);
                    if (exam != null)
                    {
                        exam.Name = item.examName;
                        exam.Average = (double)item.average;
                        _context.Exams.Update(exam);
                        _context.SaveChanges();

                    }
                    else
                    {
                        exam = new();
                        exam.CourceId = courseID;
                        exam.Name = item.examName;
                        exam.Average = (double)item.average;
                        _context.Exams.Add(exam);
                        _context.SaveChanges();
                        var newExam = await _context.Exams.OrderBy(x => x.Id).LastAsync();
                        IdsOfNewExam.Add(newExam.Id);
                    }
                    IdsOfUpdateExam.Add(exam.Id);




                }
                var listDBExams = await _context.Exams.Where(x => x.CourceId == courseID).ToListAsync();
                foreach(var examItem in listDBExams)
                {
                    if (!IdsOfUpdateExam.Contains(examItem.Id))
                    {
                        DeleteExam(examItem.Id);
                    }
                }
                if(IdsOfNewExam.Count > 0)
                {
                    var classes = await _context.Classes.Where(x => x.CourceId == courseID).ToListAsync();
                    List<int> studentClassIDs = new List<int>();
                    foreach (var classItem in classes)
                    {
                        var studentClasses = await _context.StudentClasses.Where(x => x.ClassId == classItem.Id).ToListAsync();
                        foreach (var studentClass in studentClasses)
                        {
                            studentClassIDs.Add(studentClass.Id);
                        }
                        
                    }
                    foreach (var examID in IdsOfNewExam)
                    {
                        foreach (var studentClassId in studentClassIDs)
                        {
                            ExamClass examClass = new ExamClass()
                            {
                                ExamId = examID,
                                StudentClassId = studentClassId
                            };
                            _context.ExamClasses.Add(examClass);
                            _context.SaveChanges();

                        }

                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false;

            }



        }
        public async Task<List<string>> ValidateExam(List<ExamDTOsRequest> exam)
        {
            List<string> errors = new List<string>();
            try
            {
                var duplicateExamNames = exam.GroupBy(e => e.examName)
                                     .Where(g => g.Count() > 1)
                                     .Select(g => g.Key)
                                     .ToList();

                if (duplicateExamNames.Any())
                {
                    foreach (var examName in duplicateExamNames)
                    {
                        errors.Add("Bài kiểm tra tên " + examName + " bị trùng lặp!");
                    }
                }
                double? total = 0;
                bool checkNull = true;
                foreach (var item in exam)
                {
                    total += item.average;
                    if (string.IsNullOrEmpty(item.examName) || string.IsNullOrEmpty(item.average.ToString()))
                    {
                        checkNull = false;

                    }
                    if (item.average <= 0)
                    {
                        errors.Add("Trọng số bài kiểm tra tên " + item.examName + " ,id " + item.examID + " phải lớn hơn 0");
                    }
                    if(item.examName.Length > 50)
                    {
                        errors.Add("Độ dài tên bài kiểm tra không quá 50 kí tự");
                    }
                }
                if (checkNull == false)
                {
                    errors.Add("Tên bài kiểm tra và trọng số phải được điền");
                }
                if (total != 100.0)
                {
                    errors.Add("Tổng trọng số phải bằng 100");
                }
                return errors;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return new List<string> { ex.Message };

            }


        }
        public async Task<List<FilterOfCourse>> GetCourseOfStudent(string username)
        {

            List<FilterOfCourse> list = new List<FilterOfCourse>();
            try
            {
                var student = await _context.StudentProfiles.FirstOrDefaultAsync(x => x.UserName.Equals(username));
                var listClass = await _context.StudentClasses.Where(x => x.StudentId.Equals(student.Id)).ToListAsync();
                foreach (var item in listClass)
                {
                    var classItem = await _context.Classes.Include(x => x.Cource).FirstOrDefaultAsync(x => x.Id == item.ClassId);
                    FilterOfCourse course = new FilterOfCourse()
                    {
                        courseID = classItem.CourceId,
                        courseName = classItem.Cource.Name,
                        classID = classItem.Id,
                        className = classItem.Name 
                    };
                    list.Add(course);

                }
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;

            }

        }
        public void DeleteExam(int id)
        {

            
            try
            {
                var exams =  _context.ExamClasses.Where(x => x.ExamId == id).ToList();
                foreach (var item in exams)
                {
                 
                    _context.ExamClasses.Remove(item);
                    _context.SaveChanges();
                }
                var exam =  _context.Exams.FirstOrDefault(X => X.Id == id);
                _context.Exams.Remove(exam);
                _context.SaveChanges();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

            }

        }
        public async Task<List<MarkReportDTOs>> GetMarkOfStudent(string username,int courseID,int classID)
        {

            List<MarkReportDTOs> list = new List<MarkReportDTOs>();
            try
            {
                var classItem = await _context.Classes.Where(x => x.Id == classID).FirstOrDefaultAsync();
                var student = await _context.StudentProfiles.Where(x => x.UserName.Equals(username)).FirstOrDefaultAsync();
                var studentClass = await _context.StudentClasses.Where(x => x.ClassId == classID && x.StudentId.Equals(student.Id)).FirstOrDefaultAsync();
                var exams = await _context.Exams.Where(x => x.CourceId == courseID).ToListAsync();
                foreach (var exam in exams)
                {
                    var item = _context.ExamClasses.Include(x => x.Exam).FirstOrDefault(x => x.ExamId == exam.Id && x.StudentClassId == studentClass.Id);
                    list.Add(new MarkReportDTOs()
                    {
                        ExamId = item.ExamId,
                        Evaluate= item.Evaluate,
                        ExamName = item.Exam.Name,
                        Mark = item.Mark,
                        Average = item.Exam.Average
                    });                    
                }
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;

            }

        }
    }
  }
