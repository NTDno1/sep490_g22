﻿using BusinessObject.Models;
using BusinessObject.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.SyllabusDTO;
using BusinessObject.DTOs.OrderDTO;
using BusinessObject.DTOs.SQADTO;

namespace DataAccess.Repositories
{
    public class IOrderImplement : IOrderRepository
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        public IOrderImplement(IMapper mapper, sep490_g22Context context)
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<ViewOrderDTO> Order(string orderId, string userId, string roleName)
        {
            try
            {
                Order listOrderByEmployee = new Order();
                if (roleName != null && roleName == "Employee" && userId != null)
                {
                     listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).FirstOrDefaultAsync(u => u.Id.ToString() == orderId && u.EmployeeId.ToString() == userId);
                }
                if (roleName != null && roleName == "Student" && userId != null)
                {
                     listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).FirstOrDefaultAsync(u => u.Id.ToString() == orderId && u.StudentId.ToString() == userId);
                }
                return _mapper.Map<ViewOrderDTO>(listOrderByEmployee);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<Order> OrderMore(string orderId)
        {
            try
            {
                var listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).FirstOrDefaultAsync(u => u.Id.ToString() == orderId);
                return listOrderByEmployee;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<List<ViewOrderDTO>> ListOrder(string roleName, string userId, string? toMonth, string? fromMonth, string? year, string centerId)
        {
            try
            {
                List<Order> listOrderByEmployee = new List<Order>();
                if (roleName != null && roleName == "Employee" && userId != null)
                {
                    if (!string.IsNullOrEmpty(toMonth) && !string.IsNullOrEmpty(fromMonth) && !string.IsNullOrEmpty(year))
                    {
                        int toMonthInt = int.Parse(toMonth);
                        int fromMonthInt = int.Parse(fromMonth);
                        int yearInt = int.Parse(year);
                        listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).Include(u=>u.OrderDetails)
                            .Where(o => o.CreateDate.Value.Month >= fromMonthInt && o.CreateDate.Value.Month <= toMonthInt && o.CreateDate.Value.Year == yearInt)
                            .ToListAsync();
                        //listOrderByEmployee = listOrderByEmployee
                        //    .ToList();
                    }
                    else
                    {
                        listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).Include(u => u.OrderDetails).Where(u=>u.Employee.CenterId.ToString() ==centerId).ToListAsync();
                    }
                }
                if(roleName != null && roleName == "Student" && userId != null)
                {
                    if (!string.IsNullOrEmpty(toMonth) && !string.IsNullOrEmpty(fromMonth) && !string.IsNullOrEmpty(year))
                    {
                        int toMonthInt = int.Parse(toMonth);
                        int fromMonthInt = int.Parse(fromMonth);
                        int yearInt = int.Parse(year);
                        listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).Include(u => u.OrderDetails).Where(u => u.StudentId.ToString() == userId && u.Status == 1).ToListAsync();
                        listOrderByEmployee = listOrderByEmployee
                            .Where(o => o.CreateDate?.Month >= fromMonthInt && o.CreateDate?.Month <= toMonthInt && o.CreateDate?.Year == yearInt)
                            .ToList();
                    }
                    else
                    {
                        listOrderByEmployee = await _context.Orders.Include(u => u.Student).Include(u => u.Employee).Include(u => u.OrderDetails).Where(u => u.StudentId.ToString() == userId && u.Status == 1).ToListAsync();
                    }
                }
                return _mapper.Map<List<ViewOrderDTO>>(listOrderByEmployee);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<List<ViewOrderDetailDTO>> ListOrderDetail(string orderId, string roleName, string userId)
        {
            try
            {
                List<OrderDetail> listOrdetail = new List<OrderDetail>();
                if (roleName != null && roleName == "Employee" && userId != null)
                {
                    listOrdetail = await _context.OrderDetails.Include(u => u.Course).Include(u => u.Order).Include(u => u.Class).Where(u => u.OrderId.ToString() == orderId).ToListAsync();

                }
                if (roleName != null && roleName == "Student" && userId != null)
                {
                    listOrdetail = await _context.OrderDetails.Include(u => u.Course).Include(u => u.Order).Include(u => u.Class).Where(u => u.OrderId.ToString() == orderId && u.Order.StudentId.ToString() == userId && u.Status == 1).ToListAsync();
                }
                if (listOrdetail != null)
                {
                    return _mapper.Map<List<ViewOrderDetailDTO>>(listOrdetail);
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<(bool, List<string> , ViewOrderDetailDTO)> AddOrderDetail(AddOrderDetailDTO addOrderDetail, string orderDetailId)
        {
            try
            {
                var orderd = await _context.Orders.FirstOrDefaultAsync(u => u.Id == addOrderDetail.OrderDetailId);
                var orderDetaid = await _context.OrderDetails.Include(u => u.Order).Where(u => u.CourseId == addOrderDetail.CourseId && u.Order.StudentId == orderd.StudentId && u.Status<3).ToListAsync();
                if (orderDetaid.Count > 0)
                {
                    var student = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id == orderDetaid.First().Order.StudentId);
                    var course = await _context.Courses.FirstOrDefaultAsync(u => u.Id == orderDetaid.First().CourseId);
                    return (false, new List<string> { $"Tạo Mới Hóa Đơn Thất Bại: Học Sinh {student.FirstName} Đang Chờ Thanh Toán Hoặc Đang Học Khóa Học :{course.Name}" }, null);
                }
                OrderDetail orderDetail = new OrderDetail
                {
                    CourseId = addOrderDetail.CourseId, 
                    Price = addOrderDetail.Price,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    Discount = addOrderDetail.Discount,
                    Note = addOrderDetail.Note,
                    Status = 0,
                    StatusPass = 0,
                    OrderId = addOrderDetail.OrderDetailId,
                };
                _context.OrderDetails.Add(orderDetail);
                await _context.SaveChangesAsync();
                int orderId = orderDetail.Id;
                Console.WriteLine("Đây là orderid ordeimplemen 80: " + orderId);
                return (true, new List<string> { "Tạo Mới Hóa Đơn Thành Công" }, _mapper.Map<ViewOrderDetailDTO>(orderDetail));
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Tạo Mới Hóa Đơn Thất Bại Vui lòng liên hệ với bên quản trị dự án để được hỗ trợ sớm nhất. 154" }, null);
            }
        }

        public async Task<(bool, List<string>, ViewOrderDTO)> AddOrder(AddOrderDTO addOrderDTO, string employeeId)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random random = new Random();
                int length = 12;
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                Order order = new Order()
                {
                    Name = addOrderDTO.Name,
                    Title = addOrderDTO.Title,
                    OrderCode = randomString,
                    Note = addOrderDTO.Note,
                    UpdateDate = DateTime.Now,
                    CreateDate = DateTime.Now,
                    Status = 0,
                    StudentId = addOrderDTO.StudentId,
                    EmployeeId = Guid.Parse(employeeId),
                };
                _context.Orders.Add(order);
                await _context.SaveChangesAsync();
                int orderId = order.Id;
                Console.WriteLine("Đây là orderid ordeimplemen 110: " + orderId);
                return (true, new List<string> { "Tạo Mới Hóa Đơn Thành Công" }, _mapper.Map<ViewOrderDTO>(order));
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Tạo Mới Hóa Đơn Thất Bại. Mã Lỗi: " + ex.Message }, null);
            }
        }

        public async Task<(bool, List<string>)> DeleteOrderDetail(string orderDetailId)
        {
            try
            {
                var orderDetail = await _context.OrderDetails.FirstOrDefaultAsync(u => u.Id.ToString() == orderDetailId);
                if (orderDetail != null && orderDetail.Status == 0)
                {
                    _context.OrderDetails.Remove(orderDetail);
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Xóa Thành Công Sản Phẩm" });
                }
                return (false, new List<string> { "Không thể xóa vật phẩm liên hệ với bên quản trị hệ thống để có thể nhận hỗ trợ sớm nhất" });

            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Không thể xóa vật phẩm liên hệ với bên quản trị hệ thống để có thể nhận hỗ trợ sớm nhất" });
            }
        }
        public async Task<(bool, List<string>)> DeteleOrder(string orderId)
        {
            try
            {
                var deleteOrder = await _context.Orders.FirstOrDefaultAsync(u => u.Id.ToString() == orderId);
                if (deleteOrder != null && deleteOrder.Status == 0)
                {
                    var orderDetail = await _context.OrderDetails.Where(u => u.OrderId.ToString() == orderId).ToListAsync();
                    if(orderDetail != null)
                    {
                            _context.OrderDetails.RemoveRange(orderDetail);
                    }
                    _context.Orders.Remove(deleteOrder);
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Xóa Hóa Đơn Thành Công" });
                }
                return (false, new List<string> { "Không thể xóa hóa đơn liên hệ với bên quản trị hệ thống để có thể nhận hỗ trợ sớm nhất" });
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Không thể xóa hóa đơn liên hệ với bên quản trị hệ thống để có thể nhận hỗ trợ sớm nhất" });
            }
        }
        public async Task<(bool, List<string>)> Payment(string orderID, string employeeId)
        {
            try
            {
                var order = await _context.Orders.FirstOrDefaultAsync(u => u.Id.ToString() == orderID && u.EmployeeId.ToString() == employeeId && u.Status == 0 );
                var orderdetail = await _context.OrderDetails.Where(u => u.OrderId.ToString() == orderID).ToListAsync();
                if (order != null && orderdetail != null)
                {
                    foreach(var item in orderdetail)
                    {
                        item.Status = 1;
                        
                    }
                    order.Status= 1;
                    WaitingStudent waitingStudent = new WaitingStudent()
                    {
                        Id = 0,
                        OrderId = int.Parse(orderID),
                        Status = 0,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        StudentId = order.StudentId,
                        EmployeeId = order.EmployeeId,
                    };
                    await _context.WaitingStudents.AddAsync(waitingStudent);
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Tạo Hóa Đơn Thành Công Và Thêm Học Sinh Vào Trong Danh Sách Chờ" });
                }
                return (false, new List<string> { "Tạo Hóa Đơn Thất Bại. " });
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Tạo Hóa Đơn Thất Bại. Mã Lỗi: " + ex.Message });
            }
        }
    }
}