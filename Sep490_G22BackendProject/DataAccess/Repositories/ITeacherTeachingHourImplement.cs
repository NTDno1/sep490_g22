﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SchedualDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherTeachingHourDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ITeacherTeachingHourImplement : ITeacherTeachingHourRepositorys
    {
        readonly sep490_g22Context _context = new();
        private readonly IMapper _mapper;
        public ITeacherTeachingHourImplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _context= context;
        }
        public async Task<(bool, List<string>?)> AddTeacherTeachingHour(ViewSchedualDTO viewSchedualDTO, int timeTeaching)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var teachingHour = new TeacherTeachingHour
                    {
                        TeacherId = viewSchedualDTO.TeacherId,
                        ScheduleId = viewSchedualDTO.Id,
                        TimeTeaching = timeTeaching,
                        Note = "Add through attendance "
                    };
                    _context.TeacherTeachingHours.Add(teachingHour);
                    await _context.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Error: " + ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return (false, null);
        }
        public async Task<bool> CheckScheduleInTeachHour(string scheduleId)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var teacherHour = await _context.TeacherTeachingHours.Where(u => u.ScheduleId.ToString() == scheduleId).ToListAsync();
                    if(teacherHour!= null && teacherHour.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                //}
            }
            catch (Exception)
            {
                return (false);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<List<ViewTeacherTeachingHourDTO>> ListTeacherTeachingHour(string teacherId)
        {
            List<TeacherTeachingHour> teacherProfile = new List<TeacherTeachingHour>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    teacherProfile = await _context.TeacherTeachingHours.Where(u => u.TeacherId.ToString() == teacherId).Include(u => u.Schedule).ThenInclude(u => u.Class)
                            .Include(u => u.Schedule).ThenInclude(u => u.SlotDate).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewTeacherTeachingHourDTO>>(teacherProfile);
        }
        public async Task<List<ViewTeacherTeachingHourDTO>> ListTeacherTeachingHourAdminSide(string centerId)
        {
            List<TeacherTeachingHour> teacherProfiles = new List<TeacherTeachingHour>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getteacher = await _context.TeacherProfiles.Where(u=> u.CenterId.ToString() == centerId).ToListAsync();
                    foreach(var profile in getteacher)
                    {
                       var teacherProfile = await _context.TeacherTeachingHours.Where(u => u.TeacherId == profile.Id).Include(u=>u.Schedule).ThenInclude(u=>u.Class)
                            .Include(u=>u.Schedule).ThenInclude(u=>u.SlotDate).ToListAsync();
                        teacherProfiles.AddRange(teacherProfile);
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewTeacherTeachingHourDTO>>(teacherProfiles);
        }
    }
}
