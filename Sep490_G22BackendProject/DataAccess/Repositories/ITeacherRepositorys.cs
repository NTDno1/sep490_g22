﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ITeacherRepositorys
    {
        Task<List<ViewTeacherDTO>> ListTeacherAdminSide(string? centerId);
        Task<List<ViewTeacherDTO>> ListTeacherClientSide(string? centerId);
        Task<(bool, List<string>)> UpdateTeacher(UpdateProfileTeacherDTO updateProfileTeacherDTO, bool avaimg, bool cetimg, string avataimg, string cetiimgs);
        Task<(bool, List<string>)> AddTeacher(AddTeacherDTOs addTeacherDTO, bool avaimg, bool cetimg, string avataimg, string cetiimgs, string centerId, string userId);
        Task<(bool, List<string>)> DeleteTeacher(string id);
        Task<(bool, List<string>)> DeleteListTeacher(List<string> id);
        Task<(bool, List<string>)> DeactivateTeacher(string id);
        Task<(bool, List<string>)> ActivateTeacher(string id);
        Task<ViewTeacherDTO?> TeacherDetail(string id);
    }
}
