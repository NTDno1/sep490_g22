﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface INotificationClassRepositorys
    {
        Task<List<ViewNotificationClassDTO>?> ListNotificationClass(string userId, string roleName);
        Task<(bool, List<string>)> AddNotificationClass(AddNotificationClass addNotificationClass);
    }
}
