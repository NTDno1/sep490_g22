﻿using BusinessObject.DTOs;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IAuthenticationRepository
    {
       public Account? checkLogin(UserDTO userRequest);
        public List<Account?> GetAccount();
       public Account? checkLoginAccount(UserDTO userRequest,Boolean isRefresh);
        public (int, StudentProfile?) checkLoginStudent(UserDTO userRequest, Boolean isRefresh);
        public (int, TeacherProfile?) checkLoginTeacher(UserDTO userRequest, Boolean isRefresh);
        public (int, Employee?) checkLoginEmployee(UserDTO userRequest, Boolean isRefresh);
        public Center? GetCenter(string id);
    }
}
