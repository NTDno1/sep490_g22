﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IClassImplement : IClassRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        private readonly ITeacherRepositorys _teacherRepositorys;
        public IClassImplement(HashPassword hashPassword, IMapper mapper, ITeacherRepositorys teacherRepositorys, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context;
            _teacherRepositorys = teacherRepositorys;
        }
        public async Task<List<ViewClassDTO>> GetListClassTeachertSide(string userId)
        {
            List<Class> classes = new List<Class>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getUser = _context.TeacherProfiles.FirstOrDefault(u => u.Id.ToString() == userId);
                    if (getUser == null)
                    {
                        throw new Exception("getUser is null");
                    }
                    classes = await _context.Classes.Where(u => u.TeacherId == getUser.Id && u.CenterId == getUser.CenterId && (u.Status == 2 || u.Status == 1))
                        .Include(u => u.Scheduals).Include(u => u.Teacher).Include(u => u.AdminCenter).Include(u => u.Cource)
                        .Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).Include(u => u.ClassSlotDates).ThenInclude(u => u.Room).ThenInclude(u=>u.Type)
                        .Include(u => u.StudentClasses)
                        .ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassDTO>>(classes);
        }
        public async Task<List<ViewClassDTO>> GetListClassStudentSide(string userId)
        {
            List<Class> classes = new List<Class>();
            List <StudentClass> studentClasses = new List<StudentClass>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getUser = _context.StudentProfiles.FirstOrDefault(u => u.Id.ToString() == userId);
                    if (getUser == null)
                    {
                        throw new Exception("getUser is null");

                    }
                    var listStudentClass = await _context.StudentClasses.Where(u => u.StudentId == getUser.Id).ToListAsync();
                    var uniqueClassIds = listStudentClass.Select(c => c.ClassId).Distinct().ToList();
                    foreach(var item in uniqueClassIds)
                    {
                        var classs = _context.Classes.Include(u => u.Scheduals).Include(u => u.Teacher)
                            .Include(u => u.AdminCenter).Include(u => u.Cource).Include(u => u.ClassSlotDates)
                            .ThenInclude(u => u.Slot).Include(u=>u.ClassSlotDates).ThenInclude(u=>u.Room).ThenInclude(u=>u.Type)
                            .Include(u => u.StudentClasses)
                            .FirstOrDefault(u => u.Id == item.Value && (u.Status == 2 || u.Status == 1));
                        if(classs != null)
                        {
                            classes.Add(classs);
                        }
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassDTO>>(classes);
        }
        public async Task<List<ViewClassDTO>> GetListClassAdminSide(string userId)
        {
            List<Class> classes = new List<Class>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getUser = _context.Employees.FirstOrDefault(u => u.Id.ToString() == userId);
                    if (getUser == null)
                    {
                        throw new Exception("getUser is null");
                    }
                    classes = await _context.Classes.Where(u => u.CenterId == getUser.CenterId).
                        Include(u=>u.Scheduals).Include(u => u.Teacher).Include(u => u.Cource).
                        Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).Include(u => u.ClassSlotDates).
                        ThenInclude(u => u.Room).ThenInclude(u=> u.Type).Include(u => u.StudentClasses).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassDTO>>(classes);
        }
        public async Task<List<ViewClassDTO>> GetListClassToAttend(string userId, int status)
        {
            List<Class> classes = new List<Class>();
            try
            {
                if(status != null && int.TryParse(status.ToString(), out _))
                {
                    var getUser = _context.Employees.FirstOrDefault(u => u.Id.ToString() == userId);
                    if (getUser == null)
                    {
                        throw new Exception("getUser is null");
                    }
                    classes = await _context.Classes.Where(u => u.CenterId == getUser.CenterId).
                        Include(u => u.Scheduals).Include(u => u.Teacher).Include(u => u.Cource).
                        Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).Include(u => u.ClassSlotDates).
                        ThenInclude(u => u.Room).ThenInclude(u => u.Type).Include(u => u.StudentClasses).ToListAsync();
                }
                else
                {
                    var getUser = _context.Employees.FirstOrDefault(u => u.Id.ToString() == userId);
                    if (getUser == null)
                    {
                        throw new Exception("getUser is null");
                    }
                    classes = await _context.Classes.Where(u => u.CenterId == getUser.CenterId && (u.Status == 1)).
                        Include(u => u.Scheduals).Include(u => u.Teacher).Include(u => u.Cource).
                        Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).Include(u => u.ClassSlotDates).
                        ThenInclude(u => u.Room).ThenInclude(u => u.Type).Include(u => u.StudentClasses).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassDTO>>(classes);
        }
        public async Task<ViewClassDTO> ClassDetail(string classId, string? checkUpdate)
        {
            Class classes = new Class();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if(checkUpdate!= null)
                    {
                        classes = await _context.Classes.Include(u => u.Teacher).Include(u => u.AdminCenter).Include(u => u.Cource)
                        .Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).ThenInclude(u => u.ClassSlotDates)
                        .ThenInclude(u => u.Room).ThenInclude(u => u.Type).FirstOrDefaultAsync(u => u.Id.ToString() == classId && u.Status == 0);

                    }
                    else
                    {
                        classes = await _context.Classes.Include(u => u.Teacher).Include(u => u.AdminCenter).Include(u => u.Cource)
                        .Include(u => u.ClassSlotDates).ThenInclude(u => u.Slot).ThenInclude(u => u.ClassSlotDates)
                        .ThenInclude(u => u.Room).ThenInclude(u => u.Type).FirstOrDefaultAsync(u => u.Id.ToString() == classId);
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<ViewClassDTO>(classes);
        }
        public async Task<bool> ChangeStatusClass(int classId, int status)
        {
            Class classes = new Class();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    classes = await _context.Classes.FirstOrDefaultAsync(u => u.Id == classId);
                    if(classes!= null)
                    {
                        classes.Status = status;
                        await _context.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ViewClassDTO> GetNewClass()
        {
            Class lastItem = new Class();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    lastItem = await _context.Classes
                    .OrderByDescending(u => u.Id)
                    .Include(u => u.Teacher)
                    .Include(u => u.AdminCenter)
                    .Include(u => u.Cource)
                    .Include(u => u.ClassSlotDates)
                    .ThenInclude(u => u.Slot)
                    .FirstOrDefaultAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<ViewClassDTO>(lastItem);
        }
        public async Task<List<ViewStudentClassDTO>> ListStudentInClass(string classId)
        {
            List<StudentClass> classes = new List<StudentClass>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    classes = await _context.StudentClasses.Where(u => u.ClassId.ToString() == classId)
                        .Include(u => u.Student).Include(u => u.Class).ThenInclude(u => u.Scheduals)
                        .ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewStudentClassDTO>>(classes);
            //return classes;
        }
        public async Task<bool> AddClass(AddClassDTOs addClassDTOs)
        {
            //var httpRequest = _httpContextAccessor.HttpContext.Request;
            //var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            try
            {
                Class classes = new Class
                {
                    Name = addClassDTOs.Name,
                    Code = addClassDTOs.Code,
                    CourceId = addClassDTOs.CourceId,
                    AdminCenterId = addClassDTOs.AdminCenterId,
                    TeacherId = addClassDTOs.TeacherId,
                    CenterId = addClassDTOs.CenterId,
                    //NotificationId= addClassDTOs .NotificationId,
                    Status = addClassDTOs.Status,
                    SlotNumber = addClassDTOs.SlotNumber,
                    StartDate = addClassDTOs.StartDate,
                    DateCreate = DateTime.Now,
                    DateUpdate = DateTime.Now,
                };
                //using (var _context = new sep490_g22Context())
                //{
                    await _context.AddAsync(classes);
                    await _context.SaveChangesAsync();
                //}
                await _teacherRepositorys.ActivateTeacher(addClassDTOs.TeacherId.ToString());
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return false;
        }
        public async Task<(bool, List<string>)> UpdateClass(UpdateClassDTO updateClassDTO)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    bool exists;
                    var messages = new List<string>();
                    var updateClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id == updateClassDTO.Id);
                    if (updateClass != null)
                    {
                        if(updateClass.Status == 4)
                    {
                        updateClass.TeacherId= updateClassDTO.TeacherId;
                        _context.Update(updateClass);
                        await _context.SaveChangesAsync();
                    }
                    }
                    var classSlotDateCheck = await _context.ClassSlotDates.Where(u => u.ClassId == updateClassDTO.Id).ToListAsync();
                //var get()
                if (classSlotDateCheck != null && classSlotDateCheck.Count() > 0)
                {
                    messages.Add($"Bạn Cần xóa bỏ danh sách lich học trước khi cập nhật");
                }
                var classSlotDate = new AddClassSlotDateDTO();
                //foreach (var item in getClass.classSlotDates)
                {
                    //    exists = await _context.ClassSlotDates.Include(u => u.Class).Include(u => u.Slot).AnyAsync(csd => csd.ClassId == updateClassDTO.Id && csd.SlotId == item.Id && csd.Class.Status == 1);
                    //    if (exists)
                    //    {
                    //        messages.Add($"Item already exists in the database: ClassName: {getClass.Name}, SlotName: {classSlotDate.SlotName}");
                    //    }
                    //    exists = await _context.ClassSlotDates.Include(u => u.Class).Include(u => u.Room).Include(u => u.Slot).AnyAsync(csd => csd.SlotId == item.Id && csd.RoomId == item.RoomId && csd.Class.Status == 1);
                    //    if (exists)
                    //    {
                    //        messages.Add($"Item already exists in the database: SlotName: {classSlotDate.SlotName}, RoomName: {classSlotDate.RoomName}");
                    //    }
                //    exists = await _context.ClassSlotDates.Include(u => u.Class).Include(u => u.Slot).AnyAsync(csd => csd.TeacherId == getClass.TeacherId && csd.SlotId == item.Id && csd.Class.Status == 1);
                //if (exists)
                //{
                //    messages.Add($"Item already exists in the database: SlotName: {classSlotDate.SlotName}, TeacherName: {getClass.TeacherName}");
                //}
                    //    exists = await _context.ClassSlotDates.Include(u => u.Class).Include(u => u.Slot).Include(u => u.Room).AnyAsync(csd => csd.ClassId == updateClassDTO.Id && csd.SlotId == item.Id && csd.RoomId == item.RoomId && csd.TeacherId == getClass.TeacherId && csd.Class.Status == 1);
                    //    if (exists)
                    //    {
                    //        messages.Add($"Item already exists in the database: ClassId: {updateClassDTO.Id}, SlotId: {item.Id}, RoomId: {item.RoomId}, TeacherId: {getClass.TeacherName}");
                    //    }
                }
                if (messages != null && messages.Count > 0)
                        {
                            return (false, messages);
                        }
                        if(updateClass != null)
                        {
                        updateClass.Name = updateClassDTO.Name; 
                        //updateClass.Code = updateClassDTO.Code;
                        updateClass.CourceId = updateClassDTO.CourceId;
                        updateClass.AdminCenterId = updateClassDTO.AdminCenterId;
                        updateClass.TeacherId = updateClassDTO.TeacherId;
                        updateClass.CenterId = updateClassDTO.CenterId;
                        updateClass.SlotNumber = updateClassDTO.SlotNumber;
                        updateClass.StartDate = updateClassDTO.StartDate;
                        updateClass.DateUpdate = DateTime.Now;
                        _context.Update(updateClass);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return (false, new List<string> {"Update False"});
                    }
                //}
                await _teacherRepositorys.ActivateTeacher(updateClassDTO.TeacherId.ToString());
                return (true, new List<string> { "Update Success" });
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Error: " + ex.Message);
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> CheckAdd(string classId)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var updateClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id == int.Parse(classId));
                    if (updateClass != null)
                    {
                        updateClass.SlotId = 1;
                        _context.Update(updateClass);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Update Success" });
                    }
                    else
                    {
                        return (false, new List<string> { "Update False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> CloseClass(string classId)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var updateClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id == int.Parse(classId));
                    if (updateClass != null)
                    {
                        updateClass.Status = 3;
                        _context.Update(updateClass);
                        await _context.SaveChangesAsync();
                        List<Schedual> listSchedual = await _context.Scheduals.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                        await _context.Scheduals.Where(u => u.ClassId.ToString() == classId).ForEachAsync(s => s.Staus = 4);
                        await _context.SaveChangesAsync();
                    return (true, new List<string> { "Classes Closed" });
                    }
                    else
                    {
                        return (false, new List<string> { "An error occurred while closing the class" });
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteClass(string classId)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var studenclass = await _context.StudentClasses.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                    var classSlotDate = await _context.ClassSlotDates.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                    var shedule = await _context.Scheduals.Where(u => u.ClassId.ToString() == classId).ToListAsync();
                    var removeClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id == int.Parse(classId));
                    if(removeClass.Status == 0)
                    {
                        _context.RemoveRange(studenclass);
                        _context.RemoveRange(classSlotDate);
                        _context.RemoveRange(shedule);
                        _context.Remove(removeClass);
                        await _context.SaveChangesAsync();
                        Console.WriteLine("đã vào đây");
                        return (true, new List<string> { "Classes Deleted" });
                    }
                    else
                    {
                        return (false, new List<string> { "An error occurred while Delete the class" });
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
