﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using DataAccess.Validation;
using Microsoft.EntityFrameworkCore;
using BusinessObject.DTOs.EmployeeDTO;
using System.Xml.Linq;
using BusinessObject.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using BusinessObject.DTOs.TeacherDTO;

namespace DataAccess.Repositories
{
    public class IAccountImplement : IAccountRepository
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        readonly HashPassword _hashPassword;
        protected ApiResponse _response;
        public IAccountImplement(HashPassword hashPassword, IMapper mapper, IWebHostEnvironment webHostEnvironment, sep490_g22Context context)
        {
            _hashPassword = hashPassword;
            _mapper = mapper;
            _response = new();
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }
        public async Task DeleteEmployee(string id)
        {
            try
            {

                if (id != null)
                {
                    //using (var _context = new sep490_g22Context())
                    //{
                        var employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        _context.Remove(employee);
                        await _context.SaveChangesAsync();
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task UpdateEmployee(UpdateEmployeeDTO updateEmployeeDTO)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    Employee employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id == updateEmployeeDTO.Id);
                    if (employee != null)
                    {
                        employee.Name = updateEmployeeDTO.Name;
                        employee.Adress = updateEmployeeDTO.Adress;
                        employee.Email = updateEmployeeDTO.Email;
                        employee.PhoneNumber = updateEmployeeDTO.PhoneNumber;
                        employee.Dob = updateEmployeeDTO.Dob;
                        employee.DateUpdate = DateTime.Now;
                    };
                    await _context.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        // public async Task AddEmployee(AddEmployeeDTO addEmployeeDTO, string accountId)
        // {
        //     Guid guid;
        //     bool isGuid = Guid.TryParse(accountId, out guid);
        //     try
        //     {
        //         Role roles = new Role();
        //         //using (var _context = new sep490_g22Context())
        //         //{
        //             roles = await _context.Roles.FirstOrDefaultAsync(u => u.Id == addEmployeeDTO.RoleId);
        //         //}
        //         var addEmployee = new Employee
        //         {
        //             Id = Guid.NewGuid(),
        //             Name = addEmployeeDTO.Name,
        //             UserName = addEmployeeDTO.UserName,
        //             RoleId = addEmployeeDTO.RoleId,
        //             CenterId = addEmployeeDTO.CenterId,
        //             AccountId = guid,
        //             PassWord = _hashPassword.HashPass($"{addEmployeeDTO.PassWord}"),
        //             DateCreate = DateTime.Now,
        //             DateUpdate = DateTime.Now,
        //         };
        //         using (var _context = new sep490_g22Context())
        //         {
        //             await _context.AddAsync(addEmployee);
        //             await _context.SaveChangesAsync();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Console.WriteLine("Error: " + ex.Message);
        //     }
        //     //finally
        //     //{
        //     //    if (_context != null)
        //     //    {
        //     //        await _context.DisposeAsync();
        //     //    }
        //     //}
        // }
        public async Task<List<ListEmployeeDTO>> GetEmployees()
        {
            List<Employee> employee = new List<Employee>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    employee = await _context.Employees.Include(x => x.Center).Include(u => u.Account).Include(u => u.Role).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ListEmployeeDTO>>(employee);
        }



        public Account getAccountByUsername(string username)
        {
            try
            {
                return _context.Accounts.SingleOrDefault(x => x.UserName.Equals(username));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public void UpdateAccount(Account? account)
        {
            try
            {
                if (account == null)
                {
                    return;
                }
                _context.Accounts.Update(account);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public StudentProfile getStudentByUsername(string username)
        {
            try
            {
                return _context.StudentProfiles.SingleOrDefault(x => x.UserName.Equals(username));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public void UpdateStudent(StudentProfile? studentProfile)
        {
            try
            {
                if (studentProfile == null)
                {
                    return;
                }
                _context.StudentProfiles.Update(studentProfile);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public TeacherProfile getTeacherByUsername(string username)
        {        
              
            try
            {
                return _context.TeacherProfiles.SingleOrDefault(x => x.UserName.Equals(username));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public void UpdateTeacher(TeacherProfile? teacherProfile)
        {
            try
            {
                if (teacherProfile == null)
                {
                    return;
                }
                _context.TeacherProfiles.Update(teacherProfile);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
        public Employee getEmployeeByUsername(string username)
        {
            try
            {
                return _context.Employees.SingleOrDefault(x => x.UserName.Equals(username));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public void UpdateEmployee(Employee? employee)
        {
            try
            {
                if (employee == null)
                {
                    return;
                }
                _context.Employees.Update(employee);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
         public async Task<(bool, string)> GetUser(string roleName, string userName, string email)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random random = new Random();
                int length = 8;
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //using (var _context = new sep490_g22Context())
                //{
                    if (roleName != null && roleName == "Student")
                    {
                       var user = await  _context.StudentProfiles.FirstOrDefaultAsync(x => x.UserName == userName 
                       && x.Email == email);
                        if(user != null)
                        {
                            user.PassWord = _hashPassword.HashPass($"{randomString}");
                            await _context.SaveChangesAsync();
                            return (true, randomString);
                        }
                        return (false, "Wrong User Name Or Email");
                    }
                    if (roleName != null && roleName == "Teacher")
                    {
                        var user =  await _context.TeacherProfiles.FirstOrDefaultAsync(x => x.UserName == userName
                                              && x.Email == email);
                        if(user != null)
                        {
                            user.PassWord = _hashPassword.HashPass($"{randomString}");
                            await _context.SaveChangesAsync();
                            return (true, randomString);
                        }
                        return (false, "Wrong User Name Or Email");
                    }
                    if (roleName != null && roleName == "Employee")
                    {
                        var user =  await _context.Employees.FirstOrDefaultAsync(x => x.UserName == userName
                                              && x.Email == email);
                        if(user != null)
                        {
                            user.PassWord = _hashPassword.HashPass($"{randomString}");
                            await _context.SaveChangesAsync();
                            return (true, randomString);
                        }
                        return (false, "Wrong User Name Or Email");
                    }
                    else
                    {
                        return (false, "Wrong User Name Or Email");
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, "Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public (bool, string) NewPassWord(string roleName, string userName, string email)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                // Create an instance of the Random class
                Random random = new Random();

                int length = 8;

                // Generate a random string
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //using (var _context = new sep490_g22Context())
                //{
                    if (roleName != null && roleName == "Student")
                    {
                        var student = _context.StudentProfiles.FirstOrDefault(x => x.UserName == userName
                        && x.Email == email);
                        student.PassWord = _hashPassword.HashPass($"{randomString}");
                        return (true, randomString);
                    }
                    if (roleName != null && roleName == "Teacher")
                    {
                        var teacher = _context.TeacherProfiles.FirstOrDefault(x => x.UserName == userName
                                              && x.Email == email);
                        teacher.PassWord = _hashPassword.HashPass($"{randomString}");
                        return (true, randomString);
                    }
                    if (roleName != null && roleName == "Employee")
                    {
                        var teacher = _context.Employees.FirstOrDefault(x => x.UserName == userName
                                              && x.Email == email);
                        teacher.PassWord = _hashPassword.HashPass($"{randomString}");
                        return (true, randomString);
                    }
                    else
                    {
                        return (false, "Wrong User Name Or Email");
                    }
                //}
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Error: " + ex.Message);
                return (false, "Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, string)> ChangePassWord(ChangePasswordDTO form, string roleName, string centerId, string id)
        {
            Console.WriteLine(roleName);
            Console.WriteLine(centerId);
            Console.WriteLine(id);

            if (form.Password != form.ResetPassword)
            {
                return (false, "New password does not match.");
            }
            if (roleName == "Student")
            {
                var user = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id && u.CenterId.ToString() == centerId);
                if (user != null)
                {
                    //var oldPass = _hashPassword.HashPass($"{user.PassWord}");
                    var formPass = _hashPassword.HashPass($"{form.OldPassword}");
                    if (formPass == user.PassWord)
                    {
                        if (_hashPassword.HashPass($"{form.Password}") == user.PassWord)
                        {
                            return (false, "The new password needs to be different from the old password.");
                        }
                        user.PassWord = _hashPassword.HashPass($"{form.Password}");
                        await _context.SaveChangesAsync();
                        return (true, "Password changed successfully.");
                    }
                    else
                    {
                        return (false, "Incorrect password.");
                    }
                }
            }
            if (roleName == "Teacher")
            {
                var user = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id && u.CenterId.ToString() == centerId);
                if (user != null)
                {
                    //var oldPass = _hashPassword.HashPass($"{user.PassWord}");
                    var formPass = _hashPassword.HashPass($"{form.OldPassword}");
                    if (formPass == user.PassWord)
                    {
                        if (_hashPassword.HashPass($"{form.Password}") == user.PassWord)
                        {
                            return (false, "The new password needs to be different from the old password.");
                        }
                        user.PassWord = _hashPassword.HashPass($"{form.Password}");
                        await _context.SaveChangesAsync();
                        return (true, "Password changed successfully.");
                    }
                    else
                    {
                        return (false, "Incorrect password.");
                    }
                }
            }
            if (roleName == "Employee")
            {
                var user = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id && u.CenterId.ToString() == centerId);
                if (user != null)
                {
                    //var oldPass = _hashPassword.HashPass($"{user.PassWord}");
                    var formPass = _hashPassword.HashPass($"{form.OldPassword}");
                    if (formPass == user.PassWord)
                    {
                        if (_hashPassword.HashPass($"{form.Password}") == user.PassWord)
                        {
                            return (false, "The new password needs to be different from the old password.");
                        }
                        user.PassWord = _hashPassword.HashPass($"{form.Password}");
                        await _context.SaveChangesAsync();
                        return (true, "Password changed successfully.");
                    }
                    else
                    {
                        return (false, "Incorrect password.");
                    }
                }
            }
            if (roleName == "Super Admin")
            {
                var user = await _context.Accounts.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                if (user != null)
                {
                    //var oldPass = _hashPassword.HashPass($"{user.PassWord}");
                    var formPass = _hashPassword.HashPass($"{form.OldPassword}");
                    if (formPass == user.PassWord)
                    {
                        if (_hashPassword.HashPass($"{form.Password}") == user.PassWord)
                        {
                            return (false, "The new password needs to be different from the old password.");
                        }
                        user.PassWord = _hashPassword.HashPass($"{form.Password}");
                        await _context.SaveChangesAsync();
                        return (true, "Password changed successfully.");
                    }
                    else
                    {
                        return (false, "Incorrect password.");
                    }
                }
            }
            return (false, "Fail to change your password, please contact the system administrator.");
        }


    }
}
