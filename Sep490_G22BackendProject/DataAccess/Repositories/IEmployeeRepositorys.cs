﻿using BusinessObject.DTOs.EmployeeDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IEmployeeRepository
    {
        //Task<LoginEmployeeResponeDTO> Login(LoginEmployeeRequestDTO loginEmployeeDTO);
        Task<List<ListEmployeeDTO>?> ListEmployee(string? centerId, string? name);
        Task<(bool, List<string>)> UpdateEmployee(UpdateEmployeeDTO updateEmployeeDTO);
        Task<(bool, List<string>)> AddEmployee(AddEmployeeDTO addEmployeeDTO, string accountId);
        Task<(bool, List<string>)> DeleteEmployee(string id);
        Task<(bool, List<string>)> DeleteListEmployee(List<string> id);
        Task<(bool, List<string>)> DeactivateEmployee(string id);
        Task<(bool, List<string>)> ActivateEmployee(string id);
        Task<ListEmployeeDTO?> EmployeeDetail(string id);
    }
}
