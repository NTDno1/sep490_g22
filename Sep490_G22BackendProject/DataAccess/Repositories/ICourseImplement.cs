﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using DataAccess.Validation.ValidationInput;
using Microsoft.VisualBasic;
using Pagination.EntityFrameworkCore.Extensions;

namespace DataAccess.Repositories
{
    public class ICourseImplement : ICourseRepository
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected AddCourseValidator _addCourseValidator;
        protected DeleteExitImage _deleteExitImage;

        public ICourseImplement(IMapper mapper, IWebHostEnvironment hostEnvironment, IHttpContextAccessor httpContextAccessor, AddCourseValidator addCourseValidator, DeleteExitImage deleteExitImage, sep490_g22Context context)
        {
            _mapper = mapper;
            _response = new();
            _hostEnvironment = hostEnvironment;
            _httpContextAccessor = httpContextAccessor;
            _addCourseValidator = addCourseValidator;
            _deleteExitImage= deleteExitImage;
            _context = context;
        }

        public async Task AddCourse(AddCourseDTO addCourseDTO, string employeeId)
        {
            Guid guid;
            bool isGuid = Guid.TryParse(employeeId, out guid);
            var employee = _context.Employees.FirstOrDefault(c => c.Id.ToString() == employeeId);
            try
            {
                var course = new Course
                {
                    Name = addCourseDTO.Name,
                    Code = addCourseDTO.Code,
                    Description = addCourseDTO.Description,
                    Price = addCourseDTO.Price,
                    ImageSrc = addCourseDTO.ImageSrc,
                    CenterId = employee.CenterId,
                    EmployeeId = guid,
                    SyllabusId = addCourseDTO.SyllabusId,
                    DateCreate = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Status = 1,
                };
                //using (var _context = new sep490_g22Context())
                //{
                    await _context.AddAsync(course);
                    await _context.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task TestAddImgToDb(AddCourseDTO addCourseDTO)
        {
            try
            {

                var course = new Course
                {
                    Name = addCourseDTO.Name,
                    ImageSrc = addCourseDTO.ImageSrc
                };
                //using (var _context = new sep490_g22Context())
                //{
                    await _context.AddAsync(course);
                    await _context.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }

        public async Task<(bool, List<string> message)> DeleteCourse(int courseId)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (courseId != 0)
                    {
                        var check = await _context.Classes.Include(u => u.Cource).FirstOrDefaultAsync(u => u.Cource.Id == courseId);
                        if (check != null)
                        {
                            return (false, new List<string> { "The Course already exists in the Class" });
                        }
                        var course = await _context.Courses.FirstOrDefaultAsync(u => u.Id == courseId);
                        if (course != null)
                        {
                            _context.Courses.Remove(course);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        return (false, new List<string> { "The Course already exists in the Class" });
                    }
                    else
                    {
                        return (false, new List<string> { "The Course already exists in the Class" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }

        public async Task<ListCourseDTO> GetCourseDetailDTOById(int courseId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            try
            {
                var course = _context.Courses
                           .Where(course => course.Id == courseId)
                           .Select(course => new ListCourseDTO
                           {
                               Id = course.Id,
                               Name = course.Name,
                               Code = course.Code,
                               Description = course.Description,
                               Price = (decimal)course.Price,
                               ImageSrc = $"{baseUrl}/Images/Course/{course.ImageSrc}",
                               CenterId = course.CenterId,
                               EmployeeId = course.EmployeeId,
                               SyllabusId = course.SyllabusId,
                               DateUpdate = course.DateUpdate
                           })
                           .FirstOrDefault();
                return course;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return null;
        }

        public async Task<List<ListCourseDTO>> GetCourseDTOs(string centerId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            List<Course> course = new List<Course>();
            try
            {
                if (centerId != null && centerId.Length != 0)
                {
                    //using (var _context = new sep490_g22Context())
                    //{
                        //course = await _context.Courses.Where(u=>u.CenterId.ToString() == centerId).ToListAsync();
                        course = await _context.Courses.Where(u => u.CenterId.ToString() == centerId).Select(u => new Course()
                        {
                            Id = u.Id,
                            Name = u.Name,
                            Code = u.Code,
                            Description = u.Description,
                            Price = (decimal)u.Price,
                            ImageSrc = $"{baseUrl}/Images/Course/{u.ImageSrc}",
                            DateCreate = u.DateCreate,
                            DateUpdate = u.DateUpdate,
                            Rating = u.Rating
                        }).ToListAsync();
                    //}
                }
                else
                {
                    //using (var _context = new sep490_g22Context())
                    //{
                        course = await _context.Courses.OrderByDescending(s=> s.Rating).Select(u => new Course()
                        {
                            Id = u.Id,
                            Name = u.Name,
                            Code = u.Code,
                            Description = u.Description,
                            Price = (decimal)u.Price,
                            ImageSrc = $"{baseUrl}/Images/Course/{u.ImageSrc}",
                            DateCreate = u.DateCreate,
                            DateUpdate = u.DateUpdate,
                            Rating=u.Rating
                        }).ToListAsync();
                    //}
                }
                if (course is null)
                {
                    throw new Exception("Cannot found data in this system");
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ListCourseDTO>>(course);
        }

        public async Task UpdateCourse(UpdateCourseDTO updatedCourse,string centerId , string? checkimg)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var course = await _context.Courses.FirstOrDefaultAsync(c => c.Id == updatedCourse.Id);
                    if (course != null && checkimg!= null)
                    {
                        course.Name = updatedCourse.Name;
                        //course.Code = updatedCourse.Code.Replace(" ", "");
                        course.Description = updatedCourse.Description;
                        course.Price = updatedCourse.Price;
                        course.ImageSrc = updatedCourse.ImageSrc;
                        course.EmployeeId = updatedCourse.EmployeeId;
                        course.SyllabusId = updatedCourse.SyllabusId;
                        course.DateUpdate = DateTime.Now;
                        course.CenterId = Guid.Parse(centerId);
                    }
                    else
                    {
                        course.Name = updatedCourse.Name;
                        //course.Code = updatedCourse.Code.Replace(" ", "");
                        course.Description = updatedCourse.Description;
                        course.Price = updatedCourse.Price;
                        course.EmployeeId = updatedCourse.EmployeeId;
                        course.SyllabusId = updatedCourse.SyllabusId;
                        course.DateUpdate = DateTime.Now;
                        course.CenterId = Guid.Parse(centerId);
                    }
                    await _context.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }

        public async Task<Course> GetCourseByID(int courseId)
        {
            try
            {
                var course = await _context.Courses
                           .Where(course => course.Id == courseId)
                           .FirstOrDefaultAsync();
                return course;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        _context.DisposeAsync();
            //    }
            //}
            return null;
        }

        public DetailCourseDTO GetPopularCourse()
        {
            Course course = new Course();
            try
            {
                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
                course =  _context.Courses.Include(u => u.Classes).OrderByDescending(t => t.Classes.Count).FirstOrDefault();
                course.ImageSrc = $"{baseUrl}/Images/Course/{course.ImageSrc}";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<DetailCourseDTO>(course);
        }
        public async Task<Pagination<ListCourseDTO>> ViewCourseForClientSide(int page, int limit, string? searchText, Guid? centerCode)
        {

            try
            {
                var query = _context.Courses
                    .Where(s => s.Status == 1)
                    .AsQueryable();

                if (centerCode != null)
                {
                    query = query.Where(s => s.CenterId == centerCode);
                }

                var totalItems = await query.CountAsync();

                if (totalItems == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(x => x.Name.Contains(searchText));
                }

                List<Course> paginatedData = await query
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .ToListAsync();

                foreach (var i in paginatedData)
                {
                    i.ImageSrc = $"{baseUrl}/Images/Course/{i.ImageSrc}";
                }

                IEnumerable<ListCourseDTO> listCourseDTOs = _mapper.Map<IEnumerable<ListCourseDTO>>(paginatedData);
                return new Pagination<ListCourseDTO>(listCourseDTOs, totalItems, page, limit);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
