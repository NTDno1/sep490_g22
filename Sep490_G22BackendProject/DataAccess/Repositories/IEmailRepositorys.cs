﻿using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.EmailDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IEmailRepositorys
    {
        void SendEmail(SendEamilDto request);
    }
}
