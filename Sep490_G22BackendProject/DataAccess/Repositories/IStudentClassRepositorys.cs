﻿using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IStudentClassRepositorys
    {
        Task<(bool success, List<string> message)> AddStudentClass(string classId, string studentId, string note);
        Task<List<ViewStudentClassDTO>> ListStudentOffClass(string classId);
        Task<(bool, List<string>)> deleteStudentOffClass(string classId, string studentId);
        Task<(bool, List<string>)> deleteListStudentClass(List<ViewStudentClassDTO> listStudentClass);
    }
}
