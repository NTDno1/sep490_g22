﻿using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ISAQRepositorys
    {
        Task<List<ViewSqaDTOs>> GetListSQA();
        Task<(bool, List<string>)> UpdateFaq(UpdateSqaDTOs viewSqaDTOs);
        Task<ViewSqaDTOs?> GetFaqById(int id);
        Task<(bool, List<string>)> AddFaq(AddFaqDTOs addFaqDTOs);
        Task<(bool, List<string>)> DeleteFaq(string id);
    }
}
