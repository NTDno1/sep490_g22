﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using DataAccess.Validation.ValidationInput;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IClassSlotDateImplement : IClassSlotDateRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IClassRepositorys _classRepositorys;
        private readonly ISchedualRepositorys _schedualRepositorys;
        public IClassSlotDateImplement(HashPassword hashPassword, IMapper mapper, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IClassRepositorys classRepositorys, ISchedualRepositorys schedualRepositorys, sep490_g22Context context )
        {
            _mapper = mapper;
            _response = new();
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _classRepositorys = classRepositorys;
            _schedualRepositorys = schedualRepositorys;
            _context= context;
        }
        public async Task<(bool success, List<string> message)> CreateClassSlotDate(AddClassSlotDateDTO listAddClassSlotDateDTO)
        {
            try
            {
                bool exists;
                var messages = new List<string>(); 
                //using (var _context = new sep490_g22Context())
                //{
                    var getClass = await _classRepositorys.ClassDetail(listAddClassSlotDateDTO.ClassId.ToString(), null);
                    if(getClass.Status!= 0)
                         {
                        return (false, new List<string> { $"Lớp học đang trong quá trình không thể cập nhật" });
                         }
                var checkClassSlotdate = await _context.ClassSlotDates.Where(u => u.ClassId == listAddClassSlotDateDTO.ClassId).ToListAsync();
                if(checkClassSlotdate.Count>= getClass.SlotNumber)
                {
                    return (false, new List<string> { $"Tổng Số Lịch học Trong Tuần Không Được Lớn Hơn Số Buổi Học" });
                }
                    var classSlotDate = new AddClassSlotDateDTO();
                    //foreach (var dto in listAddClassSlotDateDTO)
                    //{
                        exists = await _context.ClassSlotDates.Include(u=>u.Class).Include(u=>u.Slot).AnyAsync(csd => csd.ClassId == listAddClassSlotDateDTO.ClassId && csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.Class.Status <2);
                        classSlotDate = _mapper.Map<AddClassSlotDateDTO>(await _context.ClassSlotDates.Include(u => u.Room).Include(u => u.Slot).FirstOrDefaultAsync(csd => csd.ClassId == listAddClassSlotDateDTO.ClassId && csd.SlotId == listAddClassSlotDateDTO.SlotId));
                        if (exists)
                        {
                            messages.Add($"Lớp học: {getClass.Name}, đã tồn tại ca học: {classSlotDate.SlotName}");
                        }
                        exists = await _context.ClassSlotDates.Include(u=>u.Class).Include(u=>u.Room).Include(u=>u.Slot).AnyAsync(csd => csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.RoomId == listAddClassSlotDateDTO.RoomIds && csd.Class.Status < 2);
                        classSlotDate =  _mapper.Map<AddClassSlotDateDTO>(await _context.ClassSlotDates.Include(u => u.Room).Include(u => u.Slot).FirstOrDefaultAsync(csd => csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.RoomId == listAddClassSlotDateDTO.RoomIds));
                        if (exists)
                        {
                            messages.Add($"Ca học: {classSlotDate.SlotName}, hiện đang có phòng: {classSlotDate.RoomName} sử dụng");
                        }
                        exists = await _context.ClassSlotDates.Include(u=>u.Class).Include(u=>u.Slot).AnyAsync(csd => csd.TeacherId == getClass.TeacherId && csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.Class.Status < 2);
                        classSlotDate = _mapper.Map<AddClassSlotDateDTO>(await _context.ClassSlotDates.Include(u => u.Room).Include(u => u.Slot).FirstOrDefaultAsync(csd => csd.TeacherId == getClass.TeacherId && csd.SlotId == listAddClassSlotDateDTO.SlotId));
                        if (exists)
                        {
                            messages.Add($"Ca học: {classSlotDate.SlotName}, hiện đang có giáo viên: {getClass.TeacherName} dạy");
                        }
                        // Kiểm tra xem phần tử đã tồn tại trong cơ sở dữ liệu chưa
                        exists = await _context.ClassSlotDates.Include(u=>u.Class).Include(u=>u.Slot).Include(u=>u.Room).AnyAsync(csd => csd.ClassId == listAddClassSlotDateDTO.ClassId && csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.RoomId == listAddClassSlotDateDTO.RoomIds && csd.TeacherId == getClass.TeacherId && csd.Class.Status < 2);
                        classSlotDate = _mapper.Map<AddClassSlotDateDTO>(await _context.ClassSlotDates.Include(u => u.Room).Include(u => u.Slot).FirstOrDefaultAsync(csd => csd.ClassId == listAddClassSlotDateDTO.ClassId && csd.SlotId == listAddClassSlotDateDTO.SlotId && csd.RoomId == listAddClassSlotDateDTO.RoomIds && csd.TeacherId == getClass.TeacherId));
                        if (exists)
                        {
                            messages.Add($"Item already exists in the database: ClassId: {listAddClassSlotDateDTO.ClassId}, SlotId: {listAddClassSlotDateDTO.SlotId}, RoomId: {listAddClassSlotDateDTO.RoomIds}, TeacherId: {getClass.TeacherName}");
                        }
                    //}
                    if(messages!= null && messages.Count > 0)
                    {
                        return (false, messages);
                    }
                    var studentClass = await _context.StudentClasses.Where(u=>u.ClassId == listAddClassSlotDateDTO.ClassId).ToListAsync();
                    if(studentClass.Count>0)
                    {
                        messages.Add("Bạn Cần Loại bỏ hết học sinh trước khi cập nhật lại danh sách!!!");
                        return (false, messages);
                    }
                    else
                    {
                        ClassSlotDate classSlotDates = new ClassSlotDate
                        {
                            Id = 0,
                            ClassId = listAddClassSlotDateDTO.ClassId,
                            SlotId = listAddClassSlotDateDTO.SlotId,
                            RoomId = listAddClassSlotDateDTO.RoomIds,
                            TeacherId = getClass.TeacherId
                        };
                        await _context.AddAsync(classSlotDates);
                        await _context.SaveChangesAsync();
                        ViewClassDTO viewClassDTO = await _classRepositorys.ClassDetail(listAddClassSlotDateDTO.ClassId.ToString(), null);
                        var addClassLostDate = await _schedualRepositorys.GenerateSchedule(viewClassDTO.classSlotDates, viewClassDTO.SlotNumber, viewClassDTO.StartDate, viewClassDTO.Id);
                        await _schedualRepositorys.CreateSlotClass(addClassLostDate);
                        return (true, new List<string> { "Items added successfully." });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { "Error: " + ex.Message });
            }
        }

        public async Task<List<ViewClassSlotDateDTO>> ViewListClassSlotDate()
        {
            List<ClassSlotDate> ClassSlotDate = new List<ClassSlotDate>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                   
                    ClassSlotDate = await _context.ClassSlotDates.Include(u => u.Class).ThenInclude(u=>u.Cource).Include(u=>u.Slot).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassSlotDateDTO>>(ClassSlotDate);
        }

        public async Task<List<ViewClassSlotDateDTO>> ViewListClassSlotDateByClass(string id)
        {
            List<ClassSlotDate> ClassSlotDate = new List<ClassSlotDate>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    ClassSlotDate = await _context.ClassSlotDates.Include(u => u.Class).ThenInclude(u=>u.Teacher).Where(u=>u.Class.Id.ToString() == id).Include(u => u.Slot).Include(u=> u.Room).ThenInclude(u=>u.Type).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassSlotDateDTO>>(ClassSlotDate);
        }
        public async Task<(bool, List<string>)> DeleteClassSlotDate(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getClassSlotDate = await _context.ClassSlotDates.FirstOrDefaultAsync(u=>u.Id == int.Parse(id));
                    if(getClassSlotDate != null)
                    {
                        _context.ClassSlotDates.Remove(getClassSlotDate);
                        await _context.SaveChangesAsync();
                        ViewClassDTO viewClassDTO = await _classRepositorys.ClassDetail(getClassSlotDate.ClassId.ToString(), null);
                        var addClassLostDate = await _schedualRepositorys.GenerateSchedule(viewClassDTO.classSlotDates, viewClassDTO.SlotNumber, viewClassDTO.StartDate, viewClassDTO.Id);
                        if(addClassLostDate!= null || addClassLostDate.Count == 0)
                        {
                            var scheduleRemove = await _context.Scheduals.Where(u => u.ClassId == getClassSlotDate.ClassId).ToListAsync();
                            _context.RemoveRange(scheduleRemove);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Successfully deleted class session" });
                        }
                        else
                        {
                            await _schedualRepositorys.CreateSlotClass(addClassLostDate);
                            return (true, new List<string> { "Successfully deleted class session" });
                        }
                        
                    }
                    else
                    {
                        return (false, new List<string> { "Error" });
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
