﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccess.Repositories.IReportmplement;

namespace DataAccess.Repositories
{
    public interface IReportRepositorys
    {
        Task<Dictionary<string, Dictionary<string, Count>>?> ReportMember(string roleName, string centerId, int dateTime);
        //Task<(bool, List<string>)> AddNotificationClass(AddNotificationClass addNotificationClass);
        Task<Dictionary<string, Dictionary<string, Statistics>>?> ReportCol( string roleName, string centerId);
        Task<Dictionary<string, Statistics>?> ReportTotalMember( string roleName, string centerId);
        Task<List<TeacherProfile>> ListTeacherAdminSide(string roleName, string centerId);
    }
}
