﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.SyllabusDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ISyllabusRepositorys
    {
        Task<ViewSyllabusOfCourseDTO> ListSylabusByClassId(string classid);
        Task<(bool, List<string>)> UpdateSylabus(UpdateSyllabusDTO updateSyllabusDTO);
        Task<(bool, List<string>)> AddSyllabus(AddSyllabusDTO addSyllabusDTO);
        Task<(bool, List<string>)> DeleteSyllabus(string id);
        Task<List<ViewSyllabusDTO>> ListSylabus(string centerId);
        Task<ViewSyllabusDTO?> SyllabusDetail(string id);
    }
}
