﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmailDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IEmployeeImplement : IEmployeeRepository
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        readonly HashPassword _hashPassword;
        private readonly IEmailRepositorys _emailRepository;
        public IEmployeeImplement(HashPassword hashPassword, IMapper mapper,sep490_g22Context context, IEmailRepositorys emailRepository)
        {
            _mapper = mapper;
            _context = context;
            _hashPassword = hashPassword;
            _emailRepository = emailRepository;
        }
        public async Task<List<ListEmployeeDTO>?> ListEmployee(string? centerId, string? name)
        {
            List<Employee> listCenters = new List<Employee>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    listCenters = await _context.Employees
                        .Include(u=>u.Account)
                    .Include(u => u.Center)
                    .Where(c => (string.IsNullOrEmpty(centerId) || c.CenterId.ToString() == centerId) &&
                                (string.IsNullOrEmpty(name) || c.Name.Contains(name)))
                    .ToListAsync();
                    if (listCenters != null)
                    {
                        return _mapper.Map<List<ListEmployeeDTO>>(listCenters);
                    }
                    else
                    {
                        return null;
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}

        }
        public async Task<(bool, List<string>)> UpdateEmployee(UpdateEmployeeDTO updateEmployeeDTO)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var center = await _context.Employees.FirstOrDefaultAsync(c => c.Id.Equals(updateEmployeeDTO.Id));
                    if (center != null)
                    {
                        center.Name = updateEmployeeDTO.Name;
                        center.Adress = updateEmployeeDTO.Adress;
                        center.Email = updateEmployeeDTO.Email;
                        center.PhoneNumber = updateEmployeeDTO.PhoneNumber;
                        center.Dob = updateEmployeeDTO.Dob;
                        center.DateUpdate = DateTime.Now;
                        //center.RoleId = updateEmployeeDTO.RoleId;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Update Success" });
                    }
                    else
                    {
                        return (false, new List<string> { "Update False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddEmployee(AddEmployeeDTO addEmployeeDTO, string accountId)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random random = new Random();
                int length = 8;
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //using (var _context = new sep490_g22Context())
                //{
                    if (addEmployeeDTO != null)
                    {
                        var newCenter = new Employee
                        {
                            Id = Guid.NewGuid(),
                            Name = addEmployeeDTO.Name,
                            UserName = addEmployeeDTO.UserName,
                            Email = addEmployeeDTO.Email,
                            RoleId = Guid.Parse("36cb6da0-72af-4381-8b9b-38fe047de6fd"),
                            CenterId = addEmployeeDTO.CenterId,
                            AccountId = Guid.Parse(accountId),
                            PassWord = _hashPassword.HashPass($"{randomString}"),
                            DateCreate = DateTime.Now,
                            DateUpdate = DateTime.Now,
                            Status = 0,
                        };
                        await _context.AddAsync(newCenter);
                        await _context.SaveChangesAsync();
                        SendEamilDto request = new SendEamilDto
                        {
                            To = addEmployeeDTO.Email,
                            Subject = "New Account PI ONLINE TRAINING WEBSITE",
                            Body = $"<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n  <style>\r\n    body {{\r\n      font-family: Arial, sans-serif;\r\n    }}\r\n\r\n    .notification {{\r\n      background-color: #f9f9f9;\r\n      border: 1px solid #ddd;\r\n      padding: 10px;\r\n      margin: 10px 0;\r\n    }}\r\n\r\n    .notification-header {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      padding: 10px;\r\n      text-align: center;\r\n      font-size: 20px;\r\n    }}\r\n\r\n    .notification span {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      width:300px;\r\n      border: none;\r\n      padding: 10px 20px;\r\n      text-align: center;\r\n      text-decoration: none;\r\n      display: inline-block;\r\n      font-size: 16px;\r\n      margin: 10px 2px;\r\n      display: block;\r\n      margin-left: auto;\r\n      margin-right: auto;\r\n    }}\r\n    .notification a {{\r\n      color: #4CAF50; /* Màu của đường link */\r\n      text-decoration: underline; /* Gạch chân dưới cho đường link */\r\n      font-weight: bold; /* Đậm chữ cho đường link */\r\n    }}\r\n  </style>\r\n</head>\r\n\r\n<body>\r\n\r\n  <div class=\"notification\">\r\n    <div class=\"notification-header\">\r\n      POTMS Training Platform\r\n    </div>\r\n    <p>Hello New Employee!</p>\r\n    <p>This is your UserName:</p>\r\n     <span>{addEmployeeDTO.UserName}</span> \r\n    <p>This is your Password:</p>\r\n    <span>{randomString}</span>\r\n    <p>You can log in here: <a href=\"http://pitech.edu.vn/login\">Login</a>.</p>\r\n    <p>Please do not reply directly to this email. Copyright © 2023 POTMS. All rights reserved.\r\n      Contact Us | Legal Notices and Terms of Use | Privacy Statement.</p>\r\n  </div>\r\n\r\n</body>\r\n\r\n</html>"
                        };
                        _emailRepository.SendEmail(request);
                        return (true, new List<string> { "Add Sucess" });
                    }
                    else
                    {
                        return (false, new List<string> { "Add False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteEmployee(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var checkStatus = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id && (u.Status == 1 || u.Status == 2));
                        if (checkStatus != null)
                        {
                            return (false, new List<string> { "The Employee is in operation and cannot be deleted" });
                        }
                        var employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (employee != null)
                        {
                            _context.Employees.Remove(employee);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        return (false, new List<string> { "Delete False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Delete False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteListEmployee(List<string> id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        List<Employee> employees = new List<Employee>();
                        foreach (var item in id)
                        {
                            var checkStatus = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == item && u.Status == 1);
                            if (checkStatus != null)
                            {
                                employees.Add(checkStatus);
                            }
                        }
                        if (employees == null || employees.Count == 0)
                        {
                            foreach (var item in id)
                            {
                                var employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == item);
                                if (employee != null)
                                {
                                    _context.Employees.Remove(employee);
                                    await _context.SaveChangesAsync();
                                }
                            }
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        else
                        {
                            List<string> strings = new List<string>();
                            foreach (var item in employees)
                            {
                                strings.Add($"Staff:{item.Name}: Already Exist In Courses And Classes Cannot Be Deleted");
                            }
                            return (false, strings);
                        }
                    }
                    else
                    {
                        return (false, new List<string> { "Delete False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeactivateEmployee(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (employee != null)
                        {
                            employee.Status = 2;
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Deactivate Success" });
                        }
                        return (false, new List<string> { "Deactivate False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Deactivate False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> ActivateEmployee(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var employee = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (employee != null)
                        {
                            employee.Status = 1;
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Activate Success" });
                        }
                        return (false, new List<string> { "Activate False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Activate False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ListEmployeeDTO?> EmployeeDetail(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var center = await _context.Employees.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    return _mapper.Map<ListEmployeeDTO>(center);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
