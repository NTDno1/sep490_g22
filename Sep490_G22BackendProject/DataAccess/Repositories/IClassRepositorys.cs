﻿using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IClassRepositorys
    {
        Task<List<ViewClassDTO>> GetListClassTeachertSide(string userId);
        Task<ViewClassDTO> ClassDetail(string classId, string? checkUpdate);
        Task<List<ViewStudentClassDTO>> ListStudentInClass(string classId); 
        Task<bool> AddClass(AddClassDTOs addClassDTOs);
        Task<List<ViewClassDTO>> GetListClassAdminSide(string userId);
        Task<List<ViewClassDTO>> GetListClassToAttend(string userId, int status);
        Task<ViewClassDTO> GetNewClass();
        Task<List<ViewClassDTO>> GetListClassStudentSide(string userId);
        Task<bool> ChangeStatusClass(int classId, int status);
        Task<(bool, List<string>)> UpdateClass(UpdateClassDTO updateClassDTO);
        Task<(bool, List<string>)> CheckAdd(string classId);
        Task<(bool, List<string>)> CloseClass(string classId);
        Task<(bool, List<string>)> DeleteClass(string classId);
    }
}
