﻿using BusinessObject.DTOs.EmailDTO;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;

namespace DataAccess.Repositories
{
    public class IEmailImplement : IEmailRepositorys
    {
        private readonly IConfiguration _config;

        public IEmailImplement(IConfiguration config)
        {
            _config = config;
        }

        public void SendEmail(SendEamilDto request)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_config.GetSection("EmailUsername").Value));
            email.To.Add(MailboxAddress.Parse(request.To));
            email.Subject = request.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

            using var smtp = new SmtpClient();
            smtp.Connect(_config.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);
            smtp.Authenticate(_config.GetSection("EmailUsername").Value, _config.GetSection("EmailPassword").Value);
            string test = smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
