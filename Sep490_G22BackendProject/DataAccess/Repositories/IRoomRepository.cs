﻿using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.RoomDTO;
using Pagination.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IRoomRepository
    {
        public Pagination<ViewRoomDTO> ViewRooms(int page, int limit, string? searchText);
        public ViewRoomDTO FindRoomById(int id);
        public void AddRoom(AddRoomDTO addRoomDTO);

        public void UpdateRoom(int id, UpdateRoomDTO updateRoomDTO);

        public void DeleteRoom(List<int> ids);

        public Task<List<ViewRoomAdminDTO>> ViewClassRoomsDTO(string centerId);
        public Task<ViewRoomDetailDTO> FindClassRoomById(int id);
        public Task AddClassRoom(AddClassRoomDTO addRoomDTO, Guid employeeId);
        public Task UpdateClassRoom(int id, UpdateClassRoomDTO updateRoomDTO, Guid employeeId);
        public Task DeleteClassRoom(int id);

    }

}
