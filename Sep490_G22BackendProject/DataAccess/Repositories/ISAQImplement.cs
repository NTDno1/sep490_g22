﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ISAQImplement : ISAQRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ISAQImplement(HashPassword hashPassword, IMapper mapper, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _response = new();
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }
        public async Task<List<ViewSqaDTOs>> GetListSQA()
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            List<Saq> saqs = new List<Saq>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    //employee = await _context.Employees.Include(x => x.Center).Include(u => u.Account).Include(u => u.Role).ToListAsync();
                    saqs = await _context.Saqs.Select(u=> new Saq()
                    {
                        Id = u.Id,
                        Title= u.Title, 
                        Desc = u.Desc,
                        Image = $"{baseUrl}/Images/Faq/{u.Image}",
                        DateCreate =u.DateCreate,
                        DateUpdate = u.DateUpdate,
                    } ).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSqaDTOs>>(saqs);
        }
        public async Task<ViewSqaDTOs?> GetFaqById(int id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                  var faq = await _context.Saqs.FirstOrDefaultAsync(u => u.Id == id);
                    return _mapper.Map<ViewSqaDTOs>(faq);
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> UpdateFaq(UpdateSqaDTOs viewSqaDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var updateFaq = await _context.Saqs.FirstOrDefaultAsync(u => u.Id == viewSqaDTOs.Id);
                    if(updateFaq!= null)
                    {
                        updateFaq.Title = viewSqaDTOs.Title;
                        updateFaq.Desc = viewSqaDTOs.Desc;
                        //updateFaq.Image = viewSqaDTOs.Image;
                        updateFaq.DateUpdate = DateTime.Now;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Update Success" });
                    }
                    else
                    {
                        return (false, new List<string> { "Update False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddFaq(AddFaqDTOs addFaqDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if(addFaqDTOs != null)
                    {
                        Saq addFaq = new Saq
                        {
                            Title = addFaqDTOs.Title,
                            Desc = addFaqDTOs.Desc,
                            //Image = addFaqDTOs.Image,
                            DateCreate = DateTime.Now,
                            DateUpdate = DateTime.Now,
                        };
                        await _context.AddAsync(addFaq);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Add Sucess" });
                    }
                    else
                    {
                        return (false, new List<string> { "Add False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteFaq(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if(id != null && int.TryParse(id, out int numericId))
                    {
                        var faq = await _context.Saqs.FirstOrDefaultAsync(u => u.Id.ToString() == id); 
                        if(faq!= null)
                        {
                            _context.Saqs.Remove(faq);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        return (false, new List<string> { "Delete False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Delete False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
