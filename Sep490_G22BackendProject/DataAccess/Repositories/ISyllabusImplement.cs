﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.SyllabusDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ISyllabusImplement : ISyllabusRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ISyllabusImplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<ViewSyllabusDTO>> ListSylabus(string centerId)
        {
            List<Syllabus> listSyllabus = new List<Syllabus>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                   
                    listSyllabus = await _context.Syllabi.Where(u=>u.CenterId.ToString() == centerId).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSyllabusDTO>>(listSyllabus);
        }
        public async Task<ViewSyllabusOfCourseDTO> ListSylabusByClassId(string classid)
        {
            Course course = new Course();
            ExtendedCourse courseNew = new ExtendedCourse();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var getClass = await _context.Classes.FirstOrDefaultAsync(u => u.Id.ToString() == classid);
                if (getClass != null)
                {
                    course = await _context.Courses.Include(u => u.Syllabus).FirstOrDefaultAsync(u => u.Id == getClass.CourceId);
                    if (course != null)
                    {
                        courseNew = new ExtendedCourse
                        {
                            Syllabus = course.Syllabus,
                            ClassName = getClass.Name
                        };
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            Console.WriteLine(courseNew.ClassName);
            return _mapper.Map<ViewSyllabusOfCourseDTO>(courseNew);
        }
        public async Task<(bool, List<string>)> UpdateSylabus(UpdateSyllabusDTO updateSyllabusDTO)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var updateSyllabus = await _context.Syllabi.FirstOrDefaultAsync(u => u.Id == updateSyllabusDTO.Id);
                    if (updateSyllabus != null)
                    {
                        updateSyllabus.SyllabusName = updateSyllabusDTO.SyllabusName;
                        updateSyllabus.Description = updateSyllabusDTO.Description;
                        updateSyllabus.TimeAllocation = updateSyllabusDTO.TimeAllocation; 
                        updateSyllabus.StudentTasks = updateSyllabusDTO.StudentTasks;
                        updateSyllabus.CourseObjectives = updateSyllabusDTO.CourseObjectives;
                        updateSyllabus.Exam = updateSyllabusDTO.Exam; 
                        updateSyllabus.SubjectCode = updateSyllabusDTO.SubjectCode;
                        updateSyllabus.Contact = updateSyllabusDTO.Contact;
                        updateSyllabus.CourseLearningOutcome = updateSyllabusDTO.CourseLearningOutcome; 
                        updateSyllabus.MainTopics = updateSyllabusDTO.MainTopics;
                        updateSyllabus.RequiredKnowledge = updateSyllabusDTO.RequiredKnowledge; 
                        updateSyllabus.LinkBook = updateSyllabusDTO.LinkBook;
                        updateSyllabus.LinkDocument = updateSyllabusDTO.LinkDocument;
                        updateSyllabus.Status= updateSyllabusDTO.Status;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Update Success" });
                    }
                    else
                    {
                        return (false, new List<string> { "Update False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddSyllabus(AddSyllabusDTO addSyllabusDTO)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                   
                    if (addSyllabusDTO != null)
                    {
                        Syllabus addSyllabus = new Syllabus
                        {
                            SyllabusName = addSyllabusDTO.SyllabusName,
                            Description = addSyllabusDTO.Description,
                            TimeAllocation = addSyllabusDTO.TimeAllocation,
                            StudentTasks = addSyllabusDTO.StudentTasks,
                            CourseObjectives = addSyllabusDTO.CourseObjectives,
                            Exam = addSyllabusDTO.Exam,
                            SubjectCode = addSyllabusDTO.SubjectCode,
                            Contact = addSyllabusDTO.Contact,
                            CourseLearningOutcome = addSyllabusDTO.CourseLearningOutcome,
                            MainTopics = addSyllabusDTO.MainTopics,
                            RequiredKnowledge = addSyllabusDTO.RequiredKnowledge,
                            LinkBook = addSyllabusDTO.LinkBook,
                            LinkDocument = addSyllabusDTO.LinkDocument,
                            Status = 1,
                            CenterId = addSyllabusDTO.CenterId,
                        };
                        await _context.AddAsync(addSyllabus);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Add Sucess" });
                    }
                    else
                    {
                        return (false, new List<string> { "Add False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteSyllabus(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null && int.TryParse(id, out int numericId))
                    {
                       var check = await _context.Courses.Include(u => u.Syllabus).Where(u => u.Syllabus.Id.ToString() == id).ToListAsync();
                        if(check!= null && check.Count >0)
                        {
                            return (false, new List<string> { "The syllabus already exists in the course" });
                        }
                        var syllabus = await _context.Syllabi.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (syllabus != null)
                        {
                            _context.Syllabi.Remove(syllabus);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        return (false, new List<string> { "Delete False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Delete False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ViewSyllabusDTO?> SyllabusDetail(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var syllabusDetail = await _context.Syllabi.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    return _mapper.Map<ViewSyllabusDTO>(syllabusDetail);
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
