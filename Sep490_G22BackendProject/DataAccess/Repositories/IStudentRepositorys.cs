﻿using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IStudentRepositorys
    {
        Task<List<ViewListStudentDTO>> ListAllStudentAdminSide(string roleName, string centerId, string? classId);
        Task<List<ViewListStudentDTO>> ListWaitingStudent(string roleName, string centerId);
        Task<(bool, List<string>)> UpdateStudent(UpdateStudentDTOs updateStudentDTO, bool avaimg, string userId, string image);
        Task<(bool, List<string>)> AddStudent(AddStudentDTOs addStudentDTO, string centerID, string userId, bool avaimg, string image);
        Task<(bool, List<string>)> DeleteStudent(string id);
        Task<(bool, List<string>)> DeleteListStudent(List<string> id);
        Task<(bool, List<string>)> DeactivateStudent(string id);
        Task<ViewListStudentDTO?> StudentDetail(string id);
        Task<(bool, List<string>)> ActivateStudent(string id);
    }
}
