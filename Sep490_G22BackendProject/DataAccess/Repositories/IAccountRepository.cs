﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IAccountRepository
    {
        Task<List<ListEmployeeDTO>> GetEmployees();
        // Task AddEmployee(AddEmployeeDTO addEmployeeDTO, string accountId);
        Task UpdateEmployee(UpdateEmployeeDTO updateEmployeeDTO);
        Task DeleteEmployee(string id);
        public void UpdateAccount(Account? account);
        public Account getAccountByUsername(String username);
        public void UpdateStudent(StudentProfile? account);

        public StudentProfile getStudentByUsername(String username);
        public void UpdateTeacher(TeacherProfile? teacherProfile);

        public TeacherProfile getTeacherByUsername(String username);

        public void UpdateEmployee(Employee? employee);

        public Employee getEmployeeByUsername(String username);

        Task<(bool, string)> GetUser(string roleName, string userName, string email);
        (bool, string) NewPassWord(string roleName, string userName, string email);
        Task<(bool, string)> ChangePassWord(ChangePasswordDTO form, string roleName, string centerId, string id);

    }
}
