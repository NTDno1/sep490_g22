﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmailDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.OrderDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IStudentImplement : IStudentRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        readonly HashPassword _hashPassword;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailRepositorys _emailRepository;
        public IStudentImplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, IEmailRepositorys emailRepository, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _hashPassword = hashPassword;
            _emailRepository = emailRepository;
        }
        public async Task<List<ViewListStudentDTO>> ListAllStudentAdminSide(string roleName, string centerId, string? classId)
        {
            List<StudentProfile> getStudent = new List<StudentProfile>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                var checkClass = await _context.Classes.Include(u=>u.Cource).FirstOrDefaultAsync(u => u.Id.ToString() == classId);

                if (classId!= null && checkClass.Cource.Price >2000)
                {
                    var classDetail = await _context.Classes.FirstOrDefaultAsync(u => u.Id.ToString() == classId);
                    var orderDetail = await _context.OrderDetails.Include(u => u.Order).Include(u => u.Course).Include(u => u.Order.Student).Where(u=>u.Status >0).ToListAsync();
                    //var listOrdertail = _mapper.Map<List<OrderDetailView>>(orderDetail);
                    var distinctOrderDetails = orderDetail
                    .GroupBy(od => new { od.CourseId, od.Order.StudentId })
                    .Select(group => group.First())
                    .ToList();

                    var listOrdertail = _mapper.Map<List<OrderDetailView>>(distinctOrderDetails.Where(u => u.CourseId == classDetail.CourceId));
                    if (roleName != null && roleName == "Employee" && centerId != null)
                    {
                        foreach(var item in listOrdertail)
                        {
                            getStudent.AddRange(await _context.StudentProfiles.Include(u => u.Emoloyee).Where(u => (u.Status < 2) && u.Id == item.StudentID).ToListAsync());
                        }
                    }
                }
                else
                {
                    if (roleName != null && roleName == "Employee" && centerId != null)
                    {
                        getStudent = await _context.StudentProfiles.Include(u => u.Emoloyee).Where(u => (u.Status == 1 || u.Status == 0 || u.Status == 2) && u.CenterId.ToString() == centerId).ToListAsync();
                    }
                    if (roleName != null && roleName == "Super Admin")
                    {
                        getStudent = await _context.StudentProfiles.ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewListStudentDTO>>(getStudent);
        }

        public async Task<List<ViewListStudentDTO>> ListWaitingStudent(string roleName, string centerId)
        {
            List<StudentProfile> getStudent = new List<StudentProfile>();
            try
            {
                if (roleName != null && roleName == "Employee" && centerId != null)
                {
                    var waitingStudent = await _context.WaitingStudents.Include(u => u.Student).Where(u => u.Student.CenterId.ToString() == centerId).ToListAsync();
                    foreach (var item in waitingStudent)
                    {
                        var addStudent = await _context.StudentProfiles.Include(u => u.Emoloyee).FirstOrDefaultAsync(u => (u.Status == 1 || u.Status == 0 || u.Status == 2) && u.CenterId.ToString() == centerId && u.Id == item.StudentId);
                        if (addStudent != null)
                        {
                            getStudent.Add(addStudent);
                        }

                    }
                }
                if (roleName != null && roleName == "Super Admin")
                {
                    getStudent = await _context.StudentProfiles.ToListAsync();
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewListStudentDTO>>(getStudent);
        }
        public async Task<(bool, List<string>)> UpdateStudent(UpdateStudentDTOs updateStudentDTO, bool avaimg, string userId, string image)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                var student = await _context.StudentProfiles.FirstOrDefaultAsync(c => c.Id.Equals(updateStudentDTO.Id));
                if (student != null)
                {
                    student.FirstName = updateStudentDTO.FirstName;
                    student.MidName = "";
                    student.LastName = "";
                    student.Address = updateStudentDTO.Address;
                    student.Email = updateStudentDTO.Email;
                    student.PhoneNumber = updateStudentDTO.PhoneNumber;
                    student.Dob = updateStudentDTO.Dob;
                    student.Image = avaimg ? image : student.Image;
                    student.Gender = updateStudentDTO.Gender;
                    student.Cccd = updateStudentDTO.Cccd;
                    if (userId != null)
                    {
                        student.EmoloyeeId = Guid.Parse(userId);

                    }
                    student.DateUpdate = DateTime.Now;
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Update Success" });
                }
                else
                {
                    return (false, new List<string> { "Update False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddStudent(AddStudentDTOs addStudentDTO, string centerID, string userId, bool avaimg, string image)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random random = new Random();
                int length = 8;
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //using (var _context = new sep490_g22Context())
                //{
                if (addStudentDTO != null)
                {
                    var newStudent = new StudentProfile
                    {
                        Id = Guid.NewGuid(),
                        UserName = addStudentDTO.UserName,
                        PassWord = _hashPassword.HashPass($"{randomString}"),
                        FirstName = addStudentDTO.FirstName,
                        MidName = "",
                        LastName = "",
                        Email = addStudentDTO.Email,
                        Image = avaimg ? image : "",
                        Gender = addStudentDTO.Gender,
                        Cccd = addStudentDTO.Cccd,
                        PhoneNumber = addStudentDTO.PhoneNumber,
                        Dob = new DateTime(2000, 2, 2),
                        CenterId = Guid.Parse(centerID),
                        EmoloyeeId = Guid.Parse(userId),
                        Status = 0,
                        DateCreate = DateTime.Now,
                        DateUpdate = DateTime.Now
                    };
                    await _context.AddAsync(newStudent);
                    await _context.SaveChangesAsync();
                    SendEamilDto request = new SendEamilDto
                    {
                        To = addStudentDTO.Email,
                        Subject = " New Account PI ONLINE TRAINING WEBSITE",
                        Body = $"<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n  <style>\r\n    body {{\r\n      font-family: Arial, sans-serif;\r\n    }}\r\n\r\n    .notification {{\r\n      background-color: #f9f9f9;\r\n      border: 1px solid #ddd;\r\n      padding: 10px;\r\n      margin: 10px 0;\r\n    }}\r\n\r\n    .notification-header {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      padding: 10px;\r\n      text-align: center;\r\n      font-size: 20px;\r\n    }}\r\n\r\n    .notification span {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      width:300px;\r\n      border: none;\r\n      padding: 10px 20px;\r\n      text-align: center;\r\n      text-decoration: none;\r\n      display: inline-block;\r\n      font-size: 16px;\r\n      margin: 10px 2px;\r\n      display: block;\r\n      margin-left: auto;\r\n      margin-right: auto;\r\n    }}\r\n    .notification a {{\r\n      color: #4CAF50; /* Màu của đường link */\r\n      text-decoration: underline; /* Gạch chân dưới cho đường link */\r\n      font-weight: bold; /* Đậm chữ cho đường link */\r\n    }}\r\n  </style>\r\n</head>\r\n\r\n<body>\r\n\r\n  <div class=\"notification\">\r\n    <div class=\"notification-header\">\r\n      POTMS Training Platform\r\n    </div>\r\n    <p>Hello New Student!</p>\r\n    <p>This is your UserName:</p>\r\n     <span>{addStudentDTO.UserName}</span> \r\n    <p>This is your Password:</p>\r\n    <span>{randomString}</span>\r\n    <p>You can log in here: <a href=\"http://pitech.edu.vn/login\">Login</a>.</p>\r\n    <p>Please do not reply directly to this email. Copyright © 2023 POTMS. All rights reserved.\r\n      Contact Us | Legal Notices and Terms of Use | Privacy Statement.</p>\r\n  </div>\r\n\r\n</body>\r\n\r\n</html>"
                    };
                    _emailRepository.SendEmail(request);
                    return (true, new List<string> { "Add Sucess" });
                }
                else
                {
                    return (false, new List<string> { "Add False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteStudent(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var checkStatus = await _context.StudentClasses.FirstOrDefaultAsync(u => u.StudentId.ToString() == id);
                    if (checkStatus != null)
                    {
                        return (false, new List<string> { "Student existing in the classroom cannot be deleted." });
                    }
                    var studentProfile = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    if (studentProfile != null && studentProfile.Status == 0)
                    {
                        _context.StudentProfiles.Remove(studentProfile);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Delete Sucess" });
                    }
                    return (false, new List<string> { "Delete False" });
                }
                else
                {
                    return (false, new List<string> { "Delete False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteListStudent(List<string> id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    List<StudentClass> studentClasses = new List<StudentClass>();
                    foreach (var item in id)
                    {
                        var checkStatus = await _context.StudentClasses.FirstOrDefaultAsync(u => u.StudentId.ToString() == item);
                        if (checkStatus != null)
                        {
                            studentClasses.Add(checkStatus);
                        }
                    }
                    if (studentClasses == null || studentClasses.Count == 0)
                    {
                        foreach (var item in id)
                        {
                            var studentProfile = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == item);
                            if (studentProfile != null)
                            {
                                _context.StudentProfiles.Remove(studentProfile);
                                await _context.SaveChangesAsync();
                            }
                        }
                        return (true, new List<string> { "Delete Sucess" });
                    }
                    else
                    {
                        List<string> strings = new List<string>();
                        foreach (var item in studentClasses)
                        {
                            var u = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id == item.StudentId);
                            strings.Add($"Student: {u.FirstName + " " + u.MidName + " " + u.LastName} existing in the classroom cannot be deleted.");
                        }
                        return (false, strings);
                    }
                }
                else
                {
                    return (false, new List<string> { "Delete False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeactivateStudent(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var student = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    if (student != null)
                    {
                        student.Status = 2;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Deactivate Success" });
                    }
                    return (false, new List<string> { "Deactivate False" });
                }
                else
                {
                    return (false, new List<string> { "Deactivate False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> ActivateStudent(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var student = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    if (student != null)
                    {
                        student.Status = 1;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Activate Success" });
                    }
                    return (false, new List<string> { "Activate False" });
                }
                else
                {
                    return (false, new List<string> { "Activate False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ViewListStudentDTO?> StudentDetail(string id)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                var student = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                student.Image = $"{baseUrl}/Images/Student/{student.Image}";
                return _mapper.Map<ViewListStudentDTO>(student);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return null;
        }
    }
}
