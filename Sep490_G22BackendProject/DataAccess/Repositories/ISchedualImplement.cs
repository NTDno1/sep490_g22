﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.SchedualDTO;
using System.Globalization;
using Microsoft.AspNetCore.Http.Metadata;

namespace DataAccess.Repositories
{
    public class ISchedualImplement : ISchedualRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        readonly ValidationAddClass _validationAddClass;
        private readonly IClassRepositorys _classRepositorys;
        readonly CheckUser _checkUser;
        public ISchedualImplement(IMapper mapper, ValidationAddClass validationAddClass, IClassRepositorys classRepositorys, CheckUser checkUser, sep490_g22Context context)
        {
            _mapper = mapper;
            _response = new();
            _validationAddClass = validationAddClass;
            _classRepositorys = classRepositorys;
            _checkUser = checkUser;
            _context = context;
        }
        public async Task<List<Schedual>> GenerateSchedule(List<ViewSlotdateDTO> classSlotDates, int slotNumber, DateTime startDate, int classId)
        {
            List<Schedual> schedule = new List<Schedual>();
            DateTime currentDate = startDate.AddDays(1);
            var classSlotDateRemake = _validationAddClass.SortClassSlots(classSlotDates);
            //bool checkSchedule = await _context.Scheduals.Where(u => u.ClassId == classId).AnyAsync();
            for (int i = 0; i < 1000; i++)
            {
                foreach (var classSlotDate in classSlotDateRemake)
                {
                    int count = schedule.Count;
                    if (classSlotDate.Day == currentDate.AddDays(0.5).DayOfWeek.ToString().Substring(0, 3))
                    {
                        var slot = new Schedual
                        {
                            ClassId = classId,
                            DateOffSlot = currentDate.Date.Add(classSlotDate.TimeStart),
                            Staus = 3,
                            SlotDateId = classSlotDate.Id,
                            RoomIdUpDate = classSlotDate.RoomId,
                        };
                        slot.SlotNum = count + 1;
                        schedule.Add(slot);
                        if (count >= slotNumber - 1)
                        {
                            return schedule;
                        }
                    }
                }
                currentDate = currentDate.AddDays(1);
            }
            return schedule;
        }
        public async Task<bool> CreateSlotClass(List<Schedual> scheduals)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                bool checkSchedule = await _context.Scheduals.Where(u => u.ClassId == scheduals.First().ClassId).AnyAsync();
                if (checkSchedule == true)
                {
                    var schedulesToDelete = await _context.Scheduals
                    .Where(u => u.ClassId == scheduals.First().ClassId)
                    .ToListAsync(); // Lấy tất cả schedules có classId tương tự

                    _context.Scheduals.RemoveRange(schedulesToDelete); // Xóa tất cả các schedules tìm thấy
                    await _context.SaveChangesAsync(); // Lưu thay đổi vào cơ sở dữ liệu
                    await _context.AddRangeAsync(scheduals);
                    await _context.SaveChangesAsync();
                    Schedual maxDateSchedule = scheduals.OrderByDescending(s => s.DateOffSlot).FirstOrDefault();
                    var classes = await _context.Classes.FirstOrDefaultAsync(c => c.Id == maxDateSchedule.ClassId);
                    if (classes != null)
                    {
                        classes.EndDate = maxDateSchedule.DateOffSlot;
                    };
                    await _context.SaveChangesAsync();
                }
                else
                {
                    await _context.AddRangeAsync(scheduals);
                    await _context.SaveChangesAsync();
                    Schedual maxDateSchedule = scheduals.OrderByDescending(s => s.DateOffSlot).FirstOrDefault();
                    var classes = await _context.Classes.FirstOrDefaultAsync(c => c.Id == maxDateSchedule.ClassId);
                    if (classes != null)
                    {
                        classes.EndDate = maxDateSchedule.DateOffSlot;
                    };
                    await _context.SaveChangesAsync();
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return true;
        }
        public async Task<(bool, List<string>?)> UpdateSchedule(string scheduleId, string slotdate, string dateChange, string roomId)
        {
            try
            {
                if (DateTime.TryParseExact(dateChange, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDate))
                {
                    //using (var _context = new sep490_g22Context())
                    //{
                    //if(parsedDate< DateTime.Now)
                    //{
                    //    return (false, new List<string> { $"The change schedule cannot be less than today's date" });
                    //}
                    var slotDate = await _context.SlotDates.FirstOrDefaultAsync(u => u.Id.ToString() == slotdate);
                    var getDay = _checkUser.GetDayOfWeekString(parsedDate.DayOfWeek);
                    var foundClass = await _context.Classes.Include(u => u.Scheduals).FirstOrDefaultAsync(u => u.Scheduals.Any(s => s.Id.ToString() == scheduleId));
                    //Console.WriteLine(foundClass.Name);
                    if (slotDate.Day != getDay)
                    {
                        return (false, new List<string> { $"Ngày và lịch được chọn không khớp" });
                    }
                    if (parsedDate < DateTime.Now)
                    {
                        return (false, new List<string> { $"Không được phép chuyển lịch về trước ngày hôm nay" });
                    }
                    var check = await _context.Scheduals.Where(u => u.DateOffSlot == parsedDate && u.SlotDateId == int.Parse(slotdate) && u.RoomIdUpDate.ToString() == roomId).ToListAsync();
                    if (check != null && check.Count > 0)
                    {
                        var item = await _context.ClassRooms.FirstOrDefaultAsync(u => u.Id == check.First().RoomIdUpDate);
                        var slot = await _context.SlotDates.FirstOrDefaultAsync(u => u.Id == check.First().SlotDateId);
                        return (false, new List<string> { $"Hiện tại phòng: {item.Name} đang có ca học: {slot.TimeStart.ToString(@"hh\:mm") + "-" + slot.TimeEnd.ToString(@"hh\:mm")} tới: {parsedDate.ToString("dd/MM/yyyy")}" });
                    }
                    var updateSchedual = await _context.Scheduals.Include(u => u.Class).FirstOrDefaultAsync(u => u.Id.ToString() == scheduleId);
                    var getClassOfTeacher = await _context.Classes.Include(u => u.Teacher).Where(u => u.TeacherId == updateSchedual.Class.TeacherId && u.Status < 2).ToListAsync();
                    List<Schedual> checkScheduleOfTeacher = new List<Schedual>();
                    if (getClassOfTeacher != null)
                    {
                        foreach (var item in getClassOfTeacher)
                        {
                            checkScheduleOfTeacher = await _context.Scheduals.Where(u => u.DateOffSlot == parsedDate && u.SlotDateId == int.Parse(slotdate) && u.ClassId == item.Id).ToListAsync();
                        }
                        if (checkScheduleOfTeacher != null && checkScheduleOfTeacher.Count() > 0)
                        {
                            var slot = await _context.SlotDates.FirstOrDefaultAsync(u => u.Id == checkScheduleOfTeacher.First().SlotDateId);
                            return (false, new List<string> { $"Giáo viên: {getClassOfTeacher.First().Teacher.FirstName} hiện tại đang có lịch vào ca học: {slot.TimeStart.ToString(@"hh\:mm") + "-" + slot.TimeEnd.ToString(@"hh\:mm")} tới: {parsedDate.ToString("dd/MM/yyyy")}" });
                        }
                    }
                    var checkEqualClass = await _context.Scheduals.Include(u => u.Class).Where(u => u.DateOffSlot == parsedDate && u.SlotDateId == int.Parse(slotdate) && u.ClassId == updateSchedual.Class.Id).ToListAsync();
                    if (checkEqualClass != null && check.Count > 0)
                    {
                        var classs = await _context.Classes.FirstOrDefaultAsync(u => u.Id == checkEqualClass.First().ClassId);
                        var slot = await _context.SlotDates.FirstOrDefaultAsync(u => u.Id == checkEqualClass.First().SlotDateId);
                        return (false, new List<string> { $"Đã tồn tại lớp học: {classs.Name} Tại ca học: {slot.TimeStart.ToString(@"hh\:mm") + "-" + slot.TimeEnd.ToString(@"hh\:mm")} vào ngày: {parsedDate.ToString("dd/MM/yyyy")}" });
                    }
                    var studentClass = await _context.StudentClasses.Where(u => u.ClassId == updateSchedual.ClassId).ToArrayAsync();
                    Dictionary<string, List<StudentClass>> keyValuePairs = new Dictionary<string, List<StudentClass>>();
                    foreach (var item in studentClass)
                    {
                        var find = await _context.StudentClasses.Where(u => u.StudentId == item.StudentId && u.ClassId != item.ClassId && u.Class.Status < 2).ToListAsync();
                        // Kiểm tra xem `find` khác null
                        if (find != null && find.Any())
                        {
                            // Kiểm tra xem key đã tồn tại trong từ điển chưa
                            if (keyValuePairs.ContainsKey(item.StudentId.ToString()))
                            {
                                // Key đã tồn tại, cập nhật giá trị của key đó
                                keyValuePairs[item.StudentId.ToString()].AddRange(find);
                            }
                            else
                            {

                            }
                            {
                                // Key chưa tồn tại, thêm cặp key-value mới vào từ điển
                                keyValuePairs.Add(item.StudentId.ToString(), find);
                            }
                        }
                    }
                    foreach (var item in keyValuePairs.Keys)
                    {
                        var student = await _context.StudentProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == item);
                        List<Schedual> checkScheduleOfEachStudent = new List<Schedual>();
                        foreach (var items in keyValuePairs.Values)
                        {
                            checkScheduleOfEachStudent = await _context.Scheduals.Where(u => u.DateOffSlot == parsedDate && u.SlotDateId == int.Parse(slotdate)).ToListAsync();

                        }
                        if (checkScheduleOfEachStudent != null && checkScheduleOfEachStudent.Count() > 0)
                        {
                            var slot = await _context.SlotDates.FirstOrDefaultAsync(u => u.Id == checkScheduleOfEachStudent.First().SlotDateId);
                            return (false, new List<string> { $"Học sinh: {student.FirstName} đã có lịch học vào ca: {slot.TimeStart.ToString(@"hh\:mm") + "-" + slot.TimeEnd.ToString(@"hh\:mm")} Ngày: {parsedDate.ToString("dd/MM/yyyy")}" });
                        }
                    }
                    if (updateSchedual != null)
                    {
                        updateSchedual.SlotDateId = int.Parse((string)slotdate);
                        updateSchedual.RoomIdUpDate = int.Parse((string)roomId);
                        updateSchedual.DateOffSlot = parsedDate;
                        updateSchedual.Staus = 5;
                        await _context.SaveChangesAsync();
                        if (parsedDate > foundClass.EndDate)
                        {
                            //foundClass.EndDate = parsedDate;
                            //await _context.SaveChangesAsync();
                            return (false, new List<string> { "Lịch học không được lớn hơn ngày kết thúc." });
                        }
                        if (parsedDate < foundClass.StartDate)
                        {
                            //foundClass.EndDate = parsedDate;
                            //await _context.SaveChangesAsync();
                            return (false, new List<string> { "Lịch Học Không Được Nhỏ Hơn Ngày Bắt Đầu" });
                        }
                        return (true, new List<string> { "The class schedule has been updated successfully and the changed class schedule may overlap with other classes of students and teachers." });
                    }
                    else
                    {
                        return (false, new List<string> { "Đã sảy ra lỗi khi cập nhật lịch học, vui lòng liên lạc với bên quản trị hệ thống." });
                    }
                }
                else
                {
                    return (false, new List<string> { "Ngày tháng không đúng định dạng" });
                }
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }

        public async Task<List<ViewSchedualDTO>> ViewSchedualByMonth(string month, string centerId)
        {
            List<Schedual> scheduals = new List<Schedual>();
            try
            {
                //if (month != null)
                //{
                //    if (DateTime.TryParseExact(month, "MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedMonth))
                //    {
                        scheduals = await _context.Scheduals
                            .Include(u => u.Class).ThenInclude(u => u.Teacher).Include(u => u.Class.Cource).Include(u => u.Class.AdminCenter)
                            .Include(u => u.SlotDate)
                            .Include(u => u.RoomIdUpDateNavigation).ThenInclude(u => u.Type)
                            //.Where(u => u.DateOffSlot.Month == parsedMonth.Month && u.DateOffSlot.Year == parsedMonth.Year && u.Class.CenterId.ToString() == centerId)
                            .Where(u => u.Class.CenterId.ToString() == centerId)
                            .ToListAsync();
                //    }
                //}
                //else
                //{
                //    //using (var _context = new sep490_g22Context())
                //    //{
                //    scheduals = await _context.Scheduals.Include(u => u.SlotDate).Include(u => u.RoomIdUpDateNavigation).ThenInclude(u => u.Type).Include(u => u.Class).ThenInclude(u => u.Teacher).Include(u => u.Class.Cource).Include(u => u.Class.AdminCenter).Where(u => u.DateOffSlot.Year == DateTime.Now.Year && u.Class.CenterId.ToString() == centerId).ToListAsync();
                //    //}
                //}

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSchedualDTO>>(scheduals);
        }
        public async Task<List<ViewSchedualDTO>> ViewClassSchedual(string classId)
        {
            List<Schedual> scheduals = new List<Schedual>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                scheduals = await _context.Scheduals.Include(u => u.SlotDate).Include(u => u.Class).ThenInclude(u => u.Teacher).Include(u => u.Class.Cource).Include(u => u.Class.AdminCenter).Where(u => u.ClassId.ToString() == classId).Include(u => u.RoomIdUpDateNavigation).ThenInclude(u => u.Type).ToListAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSchedualDTO>>(scheduals);
        }
        public async Task<(bool, ViewSchedualDTO?)> GetScheduleByScheduleId(int? scheduleId)
        {
            Schedual scheduals = new Schedual();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                scheduals = await _context.Scheduals.Include(u => u.SlotDate).Include(u => u.Class).ThenInclude(u => u.Teacher).FirstOrDefaultAsync(u => u.Id == scheduleId);
                if (scheduals != null)
                {
                    return (true, _mapper.Map<ViewSchedualDTO>(scheduals));
                }
                else
                {
                    return (false, null);
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return (false, null);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}

        }

        public async Task<List<ViewSchedualDTO>> ViewTeacherClassSchedual(string teacherId)
        {
            List<Schedual> scheduals = new List<Schedual>();
            var listClass = await _context.Classes.Where(u => u.TeacherId.ToString() == teacherId).ToListAsync();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                List<Schedual> add;
                foreach (var item in listClass)
                {
                    Console.WriteLine(item.Id);
                    add = _context.Scheduals.Include(u => u.SlotDate).Include(u => u.Class).ThenInclude(u => u.Teacher).Include(u => u.Class.Cource).Include(u => u.Class.AdminCenter).Include(u => u.RoomIdUpDateNavigation).ThenInclude(u => u.Type).Where(u => u.ClassId == item.Id).ToList();
                    scheduals.AddRange(add);
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSchedualDTO>>(scheduals);
        }
        public async Task<List<ViewSchedualDTO>> ViewStudentClassSchedual(string studentId)
        {
            List<Schedual> scheduals = new List<Schedual>();
            //var listClass = await _context.Classes.Where(u => u.TeacherId.ToString() == teacherId).ToListAsync();
            var listStudentClass = await _context.StudentClasses
            .Where(u => u.StudentId.ToString() == studentId)
            .ToListAsync();

            // Lọc ra các classid duy nhất
            var uniqueClassIds = listStudentClass.Select(c => c.ClassId).Distinct().ToList();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                //List<Schedual> add;
                foreach (var item in uniqueClassIds)
                {
                    Console.WriteLine("Đây là lớp học" + item.Value);
                    var add = _context.Scheduals.Include(u => u.SlotDate).Include(u => u.Class).ThenInclude(u => u.Teacher).Include(u => u.Class.Cource).Include(u => u.Class.AdminCenter).Where(u => u.ClassId == item.Value).ToList();
                    scheduals.AddRange(add);
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewSchedualDTO>>(scheduals);
        }
    }
}
