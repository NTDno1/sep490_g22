﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.ExamDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IExamRepository
    {
        public List<StudentExamResponse> FilterExamClass(int examID,int classID);

        public CourseClassExamResponse GetCourseClassExamFilter(string TeacherName);

        public void UpdateEvaluate(List<StudentExamResponse> exams);

        public List<CourseFilter> GetListCourseOfTeacher(string TeacherName);

        public ExamClassFilter GetListExamAndClassOfCourse(int CourseID,string teacherName);


        public Task<ManageExamDTOs> ListExam(int? courseID);
        public Task<bool> UpdateExam(List<ExamDTOsRequest> exam,int courseID);
        public Task<List<string>> ValidateExam(List<ExamDTOsRequest> exam);

        public Task<List<FilterOfCourse>> GetCourseOfStudent(string username);

        public Task<List<MarkReportDTOs>> GetMarkOfStudent(string username,int courseID,int classID);



    }
}
