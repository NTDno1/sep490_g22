﻿using BusinessObject.DTOs.AttendenceDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IAttendenceRepositorys
    {
        Task<(bool success, List<string> message)> GenerateAttendence(string classId);
        Task<List<LOSNAttendanceDTO>> ListAttendenceBySchedualId(string schedualId);
        Task<(bool success, List<string> message)> TakeAttendence(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs);
        Task<List<ViewAttendenceDTO>> ListAttendenceByStudentId(string studentId);
        Task<List<ViewAttendenceDTO>> ListAttendenceByClassId(string classId, string studentId);
        Task<(bool success, List<string> message)> TakeAttendenceAdmin(List<LOSNAttendanceDTO> listlOSNAttendanceDTOs);
    }
}
