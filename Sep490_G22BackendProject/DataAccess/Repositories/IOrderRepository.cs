using BusinessObject.Models;
using BusinessObject.DTOs;
using BusinessObject.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject.DTOs.OrderDTO;

namespace DataAccess.Repositories
{
    public interface IOrderRepository
    {
        Task<ViewOrderDTO> Order(string orderId, string userId, string roleName);
        Task<List<ViewOrderDTO>> ListOrder(string roleName, string userId, string? toMonth, string? fromMonth, string? year, string centerId);
        Task<Order> OrderMore(string orderId);
        Task<List<ViewOrderDetailDTO>> ListOrderDetail(string orderId, string roleName, string userId);
        Task<(bool, List<string>, ViewOrderDetailDTO)> AddOrderDetail(AddOrderDetailDTO addOrderDetail, string orderDetailId);
        Task<(bool, List<string>, ViewOrderDTO)> AddOrder(AddOrderDTO addOrderDTO, string employeeId);
        Task<(bool, List<string>)> Payment(string orderID, string employeeId);
        Task<(bool, List<string>)> DeteleOrder(string orderId);
        Task<(bool, List<string>)> DeleteOrderDetail(string orderDetailId);
    }
}
