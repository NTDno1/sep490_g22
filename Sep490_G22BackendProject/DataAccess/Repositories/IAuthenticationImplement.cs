﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IAuthenticationImplement : IAuthenticationRepository
    {
        private readonly sep490_g22Context _context;
        readonly HashPassword _hashPassword;
        public IAuthenticationImplement(HashPassword hashPassword, sep490_g22Context context)
        {
            _context = context;
            _hashPassword = hashPassword; 
        }
        public Account? checkLoginAccount(UserDTO userRequest, Boolean isRefresh)
        {
            try
            {
                Account account = new Account();
                if(isRefresh)
                {
                    account = _context.Accounts.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                    && x.PassWord.Equals(userRequest.password));
                }
                else
                {
                    account = _context.Accounts.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                    && x.PassWord.Equals(_hashPassword.HashPass($"{userRequest.password}")));
                }
                
                if (account != null)
                {
                    return account;
                }
                else
                    return null;
                
            }
            catch (Exception ex)
            {
                return null;    
            }
        }
        public List<Account> GetAccount()
        {
            return _context.Accounts.ToList();
        }
        public (int , StudentProfile?) checkLoginStudent(UserDTO userRequest,Boolean isRefresh)
        {
            try
            {
                StudentProfile account = new StudentProfile();
                if (isRefresh)
                {
                    account = _context.StudentProfiles.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   //&& x.PassWord.Equals(userRequest.password));
                   && x.PassWord.Equals(userRequest.password) && (x.Status == 1 || x.Status == 0));
                }
                else
                {
                    account = _context.StudentProfiles.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   //&& x.PassWord.Equals(userRequest.password));
                   && x.PassWord.Equals(_hashPassword.HashPass($"{userRequest.password}")) && (x.Status == 1 || x.Status == 0));
                }
                
                if (account != null)
                {
                    bool check = CheckCenter(account.CenterId.ToString());
                    if (check == false)
                    {
                        return (1, account);
                    }
                    else
                        return (0, account);
                }
                else return (2, null);
            }
            catch (Exception ex)
            {
                return (2, null);
            }
        }
        public (int, TeacherProfile?) checkLoginTeacher(UserDTO userRequest,Boolean isRefresh)
        {
            try
            {
                TeacherProfile account = new TeacherProfile();
                if(isRefresh)
                {
                    account = _context.TeacherProfiles.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   && x.PassWord.Equals(userRequest.password) && (x.Status == 1 || x.Status == 0));
                }
                else
                {
                    account = _context.TeacherProfiles.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   //&& x.PassWord.Equals(userRequest.password));
                   && x.PassWord.Equals(_hashPassword.HashPass($"{userRequest.password}")) && (x.Status == 1 || x.Status == 0));
                }
                
                if (account != null)
                {
                    bool check = CheckCenter(account.CenterId.ToString());
                    if (check == false)
                    {
                        return (1, account);
                    }
                    else
                        return (0, account);
                }
                else return (2, null);
            }
            catch (Exception ex)
            {
                return (2, null);
            }
        }
        public (int, Employee?) checkLoginEmployee(UserDTO userRequest,Boolean isRefresh)
        {
            try
            {
                Employee account = new Employee();
                if (isRefresh)
                {
                    account = _context.Employees.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   && x.PassWord.Equals(userRequest.password) && (x.Status == 1 || x.Status == 0));
                }
                else
                {
                    account = _context.Employees.SingleOrDefault(x => x.UserName.Equals(userRequest.username)
                   //&& x.PassWord.Equals(userRequest.password));
                   && x.PassWord.Equals(_hashPassword.HashPass($"{userRequest.password}")) && (x.Status == 1 || x.Status == 0));
                }
                if (account != null)
                {
                    bool check = CheckCenter(account.CenterId.ToString());
                    if (check == false)
                    {
                        return (1, account);
                    }
                    else
                        return (0, account);
                }
                else return (2, null);
            }
            catch (Exception ex)
            {
                return (2, null);
            }

        }

        public Center? GetCenter(string id )
        {
            try
            {
                return _context.Centers.FirstOrDefault(u=>u.Id.ToString() == id);
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public bool CheckCenter(string id)
        {
            try
            {
                var check = _context.Centers.FirstOrDefault(u => u.Id.ToString() == id && (u.Status == 0 || u.Status == 1));
                if (check != null)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public Account? checkLogin(UserDTO userRequest)
        {
            throw new NotImplementedException();
        }
    }
}
