﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.NotificationClass;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.DTOs.TeacherTeachingHourDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IReportmplement : IReportRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        readonly HashPassword _hashPassword;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IReportmplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _hashPassword = hashPassword;
            _httpContextAccessor = httpContextAccessor;
            _context= context;
        }
        public class Statistics
        {
            public int Total { get; set; }
            public int InProgress { get; set; }
            public int Finished { get; set; }
            public int Closed { get; set; }
            public int OffMonth { get; set; }
        }
        public class Count
        {
            public int Counts { get; set; }
        }
        public async Task<Dictionary<string, Dictionary<string, Count>>?> ReportMember(string roleName, string centerId, int dateTime)
        {
            List<NotificationClase> viewNotificationClassDTOs = new List<NotificationClase>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    DateTime date;
                    if(dateTime != 0)
                    {
                       date = new DateTime(dateTime, 1, 1);
                    }
                    else
                    {
                        date = DateTime.Now;
                    }
                    Console.WriteLine(date.Year);
                    DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    List<StudentProfile> totalStudent = new List<StudentProfile>();
                    List<TeacherProfile> totalTeacher = new List<TeacherProfile>();
                    List<TeacherTeachingHour> teachingHourDTOs = new List<TeacherTeachingHour>();
                    if (roleName!= null && roleName == "Employee" && centerId != null)
                    {
                        totalStudent = await _context.StudentProfiles.Where(u => u.CenterId.ToString() == centerId && u.DateCreate.Value.Year == date.Year).ToListAsync();
                        teachingHourDTOs = await _context.TeacherTeachingHours.Include(u => u.Schedule).ThenInclude(u => u.Class)
                            .Include(u => u.Schedule).ThenInclude(u => u.SlotDate).Where(u=>u.Schedule.Class.CenterId.ToString() == centerId && u.Schedule.DateOffSlot.Year == date.Year).ToListAsync();
                        totalTeacher = await _context.TeacherProfiles.Where(u => u.CenterId.ToString() == centerId && u.CreateDate.Value.Year == date.Year).ToListAsync();
                    }
                    if (roleName != null && roleName == "Super Admin")
                    {
                        totalStudent = await _context.StudentProfiles.Where(u=>u.DateCreate.Value.Year == date.Year).ToListAsync();
                        teachingHourDTOs = await _context.TeacherTeachingHours.Include(u => u.Schedule).ThenInclude(u => u.Class)
                           .Include(u => u.Schedule).ThenInclude(u => u.SlotDate).Where(u=> u.Schedule.DateOffSlot.Year == date.Year).ToListAsync();
                        totalTeacher = await _context.TeacherProfiles.Where(u => u.CreateDate.Value.Year == date.Year).ToListAsync();
                    }
                    var studentOfMonth = totalStudent.Where(u => u.DateCreate >= firstDayOfMonth && u.DateCreate <= lastDayOfMonth);
                    Dictionary<string, Count> monthlyStatisticsDict = new Dictionary<string, Count>();
                    Dictionary<string, Dictionary<string, Count>> list = new Dictionary<string, Dictionary<string, Count>>();
                    // Lặp qua mỗi lớp và cập nhật thống kê 
                    //Console.WriteLine(totalStudent.Count());
                    foreach (var @class in totalTeacher)
                    {
                        string monthKey = $"{@class.CreateDate?.Year ?? 0}-{@class.CreateDate?.Month ?? 0}";
                        if (!monthlyStatisticsDict.ContainsKey(monthKey))
                        {
                            monthlyStatisticsDict[monthKey] = new Count();
                        }
                        monthlyStatisticsDict[monthKey].Counts++;
                    }
                    for (int month = 1; month <= 12; month++)
                    {
                        string monthKey = $"{DateTime.Now.Year}-{month}";

                        if (!monthlyStatisticsDict.ContainsKey(monthKey))
                        {
                            monthlyStatisticsDict[monthKey] = new Count();
                        }
                    }
                    list.Add("Teacher", monthlyStatisticsDict.OrderBy(kv => int.Parse(kv.Key.Split('-')[1]))
                    .ToDictionary(kv => kv.Key, kv => kv.Value));
                    monthlyStatisticsDict = new Dictionary<string, Count>();
                   foreach (var @class in teachingHourDTOs)
                    {
                        string monthKey = $"{@class.Schedule.DateOffSlot.Year}-{@class.Schedule.DateOffSlot.Month}";
                        if (!monthlyStatisticsDict.ContainsKey(monthKey))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[monthKey] = new Count();
                        }
                        int ins = @class.TimeTeaching?? 0;
                        //Console.WriteLine(ins);
                            monthlyStatisticsDict[monthKey].Counts += ins/60;
                        //Console.WriteLine(monthlyStatisticsDict.Count);
                    }
                    for (int month = 1; month <= 12; month++)
                    {
                        string monthKey = $"{DateTime.Now.Year}-{month}";

                        if (!monthlyStatisticsDict.ContainsKey(monthKey))
                        {
                            monthlyStatisticsDict[monthKey] = new Count();
                        }
                    }
                    list.Add("TeachingHour", monthlyStatisticsDict.OrderBy(kv => int.Parse(kv.Key.Split('-')[1]))
                    .ToDictionary(kv => kv.Key, kv => kv.Value));
                    return list;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<Dictionary<string, Dictionary<string, Statistics>>?> ReportCol(string roleName, string centerId)
        {
            List<NotificationClase> viewNotificationClassDTOs = new List<NotificationClase>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    List<Course> totalCourse = new List<Course>();
                    List<Class> totalClass = new List<Class>();
                    List<ClassRoom> totalRooom = new List<ClassRoom>();

                    if (roleName != null && roleName == "Employee" && centerId != null)
                    {
                        totalCourse = await _context.Courses.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                        totalClass = await _context.Classes.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                        totalRooom = await _context.ClassRooms.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                    }
                    if (roleName != null && roleName == "Super Admin")
                    {
                        totalCourse = await _context.Courses.ToListAsync();
                        totalClass = await _context.Classes.ToListAsync();
                        totalRooom = await _context.ClassRooms.ToListAsync();
                    }
                    Dictionary<string, Statistics> monthlyStatisticsDict = new Dictionary<string, Statistics>();
                    Dictionary<string, Dictionary<string, Statistics>> list = new Dictionary<string, Dictionary<string, Statistics>>();
                    foreach (var @class in totalCourse)
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "Course";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                        // Cập nhật thống kê cho tháng

                        if (@class.Status == 1 || @class.Status == 0)
                        {
                            monthlyStatisticsDict[course].InProgress++;
                        }
                        else if (@class.Status == 2)
                        {
                            monthlyStatisticsDict[course].Finished++;
                        }
                        else if (@class.Status == 3)
                        {
                            monthlyStatisticsDict[course].Closed++;
                        }
                        else if (@class.DateCreate >= firstDayOfMonth && @class.DateCreate <= lastDayOfMonth)
                        {
                            monthlyStatisticsDict[course].OffMonth++;
                        }
                    }
                    foreach (var @class in totalClass )
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "Class";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                        // Cập nhật thống kê cho tháng

                        if (@class.Status == 1 || @class.Status == 0)
                        {
                            monthlyStatisticsDict[course].InProgress++;
                        }
                        else if (@class.Status == 2)
                        {
                            monthlyStatisticsDict[course].Finished++;
                        }
                        else if (@class.Status == 3)
                        {
                            monthlyStatisticsDict[course].Closed++;
                        }
                        else if (@class.DateCreate >= firstDayOfMonth && @class.DateCreate <= lastDayOfMonth)
                        {
                            monthlyStatisticsDict[course].OffMonth++;
                        }
                    }
                    foreach (var @class in totalRooom )
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "ClassRoom";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                        // Cập nhật thống kê cho tháng

                        //if (@class.Status == 1 || @class.Status == 0)
                        //{
                           monthlyStatisticsDict[course].InProgress++;
                        //}
                        //else if (@class.Status == 2)
                        //{
                        //    monthlyStatisticsDict[course].Finished++;
                        //}
                        //else if (@class.Status == 3)
                        //{
                        //    monthlyStatisticsDict[course].Closed++;
                        //}
                        //else if (@class.DateCreate >= firstDayOfMonth && @class.DateCreate <= lastDayOfMonth)
                        //{
                        //    monthlyStatisticsDict[course].OffMonth++;
                        //}
                    }
                    list.Add("string", monthlyStatisticsDict);
                    return list;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<Dictionary<string, Statistics>?> ReportTotalMember(string roleName, string centerId)
        {
            List<NotificationClase> viewNotificationClassDTOs = new List<NotificationClase>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    List<StudentProfile> totalStudent = new List<StudentProfile>();
                    List<TeacherProfile> totalTeacher = new List<TeacherProfile>();
                    List<Employee> totalEmployee = new List<Employee>();
                    if (roleName != null && roleName == "Employee" && centerId != null)
                    {
                        totalStudent = await _context.StudentProfiles.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                        totalTeacher = await _context.TeacherProfiles.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                        totalEmployee = await _context.Employees.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                    }
                    if (roleName != null && roleName == "Super Admin")
                    {
                        totalStudent = await _context.StudentProfiles.ToListAsync();
                        totalTeacher = await _context.TeacherProfiles.ToListAsync();
                        totalEmployee = await _context.Employees.ToListAsync();
                    } 
                    DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    var studentOffMonth = totalStudent.Where(u => u.DateCreate >= firstDayOfMonth && u.DateCreate <= lastDayOfMonth).ToList();

                    Dictionary<string, Statistics> monthlyStatisticsDict = new Dictionary<string, Statistics>();
                    foreach (var @class in totalStudent)
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "Student";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                    }
                    foreach (var @class in totalTeacher)
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "Teacher";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                    }
                    foreach (var @class in totalEmployee)
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "Employee";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                    }
                    foreach (var @class in studentOffMonth)
                    {
                        //string monthKey = $"{@class.DateCreate?.Year ?? 0}-{@class.DateCreate?.Month ?? 0}";
                        string course = "StudnentOffMonth";
                        if (!monthlyStatisticsDict.ContainsKey(course))
                        {
                            // Nếu tháng chưa tồn tại trong Dictionary, tạo mới
                            monthlyStatisticsDict[course] = new Statistics();
                        }
                        monthlyStatisticsDict[course].Total++;
                    }
                    return monthlyStatisticsDict;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<List<TeacherProfile>> ListTeacherAdminSide(string roleName, string centerId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            List<TeacherProfile> teacherProfile = new List<TeacherProfile>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    
                    if (roleName != null && roleName == "Employee" && centerId != null)
                    {
                        teacherProfile = await _context.TeacherProfiles
                            .Where(u => u.CenterId.ToString() == centerId).Include(u => u.TeacherTeachingHours)
                            .ToListAsync();
                    }
                    if (roleName != null && roleName == "Super Admin")
                    {
                        teacherProfile = await _context.TeacherProfiles.Include(u => u.TeacherTeachingHours)
                             .ToListAsync();
                    }
                    foreach (var item in teacherProfile)
                    {
                        // Update the Image property by appending the desired string
                        item.Image = $"{baseUrl}/Images/{item.Image}";
                    }

                    // Sắp xếp theo số lớp học giảng viên đang dạy giảm dần
                    teacherProfile = teacherProfile.OrderByDescending(t => t.TeacherTeachingHours.Count()).ToList();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return teacherProfile;
        }
    }
}
