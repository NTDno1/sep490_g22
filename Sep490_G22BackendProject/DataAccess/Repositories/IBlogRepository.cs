﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.CateBlogDTO;
using BusinessObject.Models;
using Pagination.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IBlogRepository
    {
        Task<List<ViewBlogDTO>> viewBlogs(string? searchText, int? cateId);
        Task<Pagination<ViewBlogDTO>> viewBlogsForClientSide(int page, int limit, string? searchText, int? cateId);
        Task<List<ViewBlogDTO>> viewBlogHP();
        Task<ViewBlogDTO> findBlogById(string code);
        Task addBlog(CreateBlogDTO blogDTO, Guid employeeId);
        Task updateBlog(string code, UpdateBlogDTO updateBlogDTO, Guid employeeId);
        Task deleteBlog(List<string> ids);
        Task<List<ViewCateBlogDTO>> getCateBlog();
        Task<List<ViewBlogDTO>> getBlogByCate(int? cateId);
        Task<List<ViewCateBlogDTO>> getCateBlogById(int? cateId);
        Task<List<ViewBlogDTO>> getRelatedBlog(int? cateId, string? blogCode);
        Task updateStatusBlog(int? status, string? code);

        public List<string> getImageFileNames(string description);

        public void changeImageFolder(List<string> imagePath);
        public string changeImgDescription(string description);

    }
}
