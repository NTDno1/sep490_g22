﻿using BusinessObject.Models;
using BusinessObject.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.SyllabusDTO;

namespace DataAccess.Repositories
{
    public class ICenterImplement : ICenterRepository
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        public ICenterImplement(IMapper mapper , sep490_g22Context context)
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<ListCenterDTOs>?> ListCenter()
        {
            List<Center> listCenters = new List<Center>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    listCenters = await _context.Centers.Include(u=>u.Admin).ToListAsync();
                    if(listCenters != null)
                    {
                        return _mapper.Map<List<ListCenterDTOs>>(listCenters);
                    }
                    else
                    {
                        return null;
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            
        }
        public async Task<(bool, List<string>)> UpdateCenter(UpdateCenterDTOs updateCenterDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var center = await _context.Centers.FirstOrDefaultAsync(c => c.Id.Equals(updateCenterDTOs.Id));
                    if (center != null)
                    {
                        center.Name = updateCenterDTOs.Name;
                        center.Description = updateCenterDTOs.Description;
                        center.Adress = updateCenterDTOs.Adress;
                        //center.CodeCenter = updateCenterDTOs.CodeCenter;
                        center.NumberPhone = updateCenterDTOs.NumberPhone;
                        center.UpdateDate = DateTime.Now;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Update Success" });
                    }
                    else
                    {
                        return (false, new List<string> { "Update False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddCenter(AddNewCenterDTOs addNewCenterDTOs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (addNewCenterDTOs != null)
                    {
                        var newCenter = new Center
                        {
                            Id = Guid.NewGuid(),
                            Name = addNewCenterDTOs.Name,
                            Description = addNewCenterDTOs.Description,
                            Adress = addNewCenterDTOs.Adress,
                            CodeCenter = addNewCenterDTOs.CodeCenter,
                            AdminId = Guid.Parse(addNewCenterDTOs.AdminId),
                            NumberPhone = addNewCenterDTOs.NumberPhone,
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            Status = 0,
                        };
                        await _context.AddAsync(newCenter);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Add Sucess" });
                    }
                    else
                    {
                        return (false, new List<string> { "Add False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteCenter(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var checkStatus = await _context.Centers.FirstOrDefaultAsync(u=>u.Id.ToString() == id && (u.Status == 1 || u.Status ==2));
                        if (checkStatus!= null)
                        {
                            return (false, new List<string> { "The center is in operation and cannot be deleted" });
                        }
                        var check = await _context.Syllabi.Include(u => u.Center).FirstOrDefaultAsync(u => u.Center.Id.ToString() == id);
                        if (check != null)
                        {
                            return (false, new List<string> { "The center is in operation and cannot be deleted" });
                        }
                        var center = await _context.Centers.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (center != null)
                        {
                            _context.Centers.Remove(center);
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Delete Sucess" });
                        }
                        return (false, new List<string> { "Delete False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Delete False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> CloseCenter(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var closeCenter = await _context.Centers.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if(closeCenter != null)
                        {
                            closeCenter.Status = 2;
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Closed Success" });
                        }
                        return (false, new List<string> { "Closed False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Closed False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> OpenCenter(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (id != null)
                    {
                        var closeCenter = await _context.Centers.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                        if (closeCenter != null)
                        {
                            closeCenter.Status = 1;
                            await _context.SaveChangesAsync();
                            return (true, new List<string> { "Opened Success" });
                        }
                        return (false, new List<string> { "Opened False" });
                    }
                    else
                    {
                        return (false, new List<string> { "Opened False" });
                    }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ListCenterDTOs?> CenterDetail(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    var center = await _context.Centers.Include(u=>u.Admin).FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    return _mapper.Map<ListCenterDTOs>(center);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
    }
}
