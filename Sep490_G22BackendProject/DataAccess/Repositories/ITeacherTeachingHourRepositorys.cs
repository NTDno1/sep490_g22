﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SchedualDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherTeachingHourDTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ITeacherTeachingHourRepositorys
    {
        Task<(bool, List<string>?)> AddTeacherTeachingHour(ViewSchedualDTO viewSchedualDTO, int timeTeaching);
        Task<bool> CheckScheduleInTeachHour(string scheduleId);
        Task<List<ViewTeacherTeachingHourDTO>> ListTeacherTeachingHour(string teacherId);
        Task<List<ViewTeacherTeachingHourDTO>> ListTeacherTeachingHourAdminSide(string centerId);
    }
}
