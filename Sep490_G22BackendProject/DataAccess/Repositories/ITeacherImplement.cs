﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmailDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.StudentClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ITeacherImplement : ITeacherRepositorys
    {
        private readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        readonly HashPassword _hashPassword;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailRepositorys _emailRepository;
        public ITeacherImplement(HashPassword hashPassword, IMapper mapper, IHttpContextAccessor httpContextAccessor, IEmailRepositorys emailRepository, sep490_g22Context context)
        {
            _mapper = mapper;
            _hashPassword = hashPassword;
            _httpContextAccessor = httpContextAccessor;
            _emailRepository = emailRepository;
            _context = context;
        }
        public async Task<List<ViewTeacherDTO>> ListTeacherAdminSide(string? centerId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            List<TeacherProfile> teacherProfile = new List<TeacherProfile>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (centerId == null || centerId.Length <= 0)
                {
                    teacherProfile = await _context.TeacherProfiles.ToListAsync();
                    foreach (var item in teacherProfile)
                    {
                        // Update the Image property by appending the desired string
                        item.Image = $"{baseUrl}/Images/Teacher/{item.Image}";
                    }
                }
                if (!(centerId == null || centerId.Length <= 0))
                {
                    teacherProfile = await _context.TeacherProfiles.Where(u => u.CenterId.ToString() == centerId).ToListAsync();
                    foreach (var item in teacherProfile)
                    {
                        // Update the Image property by appending the desired string
                        item.Image = $"{baseUrl}/Images/Teacher/{item.Image}";
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewTeacherDTO>>(teacherProfile);
        }
        public async Task<List<ViewTeacherDTO>> ListTeacherClientSide(string? centerId)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            List<TeacherProfile> teacherProfile = new List<TeacherProfile>();
            try
            {
                if (centerId == null || centerId.Length <= 0)
                {
                    var teacherProfiles = await _context.TeacherProfiles
                        .Include(u => u.TeacherTeachingHours)
                        .OrderByDescending(t => t.TeacherTeachingHours.Count)
                        .ToListAsync();
                    teacherProfile = teacherProfiles.Take(4).ToList();
                    foreach (var item in teacherProfile)
                    {
                        // Update the Image property by appending the desired string
                        item.Image = $"{baseUrl}/Images/Teacher/{item.Image}";
                        item.UserName = null;
                        item.PassWord = null;
                    }
                }
                if (!(centerId == null || centerId.Length <= 0))
                {
                   var teacherProfiles = await _context.TeacherProfiles
     .Include(u => u.TeacherTeachingHours)
     .OrderByDescending(t => t.TeacherTeachingHours.Count)
     .Where(u => u.CenterId.ToString() == centerId)
     .ToListAsync();
                    // Chọn ra top 4
                    teacherProfile = teacherProfiles.Take(4).ToList();
                    foreach (var item in teacherProfile)
                    {
                        // Update the Image property by appending the desired string
                        item.Image = $"{baseUrl}/Images/Teacher/{item.Image}";
                        item.UserName = null;
                        item.PassWord = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return _mapper.Map<List<ViewTeacherDTO>>(teacherProfile);
        }
        public async Task<(bool, List<string>)> UpdateTeacher(UpdateProfileTeacherDTO updateProfileTeacherDTO, bool avaimg, bool cetimg, string avataimg, string cetiimgs)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                var teacher = await _context.TeacherProfiles.FirstOrDefaultAsync(c => c.Id.Equals(updateProfileTeacherDTO.Id));
                if (teacher != null)
                {
                    teacher.FirstName = updateProfileTeacherDTO.FirstName;
                    //teacher.MidName = updateProfileTeacherDTO.MidName;
                    //teacher.LastName = updateProfileTeacherDTO.LastName;
                    teacher.Address = updateProfileTeacherDTO.Address;
                    teacher.Email = updateProfileTeacherDTO.Email;
                    teacher.PhoneNumber = updateProfileTeacherDTO.PhoneNumber;
                    teacher.Dob = updateProfileTeacherDTO.Dob;
                    if(avaimg== true && !avataimg.Contains("https://")){
                        teacher.Image = avataimg;
                    }
                    teacher.UpdateDate = DateTime.Now;
                    teacher.CertificateName = updateProfileTeacherDTO.CertificateName;
                    teacher.Title = updateProfileTeacherDTO.Title;
                    teacher.Description = updateProfileTeacherDTO.Description;
                    teacher.ReleaseDate = updateProfileTeacherDTO.ReleaseDate;
                    if(cetimg == true && !cetiimgs.Contains("https://")){
                        teacher.ImageCertificateName = cetiimgs;
                    }
                    await _context.SaveChangesAsync();
                    return (true, new List<string> { "Update Success" });
                }
                else
                {
                    return (false, new List<string> { "Update False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> AddTeacher(AddTeacherDTOs addTeacherDTO , bool avaimg, bool cetimg, string avataimg, string cetiimgs, string centerId, string userId)
        {
            try
            {
                string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random random = new Random();
                int length = 8;
                string randomString = new string(Enumerable.Repeat(characters, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //using (var _context = new sep490_g22Context())
                //{
                if (addTeacherDTO != null)
                {
                    var newTeacher = new TeacherProfile
                    {
                        Id = Guid.NewGuid(),
                        UserName = addTeacherDTO.UserName,
                        PassWord = _hashPassword.HashPass($"{randomString}"),
                        CenterId = Guid.Parse(centerId),
                        FirstName = addTeacherDTO.FirstName,
                        Address = addTeacherDTO.Address, 
                        PhoneNumber = addTeacherDTO.PhoneNumber, 
                        Email = addTeacherDTO.Email,
                        Dob = addTeacherDTO.Dob?? new DateTime(2000, 2, 2),
                        CertificateName= addTeacherDTO.CertificateName,
                        Title = addTeacherDTO.Title,
                        Description =addTeacherDTO.Description,
                        ReleaseDate = addTeacherDTO.ReleaseDate,
                        Image = avaimg ? avataimg : "",
                        ImageCertificateName = cetimg ? cetiimgs : "",
                        Status = 0,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                    };
                    await _context.AddAsync(newTeacher);
                    await _context.SaveChangesAsync();
                    SendEamilDto request = new SendEamilDto
                    {
                        To = addTeacherDTO.Email,
                        Subject = "New Account PI ONLINE TRAINING WEBSITE",
                        Body = $"<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n  <style>\r\n    body {{\r\n      font-family: Arial, sans-serif;\r\n    }}\r\n\r\n    .notification {{\r\n      background-color: #f9f9f9;\r\n      border: 1px solid #ddd;\r\n      padding: 10px;\r\n      margin: 10px 0;\r\n    }}\r\n\r\n    .notification-header {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      padding: 10px;\r\n      text-align: center;\r\n      font-size: 20px;\r\n    }}\r\n\r\n    .notification span {{\r\n      background-color: #4CAF50;\r\n      color: white;\r\n      width:300px;\r\n      border: none;\r\n      padding: 10px 20px;\r\n      text-align: center;\r\n      text-decoration: none;\r\n      display: inline-block;\r\n      font-size: 16px;\r\n      margin: 10px 2px;\r\n      display: block;\r\n      margin-left: auto;\r\n      margin-right: auto;\r\n    }}\r\n    .notification a {{\r\n      color: #4CAF50; /* Màu của đường link */\r\n      text-decoration: underline; /* Gạch chân dưới cho đường link */\r\n      font-weight: bold; /* Đậm chữ cho đường link */\r\n    }}\r\n  </style>\r\n</head>\r\n\r\n<body>\r\n\r\n  <div class=\"notification\">\r\n    <div class=\"notification-header\">\r\n      POTMS Training Platform\r\n    </div>\r\n    <p>Hello New Teacher!</p>\r\n    <p>This is your UserName:</p>\r\n     <span>{addTeacherDTO.UserName}</span> \r\n    <p>This is your Password:</p>\r\n    <span>{randomString}</span>\r\n    <p>You can log in here: <a href=\"http://pitech.edu.vn/login\">Login</a>.</p>\r\n    <p>Please do not reply directly to this email. Copyright © 2023 POTMS. All rights reserved.\r\n      Contact Us | Legal Notices and Terms of Use | Privacy Statement.</p>\r\n  </div>\r\n\r\n</body>\r\n\r\n</html>"
                    };
                    _emailRepository.SendEmail(request);
                    return (true, new List<string> { "Add Sucess" });
                }
                else
                {
                    return (false, new List<string> { "Add False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteTeacher(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var checkStatus = await _context.Classes.FirstOrDefaultAsync(u => u.TeacherId.ToString() == id);
                    if (checkStatus != null)
                    {
                        return (false, new List<string> { "Teachers existing in the classroom cannot be deleted." });
                    }
                    var teacherProfile = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    if (teacherProfile != null)
                    {
                        _context.TeacherProfiles.Remove(teacherProfile);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Delete Sucess" });
                    }
                    return (false, new List<string> { "Delete False" });
                }
                else
                {
                    return (false, new List<string> { "Delete False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeleteListTeacher(List<string> id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    List<Class> classes = new List<Class>();
                    foreach (var item in id)
                    {
                        var checkStatus = await _context.Classes.FirstOrDefaultAsync(u => u.TeacherId.ToString() == item);
                        if (checkStatus != null)
                        {
                            classes.Add(checkStatus);
                        }
                    }
                    if (classes == null || classes.Count == 0)
                    {
                        foreach (var item in id)
                        {
                            var teacherProfile = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == item);
                            if (teacherProfile != null)
                            {
                                _context.TeacherProfiles.Remove(teacherProfile);
                                await _context.SaveChangesAsync();
                            }
                        }
                        return (true, new List<string> { "Delete Sucess" });
                    }
                    else
                    {
                        List<string> strings = new List<string>();
                        foreach (var item in classes)
                        {
                            var teacher = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id == item.TeacherId);
                            strings.Add($"Teachers: {teacher.FirstName + " " + teacher.MidName + " " + teacher.LastName} existing in the classroom: {item.Name} cannot be deleted.");
                        }
                        return (false, strings);
                    }
                }
                else
                {
                    return (false, new List<string> { "Delete False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> DeactivateTeacher(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var employee = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    var listClassOfTeacher = await _context.Classes.Where(u => u.TeacherId.ToString() == id).ToListAsync();
                    if (employee != null)
                    {
                        employee.Status = 2;
                        await _context.SaveChangesAsync();
                        if (listClassOfTeacher != null) { }
                        await _context.Classes.Where(u => u.TeacherId.ToString() == id).ForEachAsync(s => s.Status = 4);
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Deactivate Success" });
                    }
                    return (false, new List<string> { "Deactivate False" });
                }
                else
                {
                    return (false, new List<string> { "Deactivate False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<(bool, List<string>)> ActivateTeacher(string id)
        {
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                if (id != null)
                {
                    var employee = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                    if (employee != null)
                    {
                        employee.Status = 1;
                        await _context.SaveChangesAsync();
                        return (true, new List<string> { "Activate Success" });
                    }
                    return (false, new List<string> { "Activate False" });
                }
                else
                {
                    return (false, new List<string> { "Activate False" });
                }
                //}
            }
            catch (Exception ex)
            {
                return (false, new List<string> { ex.Message });
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
        }
        public async Task<ViewTeacherDTO?> TeacherDetail(string id)
        {
            var httpRequest = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                var center = await _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                center.Image = $"{baseUrl}/Images/Teacher/{center.Image}";
                center.ImageCertificateName = $"{baseUrl}/Images/Teacher/{center.ImageCertificateName}";
                return _mapper.Map<ViewTeacherDTO>(center);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return null;
        }
    }
}
