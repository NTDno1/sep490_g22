﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.ClassRoomDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using DataAccess.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IClassRoomImplement : IClassRoomRepositorys
    {
        readonly sep490_g22Context _context;
        private readonly IMapper _mapper;
        protected ApiResponse _response;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IClassRoomImplement(HashPassword hashPassword, IMapper mapper, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _context = context;
            _response = new();
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<List<ViewClassRoomDTO>> ListAllRoom(string centerId)
        {
            List<ClassRoom> ClassRooms = new List<ClassRoom>();
            try
            {
                //using (var _context = new sep490_g22Context())
                //{
                    if (centerId == null || centerId.Length <= 0)
                    {
                        ClassRooms = await _context.ClassRooms.ToListAsync();
                    }
                    if (!(centerId == null || centerId.Length <= 0))
                    {
                        ClassRooms = await _context.ClassRooms.Where(u => u.CenterId.ToString() == centerId).Include(u => u.Type).ToListAsync();
                    }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //finally
            //{
            //    if (_context != null)
            //    {
            //        await _context.DisposeAsync();
            //    }
            //}
            return _mapper.Map<List<ViewClassRoomDTO>>(ClassRooms);
        }
    }
}
