﻿using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.DTOs.CateBlogDTO;
using BusinessObject.Models;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Pagination.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace DataAccess.Repositories
{
    public class IBlogImplement : IBlogRepository
    {
        private readonly sep490_g22Context _context;
        public static readonly int STATUS_ACTIVE = 1;
        public static readonly int STATUS_NOT_ACTIVE = 0;
        public static readonly int CATE_BLOG = 1;
        public static readonly int CATE_NOTIFICATION = 2;
        public static readonly int CATE_NEWS = 3;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IBlogImplement(IMapper mapper, IWebHostEnvironment hostEnvironment, IHttpContextAccessor httpContextAccessor, sep490_g22Context context)
        {
            _mapper = mapper;
            _hostEnvironment = hostEnvironment;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }

        private readonly IMapper _mapper;
        public async Task<List<ViewBlogDTO>> viewBlogs(string? searchText, int? cateId)
        {
            try
            {
                var query = _context.Blogs.AsQueryable();

                if (cateId != null)
                {
                    query = query.Include(s => s.Cate).Where(s => s.CateId == cateId).OrderByDescending(article => article.UpdateDate.HasValue ? article.UpdateDate : article.CreatedDate);
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(x => x.Title.Contains(searchText));
                }

                List<Blog> listBlog = await query.ToListAsync();

                if (listBlog == null || listBlog.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu");
                }

                foreach (var blog in listBlog)
                {
                    blog.Images = $"{baseUrl}/Images/{blog.Images}";
                }

                List<ViewBlogDTO> listBlogDTOs = _mapper.Map<List<ViewBlogDTO>>(listBlog);
                return listBlogDTOs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task addBlog(CreateBlogDTO createblogDTO, Guid employeeId)
        {
            try
            {
                if (string.IsNullOrEmpty(createblogDTO.Title.Trim()) || string.IsNullOrEmpty(createblogDTO.ShortDescription.Trim()))
                {
                    throw new Exception("Thông tin không được để trống");
                }

                if (createblogDTO.Description != null)
                {
                    bool containsImageSrc = Regex.IsMatch(createblogDTO.Description, @"<img[^>]*src=[\""']([^\""']*)[\""']");
                    if (containsImageSrc)
                    {
                        List<string> lstImagePath = getImageFileNames(createblogDTO.Description);
                         changeImageFolder(lstImagePath);
                        createblogDTO.Description =  changeImgDescription(createblogDTO.Description);
                    }
                }

                var userName = await _context.Accounts
                    .Where(s => s.Id == employeeId)
                    .Select(x => x.UserName)
                    .FirstOrDefaultAsync();
                Blog blog = _mapper.Map<Blog>(createblogDTO);
                blog.CateId = createblogDTO.CateId;
                blog.Description = createblogDTO.Description;
                blog.BlogCode = Guid.NewGuid();
                blog.CreatedDate = DateTime.Now;
                blog.CreatedBy = userName.ToString();
                blog.Status = STATUS_ACTIVE;
                if (!string.IsNullOrEmpty(createblogDTO.Url))
                {
                    blog.Url = createblogDTO.Url;
                }
                if(createblogDTO.Position.HasValue)
                {
                    blog.Position = createblogDTO.Position;
                }
                _context.Blogs.Add(blog);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task updateBlog(string code, UpdateBlogDTO updateBlogDTO, Guid employeeId)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                {
                    throw new Exception("Mã code không được trống");
                }

                if (updateBlogDTO.Description != null)
                {
                    bool containsImageSrc = Regex.IsMatch(updateBlogDTO.Description, @"<img[^>]*src=[\""']([^\""']*)[\""']");
                    if (containsImageSrc)
                    {
                        List<string> lstImagePath = getImageFileNames(updateBlogDTO.Description);
                         changeImageFolder(lstImagePath);
                        updateBlogDTO.Description = changeImgDescription(updateBlogDTO.Description);
                    }
                }

                var data = await _context.Blogs.FirstOrDefaultAsync(c => c.BlogCode.ToString() == code);

                if (data == null)
                {
                    throw new Exception("Không tìm thấy bài đăng");
                }

                if (updateBlogDTO.Title != null) { data.Title = updateBlogDTO.Title; }
                data.Description = updateBlogDTO.Description ?? data.Description;
                if (updateBlogDTO.ShortDescription != null) { data.ShortDescription = updateBlogDTO.ShortDescription; }
                if (updateBlogDTO.Img != null) { data.Images = updateBlogDTO.Img; }

                var userName = await _context.Accounts
                     .Where(s => s.Id == employeeId)
                     .Select(x => x.UserName)
                     .FirstOrDefaultAsync();

                data.UpdateDate = DateTime.Now;
                data.UpdateBy = userName.ToString();
                if (!string.IsNullOrEmpty(updateBlogDTO.Url))
                {
                    data.Url = updateBlogDTO.Url;
                }
                if (updateBlogDTO.Position.HasValue)
                {
                    data.Position = updateBlogDTO.Position;
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewBlogDTO> findBlogById(string code)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                {
                    throw new Exception("Mã code không được trống");
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                var data = await _context.Blogs.FirstOrDefaultAsync(c => c.BlogCode.ToString() == code);

                if (data == null)
                {
                    throw new Exception("Không tìm thấy dữ liệu");
                }

                data.Images = $"{baseUrl}/Images/{data.Images}";

                ViewBlogDTO blogDTO = _mapper.Map<ViewBlogDTO>(data);
                return blogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task deleteBlog(List<string> ids)
        {
            try
            {
                if (ids == null || ids.Count == 0)
                {
                    throw new Exception("Hãy chọn bài đăng!");
                }

                foreach (string id in ids)
                {
                    var data = await _context.Blogs.FirstOrDefaultAsync(c => c.Id.ToString() == id);

                    if (data == null)
                    {
                        throw new Exception("Không tìm thấy id: " + id + " trên hệ thống");
                    }

                    bool containsImageSrc = false;

                    if (data.Description!= null)
                    {
                        containsImageSrc = Regex.IsMatch(data.Description, @"<img[^>]*src=[\""']([^\""']*)[\""']");
                    }

                    if (containsImageSrc)
                    {
                        List<string> lstImagePath = getImageFileNames(data.Description);
                        string sourceDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Images");

                        foreach (var fileName in lstImagePath)
                        {
                            string sourceFilePath = Path.Combine(sourceDirectory, fileName);

                            if (File.Exists(sourceFilePath))
                            {
                                File.Delete(sourceFilePath);
                            }
                        }
                    }

                    if (data.Images != null)
                    {
                        string fileName = Path.GetFileName(data.Images);
                        string sourceDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                        string sourceFilePath = Path.Combine(sourceDirectory, fileName);

                        if (File.Exists(sourceFilePath))
                        {
                            File.Delete(sourceFilePath);
                        }
                    }

                    _context.Remove(data);
                }

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewBlogDTO>> viewBlogHP()
        {
            try
            {
                var data = await _context.Blogs
                    .Where(s => s.CateId == 1 && s.Status == 1)
                    .OrderByDescending(c => c.CreatedDate)
                    .Take(3)
                    .ToListAsync();

                if (data == null || data.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                foreach (var item in data)
                {
                    item.Images = $"{baseUrl}/Images/{item.Images}";
                }

                List<ViewBlogDTO> lstBlogDTO = _mapper.Map<List<ViewBlogDTO>>(data);
                return lstBlogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<string> getImageFileNames(string description)
        {
            List<string> lstImage = new List<string>();
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(description);

            var imageUrls = doc.DocumentNode
                .SelectNodes("//img[@src]")
                .Select(img => img.GetAttributeValue("src", null))
                .ToList();

            foreach (var imageUrl in imageUrls)
            {
                string fileName = System.IO.Path.GetFileName(new Uri(imageUrl).LocalPath);
                //  string updatedImageUrl = imageUrl.Replace("Uploads", "Images");
                lstImage.Add(fileName);
            }

            return lstImage;
        }


        public void changeImageFolder(List<string> imagePaths)
        {
            string currentProjectDirectory = Directory.GetCurrentDirectory();
            string sourceDirectory = Path.Combine(currentProjectDirectory, "Uploads");
            string destinationDirectory = Path.Combine(currentProjectDirectory, "Images");

            foreach (var fileName in imagePaths)
            {
                string sourceFilePath = Path.Combine(sourceDirectory, fileName);
                string destinationFilePath = Path.Combine(destinationDirectory, fileName);
                if (!File.Exists(sourceFilePath))
                {
                    break;
                }
                if (!File.Exists(destinationFilePath))
                {
                    try
                    {
                        File.Copy(sourceFilePath, destinationFilePath, true);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Lỗi khi xử lý tệp tin");
                    }
                }
            }
            foreach (string filePath in Directory.EnumerateFiles(sourceDirectory))
            {
                File.Delete(filePath);
            }
        }
        public string changeImgDescription(string description)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(description);

            var imageNodes = doc.DocumentNode.SelectNodes("//img[@src]");

            if (imageNodes != null)
            {

                foreach (var imageNode in imageNodes)
                {
                    string imageUrl = imageNode.GetAttributeValue("src", null);
                    if (imageUrl != null)
                    {
                        string updatedImageUrl = ReplaceFirstUploadsInUrl(imageUrl);
                        imageNode.SetAttributeValue("src", updatedImageUrl);
                    }
                }
            }
            return doc.DocumentNode.OuterHtml;
        }

        public string ReplaceFirstUploadsInUrl(string input)
        {
            int firstOccurrenceIndex = input.IndexOf("Uploads");

            if (firstOccurrenceIndex >= 0)
            {
                // Tìm vị trí của "Uploads" đầu tiên
                int startIndex = firstOccurrenceIndex;
                int endIndex = firstOccurrenceIndex + "Uploads".Length;

                // Tách chuỗi thành ba phần: phần trước "Uploads", "Uploads", và phần sau "Uploads"
                string partBefore = input.Substring(0, startIndex);
                string partAfter = input.Substring(endIndex);

                // Thay thế "Uploads" đầu tiên bằng "Images" và ghép lại thành chuỗi mới
                input = partBefore + "Images" + partAfter;
            }

            return input;
        }

        public async Task<List<ViewCateBlogDTO>> getCateBlog()
        {
            try
            {
                var data = await _context.CateBlogs.ToListAsync();

                if (data == null || data.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu");
                }

                List<ViewCateBlogDTO> lstCateBlogDTO = _mapper.Map<List<ViewCateBlogDTO>>(data);
                return lstCateBlogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewBlogDTO>> getBlogByCate(int? cateId)
        {
            try
            {
                var data = _context.Blogs.Where(s => s.CateId == cateId).AsQueryable();

                
if (cateId == 3 || cateId == 2||cateId==11)
                {
                    data = data.Where(s => s.Status == STATUS_ACTIVE)
                               .OrderBy(s => s.Position);
                }
                else
                {
                    data = (IQueryable<Blog>)data.ToList();
                }

                List<ViewBlogDTO> lstBlogDTO = _mapper.Map<List<ViewBlogDTO>>(await data.ToListAsync());

                if (lstBlogDTO == null || lstBlogDTO.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                return lstBlogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ViewCateBlogDTO>> getCateBlogById(int? cateId)
        {
            try
            {
                var data = await _context.CateBlogs
                    .Where(s => s.ParentId == cateId)
                    .Include(s => s.Blogs)
                    .ToListAsync();

                if (data == null || data.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                List<ViewCateBlogDTO> lstBlogDTO = _mapper.Map<List<ViewCateBlogDTO>>(data);
                return lstBlogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void DeleteImage(string imageName)
        {
            try
            {
                var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);

                if (File.Exists(imagePath))
                {
                    File.Delete(imagePath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi ảnh!");
            }
        }

        public async Task<Pagination<ViewBlogDTO>> viewBlogsForClientSide(int page, int limit, string? searchText, int? cateId)
        {
            try
            {
                var query = _context.Blogs
                    .Where(s => s.Status == STATUS_ACTIVE)
                    .AsQueryable();

                if (cateId != null)
                {
                    query = query.Where(s => s.CateId == cateId);
                }

                var totalItems = await query.CountAsync();

                if (totalItems == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(x => x.Title.Contains(searchText));
                }

                List<Blog> paginatedData = await query
                    .OrderByDescending(article => article.UpdateDate.HasValue ? article.UpdateDate : article.CreatedDate)
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .ToListAsync();

                foreach (var i in paginatedData)
                {
                    i.Images = $"{baseUrl}/Images/{i.Images}";
                }

                IEnumerable<ViewBlogDTO> listBlogDTOs = _mapper.Map<IEnumerable<ViewBlogDTO>>(paginatedData);
                return new Pagination<ViewBlogDTO>(listBlogDTOs, totalItems, page, limit);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewBlogDTO>> getRelatedBlog(int? cateId, string? blogCode)
        {
            try
            {
                var data = await _context.Blogs
                    .Where(s => s.CateId == 1 && !s.BlogCode.ToString().Equals(blogCode))
                    .OrderByDescending(c => c.CreatedDate)
                    .Take(3)
                    .ToListAsync();

                if (data == null || data.Count == 0)
                {
                    throw new Exception("Không tìm thấy dữ liệu trên hệ thống");
                }

                var httpRequest = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{httpRequest.Scheme}://{httpRequest.Host}{httpRequest.PathBase}";

                foreach (var item in data)
                {
                    item.Images = $"{baseUrl}/Images/{item.Images}";
                }

                List<ViewBlogDTO> lstBlogDTO = _mapper.Map<List<ViewBlogDTO>>(data);
                return lstBlogDTO;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task updateStatusBlog(int? status, string? code)
        {
            try
            {
                if (String.IsNullOrEmpty(code))
                {
                    throw new Exception("Mã code không được để trống");
                }

                var data = await _context.Blogs.FirstOrDefaultAsync(c => c.BlogCode.ToString() == code);

                if (data == null)
                {
                    throw new Exception("Không tìm thấy dữ liệu này");
                }

                data.Status = status;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

