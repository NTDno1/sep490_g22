﻿using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.SchedualDTO;

namespace DataAccess.Repositories
{
    public interface ISchedualRepositorys
    {
        Task<bool> CreateSlotClass(List<Schedual> listSlotClasses);
        Task<List<Schedual>> GenerateSchedule(List<ViewSlotdateDTO> classSlotDates, int slotNumber, DateTime startDate, int classId);
        Task<List<ViewSchedualDTO>> ViewSchedualByMonth(string month, string centerId);
        Task<List<ViewSchedualDTO>> ViewClassSchedual(string classId);
        Task<List<ViewSchedualDTO>> ViewTeacherClassSchedual(string teacherId);
        Task<List<ViewSchedualDTO>> ViewStudentClassSchedual(string studentId);
        Task<(bool, ViewSchedualDTO?)> GetScheduleByScheduleId(int? scheduleId);
        Task<(bool, List<string>?)> UpdateSchedule(string scheduleId, string slotdate, string dateChange, string roomId);
    }
}
