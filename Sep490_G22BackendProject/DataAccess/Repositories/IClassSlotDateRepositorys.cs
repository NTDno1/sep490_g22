﻿using BusinessObject.DTOs.ClassSlotDateDTO;
using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.DTOs.SlotDateDTO;
using BusinessObject.DTOs.SQADTO;
using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IClassSlotDateRepositorys
    {
        Task<(bool success, List<string> message)> CreateClassSlotDate(AddClassSlotDateDTO listAddClassSlotDateDTO);
        Task<List<ViewClassSlotDateDTO>> ViewListClassSlotDate();
        Task<(bool, List<string>)> DeleteClassSlotDate(string id);
        Task<List<ViewClassSlotDateDTO>> ViewListClassSlotDateByClass(string id); 

    }
}
