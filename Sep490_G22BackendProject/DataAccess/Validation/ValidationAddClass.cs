﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using BusinessObject.Models;
using BusinessObject.DTOs.SlotDateDTO;

namespace DataAccess.Validation
{
    public class ValidationAddClass
    {
        public string ClassSlotDateExit(ClassSlotDate classSlotDate)
        {
            return $"Item already exists in the database: ClassId: {classSlotDate.ClassId}, SlotId: {classSlotDate.SlotId}, RoomId: {classSlotDate.RoomId}"; 

        }
        public List<ViewSlotdateDTO> SortClassSlots(List<ViewSlotdateDTO> slots)
        {
            var orderedClassSlotDates = slots.OrderBy(slot =>
            {
                var daysOrder = new Dictionary<string, int> { { "Mon", 1 }, { "Tue", 2 }, { "Wed", 3 }, { "Thu", 4 }, { "Fri", 5 }, { "Sat", 6 }, { "Sun", 7 } };
                return daysOrder[slot.Day] * 10000 + slot.TimeStart.TotalSeconds;
            }).ToList();

            return orderedClassSlotDates;
        }

    }
}
