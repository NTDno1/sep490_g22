﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using BusinessObject.Models;

namespace DataAccess.Validation
{
    public class HashPassword
    {
        private readonly string _key;
        public HashPassword(IConfiguration configuration)
        {
            _key = configuration.GetSection("SecretsPassWord")["Key"];
        }
        public string HashPass(string password)
        {
            SHA256 hash = SHA256.Create();

            var passwordBytes = Encoding.Default.GetBytes($"{password}{_key}");

            var hashedpassword = hash.ComputeHash(passwordBytes);

            return Convert.ToHexString(hashedpassword);

        }
    }
}
