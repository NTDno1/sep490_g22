﻿using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.SQADTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddFaqValidator : AbstractValidator<AddFaqDTOs>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddFaqValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");
            RuleFor(dto => dto.Title)
                .NotNull().WithMessage("Title cannot be null.")
                .NotEmpty().WithMessage("Title cannot be empty.");

            RuleFor(dto => dto.Desc)
                .NotNull().WithMessage("Desc cannot be null.")
                .NotEmpty().WithMessage("Desc cannot be empty.");
        }
    }
}
