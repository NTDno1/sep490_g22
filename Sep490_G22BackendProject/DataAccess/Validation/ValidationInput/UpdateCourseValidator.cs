﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateCourseValidator : AbstractValidator<UpdateCourseDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateCourseValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
           .NotNull().WithMessage("UpdateCourseDTO cannot be null.");

            RuleFor(dto => dto.Id)
                .NotNull().WithMessage("Id cannot be null.")
                .NotEmpty().WithMessage("Id cannot be empty.")
                .GreaterThan(0).WithMessage("ID Khóa Học Không Được Để Trống");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("Tên Khóa Học Không Được Để Trống")
                .NotEmpty().WithMessage("Tên Khóa Học Không Được Để Trống");

            //RuleFor(dto => dto.Code)
            //    .NotNull().WithMessage("Mã Khóa Học Không Được Để Trống")
            //    .NotEmpty().WithMessage("Mã Khóa Học Không Được Để Trống")
            //    .Must(code => _checkValidationInDb.DoesNotContainSpecialCharacters(code)).WithMessage("Mã Code Không Chứa Ký Tự Đặc Biệt Hoặc Khoảng Trắng")
            //    .Must(code => _checkValidationInDb.BeUniqueCourseCode(code.Replace(" ", ""))).WithMessage("Mã Course Đã Tồn Tại")
            //    .Must(code => _checkValidationInDb.LengCoded(code.Replace(" ", ""))).WithMessage("Độ Dài Của Mã Khóa Học Không Được Lớn Hơn 5");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Mô Tả Không Được Để Trống")
                .NotEmpty().WithMessage("Mô Tả Không Được Để Trống");

            RuleFor(dto => dto.Price)
                .NotNull().WithMessage("Giá Không Được Để Trống")
                .NotEmpty().WithMessage("Giá Không Được Phép Null.")
                .GreaterThan(1000).WithMessage("Giá Cần Phải Lớn Hơn 1000");

            RuleFor(dto => dto.SyllabusId)
                .NotNull().WithMessage("Bạn Chưa Chọn Giáo Trình")
                .NotEmpty().WithMessage("Giáo Trình Không Được Để Trống")
                .GreaterThan(0).WithMessage("Giáo Trình Không Được Để Trống");
        }
    }
}
