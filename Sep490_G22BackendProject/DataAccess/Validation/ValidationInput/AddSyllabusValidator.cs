﻿using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.SyllabusDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddSyllabusValidator : AbstractValidator<AddSyllabusDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddSyllabusValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.SyllabusName)
                .NotNull().WithMessage("SyllabusName cannot be null.")
                .NotEmpty().WithMessage("SyllabusName cannot be empty.");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Description cannot be null.")
                .NotEmpty().WithMessage("Description cannot be empty.");
        }
    }
}
