﻿using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.CourseDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddClassValidator : AbstractValidator<AddClassDTOs>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddClassValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("Tên Không Được Để Trống.")
                .NotEmpty().WithMessage("Tên Không Được Để Trống.");

            RuleFor(dto => dto.Code )
                .NotNull().WithMessage("Mã Code Không Được Để Trống.")
                .NotEmpty().WithMessage("Mã Code Không Được Để Trống.")
                //.Must(code => _checkValidationInDb.BeUniqueClassCodeAsync(code.Replace(" ", ""), null)).WithMessage("Mã Lớp Học Đã Tồn Tại")
                .Must(code => _checkValidationInDb.DoesNotContainSpecialCharacters(code)).WithMessage("Mã Code Không Chứa Ký Tự Đặc Biệt Hoặc Khoảng Trắng")
                .Must(code => _checkValidationInDb.LengCoded(code.Replace(" ", ""))).WithMessage("Độ Dài Của Mã Học Không Được Lớn Hơn 5");

            RuleFor(dto => dto.CourceId)
            .NotNull().WithMessage("CKhóa Học Không Được Để Trống.")
            .NotEmpty().WithMessage("Khóa Học Không Được Để Trống.")
            .GreaterThan(0).WithMessage("Khóa Học Không Được Để Trống");

            RuleFor(dto => dto.TeacherId)
                .NotEmpty().WithMessage("TeacherId không được để trống.")
                .NotEqual(Guid.Empty).WithMessage("TeacherId không được là Guid.Empty.");

            RuleFor(dto => dto.StartDate)
                .NotNull().WithMessage("Ngày Bắt Đầu Không Được Để Trống.")
                .NotEmpty().WithMessage("Ngày Bắt Đầu Không Được Để Trống.")
                .Must(startDate => startDate > DateTime.Today).WithMessage("Ngày Bắt Đầu Không Được Nhỏ Hơn Ngày Hôm Nay.");
        }
    }
}
