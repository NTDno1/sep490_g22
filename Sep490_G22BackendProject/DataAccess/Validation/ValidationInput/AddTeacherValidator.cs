﻿using BusinessObject.DTOs.ClassDTO;
using BusinessObject.DTOs.TeacherDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddTeacherValidator: AbstractValidator<AddTeacherDTOs>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddTeacherValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.FirstName)
           .NotNull()
           .WithMessage("Không Được Để Trống Tên.")
           .NotEmpty()
           .WithMessage("Không Được Để Trống Tên.");

            RuleFor(dto => dto.UserName)
                .NotNull()
                .WithMessage("UserName Không Được Để Trống.")
                .NotEmpty()
                .WithMessage("UserName Không Được Để Trống.")
                .Must(code => _checkValidationInDb.BeUniqueUserNameTeacher(code.Replace(" ", ""), null)).WithMessage("Tên Người Dùng Đã Tồn Tại Hoặc Đang Được Sử Dụng ở Một Trung Tâm Khác");

            RuleFor(dto => dto.Email)
                .NotNull()
                .WithMessage("UserName cannot be null.")
                .NotEmpty()
                .WithMessage("UserName cannot be empty.")
                .Must(code => _checkValidationInDb.BeUniqueEmailTeacher(code.Replace(" ", ""), null)).WithMessage("Email Đã Tồn Tại Hoặc Đang Được Sử Dụng ở Một Trung Tâm Khác")
                .Must(code => _checkValidationInDb.IsValidEmail(code.Replace(" ", ""))).WithMessage("Bạn Cần Nhập Đúng Định Dạng Email");
        }
    }
}
