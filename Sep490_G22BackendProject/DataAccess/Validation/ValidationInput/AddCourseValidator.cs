﻿using BusinessObject.DTOs.CourseDTO;
using BusinessObject.DTOs.EmployeeDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Org.BouncyCastle.Crypto.Engines.SM2Engine;

namespace DataAccess.Validation.ValidationInput
{
    public class AddCourseValidator : AbstractValidator<AddCourseDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddCourseValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("Name cannot be null.")
                .NotEmpty().WithMessage("Name cannot be empty.");

            RuleFor(dto => dto.Code)
                .NotNull().WithMessage("Code cannot be null.") 
                .NotEmpty().WithMessage("Code cannot be empty.")
                .Must(code => _checkValidationInDb.DoesNotContainSpecialCharacters(code)).WithMessage("Mã Code Không Chứa Ký Tự Đặc Biệt Hoặc Khoảng Trắng")
                //.Must(code => _checkValidationInDb.BeUniqueCourseCode(code.Replace(" ", ""))).WithMessage("Mã Course Đã Tồn Tại")
                .Must(code => _checkValidationInDb.LengCoded(code.Replace(" ", ""))).WithMessage("Độ Dài Của Mã Khóa Học Không Được Lớn Hơn 5");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Description cannot be null.")
                .NotEmpty().WithMessage("Description cannot be empty.");

            RuleFor(dto => dto.Price)
                .NotNull().WithMessage("Giá Không Được Để Trống")
                .NotEmpty().WithMessage("Giá Không Được Phép Null.")
                .GreaterThan(1000).WithMessage("Giá Cần Phải Lớn Hơn 1000");

            RuleFor(dto => dto.SyllabusId)
                .NotNull().WithMessage("Bạn Chưa Chọn Giáo Trình")
                .NotEmpty().WithMessage("Giáo Trình Không Được Để Trống")
                .GreaterThan(0).WithMessage("Giáo Trình Không Được Để Trống");
        }
    }
}
