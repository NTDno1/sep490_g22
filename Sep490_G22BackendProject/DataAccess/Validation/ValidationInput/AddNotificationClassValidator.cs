﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.NotificationClass;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddNotificationClassValidator : AbstractValidator<AddNotificationClass>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddNotificationClassValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Title)
                .NotNull().WithMessage("Title cannot be null.")
                .NotEmpty().WithMessage("Title cannot be empty.");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Description cannot be null.")
                .NotEmpty().WithMessage("Description cannot be empty.");
            
            RuleFor(dto => dto.ClassId)
               .NotNull().WithMessage("ClassId cannot be null.")
               .NotEmpty().WithMessage("ClassId cannot be empty.");
        }
    }
}
