﻿using BusinessObject.DTOs.TeacherDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateTeacherValidator : AbstractValidator<UpdateProfileTeacherDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateTeacherValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.FirstName)
          .NotNull()
          .WithMessage("Không Được Để Trống Tên.")
          .NotEmpty()
          .WithMessage("Không Được Để Trống Tên.");

            RuleFor(dto => dto.Email)
                .NotNull()
                .WithMessage("UserName cannot be null.")
                .NotEmpty()
                .WithMessage("UserName cannot be empty.")
                .Must((dto, code) => _checkValidationInDb.BeUniqueEmailTeacher(code.Trim(), dto.Id.ToString())).WithMessage("Email đã tồn tại")
                .Must(code => _checkValidationInDb.IsValidEmail(code.Replace(" ", ""))).WithMessage("Bạn Cần Nhập Đúng Định Dạng Email");
        }
    }
}
 