﻿using BusinessObject.DTOs.Student;
using BusinessObject.DTOs.TeacherDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateStudentValidator : AbstractValidator<UpdateStudentDTOs>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateStudentValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.FirstName)
           .NotNull()
           .WithMessage("Không Được Để Trống Tên.")
           .NotEmpty()
           .WithMessage("Không Được Để Trống Tên.");

            RuleFor(dto => dto.Email)
                .NotNull()
                .WithMessage("Email Không Được Phép Để Trống")
                .NotEmpty()
                .WithMessage("Email Không Được Phép Để Trống")
                .Must((dto, code) => _checkValidationInDb.BeUniqueEmailStudent(code.Trim(), dto.Id.ToString())).WithMessage("Email Đã Tồn Tại")
                .Must(code => _checkValidationInDb.IsValidEmail(code.Replace(" ", ""))).WithMessage("Bạn Cần Nhập Đúng Định Dạng Email");
        }
    }
}
