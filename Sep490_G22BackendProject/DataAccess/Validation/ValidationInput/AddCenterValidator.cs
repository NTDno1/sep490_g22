﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.SyllabusDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddCenterValidator : AbstractValidator<AddNewCenterDTOs>
    {

        readonly CheckValidationInDb _checkValidationInDb;
        public AddCenterValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb= checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("SyllabusName cannot be null.")
                .NotEmpty().WithMessage("SyllabusName cannot be empty.");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Description cannot be null.")
                .NotEmpty().WithMessage("Description cannot be empty.");

            RuleFor(dto => dto.CodeCenter)
                .NotNull().WithMessage("Status cannot be null.")
                .Must(code => _checkValidationInDb.BeUniqueCenterCode(code)).WithMessage("Mã Trung Tâm Đã Tồn Tại")
                .NotEmpty().WithMessage("Status cannot be empty.");

            RuleFor(dto => dto.Adress)
                .NotNull().WithMessage("Status cannot be null.")
                .NotEmpty().WithMessage("Status cannot be empty.");
        }
    }
}
