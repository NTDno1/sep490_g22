﻿using BusinessObject.DTOs.EmployeeDTO;
using BusinessObject.Models;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class AddEmployeeValidator : AbstractValidator<AddEmployeeDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public AddEmployeeValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto).NotNull().WithMessage("AddEmployeeDTO cannot be null.");

            RuleFor(dto => dto.Name)
                .NotNull()
                .WithMessage("Name cannot be null.")
                .NotEmpty()
                .WithMessage("Name cannot be empty.");

            RuleFor(dto => dto.UserName)
                .NotNull()
                .WithMessage("UserName cannot be null.")
                .NotEmpty()
                .WithMessage("UserName cannot be empty.")
                .Must(code => _checkValidationInDb.BeUniqueUserNameEmployee(code.Replace(" ", ""), null)).WithMessage("Tên Người Dùng Đã Tồn Tại");

            RuleFor(dto => dto.Email)
                .NotNull()
                .WithMessage("Email cannot be null.")
                .NotEmpty()
                .WithMessage("Email cannot be empty.")
                .Must(code => _checkValidationInDb.BeUniqueEmailEmployee(code.Replace(" ", ""), null)).WithMessage("Email Dùng Đã Tồn Tại")
                .Must(code => _checkValidationInDb.IsValidEmail(code.Replace(" ", ""))).WithMessage("Bạn Cần Nhập Đúng Định Dạng Email");
        }
    }
}
