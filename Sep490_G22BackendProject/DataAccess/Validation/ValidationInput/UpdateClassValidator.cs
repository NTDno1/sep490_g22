﻿using BusinessObject.DTOs.ClassDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateClassValidator : AbstractValidator<UpdateClassDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateClassValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Id)
               .NotNull().WithMessage("Id cannot be null.")
               .NotEmpty().WithMessage("Id cannot be empty.");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("ClassName cannot be null.")
                .NotEmpty().WithMessage("ClassName cannot be empty.")
                .Must(code => _checkValidationInDb.ContainsLetterAndDigitOnly(code.Trim())).WithMessage("ClassName must contain both letters and numbers and should not contain special characters.")
                .Must(code => _checkValidationInDb.DoesNotContainSpecialCharacters(code.Trim())).WithMessage("ClassName cannot contain special characters.");

            //RuleFor(dto => dto.Code)
            //    .NotNull().WithMessage("ClassCode cannot be null.")
            //    .NotEmpty().WithMessage("ClassCode cannot be empty.")
            //    .Must((dto, code) => _checkValidationInDb.BeUniqueClassCodeAsync(code.Trim(), dto.Id.ToString())).WithMessage("Mã Lớp Học Đã Tồn Tại")
            //    .Must(code => _checkValidationInDb.ContainsLetterAndDigitOnly(code.Trim())).WithMessage("ClassCode must contain both letters and numbers and should not contain special characters.")
            //    .Must(code => _checkValidationInDb.DoesNotContainSpecialCharacters(code.Trim())).WithMessage("ClassCode cannot contain special characters.");

            RuleFor(dto => dto.TeacherId)
                .NotNull().WithMessage("TeacherId cannot be null.")
                .NotEmpty().WithMessage("TeacherId cannot be empty.");

            RuleFor(dto => dto.SlotNumber)
                .NotNull().WithMessage("Name cannot be null.")
                .GreaterThan(0).WithMessage("SlotNumber must be greater than 0.")
                .NotEmpty().WithMessage("Name cannot be empty.");

            RuleFor(dto => dto.StartDate)
                .NotNull().WithMessage("StartDate cannot be null.")
                .NotEmpty().WithMessage("StartDate cannot be empty.")
                .Must(startDate => startDate > DateTime.Today).WithMessage("StartDate must be greater than today.");
        }
    }
}
