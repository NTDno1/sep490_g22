﻿using BusinessObject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class CheckValidationInDb
    {
        readonly sep490_g22Context _context;
        public CheckValidationInDb(sep490_g22Context context)
        {
            _context= context;
        }
        public bool BeUniqueUserNameEmployee(string userName, string? id)
        {
            if(id!= null)
            {
                var getUser = _context.Employees.FirstOrDefault(u => u.Id.ToString() == id);
                var check = _context.Employees.Where(u => u.UserName.Replace(" ", "") == userName && getUser.Id.ToString() == id).FirstOrDefault();
                if (check != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var check = _context.Employees.Where(u => u.UserName.Replace(" ", "") == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }

        }
        public bool BeUniqueUserNameStudent(string userName, string id)
        {
            if(id!= null)
            {
                var getUser = _context.StudentProfiles.FirstOrDefault(u => u.Id.ToString() == id);
                var check = _context.StudentProfiles.Where(u => u.UserName.Replace(" ", "") == userName && getUser.Id.ToString() == id).FirstOrDefault();
                if (check != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var check = _context.StudentProfiles.Where(u => u.UserName.Replace(" ", "") == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }
        public bool BeUniqueUserNameTeacher(string userName, string id)
        {
            if(id!= null)
            {
                var getUser = _context.TeacherProfiles.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                var check = _context.TeacherProfiles.Where(u => u.UserName.Replace(" ", "") == userName && getUser.Id.ToString() == id).FirstOrDefault();
                if (check != null)
                {
                    return false;
                }
                return true;    
            }
            else
            {
                var check = _context.TeacherProfiles.Where(u => u.UserName.Replace(" ", "") == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }

        }
        public bool BeUniqueEmailTeacher(string userName, string? id)
        {
            if(id!= null)
            {
                // var getUser = _context.TeacherProfiles.FirstOrDefault(u => u.Id.ToString() == id);
                var checks = _context.TeacherProfiles.Where(u => u.Email.Replace(" ", "") == userName && u.Id.ToString() != id).FirstOrDefault();
                if (checks != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var check = _context.TeacherProfiles.Where(u => u.Email.Replace(" ", "") == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }
        public bool BeUniqueEmailStudent(string userName, string? id)
        {
            if(id!= null)
            {
                // var getUser = _context.StudentProfiles.FirstOrDefault(u => u.Id.ToString() == id);
                var check = _context.StudentProfiles.Where(u => u.Email.Replace(" ", "") == userName && u.Id.ToString() != id).FirstOrDefault();
                if (check != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var check = _context.StudentProfiles.Where(u => u.Email.Replace(" ", "") == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }
        }
        public bool BeUniqueEmailEmployee(string userName, string id)
        {
            if(id!= null)
            {
                var getUser = _context.Employees.FirstOrDefault(u => u.Id.ToString() == id);
                var check = _context.Employees.Where(u => u.Email == userName).FirstOrDefault();
                if (check != null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var check = _context.Employees.Where(u => u.Email == userName).ToList();
                if (check != null && check.Count > 0)
                {
                    return false;
                }
                return true;
            }

        }
        public bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            try
            {
                // Kiểm tra định dạng email bằng biểu thức chính quy
                var emailRegex = new Regex(@"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");
                return emailRegex.IsMatch(email);
            }
            catch (Exception)
            {
                return false; // Nếu có lỗi trong quá trình kiểm tra biểu thức chính quy, coi như email không đúng định dạng
            }
        }
        public bool BeUniqueEmployeeId(string employeeId)
        {
            return _context.Employees.Any(r => r.Id.ToString().ToLower() == employeeId.ToLower());
        }
        public bool BeUniqueCenterCode(string centerCode)
        {
            return !_context.Centers.Any(r => r.CodeCenter == centerCode);
        }   
        public bool BeUniqueRoleId(string id)
        {
            return _context.Roles.Any(r => r.Id.ToString().ToLower() == id.ToLower());
        }
        public bool BeUniqueCenterId(string id)
        {
            return _context.Centers.Any(r => r.Id.ToString().ToLower() == id.ToLower());
        }

        public bool BeUniqueName(string name)
        {
            return _context.Courses.Any(r => r.Name == name);
        }

        public bool BeUniqueCourseCode(string code, string centerId)
        {
            Console.WriteLine("ĐÃ vàđe");
            return !_context.Courses.Any(r => r.Code.ToLower().Replace(" ", "") == code.ToLower().Trim() && r.CenterId.ToString() == centerId);
        }
        public bool LengCoded(string code)
        {
            if (code.Length > 5)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool BeUniqueClassCodeAsync(string code, string? id, string centerId)
        {
            if(id!= null)
            {
                var getUser = _context.Classes.FirstOrDefaultAsync(u => u.Id.ToString() == id);
                bool check = _context.Classes.Any(r => r.Code.ToLower().Replace(" ", "") == code.ToLower() && r.CenterId.ToString() == centerId);
                return !check;
            }
            else
            {
                bool check = _context.Classes.Any(r => r.Code.ToLower().Replace(" ", "") == code.ToLower() && r.CenterId.ToString() == centerId);
                return !check;
            }
        }
        public bool ContainsLetterAndDigitOnly(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            bool hasLetter = false;
            bool hasDigit = false;

            foreach (char c in value)
            {
                if (char.IsLetter(c))
                {
                    hasLetter = true;
                }
                else if (char.IsDigit(c))
                {
                    hasDigit = true;
                }

                // Nếu có chữ cái và số, thoát khỏi vòng lặp
                if (hasLetter && hasDigit)
                {
                    return true;
                }
            }

            return false;
        }
        public bool DoesNotContainSpecialCharacters(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            // Kiểm tra xem chuỗi có chứa bất kỳ ký tự đặc biệt nào không
            return !Regex.IsMatch(value, @"[!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]");
        }
        public bool IsAlphaNumeric(string value)
        {
            return !string.IsNullOrEmpty(value) && value.All(char.IsLetterOrDigit);
        }
        public bool IsNumeric(string value)
        {
            return int.TryParse(value, out _);
        }
    }
}
