﻿using BusinessObject.DTOs.SQADTO;
using BusinessObject.DTOs.SyllabusDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateSyllabusValidator : AbstractValidator<UpdateSyllabusDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateSyllabusValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddCourseDTO cannot be null.");

            RuleFor(dto => dto.Id)
               .NotNull().WithMessage("Id cannot be null.")
               .NotEmpty().WithMessage("Id cannot be empty.");

            RuleFor(dto => dto.SyllabusName)
                .NotNull().WithMessage("SyllabusName cannot be null.")
                .NotEmpty().WithMessage("SyllabusName cannot be empty.");

            RuleFor(dto => dto.Description)
                .NotNull().WithMessage("Description cannot be null.")
                .NotEmpty().WithMessage("Description cannot be empty.");

            RuleFor(dto => dto.Status)
                .NotNull().WithMessage("Status cannot be null.")
                .NotEmpty().WithMessage("Status cannot be empty.");
        }
    }
}
