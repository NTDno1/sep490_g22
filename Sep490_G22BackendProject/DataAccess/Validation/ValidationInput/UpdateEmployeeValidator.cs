﻿using BusinessObject.DTOs.EmployeeDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation.ValidationInput
{
    public class UpdateEmployeeValidator : AbstractValidator<UpdateEmployeeDTO>
    {
        readonly CheckValidationInDb _checkValidationInDb;
        public UpdateEmployeeValidator(CheckValidationInDb checkValidationInDb)
        {
            _checkValidationInDb = checkValidationInDb;
            RuleFor(dto => dto)
            .NotNull().WithMessage("AddEmployeeDTO cannot be null.");

            RuleFor(dto => dto.Name)
                .NotNull().WithMessage("Name cannot be null.")
                .NotEmpty().WithMessage("Name cannot be empty.");
            RuleFor(dto => dto.Adress)
               .NotNull().WithMessage("Adress cannot be null.")
               .NotEmpty().WithMessage("Adress cannot be empty.");
            RuleFor(dto => dto.Email)
               .NotNull().WithMessage("Email cannot be null.")
               .NotEmpty().WithMessage("Email cannot be empty.");
            RuleFor(dto => dto.PhoneNumber)
               .NotNull().WithMessage("PhoneNumber cannot be null.")
               .NotEmpty().WithMessage("PhoneNumber cannot be empty.");
            RuleFor(dto => dto.Dob)
               .NotNull().WithMessage("Dob cannot be null.")
               .NotEmpty().WithMessage("Dob cannot be empty.");
            RuleFor(dto => dto.Id.ToString())
               .NotNull().WithMessage("Id cannot be null.")
               .NotEmpty().WithMessage("Id cannot be empty.")
               .Must(_checkValidationInDb.BeUniqueEmployeeId)
               .WithMessage("Id is not exists in the database.");
        }
    }
}
