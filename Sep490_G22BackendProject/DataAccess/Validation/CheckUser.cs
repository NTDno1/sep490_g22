﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Validation
{
    public class CheckUser
    {
        private readonly sep490_g22Context _context;
        public CheckUser(sep490_g22Context context)
        {
            _context = context;
        }
        public bool CheckAccountAdmin(string id)
        {
            var user = _context.Accounts.FirstOrDefault(u => u.Id.ToString() == id);
            if (user != null)
            {
                return true;
            }
            return false;
        }
        public bool CheckAccountEmployee(string id)
        {
            var user = _context.Employees.FirstOrDefault(u => u.Id.ToString() == id);
            if (user != null)
            {
                return true;
            }
            return false;
        }
        public string GetDayOfWeekString(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return "Mon";
                case DayOfWeek.Tuesday:
                    return "Tue";
                case DayOfWeek.Wednesday:
                    return "Wed";
                case DayOfWeek.Thursday:
                    return "Thu";
                case DayOfWeek.Friday:
                    return "Fri";
                case DayOfWeek.Saturday:
                    return "Sat";
                case DayOfWeek.Sunday:
                    return "Sun";
                default:
                    return string.Empty;
            }
        }
    }
}

