import React from 'react'

export default function FooterAdmin() {
  return (
    <div style={{backgroundColor:'lightgray'}} class="py-4 footer-admin">
    <div class="container text-center">
      <p class="text-muted mb-0 py-2">© 2023 PI Online Center.</p>
    </div>
  </div>
  )
}
  