import React, { useEffect, useState } from "react";
import { Box, IconButton, InputBase, Typography, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import SearchIcon from "@mui/icons-material/Search";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link } from "react-router-dom";

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
const RoomAdmin = () => {
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);

  useEffect(() => {
    fetch("https://localhost:7241/api/SAQ/ListSAQ").then((res) => res.json())
    .then((data) => {
        // setFAQ(data.result)
      console.log(data)
});
},[])

  const handleRemoveClick = async () => {
    if (window.confirm("Are you sure you want to delete selected blogs?")) {
      await fetch(`https://localhost:7241/api/Blog`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(selectedBlogCodes.map(String)),
      }).then((response) => {
        if (response.ok) {
          toast.success('Blogs deleted successfully');
          window.location.reload();
        } else {
          console.error('Failed to delete blogs');
        }
      });
    }
  };

  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };

  const handleRemoveSelected = () => {
    if (selectedBlogCodes.length > 0) {
      console.log("Button clicked!");
      handleRemoveClick(selectedBlogCodes);
    } else {
      console.log("No blogs selected for removal");
    }
  };

  const columns = [
    { field: "id", headerName: "ID" },
    {
      field: "title",
      headerName: "Title",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "desc",
      headerName: "Descreption",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "dateCreate",
      headerName: "DateCreate",
      flex: 1,
    },
    {
      field: "dateUpdate",
      headerName: "DateUpdate",
      flex: 1,
    },
   
    {
      headerName: "Edit",
      flex: 1,
      renderCell: (params) => <a href={`/updateblog/`}>
        <UilEdit />
      </a>,

    },
    {
      field: "Remove",
      headerName: "Remove",
      flex: 1,
      renderCell: (params) => (
        <UilTimesCircle
          style={{ cursor: 'pointer' }}
          onClick={() => handleRemoveClick([params.row.blogCode])}
        />
      ),
    },
  ];

  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <div style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" style={{ minWidth: '170.3vh' ,height:'100vh' }}>
                  <Topbar setIsSidebar={setIsSidebar} />
    <Box m="20px">
      <Header title="Blog" subtitle="Managing the Blog" />
      <Link to='./addBlog' className="btn btn-success m-btn">
        <i className="fa fa-plus"></i> Add Blog
      </Link>
      <button className="btn btn-danger m-btn" onClick={handleRemoveSelected}>
        <i className="fa fa-trash"></i> Remove
      </button>

      <Box
        width="15%"
        marginTop="20px"
        backgroundColor={colors.primary[400]}
        borderRadius="3px"
      >
        <InputBase sx={{ ml: 2, flex: 1 }} placeholder="Search" />
        <IconButton type="button" sx={{ p: 1 }}>
          <SearchIcon />
        </IconButton>
      </Box>
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
        }}
      >
        <DataGrid checkboxSelection rows={""} columns={columns} onRowSelectionModelChange={handleSelectionChange} />
      </Box>
    </Box>
    
  </main>
              </div>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  );
};


export default RoomAdmin;
