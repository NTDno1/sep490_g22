import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import {  useNavigate } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";

import { addNewSyllabus } from '../../../api/apiClient/Courses/CoursesAPI';
import Topbar from '../../scenes/global/Topbar';
import FooterAdmin from '../../FooterAdmin';
import Header from '../Header';
import { ToastContainer, toast } from 'react-toastify';

const initialFieldValues = {

  syllabusName: '',
  description: '',
  timeAllocation: '',
  studentTasks: '',
  courseObjectives: '',
  exam: '',
  subjectCode: '',
  contact: '',
  courseLearningOutcome: '',
  mainTopics: '',
  requiredKnowledge: '',
  linkBook: '',
  linkDocument: '',
  status: 1,
}

export default function AddSyllabus() {
  const [values, setValues] = useState(initialFieldValues);
  
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);
  
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');


  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);

    
 
  const addOrEdit = async(formData) => {
   await addNewSyllabus(formData)
    .then(data => {
      toast.success("Thêm thành công")
      setTimeout(() => {
      navigate('/Syllabusadmin')
      },2000)
    })
    .catch(error => {
      // Xử lý khi gửi thất bại
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    });
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
   
    formData.append('syllabusName', values.syllabusName)
    formData.append('description', values.description)
    formData.append('timeAllocation', values.timeAllocation)
    formData.append('studentTasks', values.studentTasks)
    formData.append('courseObjectives', values.courseObjectives)
    formData.append('exam', values.exam)
    formData.append('subjectCode', values.subjectCode)
    formData.append('contact', values.contact)
    formData.append('courseLearningOutcome', values.courseLearningOutcome)
    formData.append('mainTopics', values.mainTopics)
    formData.append('requiredKnowledge', values.requiredKnowledge)
    formData.append('linkBook', values.linkBook)
    formData.append('linkDocument', values.linkDocument)
    formData.append('status', values.status)

    addOrEdit(formData)
  }


  const handleInputChange = e => {
    const { name, value } = e.target; 
    setValues({
      ...values,
      [name]: value
    })
  }
  
  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                <Topbar setIsSidebar={setIsSidebar} />
    <div className="container">
    <Header title="Giáo Trình" subtitle="Thêm giáo trình" />
  <ToastContainer/>
      <div className="row">
        <div className="col-12">
          {/* Page title */}
         

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>

    <div className="row mb-5 gx-12">
     {/* Contact detail */}
     <div className="col-xxl-12 mb-5 mb-xxl-0">
    <div className="bg-secondary-soft px-4 py-5 rounded">
      <div className="row g-3">
        {/* First Name */}
        <div className="col-md-6">
          <label className="form-label">Trung tâm</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            value={centername} disabled

          />
        </div>
        {/* Last name */}


        {/* Phone number */}

        {/* Mobile number */}
        <div className="col-md-6">
          <label className="form-label">Tên giáo trình <span className='required-field'>*</span></label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='syllabusName'
            value={values.syllabusName}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Mô tả</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='description'
            value={values.description}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Phân bổ thời gian</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='timeAllocation'
            value={values.timeAllocation}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Nhiệm vụ học viên</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='studentTasks'
            value={values.studentTasks}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Nội dung bài học</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='courseObjectives'
            value={values.courseObjectives}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Bài thi</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='exam'
            value={values.exam}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Code</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='subjectCode'
            value={values.subjectCode}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Liên hệ</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='contact'
            value={values.contact}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Kiến thức cần đạt</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='courseLearningOutcome'
            value={values.courseLearningOutcome}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Chủ đề</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='mainTopics'
            value={values.mainTopics}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Kiến thức yêu cầu</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='requiredKnowledge'
            value={values.requiredKnowledge}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Link Sách</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='linkBook'
            value={values.linkBook}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="col-md-6">
          <label className="form-label">Link Tài liệu</label>
          <input
            type="text"
            className="form-control"
            placeholder=""
            name='linkDocument'
            value={values.linkDocument}
            onChange={handleInputChange}
            required
          />
        </div>
      </div>{" "}

    </div>
  </div>

</div>{" "}


<div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
<a className='backtolist' href='/Syllabusadmin'> {'<< Quay về danh sách'}</a>
<button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
</div>
</form>
          {/* Form END */}
        </div>


      </div>
    </div>
   
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
