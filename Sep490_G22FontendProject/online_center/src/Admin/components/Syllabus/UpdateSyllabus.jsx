import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";

import { SyllabusDetail, UpdateInfoSyllabus } from '../../../api/apiClient/Courses/CoursesAPI';
import { convertDateFormat } from '../../../constants/constant';
import { ToastContainer, toast } from 'react-toastify';
import Topbar from '../../scenes/global/Topbar';
import FooterAdmin from '../../FooterAdmin';
import Header from '../Header';
const initialFieldValues = {
  id: 0,
  syllabusName: '',
  description: '',
  timeAllocation: '',
  studentTasks: '',
  courseObjectives: '',
  exam: '',
  subjectCode: '',
  contact: '',
  courseLearningOutcome: '',
  mainTopics: '',
  requiredKnowledge: '',
  linkBook: '',
  linkDocument: '',
  status: 1,
}

export default function UpdateSyllabus() {
  const [values, setValues] = useState(initialFieldValues);
  const { code } = useParams();
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {

    getSyllabus();

  }, [])
  const getSyllabus = async() => {
    try{
   const data = await SyllabusDetail(code)
        const result = data.result;
        setValues({
          id: result.id,
          syllabusName: result.syllabusName,
          description: result.description,
          timeAllocation: result.timeAllocation,
          studentTasks: result.studentTasks,
          courseObjectives: result.courseObjectives,
          exam: result.exam,
          subjectCode: result.subjectCode,
          contact: result.contact,
          courseLearningOutcome: result.courseLearningOutcome,
          mainTopics: result.mainTopics,
          requiredKnowledge: result.requiredKnowledge,
          linkBook: result.linkBook,
          linkDocument: result.linkDocument,
          status : 1,
      });
    }catch(error){
      
    }
  }
  
 
  const addOrEdit = async(formData) => {
    try {
      const response = await UpdateInfoSyllabus(formData);
      toast.success("Cập nhật thành công");
      window.location.reload();
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('id', code)
    formData.append('syllabusName', values.syllabusName)
    formData.append('description', values.description)
    formData.append('timeAllocation', values.timeAllocation)
    formData.append('studentTasks', values.studentTasks)
    formData.append('courseObjectives', values.courseObjectives)
    formData.append('exam', values.exam)
    formData.append('subjectCode', values.subjectCode)
    formData.append('contact', values.contact)
    formData.append('courseLearningOutcome', values.courseLearningOutcome)
    formData.append('mainTopics', values.mainTopics)
    formData.append('requiredKnowledge', values.requiredKnowledge)
    formData.append('linkBook', values.linkBook)
    formData.append('linkDocument', values.linkDocument)
    formData.append('status', values.status)
    addOrEdit(formData)
  }

  const handleInputChange = e => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value
    })
  }

  return (
    <>
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
             
                <ToastContainer/>
                <main className="content">
                <Topbar setIsSidebar={setIsSidebar} />
    <div style={{height:'90vh',overflow:'auto'}} className="full-width-container">
    <div   className="container">
    <Header title="Giáo Trình" subtitle="Cập nhật giáo trình" />

      <div className="row">
        <div className="col-12">
          {/* Page title */}

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>

            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                    {/* First Name */}
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={centername} disabled

                      />
                    </div>
                    {/* Last name */}


                    {/* Phone number */}
      
                    {/* Mobile number */}
                    <div className="col-md-12">
                      <label className="form-label">Tên giáo trình <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                      
                        name='syllabusName'
                        value={values.syllabusName}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Mô tả</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                      
                        name='description'
                        value={values.description}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Thời gian</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='timeAllocation'
                        value={values.timeAllocation}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Nhiệm vụ học viên</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='studentTasks'
                        value={values.studentTasks}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Nội dung</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='courseObjectives'
                        value={values.courseObjectives}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Bài thi</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='exam'
                        value={values.exam}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Code</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='subjectCode'
                        value={values.subjectCode}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Liên hệ</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='contact'
                        value={values.contact}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Kiến thức cần đạt</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='courseLearningOutcome'
                        value={values.courseLearningOutcome}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Chủ đề</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='mainTopics'
                        value={values.mainTopics}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Kiến thức yêu cầu</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='requiredKnowledge'
                        value={values.requiredKnowledge}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Link Sách</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='linkBook'
                        value={values.linkBook}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-md-12">
                      <label className="form-label">Link tài liệu</label>
                      <input 
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='linkDocument'
                        value={values.linkDocument}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                  </div>{" "}

                </div>
              </div>
              
            </div>{" "}


            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
            <a className='backtolist' href='/Syllabusadmin'> {'<< Quay về danh sách'}</a>
            <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
            </div>
          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
    </div>
 
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
    </>
  )
}
