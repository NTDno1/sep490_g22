import React, { useEffect, useState } from "react";
import { Box, IconButton, InputBase, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import { clean } from 'diacritic';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
 
} from "@iconscout/react-unicons";
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import { DeleteSyllabus, viewAllSyllabus } from "../../../api/apiClient/Courses/CoursesAPI";
import Topbar from "../../scenes/global/Topbar";
import FooterAdmin from "../../FooterAdmin";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
const SyllabusAdmin = () => {
  const [syllabus, setSyllabus] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchSyllabus, setSearchSyllabus] = useState("");
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  let jwttoken = Cookies.get('jwttoken');
  const apiRef = useGridApiRef();
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    
    ListSyllabus();
  
  }, [apiRef])
  const ListSyllabus = async () => {
    try{
    const data = await viewAllSyllabus();
    if(data.result.length > 0){
    setOriginalClasses(data.result)
    setSyllabus(data.result)
   
      apiRef.current.setPageSize(7);

    } else{}
  } catch(error){

  }
}

  const dataWithSyllabus = syllabus.map((item, index) => ({
    ...item,
    SyllabusID: index + 1,
  }))
  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa mục này không ?")) {
      try{
      const response = await DeleteSyllabus(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        ListSyllabus();
      } else {
        toast.error(response.errorMessages)
      }
    }catch(error){
      toast.error("Có lỗi xảy ra")
    }

    }
  };

  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };

  const handleRemoveSelected = () => {
    if (selectedBlogCodes.length > 0) {
      console.log("Button clicked!");
      handleRemoveClick(selectedBlogCodes);
    } else {
      console.log("No blogs selected for removal");
    }
  };

  const columns = [
    {
      headerName: "STT",
      field: "SyllabusID",
      flex: 0.5,
      align:'center'
    },
    {
      field: "syllabusName",
      headerName: "Tên giáo trình",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "description",
      headerName: "Mô tả",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "timeAllocation",
      headerName: "Thời gian",
      flex: 1,
    },
    {
      field: "studentTasks",
      headerName: "Nhiệm vụ học viên",
      flex: 1,
    },

    {
      headerName: "Chỉnh sửa",
      flex: 0.5,
      renderCell: (params) => (
        <div style={{ display: 'flex', alignItems: 'center', marginLeft:'15px' }}>
        
            <DeleteOutlineIcon
              style={{ cursor: 'pointer', fontSize: '25px' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
     
         
            <a href={`/updateSyllabusadmin/${params.row.id}`}>
              <UilEdit style={{ marginLeft: '5px' }} />
            </a>
         
        </div>
      ),
    }
  
  ];
  const filterNameSyllabus = (event) => {
    event.preventDefault();
    if (searchSyllabus) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.syllabusName.toLowerCase()).includes(clean(searchSyllabus.toLowerCase()))
      );
      setSyllabus(filteredClasses);
    } else {
      setSyllabus(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
            <Topbar setIsSidebar={setIsSidebar} />
            <ToastContainer />
              <Box m="20px">
                <Header title="Giáo Trình" subtitle="Quản lý giáo trình" />
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Link to='./addSyllabus' className="btn btn-success m-btn">
                      <i className="fa fa-plus"></i> Thêm mới 
                    </Link>

                  </Box>
               

                  <form onSubmit={filterNameSyllabus}>
                    <input
                      type="text"
                      className="form-control m-input"
                      placeholder="Tìm kiếm tên giáo trình ..."
                      onChange={(e) => setSearchSyllabus(e.target.value)}
                      value={searchSyllabus}
                    />
                  </form>
                </Box>
                <Box
                  m="40px 0 0 0"
                  maxHeight="80vh"
                  sx={{
                    "& .MuiDataGrid-root": {
                      border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                      borderBottom: "none",
                      borderRight: "0.5px solid #E0E0E0 !important"
                    },
                    "& .name-column--cell": {
                      color: colors.greenAccent[300],
                    },
                    "& .MuiDataGrid-columnHeaderTitle":{
                      color:"white"
                    },
                    "& .MuiDataGrid-columnHeaders": {
                      backgroundColor: "#2746C0",
                      borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                      backgroundColor: colors.primary[400],
                    },
                    "& .MuiDataGrid-footerContainer": {
                      borderTop: "none",
                      backgroundColor: "#2746C0",
                    },
                    "& .MuiCheckbox-root": {
                      color: `${colors.greenAccent[200]} !important`,
                    },
                  }}
                >
                   {dataWithSyllabus.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20,25]}  apiRef={apiRef} rows={dataWithSyllabus} columns={columns} onRowSelectionModelChange={handleSelectionChange} />

                  )}
                </Box>
              </Box>
             

            </main>
            
          </Box>
          <FooterAdmin/>
        </div>
      
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default SyllabusAdmin;
