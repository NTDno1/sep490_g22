import { Box, IconButton, Menu, useTheme } from "@mui/material";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";


import { useState } from "react";
import FooterAdmin from "../../FooterAdmin";

const HomeSuperAdmin = () => {
    const themes = useTheme();

    const [isSidebar, setIsSidebar] = useState(true);
    const [theme, colorMode] = useMode();
    return (

        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <div className="app">
                    <Box style={{ display: 'flex' }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main className="content" >
                            <Topbar setIsSidebar={setIsSidebar} />
                            <br/>
                            <br/>
                            <img
                                style={{ width: '100%', height: '70%', objectFit: 'cover', margin: '0 10px' }}
                                src="https://pitech.edu.vn:82/Images/PitechLab233737093.jpg"
                                alt="Background Image"
                            /> <br/>
                            <h3 style={{fontWeight:'bolder',justifyContent:'center',textAlign:'center'}}>CHÀO MỪNG BẠN ĐẾN VỚI HỆ THỐNG QUẢN TRỊ CỦA PITECH</h3>
                        </main>
                    </Box>
                    <FooterAdmin />
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    );
};

export default HomeSuperAdmin;
