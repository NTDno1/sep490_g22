import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify"
import Cookies from 'js-cookie';
import { convertDateFormat, convertDatePicker, formatDateString } from '../../../constants/constant';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { GetStudentDetail, UpdateInfoStudent } from '../../../api/apiClient/Student/studentAPI';
import "./student.css"
import FooterAdmin from '../../FooterAdmin';
const initialFieldValues = {
    id: '',
    firstName: '',
    midName: '',
    lastName: '',
    address: '',
    email: '',
    phoneNumber: '',
    dob: '',
    cccd: '',
    gender: 0,
    userName: '',
    image: File,

}

export default function ShowStudent() {
    const [values, setValues] = useState(initialFieldValues);
    const { code } = useParams();
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    let jwttoken = Cookies.get('jwttoken');
    const currentDate = new Date().toISOString().split('T')[0];
    const navigate = useNavigate();
    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login_admin');
            }
        };

        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        DetailStudent();
    }, [])
    const DetailStudent = async () => {
        const data = await GetStudentDetail(code);
        const result = data.result;
        if (data.result.gender === null) { data.result.gender = 1 }
        setValues({
            id: code,
            firstName: result.firstName,
            midName: result.midName,
            lastName: result.lastName,
            gender: result.gender,
            address: result.address,
            email: result.email,
            phoneNumber: result.phoneNumber,
            dob: (result.dob),
            cccd: result.cccd,
            image: result.image,
            userName: result.userName,
        });

    }
    const handleFormSubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData()
        formData.append('id', code)
        formData.append('firstName', values.firstName)
        formData.append('midName', values.midName)
        formData.append('lastName', values.lastName)
        formData.append('address', values.address)
        formData.append('email', values.email)
        formData.append('phoneNumber', values.phoneNumber)
        formData.append('dob', convertDatePicker(values.dob))
        formData.append('cccd', values.cccd)
        formData.append('gender', values.gender)
        formData.append('userName', values.userName)
        formData.append('image', values.image)

        try {
            await UpdateInfoStudent(formData)
            toast.success("Update Success")
        } catch (error) {
            console.error('Lỗi trong quá trình gửi biểu mẫu:', error);
        }
    }

    const handleInputChange = e => {
        const { name, value } = e.target;

        setValues({
            ...values,
            [name]: value
        })
    }
    const readFile = (uploadedFile) => {
        if (uploadedFile) {
            const reader = new FileReader();
            console.log(reader);
            reader.onload = (x) => {
                const img = document.querySelector('#preview-box');


                img.src = reader.result;

            };
            reader.readAsDataURL(uploadedFile);
        }
    };
    const handleChange = (event) => {
        setValues({
            ...values,
            image: event.target.files[0]
        })
        const fileUploader = document.querySelector('#customFile');
        const getFile = fileUploader.files;
        if (getFile.length !== 0) {
            const uploadedFile = getFile[0];
            readFile(uploadedFile);
        }
    };
    return (
        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <ToastContainer/>
                <div className="app">
                    <Box style={{ display: 'flex' }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main className="content" >
                            <Topbar setIsSidebar={setIsSidebar} />
                            <div className="container">
                                <div className="row">
                                    <div className="col-xl-5">
                                        {/*begin:: XemHocVien*/}
                                        <div
                                            className="m-portlet m-portlet m-portlet--head-sm m-portlet--rounded m-portlet--full-height"

                                        >
                                            <div
                                                className="m-scrollable"
                                                data-scrollable="true"
                                                data-max-height={300}
                                                style={{ height: 300, overflow: "hidden" }}
                                            >
                                                <div className="m-portlet__body">
                                                    {/*begin::Widget19*/}
                                                    <div className="m-widget19">
                                                        <div className="m-widget19__content" id="student_info">
                                                            <span
                                                                className="m-widget19__username m--font-info"
                                                                style={{ fontSize: "1.1rem" }}
                                                            >
                                                                <strong>Nguyễn Đỗ Huyền Chi</strong>
                                                                <small> </small>
                                                                <a
                                                                    href="javascript:;"
                                                                    className="text-info"
                                                                    onclick="show_modal_edit_info(563549)"
                                                                    title="Chỉnh sửa thông tin học viên"
                                                                >
                                                                    <i className="la la-edit pull-right" />
                                                                </a>
                                                            </span>
                                                            <div
                                                                className="m-widget19__header"
                                                                style={{ marginTop: "1.25rem" }}
                                                            >
                                                                <div className="m-widget19__user-img">
                                                                    <img
                                                                        className="m-widget19__img fit-img"
                                                                        src="/upload/images/original/students/alaska/1693040765_120780.jpg"
                                                                    />
                                                                </div>
                                                                <div className="m-widget19__info">
                                                                    <span className="m-widget19__time">
                                                                        <i className="la la-venus-mars m--font-metal" />
                                                                        &nbsp; Nữ{" "}
                                                                    </span>
                                                                    <br />
                                                                    <br />
                                                                    <span className="m-widget19__time">
                                                                        <i className="la la-birthday-cake m--font-metal" />
                                                                        &nbsp; 30/09/2007
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="">
                                                                <div className="m-list-search">
                                                                    <div className="m-list-search__results">
                                                                        <span className="m-list-search__result-item">
                                                                            <span className="m-list-search__result-item-icon">
                                                                                <i className="la la-qrcode m--font-metal" />
                                                                            </span>
                                                                            <span className="m-list-search__result-item-text">
                                                                                Mã:
                                                                                <strong>ALKVN-ALKST-KH-43815</strong>
                                                                            </span>
                                                                        </span>
                                                                        <span className="m-list-search__result-item">
                                                                            <span className="m-list-search__result-item-icon">
                                                                                <i className="la la-user m--font-metal" />
                                                                            </span>
                                                                            <span className="m-list-search__result-item-text">
                                                                                Tên đăng nhập:
                                                                                <strong>ALKVN-ALKST-KH-43815</strong>
                                                                            </span>
                                                                        </span>
                                                                        <span className="m-list-search__result-item">
                                                                            <span className="m-list-search__result-item-icon">
                                                                                <i className="la la-phone m--font-metal" />
                                                                            </span>
                                                                            <span className="m-list-search__result-item-text">
                                                                                Sale chính:
                                                                                <strong>
                                                                                    ST-Phùng Ngọc Linh -{" "}
                                                                                    <a
                                                                                        href="javascript:void(0)"
                                                                                        onclick='CallReceivedServices.CallToPhoneNumber("0973893365")'
                                                                                    >
                                                                                        0973893365
                                                                                    </a>
                                                                                </strong>
                                                                            </span>
                                                                        </span>
                                                                        <span className="m-list-search__result-item">
                                                                            <span className="m-list-search__result-item-icon">
                                                                                <i className="la la-comments m--font-metal" />
                                                                            </span>
                                                                            <span className="m-list-search__result-item-text">
                                                                                Hỗ trợ:
                                                                                <strong>
                                                                                    ST-Phùng Ngọc Linh{" "}
                                                                                    <a
                                                                                        href="javascript:void(0)"
                                                                                        onclick='CallReceivedServices.CallToPhoneNumber("0973893365")'
                                                                                    >
                                                                                        0973893365
                                                                                    </a>
                                                                                </strong>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/*end::Widget19*/}
                                                </div>
                                            </div>
                                        </div>
                                        {/*end:: XemHocVien*/}
                                    </div>
                                    <div className="col-xl-5">
                                        {/*begin:: XemPhuHuynh*/}
                                        <div
                                            className="m-portlet m-portlet--head-sm m-portlet--rounded m-portlet--full-height"
                                            style={{ minHeight: 300, marginBottom: "0.5rem" }}
                                        >
                                            <div className="m-portlet__body">
                                                {/*begin::Widget19*/}
                                                <div className="m-widget19">
                                                    <div className="m-widget19__content" id="list_data_parent">
                                                        <h3 style={{ fontSize: "1.3rem" }}>Phụ huynh</h3>
                                                        <br />
                                                        <span className="m-widget19__username m--font-success">
                                                            <a
                                                                href="https://alaska.center.edu.vn/center/parent/detail/563550"
                                                                target="_blank"
                                                            >
                                                                <strong>Chị Huệ</strong>
                                                            </a>
                                                        </span>
                                                        <div className="m-widget19__header" style={{ marginTop: 0 }}>
                                                            <div className="m-widget19__user-img">
                                                                <img
                                                                    className="m-widget19__img fit-img"
                                                                    src="/img/no_user.png"
                                                                />
                                                            </div>
                                                            <div className="m-widget19__info">
                                                                <span className="m-widget19__time">
                                                                    <i className="la la-phone m--font-metal" />
                                                                    &nbsp;&nbsp;
                                                                    <a
                                                                        href="javascript:void(0)"
                                                                        onclick='CallReceivedServices.CallToPhoneNumber("0976754384")'
                                                                    >
                                                                        0976754384
                                                                    </a>
                                                                </span>
                                                                <span className="m-widget19__time">
                                                                    <i className="la la-venus-mars m--font-metal" />
                                                                    &nbsp; Nam{" "}
                                                                </span>
                                                                <br />
                                                                <span className="m-widget19__time">
                                                                    <i className="la la-envelope m--font-metal" />
                                                                    &nbsp;
                                                                </span>
                                                                <br />
                                                                <span className="m-widget19__time">
                                                                    <i className="la la-user m--font-metal" />
                                                                    &nbsp;&nbsp;0976754384
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/*end::Widget19*/}
                                            </div>
                                        </div>
                                        {/*end:: XemPhuHuynh*/}
                                    </div>

                                </div>

                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="m-portlet m-portlet--rounded m-portlet--full-height">
                                            <div className="m-portlet__head">
                                                <div className="m-portlet__head-caption">
                                                    <div className="m-portlet__head-title">
                                                        <h3 className="m-portlet__head-text">
                                                            Tổng hợp ghi danh lớp học -{" "}
                                                            <small style={{ fontFamily: "Montserrat !important" }}>
                                                                Tính theo hợp đồng
                                                            </small>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="m-portlet__body">
                                                <div
                                                    className="m-scrollable"
                                                    data-scrollable="true"
                                                    data-max-height={187}
                                                    style={{ height: 187, overflow: "hidden" }}
                                                >
                                                    <div className="m-list-timeline m-list-timeline--skin-light">
                                                        {/* Widget 11 */}
                                                        <div className="">
                                                            {/*begin::Table*/}
                                                            <table className="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th style={{ textAlign: "center" }}> STT</th>
                                                                        <th style={{ textAlign: "center" }}> Khóa</th>
                                                                        <th style={{ textAlign: "center" }}>
                                                                            {" "}
                                                                            Còn lại / Đã học / Tổng đăng ký
                                                                        </th>
                                                                        <th style={{ textAlign: "center" }}>Chi tiết</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>1</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            IELTS5.0B-Phil(18b)-21
                                                                        </td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">0</b>/
                                                                            <b className="m--font-success">5</b> /
                                                                            <b className="m--font-warning">5</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>2</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            IELTS5.0C-Phil(18b)-21
                                                                        </td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">18</b>/
                                                                            <b className="m--font-success">0</b> /
                                                                            <b className="m--font-warning">18</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>3</td>
                                                                        <td style={{ textAlign: "center" }}>IELTS6.0A-PHIL-23</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">18</b>/
                                                                            <b className="m--font-success">2</b> /
                                                                            <b className="m--font-warning">20</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>4</td>
                                                                        <td style={{ textAlign: "center" }}>IELTS6.0B-PHIL-23</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">20</b>/
                                                                            <b className="m--font-success">0</b> /
                                                                            <b className="m--font-warning">20</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>5</td>
                                                                        <td style={{ textAlign: "center" }}>IELTS6.5A-PHIL-23</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">20</b>/
                                                                            <b className="m--font-success">0</b> /
                                                                            <b className="m--font-warning">20</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style={{ textAlign: "center" }}>6</td>
                                                                        <td style={{ textAlign: "center" }}>IELTS6.5B-PHIL-23</td>
                                                                        <td style={{ textAlign: "center" }}>
                                                                            <b className="m--font-info">20</b>/
                                                                            <b className="m--font-success">0</b> /
                                                                            <b className="m--font-warning">20</b>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            {/*end::Table*/}
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* End: Widget 11 */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </main>
                    </Box>
                    <FooterAdmin/>
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    )
}
