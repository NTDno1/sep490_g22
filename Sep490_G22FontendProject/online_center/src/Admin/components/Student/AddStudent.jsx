import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useLocation, useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ToastContainer, toast } from 'react-toastify';
import { AddNewStudent } from '../../../api/apiClient/Student/studentAPI';
import FooterAdmin from '../../FooterAdmin';
import { convertDateFormat, convertDatePicker, convertoDate, formatDateString } from '../../../constants/constant';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import Header from '../Header';
const initialFieldValues = {
  userName: '',
  firstName: '',
  address: '',
  email: '',
  phoneNumber: '',
  dob: formatDateString(new Date()),
  cccd: '',
  gender: 0,
  userName: '',
  dateUpdate: '',
  image: File,

}

export default function AddStudent() {
  const [values, setValues] = useState(initialFieldValues);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const defaultImage = "https://pitech.edu.vn:82/Images/Teacher/DefaultAvatar.png"
  let jwttoken = Cookies.get('jwttoken');
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
        setValues({
          ...values,
          address: response.data[0],
        });
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);

  const handleFormSubmit = async (e) => {
    e.preventDefault()
   
    const formData = new FormData()
    formData.append('userName', values.userName)
    formData.append('firstName', values.firstName)
    formData.append('address', values.address)
    formData.append('email', values.email)
    formData.append('phoneNumber', values.phoneNumber)
    formData.append('dob', convertoDate(values.dob))
    formData.append('cccd', values.cccd)
    formData.append('gender', values.gender)
    formData.append('userName', values.userName)
    if (values.image instanceof File) {
    formData.append('image', values.image)
    } else {
     
      formData.append('image', defaultImage)
    }

    try {
      const response = await AddNewStudent(formData);
      toast.success("Thêm thành công")
      setTimeout(() => {
        navigate('/student');
      }, 1000);
    } catch (error) {
      // const errorMessages = error.response.data.errors.Dob;
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  }

  const readFile = (uploadedFile) => {
    if (uploadedFile) {
      const reader = new FileReader();
      reader.onload = (x) => {
        const img = document.querySelector('#preview-box');


        img.src = reader.result;

      };
      reader.readAsDataURL(uploadedFile);
    }
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      image: event.target.files[0]
    })

    const fileUploader = document.querySelector('#customFile');
    const getFile = fileUploader.files;
    if (getFile.length !== 0) {
      const uploadedFile = getFile[0];
      readFile(uploadedFile);
    }
  };
  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    })
  }

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
            
              <div className="container">
                <Header title="Học Viên" subtitle="Thêm học viên" />
                <ToastContainer />
                <div className="row">
                  <div className="col-12">
                    {/* Page title */}


                    {/* Form START */}
                    <form className="file-upload" onSubmit={handleFormSubmit}>
                      <div className="row mb-5 gx-5">
                        {/* Contact detail */}

                        <div className="col-xxl-8 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <div className="col-md-6">
                                <input style={{ display: 'none' }}
                                  name='id'

                                />

                                <label className="form-label">Tên người dùng <span className='required-field'>*</span></label>

                                <input
                                  value={values.userName}
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='userName'
                                  aria-label="First name"
                                  required
                                  onChange={handleInputChange}
                                />
                              </div>
                              {/* First Name */}
                              <div className="col-md-6">
                                <input style={{ display: 'none' }}


                                  name='id'

                                />

                                <label className="form-label">Họ và tên <span className='required-field'>*</span></label>

                                <input
                                  value={values.firstName}
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='firstName'
                                  aria-label="First name"
                                  required
                                  onChange={handleInputChange}
                                />
                              </div>
                              {/* Last name */}

                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Ngày sinh <span className='required-field'>*</span>
                                </label>
                                <DatePicker
                                  value={(values.dob)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  required
                                  maxDate={new Date()}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                    setValues({
                                      ...values,
                                      dob: formatDateString(selectedDate),
                                    });
                                  }}
                                />
                              </div>
                              {/* Phone number */}
                              <div className="col-md-6">
                                <label className="form-label">Địa chỉ <span className='required-field'>*</span></label>
                                <select name="address" value={values.address} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">
                                  Số điện thoại <span className='required-field'>*</span>
                                </label>
                                <input
                                  value={values.phoneNumber}
                                  type="text"
                                  name='phoneNumber'
                                  className="form-control"

                                  pattern="[0-9]{10}"
                                  maxLength="10"
                                  required
                                  onChange={handleInputChange}
                                />
                              </div>
                              {/* Mobile number */}
                              {/* Email */}
                              <div className="col-md-6">
                                <label className="form-label">
                                  Email <span className='required-field'>*</span>
                                </label>
                                <input
                                  required
                                  value={values.email}
                                  type="email"
                                  name='email'
                                  className="form-control"


                                  onChange={handleInputChange}
                                />
                              </div>


                              <div className="col-md-6">
                                <label className="form-label">
                                  CCCD <span className='required-field'>*</span>
                                </label>
                                <input
                                  value={values.cccd}
                                  required
                                  type="text"
                                  name='cccd'
                                  pattern="[0-9]{12}"
                                  maxLength="12"
                                  className="form-control"


                                  onChange={handleInputChange}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="gender" className="form-label">
                                  Giới tính <span className='required-field'>*</span>
                                </label>
                                <div style={{ display: 'flex', justifyContent: 'space-around', fontSize: '18px' }}>
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="gender"
                                      id="male"
                                      value="1"
                                      checked={values.gender == '1'}
                                      onChange={handleInputChange}
                                    />
                                    <label className="form-check-label" htmlFor="male">
                                      Nam
                                    </label>
                                  </div>
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="gender"
                                      id="female"
                                      value="2"
                                      checked={values.gender == '2'}
                                      onChange={handleInputChange}
                                    />
                                    <label className="form-check-label" htmlFor="female">
                                      Nữ
                                    </label>
                                  </div>
                                </div>

                              </div>

                            </div>{" "}
                            {/* Row END */}
                          </div>
                        </div>
                        {/* Upload profile */}
                        <div className="col-xxl-4">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <div className="mb-4 mt-0 text-center" style={{ fontSize: '20px', fontWeight: 'bolder' }}>Cập nhật ảnh </div>
                              <div className="text-center">
                                {/* Image upload */}
                                <div className="square position-relative display-2 mb-3">
                                  <img style={{ width: '250px', height: '250px' }} src={values.image} id='preview-box'></img>
                                </div>
                                {/* Button */}
                                <input type="file" id="customFile" name="file" accept="image/*" onChange={handleChange} hidden="true" />
                                <label
                                  className="btn btn-success-soft btn-block"
                                  htmlFor="customFile"
                                >
                                  Chọn file
                                </label>
                                {/* Content */}

                              </div>
                            </div>

                          </div>
                        </div>
                        <div className="col-md-12">


                        </div>
                      </div>{" "}

                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                        <a className='backtolist' href='/student'> {'<< Quay về danh sách'}</a>
                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                          <i className="addicon fa fa-save"></i>Lưu
                        </button>

                      </div>
                    </form>{" "}
                    {/* Form END */}
                  </div>


                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
