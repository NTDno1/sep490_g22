import { Box, IconButton, Menu, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { ToastContainer, toast } from "react-toastify";
import Header from "../../components/Header";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useRef, useState } from "react";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import Cookies from "js-cookie";
import { convertDateFormat } from "../../../constants/constant";
import { clean } from 'diacritic';
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ActivateStudent, DeactivateStudent, DeleteListStudent, DeleteStudent, GetStudentAdminSide } from "../../../api/apiClient/Student/studentAPI";
import FooterAdmin from "../../FooterAdmin";
const Student = () => {
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [student, setStudent] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const jwttoken = Cookies.get('jwttoken');
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchStudent, setSearchStudent] = useState("");
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
 
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    ListAllStudent();
  }, [apiRef])
  const ListAllStudent = async () => {
    try{
    const data = await GetStudentAdminSide();
    console.log(data.result.length)
    if(data.result.length > 0){
    data.result = data.result.map((item) => {
      if (item.lastName === null) { item.lastName = "" }
      item.dateCreate = convertDateFormat(item.dateCreate);
      item.dob = convertDateFormat(item.dob);
      return item;
    });
    setOriginalClasses(data.result)
    setStudent(data.result)
   
      apiRef.current.setPageSize(7);
     
  }else {

  }
} catch(error){ } 
}
  const dataWithStudent = student.map((item, index) => ({
    ...item,
    StudentId: index + 1,
  }))


  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa tài khoản này ?")) {
      try{
      const response = await DeleteStudent(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        ListAllStudent();
      } else {
        toast.error(response.errorMessages)
      }
    }catch(error){
      toast.error('Có lỗi xảy ra');
    }
    };
  }
  const handleActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn kích hoạt tài khoản này ?")) {
      try{
      const response = await ActivateStudent(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        ListAllStudent();
      } else {
        toast.error(response.errorMessages)
      } }catch(error){}
    }
  }
  const handleDeActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn hủy kích hoạt tài khoản này ?")) {
      try{
      const response = await DeactivateStudent(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        ListAllStudent();
      } else {
        toast.error(response.errorMessages)
      } }catch(error){}
    }
  }
  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };

 



  const columns = [
    {
      headerName: "STT",
      field: "StudentId",
      flex: 0.5,
      align:'center'
    },
    {
      field: "userName",
      headerName: "Tên người dùng",
      flex: 1,
    },
    {
      field: "firstName",
      headerName: "Họ tên",
      flex: 1.5,
    },
  
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "phoneNumber",
      headerName: "Số điện thoại",
      align: "center",
      flex: 1,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      align: "center",
      flex: 1,
    },
    {
      field: "dateCreate",
      headerName: "Ngày tạo",
      align: "center",
      flex: 1,
    },
    {
      field: "Action",
      headerName: "Hành động",
      flex: 0.75,
      renderCell: (params) => (
      ( params.row.status === 0 ? (
        <UilLock
          style={{ cursor: 'pointer', color: 'gray' }}
    
        />
      ) :
          params.row.status === 1 ? (

            <UilUnlock
              style={{ cursor: 'pointer', color: 'green' }}
              onClick={() => handleDeActiveCenter([params.row.id])}
            />
          ) : (
            params.row.status === 2 ? (
              <UilLock
                style={{ cursor: 'pointer', color: 'red' }}
                onClick={() => handleActiveCenter([params.row.id])}
              />
            ) : null
          )
        )
      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 0.75,
      renderCell: (params) =>   <div style={{ display: 'flex', alignItems: 'center', marginLeft:'10px' }}>
      {params.row.status === 0 && (
        <DeleteOutlineIcon
          style={{ cursor: 'pointer', fontSize: '25px' }}
          onClick={() => handleRemoveClick([params.row.id])}
        />
      )}
     
        <a href={`/updateStudent/${params.row.id}`}>
          <UilEdit style={{ marginLeft: '5px' }} />
        </a>
     
    </div>

    },
 
  ];
  const filterNameTeacher = (event) => {
    event.preventDefault();
    if (searchStudent) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.userName.toLowerCase()).includes(clean(searchStudent.toLowerCase())) ||
        clean(val.lastName.toLowerCase()).includes(clean(searchStudent.toLowerCase()))
      );
      setStudent(filteredClasses);
    } else {
      setStudent(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>

                <Box m="20px">
                
                  <Header title="Học Viên" subtitle="Quản lý học viên" />
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box>
                    <Link to='/addstudent' className="btn btn-success m-btn">
                    <i className="fa fa-plus"></i> Thêm mới 
                  </Link>
                   <ToastContainer/>
                    </Box>


                    <form className="filter-search" onSubmit={filterNameTeacher}>
                        <input
                          type="text"
                          className="form-control m-input"
                          placeholder="Tìm kiếm tên học sinh..."
                          onChange={(e) => setSearchStudent(e.target.value)}
                          value={searchStudent}
                        />
                      </form>

                  </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                   
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important", 
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                        {dataWithStudent.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid
                      pageSizeOptions={[7, 20, 25]}
                      apiRef={apiRef}
                      rows={dataWithStudent}
                      columns={columns}
                      onRowSelectionModelChange={handleSelectionChange}
                    />
                  )}
                  </Box>
                </Box>
              </Box>
         
            </main>
          </Box>
          <FooterAdmin/>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default Student;
