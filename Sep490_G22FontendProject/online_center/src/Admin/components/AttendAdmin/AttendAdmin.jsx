import React, { useEffect, useState } from "react"
import "../../../Client/components/Attend/attend.css"
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from "react-toastify";
import Cookies from "js-cookie";
import { convertDateFormat, getLastPartAfterSlash } from "../../../constants/constant";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import Header from "../Header";
import { useNavigate } from "react-router-dom";
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { getAllClassAdminSide, SchedualofClass } from "../../../api/apiClient/Class/ClassAPI";
import { ListAttendenceBySchedule, TakeAttendenceAdmin } from "../../../api/apiClient/Attend/Attend";
import FooterAdmin from "../../FooterAdmin";
const AttendAdmin = () => {
  const [classes, setClases] = useState([]);
  const [selectclassId,setClassID] = useState();

  const [student, setStudent] = useState([]);
  const [slot, setSlot] = useState([]);
  const [scheduleId, setScheduleId] = useState();
  const [studentRadioStates, setStudentRadioStates] = useState([]);
  const attendanceData = [];
  const [selectslotId,setSlotID] = useState();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const  jwttoken =  Cookies.get('jwttoken');
  const defaultImage = "https://pitech.edu.vn:82/Images/Teacher/DefaultAvatar.png"
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) { 
        navigate('/login_admin');
      }
    }; 
    checkTokenBeforeRoute();
  }, [jwttoken]);
  const handleRadioChange = (index, type) => {
    const updatedStudentRadioStates = studentRadioStates.map((radioState, i) => {
      if (i === index) {
        return {
          ...radioState,
          [type]: true,
          [type === 'attend' ? 'absent' : 'attend']: false
        };
      }
      return radioState;
    });
    setStudentRadioStates(updatedStudentRadioStates);
  };
  useEffect(() => {  
    
    fetchDataTeacher();
    if (student) {
      const initialStudentRadioStates = student.map((val) => ({
        attend: val.status === 1,
        absent: (val.status === 0 || val.status === 2),
      }));
      setStudentRadioStates(initialStudentRadioStates);
    }
    ListAttendenceBySchedule(selectslotId)
  }, [student]);

  const fetchDataTeacher = async () => {
    try{
    const data = await getAllClassAdminSide();
    setClases(data.result);
    setClassID(data.result[0].id)
    fetchStudentClass(data.result[0].id);
    }catch(error){
    }
    
  };

  const handleSelectChange = (event) => {

    const selectedClassId = event.target.value;
    setClassID(selectedClassId)
    
    fetchStudentClass(selectedClassId);
  };
  const handleSelectChangeSlot = (event) => {
    const selectSlotId = event.target.value;
    setSlotID(selectSlotId);
  }

  const fetchStudentClass = async (classId) => {
    const data = await SchedualofClass(classId);
   
      setSlot(data.result);
      setSlotID(data.result[0].id)
    
  };

  const handleTakeAttend = async (e) => {
    e.preventDefault();
    student?.forEach((val, index) => {
      const rowData = {
        id: val.id,
        studentId: val.studentId,
        status: document.querySelector(`input[name=contact_${index}_absent]`).checked ? 2 : 1,
        schedualId: parseInt(scheduleId),
        note: val.note
      };
      attendanceData.push(rowData);
    });
    try {
      const jsonData = JSON.stringify(attendanceData);
      const response = await TakeAttendenceAdmin(jsonData);
      toast.success('Update successfully')
      handleListStudent(e)
     
    } catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          const errorMessage = errorMessages[0];
          toast.error(errorMessage);
          console.error('Error when send data:', error);
        }
      }
    }
  }

  const handleListStudent = async(e) => {
  
    e.preventDefault();
    try{
      const data = await ListAttendenceBySchedule(selectslotId);
      
        setScheduleId(selectslotId);
        data.result = data.result.map((item) => {
          if((getLastPartAfterSlash(item.studentImage)) === "") {
            item.studentImage = defaultImage
          }
        
          return item;
        });
        setStudent(data.result);
      
    }catch(error){
        toast.error("Xảy ra lỗi")
    }
  }
  return (
    <>
   <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main>
                  <Topbar setIsSidebar={setIsSidebar} />

                  <div className="m-content">
                  <Header title="Attend" subtitle="Điểm danh học sinh" />
        <div>
        <form onSubmit={(e) => handleListStudent(e)}>
          <div style={{ marginTop: '30px' }} className="col-xl-8 order-2 order-xl-1">
            <div className="form-group m-form__group row align-items-center">
             
              <div className="col-md-3">
                <label>Lớp học</label>
                <select
                  className="form-control"
                  id="select-course"
                  onChange={handleSelectChange}
                  value={selectclassId}
                >
                 
                {classes.map((classItem, index) => (
                  <option key={index} value={classItem.id}>
                    {classItem.name}
                  </option>
                ))}               
                </select>
              </div>
              <div className="col-md-5">
              <label>Ca học</label>
              <select
                className="form-control"
                id="select-course"
                onChange={handleSelectChangeSlot}
                  value={selectslotId}
              >
              
                {slot.map((slotItem, index) => (
                  <option key={index} value={slotItem.id} >
                    Slot {slotItem.slotNum} - {convertDateFormat(slotItem.dateOffSlot)} ({slotItem.timeStart} - {slotItem.timeEnd})
                    {slotItem.staus === 1 && <span>&#10003;</span>}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-md-3">
              <label></label>
            <div className="select-student">
              <button style={{ display: 'flex', background: ' #2746C0', padding: '10px 15px' }} type="submit" className="attended"> <span style={{ display:'inline-block',lineHeight:'auto',marginTop:'-2px',marginRight:'4px' }}><RemoveRedEyeIcon/> </span>Xem</button>
            </div>
            </div>
            </div>
          </div>
          </form>
          {student.length > 0 ? (
          <form onSubmit={handleTakeAttend}>
          
            {/*end: Search Form */}
            <table id="keywords" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                  <th className="center" style={{ width: "4%" }}>
                    ID
                  </th>
                  <th>Hình ảnh</th>
                  <th>Học viên</th>

                  <th>Trạng thái</th>
                  <th>Ghi chú</th>


                  <th style={{ width: "15%" }}>Hành động</th>
                </tr>
              </thead>
              <tbody>
                {student?.map((val, index) => (
                  <tr key={index}>
                    <td >{index + 1}</td>
                    <td><img className="img-attend" src={val.studentImage}></img></td>
                    <td> <b style={{ color: 'blue', width: '30%' }}>{val.studentName}</b></td>
                    <td style={{ color: val.status === 1 ? 'green' : val.status === 2 ? 'red' : 'black' }}>
                      {val.status === 0 ? 'Chưa điểm danh' : val.status === 1 ? 'Có mặt' : val.status === 2 ? 'Vắng mặt' : ''}
                    </td>
                    <td>{val.note}</td>
                    <td style={{ display: 'flex', justifyContent: 'space-around', height: '97px' }}>
                      <div>
                        <input
                          type="radio"
                          name={`contact_${index}_absent`}
                          value="absent"
                          checked={studentRadioStates[index]?.absent}
                          onChange={() => handleRadioChange(index, 'absent')}
                        />


                        <label>Vắng mặt</label>
                      </div>
                      <div>
                        <input
                          type="radio"
                          name={`contact_${index}_attend`}
                          value="attend"
                          checked={studentRadioStates[index]?.attend}
                          onChange={() => handleRadioChange(index, 'attend')}
                        />
                        <ToastContainer />
                        <label>Điểm danh</label>
                      </div>
                    </td>

                  </tr>
                ))}
              </tbody>
            </table>
            <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: '-20px' }} className="select-student">
              <button style={{ display: 'flex', justifyContent: 'flex-end', background: ' #2746C0', padding: '10px 30px' }} className="attended"> <span style={{ marginRight: '3px' }}>&#10003; </span>Lưu</button>
            </div>
          </form>
          ) : (
            <>
            <br/>
             <div style={{textAlign:'center',fontSize:'18px'}}> Không có dữ liệu</div>
             </>
          )} 
        </div>

      </div>
     
      </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>



    </>
  )
}

export default AttendAdmin
