import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import {  useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider, useTheme } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ToastContainer, toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';
import { EmployeeDetail, UpdateInfosEmployee } from '../../../api/apiClient/Profile/Profile';
import { convertDateFormat, convertDatePicker, formatDateString } from '../../../constants/constant';
import FooterAdmin from '../../FooterAdmin';
import axios from 'axios';
const initialFieldValues = {
  id: '',
  name: '',
  adress: '',
  email: '',
  phoneNumber: '',
  dob:'',
  centerName: '',
  userName: '',
}

export default function AdminProfile() {
  const [user, setValues] = useState(initialFieldValues);
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    const navigate = useNavigate();
    const jwttoken = Cookies.get('jwttoken');
    const id = Cookies.get('empId');
    const host = "https://provinces.open-api.vn/api/";
    const [provinces, setProvinces] = useState([]);

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
    
  }, [jwttoken]);

  useEffect(() => {
    getInformationAdmin();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
     
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [])
 

  const getInformationAdmin = async() => {
    const data = await EmployeeDetail(id);     
    setValues({
      id: data.result.id,
      name: data.result.name,
      adress: data.result.adress,
      email: data.result.email,
      dob : data.result.dob,  
      phoneNumber: data.result.phoneNumber,
      centerName: data.result.centerName,   
      userName : data.result.userName,   
    });
  };
    

  const handleFormSubmit = async(e) => {
    e.preventDefault()
    const formSubmit = new FormData()
    formSubmit.append("id",user.id);
    formSubmit.append("name",user.name);
    formSubmit.append("adress",user.adress);
    formSubmit.append("email",user.email);
    formSubmit.append("phoneNumber",user.phoneNumber);
    formSubmit.append("dob",user.dob);
    try {
      const response = await UpdateInfosEmployee(user.id,user.name,user.adress,user.email,user.phoneNumber,convertDateFormat(user.dob));
      toast.success("Cập nhật thành công")
      setTimeout(() => {
        window.location.reload()
      },1000)
    } catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi

        }
      }
    }
 
}
  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...user,
      [name]: value,
    });
  };

  console.log(user)
  return (
    <>
      <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
           <ToastContainer/>
           <div className="container">
             <div className="row">
             <div className="col-12">
                {/* Page title */}
                <div className="my-5">
                  <h3>Thông tin của tôi</h3>
                  <hr />
          </div>
          {/* Form START */}
          <form className="file-upload" onSubmit={handleFormSubmit}>
            <div className="row mb-5 gx-5">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-xxl-0 pb-5">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                    <div className="text-center mt-1"><h4 style={{ fontWeight: 'bolder' }}>Thông tin</h4></div>
                    {/* First Name */}
                    <div className="col-md-6">
                      <label className="form-label">Tên người dùng </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        aria-label="First name"
                        value={user.name}
                        onChange={handleInputChange}
                        name='name'
                      />
                    </div>
                
                    {/* Last name */}
                    <div className="col-md-6">
                      <label className="form-label">Email </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        aria-label="Last name"
                        value={user.email}
                        onChange={handleInputChange}
                        name='email'
                      />
                    </div>
                    {/* Phone number */}
                    <div className="col-md-6">
                      <label className="form-label">Số điện thoại</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        aria-label="Phone number"
                        value={user.phoneNumber}
                        onChange={handleInputChange}
                        name='phoneNumber'
                      />
                    </div>
                    {/* Mobile number */}
                    <div className="col-md-6">
                                <label className="form-label">Địa chỉ</label>
                                <select name="adress" value={user.adress} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>
                    
                    {/* Skype */}
                    <div className="col-md-6">
                      <label className="form-label">DoB</label>
                      <DatePicker
                        value={formatDateString(user.dob)}
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                        required
                        maxDate={new Date()}
                        showYearDropdown
                        scrollableYearDropdown
                        yearDropdownItemNumber={25}
                        onChange={(date) => {
                          const selectedDate = new Date(date);
                            setValues({
                              ...user,
                              dob: convertDatePicker(selectedDate),
                            });
                        }}            
                      />
                    </div>
                  
                  </div>{" "}
                  {/* Row END */}

                </div>

              </div>

          
           
            </div>{" "}
            <div className='text-center mb-5 mt-5'><button type="submit" className="btn btn-success-soft">
              Cập nhật
            </button></div>
            <ToastContainer/>
            {/* Row END */}
            {/* Social media detail */}
          </form>

     
        </div>
      </div>
    </div>
  
  </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
    </>

  )
}
