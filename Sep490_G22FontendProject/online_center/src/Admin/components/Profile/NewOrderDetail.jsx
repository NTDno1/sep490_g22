import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from 'react-router-dom';
import {

    UilTimesCircle,
} from "@iconscout/react-unicons";
import Cookies from 'js-cookie';
import { convertDateFormat, formatNumberWithDecimalSeparator } from '../../../constants/constant';
import Modal from 'react-bootstrap/Modal';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import FooterAdmin from '../../FooterAdmin';
import { AddNewOrderDetail, DeleteOrder, Payment, listOrderDetailbyID, listOrderbyID } from '../../../api/apiClient/Order/Order';
import { getAllViewCourse } from '../../../api/apiClient/Courses/CoursesAPI';
import axios from 'axios';
const initialFieldValues = {
    courseId: '',
    price: '',
    discount: 0,
    note: '',
    orderDetailId: '',
}

const initialFieldValuesOrder = {
    name: '',
    title: '',
    orderCode: '',
    createDate: '',
    updateDate: '',
    studentName: '',
    status: '',
    employeeName: '',
    note: '',

}
export default function AddClassMember() {
    const [order, setOrder] = useState([]);
    const [values, setValues] = useState(initialFieldValues);
    const [valuesOrder, setValuesOrder] = useState(initialFieldValuesOrder);

    let totalPrice = 0;
    let jwttoken = Cookies.get('jwttoken');
    const [course, setCourse] = useState([]);
    const { envoiceid } = useParams();
    const navigate = useNavigate();
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login_admin');
            }
        };
        checkTokenBeforeRoute();

    }, [jwttoken]);
    useEffect(() => {
        getOrderAdmin();
        fetchDataCourse();
        getOrderDetailbyId();
    }, []);

    const getOrderDetailbyId = async () => {

        try {
            const data = await listOrderDetailbyID(envoiceid)
            const result = data.result;
            setValuesOrder({
                name: result.name,
                title: result.title,
                orderCode: result.orderCode,
                createDate: convertDateFormat(result.createDate),
                updateDate: convertDateFormat(result.updateDate),
                studentName: result.studentName,
                status: result.status,
                employeeName: result.employeeName,
                note: result.note,

            });
        } catch (error) {

        }

    }

    const getOrderAdmin = async () => {
        try {
            const data = await listOrderbyID(envoiceid);
            console.log(data)
            if (data.result) {
                data.result = data.result.map((item) => {
                    item.createDate = convertDateFormat(item.createDate);
                    item.updateDate = convertDateFormat(item.updateDate);
                    return item;
                });
                setOrder(data.result)

            } else { }
        } catch (error) { }
    }
    const fetchDataCourse = async () => {
        try {
            const data = await getAllViewCourse();
            if (data && data.result && data.result.length > 0) {
                setCourse(data.result);
                setValues((prevValues) => ({
                    ...prevValues,
                    courseId: data.result[0].id,
                    price: data.result[0].price,
                }));

            }
        } catch (error) {
            console.error("Lỗi khi lấy dữ liệu khóa học:", error.message);
        }

    };
    const handleInputChange = (e) => {
        const { name, value } = e.target;
        // If the changed input is the courseId, update the price accordingly
        if (name === 'courseId') {
            const selectedCourse = course.find(course => course.id === parseInt(value, 10));
            if (selectedCourse) {
                setValues(prevValues => ({
                    ...prevValues,
                    [name]: value,
                    discount: 0,
                    price: selectedCourse.price,
                    courseId: selectedCourse.id
                }));
            }
        } else if (name === 'discount') {
            const discountValue = parseFloat(value) || 0;
            const originalPrice = course.find(course => course.id === values.courseId)?.price || 0;
            const discountedPrice = Math.round(originalPrice - originalPrice * (discountValue / 100));

            setValues(prevValues => ({
                ...prevValues,
                [name]: discountValue,
                price: discountedPrice,
            }));


        } else {
            setValues(prevValues => ({
                ...prevValues,
                [name]: value,
            }));
        }
    };

    const addOrEdit = async (formData) => {
        try {
            const response = await AddNewOrderDetail(formData);

            toast.success("Thành công")
            getOrderAdmin();
        }
        catch (error) {
            if (error.response && error.response.data && error.response.data.errorMessages) {
                const errorMessages = error.response.data.errorMessages;
                if (errorMessages && errorMessages.length > 0) {
                    // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                    const errorMessage = errorMessages[0];
                    toast.error(errorMessage); // Hiển thị thông điệp lỗi
                    console.error('Error:', error);
                }
            }
        }
    };
    const handleFormSubmit = (e) => {
        e.preventDefault()
        const formData = new FormData()
        formData.append('courseId', values.courseId)
        formData.append('price', values.price)
        formData.append('discount', values.discount)
        formData.append('note', values.note)
        formData.append('orderDetailId', envoiceid)
        addOrEdit(formData)
    }
    const handleFormPayment = async (e) => {
        e.preventDefault()
        try {
            const response = await Payment(`"${envoiceid}"`);
            toast.success("Cập nhật thành công");
            getOrderAdmin();
        }
        catch (error) {
            toast.error("Xảy ra lỗi trong quá trình thực hiện")
        }
    }
    order.forEach((val) => {
        totalPrice += val.price;
    });
    const PDFGenerate = async (e) => {
        try {
            e.preventDefault()
            const response = await axios.get(`https://pitech.edu.vn:82/api/Order/generatepdf?orderId=${envoiceid}`, {
                responseType: 'arraybuffer',
                headers: {
                    'Authorization': `Bearer ${jwttoken}`,
                },
            });

            // Tạo Blob từ arraybuffer để tạo URL và hiển thị PDF trong trình duyệt
            const blob = new Blob([response.data], { type: 'application/pdf' });
            const url = URL.createObjectURL(blob);

            // Mở trang mới hiển thị PDF
            window.open(url, '_blank');
            console.log(url)
        } catch (error) {
            console.error('Error generating PDF:', error);
        }
    }
    const deleteOrder = async (id) => {
       
        if (window.confirm("Bạn có muốn xóa hóa đơn này")) {
            try {
                const response = await DeleteOrder(id);
                toast.success('Thành công')
                getOrderAdmin();
            }
            catch (error) {
                toast.error("Xảy ra lỗi");
            }

        }
    }
    return (

        <>
            <ColorModeContext.Provider value={colorMode}>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <div className="app">
                        <Box style={{ display: 'flex' }}>
                            <Sidebars isSidebar={isSidebar} />
                            <main className="content">
                                <Topbar setIsSidebar={setIsSidebar} />
                                <ToastContainer />
                                <div className="container">
                                    <ToastContainer />
                                    <br/>
                                    <br/>
                                    <div className="row">

                                        <div className="col-12">

                                            <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '40px' }}>
                                                <div className="m-portlet__head-tools m--pull-right">

                                                </div>

                                                <div
                                                    className="m-portlet__head-tools m--pull-right"
                                                    style={{ marginTop: 8, marginRight: 10, display: 'flex' }}
                                                >
                                                    {(valuesOrder.status == 0 && (
                                                        <button style={{ background: '#2746C0', padding: '10px 20px', marginRight: '20px' }} type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" onClick={handleShow}>
                                                            Thêm mới
                                                        </button>)
                                                    )}
                                                    <form onSubmit={PDFGenerate}>
                                                        <button style={{ background: '#2746C0', padding: '10px 20px' }} type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                                            Xem dưới dạng PDF
                                                        </button>
                                                    </form>
                                                </div>

                                            </div>
                                            <h3> Chi tiết hóa đơn</h3>
                                            <div className="table-responsive" id="table-content">
                                                <table
                                                    className="table table-bordered table-hover"
                                                    id="class-table"
                                                    style={{ minHeight: "250px !important" }}
                                                >
                                                    <thead>
                                                        <tr>

                                                            <th>Tên hóa đơn</th>
                                                            <th>Mã hóa đơn</th>

                                                            <th>Học viên</th>
                                                            <th>Ghi chú</th>
                                                            <th>Ngày tạo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td>
                                                                {valuesOrder.name}
                                                            </td>
                                                            <td>{valuesOrder.orderCode}</td>
                                                            <td>
                                                                {valuesOrder.studentName}
                                                            </td>

                                                            <td>{valuesOrder.note}</td>
                                                            <td>{valuesOrder.createDate}</td>
                                                        </tr>

                                                    </tbody>
                                                </table>

                                            </div>
                                            <div className="table-responsive" id="table-content">
                                                <table
                                                    className="table table-bordered table-hover"
                                                    id="class-table"
                                                    style={{ minHeight: "250px !important" }}
                                                >
                                                    <thead>
                                                        <tr>

                                                            <th>STT</th>
                                                            <th>Tên khóa học</th>

                                                            <th>Ngày tạo</th>
                                                            <th>Ngày cập nhật</th>
                                                            <th>Giảm giá</th>
                                                            <th>Giá</th>
                                                            <th>Ghi chú</th>
                                                            {(valuesOrder.status == 0 && (
                                                            <th>Hành động</th>
                                                            ))}

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {order.map((val, index) => (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                    >
                                                                        {val.courseName}
                                                                    </span>
                                                                </td>

                                                                <td>
                                                                    {val.createDate}
                                                                </td>
                                                                <td>
                                                                    {val.updateDate}
                                                                </td>
                                                                <td>{val.discount}%</td>
                                                                <td>
                                                                    {formatNumberWithDecimalSeparator(val.price)}VNĐ
                                                                </td>
                                                                <td>
                                                                    {val.note}
                                                                </td>
                                                                {(valuesOrder.status == 0 && (
                                                                <td><a style={{ cursor: 'pointer' }} onClick={() => deleteOrder(val.id)}><UilTimesCircle /></a> </td>
                                                                ))}
                                                                </tr>
                                                        ))}
                                                        <tr>
                                                            <td colSpan="5">Tổng giá trị hóa đơn</td>
                                                            <td>{formatNumberWithDecimalSeparator(totalPrice)}VNĐ</td>
                                                            <td colSpan="2"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            {/* Form END */}
                                        </div>

                                        <div class="col-md-12" align="right">
                                            <form onSubmit={handleFormPayment}>

                                                <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                                                    <a className='backtolist' href='/invoice'> {'<< Quay về danh sách'}</a>
                                                    {(valuesOrder.status == 0 && (
                                                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                                                            <i className="addicon fa fa-save"></i>Thanh toán
                                                        </button>
                                                    ))}
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <Modal show={show} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Thêm hóa đơn</Modal.Title>
                                    </Modal.Header>
                                    <form onSubmit={handleFormSubmit}>
                                        <Modal.Body>
                                            <label className="form-label">Khóa học</label>
                                            <select
                                                className="form-control"
                                                name="courseId"
                                                value={values.courseId}
                                                onChange={handleInputChange}
                                            >
                                                {course?.map((option) => (
                                                    <option key={option.id} value={option.id}>
                                                        {option.name}
                                                    </option>
                                                ))}
                                            </select>
                                            <label className="form-label">Giảm giá</label>

                                            <input className="form-control" name='discount' onChange={handleInputChange} value={values.discount} type="number" min="0" />


                                            <label className="form-label">Giá</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder=""
                                                name='price'
                                                value={values.price}

                                                disabled
                                            />
                                            <label htmlFor="textareaInput">Note:</label>
                                            <textarea value={values.note} name='note' style={{ height: '200px', width: '100%' }} onChange={handleInputChange} type='text' ></textarea>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <button className='btn btn-success m-btn' variant="primary" type="submit">
                                                Lưu thay đổi
                                            </button>
                                        </Modal.Footer>
                                    </form>
                                </Modal>

                            </main>
                        </Box>
                        <FooterAdmin />
                    </div>
                </ThemeProvider>
            </ColorModeContext.Provider>
        </>
    )
}
