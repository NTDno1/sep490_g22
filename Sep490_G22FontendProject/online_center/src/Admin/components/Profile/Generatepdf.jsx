import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from 'react-router-dom';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { DeleteListMemberClass, DeleteMemberClass, StudentofClass } from '../../../api/apiClient/Class/ClassAPI';
import { GenerateAttendence } from '../../../api/apiClient/Attend/Attend';
import FooterAdmin from '../../FooterAdmin';
import { getAllViewCourse } from '../../../api/apiClient/Courses/CoursesAPI';
import { AddNewOrderDetail, generatepdf } from '../../../api/apiClient/Order/Order';
import axios from 'axios';
const initialFieldValues = {
    courseId: '',
    price: '',
    discount: '',
    note: '',
    orderDetailId: '',
}
export default function Generatepdf() {
    let jwttoken = Cookies.get('jwttoken');
    const [course, setCourse] = useState([]);
    const [values, setValues] = useState(initialFieldValues); 
    const { envoiceid } = useParams();
    const navigate = useNavigate();
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login_admin');
            }
        };

        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        fetchDataCourse();
    }, []);

    const fetchDataCourse = async () => {
        try {
            const data = await getAllViewCourse();
            if (data && data.result && data.result.length > 0) {
                setCourse(data.result);
                setValues((prevValues) => ({
                    ...prevValues,
                    courseId: data.result[0].id,
                    price: data.result[0].price,
                }));
            }
        } catch (error) {
            console.error("Lỗi khi lấy dữ liệu khóa học:", error.message);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        // If the changed input is the courseId, update the price accordingly
        if (name === 'courseId') {
            const selectedCourse = course.find(course => course.id === parseInt(value, 10));
            if (selectedCourse) {
                setValues(prevValues => ({
                    ...prevValues,
                    [name]: value,
                    price: selectedCourse.price,
                }));
            }
        }  else if (name === 'discount') {    
            const discountValue = parseFloat(value) || 0; 
            const originalPrice = course.find(course => course.id === values.courseId)?.price || 0;
            const discountedPrice = originalPrice - originalPrice * (discountValue / 100);
            setValues(prevValues => ({
                ...prevValues,
                [name]: discountValue,
                price: discountedPrice,
            }));
        } else {
            setValues(prevValues => ({
                ...prevValues,
                [name]: value,
            }));
        }
    };

    const addOrEdit = async (formData) => {
        try {
            const response = await AddNewOrderDetail(formData);
            toast.success("Thành công")
            navigate("/generatePdf")
        }
        catch (error) {
            if (error.response && error.response.data && error.response.data.errorMessages) {
                const errorMessages = error.response.data.errorMessages;
                if (errorMessages && errorMessages.length > 0) {
                    // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                    const errorMessage = errorMessages[0];
                    toast.error(errorMessage); // Hiển thị thông điệp lỗi
                    console.error('Error:', error);
                }
            }
        }
    };
    const handleFormSubmit = async(e) => {
        try {
            e.preventDefault()
            const response = await axios.get(`https://pitech.edu.vn:82/api/Order/generatepdf?orderId=15`, {
              responseType: 'arraybuffer', // Để xử lý dữ liệu PDF dưới dạng arraybuffer
            });
      
            // Tạo Blob từ arraybuffer để tạo URL và hiển thị PDF trong trình duyệt
            const blob = new Blob([response.data], { type: 'application/pdf' });
            const url = URL.createObjectURL(blob);
      
            // Mở trang mới hiển thị PDF
            window.open(url, '_blank');
            console.log(url)
          } catch (error) {
            console.error('Error generating PDF:', error);
          }
    }
    
    return (
        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <div className="app">
                    <Box style={{ display: 'flex' }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main className="content" >
                            <Topbar setIsSidebar={setIsSidebar} />
                            <div className="container">
                                <ToastContainer />
                                <div className="row">
                                    <div className="col-12">
                                        <form onSubmit={handleFormSubmit}>

                                         
                                                <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                                                    <i className="addicon fa fa-save"></i>Lưu
                                                </button>

                                        

                                        </form>
                                        {/* Form END */}
                                    </div>


                                </div>
                            </div>

                        </main>
                    </Box>
                    <FooterAdmin />
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    )
}
