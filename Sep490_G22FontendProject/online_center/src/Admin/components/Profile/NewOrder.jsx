import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify"
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider, Tooltip } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import FooterAdmin from '../../FooterAdmin';
import { GetStudentAdminSide } from '../../../api/apiClient/Student/studentAPI';
import { AddNewOrder } from '../../../api/apiClient/Order/Order';
const initialFieldValues = {
    name: '',
    title: '',
    note: '',
    studentId: '',
}
export default function NewOrder() {
    let jwttoken = Cookies.get('jwttoken');
    const [student, setStudent] = useState([]);
    const [values, setValues] = useState(initialFieldValues);
    const [liststudent, SetListstudent] = useState([]);
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    const [selectAll, setSelectAll] = useState(false);
    const [checkedItems, setCheckedItems] = useState({});
    const navigate = useNavigate();


    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login_admin');
            }
        };

        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        
        fetchDataListStudent();
    }, []);

  
    const fetchDataListStudent = async () => {
        const data = await GetStudentAdminSide();
        if (data.result.length) {
            setStudent(data.result);

        }
    };

    const handleInputChange = e => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }
    const handleFormSubmit = (e) => {
        e.preventDefault()
        const formData = new FormData()
        formData.append('name', values.name)
        formData.append('title', values.title)
        formData.append('note', values.note)
        formData.append('studentId', values.studentId)
        addOrEdit(formData)
      }
      let envoiceID = 0;
      const addOrEdit = async (formData) => {
        try {
          const response = await AddNewOrder(formData);
          console.log(response)
          envoiceID = response.result.id;
          toast.success('Thành công');
          navigate(`/orderdetail/${envoiceID}`);
        }
        catch (error) {
          if (error.response && error.response.data && error.response.data.errorMessages) {
            const errorMessages = error.response.data.errorMessages;
            if (errorMessages && errorMessages.length > 0) {
              // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
              const errorMessage = errorMessages[0];
              toast.error(errorMessage); // Hiển thị thông điệp lỗi
              console.error('Error:', error);
            }
          }
        }
      };

 
    return (
        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <div className="app">
                    <Box style={{ display: 'flex' }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main className="content" >
                            <Topbar setIsSidebar={setIsSidebar} />
                            <div className="container">
                                <ToastContainer />
                                <br/>
                                <br/>
                                <div className="row">
                                    <div className="col-12">
                                        <form onSubmit={handleFormSubmit}>
                                            <div className="row mb-5 gx-12">
                                                {/* Contact detail */}
                                                <div className="col-xxl-12 mb-5 mb-xxl-0">
                                                    <div className="bg-secondary-soft px-4 py-5 rounded">
                                                        <div className="row g-3">
                                                            {/* First Name */}

                                                            <div className="col-md-12">
                                                                <label className="form-label">Tên hóa đơn *</label>
                                                                <div style={{ position: 'relative', display: 'flex' }}>
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        placeholder=""
                                                                        name='name'
                                                                        value={values.name}
                                                                        onChange={handleInputChange}
                                                                        required
                                                                    />

                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label className="form-label">Tiêu đề</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder=""
                                                                    name='title'
                                                                    value={values.title}
                                                                    onChange={handleInputChange}
                                                                    required
                                                                />
                                                            </div>
                                                            {/* Last name */}
                                                            <div className="col-md-6">
                                                                <label className="form-label">Học viên</label>
                                                                <select className="form-control" name="studentId" onChange={handleInputChange}>
                                                                    <option value=""  >Chọn học viên</option>
                                                                    {student?.map((option) => (
                                                                        <option key={option.id} value={option.id}>
                                                                            {option.firstName} {option.midName} {option.lastName} #{option.studentCode}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </div>
                                                            <div className="col-md-12">
                                                                <label htmlFor="textareaInput">Note:</label>
                                                                <textarea name='note' style={{ height: '200px', width: '100%' }} onChange={handleInputChange} type='text' ></textarea>
                                                            </div>
                                                            {/* Phone number */}

                                                            {/* Mobile number */}




                                                        </div>{" "}

                                                    </div>
                                                </div>

                                            </div>{" "}


                                            <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                                                <a className='backtolist' href='/classadmin'> {'<< Quay về danh sách'}</a>
                                                <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                                                    <i className="addicon fa fa-save"></i>Lưu
                                                </button>

                                            </div>

                                        </form>
                                        {/* Form END */}
                                    </div>


                                </div>
                            </div>

                        </main>
                    </Box>
                    <FooterAdmin />
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    )
}
