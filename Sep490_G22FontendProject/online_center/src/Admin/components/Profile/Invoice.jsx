import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import { useEffect, useState } from "react";
import ViewListIcon from '@mui/icons-material/ViewList';
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import { ToastContainer, toast } from "react-toastify";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import FooterAdmin from "../../FooterAdmin";
import { convertDateFormat, formatNumberWithDecimalSeparator } from "../../../constants/constant";
import { listOrderDetailbyID, listOrderbyAdmin } from "../../../api/apiClient/Order/Order";
import { Modal } from "react-bootstrap";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const initialFieldValues = {
  name: '',
  title: '',
  orderCode: '',
  createDate: '',
  updateDate: '',
  studentName: '',
  employeeName: '',
  note: '',
}
const Invoice = () => {
  const [order, setOrder] = useState([]);
  const [values, setValues] = useState(initialFieldValues);
  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, SetEndDate] = useState(new Date());

 
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    const getOrderAdmin = async () => {
      try {
        const data = await listOrderbyAdmin();
        if (data.result.length > 0) {
          data.result = data.result.map((item) => {
            item.createDate = convertDateFormat(item.createDate);
            item.updateDate = convertDateFormat(item.updateDate);
            item.totalCourse = formatNumberWithDecimalSeparator(item.totalCourse) + "VNĐ"
            return item;
          });
          setOrder(data.result)
          setTimeout(() => {
          apiRef.current.setPageSize(7);
          })
        } else { }
      } catch (error) { }
    }
    getOrderAdmin();


  }, [apiRef])

  const dataWithOrder = order.map((item, index) => ({
    ...item,
    OrderID: index + 1,
  }))
  const totalRevenue = dataWithOrder
  .filter((item) => item.status === 1)
  .reduce((total, item) => {
    const numericValue = parseFloat(item.totalCourse.replace(',', '').replace('.', '').replace(',', '.'));
    return total + (isNaN(numericValue) ? 0 : numericValue);
  }, 0);

const roundedTotalRevenue = parseFloat(totalRevenue.toFixed(0));
  const handleDetail = async (id) => {
    try {
      const data = await listOrderDetailbyID(id)
      const result = data.result;
      setValues({
        name: result.name,
        title: result.title,
        orderCode: result.orderCode,
        createDate: convertDateFormat(result.createDate),
        updateDate: convertDateFormat(result.updateDate),
        studentName: result.studentName,
        employeeName: result.employeeName,
        note: result.note,

      });
      setShow(true);
    } catch (error) {
    }
  }
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const columns = [
    {
      headerName: "STT",
      field: "OrderID",
      flex: 0.5,
      align:'center'
    },
    {
      field: "name",
      headerName: "Tên hóa đơn",
      headerAlign: "left",
      align: "left",
      flex: 1,
      cellClassName: "name-column--cell",
      renderCell: (params) => (
        <a style={{ textDecoration: 'none', cursor: 'pointer',margin:'0' }} onClick={() => handleDetail([params.row.id])}>
          {params.row.name}
        </a>
      )
    },
    {
      field: "orderCode",
      headerName: "Mã hóa đơn",
    
      flex: 1,
    },

    {
      field: "studentName",
      headerName: "Học sinh",
    
      flex: 1,
    },
    {
      field: "employeeName",
      headerName: "Người tạo",
      flex: 1,
    },
    {
      field: "createDate",
      headerName: "Ngày tạo",
      align:'center',
      flex: 1,
    },
    {
      field: "totalCourse",
      headerName: "Tổng giá",
      align:'center',
      flex: 1,
      renderCell: (params) => {
        if (params.row.status === 0) {
          return (
            <div style={{ color: 'red' }}>
              Đang chờ thanh toán
            </div>
          );
        } else {
          return `${params.row.totalCourse}`;
        }
      },
    },
    {
      field: "",
      headerName: "Xem danh sách",
      flex: 0.75,
      cellClassName: "name-column--cell",
      renderCell: (params) => (
        params.row.status === 0 ? (
          <Link style={{ textDecoration: 'none' }} to={`/orderdetail/${params.row.id}`}>
            <ViewListIcon />
          </Link>
        ) :
          <Link style={{ textDecoration: 'none' }} to={`/orderdetail/${params.row.id}`}>
            <RemoveRedEyeIcon />
          </Link>
      )
    },
  ];

  const filterbyDate = async (e) => {
    e.preventDefault();
    try {
      const data = await listOrderbyAdmin();
      if (data.result.length > 0) {

        const filteredData = data.result.filter((item) => {
          const orderDate = new Date(item.createDate);
          return orderDate >= startDate && orderDate <= endDate;
        });

        const convertedData = filteredData.map((item) => ({
          ...item,
          createDate: convertDateFormat(item.createDate),
          updateDate: convertDateFormat(item.updateDate),
          totalCourse:formatNumberWithDecimalSeparator(item.totalCourse) + "VNĐ"
        }));
        setOrder(convertedData);
        apiRef.current.setPageSize(7);
      } else { }
    } catch (error) { }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Box m="20px">
                <Header title="Hóa đơn" subtitle="Quản lý hóa đơn" />
                <ToastContainer />
                <Box>
                  <form onSubmit={(e) => filterbyDate(e)}>
                    <Box></Box>
                    <div className="col-xl-12">
                      <div className="form-group m-form__group row align-items-center">
                        <div className="col-md-3">
                          <Link to='/order' class="btn btn-success m-btn ">
                            <i class="fa fa-plus"></i> Thêm mới
                          </Link>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }} className="col-md-2">
                        <label style={{ marginRight: '10px' }}>Từ</label>
                          <DatePicker
                            selected={startDate} onChange={(date) => setStartDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                            showYearDropdown
                            scrollableYearDropdown
                            yearDropdownItemNumber={25}
                          />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }} className="col-md-2">
                          <label style={{ marginRight: '10px' }}>Đến</label>
                          <DatePicker
                            selected={endDate} onChange={(date) => SetEndDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                            showYearDropdown
                            scrollableYearDropdown
                            yearDropdownItemNumber={25}
                          />
                        </div>
                        <div className="col-md-2">
                          <div className="select-student">
                            <button style={{ display: 'flex', background: ' #2746C0', padding: '10px 15px' }} type="submit" className="attended"> <span><i className="fa fa-filter"></i> </span>Lọc</button>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <form className="filter-search">
                            <input
                              type="text"
                              className="form-control m-input"
                              placeholder="Tìm kiếm..."
                            />
                          </form>
                        </div>
                      </div>
                    </div>
                  </form>
                </Box>
                <Box  m={3}>
                  <div className="col-md-12 invoice-header">
                    <div className="row">
                  
                        <div className="col-lg-4" align="center">
                          <strong className="m--font-info">
                            <span>Số hóa đơn:</span>
                            <span className="number_bill">{order.length}</span>
                          </strong>
                        </div>
                        <div className="col-lg-4" align="center">
                          <strong className="m--font-primary">
                            <span>Tiền thu được</span>
                            <span className="money_total">{formatNumberWithDecimalSeparator(roundedTotalRevenue)}VNĐ</span>
                          </strong>
                        </div>
                        <div className="col-lg-4" align="center">
                          <strong className="m--font-info">
                            <span>Tổng số khóa học:</span>
                            <span className="money_contract">0 </span>
                          </strong>
                        </div>
                      </div>
                  
                 

                  </div>
                </Box>
                <Box
                  m="40px 0 0 0"
                  maxHeight="80vh"
                  sx={{
                    "& .MuiDataGrid-root": {
                      border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                      borderBottom: "none",
                      borderRight: "0.5px solid #E0E0E0 !important"
                    },
                    "& .name-column--cell": {
                      color: colors.greenAccent[300],
                    },
                    "& .MuiDataGrid-columnHeaderTitle":{
                      color:"white"
                    },
                    "& .MuiDataGrid-columnHeaders": {
                      backgroundColor: "#2746C0",
                      borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                      backgroundColor: colors.primary[400],
                    },
                    "& .MuiDataGrid-footerContainer": {
                      borderTop: "none",
                      backgroundColor: "#2746C0",
                    },
                    "& .MuiCheckbox-root": {
                      color: `${colors.greenAccent[200]} !important`,
                    },
                  }}
                >
                  {dataWithOrder.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20, 25]} apiRef={apiRef} rows={dataWithOrder} columns={columns} />
                  )}

                </Box>
              </Box>
              <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Chi tiết hóa đơn</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <label className="form-label">Tên hóa đơn</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='name'
                    value={values.name}
                    required
                  />
                  <label className="form-label">Mã hóa đơn</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='orderCode'
                    value={values.orderCode}
                    required
                  />
                  <label className="form-label">Tiêu đề</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='title'
                    value={values.title}
                    required
                  />
                  <label className="form-label">Học sinh</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='studentName'
                    value={values.studentName}
                    required
                  />
                  <label className="form-label">Tên quản trị</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='employeeName'
                    value={values.employeeName}
                    required
                  />
                  <label className="form-label">Ghi chú</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='note'
                    value={values.note}
                    required
                  />
                  <label className="form-label">Ngày tạo</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='createDate'
                    value={values.createDate}
                    required
                  />
                  <label className="form-label">Ngày cập nhật</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='updateDate'
                    value={values.updateDate}
                    required
                  />
                </Modal.Body>


              </Modal>
            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default Invoice;
