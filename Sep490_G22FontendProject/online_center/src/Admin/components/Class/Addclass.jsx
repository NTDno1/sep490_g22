import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify"
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider, Tooltip } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { getAllCourseHome, getAllViewCourse } from '../../../api/apiClient/Courses/CoursesAPI';
import { GetTeacherAdminSide } from '../../../api/apiClient/Teacher/teacherAPI';
import { AddNewClass } from '../../../api/apiClient/Class/ClassAPI';
import { convertDateFormattoAm, convertDatePicker, convertYearMonthDay, convertoDate, formatDateString, generateRandomCode } from '../../../constants/constant';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import FooterAdmin from '../../FooterAdmin';
import ReplayIcon from '@mui/icons-material/Replay';
import Header from '../Header';
const initialFieldValues = {
  courceId: '',
  name: '',
  code: '',
  teacherId: '',
  slotnumber: 0,
  startDate: formatDateString(new Date())
}
export default function AddClass() {
  const [values, setValues] = useState(initialFieldValues);
  const [course, setCourse] = useState([]);
  const [teacher, setTeacher] = useState([]);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');


  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);

  useEffect(() => {
    fetchDataCourse();
    fetchDataTeacher();
  }, []);
  
  const fetchDataCourse = async () => {
    try {
      const data = await getAllViewCourse();
      if (data && data.result && data.result.length > 0) {
        setCourse(data.result);
  
        // Gán giá trị đầu tiên của mảng vào courceId
        setValues((prevValues) => ({
          ...prevValues,
          courceId: data.result[0].id,
        }));
      }
    } catch (error) {
      console.error("Lỗi khi lấy dữ liệu khóa học:", error.message);
    }
  };
  
  const fetchDataTeacher = async () => {
    try {
      const data = await GetTeacherAdminSide();
      if (data && data.result && data.result.length > 0) {
        const checkdata = data.result.filter(item => item.status === 1 || item.status === 0);
        setTeacher(checkdata);
  
        // Gán giá trị đầu tiên của mảng vào teacherId
        setValues((prevValues) => ({
          ...prevValues,
          teacherId: data.result[0].id,
        }));
      }
    } catch (error) {
      console.error("Lỗi khi lấy dữ liệu giáo viên:", error.message);
    }
  };

  let classId = 0;
  const addOrEdit = async(formData) => {
    try {
      const response = await AddNewClass(formData);
      classId = response.result.id;
      toast.success('Thành công');
      navigate(`/addclassschedule/${classId}`);
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }    
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('name', values.name)
    formData.append('code', values.code)
    formData.append('courceId', values.courceId)
    formData.append('teacherId', values.teacherId)
    formData.append('slotnumber', values.slotnumber)
    formData.append('startDate', convertoDate(values.startDate))
    addOrEdit(formData)
  }

  

  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    })
  }
  const generateCode = () => {
    const genCode = generateRandomCode();
    setValues({
      ...values,
      code: genCode
    }) 
    
  }
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
              <Header title="Lớp Học" subtitle="Thêm lớp học" />
              <ToastContainer />
                <div className="row">
                  <div className="col-12">
                    {/* Page title */}
                    <div className="col-12 my-5">
                      <form id="form">
                        <ul id="progressbar">
                          <li class="active" id="step1">
                            <strong className='class-progress'><i class="fa fa-users"></i></strong>
                          </li>
                          <li id="step2"><strong className='calender-progress'><i class="fa fa-calendar-plus"></i></strong></li>
                          <li id="step3"><strong className='user-progress'><i class="fa fa-user-plus"></i></strong></li>
                        </ul>
                      </form>
                      <hr />
                    </div>

                    {/* Form START */}
                    <form onSubmit={handleFormSubmit}>

                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              {/* First Name */}
                              <div className="col-md-6">
                                <label className="form-label">Trung tâm <span className='required-field'>*</span></label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  value={centername}  disabled

                                />
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Mã lớp học  <span className='required-field'>*</span></label>
                                <div style={{position:'relative',display: 'flex' }}>
                                  <input 
                                    value={values.code}
                                    type="text"
                                    className="form-control"
                                    placeholder=""
                                    name='code'
                                    aria-label="Last name"
                                    required
                                    onChange={handleInputChange}
                                  />
                                 <Tooltip title="Tự tạo" placement='top'>
                                  <a className='replayCode'  onClick={() => generateCode()}><ReplayIcon/></a>
                                  </Tooltip>      
                                   </div>
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Tên lớp học  <span className='required-field'>*</span></label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='name'
                                  value={values.name}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              {/* Last name */}
                              <div className="col-md-6">
                                <label className="form-label">Khóa học  <span className='required-field'>*</span></label>
                                <select
                                  className="form-control"
                                  name="courceId"
                                  value={values.courceId}
                                  onChange={handleInputChange}
                                >                              
                                  {course?.map((option) => (
                                    <option key={option.id} value={option.id}>
                                      {option.name} 
                                    </option>
                                  ))}
                                </select>
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Giáo viên  <span className='required-field'>*</span></label>
                                <select
                                  className="form-control"
                                  name="teacherId"
                                  value={values.teacherId}
                                  onChange={handleInputChange}
                                >
                             
                                  {teacher?.map((option) => (
                                    <option key={option.id} value={option.id}>
                                      {option.firstName} 
                                    </option>
                                  ))}
                                </select>
                              </div>
                              {/* Phone number */}
                             
                              {/* Mobile number */}
                           
                              <div className="col-md-6">
                                <label className="form-label">Số buổi học  <span className='required-field'>*</span></label>
                                <input
                                  type="number"
                                  className="form-control"
                                  placeholder=""
                                  name='slotnumber'
                                  value={values.slotnumber}
                                  onChange={handleInputChange}
                                  min={1}
                                  required
                                />
                              </div>

                              <div className="col-md-6">
                                <label className="form-label">Ngày bắt đầu</label>
                                <DatePicker
                        value={(values.startDate)}
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                        required
                        showMonthDropdown
                        showYearDropdown
                        scrollableYearDropdown
                        yearDropdownItemNumber={25}
                        onChange={(date) => {
                          const selectedDate = new Date(date);                                                   
                            setValues({
                              ...values,
                              startDate: formatDateString(selectedDate),
                            });                      
                        }}            
                      />
                              </div>
                            </div>{" "}

                          </div>
                        </div>

                      </div>{" "}


                      <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
                      <a className='backtolist' href='/classadmin'> {'<< Quay về danh sách'}</a>
                      <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
           <i className="addicon fa fa-save"></i>Lưu
         </button>
                        
                      </div>

                    </form>
                    {/* Form END */}
                  </div>


                </div>
              </div>
         
            </main>
          </Box>
          <FooterAdmin/>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
