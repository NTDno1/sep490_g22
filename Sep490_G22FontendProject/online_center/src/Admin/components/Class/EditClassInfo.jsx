
import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { toast, ToastContainer } from 'react-toastify';
import { ClassDetail, UpdateInfoClass } from '../../../api/apiClient/Class/ClassAPI';
import { getAllViewCourse } from '../../../api/apiClient/Courses/CoursesAPI';
import { GetTeacherAdminSide } from '../../../api/apiClient/Teacher/teacherAPI';
import { convertDateFormat, convertDatePicker, convertYearMonthDay, convertoDate, formatDateString, generateRandomCode } from '../../../constants/constant';
import FooterAdmin from '../../FooterAdmin';
import Header from '../Header';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const initialFieldValues = {
  id: 0,
  courceId: 0,
  name: '',
  code: '',
  teacherId: '',
  slotnumber: 0,
  startDate: '',
  dateUpdate: '',
}

export default function EditClassInfo() {
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [values, setValues] = useState(initialFieldValues);
  const [course, setCourse] = useState([]);
  const [teacher, setTeacher] = useState([]);
  const { newclassId } = useParams();
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    getInfoClass();
    getListCourse();
    getListTeacher();

  }, [])
  const getInfoClass = async () => {
    try{
    const data = await ClassDetail(newclassId);
    console.log(data)
    setValues({
      courceId: data.result.courceId,
      name: data.result.name,
      code: data.result.code,
      teacherId: data.result.teacherId,
      slotnumber: data.result.slotNumber,
      startDate: convertDateFormat(data.result.startDate),
      dateUpdate: data.result.dateUpdate
    });
  }catch(error){}
  }
  const getListCourse = async () => {
    const data = await getAllViewCourse();
    console.log(data)
    setCourse(data.result);

  }
  const getListTeacher = async () => {
    const data = await GetTeacherAdminSide();
    setTeacher(data.result)
  }




  let classId = 0;
  const addOrEdit = async (formData) => {

    try {

      const response = await UpdateInfoClass(formData);

      toast.success("Update Success");
      classId = response.result.id;
      localStorage.setItem('classId', classId);
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          const errorMessage = errorMessages[0];
          toast.error(errorMessage);
          console.error('Error when send data:', error);
        }
      }
    }
  }

  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('id', newclassId)
    formData.append('name', values.name)
    formData.append('code', values.code)
    formData.append('courceId', values.courceId)
    formData.append('teacherId', values.teacherId)
    formData.append('slotnumber', values.slotnumber)
    formData.append('startDate', convertoDate(values.startDate))
    addOrEdit(formData)
  }

  const currentDate = new Date().toISOString().split('T')[0];
  const handleInputChange = e => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value
    })
  }
  const generateCode = () => {
    const genCode = generateRandomCode();
    setValues({
      ...values,
      code: genCode
    })

  }

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <ToastContainer />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <Header title="Lớp Học" subtitle="Cập nhật lớp học" />
                <div className="row">
                  <div className="col-12">
                    {/* Page title */}
                    <div className="col-12 my-5">
                      <form id="form">
                        <ul id="progressbar">
                          <li class="active" id="step1">
                            <strong className='class-progress'><i class="fa fa-users"></i></strong>

                          </li>
                          <li onClick={() => { navigate(`/addclassschedule/${newclassId}`); }} id="step2"><strong className='calender-progress'><i class="fa fa-calendar-plus"></i></strong></li>
                          <li onClick={() => { navigate(`/addclasssmember/${newclassId}`); }} id="step3"><strong className='user-progress'><i class="fa fa-user-plus"></i></strong></li>
                        </ul>
                      </form>
                      <hr />
                    </div>

                    {/* Form START */}
                    <form onSubmit={handleFormSubmit}>

                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              {/* First Name */}
                              <div className="col-md-6">
                                <label className="form-label">Trung tâm</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  value={centername} disabled

                                />
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Tên lớp học *</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='name'
                                  value={values.name}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              {/* Last name */}
                              <div className="col-md-6">
                                <label className="form-label">Khóa học *</label>
                                <select
                                  className="form-control"
                                  name="courceId"
                                  value={values.courceId}
                                  onChange={handleInputChange}
                                >

                                  {course?.map((option) => (
                                    <option key={option.id} value={option.id}>
                                      {option.name}
                                    </option>
                                  ))}
                                </select>
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Giáo viên *</label>
                                <select
                                  className="form-control"
                                  name="teacherId"
                                  value={values.teacherId}
                                  onChange={handleInputChange}
                                >

                                  {teacher?.map((option) => (
                                    <option key={option.id} value={option.id}>
                                     {option.firstName} 
                                    </option>
                                  ))}
                                </select>
                              </div>
                              {/* Phone number */}

                              {/* Mobile number */}

                              <div className="col-md-6">
                                <label className="form-label">Số buổi học *</label>
                                <input
                                  type="number"
                                  className="form-control"
                                  placeholder=""
                                  name='slotnumber'
                                  value={values.slotnumber}
                                  onChange={handleInputChange}
                                  min={0}
                                  required
                                />
                              </div>

                              <div className="col-md-6">
                                <label className="form-label">Ngày bắt đầu</label>
                                <DatePicker
                                  value={(values.startDate)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  required
                                  showMonthDropdown
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                    setValues({
                                      ...values,
                                      startDate: formatDateString(selectedDate),
                                    });
                                  }}
                                />
                              </div>
                            </div>{" "}

                          </div>
                        </div>

                      </div>{" "}

                      {newclassId ? (
                        <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                          <a className='backtolist' href='/classadmin'> {'<< Quay về danh sách'}</a>
                          <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                            <i className="addicon fa fa-save"></i>Lưu
                          </button>
                        </div>
                      ) : (
                        <div className="col-md-12" align="right">
                          <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                            <i className="addicon fa fa-save"></i>Lưu & Tiếp tục
                          </button>
                        </div>
                      )}
                    </form>
                    {/* Form END */}
                  </div>


                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )


}
