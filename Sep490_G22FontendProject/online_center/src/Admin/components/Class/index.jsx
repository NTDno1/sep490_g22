import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import { useEffect, useState } from "react";
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import { Link, useNavigate } from "react-router-dom";
import { clean } from 'diacritic';
import Cookies from "js-cookie";
import { ToastContainer, toast } from "react-toastify";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { CloseClass, DeleteClass, OpenClass, getAllClassAdminSide } from "../../../api/apiClient/Class/ClassAPI";
import FooterAdmin from "../../FooterAdmin";
import { convertDateFormat } from "../../../constants/constant";
const Class = () => {
  const [course, setCourse] = useState([]);
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchClass, setSearchClass] = useState("");
  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
  
    getClassAdmin();


  }, [apiRef])
  const getClassAdmin = async () => {
    try {
      const data = await getAllClassAdminSide();
      if (data.result.length > 0) {
        data.result = data.result.map((item) => {
          item.startDate = convertDateFormat(item.startDate);
          return item;
        });
        setOriginalClasses(data.result)
        setCourse(data.result)

        apiRef.current.setPageSize(7);


      } else { }
    } catch (error) { }
  }
  const dataWithClass = course.map((item, index) => ({
    ...item,
    ClassID: index + 1,
  }))


  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa lớp học này không ?")) {
      try{
      const response = await DeleteClass(id)
      console.log(response)
      if (response.statusCode === 0) {
        toast.success('Thành công')
        getClassAdmin();
      } else {
        console.error('Failed to delete resource');
      }

    } catch(error){
      toast.error("Có lỗi xảy ra")
    }
  }
    
  };
  const handleCloseClass = async (id) => {

    if (window.confirm("Bạn có muốn đóng lớp học này không ?")) {

      try {
        const response = await CloseClass(id);
        toast.success("Đóng thành công");
        getClassAdmin();
      }
      catch (error) {
        toast.error(error.response.data.errorMessages[0]);
      }

    }
  }
  // const handleOpenClass = async (id) => {
  //   if (window.confirm("Bạn có muốn mở lớp học này không ?")) {
  //     try {
  //       const response = await OpenClass(id);
  //       toast.success("Mở thành công");
  //       window.location.reload();
  //     }
  //     catch (error) {
  //       toast.error(error.response.data.errorMessages[0]);
  //     }

  //   }
  // }
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const columns = [

    {
      headerName: "STT",
      field: "ClassID",
      flex: 0.5,
      align: 'center'
    },
    {
      field: "name",
      headerName: "Lớp học",
      align: "left",
      flex: 1,

      renderCell: (params) => (
        <a style={{ textDecoration: 'none', margin: 0 }} href={`/coursesadmin/${params.row.id}`}>
          {params.row.name}
        </a>
      )
    },
    {
      field: "teacherName",
      headerName: "Giáo viên",
      headerAlign: "left",
      align: "left",
      flex: 1,
    },
    {
      field: "studentNumber",
      headerName: "Học viên",
      align: 'center',
      flex: 0.5,
    },
    {
      field: "startDate",
      headerName: "Thời gian",
      align: "center",
      flex: 0.5,
    },
    {
      field: "slotNumber",
      headerName: "Số buổi",
      align: 'center',
      flex: 0.5,
    },
    {
      field: "Status",
      headerName: "Trạng thái",
      renderCell: (params) => (
        params.row.status === 0 ? (
          <div style={{ cursor: 'pointer' }}>
            Chưa mở
          </div>
        ) : params.row.status === 1 ? (
          <div style={{ cursor: 'pointer', color: 'green' }}>
            Trong quá trình
          </div>
        ) : params.row.status === 2 ? (
          <div style={{ cursor: 'pointer', color: 'red' }}>
            Đã kết thúc
          </div>
        ) :
          <div style={{ cursor: 'pointer' }}>
            Đã đóng
          </div>
      ),
      flex: 0.5,
    },
    {
      field: "Action",
      headerName: "Hành động",
      flex: 0.5,
      renderCell: (params) => (
        params.row.status === 0 ? (
          <UilLock
            style={{ cursor: 'pointer', color: 'gray' }}

          />
        ) :
          params.row.status === 1 ? (
            <UilUnlock
              style={{ cursor: 'pointer', color: 'green' }}
              onClick={() => handleCloseClass([params.row.id])}
            />
          ) : params.row.status === 2 ? (
            <UilLock
              style={{ cursor: 'pointer', color: 'red' }}
            />
          ) : params.row.status === 3 ? (
            <UilLock
              style={{ cursor: 'pointer', color: '#696969' }}
            />
          ) : null
      )
    },
    {
      headerName: "Chỉnh sửa",
      flex: 0.5,
      renderCell: (params) => (
        <div style={{ display: 'flex', alignItems: 'center', marginLeft:'10px' }}>
          {params.row.status === 0 && (
            <DeleteOutlineIcon
              style={{ cursor: 'pointer', fontSize: '25px' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
          )}
         
         <Link style={{ cursor: 'pointer' }} to={`/classadmin/editclass/${params.row.id}`}>
              <UilEdit style={{ marginLeft: '5px' }} />
            </Link>
         
        </div>
      ),
    }


  ];
  const filterNameClass = (event) => {
    event.preventDefault();
    if (searchClass) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.name.toLowerCase()).includes(clean(searchClass.toLowerCase()))
      );
      setCourse(filteredClasses);
    } else {
      setCourse(originalClasses);
    }
  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Box m="20px">
                <Header title="Lớp Học" subtitle="Quản lý lớp học" />
                <ToastContainer />
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Link to='./addclass' class="btn btn-success m-btn ">
                      <i class="fa fa-plus"></i> Thêm mới
                    </Link>
                  </Box>

                  <form className="filter-search" onSubmit={filterNameClass}>
                    <input
                      type="text"
                      className="form-control m-input"
                      placeholder="Tìm kiếm tên lớp học..."
                      onChange={(e) => setSearchClass(e.target.value)}
                      value={searchClass}
                    />
                  </form>

                </Box>
                <Box
                  m="40px 0 0 0"
                  maxHeight="80vh"
                  sx={{
                    "& .MuiDataGrid-root": {
                      border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                      borderBottom: "none",
                      borderRight: "0.5px solid #E0E0E0 !important"
                    },
                    "& .name-column--cell": {
                      color: colors.greenAccent[300],
                    },
                    "& .MuiDataGrid-columnHeaderTitle":{
                      color:"white"
                    },
                    "& .MuiDataGrid-columnHeaders": {
                      backgroundColor: "#2746C0",
                      borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                      backgroundColor: colors.primary[400],
                    },
                    "& .MuiDataGrid-footerContainer": {
                      borderTop: "none",
                      backgroundColor: "#2746C0",
                    },
                    "& .MuiCheckbox-root": {
                      color: `${colors.greenAccent[200]} !important`,
                    },
                  }}
                >
                  {dataWithClass.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20, 25]} apiRef={apiRef} rows={dataWithClass} columns={columns} />
                  )}

                </Box>
              </Box>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default Class;
