
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useEffect, useState } from "react";
import Cookies from 'js-cookie'
import fetchData from '../../../api/apiUtils'

import NotificationsIcon from '@mui/icons-material/Notifications';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { viewSyllabus } from '../../../api/apiClient/Courses/CoursesAPI'
import FooterAdmin from '../../FooterAdmin'
const AdminClassInfoSyllabus = () => {
  const [classinfoSyllabus, setClassInfoSyllabus] = useState({});
  const { id } = useParams();
  
  const [className, setClassName] = useState('');
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);
  function splitAndRenderDescription(description) {
    return description.split('_').map((item, index) => {
      return <div style={{ textAlign: 'left' }} key={index}>{item.trim()}</div>;
    });
  }

  const jwttoken = Cookies.get('jwttoken');
      const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };
      
        checkTokenBeforeRoute();
      }, [jwttoken]);
      useEffect(() => {
        getSyllabus();
      }, [id]);
      const getSyllabus = async () => {
        const data = await viewSyllabus(id);
        if (data.result.description !== null && data.result.studentTasks !== null && data.result.exam !== null) {
        data.result.description = splitAndRenderDescription(data.result.description);
        data.result.studentTasks = splitAndRenderDescription(data.result.studentTasks);
        data.result.exam = splitAndRenderDescription(data.result.exam);
        setClassInfoSyllabus(data.result);
        setClassName(data.result.className);
        }
      }
 
  return (
    <>
   <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
      <div className="container">

        <div className="row">

          <div className="col-12">

            {/* Page title */}
            <div className="col-12 my-5">
            
        
              <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
              <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

                <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                  <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class'  ><FontAwesomeIcon className='iconcourse-active' icon={faFileLines} /><span class="hover-text">Khung chương trình</span></Link>
                  </div>
                 <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>    <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                  </div>
                </div>
              </div>
              <hr />
          
            {/* Form START */}

            <h1 style={{ margin: '3%', marginLeft: '2%' }}>Thông tin giáo trình</h1>
            <table style={{ border: "1px solid #ccc" }}>
              <tbody>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right">
                    ID
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSyllabusID">{classinfoSyllabus.id}</span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style3" align="right" >
                    Tên bài học
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style4">
                    <span id="lblSyllabusName" style={{ fontWeight: "bold" }}>
                      {classinfoSyllabus.syllabusName}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style3" align="right" >
                   Nội dung bài học
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style4">
                    <span id="lblSyllabusNameEnglish">
                      {(classinfoSyllabus.description)}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Nhiệm vụ
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {(classinfoSyllabus.studentTasks)}
                    </span>
                  </td>
                </tr>

                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Bài thi
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.exam}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Chủ đề chính
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.mainTopics}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Kiến thức cần đạt
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.requiredKnowledge}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Phân bổ thời gian
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.timeAllocation}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                    Liên hệ
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {(classinfoSyllabus.contact)}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                  Kết quả khóa học
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.courseLearningOutcome}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                   Mục tiêu khóa học
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      {classinfoSyllabus.courseObjectives}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                  Link Sách
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                    <a href={classinfoSyllabus.linkBook}>{classinfoSyllabus.linkBook}</a>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                 Link Tài liệu
                  </td>
                  <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                    <span id="lblSubjectCode" >
                      <a href={classinfoSyllabus.linkDocument}>{classinfoSyllabus.linkDocument}</a>
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
            </div>

            {/* Form END */}
          </div>
        </div>
      </div>
  
      </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
    </>

  );
};

export default AdminClassInfoSyllabus;
