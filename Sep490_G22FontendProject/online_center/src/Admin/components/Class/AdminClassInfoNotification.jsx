import React, { useEffect, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import NotificationsIcon from '@mui/icons-material/Notifications';
import Cookies from 'js-cookie'

import { ToastContainer, toast } from "react-toastify";

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";

import { AddNotificationClass } from '../../../api/apiClient/Class/ClassAPI'
import FooterAdmin from '../../FooterAdmin'
const initialFieldValues = {
    title: '',
    description:'',
    classId:''
}

export default function AdminClassInfoNotification() {
    const { id } = useParams();
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    
      const [values, setValues] = useState(initialFieldValues);
  
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };
      
        checkTokenBeforeRoute();
      }, [jwttoken]);

  const handleInputChange = e => {
    const { name, value } = e.target; 
    setValues({
      ...values,
      [name]: value
    })
  }
  const addOrEdit = async(formData) => {
    try {
      const response = await AddNotificationClass(formData);
      toast.success("Thêm thông báo thành công");
      setTimeout(() => {
      window.location.reload();
      },1000)
    }
    catch(error) {
      // Xử lý khi gửi thất bại
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          const errorMessage = errorMessages[0];
          toast.error(errorMessage);
          console.error('Error when send data:', error);
        }
      }
    }
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('title', values.title)
    formData.append('description', values.description)
    formData.append('classId', id)
    addOrEdit(formData)
  }

  return (
    <>
      <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
    <div className="container">
    <div className="row">
        <ToastContainer/>
        <div className="col-12">
            {/* Page title */}
            <div className="col-12 my-5">
       
               
                    <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                        <h3 style={{ flex: '1' }}></h3>
                        <div style={{ flex: '2' }}></div>

                        <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                            <div class="icon-container">
                                <Link className='icon-class' to={`/../coursesadmin/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                            </div>
                            <div class="icon-container">
                                <Link className='icon-class' to={`/../coursesadmin/calender/${id}`}><i class=" iconcourse fa fa-calendar"></i>  <span class="hover-text">Lịch học</span> </Link>
                            </div>
                            <div class="icon-container">
                                <Link className='icon-class' to={`/../coursesadmin/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Khung chương trình</span></Link>
                            </div>
                           <div class="icon-container">
                                <Link className='icon-class' to={`/../coursesadmin/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                            </div>
                            <div class="icon-container">
                                <Link className='icon-class' to={`/../coursesadmin/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                            </div>
                            <div class="icon-container">
                       <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>    <i class="iconcourse-active fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                            </div>
                        </div>
                    </div>
                    <hr />
          
                    {/* Form START */}
                 
                
            </div>
            <div className="container">
      <div className="row">
        <div className="col-12">
          {/* Page title */}
         

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>
        
            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                  
                    <div className="col-md-12">
                      <label className="form-label">Tiêu đề</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='title'
                        value={values.title}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
          
                    <div className="col-md-12">
                      <label className="form-label">Mô tả</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='description'
                        value={values.description}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  </div>{" "}
             
                </div>
              </div>

            </div>{" "}

      
  <div className="col-md-12" align="right">
  <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                            <i className="addicon fa fa-save"></i>Lưu
                          </button>
  </div>

          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
            {/* Form END */}
        </div>
    </div>
</div>

</main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
</>
  )
}
