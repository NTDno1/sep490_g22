import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import {
  UilTimesCircle,
} from "@iconscout/react-unicons";
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddClassSlotDate, ClassRoom, ClassSlotDate, DeleteClassSlotDate, GenerateSchedual, SlotDate } from '../../../api/apiClient/Class/ClassAPI';
import FooterAdmin from '../../FooterAdmin';


export default function AddClassSchedule() {
  const navigate = useNavigate();
  const [numberSlot, setNumberSlot] = useState();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [classlotDate, setClassslotDate] = useState([]);
  const [slot, setSlot] = useState([]);
  const [room, setRoom] = useState([]);

  const { classId } = useParams();

  const [selectedSlotDateId, setSlotDateId] = useState("");
  const [selectedRoomIds, setSelectedRoomId] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
     
      navigate(`/addclasssmember/${classId}`);
    }
    catch (error) {
      toast.error(error.response.data.errorMessages[0]);
    }
  }
  let jwttoken = Cookies.get('jwttoken');
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);

  useEffect(() => {
    fetchDataClassSlotDate();
    fetchDataSlotDate();
    fetchDataClassRoom();
  }, [])

  const fetchDataClassSlotDate = async () => {
    const data = await ClassSlotDate(classId);
    if (data.result.length) {
      setClassslotDate(data.result);
      setNumberSlot(data.result.slotNumber);
      
    }
  };

  const fetchDataSlotDate = async () => {
    const data = await SlotDate();
    if (data.result.length) {

      setSlot(data.result);
      setSlotDateId(data.result[0].id);

    }
  };

  const fetchDataClassRoom = async () => {
    const data = await ClassRoom();
    if (data.result.length) {
      setRoom(data.result);
      setSelectedRoomId(data.result[0].id);
    }
  };


  const addOrEdit = async (formData) => {
    try {
      const response =   await AddClassSlotDate(formData)
      toast.success('Thành công');
      fetchDataClassSlotDate();
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  };


  const handleSubmitSlotClass = (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('classId', classId)
    formData.append('slotId', selectedSlotDateId)
    formData.append('roomIds', selectedRoomIds)

    addOrEdit(formData)
  }
  const removeSlotDate = async (id) => {
    if (window.confirm("Bạn có chắc muốn xóa mục này không")) {
      try {
        const response = await DeleteClassSlotDate(id)
        toast.success('Thành công')
        fetchDataClassSlotDate();
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      }
    
    }
  }

  const handleInputChangeSlotDate = e => {
    const selectedSlotDateId = e.target.value;
    setSlotDateId(selectedSlotDateId);
  }
  const handleInputChangeRoom = e => {
    const selectedChooseRoomIds = e.target.value;
    setSelectedRoomId(selectedChooseRoomIds); // Use the correct function name here
  }

  return (
    <>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <div className="app">
            <Box style={{ display: 'flex' }}>
              <Sidebars isSidebar={isSidebar} />
              <main className="content" >
                <Topbar setIsSidebar={setIsSidebar} />

                <div className="container">
                  <div className="row">
                    <ToastContainer />
                    <div className="col-12">
                      {/* Page title */}
                      <div className="col-12 my-5">
                        <form id="form">
                          <ul id="progressbar">
                            <li onClick={() => { navigate(`/classadmin/editclass/${classId}`); }} className='active' id="step1"><strong className='class-progress'><i class="fa fa-users"></i></strong></li>
                            <li className='active' id="step2"><strong className='calender-progress'><i class="fa fa-calendar-plus"></i></strong></li>
                            <li onClick={() => { navigate(`/addclasssmember/${classId}`); }} id="step3"><strong className='user-progress'><i class="fa fa-user-plus"></i></strong></li>
                          </ul>
                        </form>
                        <hr />
                      </div>
                      <form onSubmit={handleFormSubmit}>
                        {/* Form START */}
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                          <div>
                          </div>
                          <button style={{  marginBottom: '10px' }} type="button" onClick={handleShow} class="btn btn-success m-btn m-btn--custom m-btn--icon" >
                          <i class="fa fa-plus"></i> Thêm mới   
                            
                          </button>
                        </div>
                        <div className="table-responsive" id="table-content">
                          <table
                            className="table table-bordered table-hover"
                            id="class-table"
                            style={{ minHeight: "250px !important" }}
                          >
                            <thead>
                              <tr>

                                <th>STT</th>
                                <th>Ca học</th>
                                <th>Phòng học</th>
                                <th>Giáo viên</th>
                                <th style={{ width: "10%" }}>Hành động</th>
                              </tr>
                            </thead>
                            <tbody>
                              {classlotDate.map((val, index) => (
                                <tr>

                                  <td>

                                    <div>{index + 1}</div>
                                  </td>
                                  <td>
                                    <div>{val.timeStart}-{val.timeEnd} {val.day}</div>
                                  </td>
                                  <td>
                                    <span
                                    >
                                      {val.roomName}
                                    </span>
                                  </td>
                                  <td>{val.teacherName}</td>
                                  <td><a style={{ cursor: 'pointer' }} onClick={() => removeSlotDate(val.id)}><UilTimesCircle /></a></td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                        </div>

                        {!classlotDate.length == 0 && (
                          <div class="col-md-12" align="right">
                           <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
           <i className="addicon fa fa-save"></i>Lưu
         </button>
                          </div>
                        )}
                      </form>

                      {/* Form END */}
                    </div>

                  </div>
                </div>
                <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Tạo ca học</Modal.Title>
                  </Modal.Header>
                  <form onSubmit={handleSubmitSlotClass}>
                  <Modal.Body>
                    <label className="form-label">Ca học</label>
                    <select className="form-control"   name="slotId" onChange={handleInputChangeSlotDate}  value={selectedSlotDateId}>
                        {slot?.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.timeStart} - {option.timeEnd} {option.day}
                          </option>
                        ))}
                    </select>

                    <label className="form-label">Phòng học</label>
                    <select className="form-control"  name="roomIds"   onChange={handleInputChangeRoom} value={selectedRoomIds}>
                    {room?.map((option,index) => (
                          <option key={option.id} value={option.id}>
                            {option.name}
                          </option>
                        ))}
                    </select>

                  </Modal.Body>
                  <Modal.Footer>     
                    <button style={{backgroundColor:'#2746C0',color:'white'}} class="btn btn-success m-btn m-btn--custom m-btn--icon" variant="primary" onClick={handleClose}>
                      Lưu thay đổi
                    </button>
                  </Modal.Footer> 
                  </form>
                </Modal>
             
              </main>
       
            </Box>
            <FooterAdmin/>
          </div>
         
        </ThemeProvider>
      </ColorModeContext.Provider>
   
    </>
  )
}
