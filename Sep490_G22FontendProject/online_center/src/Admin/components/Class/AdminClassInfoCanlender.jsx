import React, { useEffect, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import NotificationsIcon from '@mui/icons-material/Notifications';

import Cookies from 'js-cookie'

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ClassDetail } from '../../../api/apiClient/Class/ClassAPI'
import FooterAdmin from '../../FooterAdmin'
  
export default function AdminClassInfoCalender() {
    const { id } = useParams();
    const [classinfoClendar, setClassInfoCalendar] = useState([]);
    const [teacher, setTeaher] = useState('');
    const [className, setClassName] = useState('');
    const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  

    const jwttoken = Cookies.get('jwttoken');
    const navigate = useNavigate();
    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login');
            }
        };

        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        getInfoClass();
    }, [id]);
    const getInfoClass = async() => {
    
        const data = await ClassDetail(id);
        if(data.result){
        data.result.startDate = data.result.startDate.toString().split('T')[0];
                setClassInfoCalendar(data.result.classSlotDates);
                setClassName(data.result.name);
                setTeaher(data.result.teacherName)
        }
      }

    return (
        <>
       <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content">
                  <Topbar setIsSidebar={setIsSidebar} />
            <div className="container">
                <div className="row">

                    <div className="col-12">
                        {/* Page title */}
                        <div className="col-12 my-5">
                        

                            <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                            <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

                                <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class'><i class=" iconcourse-active fa fa-calendar"></i>  <span class="hover-text">Lịch học</span> </Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Khung chương trình</span></Link>
                                    </div>
                                     <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                                    </div>
                                    <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>    <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                  </div>
                                </div>
                            </div>
                            <hr />

                            {/* Form START */}
                            <table id="keywords" cellspacing="0" cellpadding="0">
                                <thead >
                                    <tr>
                                        <th><span>ID</span></th>
                                        <th><span>Ca học</span></th>
                                        <th><span>Phòng học</span></th>
                                        <th><span>Giáo viên</span></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {classinfoClendar?.map((val, index) => (
                                        <tr>
                                            <td> <div>{index + 1}</div></td>
                                            <td> <span

                                            >
                                                {val.timeStart} - {val.timeEnd} {val.day}
                                            </span></td>
                                            <td>{val.roomName}</td>
                                            <td>     {teacher}</td>

                                        </tr>
                                    ))}
                                </tbody>
                            </table>

                        </div>

                        {/* Form END */}
                    </div>
                </div>
            </div>
            
         
 </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
        </>

    )
}
