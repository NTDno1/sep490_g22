import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from 'react-router-dom';
import {

  UilTimesCircle,
} from "@iconscout/react-unicons";
import Cookies from 'js-cookie';
import { convertDateFormat } from '../../../constants/constant';
import Modal from 'react-bootstrap/Modal';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddStudentInClass, DeleteListMemberClass, DeleteMemberClass, StudentofClass } from '../../../api/apiClient/Class/ClassAPI';
import { GetStudentAdminSide } from '../../../api/apiClient/Student/studentAPI';
import { GenerateAttendence } from '../../../api/apiClient/Attend/Attend';
import FooterAdmin from '../../FooterAdmin';
const initialFieldValues = {
  studentId: '',
  note: '',
}
export default function AddClassMember() {
  let jwttoken = Cookies.get('jwttoken');
  const [student, setStudent] = useState([]);
  const [values, setValues] = useState(initialFieldValues);
  const [liststudent, SetListstudent] = useState([]);
  const { classId } = useParams();
  const [selectAll, setSelectAll] = useState(false);
  const [checkedItems, setCheckedItems] = useState({});
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    fetchDataStudentOfClass();
    fetchDataListStudent();
  }, []);

  const fetchDataStudentOfClass = async () => {
    const data = await StudentofClass(classId);
    if (data.result.length > 0) {
      SetListstudent(data.result);
      console.log("a", data.result);
    }
  };

  const fetchDataListStudent = async () => {
    const data = await GetStudentAdminSide();
    if (data.result.length) {
      setStudent(data.result);
    
    }
  };

  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    })
  }

  const addOrEdit = async (formData) => {
    try {
      const response = await AddStudentInClass(values.studentId, classId, values.note);
      toast.success("Thành công")
      fetchDataStudentOfClass();
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  };

  const handleSubmitSlotClass = (e) => {

    e.preventDefault()
    const formData = new FormData()
    formData.append('classId', classId)
    formData.append('note', values.note)
    formData.append('studentId', values.studentId)

    addOrEdit(formData)
  }
  const handleCreateClass = async (e) => {
    if (window.confirm("Dữ liệu sẽ không thể thay đổi sau khi lưu, bạn có chắc chắn tiếp tục không?")) {
      e.preventDefault();
      try {
        const response = await GenerateAttendence(classId);
        toast.success('Lưu thành công');
        setTimeout(() => {  
        navigate('/classadmin');
        },1000)
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      
      }

    }
  }
  const deleteMemberClass = async (id) => {

    if (window.confirm("Bạn có muốn xóa học viên này")) {
      try {
        const response = await DeleteMemberClass(classId, id);
        toast.success('Thành công')
        fetchDataStudentOfClass();
      }
      catch (error) {
        toast.error(error.response.data.errorMessages[0]);
      }

    }
  }
  const toggleSelectAll = () => {
    setSelectAll(!selectAll);
    const updatedCheckedItems = {};
    liststudent.forEach((student, index) => {
      updatedCheckedItems[index] = !selectAll;
    });
    setCheckedItems(updatedCheckedItems);
  };
  const handleCheckboxChange = (index) => {
    const updatedCheckedItems = { ...checkedItems, [index]: !checkedItems[index] };
    setCheckedItems(updatedCheckedItems);

    const allChecked = Object.values(updatedCheckedItems).every((isChecked) => isChecked);
    setSelectAll(allChecked);
  };
  const removeGroupClass = async (e) => {
    const selectedIds = [];

    Object.keys(checkedItems).forEach((index) => {
      if (checkedItems[index]) {
        const rowData = {

          classId: classId,
          studentId: liststudent[index].studentId,
          studentName: '',
          status: 0,
          totalSlot: 0,
          startDate: "2023-11-14T03:03:53.622Z",
          dateCreate: "2023-11-14T03:03:53.622Z",
          dateUpdate: "2023-11-14T03:03:53.622Z",
          note: "",
        };
        selectedIds.push(rowData);
      }
      console.log(selectedIds);
    });
    if (window.confirm("Bạn có muốn xóa những học viên này ?")) {
      try {
        const response = await DeleteListMemberClass(JSON.stringify(selectedIds))
        toast.success('Xóa học viên thành công');
            window.location.reload();
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      }
    
    }
    
  }
  return (

    <>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <div className="app">
            <Box style={{ display: 'flex' }}>
              <Sidebars isSidebar={isSidebar} />
              <main className="content">
                <Topbar setIsSidebar={setIsSidebar} />
                <div className="container">
                  <ToastContainer />
                  <div className="row">
                    <div className="col-12">
                      {/* Page title */}
                      <div className="col-12 my-5">
                        <form id="form">
                          <ul id="progressbar">
                            <li onClick={() => { navigate(`/classadmin/editclass/${classId}`); }} className='active' id="step1"><strong className='class-progress'><i class="fa fa-users"></i></strong></li>
                            <li className='active' onClick={() => { navigate(`/addclassschedule/${classId}`); }} id="step2"><strong  className='calender-progress'><i class="fa fa-calendar-plus"></i></strong></li>
                            <li className='active' id="step3"><strong className='user-progress'><i class="fa fa-user-plus"></i></strong></li>

                          </ul>
                        </form>
                        <hr />
                      </div>
                      {/* Form START */}
                      <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '40px' }}>
                        <div className="m-portlet__head-tools m--pull-right">
                        </div>
                        <div
                          className="m-portlet__head-tools m--pull-right"
                          style={{ marginTop: 8, marginRight: 10 }}
                        >
                          <button style={{ background: 'red', padding: '10px 20px', marginRight: '20px', backgroundColor: 'red' }} type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon " id="save-btn" onClick={() => removeGroupClass()}>
                            Xóa học viên
                          </button>
                          <button style={{ background: '#2746C0', padding: '10px 20px' }} type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" onClick={handleShow}>
                          <i class="fa fa-plus"></i> Thêm học viên
                          </button>
                        </div>
                      </div>
                      <div className="table-responsive" id="table-content">
                        <table
                          className="table table-bordered table-hover"
                          id="class-table"
                          style={{ minHeight: "250px !important" }}
                        >
                          <thead>
                            <tr>
                              <th><input type='checkbox' checked={selectAll}
                                onChange={toggleSelectAll}></input></th>
                              <th>STT</th>
                              <th>Học viên</th>

                              <th>Bắt đầu</th>
                              <th>Số buổi</th>
                              <th style={{ width: "10%" }}>Hành động</th>
                            </tr>
                          </thead>
                          <tbody>
                            {liststudent.map((val, index) => (
                              <tr>
                                <td>
                                  <input
                                    type='checkbox'
                                    checked={checkedItems[index] || false}
                                    onChange={() => handleCheckboxChange(index)}
                                  />
                                </td>
                                <td>

                                  <div>{index + 1}</div>
                                </td>
                                <td>
                                  <span
                                    data-container="body"
                                    data-toggle="m-popover"
                                    data-html="true"
                                    data-content="<div></div><div>Học phí: 3,895,000 (Theo khóa)</div>"
                                  >
                                    {val.studentName}
                                  </span>
                                </td>
                                <td>{convertDateFormat(val.startDate)}</td>
                                <td>
                                  {val.totalSlot}
                                </td>

                                <td><a style={{ cursor: 'pointer' }} onClick={() => deleteMemberClass(val.studentId)}><UilTimesCircle /></a></td>
                              </tr>
                            ))}
                          </tbody>
                        </table>

                      </div>


                      {/* Form END */}
                    </div>
                    <div class="col-md-12" align="right">
                      <form onSubmit={handleCreateClass}>
                      <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
           <i className="addicon fa fa-save"></i>Lưu
         </button>
                      </form>
                    </div>
                  </div>
                </div>
                <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Thêm học viên</Modal.Title>
                  </Modal.Header>
                  <form onSubmit={handleSubmitSlotClass} >
                  <Modal.Body>
                    <label className="form-label">Học viên</label>
                    <select className="form-control" name="studentId" onChange={handleInputChange}>
                      <option value=""  >Chọn học viên</option>
                      {student?.map((option) => (
                        <option key={option.id} value={option.id}>
                          {option.firstName} {option.midName} {option.lastName} #{option.studentCode}
                        </option>
                      ))}
                    </select>

                    <label htmlFor="textareaInput">Ghi chú:</label>
                    <textarea name='note' style={{ height: '200px', width: '100%' }} onChange={handleInputChange} type='text' ></textarea>
                  </Modal.Body>
                  <Modal.Footer>
                    <button className='btn btn-success m-btn' variant="primary"  type="submit">
                      Lưu thay đổi
                    </button>
                  </Modal.Footer>
                  </form>
                </Modal>
            
              </main>
            </Box>
            <FooterAdmin/>
          </div>
        </ThemeProvider>
      </ColorModeContext.Provider>
    </>
  )
}
