import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { convertDateFormat } from "../../../constants/constant";
import NotificationsIcon from '@mui/icons-material/Notifications';
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { StudentofClass } from "../../../api/apiClient/Class/ClassAPI";
import FooterAdmin from "../../FooterAdmin";
  
const AdminClassInfoMember = () => {
  const { id } = useParams();
  const themes = useTheme();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const colors = tokens(themes.palette.mode);
  const [classinfoMember, setClassInfoMember] = useState([]);
  const [className, setClassName] = useState('');
  const apiRef = useGridApiRef();
  const jwttoken = Cookies.get('jwttoken');
      const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };
      
        checkTokenBeforeRoute();
      }, [jwttoken]);
      useEffect(() => {
        getStudentofClass();
        setTimeout(() => {
          apiRef.current.setPageSize(15);
          },10)
      }, []);
      const getStudentofClass = async() => {
        const data = await StudentofClass(id);
      if(data.result.length > 0){
        const convertedCourse = data.result.map((item) => {         
          item.startDate = convertDateFormat(item.startDate);
          item.endDate = convertDateFormat(item.endDate);

          // item.endUpdate = convertDateFormat(item.endUpdate)  
        return item;
      }); 
      setClassName(data.result[0].className)
      setClassInfoMember(convertedCourse);
    }
    
      }
      const dataWithMember = classinfoMember.map((item, index) => ({
        ...item,
        MemberID: index + 1,
      }))
  const columns = [
    { field: "MemberID", headerName: "STT",align:'center' },
    {
      field: "studentName",
      headerName: "Học sinh",
      flex: 1,
      cellClassName: "name-column--cell",
    },

  
    {
      field: "startDate",
      headerName: "Ngày bắt đầu",
      align:'center',
      flex: 0.5,
    },
    {
      field: "endDate",
      headerName: "Ngày kết thúc",
      align:'center',
      flex: 0.5,
    },
    {
      field: "note",
      headerName: "Ghi chú",
      flex: 1,
    },
  ];
  console.log(Array.isArray(classinfoMember))
  return (
    <>
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />

    <div className="container">

      <div className="row">

        <div className="col-12">

          <div className="col-12 my-5">
         
            <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
            <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

              <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Khung chương trình</span></Link>
                </div>
                 <div class="icon-container">
                  <Link className='icon-class' ><FontAwesomeIcon className='iconcourse-active' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                </div>
                <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>    <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                  </div>
              </div>
            </div>
            <hr />
          </div>
          {/* Form START */}
        
          <Box
            m="40px 0 0 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
                borderRight: "0.5px solid #E0E0E0 !important"
              },
              "& .name-column--cell": {
                color: colors.greenAccent[300],
              },
              "& .MuiDataGrid-columnHeaderTitle":{
                color:"white"
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: "#2746C0",
                borderBottom: "none",
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: colors.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: "#2746C0",
              },
              "& .MuiCheckbox-root": {
                color: `${colors.greenAccent[200]} !important`,
              },
            }}
          >
            <DataGrid pageSizeOptions={[15, 20,25]}  apiRef={apiRef}  rows={dataWithMember} columns={columns} />
          </Box>


          {/* Form END */}
        </div>
      </div>
    </div>
    
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
    </>
  );
};

export default AdminClassInfoMember;
