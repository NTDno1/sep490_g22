import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines, faUserGroup, faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Cookies from 'js-cookie';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { formatStartDate } from '../../../constants/constant';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ClassDetail } from '../../../api/apiClient/Class/ClassAPI';
import FooterAdmin from '../../FooterAdmin';
  

export default function AdminClassInfo() {
  const centetname = Cookies.get('CenterName');
  const { id } = useParams();
  const [classinfo, setClassInfo] = useState({});
  const [className, setClassName] = useState('');
  const jwttoken = Cookies.get("jwttoken")

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

 
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    getInfoClass();
  },[id]);
  const getInfoClass = async() => {
   
    const data = await ClassDetail(id);
    console.log(data)
    if(data){
    data.result.startDate = data.result.startDate.toString().split('T')[0];
    setClassInfo(data.result);
    setClassName(data.result.name);
    }
  }

  return (
    <>
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />

    <div className="container">

      <div className="row">

        <div className="col-12">

          {/* Page title */}
          <div className="col-12 my-5">
            <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>

            <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

              <div style={{ display: 'flex', flex: '1', textDecoration: 'none' }}>
                <div class="icon-container">
                  <Link className='icon-class' href='#'><i class="iconcourse-active fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/calender/${classinfo.id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/syllabus/${classinfo.id}`}><FontAwesomeIcon className='iconcourse' icon={faFileLines} /> <span class="hover-text">Khung chương trình</span></Link>
                </div>
              <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/member/${classinfo.id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../coursesadmin/schedule/${classinfo.id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                </div>
                <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>     <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                  </div>
              </div>
            </div>
            <hr />

            {/* Form START */}

            <div className="row mb-12 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">

                    {/* First Name */}
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={centetname} disabled

                      />
                    </div>
                    {/* Last name */}
                    <div className="col-md-6">
                      <label className="form-label">Khóa học</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.courseName}
                        readOnly
                      />
                    </div>
                    {/* Phone number */}
                    <div className="col-md-6">
                      <label className="form-label">Giáo viên </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.teacherName}
                        readOnly
                      />
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Lớp học </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.name}
                        readOnly
                      />
                    </div>
                    {/* Mobile number */}
                    <div className="col-md-6">
                      <label className="form-label">Mã lớp học </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.code}
                        readOnly
                      />
                    </div>
                    {/* Email */}
                    <div className="col-md-6">
                      <label className="form-label">Số buổi</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.slotNumber} readOnly
                      />
                    </div>
                    {/* Skype */}
                    <div className="col-md-6">
                      <label className="form-label">Số giờ học</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={classinfo.slotNumber * 2}
                        readOnly
                      />
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Ngày bắt đầu</label><br />
                      <DatePicker
                        value={formatStartDate(classinfo.startDate)}
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                            readOnly
                      />
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Ngày kết thúc</label>
                      <DatePicker
                        value={formatStartDate(classinfo.endDate)}
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                            readOnly
                      />
                    </div>

                  </div>{" "}
                  {/* Row END */}
                </div>
              </div>

            </div>{" "}
          </div>

          {/* Form END */}
        </div>
      </div>
    </div>
  
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
    </>
  )
}
