import React, { useEffect, useState } from 'react'
import classes from "./loginadmin.css"
import { Link, useNavigate } from 'react-router-dom';
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import Cookies from 'js-cookie';
import { LoginAdminCenter, LoginSuperAdmin } from '../../../api/apiClient/Authentication/Authentication';

export default function LoginAdmin() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();
  const [selectedRole, setSelectedRole] = useState('Admin');
  const expirationTime = new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000);
  useEffect(() => {
    sessionStorage.clear();
  }, []);
  const handleRoleChange = (event) => {
    setSelectedRole(event.target.value);
  };
  const ProceedLogin = async (e) => {
    e.preventDefault();
    if (selectedRole === "Admin") {
      if (validate()) {
        let inputObj = {
          "username": username,
          "password": password,
          "role": "Employee"
        };
        try {
          const response = await LoginAdminCenter(inputObj);
          const data = response;
          const token = data.tokenInformation.accessToken;
          const refreshToken = data.tokenInformation.refreshToken
          if (token) {
            try {
              // Phân tích token
              const decoded = jwtDecode(token);
              Cookies.set('jwttoken', token, { expires: expirationTime });
              Cookies.set('refreshToken', refreshToken, { expires: expirationTime });
              Cookies.set('decoded', decoded, { expires: expirationTime });
              Cookies.set('username', decoded.name, { expires: expirationTime });
              Cookies.set('CenterName', decoded.CenterName, { expires: expirationTime });
              Cookies.set('roleName', decoded.RoleID, { expires: expirationTime });
              Cookies.set('empId', decoded.ID, { expires: expirationTime });
              navigate("/");

            } catch (error) {
              if (error.response && error.response.data && error.response.data.errorMessages) {
                const errorMessages = error.response.data.errorMessages;
                if (errorMessages && errorMessages.length > 0) {
                  // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                  const errorMessage = errorMessages[0];
                  toast.error(errorMessage); // Hiển thị thông điệp lỗi
                  console.error('Error:', error);
                }
              }
            }
          }
        }
        catch (error) {
          if (error.response && error.response.data && error.response.data.errorMessages) {
            const errorMessages = error.response.data.errorMessages;
            if (errorMessages && errorMessages.length > 0) {
              // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
              const errorMessage = errorMessages[0];
              toast.error(errorMessage); // Hiển thị thông điệp lỗi
              console.error('Error:', error);
            }
          }
        }
      }
    } else if (selectedRole === "Super Admin") {
      if (validate()) {
        let inputObj = {
          "username": username,
          "password": password,
          "role": "Super Admin"
        };
        try {
          const response = await LoginSuperAdmin(inputObj);
          const data = response;
          const token = data.tokenInformation.accessToken;
          const refreshToken = data.tokenInformation.refreshToken
          if (token) {

            try {
              // Phân tích token
              const decoded = jwtDecode(token);
              Cookies.set('jwttoken', token, { expires: expirationTime });
              Cookies.set('refreshToken', refreshToken, { expires: expirationTime });
              Cookies.set('decoded', decoded, { expires: expirationTime });
              Cookies.set('username', decoded.name, { expires: expirationTime });
              Cookies.set('CenterName', decoded.CenterName, { expires: expirationTime });
              Cookies.set('roleName', decoded.RoleID, { expires: expirationTime });
              Cookies.set('empId', decoded.ID, { expires: expirationTime });
              navigate("/");
            } catch (error) {
              if (error.response && error.response.data && error.response.data.errorMessages) {
                const errorMessages = error.response.data.errorMessages;
                if (errorMessages && errorMessages.length > 0) {
                  // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                  const errorMessage = errorMessages[0];
                  toast.error(errorMessage); // Hiển thị thông điệp lỗi
                  console.error('Error:', error);
                }
              }
            }
          }
        }
        catch (error) {
          if (error.response && error.response.data && error.response.data.errorMessages) {
            const errorMessages = error.response.data.errorMessages;
            if (errorMessages && errorMessages.length > 0) {
              // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
              const errorMessage = errorMessages[0];
              toast.error(errorMessage); // Hiển thị thông điệp lỗi
              console.error('Error:', error);
            }
          }
        }
      }
    }
  }
  const validate = () => {
    let result = true;
    if (username === '' || username === null) {
      result = false;
      alert('Vui lòng nhập tên đăng nhập');
    }
    if (password === '' || password === null) {
      result = false;
      alert('Vui lòng nhập mật khẩu');
    }
    return result;
  }

  return (
    <>
  
  <div className="login">
    <ToastContainer/>
    <form onSubmit={ProceedLogin} className="login__form">
    <h2 className="logologin_admin"> <img className='login-text' src="https://pitech.edu.vn:82/Images/LogoCenter.png"></img></h2>
      <div className="login__content">
        <div className="login__box">
          <i className="ri-user-3-line login__icon" />
          <div className="login__box-input">
            
            <input
             
              className="login__input"
              id="login-email"
              placeholder=" "
              value={username} onChange={(e) => setUsername(e.target.value)} type="text" name="" required=""
            />
            <label htmlFor="login-email" className="login__label">
              Tên đăng nhập
            </label>
          </div>
        </div>
        <div className="login__box">
          <i className="ri-lock-2-line login__icon" />
          <div className="login__box-input">
            <input
              type="password"
              required=""
              className="login__input"
              id="login-pass"
              placeholder=""
              value={password} onChange={(e) => setPassword(e.target.value)} 
            />
            <label htmlFor="login-pass" className="login__label">
              Mật khẩu
            </label>
            <i className="ri-eye-off-line login__eye" id="login-eye" />
          </div>
        </div>
      </div>
      <div className="login__check">
        <div className="login__check-group">
        <div>
            <label style={{color:'#2746C0'}}>Chọn vai trò</label>
            <select
              className="form-control"
              id="select-course"
              value={selectedRole}
              onChange={handleRoleChange}
              style={{ flex: '1', marginRight: '10px',backgroundColor:'#2746C0',color:'#fff'}}
            >
              <option value="Admin">Admin</option>
              <option value="Super Admin">Super Admin</option>
            </select>
            </div>
        </div>
        <Link to="/forgetpasswordadmin" className='forget-password'>Quên mật khẩu?</Link>
      </div>
      <div className='login-submit-container'>
                        <button style={{marginTop:'50px'}} type='submit' className={"login-submit"}>Đăng nhập</button>
                    </div>
    
    </form>
  </div>
  {/*=============== MAIN JS ===============*/}
</>


  )
}
