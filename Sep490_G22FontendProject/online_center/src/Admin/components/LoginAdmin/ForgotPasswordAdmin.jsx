import React, { useEffect, useState } from 'react'
import classes from "./loginadmin.css"

import { Link, useNavigate } from 'react-router-dom';
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import Cookies from 'js-cookie';
import { useTimer } from '../../../useTimer';

export default function ForgotPasswordAdmin() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const navigate = useNavigate();

    const [isSubmitting, setIsSubmitting] = useState(false);

    const { remainingTime, updateRemainingTime } = useTimer();

    const [timeoutId, setTimeoutId] = useState(null);


    const ProceedLogin = async (e) => {
        e.preventDefault();
        if (isSubmitting) {
            // If already submitting, do nothing
            return;
        }
        setIsSubmitting(true);
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        const newTimeoutId = setTimeout(() => {
            setIsSubmitting(false);
            updateRemainingTime(5 * 60); 
          }, remainingTime * 1000);
          setTimeoutId(newTimeoutId);
        let inputObj = {
            "userName": username,
            "email": email,
    
        };
        fetch(`https://pitech.edu.vn:82/api/Email/ForgotPassWord?userName=${username}&email=${email}&roleName=Employee`, {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(inputObj)
        }).then((res) => {
            if (!res.ok) {
                return res.json().then((errorData) => {
                    toast.error(errorData.errorMessages[0]);
                });
            } else {        
               toast.success("Gửi thành công");
            }
          
        })
            .then((data) => {
            }).catch((err) => {
            })
    }
   

    const formatTime = (seconds) => {
        const minutes = Math.floor(seconds / 60);
        const remainingSeconds = seconds % 60;
        return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
    };

    return (
        <body className="body">
            <ToastContainer />
            <div className="login_box">
                <h2>Tìm tài khoản của bạn</h2>

                <form onSubmit={ProceedLogin} >
                    <div className="user_box">
                        <input value={username} onChange={(e) => setUsername(e.target.value)} type='text' required />
                        <label>Tên đăng nhập</label>
                    </div>
                    <div className="user_box">
                        <input value={email} onChange={(e) => setEmail(e.target.value)} type='email' required />
                        <label>Email</label>
                    </div>
                   
                    
                 
                    <div style={{ marginTop: '20px' }} className='login-submit-container'>


                        <button onClick={() => navigate("/login_admin")} type='button' className="btn btn-success m-btn">Thoát</button>
                        <button type='submit' style={{ color: `${isSubmitting ? 'gray' : ''}` }} className="btn btn-success m-btn">Lưu</button>
                    </div>

                    

                </form>
            </div>
        </body>

    )
}
