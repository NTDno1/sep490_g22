import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { UpdateInfoCenter, getCenterByIdAPI } from '../../../api/apiClient/Center/CenterAPI';
import Header from '../Header';
import { toast,ToastContainer } from 'react-toastify';
import { convertDateFormat } from '../../../constants/constant';
import FooterAdmin from '../../FooterAdmin';

const initialFieldValues = {
  id:'',
  name: '',
  description: '',
  adress: '',
  adminId: '',
  numberPhone:''
}

export default function UpdateCenter() {
  const [values, setValues] = useState(initialFieldValues);
  const {code} = useParams();
  let jwttoken =  Cookies.get('jwttoken');
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const navigate = useNavigate();
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);
  
  useEffect(() => {
       fetchDataTeacher();
  },[]);

  const fetchDataTeacher = async () => {
    const data = await getCenterByIdAPI(code);
    if(data){
      const result = data.result;           
          setValues({
            id:code,
            name: result.name,
            description: result.description,
            adress: result.adress,
            adminId: result.adminId,
            numberPhone:result.numberPhone,
          });

    };
  }
  
  
  const addOrEdit = async(formData) => {
    try {
      const response = await UpdateInfoCenter(formData);
      toast.success("Cập nhật thành công");
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('id', values.id)
    formData.append('name', values.name)
    formData.append('description', values.description)
    formData.append('adress', values.adress)
    formData.append('adminId', values.adminId)
    formData.append('numberPhone', values.numberPhone)
 
    addOrEdit(formData)
  }

  const handleInputChange = e => {
    const { name, value } = e.target;
   
    setValues({
      ...values,
      [name]: value
    })
  }
  
  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
                 
    <div className="container">
    <ToastContainer/>
    <Header title="Trung Tâm" subtitle="Cập nhật trung tâm" />
      <div className="row">
      
        <div className="col-12">
          {/* Page title */}

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>
        
          <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                  
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='name'
                        value={values.name}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
                   
                    <div className="col-md-12">
                      <label className="form-label">Mô tả</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='description'
                        value={values.description}
                        onChange={handleInputChange}
                      />
                    </div>
                    <div className="col-md-6">
                                <label className="form-label">Địa chỉ <span className='required-field'>*</span></label>
                                <select name="adress" value={values.adress} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>     
                    <div className="col-md-6">
                      <label className="form-label">Số điện thoại <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='numberPhone'
                        pattern="[0-9]{10}"
                        maxLength="10"
                        value={values.numberPhone}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  </div>{" "}          
                </div>
              </div>             
            </div>{" "}
            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
            <a className='backtolist' href='/center'> {'<< Quay về danh sách'}</a>
        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
           <i className="addicon fa fa-save"></i>Lưu
         </button>
          </div>
          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
   
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
