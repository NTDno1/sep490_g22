import React, { useEffect, useState } from "react";
import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { clean } from 'diacritic';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { CloseCenter, DeleteCenter, OpenCenter, getCenterAPI } from "../../../api/apiClient/Center/CenterAPI";
import FooterAdmin from "../../FooterAdmin";
import { convertDateFormat } from "../../../constants/constant";


const Center = () => {
  const [center, setCenter] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const [originalClasses, setOriginalClasses] = useState([]);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [searchCenter, setSearchCenter] = useState("");
  let jwttoken = Cookies.get('jwttoken');

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);


  useEffect(() => {
  
    fetchDataTeacher()
   
  }, [apiRef]);
  const fetchDataTeacher = async () => {
    try{
    const data = await getCenterAPI();
    console.log(data)
    if (data.result.length > 0) {
      data.result = data.result.map((item) => {      
        item.createDate = convertDateFormat(item.createDate);
        item.updateDate = convertDateFormat(item.updateDate);
        return item;
      });
      setOriginalClasses(data.result);
      setCenter(data.result);
     
        apiRef.current.setPageSize(7);
     
    }else{};
  }catch(error){}
}
  
  const dataWithCenter = center.map((item, index) => ({
    ...item,
    CenterID: index + 1,
  }))

  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa trung tâm này không ?")) {
      try {
        const response = await DeleteCenter(id);
        toast.success("Xóa thành công");
        fetchDataTeacher();
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      }

    }
  };

  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };


  const handleActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn kích hoạt trung tâm này không ?")) {
      try {
        const response = await OpenCenter(id);
        toast.success("Kích hoạt thành công");
        fetchDataTeacher();
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      }
    }
  }
  const handleDeActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn ngừng kích hoạt trung tâm này không ?")) {
      try {
        const response = await CloseCenter(id);
        toast.success("Ngừng kích hoạt thành công");
        fetchDataTeacher();
      }
      catch (error) {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      }
    }
  }
  const columns = [
    {
      headerName: "STT",
      field: "CenterID",
      align: "center",
      flex: 0.5,
    },
    {
      field: "name",
      headerName: "Trung tâm",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "adress",
      headerName: "Địa chỉ",
      flex: 1,
    },
    {
      field: "codeCenter",
      headerName: "Mã trung tâm",
      flex: 1,
    },
    {
      field: "editor",
      headerName: "Người sửa",
      flex: 1,
    },
    {
      field: "numberPhone",
      headerName: "Số điện thoại",
      align: "center",
      flex: 1,
    },
    {
      field: "createDate",
      headerName: "Ngày tạo",
      align: "center",
      flex: 1,
    },
    {
      field: "updateDate",
      headerName: "Ngày cập nhật",
      align: "center",
      flex: 1,
    },
    {
      field: "Action",
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => (
       (params.row.status === 0 ? (
        <UilLock
          style={{ cursor: 'pointer', color: 'gray' }}
    
        />
      ) :
          params.row.status === 1 ? (

            <UilUnlock
              style={{ cursor: 'pointer', color: 'green' }}
              onClick={() => handleDeActiveCenter([params.row.id])}
            />
          ) : (
            params.row.status === 2 ? (

              <UilLock
                style={{ cursor: 'pointer', color: 'red' }}
                onClick={() => handleActiveCenter([params.row.id])}
              />
            ) : null
          )
        )
      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => (
        <div style={{ display: 'flex', alignItems: 'center', marginLeft:'20px' }}>
          {params.row.status === 0 && (
            <DeleteOutlineIcon
              style={{ cursor: 'pointer', fontSize: '25px' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
          )}
         
            <a href={`/updateCenter/${params.row.id}`}>
              <UilEdit style={{ marginLeft: '5px' }} />
            </a>
         
        </div>
      ),
    }

  ];
  const filterNameCenter = (event) => {
    event.preventDefault();
    if (searchCenter) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.name.toLowerCase()).includes(clean(searchCenter.toLowerCase()))
      );
      setCenter(filteredClasses);
    } else {
      setCenter(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <ToastContainer/>
              <Box m="20px">
                <Header title="TRUNG TÂM" subtitle="Quản lý trung tâm" />
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Link to='/center/addCenter' className="btn btn-success m-btn">
                      <i className="addicon fa fa-plus"></i> Thêm mới 
                    </Link>
                  </Box>
                  <form className="filter-search" onSubmit={filterNameCenter}>
                    <input
                      type="text"
                      className="form-control m-input"
                      placeholder="Tìm kiếm tên trung tâm..."
                      onChange={(e) => setSearchCenter(e.target.value)}
                      value={searchCenter}

                    />
                  </form>

                </Box>
                <Box
                  m="40px 0 0 0"
                  height="80vh"
                  sx={{
                    "& .MuiDataGrid-root": {
                      border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                      borderBottom: "none",
                      borderRight: "0.5px solid #E0E0E0 !important"
                    },
                    "& .name-column--cell": {
                      color: colors.greenAccent[300],
                    },
                    "& .MuiDataGrid-columnHeaderTitle":{
                      color:"white"
                    },
                    "& .MuiDataGrid-columnHeaders": {
                      backgroundColor: "#2746C0",
                      borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                      backgroundColor: colors.primary[400],
                    },
                    "& .MuiDataGrid-footerContainer": {
                      borderTop: "none",
                      backgroundColor: "#2746C0",
                    },
                    "& .MuiCheckbox-root": {
                      color: `${colors.greenAccent[200]} !important`,
                    },
                  }}
                >
                   {dataWithCenter.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20,25]}  apiRef={apiRef} rows={dataWithCenter} columns={columns} onRowSelectionModelChange={handleSelectionChange} />
                  )}
                
                </Box>
              </Box>
           
            </main>
          </Box>
          <FooterAdmin/>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};
export default Center;
