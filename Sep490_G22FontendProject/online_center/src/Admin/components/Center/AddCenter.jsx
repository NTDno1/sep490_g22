import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate } from 'react-router-dom';

import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider, Tooltip } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddCenterByAdmin } from '../../../api/apiClient/Center/CenterAPI';
import Header from '../Header';
import { toast, ToastContainer } from 'react-toastify';

import FooterAdmin from '../../FooterAdmin';
import { generateRandomCode } from '../../../constants/constant';
import ReplayIcon from '@mui/icons-material/Replay';
import axios from 'axios';
const initialFieldValues = {
    name: '',
    description: '',
    adress: '',
    codeCenter: '',
    adminId: '',
    numberPhone:''

}

export default function AddCenter() {
  const [values, setValues] = useState(initialFieldValues);
  
  
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);

  let jwttoken =  Cookies.get('jwttoken');
  const navigate = useNavigate();
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
        console.log(response.data)
        setValues({
          ...values,
          adress: response.data[0].name,
        });
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);

 
  const addOrEdit = async(formData) => {
    try {
      const response = await AddCenterByAdmin(formData);
      toast.success("Thêm thành công");
      navigate("/center")
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
         
        }
      }
    }
  
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('name', values.name)
    formData.append('description', values.description)
    formData.append('adress', values.adress)
    formData.append('codeCenter', values.codeCenter)
    formData.append('adminId', values.adminId)
    formData.append('numberPhone', values.numberPhone)
    addOrEdit(formData)
  }


  const handleInputChange = e => {
    const { name, value } = e.target; 
    setValues({
      ...values,
      [name]: value
    })
  }
  const generateCode = () => {
    const genCode = generateRandomCode();
    setValues({
      ...values,
      codeCenter: genCode
    }) 
    
  }
  return (
 
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
           
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
                  <ToastContainer/>
    <div className="container">
    <Header title="Trung Tâm" subtitle="Thêm trung tâm" />
      <div className="row">
   
        <div className="col-12">
          <form onSubmit={handleFormSubmit}>
        
            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                  <div className="col-md-6">
                      <label className="form-label">Mã trung tâm  <span className='required-field'>*</span></label>
                      <div style={{position:'relative', display: 'flex' }}>
                                  <input 
                                    value={values.codeCenter}
                                    type="text"
                                    className="form-control"
                                    placeholder=""
                                    name='codeCenter'
                                    aria-label="Last name"
                                    required
                                    onChange={handleInputChange}
                                  />
                                 <Tooltip title="Tự tạo" placement='top'>
                                  <a className='replayCode'  onClick={() => generateCode()}><ReplayIcon/></a>
                                  </Tooltip>        
                                   </div>
                    </div>
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm  <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='name'
                        value={values.name}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
                    
                    <div className="col-md-12">
                      <label className="form-label">Mô tả</label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        maxLength={500}
                        name='description'
                        value={values.description}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                    <div className="col-md-6">
                                <label className="form-label">Địa chỉ  <span className='required-field'>*</span></label>
                                <select name="adress" value={values.adress} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>   
               
                    <div className="col-md-6">
                      <label className="form-label">Số điện thoại  <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        pattern="[0-9]{10}"
                        maxLength="10"
                        name='numberPhone'
                        value={values.numberPhone}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  </div>{" "}
             
                </div>
              </div>

            </div>{" "}

      
            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
            <a className='backtolist' href='/center'> {'<< Quay về danh sách'}</a>
            <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
           <i className="addicon fa fa-save"></i>Lưu
         </button>
  </div>

          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
   
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
