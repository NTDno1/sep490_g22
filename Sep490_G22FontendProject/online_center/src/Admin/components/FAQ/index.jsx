import React, { useEffect, useState } from "react";
import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";

import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { Link, useNavigate } from "react-router-dom";
import { convertDateFormat } from "../../../constants/constant";
import { clean } from 'diacritic';
import Cookies from "js-cookie";

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { DeleteFAQs, GetFAQs } from "../../../api/apiClient/FAQ/faq";
import FooterAdmin from "../../FooterAdmin";
const FAQAdmin = () => {
  const [faq, setFAQ] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [originalClasses, setOriginalClasses] = useState([]);
  const jwttoken = Cookies.get('jwttoken');
  const [searchFAQ, setSearchFAQ] = useState("");
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    
    fetchData();
    
  }, [apiRef]);
  const fetchData = async () => {
    try {
      const data = await GetFAQs();

      if (data.result.length > 0) {
        const convertedCourse = data.result.map((item) => {
          item.dateCreate = convertDateFormat(item.dateCreate);
          item.dateUpdate = convertDateFormat(item.dateUpdate);
          return item;
        });
        setOriginalClasses(convertedCourse);
        setFAQ(convertedCourse);
     
        
     
      } else {
        // Handle the case where there is no data
        console.log("No data available");
      }
    } catch (error) {
      // Handle the error
      console.error("Error fetching data:", error);
    }
  };

  const dataWithFAQ = faq.map((item, index) => ({
    ...item,
    FaqID: index + 1,
  }))

  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa mục này ?")) {
      try{
      const response = await DeleteFAQs(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        setTimeout(() => {
          fetchData();
        },1000)
      } else {
        toast.error(response.errorMessages)
      }
    }catch(error){
      toast.error("Có lỗi xảy ra")
    }
  }
  };

  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };

  const columns = [
    {
      headerName: "STT",
      field: "FaqID",
      flex: 0.5,
      align: "center",
    },
    {
      field: "title",
      headerName: "Tiêu đề",
      flex: 1.5,
      cellClassName: "name-column--cell",
    },
    {
      field: "desc",
      headerName: "Mô tả",
      headerAlign: "left",
      flex: 1.5,
      align: "left",
    },
    {
      field: "dateCreate",
      headerName: "Ngày tạo",
      align: "center",
      flex: 0.5,
    },
    {
      field: "dateUpdate",
      headerName: "Ngày cập nhật",
      align: "center",
      flex: 0.5,
    },

    {
      headerName: "Chỉnh sửa",
      flex: 0.5,
      renderCell: (params) => 
      <>    <div style={{ display: 'flex', alignItems: 'center', marginLeft:'10px' }}>
    
        <DeleteOutlineIcon
          style={{ cursor: 'pointer', fontSize: '25px' }}
          onClick={() => handleRemoveClick([params.row.id])}
        />
    
     
     <a href={`/updateFAQ/${params.row.id}`}>
          <UilEdit style={{ marginLeft: '5px' }} />
        </a>
     
    </div>
      </>
    },
  
  ];
  const filterNameFAQ = (event) => {
    event.preventDefault();
    if (searchFAQ) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.title.toLowerCase()).includes(clean(searchFAQ.toLowerCase()))
      );
      setFAQ(filteredClasses);
    } else {
      setFAQ(originalClasses);
    }
  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <Box m="20px">
                <Header title="Câu Hỏi" subtitle="Quản lý câu hỏi" />
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Link to='./addFAQ' className="btn btn-success m-btn">
                      <i className="fa fa-plus"></i> Thêm mới
                    </Link>

                  </Box>

                  <form className="filter-search" onSubmit={filterNameFAQ}>
                    <input
                      type="text"
                      className="form-control m-input"
                      placeholder="Tìm kiếm..."
                      onChange={(e) => setSearchFAQ(e.target.value)}
                      value={searchFAQ}

                    />
                  </form>

                </Box>

                <Box
                  m="40px 0 0 0"
                  maxHeight="80vh"
                  sx={{
                    "& .MuiDataGrid-root": {
                      border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                      borderBottom: "none",
                      borderRight: "0.5px solid #E0E0E0 !important"
                    },
                    "& .name-column--cell": {
                      color: colors.greenAccent[300],
                    },
                    "& .MuiDataGrid-columnHeaderTitle":{
                      color:"white"
                    },
                    "& .MuiDataGrid-columnHeaders": {
                      backgroundColor: "#2746C0",
                      borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                      backgroundColor: colors.primary[400],
                    },
                    "& .MuiDataGrid-footerContainer": {
                      borderTop: "none",
                      backgroundColor: "#2746C0",
                    },
                    "& .MuiCheckbox-root": {
                      color: `${colors.greenAccent[200]} !important`,
                    },
                  }}
                >
                  {dataWithFAQ.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid
                      pageSizeOptions={[7, 20, 25]}
                      apiRef={apiRef}
                      rows={dataWithFAQ}
                      columns={columns}
                      onRowSelectionModelChange={handleSelectionChange}
                    />
                  )}
                </Box>
              </Box>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default FAQAdmin;
