import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddFAQs } from '../../../api/apiClient/FAQ/faq';
import FooterAdmin from '../../FooterAdmin';
import Header from '../Header';
import { ToastContainer, toast } from 'react-toastify';
import { Toast } from 'react-bootstrap';

const initialFieldValues = {
  title: '',
  desc: '',
}

export default function AddFaq() {
  const [values, setValues] = useState(initialFieldValues);
  
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);
  
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');
  
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);

  const addOrEdit = async(formData) => {
    await AddFAQs(formData)
    .then(data => {
      toast.success("Thêm thành công")
      setTimeout(() => {
      navigate('/FAQadmin')
      },2000)
    })
    .catch(error => {
      // Xử lý khi gửi thất bại
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    });
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('title', values.title)
    formData.append('desc', values.desc)
    addOrEdit(formData)
  }


  const handleInputChange = e => {
    const { name, value } = e.target; 
    setValues({
      ...values,
      [name]: value
    })
  }
  
  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />

    <div className="container">
    <Header title="Câu Hỏi" subtitle="Thêm câu hỏi" />
      <ToastContainer/>
      <div className="row">
        <div className="col-12">
          {/* Page title */}
         

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>
        
            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                    {/* First Name */}
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={centername} disabled

                      />
                    </div>
                    {/* Last name */}
                
                    {/* Phone number */}
                    <div className="col-md-12">
                      <label className="form-label">Tiêu đề  <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='title'
                        value={values.title}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
                    <div className="col-md-12">
                      <label className="form-label">Mô tả  <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='desc'
                        value={values.desc}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  
                  </div>{" "}
             
                </div>
              </div>

            </div>{" "}

      
            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
  
            <a className='backtolist' href='/FAQadmin'> {'<< Quay về danh sách'}</a>
            <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
  </div>

          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
    
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
