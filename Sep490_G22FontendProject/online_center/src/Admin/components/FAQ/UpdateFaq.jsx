import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import {  useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { GetFAQsDetail, UpdateInfoFAQs } from '../../../api/apiClient/FAQ/faq';
import { ToastContainer, toast } from 'react-toastify';
import { convertDateFormat } from '../../../constants/constant';
import FooterAdmin from '../../FooterAdmin';
import Header from '../Header';

const initialFieldValues = {
  id:0,
  title: '',
  desc: '',
  dateUpdate:'',
}

export default function UpdateFaq() {
  const [values, setValues] = useState(initialFieldValues);
  const {code} = useParams();
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const centername = Cookies.get('CenterName');

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

 
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
    useEffect(() => {
GetFAQs();
       
  },[])
  const GetFAQs = async() => {
    try{
    const data = await GetFAQsDetail(code)
      const result = data.result;           
      setValues({
        id: result.id,    
        title: result.title,
        desc: result.desc,
        dateUpdate:result.dateUpdate
      });
    }catch(error){
      
    }
  }
 


  
  const addOrEdit = async(formData) => {
    
  try {
    const response = await UpdateInfoFAQs(formData);
    toast.success("Update Success");
    window.location.reload();
  }
  catch (error) {
    toast.error(error.response.data.errorMessages[0]);
  }

   
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('id',code)
    formData.append('title', values.title)
    formData.append('desc', values.desc)
 
    addOrEdit(formData)
  }

  const handleInputChange = e => {
    const { name, value } = e.target;
   
    setValues({
      ...values,
      [name]: value
    })
  }
  
  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
                  <ToastContainer/>
    <div className="container">
    <Header title="Câu Hỏi" subtitle="Cập nhật câu hỏi" />

      <div className="row">
        <div className="col-12">
          {/* Page title */}

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>
        
            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                    {/* First Name */}
                    <div className="col-md-6">
                      <label className="form-label">Trung tâm</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={centername} disabled

                      />
                    </div>
                    {/* Last name */}
                
                
                    {/* Phone number */}
                    <div className="col-md-12">
                      <label className="form-label">Tiêu đề <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='title'
                        value={values.title}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
                    <div className="col-md-12">
                      <label className="form-label">Mô tả <span className='required-field'>*</span></label>
                      <textarea
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='desc'
                        style={{height:'200px'}}
                        value={values.desc}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  
                  </div>{" "}
             
                </div>
              </div>
             
            </div>{" "}

          
            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
  
            <a className='backtolist' href='/FAQadmin'> {'<< Quay về danh sách'}</a>
            <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
  </div>
          </form>
          {/* Form END */}
        </div>


      </div>
    </div>
    
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
