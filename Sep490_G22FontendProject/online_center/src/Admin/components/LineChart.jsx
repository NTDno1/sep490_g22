import { ResponsiveLine } from "@nivo/line";
import { Box, Typography, useTheme } from "@mui/material";
import { tokens } from "../theme";
import fetchData from "../../api/apiUtils";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { ReportMember } from "../../api/apiClient/Report/report";

const LineChart = ({ isCustomLineColors = false, isDashboard = false }) => {
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  
  const [datas,setData] = useState([]);
  let jwttoken = Cookies.get('jwttoken');
  const [year,setYear] = useState(new Date().getFullYear());
  const [method,setMethod] = useState("salary");

  useEffect(() => {
   getLineChart();
  
  },[year,method])
  const getLineChart = async() => {
   const data = await ReportMember(year)
      const datas = data.result
      if(method === "hours"){
      const mockLineData = Object.keys(datas).map((category, index) => {
        
        const color = index === 0 ? tokens("dark").greenAccent[500] : tokens("dark").blueAccent[300];    
        return {
            id: category,
            color: color,
            data: Object.keys(datas[category]).map(month => {
                return {
                    x: month,
                    y: datas[category][month].counts
                };
            })
        };
     
      });
      setData(mockLineData); 
    } else if(method === "salary"){
      const mockLineData = Object.keys(datas).map((category, index) => {
        
        const color = index === 0 ? tokens("dark").greenAccent[500] : tokens("dark").blueAccent[300];    
        return {
            id: category,
            color: color,
            data: Object.keys(datas[category]).map(month => {
                return {
                    x: month,
                    y: (datas[category][month].counts * 2)
                };
            })
        };
    
      });
      setData(mockLineData);
    }
      
   
  }
  const handleYearChange = (event) => {
    
    const selectedYear = event.target.value;

    setYear(selectedYear);
  };
  const handleMethodChange = (event) => {
    
    const selectedMethod = event.target.value;

    setMethod(selectedMethod);
  };
  return (
    <>
 
      <Box
            mt="25px"
            p="0 30px"
            display="flex "
            justifyContent="space-between"
            alignItems="center"
          >
            <Box>
              <Typography
                variant="h5"
                fontWeight="600"
                color={colors.grey[100]}
              >
              Doanh thu được tạo
              </Typography>     
            </Box>
            <Box>
            <div className="col-xl-12 my-3">
          <div className="form-group m-form__group row align-items-center">
            <div className="col-md-5">
              <select
                className="form-control"
                id="select-course"            
                defaultValue="sssssssssss"
                style={{width:'200px',backgroundColor:'transparent'}}
                value={year}
                onChange={handleYearChange}
              >
              
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
              </select>
            </div>
            <div className="col-md-5">
              <select
                className="form-control"
                id="select-course"            
                defaultValue="sssssssssss"
                style={{width:'200px',backgroundColor:'transparent',marginLeft:'30px'}}
                value={method}
                onChange={handleMethodChange}
              >
                <option value="salary">Tính theo lương</option>
                <option value="hours">Tính theo giờ dạy</option>
             
              </select>
            </div>
          </div>
        </div>
            </Box>
          </Box>
    <ResponsiveLine
      data={datas}
      theme={{
        axis: {
          domain: {
            line: {
              stroke: colors.grey[100],
            },
          },
          legend: {
            text: {
              fill: colors.grey[100],
            },
          },
          ticks: {
            line: {
              stroke: colors.grey[100],
              strokeWidth: 1,
            },
            text: {
              fill: colors.grey[100],
            },
          },
        },
        legends: {
          text: {
            fill: colors.grey[100],
          },
        },
        tooltip: {
          container: {
            color: colors.primary[500],
          },
        },
      }}
      colors={isDashboard ? { datum: "color" } : { scheme: "nivo" }} // added
      margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
      xScale={{ type: "point" }}
      yScale={{
        type: "linear",
        min: "auto",
        max: "auto",
        stacked: true,
        reverse: false,
      }}
      yFormat=" >-.2f"
      curve="catmullRom"
      axisTop={null}
      axisRight={null}
      axisBottom={{
        orient: "bottom",
        tickSize: 0,
        tickPadding: 5,
        tickRotation: 0,
        legend: isDashboard ? undefined : "transportation", // added
        legendOffset: 36,
        legendPosition: "middle",
      }}
      axisLeft={{
        orient: "left",
        tickValues: 5, // added
        tickSize: 3,
        tickPadding: 5,
        tickRotation: 0,
        legend: isDashboard ? undefined : "count", // added
        legendOffset: -40,
        legendPosition: "middle",
      }}
      enableGridX={false}
      enableGridY={false}
      pointSize={8}
      pointColor={{ theme: "background" }}
      pointBorderWidth={2}
      pointBorderColor={{ from: "serieColor" }}
      pointLabelYOffset={-12}
      useMesh={true}
      legends={[
        {
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 100,
          translateY: 0,
          itemsSpacing: 0,
          itemDirection: "left-to-right",
          itemWidth: 80,
          itemHeight: 20,
          itemOpacity: 0.75,
          symbolSize: 12,
          symbolShape: "circle",
          symbolBorderColor: "rgba(0, 0, 0, .5)",
          effects: [
            {
              on: "hover",
              style: {
                itemBackground: "rgba(0, 0, 0, .03)",
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
    />
    
    </>
  );
};

export default LineChart;
