import { Box,IconButton, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { ToastContainer, toast } from "react-toastify";
import Header from "../../components/Header";
import { clean } from 'diacritic';
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import Tooltip from '@mui/material/Tooltip';
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import Cookies from "js-cookie";
import { convertDateFormat } from "../../../constants/constant";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ActivateTeacher, DeactivateTeacher, DeleteListTeacher, DeleteTeacher, GetTeacherAdminSide } from "../../../api/apiClient/Teacher/teacherAPI";
import SearchIcon from '@mui/icons-material/Search';
import FooterAdmin from "../../FooterAdmin";
const Teacher = () => {
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [teacher, setTeacher] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  let jwttoken = Cookies.get('jwttoken');
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchTeacher, setSearchTeacher] = useState("");
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const navigate = useNavigate();
  const apiRef = useGridApiRef();

 
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
   
    ListTeacherAdminSide();
 
  }, [apiRef])
  const ListTeacherAdminSide = async () => {
    try{
  const data = await GetTeacherAdminSide();
  if(data.result.length > 0){
  data.result = data.result.map((item) => {
    if (item.lastName === null) { item.lastName = "" }
    item.dob = convertDateFormat(item.dob);
    item.createDate = convertDateFormat(item.createDate);
    return item;
  });
  setOriginalClasses(data.result)
  setTeacher(data.result)

    apiRef.current.setPageSize(7);
   
} else {}
}catch(error){}
}
  const dataWithTeacher = teacher.map((item, index) => ({
    ...item,
    TeacherId: index + 1,
  }));
  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa tài khoản này ?")) {
      try {
        const response = await DeleteTeacher(id);
        if (response.statusCode === 200) {
          toast.success('Thành công');
          ListTeacherAdminSide();
        } else {
          toast.error(response.errorMessages)
        }
      } catch (error) {
        // Handle errors that occur during the fetch or parsing of response body
        console.error('Error:', error);
      }
    }
  };
  const handleActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn cho tài khoản này hoạt động?")) {
      try{
      const response = await ActivateTeacher(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        ListTeacherAdminSide();
      } else {
        toast.error(response.errorMessages)
      }
    }catch(error) {}
    }
  }
  const handleDeActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn cho tài khoản ngừng hoạt động?")) {
      try{
      const response = await DeactivateTeacher(id)
      if (response.statusCode === 200) {
        toast.success('Thành công')
        ListTeacherAdminSide();
      } else {
        console.error('Failed to delete resource');
      }
    }catch(error){}
  }
  }
  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);

    setSelectedBlogCodes(selection);
  };
  
  const columns = [
    {
      headerName: "STT",
      field: "TeacherId",
      flex: 0.5,
      align:'center'
    },
    {
      field: "userName",
      headerName: "Tên người dùng",
      flex: 1,
    },
    {

      field: "firstName",
      headerName: "Họ tên",
      flex: 1,
      cellClassName: "name-column--cell",
      flex: 1.5,
    },
   
    {
      field: "email",
      headerName: "Email",
      flex: 1.5,
    },
    {
      field: "phoneNumber",
      headerName: "Số điện thoại",
      align: "center",
      flex: 1,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      align: "center",
      flex: 1,
    },
    {
      field: "createDate",
      headerName: "Ngày tạo",
      align: "center",
      flex: 1,
    },
    {
      field: "Action",
      headerName: "Hành động",
      flex: 1,
      renderCell: (params) => (
        params.row.status === 0 ? (
          <UilLock
            style={{ cursor: 'pointer', color: 'gray' }}
      
          />
        ) :
        (
          params.row.status === 1 ? (
            <UilUnlock
              style={{ cursor: 'pointer', color: 'green' }}
              onClick={() => handleDeActiveCenter([params.row.id])}
            />
          ) : (
            params.row.status === 2 ? (

              <UilLock
                style={{ cursor: 'pointer', color: 'red' }}
                onClick={() => handleActiveCenter([params.row.id])}
              />
            ) : null
          )
        )
      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => (
        <div style={{ display: 'flex', alignItems: 'center', marginLeft:'10px' }}>
          {params.row.status === 0 && (
            <DeleteOutlineIcon
              style={{ cursor: 'pointer', fontSize: '25px' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
          )}
         
            <a href={`/updateTeacher/${params.row.id}`}>
              <UilEdit style={{ marginLeft: '5px' }} />
            </a>
         
        </div>
      ),
    }
  
  ];

  const filterNameTeacher = (event) => {
    event.preventDefault();
    if (searchTeacher) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.userName.toLowerCase()).includes(clean(searchTeacher.toLowerCase())) ||
        clean(val.lastName.toLowerCase()).includes(clean(searchTeacher.toLowerCase()))
      );
      setTeacher(filteredClasses);
    } else {
      setTeacher(originalClasses);
    }
  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>

                <Box m="20px">
                  <ToastContainer />
                  <Header title="Giáo Viên" subtitle="Quản lý giáo viên" />
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box>
                      <Link to='/addteacher' className="btn btn-success m-btn">
                        <i className=" fa fa-plus"></i> Thêm mới
                      </Link>
                   
                    </Box>


                    <form className="filter-search" onSubmit={filterNameTeacher}>
                      <input
                        type="text"
                        className="form-control m-input"
                        placeholder="Tìm kiếm tên giáo viên..."
                        onChange={(e) => setSearchTeacher(e.target.value)}
                        value={searchTeacher}
                       />
                     
                    </form>

                  </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                   
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important", 
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                    
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                     {dataWithTeacher.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20,25]}  apiRef={apiRef} checkboxSelection rows={dataWithTeacher} columns={columns} onRowSelectionModelChange={handleSelectionChange} />
                  )}
                   
                  </Box>
                </Box>
              </Box>
         
            </main>
          </Box>
          <FooterAdmin/>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default Teacher;
