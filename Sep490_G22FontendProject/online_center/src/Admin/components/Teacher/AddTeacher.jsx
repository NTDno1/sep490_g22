import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Cookies from 'js-cookie';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddNewTeacher } from '../../../api/apiClient/Teacher/teacherAPI';
import { ToastContainer, toast } from 'react-toastify';
import FooterAdmin from '../../FooterAdmin';
import { convertDateFormat, convertDatePicker, convertYearMonthDay, convertoDate, formatDateString } from '../../../constants/constant';
import Header from '../Header';
const initialFieldValues = {
  userName: '',
  firstName: '',
  address: '',
  email: '',
  phoneNumber: '',
  dob: formatDateString(new Date()),
  certificateName: '',
  title: '',
  description: '',
  releaseDate: '',
  updateDate: '',
  image: File,
  imageCertificateName: File,
}

export default function AddTeacher() {
  const [values, setValues] = useState(initialFieldValues);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
        setValues({
          ...values,
          address: response.data[0],
        });
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);


  const handleFormSubmit = async (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('userName', values.userName)
    formData.append('firstName', values.firstName)
    formData.append('address', values.address)
    formData.append('email', values.email)
    formData.append('phoneNumber', values.phoneNumber)
    formData.append('dob', convertoDate(values.dob))
    formData.append('certificateName', values.certificateName)
    formData.append('title', values.title)
    formData.append('description', values.description)
    formData.append('releaseDate', convertoDate(values.releaseDate))
    formData.append('image', values.image)

    formData.append('imageCertificateName', values.imageCertificateName)

    try {
      const response = await AddNewTeacher(formData, jwttoken);
      toast.success("Thêm thành công");
      setTimeout(() => {
      navigate('/');
      },1000)
    }
    catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    }
  }
  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    })
  }
  const readFile = (uploadedFile) => {
    if (uploadedFile) {
      const reader = new FileReader();
      console.log(reader);
      reader.onload = (x) => {
        const img = document.querySelector('#preview-box');
        img.src = reader.result;
      };
      reader.readAsDataURL(uploadedFile);
    }
  };
  const readFileCerti = (uploadedFile) => {
    if (uploadedFile) {
      const reader = new FileReader();

      reader.onload = (x) => {
        const img = document.querySelector('#preview-certi');
        img.src = reader.result;

      };
      reader.readAsDataURL(uploadedFile);
    }

  };
  const handleChange = (event) => {
    setValues({
      ...values,
      image: event.target.files[0]
    })

    const fileUploader = document.querySelector('#customFile');
    const getFile = fileUploader.files;
    if (getFile.length !== 0) {
      const uploadedFile = getFile[0];
      readFile(uploadedFile);
    }
  };

  const handleChangeCerti = (event) => {
    setValues({
      ...values,
      imageCertificateName: event.target.files[0]
    })

    const fileUploader = document.querySelector('#customFileCerti');
    const getFile = fileUploader.files;
    if (getFile.length !== 0) {
      const uploadedFile = getFile[0];
      readFileCerti(uploadedFile);
    }

  };

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <ToastContainer />
              <div className="container">
                <Header title="Giáo Viên" subtitle="Thêm giáo viên" />

                <div className="row">
                  <div className="col-12">
                    {/* Page title */}


                    {/* Form START */}
                    <form className="file-upload" onSubmit={handleFormSubmit}>
                      <div className="row mb-5 gx-5">
                        {/* Contact detail */}

                        <div className="col-xxl-8 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">

                              {/* First Name */}
                              <div className="col-md-6">
                                <input style={{ display: 'none' }}
                                  name='id'

                                />

                                <label className="form-label"> Tên người dùng <span className='required-field'>*</span></label>
                                <input
                                  value={values.userName}
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='userName'
                                  aria-label="First name"
                                  onChange={handleInputChange}
                                  re
                                />
                              </div>

                              <div className="col-md-6">
                                <input style={{ display: 'none' }}
                                  name='id'

                                />

                                <label className="form-label"> Họ tên <span className='required-field'>*</span></label>
                                <input
                                  value={values.firstName}
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='firstName'
                                  aria-label="First name"
                                  onChange={handleInputChange}
                                />
                              </div>
                              {/* Last name */}

                              {/* Phone number */}
                              <div className="col-md-6">
                                <label className="form-label">Địa chỉ <span className='required-field'>*</span></label>
                                <select name="address" value={values.address} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>
                              {/* Mobile number */}

                              {/* Email */}
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Email <span className='required-field'>*</span>
                                </label>
                                <input

                                  value={values.email}

                                  type="email"
                                  name='email'
                                  className="form-control"
                                  id="inputEmail4"

                                  onChange={handleInputChange}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Số điện thoại <span className='required-field'>*</span>
                                </label>
                                <input
                                  value={values.phoneNumber}
                                  type="text"
                                  name='phoneNumber'
                                  className="form-control"
                                  id="inputEmail4"
                                  pattern="[0-9]{10}"
                                  maxLength="10"
                                  onChange={handleInputChange}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Ngày sinh <span className='required-field'>*</span>
                                </label>
                                <DatePicker
                                  value={(values.dob)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  showMonthDropdown
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  required
                                  maxDate={new Date()}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                    setValues({
                                      ...values,
                                      dob: formatDateString(selectedDate),
                                    });
                                  }}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Tên chứng chỉ
                                </label>
                                <input

                                  value={values.certificateName}

                                  type="text"
                                  name='certificateName'
                                  className="form-control"
                                  id="inputEmail4"

                                  onChange={handleInputChange}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Tiêu đề
                                </label>
                                <input

                                  value={values.title}

                                  type="text"
                                  name='title'
                                  className="form-control"
                                  id="inputEmail4"

                                  onChange={handleInputChange}
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="inputEmail4" className="form-label">
                                  ReleaseDate <span className='required-field'>*</span>
                                </label>
                                <DatePicker
                                  value={(values.releaseDate)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  required
                                  showMonthDropdown
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  maxDate={new Date()}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                    setValues({
                                      ...values,
                                      releaseDate: formatDateString(selectedDate),
                                    });

                                  }}
                                />
                              </div>
                              <div className="col-md-12">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Mô tả
                                </label>
                                <textarea
                                  style={{ height: '200px' }}
                                  value={values.description}
                                  type="text"
                                  maxLength={2000}
                                  name='description'
                                  className="form-control"
                                  id="inputEmail4"
                                  onChange={handleInputChange}
                                />
                              </div>

                            </div>{" "}
                            {/* Row END */}
                          </div>
                        </div>
                        {/* Upload profile */}
                        <div className="col-xxl-4">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <div className="mb-4 mt-0 text-center" style={{ fontSize: '20px', fontWeight: 'bolder' }}>Chọn ảnh của bạn</div>
                              <div className="text-center">
                                {/* Image upload */}
                                <div className="square position-relative display-2 mb-3">
                                  <img style={{ width: '250px', height: '250px' }} src={values.image} id='preview-box'></img>
                                </div>
                                {/* Button */}
                                <input type="file" id="customFile" name="file" accept="image/*" onChange={handleChange} hidden="true" />
                                <label
                                  className="btn btn-success-soft btn-block"
                                  htmlFor="customFile"
                                >
                                  Chọn tập tin
                                </label>
                                {/* Content */}
                              </div>
                            </div>
                            <div className="row py-4 g-3">
                              <div className="mb-4 mt-0 text-center" style={{ fontSize: '20px', fontWeight: 'bolder' }}>Chọn chứng chỉ của bạn</div>
                              <div className="text-center">
                                {/* Image upload */}
                                <div className="square position-relative display-2 mb-3">
                                  <img style={{ width: '250px', height: '250px' }} src={values.imageCertificateName} id='preview-certi'></img>
                                </div>
                                {/* Button */}
                                <input type="file" id="customFileCerti" name="file" accept="image/*" onChange={handleChangeCerti} hidden="true" />
                                <label
                                  className="btn btn-success-soft btn-block"
                                  htmlFor="customFileCerti"
                                >
                                  Chọn tập tin
                                </label>

                                {/* Content */}

                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">


                        </div>
                      </div>{" "}
                      <br/>
                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                      <a className='backtolist' href='/'> {'<< Quay về danh sách'}</a>
                      <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
                      </div>
                    </form>{" "}
                    {/* Form END */}
                  </div>


                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
