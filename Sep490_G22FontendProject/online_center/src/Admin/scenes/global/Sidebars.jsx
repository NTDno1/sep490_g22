import { useEffect, useState } from "react";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, IconButton, Typography, useTheme } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import "react-pro-sidebar/dist/css/styles.css";
import { tokens } from "../../theme";
import "./sidebar.css"
import CheckIcon from '@mui/icons-material/Check';
import PeopleOutlinedIcon from "@mui/icons-material/PeopleOutlined";
import BookIcon from '@mui/icons-material/Book';
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import SchoolIcon from '@mui/icons-material/School';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import BarChartOutlinedIcon from "@mui/icons-material/BarChartOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import Cookies from "js-cookie";
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import HistoryEduIcon from '@mui/icons-material/HistoryEdu';
import RoomPreferencesIcon from '@mui/icons-material/RoomPreferences';
import CottageIcon from '@mui/icons-material/Cottage';
import ReceiptIcon from '@mui/icons-material/Receipt';
const Item = ({ title, to, icon, selected, setSelected }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  let centerName = Cookies.get('CenterName');
  const handleItemClick = () => {
    setSelected((prevSelected) => {
      localStorage.setItem('selected', title);
      return title;
    });
  };

  return (
    <MenuItem
      active={selected === title}
      style={{
        color: colors.grey[100],
      }}
      onClick={handleItemClick}
      icon={icon}
    >
      <Typography>{title}</Typography>
      <Link to={to} />
    </MenuItem>
  );
};
const Sidebar = () => {
  const theme = useTheme();
  const storedSelected = localStorage.getItem('selected');
  const colors = tokens(theme.palette.mode);
  const [isCollapsed, setIsCollapsed] = useState(false);
  const [selected, setSelected] = useState("Quản lý giáo viên");
  const rolename = Cookies.get('roleName');
  const CenterName = Cookies.get('CenterName');

  useEffect(() => {
    if (storedSelected) {
      setSelected(storedSelected);
    } else {

      setSelected("Quản lý giáo viên")
    }
  }, [storedSelected]);
  return (

    <Box style={{ scrollBehavior: 'smooth', display: 'flex', flexDirection: 'column' }}
      sx={{
        "& .pro-sidebar-inner": {
          background: `${colors.primary[400]} !important`,
        },
        "& .pro-icon-wrapper": {
          backgroundColor: "transparent !important",
        },
        "& .pro-inner-item": {
          padding: "5px 35px 5px 20px !important",
        },
        "& .pro-inner-item:hover": {
          color: "#2746C0 !important",
        },
        "& .pro-menu-item.active": {
          color: "#2746C0 !important",
        },
      }}
    >
      <ProSidebar collapsed={isCollapsed}>
        <Menu iconShape="square">
          {/* LOGO AND MENU ICON */}
          <MenuItem

            icon={isCollapsed ? <MenuOutlinedIcon /> : undefined}
            style={{
              margin: "10px 0 20px 0",
              color: colors.grey[100],
            }}
          >
            {!isCollapsed && (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px"
              >
                <Typography variant="h3" style={{ marginLeft: '30px' }} color={colors.grey[100]}>
                  <img style={{ fontSize: '26px' }} src="https://pitech.edu.vn:82/Images/LogoCenter.png"></img>
                </Typography>
              </Box>
            )}
          </MenuItem>
          {!isCollapsed && (
            <Box mb="25px">

              <Box display="flex" justifyContent="center" alignItems="center">
              <Typography variant="h3" color={colors.grey[100]}>
                  {CenterName}
                </Typography>

              </Box>

            </Box>
          )}
          <Box paddingLeft={isCollapsed ? undefined : "10%"}>
            {rolename === 'Super Admin' && (
              <>
                <Item
                  title="Trang chủ"
                  to="/"
                  icon={<CottageIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý trung tâm"
                  to="/center"
                  icon={<PeopleOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />

                <Item
                  title="Quản lý nhân viên"
                  to="/manageteam"
                  icon={<PeopleOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý tin tức"
                  to="/blogadmin"
                  icon={<BookIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý câu hỏi"
                  to="/FAQadmin"
                  icon={<QuestionAnswerIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Bảng điều khiển"
                  to="/dashboard"
                  icon={<BarChartOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
              </>
            )}
            {rolename === 'Employee' && (
              <>
                <Item
                  title="Quản lý giáo viên"
                  to="/"
                  icon={<PeopleOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý học sinh"
                  to="/student"
                  icon={<SchoolIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý giáo trình"
                  to="/Syllabusadmin"
                  icon={<HistoryEduIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý khóa học"
                  to="/courseadmin"
                  icon={<MenuBookIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />

                <Item
                  title="Bảng điều khiển"
                  to="/dashboard"
                  icon={<BarChartOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Điểm danh"
                  to="/attendadmin"
                  icon={<CheckIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />

                <Item
                  title="Quản lý lớp học"
                  to="/classadmin"
                  icon={<MeetingRoomIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quản lý hóa đơn"
                  to="/invoice"
                  icon={<ReceiptIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />


                <Item
                  title="Quản lý phòng học"
                  to="/roomadmin"
                  icon={<RoomPreferencesIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />

                <Item
                  title="Lịch học"
                  to="/calendar"
                  icon={<CalendarTodayOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />

              </>
            )}
          </Box>
        </Menu>
      </ProSidebar>
    </Box>
  );
};

export default Sidebar;
