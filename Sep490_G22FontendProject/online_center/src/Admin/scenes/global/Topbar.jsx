import { Box, IconButton, useTheme } from "@mui/material";
import { useContext } from "react";
import { ColorModeContext, tokens } from "../../theme";

import Cookies from "js-cookie";
import LogoutIcon from '@mui/icons-material/Logout';
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import InputBase from "@mui/material/InputBase";
import { Link } from "react-router-dom";



const Topbar = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const colorMode = useContext(ColorModeContext);
  const rolename = Cookies.get('roleName'); 
  const removesession = () => {
    Cookies.remove('decoded');
    Cookies.remove('jwttoken');
    Cookies.remove('username');
    Cookies.remove('roleName');

    localStorage.removeItem("selected");

  }
  return (
    <Box  display="flex" justifyContent="space-between" p={2}>
      {/* SEARCH BAR */}
      <Box
        display="flex"
        backgroundColor={colors.primary[400]}
        borderRadius="3px"
        
      >
   
       
      </Box>

      {/* ICONS */}
      <Box display="flex">
      {rolename === 'Employee' && (
        <IconButton >
        
          <Link style={{ textDecoration: 'none' }} to={`/AdminProfile`}>
            <PersonOutlinedIcon />
          </Link>
        </IconButton>
      )}
        <IconButton>
          <a style={{ textDecoration: 'none' }} onClick={removesession} href="/login_admin"><LogoutIcon /></a>
        </IconButton>
      </Box>
    </Box>
  );
};

export default Topbar;
