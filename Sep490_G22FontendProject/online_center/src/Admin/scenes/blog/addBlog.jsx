import React, { useState, useEffect } from "react";
import Editor from "../test_cket_editor/editor.js";
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from "react-router-dom";
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { AddBlogAdmin } from "../../../api/apiClient/Banner/BannerAPI.js";
import FooterAdmin from "../../FooterAdmin.jsx";
import Header from "../../components/Header.jsx";

export default function AddBlog() {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [editorData, setEditorData] = useState("");
  const [formData, setFormData] = useState({
    title: "",
    description: "",
    shortDescription: "",
    imageFile: null,
    url: "",
    position: null,
  });
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const handleInputChange = (e) => {
    const { name, value, files } = e.target;
    setFormData({
      ...formData,
      [name]: name === "imageFile" ? files[0] : value,
    });
  };
  const { cateId } = useParams();

  const navigate = useNavigate();
  const handleEditorChange = (data) => {
    setEditorData(data.replace(/"/g, "'"));
  };

  useEffect(() => {
    try {
      setEditorLoaded(true);
    } catch (error) {
      console.log("error", error.message);
    }
  }, []);

  const handleSubmit = async (event) => {
    try {
      event.preventDefault();
      const formDataToSend = new FormData();
      formDataToSend.append("title", formData.title);
      formDataToSend.append("description", editorData.replace(/"/g, "'"));
      formDataToSend.append("shortDescription", formData.shortDescription);
      formDataToSend.append("imageFile", formData.imageFile);
      formDataToSend.append("cateId", cateId);
      if (formData.url != null) {
        formDataToSend.append("url", formData.url);
      } if (formData.position != null) {
        formDataToSend.append("position", formData.position);
      }
      if (!formData.title.trim() || !formData.shortDescription.trim()) {
        toast.error("Vui lòng điền đầy đủ thông tin")
        return;
      }
      console.log(formDataToSend);
      const data = await AddBlogAdmin(formDataToSend);
      if (data.isSuccess) {
        toast.success("Tạo thành công");
        await new Promise(resolve => setTimeout(resolve, 1000));
        navigate("/blogadmin");
      } else {
        toast.error("Lỗi khi add blog");
      }
    } catch (error) {
      console.log("Error:", error);
      toast.error(error.response.data.message);
    }
  };

  return (

    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <ToastContainer />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <Header title="Tin Tức" subtitle="Thêm tin tức" />

                <div className="row">
                  <div className="col-12">
                    {/* Page title */}


                    {/* Form START */}
                    <form onSubmit={handleSubmit}>

                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">

                              <div className="col-md-12">
                                <label className="form-label">Tiêu đề  <span className='required-field'>*</span></label>
                                <input
                                  type="text"
                                  className="form-control"
                                  maxLength={500}
                                  name="title"
                                  placeholder="Nhập tiêu đề"
                                  value={formData.title}
                                  onChange={handleInputChange} required
                                />
                              </div>
                              {/* Mobile number */}

                              <div className="col-md-12">
                                <label className="form-label">Mô tả chung  <span className='required-field'>*</span></label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="shortDescription"
                                  placeholder="Nhập nội dung"
                                  value={formData.shortDescription}
                                  maxLength={500}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              {(cateId === '3' || cateId === '2') && (
                                <div className="col-md-12">
                                  <label className="form-label">URL</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="url"
                                    value={formData.url}
                                    onChange={handleInputChange}
                                  />
                                  <br />
                                  <label className="form-label">Position</label>
                                  <input
                                    type="number"
                                    max={10}
                                    name="position"
                                    className="form-control"
                                    value={formData.position}
                                    onChange={handleInputChange}
                                  />
                                </div>
                              )}
                              <div className="col-md-12">
                                <label className="form-label">Mô tả </label>
                                <Editor
                                  name="description"
                                  onChange={handleEditorChange}
                                  editorLoaded={editorLoaded}
                                  value={formData.description}
                                />
                              </div>
                              <div className="col-md-12">
                                <label className="form-label">Hình ảnh </label>
                                <input
                                  type="file"
                                  className="form-control"
                                  name="imageFile"
                                  placeholder="Enter image"
                                  onChange={handleInputChange}
                                />
                              </div>
                            </div>{" "}

                          </div>
                        </div>

                      </div>{" "}
                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                        <a className='backtolist' href='/blogadmin'> {'<< Quay về danh sách'}</a>
                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                          <i className="addicon fa fa-save"></i>Lưu
                        </button>
                      </div>

                    </form>
                    {/* Form END */}
                  </div>


                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
