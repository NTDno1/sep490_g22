import React, { useState, useEffect } from "react";
import Editor from "../test_cket_editor/editor.js";
import { useNavigate, useParams } from 'react-router-dom';

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ToastContainer, toast } from "react-toastify";
import { UpdateBlogAdmin, ViewBlogDetail } from "../../../api/apiClient/Banner/BannerAPI.js";
import FooterAdmin from "../../FooterAdmin.jsx";
import Header from "../../components/Header.jsx";

export default function UpdateBlog() {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const navigate = useNavigate();
  const [editorData, setEditorData] = useState("");
  const [formData, setFormData] = useState({
    title: "",
    description: "",
    shortDescription: "",
    imageFile: null,
    url: "",
    position: null,
  });

  // Thêm state để lưu trạng thái cập nhật
  const [isUpdating, setIsUpdating] = useState(false);

  // Thêm state để lưu dữ liệu blog chi tiết
  const [blogDetail, setBlogDetail] = useState({});
  const { code } = useParams();
  // Thêm một hàm để gọi API lấy thông tin blog chi tiết
  useEffect(() => {
    fetchDataBlog(code);
  }, [code]);
  const fetchDataBlog = async (code) => {
    try {
      const data = await ViewBlogDetail(code);
      if (data.result) {
        setBlogDetail(data.result);
      }
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    setFormData({
      title: blogDetail.title || "",
      shortDescription: blogDetail.shortDescription || "",
      description: blogDetail.description || "",
      imageFile: blogDetail.img || null,
      url: blogDetail.url || null,
      position: blogDetail.position || null,
    });
  }, [blogDetail]);

  const handleUpdate = async (e) => {
    try {
      e.preventDefault();
      setIsUpdating(true);
      const formDataToSend = new FormData();
      formDataToSend.append("title", formData.title);
      formDataToSend.append("description", editorData.replace(/"/g, "'"));
      formDataToSend.append("shortDescription", formData.shortDescription);
      formDataToSend.append("imageFile", formData.imageFile);
      if (formData.url != null) {
        formDataToSend.append("url", formData.url);
      } if (formData.position != null) {
        formDataToSend.append("position", formData.position);
      }
      // Gọi API để cập nhật blog
      const data = await UpdateBlogAdmin(code, formDataToSend);
      if (data.isSuccess) {
        toast.success("Cập nhật thành công");
        await new Promise(resolve => setTimeout(resolve, 1500));
        window.location.reload();
      } else {
        toast.error("Cập nhật thất bại.");
        setIsUpdating(false);
      }
    } catch (error) {
      toast.error(error.response.data.message);
      setIsUpdating(false);
    }
  };

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  // Thêm hàm xử lý sự kiện cho việc nhập liệu
  const handleInputChange = (e) => {
    const { name, value, files } = e.target;
    setFormData({
      ...formData,
      [name]: name === "imageFile" ? files[0] : value,
    });
  };
  // Thêm hàm xử lý sự kiện cho việc thay đổi trình soạn thảo
  const handleEditorChange = (data) => {
    setEditorData(data.replace(/"/g, "'"));
  };

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <ToastContainer />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <Header title="Tin Tức" subtitle="Cập nhật tin tức" />

                <div className="row">
                  <div className="col-12">
                    {/* Page title */}


                    {/* Form START */}
                    <form onSubmit={handleUpdate}>

                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">

                              <div className="col-md-12">
                                <label className="form-label">Tiêu đề  <span className='required-field'>*</span></label>
                                <input
                                  type="text"
                                  className="form-control"
                                  maxLength={500}
                                  name="title"
                                  placeholder="Enter title"
                                  value={formData.title}
                                  onChange={handleInputChange} required
                                />
                              </div>
                              {blogDetail.url !== null && (
                                <div className="col-md-12">
                                  <label className="form-label">URL</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="url"
                                    value={formData.url}
                                    onChange={handleInputChange}
                                  />
                                </div>
                              )}
                              {blogDetail.position !== null && (
                                <div className="col-md-12">
                                  <label className="form-label">Position*</label>
                                  <input
                                    type="number"
                                    max={10}
                                    name="position"
                                    className="form-control"
                                    value={formData.position}
                                    onChange={handleInputChange}
                                  />
                                </div>
                              )}

                              <div className="col-md-12">
                                <label className="form-label">Mô tả chung*</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  maxLength={500}
                                  name="shortDescription"
                                  placeholder="Enter short description"
                                  value={formData.shortDescription}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              <div className="col-md-12">
                                <label className="form-label">Mô tả</label>
                                <Editor

                                  name="description"
                                  onChange={handleEditorChange}
                                  editorLoaded={editorLoaded}
                                  value={formData.description}
                                />
                              </div>
                              <div className="col-md-12">
                                {blogDetail.img && typeof blogDetail.img === 'string' && blogDetail.img !== "https://pitech.edu.vn:82/api/Images/" && (
                                  <div style={{ marginTop: '10px' }}>
                                    <img
                                      src={blogDetail.img}
                                      alt="Selected Image"
                                      style={{ width: '200px', height: '200px' }}
                                    />
                                  </div>
                                )}
                                <label className="form-label">Hình ảnh</label>
                                <input
                                  type="file"
                                  className="form-control"
                                  name="imageFile"
                                  value={formData.img}
                                  onChange={handleInputChange}
                                />
                              </div>
                            </div>{" "}

                          </div>
                        </div>

                      </div>{" "}


                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                        <a className='backtolist' href='/blogadmin'> {'<< Quay về danh sách'}</a>
                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                          <i className="addicon fa fa-save"></i>Lưu
                        </button>
                      </div>

                    </form>
                    {/* Form END */}
                  </div>
                </div>
              </div>
            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
