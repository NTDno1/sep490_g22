import React, { useEffect, useState } from "react";
import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link, useNavigate } from "react-router-dom";
import { clean } from 'diacritic';
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import { ChangeStatus, DeleteBlog, ListBlogAdmin, UpdateBlogAdmin, ViewBlogCate } from "../../../api/apiClient/Banner/BannerAPI";
import FooterAdmin from "../../FooterAdmin";
const BlogAdmin = () => {
  const [blog, setBlog] = useState([]);
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [selectedCategory, setSelectedCategory] = useState([]); // State for the selected category
  const [selectedCategoryId, setSelectedCategoryId] = useState("");
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchBlog, setSearchBlog] = useState("");
  const apiRef = useGridApiRef();

  useEffect(() => {
    
    getBlogAdminSide();
  
  }, [selectedCategoryId, apiRef]);
  const getBlogAdminSide = async () => {
    try {
      const data = await ListBlogAdmin(selectedCategoryId);
      
      if (data.result.length > 0) {

        setOriginalClasses(data.result);
        setBlog(data.result);
        apiRef.current.setPageSize(7);
        
    
      } else {
        setBlog([]);
      }
    } catch (error) {
    
    }
  }
  const handleCategoryChange = (e) => {
    const selectedCategoryId = e.target.value;
    setSelectedCategoryId(selectedCategoryId);
  };

  useEffect(() => {
    getCateBlog();
  }, []);
  const getCateBlog = async () => {
    try {
      const data = await ViewBlogCate();
      if (data.result) {
        setSelectedCategory(data.result);
        setSelectedCategoryId(data.result[0].id);
      }
    } catch (error) {
      console.log(error.response.data.message);
    }
  }
  const handleChangeActiveStatus = async (id) => {
    if (window.confirm("Bạn muốn blog này hoạt đông ?")) {
      try {
        const response = await ChangeStatus(id, "1")
        toast.success("Cập nhật thành công");
        getBlogAdminSide();
      }
      catch (error) {
        toast.error(error.response.data.message);
      }

    }
  }

  const handleChangeDeActiveStatus = async (id) => {
    if (window.confirm("Bạn muốn blog này ngừng hoạt đông ?")) {
      try {
        const response = await ChangeStatus(id, "0")
        toast.success("Cập nhật thành công");
        getBlogAdminSide();
      }
      catch (error) {
        toast.error(error.response.data.message);
      }
    }
  }

  const handleRemoveClick = async () => {
    if (window.confirm("Bạn có chắc muốn xóa những bài đăng này?")) {
      try {
        const response = await DeleteBlog(selectedBlogCodes.map(String));
        toast.success('Xóa bài đăng thành công');
        getBlogAdminSide();
      } catch (error) {
        toast.error(error.response.data.message);
      }
    }
  };


  const dataWithBlog = blog.map((item, index) => ({
    ...item,
    blogId: index + 1,
  }));
  const handleSelectionChange = (selection) => {
    setSelectedBlogCodes(selection);
  };

  const handleRemoveSelected = () => {
    try {
      if (selectedBlogCodes.length === 0) {
        alert("Vui lòng chọn ít nhất một blog để xóa.");
        return;
      }
      if (selectedBlogCodes.length > 0) {
        handleRemoveClick(selectedBlogCodes);
      } else {
      }
    } catch (error) {
      console.log(error);
    }
  };

  const columns = [
    {
      headerName: "STT",
      field: "blogId",
      align: "center",
      flex: 0.5,
    },
    {
      field: "title",
      headerName: "Tiêu đề",
      flex: 1.5,
      cellClassName: "name-column--cell",
    },
    {
      field: "blogCode",
      headerName: "Code",
      headerAlign: "left",
      align: "left",
      flex: 1.5,
    },
    {
      field: "createdAt",
      headerName: "Ngày tạo",
      align: "center",
      flex: 1,
    },
    {
      field: "createdBy",
      headerName: "Người tạo",
      flex: 1,
    },
    {
      field: "lastModifiedAt",
      headerName: "Ngày cập nhật",
      align: "center",
      flex: 1,
    },
    {
      field: "lastModifiedBy",
      headerName: "Người cập nhật",
      flex: 1,
    },
    
    {
      field: "status",
      headerName: "Trạng thái",
      renderCell: (params) => (
        <div style={{ color: params.row.status === 1 ? "green" : "red" }}>
          {params.row.status === 1 ? "Kích hoạt" : "Đóng"}
        </div>
      ),
      flex: 1
    },

    // {
    //   field: "Remove",
    //   headerName: "Remove",
    //   flex: 0.5,
    //   renderCell: (params) => (
    //     <UilTimesCircle
    //       style={{ cursor: 'pointer' }}
    //       onClick={() => handleRemoveClick([params.row.blogCode])}
    //     />
    //   ),
    // },
    {
      field: "Action",
      headerName: "Hành động",
      flex: 0.75,
      renderCell: (params) => (
        params.row.status === 1 ? (

          <UilUnlock
            style={{ cursor: 'pointer', color: 'green' }}
            onClick={() => handleChangeDeActiveStatus([params.row.blogCode])}
          />
        ) : (
          params.row.status === 0 ? (
            <UilLock
              style={{ cursor: 'pointer', color: 'red' }}
              onClick={() => handleChangeActiveStatus([params.row.blogCode])}
            />
          ) : null
        )

      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 0.75,
      renderCell: (params) => <a href={`/updateblog/${params.row.blogCode}`}>
        <UilEdit />
      </a>,
    },
  ];
  const filterNameBlog = (event) => {
    event.preventDefault();
    if (searchBlog) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.title.toLowerCase()).includes(clean(searchBlog.toLowerCase()))
      );
      setBlog(filteredClasses);
    } else {
      setBlog(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>

                <Box m="20px">

                  <Header title="Bài đăng" subtitle="Quản lý bài đăng" />
                  <ToastContainer />

                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box display="flex" width="60%" >
                      <Link to={`/blogadmin/addBlog/${selectedCategoryId}`} style={{ display: 'flex', textAlign: 'center' }} className="btn btn-success m-btn">
                        <i className="fa fa-plus"></i> Thêm mới
                      </Link>
                      <button style={{ marginLeft: '20px' }} className="btn btn-danger m-btn" onClick={handleRemoveSelected}>
                        <i className="fa fa-trash"></i> Xóa blog
                      </button>

                      <select
                        className="form-control"
                        style={{ marginLeft: '20px', width: "30%" }}
                        id="select-course"
                        value={selectedCategoryId}
                        onChange={handleCategoryChange}
                      >
                        {selectedCategory.map((category) => (
                          <option key={category.id} value={category.id.toString()}>
                            {category.name}
                          </option>
                        ))}
                      </select>
                    </Box>
                    <form className="filter-search" onSubmit={filterNameBlog}>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        onChange={(e) => setSearchBlog(e.target.value)}
                        value={searchBlog}
                      />
                    </form>
                  </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important"
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                    {blog.length === 0 ? (
                      <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                        Không có dữ liệu
                      </div>
                    ) : (
                      <DataGrid pageSizeOptions={[7, 20, 25]} apiRef={apiRef} checkboxSelection rows={dataWithBlog} columns={columns} onRowSelectionModelChange={handleSelectionChange} />

                    )}
                  </Box>
                </Box>
              </Box>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default BlogAdmin;
