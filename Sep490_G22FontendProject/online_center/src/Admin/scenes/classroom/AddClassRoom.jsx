import React, { useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../theme";
import Sidebars from "../global/Sidebars";
import Topbar from "../global/Topbar";
import { addClassRoom, addRoom, GetAllRoom } from "../../../api/apiClient/Room/roomAPI";

export default function AddClassRoom() {
    const [formData, setFormData] = useState({
        name: "",
        typeId: null,
    });
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    const [room, setRoom] = useState([]);
    const navigate = useNavigate();

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        const parsedValue = name === "status" ? parseInt(value, 10) : value;
        setFormData({ ...formData, [name]: parsedValue });
    };
    useEffect(() => {
        fetchDataRoom();
    }, []);

    const fetchDataRoom = async () => {
        try{
        const data = await GetAllRoom();
        if (data.result.results.length>0) {
            setRoom(data.result.results);
            setFormData({ ...formData, typeId: data.result.results[0].id });
        }}catch(error){
            toast.error(error.response.data.message);
        }

    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        if (!formData.name.trim()) {
            toast.error("Vui lòng điền đầy đủ thông tin");
            return;
        }
        try {
            const response = await addClassRoom(formData);
            toast.success('Thêm phòng học thành công');
            await new Promise(resolve => setTimeout(resolve, 1000));
            navigate('/roomadmin');
        }
        catch (error) {
            toast.error(error.response?.data?.message || "Có lỗi xảy ra khi thêm phòng học");
        }
    };

    return (
        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <div className="app">
                    <div style={{ display: "flex" }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main
                            className="content"
                            style={{ minWidth: "170.3vh", height: "100vh" }}
                        >
                            <Topbar setIsSidebar={setIsSidebar} />
                            <div className="container">
                                <ToastContainer />
                                <h1>Thêm phòng học</h1>
                                <div className="row">
                                    <div className="col-12">
                                        <form onSubmit={handleSubmit}>
                                            <div className="row mb-5 gx-12">
                                                <div className="col-xxl-12 mb-5 mb-xxl-0">
                                                    <div className="bg-secondary-soft px-4 py-5 rounded">
                                                        <div className="row g-3">
                                                            <div className="col-md-12">
                                                                <label className="form-label">Phòng học <span className='required-field'>*</span></label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="name"
                                                                    placeholder="Enter name"
                                                                    value={formData.name}
                                                                    onChange={handleInputChange}
                                                                    maxLength={200}
                                                                    required
                                                                />
                                                            </div>
                                                            <div className="col-md-12">
                                                                <label className="form-label">Tòa <span className='required-field'>*</span></label>
                                                                <select
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="typeId"
                                                                    value={formData.typeId}
                                                                    onChange={handleInputChange}
                                                                    required
                                                                >
                                                                    <option value="" disabled>
                                                                        Chọn tòa
                                                                    </option>
                                                                    {room.map((roomItem) => (
                                                                        <option key={roomItem.id} value={roomItem.id}>
                                                                            {roomItem.name}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                                                <button style={{ background: '#2746C0', padding: '10px 20px' }} type="button" onClick={() => navigate(`/roomadmin`)} className="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                                    <span>&nbsp;
                                                        <span>Quay về danh sách</span>
                                                        <i className="la la-arrow-right"></i>
                                                    </span>
                                                </button>
                                                <button style={{ background: '#2746C0', padding: '10px 20px' }} type="submit" className="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                                    <span>&nbsp;
                                                        <span>Lưu</span>
                                                        <i className="la la-arrow-right"></i>
                                                    </span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    );
}
