import React, { useState, useEffect } from "react";
import Editor from "../test_cket_editor/editor.js";
import { useNavigate, useParams } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../theme";
import Sidebars from "../global/Sidebars";
import Topbar from "../global/Topbar";
import { GetAllRoom, GetClassRoomById, updateClassRoom } from "../../../api/apiClient/Room/roomAPI.js";
const initialFieldValues = {
    name: '',
    typeId: null,
};

export default function UpdateClassRoom() {
    const [editorLoaded, setEditorLoaded] = useState(false);
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);
    const [values, setValues] = useState(initialFieldValues);
    const [isUpdating, setIsUpdating] = useState(false);
    const [room, setRoom] = useState([]);
    const { id } = useParams();
    const navigate = useNavigate();
    useEffect(() => {
        fetchDataRoom(id);
        fetchDataForRoom();
    }, [id]);

    const fetchDataForRoom = async () => {
        try {
            const data = await GetAllRoom();
            if (data.result.results.length>0) {
                setRoom(data.result.results);
            }
        } catch (error) {
            toast.error(error.response.data.message);
        }
    }
    const fetchDataRoom = async (id) => {
        try {
            const data = await GetClassRoomById(id);
            if (data && data.result) {
                const result = data.result;
                setValues({
                    name: result.name,
                    typeId: result.typeId,
                });
            }
        } catch (error) {
            toast.error(error.response.data.message);
        }
    };
    const handleUpdate = async (id, formData) => {
        try {
            setIsUpdating(true);
            const response = await updateClassRoom(id, formData);
            if (response.isSuccess) {
                toast.success(response.message);
                await new Promise(resolve => setTimeout(resolve, 1500));
                window.location.reload();
            } else {
                toast.error(response.message);
            }
        } catch (error) {
            toast.error(error.response.data.message);
        } finally {
            setIsUpdating(false);
        }
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        const formData = {
            name: values.name,
            typeId: values.typeId,
        };
        handleUpdate(id, formData);
    };
    const handleInputChange = e => {
        const { name, value } = e.target;

        setValues({
            ...values,
            [name]: value
        })
    }
    return (
        <ColorModeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <div className="app">
                    <div style={{ display: 'flex' }}>
                        <Sidebars isSidebar={isSidebar} />
                        <main className="content" style={{ minWidth: '170.3vh', height: '100vh' }}>
                            <Topbar setIsSidebar={setIsSidebar} />
                            <div className="container">
                                <ToastContainer />
                                <h1>Cập nhật phòng học</h1>

                                <div className="row">
                                    <div className="col-12">
                                        {/* Form START */}
                                        <form onSubmit={handleFormSubmit}>
                                            <div className="row mb-5 gx-12">
                                                {/* Contact detail */}
                                                <div className="col-xxl-12 mb-5 mb-xxl-0">
                                                    <div className="bg-secondary-soft px-4 py-5 rounded">
                                                        <div className="row g-3">

                                                            <div className="col-md-12">
                                                                <label className="form-label">Phòng học <span className='required-field'>*</span></label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="name"
                                                                    placeholder="Enter name"
                                                                    value={values.name}
                                                                    maxLength={500}
                                                                    onChange={handleInputChange}
                                                                    required
                                                                />
                                                            </div>
                                                            <div className="col-md-12">
                                                                <label className="form-label">Tòa <span className='required-field'>*</span></label>
                                                                <select
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="typeId"
                                                                    value={values.typeId}
                                                                    onChange={handleInputChange}
                                                                    required
                                                                >
                                                                    <option value="" disabled>
                                                                        Chọn tòa học
                                                                    </option>
                                                                    {room.map((roomItem) => (
                                                                        <option key={roomItem.id} value={roomItem.id}>
                                                                            {roomItem.name}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </div>
                                                        </div>{" "}

                                                    </div>
                                                </div>

                                            </div>{" "}


                                            <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                                                <button style={{ background: '#2746C0', padding: '10px 20px' }} type="button" onClick={() => navigate(`/roomadmin`)} className="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                                    <span>&nbsp;
                                                        <span>Quay về danh sách</span>
                                                        <i className="la la-arrow-right"></i>
                                                    </span>
                                                </button>
                                                <button style={{ background: '#2746C0', padding: '10px 20px' }} type="submit" className="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                                    <span>&nbsp;
                                                        <span>Lưu</span>
                                                        <i className="la la-arrow-right"></i>
                                                    </span>
                                                </button>
                                            </div>

                                        </form>
                                        {/* Form END */}
                                    </div>


                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </ThemeProvider>
        </ColorModeContext.Provider>
    );
}
