import React, { useEffect, useState } from "react";
import { Box, IconButton, InputBase, Typography, useTheme, Select, MenuItem } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import SearchIcon from "@mui/icons-material/Search";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link, useNavigate } from "react-router-dom";
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { DeleteClassRoom, GetAllRoom, GetAllRoomAdminSide } from "../../../api/apiClient/Room/roomAPI";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
const ClassRoomAdmin = () => {
  const [room, setRoom] = useState([]);
  const [theme, colorMode] = useMode();
  const [selectedId, setSelectedId] = useState([]);
  const [isSidebar, setIsSidebar] = useState(true);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchRoom, setSearchRoom] = useState("");
  const apiRef = useGridApiRef();
  useEffect(() => {
    fetchDataRoom();
    apiRef.current.setPageSize(7);
  }, []);

  const fetchDataRoom = async () => {
    try{
    const data = await GetAllRoomAdminSide();
    if (data.result.length>0) {
      setRoom(data.result);
      setOriginalClasses(data.result);
    }
  }catch(error){
    toast.error(error.response.data.message);
  }
}

  const handleRemoveClick = async (id) => {
    if (window.confirm("Bạn có chắc muốn xóa những phòng học này??")) {
      try {
        const response = await DeleteClassRoom(id);
        toast.success('Xóa phòng thành công');
        fetchDataRoom();
      }
      catch (error) {
        toast.error(error.response.data.message);
      }
    }
  };
  const dataWithRoom = room.map((item, index) => ({
    ...item,
    RoomId: index + 1,
  }))
  
  const columns = [
    { field: "RoomId", headerName: "STT",align:'center' },
    {
      field: "name",
      headerName: "Phòng học",
    
      flex: 1,
    },
    {
      field: "typeName",
      headerName: "Tòa",
      flex: 1,
    },
    {
      field: "createdAt",
      headerName: "Ngày khởi tạo",
      align: "center",
      flex: 1,
    },
    {
      field: "createdBy",
      headerName: "Người khởi tạo",
      flex: 1,
    },
    {
      field: "lastModifiedAt",
      headerName: "Ngày Cập Nhật",
      align: "center",
      flex: 1,
    },
    {
      field: "lastModifiedBy",
      headerName: "Người Cập Nhật",
      flex: 1,
    },
    {
      field: "status",
      headerName: "Hành động",
      flex: 1,
      renderCell: (params) => (
        params.row.status === 1 ? (

          < DeleteOutlineIcon
              style={{ cursor: 'pointer' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
        ) : (
          params.row.status === 2 ? (
            <UilLock
              style={{ cursor: 'pointer', color: 'red' }}
            />
          ) : null
        )

      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => <a href={`/updateRoom/${params.row.id}`}>
        <UilEdit />
      </a>,
    },
  ];
  const filterNameRoom = (event) => {
    event.preventDefault();
    if (searchRoom) {
      const filteredClasses = originalClasses.filter((val) =>
        val.name.toLowerCase().includes(searchRoom.toLowerCase())
      );
      setRoom(filteredClasses);
    } else {
      setRoom(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <div style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" style={{ minWidth: '170.3vh', height: '100vh' }}>
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>
                <Box m="20px">

                  <Header title="Phòng học" subtitle="Quản lý phòng học" />
                  <ToastContainer />
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box display="flex" width="60%" >
                      <Link to={`/roomadmin/addroom`} className="btn btn-success m-btn">
                        <i className="fa fa-plus"></i> Thêm mới
                      </Link>
                    </Box>
                    <form onSubmit={filterNameRoom}>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        onChange={(e) => setSearchRoom(e.target.value)}
                        value={searchRoom}
                      />
                    </form>
                  </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                    <DataGrid pageSizeOptions={[7, 10, 25]} apiRef={apiRef}  rows={dataWithRoom} columns={columns} resizable />
                  </Box>
                </Box>
              </Box>

            </main>
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default ClassRoomAdmin;
