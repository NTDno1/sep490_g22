import React, { useState, useEffect } from "react";
import Editor from "../test_cket_editor/editor.js";
import { useParams } from 'react-router-dom';

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../theme";
import Sidebars from "../global/Sidebars";
import Topbar from "../global/Topbar";
import { GetRoomById } from "../../../api/apiClient/Room/roomAPI.js";
const initialFieldValues = {
  name: '',
  status: null
};

export default function UpdateRoom() {
  const [editorLoaded, setEditorLoaded] = useState(false);
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);
const [values, setValues] = useState(initialFieldValues);
const [isUpdating, setIsUpdating] = useState(false);
const { roomId } = useParams();

useEffect(() => {
  fetchDataRoom(roomId);
}, [roomId]);

const fetchDataRoom = async (roomId) => {
  try {
    const data = await GetRoomById(roomId);
    if (data && data.result) {
      const result = data.result;
      setValues({
        name: result.name,
        status: result.status,
      });
    }
  } catch (error) {
    console.error('Error fetching room details:', error);
  }
};

const handleUpdate = async (roomId, formData) => {
  try {
    setIsUpdating(true);
    const response = await UpdateRoom(roomId, formData);
    const data = await response.json();
    console.log("Update Response:", data);
    // Consider updating local state instead of reloading the page
    // setValues({ name: data.name, status: data.status });
  } catch (error) {
    console.error('Error updating room:', error);
  } finally {
    setIsUpdating(false);
  }
};

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const formData = {
      name: values.name,
      status: values.status,
    };
    handleUpdate(formData);
  };
  const handleInputChange = e => {
    const { name, value } = e.target;
   
    setValues({
      ...values,
      [name]: value
    })
  }
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <div style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" style={{ minWidth: '170.3vh', height: '100vh' }}>
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <h1>Update Room</h1>

                <div className="row">
                  <div className="col-12">
                    {/* Form START */}
                    <form onSubmit={handleFormSubmit}>
                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">

                              <div className="col-md-12">
                                <label className="form-label">Name</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="name"
                                  placeholder="Enter name"
                                  value={values.name}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              <div className="col-md-12">
                                <label className="form-label">Status</label>
                                <select
                                  type="text"
                                  className="form-control"
                                  name="status"
                                  value={values.status}
                                  onChange={handleInputChange}
                                  required
                                >
                                  <option value="" disabled>
                                    Select Status
                                  </option>
                                  <option value="1">Active</option>
                                  <option value="0">Inactive</option>
                                </select>

                              </div>
                            </div>{" "}

                          </div>
                        </div>

                      </div>{" "}


                      <div className="col-md-12" align="right">
                        <button style={{ background: '#34bfa3', padding: '10px 20px' }} type="submit" className="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                          <span>&nbsp;
                            <span>Submit</span>
                            <i className="la la-arrow-right"></i>
                          </span>
                        </button>
                      </div>

                    </form>
                    {/* Form END */}
                  </div>


                </div>
              </div>
            </main>
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
