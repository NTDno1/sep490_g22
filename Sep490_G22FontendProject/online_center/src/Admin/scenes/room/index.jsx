import React, { useEffect, useState } from "react";
import { Box, IconButton, InputBase, Typography, useTheme, Select, MenuItem } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import Header from "../../components/Header";
import SearchIcon from "@mui/icons-material/Search";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link, useNavigate } from "react-router-dom";

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { DeleteRoom, GetAllRoom } from "../../../api/apiClient/Room/roomAPI";
import { DeleteBlog } from "../../../api/apiClient/Banner/BannerAPI";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
const RoomAdmin = () => {
  const [room, setRoom] = useState([]);
  const [theme, colorMode] = useMode();
  const [selectedId, setSelectedId] = useState([]);
  const [isSidebar, setIsSidebar] = useState(true);
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchRoom, setSearchRoom] = useState("");

  useEffect(() => {
    fetchDataRoom();
  }, []);

  const fetchDataRoom = async () => {
    const data = await GetAllRoom();
    if (data.result.results) {
      setRoom(data.result.results);
    }
  }

  const handleRemoveClick = async () => {
    if (window.confirm("Are you sure you want to delete selected rooms?")) {
      const response = await DeleteRoom(selectedId.map(String));
        if (response.ok) {
          toast.success('Rooms deleted successfully');
          window.location.reload();
        } else {
          toast.error('Failed to delete rooms');
        }
      };
    };

  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);
    setSelectedId(selection);
  };

  const handleRemoveSelected = () => {
    if (selectedId.length > 0) {
      console.log("Button clicked!");
      handleRemoveClick(selectedId);
    } else {
      console.log("No rooms selected for removal");
    }
  };

  const columns = [
    { field: "id", headerName: "ID" },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
    },
    {
      field: "createdAt",
      headerName: "Create Date",
      align: "center",
      flex: 1,
    },
    {
      field: "createdBy",
      headerName: "Create by",
      flex: 1,
    },
    {
      field: "lastModifiedAt",
      headerName: "Update Date",
      align: "center",
      flex: 1,
    },
    {
      field: "lastModifiedBy",
      headerName: "Update By",
      flex: 1,
    },
    {
      field: "status",
      headerName: "Status",
      renderCell: (params) => (
        (params.row.status === 1) ? (
          <div>
            Actived
          </div>
        ) : <div >
          Inactived
        </div>
      ),
      flex: 1,
    },
    {
      headerName: "Hành động",
      flex: 1,
      renderCell: (params) => <a href={`/updateroom/${params.row.id}`}>
        <UilEdit />
      </a>,
    },
    {
      field: "Remove",
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => (
        <DeleteOutlineIcon
          style={{ cursor: 'pointer' }}
          onClick={() => handleRemoveClick([params.row.id])}
        />
      ),
    },
  ];
  const filterNameRoom = (event) => {
    event.preventDefault();
    if (searchRoom) {
      const filteredClasses = originalClasses.filter((val) =>
        val.title.toLowerCase().includes(searchRoom.toLowerCase())
      );
      setRoom(filteredClasses);
    } else {
      setRoom(originalClasses);
    }

  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <div style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" style={{ minWidth: '170.3vh', height: '100vh' }}>
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>

                <Box m="20px">

                  <Header title="Room" subtitle="Managing Room" />
                  <ToastContainer />

                  <Link to={`/roomadmin/addroom`} className="btn btn-success m-btn">
                    <i className="fa fa-plus"></i> Add Room
                  </Link>
                  <button style={{ marginLeft: '20px' }} className="btn btn-danger m-btn" onClick={handleRemoveSelected}>
                    <i className="fa fa-trash"></i> Remove
                  </button>
                  <Box>
                    <Box marginTop="20px"
                      width="20%"
                      marginRight="20px"

                      borderRadius="3px"
                    >
                    </Box>
                    <Box marginTop="20px"
                      width="40%"
                      display="flex"
                      borderRadius="3px"
                    >

                      <form onSubmit={filterNameRoom}>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Tìm kiếm tên phòng..."
                          onChange={(e) => setSearchRoom(e.target.value)}
                          value={searchRoom}
                        />
                      </form>
                    </Box>

                  </Box>
                  <Box
                    m="40px 0 0 0"
                    height="80vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important"
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: colors.blueAccent[700],
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: colors.blueAccent[700],
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                    <DataGrid checkboxSelection rows={room} columns={columns} onRowSelectionModelChange={handleSelectionChange} />
                  </Box>
                </Box>
              </Box>

            </main>
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default RoomAdmin;
