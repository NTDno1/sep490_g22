import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../theme";
import Sidebars from "../global/Sidebars";
import Topbar from "../global/Topbar";
import { addRoom } from "../../../api/apiClient/Room/roomAPI";

export default function AddRoom() {
  const [formData, setFormData] = useState({
    name: "",
    status: null,
  });
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

  const navigate = useNavigate();

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    const parsedValue = name === "status" ? parseInt(value, 10) : value;
    setFormData({ ...formData, [name]: parsedValue });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!formData.name) {
      toast.error("Vui lòng điền đầy đủ thông tin");
      return;
    }

    try {
      const data =  await addRoom(formData);
      if (data.isSuccess) {
        toast.success("Create successful");
        navigate("/roomadmin");
      } else {
        toast.error("Failed to add room.");
      }
    } catch (error) {
      toast.error("Failed to add room. Please try again later.");
    }
  };

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <div style={{ display: "flex" }}>
            <Sidebars isSidebar={isSidebar} />
            <main
              className="content"
              style={{ minWidth: "170.3vh", height: "100vh" }}
            >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <h1>Add your room</h1>

                <div className="row">
                  <div className="col-12">
                    <form onSubmit={handleSubmit}>
                      <div className="row mb-5 gx-12">
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <div className="col-md-12">
                                <label className="form-label">Name</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="name"
                                  placeholder="Enter name"
                                  value={formData.name}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              <div className="col-md-12">
                                <label className="form-label">Status</label>
                                <select
                                  type="text"
                                  className="form-control"
                                  name="status"
                                  value={formData.status}
                                  onChange={handleInputChange}
                                  required
                                >
                                  <option value="" disabled>
                                    Select Status
                                  </option>
                                  <option value="1">Active</option>
                                  <option value="0">Inactive</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12" align="right">
                        <button
                          style={{ background: "#34bfa3", padding: "10px 20px" }}
                          type="submit"
                          className="btn btn-success m-btn m-btn--custom m-btn--icon"
                          id="save-btn"
                        >
                          <span>
                            <span>Submit</span>
                            <i className="la la-arrow-right"></i>
                          </span>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
