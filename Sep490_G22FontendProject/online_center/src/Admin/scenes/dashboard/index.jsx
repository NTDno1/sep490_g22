import { Box, Button, IconButton, Typography, useTheme } from "@mui/material";
import { tokens } from "../../theme";

import DownloadOutlinedIcon from "@mui/icons-material/DownloadOutlined";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import Header from "../../components/Header";
import LineChart from "../../components/LineChart";
import GeographyChart from "../../components/GeographyChart";
import BarChart from "../../components/BarChart";
import StatBox from "../../components/StatBox";
import { useEffect, useState } from "react";
import fetchData from "../../../api/apiUtils";
import { convertDateFormat } from "../../../constants/constant";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";

import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ReportMember, ReportTeacher, TotalMember } from "../../../api/apiClient/Report/report";
import FooterAdmin from "../../FooterAdmin";

const Dashboard = () => {
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [data,setData] = useState([]);
  const[totalmember,setTotalMember] = useState([]);
  const [total, setTotal] = useState(0);
  let jwttoken = Cookies.get('jwttoken');
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
 
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login_admin');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
   getListTeacher()
   getTotalMember();
  },[]) 
  const getListTeacher = async() => {
    
    const data = await ReportTeacher();
    setData(data.result);
  }

  const getTotalMember = async() => {
    
    const data = await TotalMember();
    const datas = data.result
    const mockBarData = Object.keys(datas).map(key => {
      const item = {
          employee: key,
          Total: datas[key].total,             
      };         
      return item;
  });

  const calculatedTotal = mockBarData.reduce(
    (acc, item) => acc + item.Total,
    0
  );
  setTotal(calculatedTotal);
  setTotalMember(mockBarData);
  }
  return (
  <> 
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content">
                  <Topbar setIsSidebar={setIsSidebar} />
    <Box m="20px">
      {/* HEADER */}
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Header title="BẢNG ĐIỀU KHIỂN" subtitle="Đây là bảng điều khiển của bạn" />
        
      </Box>

      {/* GRID & CHARTS */}
      <Box
        display="grid"
        gridTemplateColumns="repeat(12, 1fr)"
        gridAutoRows="140px"
        gap="20px"
      >
        {/* ROW 1 */}
        {totalmember?.map((key) => (
        <Box
          gridColumn="span 3"
          backgroundColor={colors.primary[400]}
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <StatBox
            title={key.Total} 
            subtitle={key.employee} 
            progress={key.Total/total}
            increase={`+${((key.Total / total) * 100).toFixed(2)}%`}
            icon={
              <PersonAddIcon
                sx={{ color: colors.greenAccent[600], fontSize: "26px" }}
              />
            }
          />
        </Box>
        ))}

        {/* ROW 2 */}
        <Box
          gridColumn="span 12"
          gridRow="span 2"
          backgroundColor={colors.primary[400]}
        >
         
          <Box height="250px" m="-20px 0 0 0">
            <LineChart isDashboard={true} />
          </Box>
        </Box>
     
        <Box
gridColumn="span 4"
gridRow="span 2"
backgroundColor={colors.primary[400]}
overflow="auto"
        >
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            borderBottom={`4px solid ${colors.primary[500]}`}
            colors={colors.grey[100]}
            p="15px"
          >
            <Typography color={colors.grey[100]} variant="h5" fontWeight="600">
              Thống kê giờ dạy
            </Typography>
          </Box>
          {data.map((transaction, i) => (
            <Box
              key={`${transaction.txId}-${i}`}
              display="flex"
          
              alignItems="center"
              borderBottom={`4px solid ${colors.primary[500]}`}
              p="15px"
            >
              <Box width="55%">
                <Typography
                  color={colors.greenAccent[500]}
                  variant="h5"
                  fontWeight="600"
                >
                  {transaction.firtsName} {transaction.midName} {transaction.lastName} 
                </Typography>
                <Typography color={colors.grey[100]}>
                  {transaction.email}
                </Typography>
              </Box>
              <Box width="25%" color={colors.grey[100]}>{convertDateFormat(transaction.dob)}</Box>
              <Box width="20%"
                backgroundColor={colors.greenAccent[500]}
                p="5px 10px"
                borderRadius="4px"
              >
                {transaction.teacherTeachingHours.length} giờ
              </Box>
            </Box>
          ))}
        </Box>
        <Box
          gridColumn="span 4"
          gridRow="span 2"
          backgroundColor={colors.primary[400]}
        >
          <Typography
            variant="h5"
            fontWeight="600"
            sx={{ padding: "30px 30px 0 30px" }}
          >
           Số lượng
          </Typography>
          <Box height="250px" mt="-20px">
            <BarChart isDashboard={true} />
          </Box>
        </Box>
        <Box
          gridColumn="span 4"
          gridRow="span 2"
    
          
        >
         
          <Box height="200px">
            <GeographyChart isDashboard={true} />
          </Box>
        </Box>
      </Box>
    </Box>
    
    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
        </>
  );
};

export default Dashboard;
