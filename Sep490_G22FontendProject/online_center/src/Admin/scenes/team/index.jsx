import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { ToastContainer, toast } from "react-toastify";
import Header from "../../components/Header";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { UilLock } from '@iconscout/react-unicons'
import { UilUnlock } from '@iconscout/react-unicons'
import {
  UilEdit,
} from "@iconscout/react-unicons";
import Cookies from "js-cookie";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { getCenterAPI } from "../../../api/apiClient/Center/CenterAPI";
import { ActiveEmployee, DeactivateEmployee, DeleteEmployee, DeleteListEmployee, ListEmployee } from "../../../api/apiClient/Profile/Profile";
import FooterAdmin from "../../FooterAdmin";
import { convertDateFormat } from "../../../constants/constant";
const Member = () => {
  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const [employee, setEmployee] = useState([]);
  let jwttoken = Cookies.get('jwttoken');
  const [selectedBlogCodes, setSelectedBlogCodes] = useState([]);
  const [listcenter, setListCenter] = useState([]);
  const [center, setCenter] = useState("");
  const [searchName, setSearchName] = useState("");
  const apiRef = useGridApiRef();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);

  useEffect(() => {
   
    getListCenter();
    getEmployee();
   
  }, [center, searchName, apiRef])
  const getEmployee = async () => {
    try {
      const data = await ListEmployee(center, searchName);
      if (data.result.length > 0) {
        data.result = data.result.map((item) => {
          item.dob = convertDateFormat(item.dob);
          return item;
        });
        setEmployee(data.result)
        apiRef.current.setPageSize(7);
      } else {
        setEmployee([])
      }
    } catch (error) {

    }
  }
  const getListCenter = async () => {
    const data = await getCenterAPI();
    setListCenter(data.result)
  }

  console.log(center)
  const dataWithAdmin = employee.map((item, index) => ({
    ...item,
    AdminID: index + 1,
  }))
  const handleRemoveClick = async (id) => {

    if (window.confirm("Bạn có muốn xóa nhân viên này ?")) {
      try {
        const data = await DeleteEmployee(id);
        toast.success('Xóa thành công');
        getEmployee();
      } catch (error) {
        toast.error(error.errorMessages)
      }
    };
  };

  const handleActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn cho tài khoản này hoạt động?")) {
      try{
      await ActiveEmployee(id).then(response => {
        if (response.statusCode === 200) {
          toast.success('Thành công')
         
            getEmployee();
       
        } else {
          console.error('Failed to delete resource');
        }
      
      })
    }catch(error){}
    }
  }
  const handleDeActiveCenter = async (id) => {
    if (window.confirm("Bạn có muốn cho tài khoản này ngừng hoạt động?")) {
      try{
      await DeactivateEmployee(id).then(response => {
        if (response.statusCode === 200) {
          toast.success('Thành công')
          getEmployee();
        } else {
          console.error('Failed to delete resource');
        }
      })
    }catch(error){}
    }
  }
  const handleSelectionChange = (selection) => {
    console.log("Selected rows:", selection);
    setSelectedBlogCodes(selection);
  };

  const columns = [
    {
      headerName: "STT",
      field: "AdminID",
      align: "center",
      flex: 0.5,
    },
    {
      field: "name",
      headerName: "Họ và tên",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "userName",
      headerName: "Tên người dùng",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "phoneNumber",
      headerName: "Số điện thoại",
      align: 'center',
      flex: 1,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      align: "center",
      flex: 1,
    },

    {
      field: "centerName",
      headerName: "Tên trung tâm",
      flex: 1,
    },
    {
      field: "superAdminName",
      headerName: "Tên siêu quản trị viên",
      flex: 1,
    },
    {
      field: "Action",
      headerName: "Hành động",
      flex: 1,
      renderCell: (params) => (
        (params.row.status === 0 ? (
          <UilLock
            style={{ cursor: 'pointer', color: 'gray' }}

          />
        ) :
          params.row.status === 1 ? (

            <UilUnlock
              style={{ cursor: 'pointer', color: 'green' }}
              onClick={() => handleDeActiveCenter([params.row.id])}
            />
          ) : (
            params.row.status === 2 ? (

              <UilLock
                style={{ cursor: 'pointer', color: 'red' }}
                onClick={() => handleActiveCenter([params.row.id])}
              />
            ) : null
          )
        )

      ),
    },
    {
      headerName: "Chỉnh sửa",
      flex: 1,
      renderCell: (params) => (
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          {params.row.status === 0 && (
            <DeleteOutlineIcon
              style={{ cursor: 'pointer', fontSize: '25px' }}
              onClick={() => handleRemoveClick([params.row.id])}
            />
          )}
          <div style={{ width: '1px', height: '20px', backgroundColor: 'gray', margin: '0 10px' }}></div>
          <a href={`/updateMember/${params.row.id}`}>
            <UilEdit />
          </a>
        </div>
      ),
    },

  ];
  const filterNameEmployee = (e) => {
    e.preventDefault();

  };
  const handleSelectCenter = (e) => {
    const selectedValue = e.target.value;
    setCenter(selectedValue);
  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Box>
                <ToastContainer />
                <Box m="20px">
                  <Header title="SIÊU QUẢN TRỊ VIÊN" subtitle="Quản lý nhân viên trung tâm" />
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box display="flex" width="60%" >

                      <Link to='/addmember' style={{ display: 'flex', textAlign: 'center' }} className="btn btn-success m-btn">
                        <i className="fa fa-plus"></i> Thêm mới
                      </Link>
                      {/* <button style={{ marginLeft: '20px' }} className="btn btn-danger m-btn" onClick={handleRemoveListEmployee}>
                        <i className="fa fa-trash"></i> Xóa
                      </button> */}

                      <select
                        className="form-control"
                        style={{ marginLeft: '20px', width: "30%" }}
                        onChange={handleSelectCenter}
                        value={center}
                      >
                        <option value="">Tất cả</option>
                        {listcenter?.map((classItem, index) => (
                          <option key={index} value={classItem.id}>
                            {classItem.name}
                          </option>
                        ))}
                      </select>
                    </Box>
                    <form className="filter-search" onSubmit={filterNameEmployee}>
                      <input
                        type="text"
                        className="form-control m-input"
                        placeholder="Tìm kiếm tên nhân viên..."
                        onChange={(e) => setSearchName(e.target.value)}
                        value={searchName}

                      />
                    </form>


                  </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important"
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                    {dataWithAdmin.length === 0 ? (
                      <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                        Không có dữ liệu
                      </div>
                    ) : (
                      <DataGrid pageSizeOptions={[7, 20, 25]} apiRef={apiRef} rows={dataWithAdmin} columns={columns} onRowSelectionModelChange={handleSelectionChange} />

                    )}
                  </Box>
                </Box>
              </Box>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default Member;
