import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

import Cookies from 'js-cookie';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { toast, ToastContainer } from 'react-toastify';
import { convertDateFormat, convertDatePicker, formatDateString } from '../../../constants/constant';
import { EmployeeDetail, UpdateInfoEmployee } from '../../../api/apiClient/Profile/Profile';
import FooterAdmin from '../../FooterAdmin';

const initialFieldValues = {
  ID: '',
  UserName: '',
  Name: '',
  RoleId: '',
  passWord: '',
  Adress: '',
  Email: '',
  PhoneNumber: '',
  Dob: '',

}
export default function UpdateMember() {
  const [values, setValues] = useState(initialFieldValues);
  const { code } = useParams();
  let jwttoken = Cookies.get('jwttoken');
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);

  useEffect(() => {
    GetEmployee();
  }, [])
  const GetEmployee = async () => {
    try{
    const data = await EmployeeDetail(code)
    const result = data.result;
    setValues({
      ID: code,
      UserName: result.userName,
      Name: result.name,
      Adress: result.adress,
      RoleId: result.roleName,
      Email: result.email,
      PhoneNumber: result.phoneNumber,
      Dob:  convertDateFormat(result.dob),
      passWord: result.passWord
    });
  }catch(error){}
  }

  const addOrEdit = async (formData) => {
    await UpdateInfoEmployee(code, values.Name, values.Adress, values.Email, values.PhoneNumber, values.Dob, values.RoleId)
      .then(data => {
        toast.success("Cập nhật thành công")
        window.location.reload();
      })
      .catch(error => {
        // Xử lý khi gửi thất bại
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi

          }
        }
      });
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('ID', values.ID)
    formData.append('UserName', values.UserName)
    formData.append('Name', values.Name)
    formData.append('RoleId', values.RoleId)
    formData.append('Adress', values.Adress)
    formData.append('PassWord', values.PassWord)
    formData.append('Email', values.Email)
    formData.append('PhoneNumber', values.PhoneNumber)
    formData.append('Dob', values.Dob)
    addOrEdit(formData)
  }
  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    })
  }
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <ToastContainer />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <div className="row">
                  <div className="col-12">
                    <form onSubmit={handleFormSubmit}>
                      <div className="row mb-5 gx-12">
                        {/* Contact detail */}
                        <div className="col-xxl-12 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">

                              <div className="col-md-6">
                                <label className="form-label">Tên người dùng</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='UserName'
                                  readOnly
                                  value={values.UserName}
                                  onChange={handleInputChange}
                                  disabled
                                />
                              </div>
                              {/* Mobile number */}

                              <div className="col-md-6">
                                <label className="form-label">Họ tên</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='Name'
                                  value={values.Name}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Ngày sinh</label>
                                <DatePicker
                                  value={(values.Dob)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  required
                                  maxDate={new Date()}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                      setValues({
                                        ...values,
                                        Dob: formatDateString(selectedDate),
                                      });     
                                  }}
                                />
                              </div>

                              <div className="col-md-6">
                                <label className="form-label">Địa chỉ</label>
                                <select name="Adress" value={values.Adress} className='form-control' onChange={handleInputChange}>

                                  {provinces.map(province => (
                                    <option key={province.code} value={province.name}>{province.name}</option>
                                  ))}
                                </select>
                              </div>
                              <div className="col-md-6">
                                <label className="form-label">Số điện thoại</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='PhoneNumber'
                                  pattern="[0-9]{10}"
                                  maxLength="10"
                                  value={values.PhoneNumber}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>      
                          
                              <div className="col-md-6">
                                <label className="form-label">Email</label>
                                <input
                                  type="email"
                                  className="form-control"
                                  placeholder=""
                                  name='Email'
                                  value={values.Email}
                                  onChange={handleInputChange}
                                  required
                                />
                              </div>
                          
                             
                            </div>{" "}

                          </div>
                        </div>
                        
                      </div>{" "}


                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                        <a className='backtolist' href='/manageteam'> {'<< Quay về danh sách'}</a>
                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
               <i className="addicon fa fa-save"></i>Lưu
                               </button>
                      </div>
                    </form>
                    {/* Form END */}
                  </div>


                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
