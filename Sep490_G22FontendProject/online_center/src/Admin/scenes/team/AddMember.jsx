import React, { useEffect, useState } from 'react'
import "../../scenes/course/addcourse.css"
import { useNavigate } from 'react-router-dom';

import Cookies from 'js-cookie';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { ToastContainer, toast } from 'react-toastify';
import Header from '../../components/Header';
import { getCenterAPI } from '../../../api/apiClient/Center/CenterAPI';
import { AddEmployee } from '../../../api/apiClient/Profile/Profile';
import FooterAdmin from '../../FooterAdmin';

const initialFieldValues = {
  UserName: '',
  Name: '',
  RoleId: '',
  CenterId: '',
  AccountID: '',
  email:''

}

export default function AddCenter() {
  const [values, setValues] = useState(initialFieldValues);
  
  
const [theme, colorMode] = useMode();
const [isSidebar, setIsSidebar] = useState(true);
  let jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
 
  const [listcenter,setListCenter] = useState([]);
  
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);

  useEffect(() => {
  ListCenter();

  },[])
  const ListCenter = async() => {  
    const data = await getCenterAPI();
    console.log(data.result)
    setListCenter(data.result)
    setValues((prevValues) => ({
      ...prevValues,
      CenterId: data.result[0].id,
    }));
  }

  const addOrEdit = async(formData) => {
   await AddEmployee(formData)
    .then(data => {
      // Xử lý khi gửi thành công
      toast.success("Thêm thành công")
      setTimeout(() => {
      navigate('/manageteam')
      },1000)
    })
    .catch(error => {
      // Xử lý khi gửi thất bại
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
          const errorMessage = errorMessages[0];
          toast.error(errorMessage); // Hiển thị thông điệp lỗi
          console.error('Error:', error);
        }
      }
    });
  }
  const handleFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('UserName', values.UserName)
    formData.append('Name', values.Name)
    formData.append('RoleId', values.RoleId)
    formData.append('CenterId', values.CenterId)
    formData.append('AccountID', values.AccountID)
    formData.append('email', values.email)
    

    
    addOrEdit(formData)
    
  }


  const handleInputChange = e => {
    const { name, value } = e.target; 
    setValues({
      ...values,
      [name]: value
    })
  }
  
  return (
    
<ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content">
                  <Topbar setIsSidebar={setIsSidebar} />
                  <ToastContainer/>
    <div className="container">
    <Header title="Nhân Viên" subtitle="Thêm nhân viên" />
      <div className="row">
        <div className="col-12">
          {/* Page title */}
         

          {/* Form START */}
          <form onSubmit={handleFormSubmit}>
        
            <div className="row mb-5 gx-12">
              {/* Contact detail */}
              <div className="col-xxl-12 mb-5 mb-xxl-0">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                  
                    <div className="col-md-12">
                      <label className="form-label">Tên người dùng <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='UserName'
                        value={values.UserName}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    {/* Mobile number */}
          
                    <div className="col-md-12">
                      <label className="form-label">Họ và tên <span className='required-field'>*</span></label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name='Name'
                        value={values.Name}
                        onChange={handleInputChange}
                          required
                      />
                    </div>

                    <div className="col-md-12">
                      <label className="form-label">Trung tâm <span className='required-field'>*</span></label>
                      <select
                        className="form-control"
                        name="CenterId"
                        value={values.CenterId}
                        onChange={handleInputChange}
                      >
                      
                        {listcenter?.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.name}
                          </option>
                        ))}
                      </select>
                    </div>
                   
                    <div className="col-md-12">
                      <label className="form-label">Email <span className='required-field'>*</span></label>
                      <input
                        type="email"
                        className="form-control"
                        placeholder=""
                        name='email'
                        value={values.email}
                        onChange={handleInputChange}
                          required
                      />
                    </div>
                  </div>{" "}
             
                </div>
              </div>

            </div>{" "}

      
            <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
            <a className='backtolist' href='/manageteam'> {'<< Quay về danh sách'}</a>
            <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
            <i className="addicon fa fa-save"></i>Lưu
    </button>
  </div>

          </form>
          {/* Form END */}
        </div>


      </div>
    </div>

    </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )
}
