import { useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { Modal } from 'react-bootstrap';
import {
  Box,
} from "@mui/material";
import Header from "../../components/Header";
import { ClassRoom, ScheduleAdmin, SlotDate, UpdateSchedule } from "../../../api/apiClient/Class/ClassAPI";
import { toast, ToastContainer } from "react-toastify";
import { convertoDate, formatDateString, formatDates, padDate } from "../../../constants/constant";
import FooterAdmin from "../../FooterAdmin";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const initialFieldValues = {
  scheduleId: 0,
  slotId: 1,
  roomIds: 1,
  date: formatDateString(new Date()),
}

const Calendar = () => {

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [selectedMonth, setSelectedMonth] = useState(new Date().getMonth() + 1);
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());
  const currentYear = new Date().getFullYear();
  const recentYears = Array.from({ length: 5 }, (_, index) => currentYear - 1 + index);
  const [slot, setSlot] = useState([]);
  const [values, setValues] = useState(initialFieldValues);
  const [room, setRoom] = useState([]);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const months = [
    "1", "2", "3", "4", "5", "6",
    "7", "8", "9", "10", "11", "12"
  ];
  const [renderCalendar, setRenderCalendar] = useState(false);
  const [classinfoschedule, setClassInfoSchedule] = useState([]);
  const [validRange, setValidRange] = useState({});
  const [scheduleId, setScheduleId] = useState();
  useEffect(() => {
    handleSubmitDate();
    if (selectedMonth !== "" && selectedYear !== "") {
      var firstDayObject = new Date(selectedYear, selectedMonth - 1, 1);
      var dayStart = padDate(firstDayObject.getDate());
      var monthStart = padDate(firstDayObject.getMonth() + 1); // Lưu ý rằng tháng bắt đầu từ 0
      var yearStart = firstDayObject.getFullYear();

      var lastDayObject = new Date(selectedYear, selectedMonth, 0);
      var dayEnd = padDate(lastDayObject.getDate() + 1);

      var formattedFirstDay = `${yearStart}-${monthStart}-${dayStart}`;
      const formattedLastDay = `${yearStart}-${monthStart}-${dayEnd}`;


      setValidRange({
        start: formattedFirstDay,
        end: formattedLastDay,
      });

      getListSlotDate();
      getListRoom();


    }
  }, [selectedMonth, selectedYear]);
  const getListSlotDate = async () => {
    const data = await SlotDate();
    setSlot(data.result)


  }
  const getListRoom = async () => {
    const data = await ClassRoom();
    setRoom(data.result)



  }
  const handleEventClick = (selected) => {
    const eventId = selected.event.id;
    setScheduleId(selected.event.extendedProps.idsche) // Giá trị val.id
    if (eventId == 0 || eventId == 5) {
      setShow(true);
    }
  };
  const addOrEdit = async (formData) => {

    await UpdateSchedule(scheduleId, values.slotId, convertoDate(values.date), values.roomIds)
      .then(response => {
        // Xử lý khi gửi thành công
        toast.success("Thành công")
        window.location.reload();
      })
      .catch(error => {
        // Xử lý khi gửi thất bại
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {

            const errorMessage = errorMessages[0];
            toast.error(errorMessage)
          }
        }
      });
  };


  const handleChangeSlotClass = (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('scheduleId', scheduleId)
    formData.append('slotId', values.slotId.toString())
    formData.append('roomIds', values.roomIds.toString())
    formData.append('date', convertoDate(values.date))

    addOrEdit(formData)
  }
  const handleInputChange = e => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value
    })
  }




  const handleSelectChange = (event) => {
    setSelectedMonth(event.target.value);
  };
  const handleYearChange = (event) => {
    setSelectedYear(event.target.value);
    // Thực hiện các hành động khác tùy thuộc vào năm được chọn (nếu cần)
  };
  const handleSubmitDate = async () => {
    try {
      const data = await ScheduleAdmin(selectedMonth, selectedYear);
      setClassInfoSchedule(data.result);
      setRenderCalendar(true);
      // toast.success('Success');
      // navigate(`/addclassschedule/${classId}`);
    }
    catch (error) {
      toast.error(error.response.data.errorMessages);
    }
  };
  const getStatusColor = (status) => {
    switch (status) {
      case 0:
        return 'light gray'; // Not yet
      case 1:
        return 'blue'; // Attend
      case 2:
        return 'gray'; // Finished
      case 3:
        return 'orange'; // Pending
      case 4:
        return 'gray'; // Closed
      case 5:
        return 'light gray'; // Change Slot
      default:
        return 'gray'; // Default color, adjust as needed
    }
  };
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Box m="20px">
                <Header title="Thời Khóa Biểu" subtitle="Trang hiển thị toàn bộ thời khóa biểu" />

                <ToastContainer />
                <div style={{ marginTop: '30px', marginBottom: '30px' }} className="col-xl-8 order-2 order-xl-1">
                  <div className="form-group m-form__group row align-items-center">

                    <div className="col-md-3">
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <label style={{ marginRight: '10px' }}>Tháng</label>
                        <select
                          className="form-control"
                          id="select-course"
                          value={selectedMonth}
                          onChange={handleSelectChange}
                        >

                          {months.map((month, index) => (
                            <option key={index} value={month}>
                              {month}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <label style={{ marginRight: '10px' }}>Năm</label>
                        <select
                          className="form-control"
                          id="select-course"
                          value={selectedYear}
                          onChange={handleYearChange}
                        >

                          {recentYears.map((year) => (
                            <option key={year} value={year}>
                              {year}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <Box display="flex" justifyContent="space-between">
                  <Box flex="1 1 100%" ml="15px">
                    {renderCalendar && (
                      <FullCalendar
                        height="75vh"
                        plugins={[
                          dayGridPlugin,
                          timeGridPlugin,
                          interactionPlugin,
                        ]}
                        headerToolbar={{
                          left: "prev,next today",
                          center: "title",
                          right: "timeGridWeek,timeGridDay",
                        }}
                        eventClassNames={(arg) => {
                          return 'custom-event'; // Lớp CSS tùy chỉnh bạn muốn áp dụng cho sự kiện
                        }}
                        editable={false}
                        selectable={false}
                        selectMirror={false}
                        dayMaxEvents={false}

                        eventClick={handleEventClick}
                        slotEventOverlap={false}
                        initialView="timeGridWeek"
                        slotDuration="02:00:00"
                        slotMinTime="07:00:00"
                        slotMaxTime="23:00:00"
                        initialEvents={classinfoschedule.map((val) => ({
                          id: val.staus,
                          idsche: val.id,
                          classId: val.classId,
                          title: `${val.staus === 0 ? 'Chưa điểm danh' : val.staus === 1 ? 'Đã điểm danh' : val.staus === 2 ? 'Kết thúc' : val.staus === 3 ? 'Đang chờ' : val.staus === 4 ? 'Đã đóng' : val.staus === 5 ? 'Đổi lịch' : ''}
                          
                    ${val.roomName}  
                    ${val.teacherName}  
                    ${val.className} \n Slot ${val.slotNum} / ${val.totalSlot}`,
                          date: formatDates(val.dateOffSlot),
                          start: `${formatDates(val.dateOffSlot)} ${val.timeStart}`,
                          end: `${formatDates(val.dateOffSlot)} ${val.timeEnd}`,
                          color: getStatusColor(val.staus)
                        }))}
                        validRange={validRange}
                      />
                    )}
                  </Box>
                </Box>
              </Box>
              <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Chuyển đổi ca học</Modal.Title>
                </Modal.Header>
                <form onSubmit={handleChangeSlotClass}>
                  <Modal.Body>
                    <label className="form-label">Ca học</label>
                    <select
                      className="form-control"
                      name="slotId"
                      onChange={handleInputChange}

                    >


                      {slot?.map((option) => (
                        <option key={option.id} value={option.id}>
                          {option.timeStart} - {option.timeEnd} {option.day}
                        </option>
                      ))}

                    </select>
                    <label className="form-label">Phòng học </label>
                    <select
                      className="form-control"
                      name="roomIds"
                      onChange={handleInputChange}

                    >

                      {room?.map((option) => (
                        <option key={option.id} value={option.id}>
                          {option.name}
                        </option>
                      ))}
                    </select>
                    <label style={{ width: '10%', marginTop: 'auto' }} className="form-label">Ngày</label>
                    <DatePicker
                      value={(values.date)}
                      dateFormat="dd/MM/yyyy"
                      className="form-control"
                      showMonthDropdown
                      showYearDropdown
                      scrollableYearDropdown
                      yearDropdownItemNumber={25}
                      required
                      minDate={new Date()}
                      onChange={(date) => {
                        const selectedDate = new Date(date);
                        setValues({
                          ...values,
                          date: formatDateString(selectedDate),
                        });

                      }}
                    />

                  </Modal.Body>
                  <Modal.Footer>
                    <button className='btn btn-success m-btn' variant="primary" type="submit">
                      Lưu thay đổi
                    </button>
                  </Modal.Footer>
                </form>
              </Modal>
            </main>
          </Box>
          <FooterAdmin />
        </div>

      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default Calendar;
