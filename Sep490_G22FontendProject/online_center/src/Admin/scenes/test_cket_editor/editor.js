import React, { useEffect, useRef } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

function Editor({ onChange, editorLoaded, name, value }) {
  const editorRef = useRef();

  useEffect(() => {
    editorRef.current = {
      CKEditor,
      ClassicEditor,
    };
  }, []);
  const onReady = (editor) => {
    editor.editing.view.change((writer) => {
      writer.setStyle('height', '400px', editor.editing.view.document.getRoot());
    });
  };
  return (
    <div style={{maxHeight:'500px', overflowY:'auto'}}>
      {editorLoaded ? (
        <CKEditor
          editor={ClassicEditor}
          config={{
            ckfinder: {
              uploadUrl: "https://pitech.edu.vn:82/api/Blog/upload",
            },
          }}
          onReady={(editor) => onReady(editor)}
          data={value}
          onChange={(event, editor) => {
            const data = editor.getData();
            onChange(data);
          }}
        />
      ) : (
        <div>Editor loading</div>
      )}
    </div>
  );
}

export default Editor;