import React, { useState, useEffect } from "react";
import "./styles.css";
import Editor from "./editor";

export default function App() {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [editorData, setEditorData] = useState("");
  const [formData, setFormData] = useState({
    title: "",
    description: "",
    shortDescription: "",
    imageFile: null, // Sử dụng "imageFile" thay vì "file"
  });

  const handleInputChange = (e) => {
    const { name, value, files } = e.target;
    setFormData({
      ...formData,
      [name]: name === "imageFile" ? files[0] : value, // Sử dụng "imageFile" thay vì "file"
    });
  };

  const handleEditorChange = (data) => {
    setEditorData(data.replace(/"/g, "'"));
  };

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    const formDataToSend = new FormData();
    formDataToSend.append("title", formData.title);
    formDataToSend.append("description", editorData.replace(/"/g, "'"));
    formDataToSend.append("shortDescription", formData.shortDescription);
    formDataToSend.append("imageFile", formData.imageFile); // Sử dụng "imageFile" thay vì "file"

    fetch("http://pitech.edu.vn:82/api/Blog", {
      method: "POST",
      body: formDataToSend,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Server Response:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <div className="App">
      <h1>Add your blog</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Titlte</label>
          <input
            type="text"
            className="form-control"
            name="title"
            placeholder="Enter title"
            value={formData.title}
            onChange={handleInputChange}
          />
        </div>
        <div className="form-group">
          <label>Short description</label>
          <input
            type="text"
            className="form-control"
            name="shortDescription"
            placeholder="Enter title"
            value={formData.shortDescription}
            onChange={handleInputChange}
          />
        </div>
        <div className="form-group">
          <label>Description</label>
          <Editor
            name="description"
            onChange={handleEditorChange}
            editorLoaded={editorLoaded}
            value={formData.description}
          />
        </div>
        <div className="form-group">
          <label>Images</label>
          <input
            type="file"
            className="form-control"
            name="imageFile" // Sử dụng "imageFile" thay vì "file"
            placeholder="Enter image"
            onChange={handleInputChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
      <div>
        <strong>Form Data:</strong>
        <pre>{JSON.stringify(formData, null, 2)}</pre>
      </div>
      <div>
        <strong>Editor Data:</strong>
        <pre>{editorData}</pre>
      </div>
    </div>
  );
}
