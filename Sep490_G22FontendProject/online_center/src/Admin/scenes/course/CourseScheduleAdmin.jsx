import React, { useEffect, useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import "../../../Client/components/schedule/schedule.css"


import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import {
  Box,
  
} from "@mui/material";

import Cookies from 'js-cookie';
import { ToastContainer, toast } from 'react-toastify';
import { ClassRoom, SchedualofClass, SlotDate, UpdateSchedule } from '../../../api/apiClient/Class/ClassAPI';
import { convertoDate, formatDateString, formatDates } from '../../../constants/constant';
import FooterAdmin from '../../FooterAdmin';
import { Modal } from 'react-bootstrap';
const initialFieldValues = {
  scheduleId : 0,
  slotId: 1,
  roomIds: 1,
  date: formatDateString(new Date()),
}
export default function CourseScheduleAdmin() {
  const {id} = useParams();
  const [classinfoschedule,setClassInfoSchedule] = useState([]);
  const [renderCalendar, setRenderCalendar] = useState(false);
  const [room, setRoom] = useState([]);
  const [slot, setSlot] = useState([]);
  const [values, setValues] = useState(initialFieldValues);
  const [scheduleId,setScheduleId] = useState();
  const  jwttoken = Cookies.get('jwttoken');
  const [className, setClassName] = useState('');
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const handleClose = () => setShow(false);
 
 

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(()=> {
   
   getListSlotDate();
    getListRoom();
   getClassSchedual();
  },[id])
      const getListSlotDate = async() =>{
        const data = await SlotDate();
          setSlot(data.result)       
          
        
      }
      const getListRoom = async() =>{
        const data = await ClassRoom();
        setRoom(data.result)
        
        

      }
      const getClassSchedual = async() =>{
        const data = await SchedualofClass(id);
        setClassInfoSchedule(data.result);
        setRenderCalendar(true);
        setClassName(data.result[0].className)
      }
  
 
  const handleEventClick = (selected) => {
    const eventId = selected.event.id;
    setScheduleId(selected.event.extendedProps.idsche) // Giá trị val.id
    if (eventId == 0 || eventId == 5) {
      setShow(true);
    }
  };
  const addOrEdit = async(formData) => {
   
    await UpdateSchedule(scheduleId,values.slotId,convertoDate(values.date), values.roomIds)
    .then(response => {
      // Xử lý khi gửi thành công
     toast.success("Thành công")
      window.location.reload();
    })
    .catch(error => {
      // Xử lý khi gửi thất bại
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          
          const errorMessage = errorMessages[0];
          toast.error(errorMessage)
        }
      }
    });
  };
  
 
  const handleChangeSlotClass = (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('scheduleId',scheduleId)
    formData.append('slotId',values.slotId.toString())
    formData.append('roomIds',values.roomIds.toString())
    formData.append('date',convertoDate(values.date))
   
    addOrEdit(formData)
  }
  const handleInputChange = e => {
    const { name, value } = e.target;
  
    setValues({
      ...values,
      [name]: value
    })
  }
  const getStatusColor = (status) => {
    switch (status) {
      case 0:
        return 'light gray'; // Not yet
      case 1:
        return 'blue'; // Attend
      case 2:
        return 'light gray'; // Finished
      case 3:
        return 'orange'; // Pending
      case 4:
        return 'light gray'; // Closed
      case 5:
        return 'light gray'; // Change Slot
      default:
        return 'gray'; // Default color, adjust as needed
    }
  };
  return (
    <>
 
<ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <ToastContainer/>
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
           <div  className="col-12 my-5">
            <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                                <h3 style={{ flex: '1' }}>{className}</h3>
                                <div style={{ flex: '2' }}></div>

                                <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                                    </div>
                                    <div class="icon-container">
                                    <Link className='icon-class' to={`/../coursesadmin/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Syllabus</span></Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class' to={`/../coursesadmin/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                                    </div>
                                    <div class="icon-container">
                                        <Link className='icon-class'><i class="iconcourse-active fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                                    </div>
                                    <div class="icon-container">
                    <Link className='icon-class' to={`/../coursesadmin/notification/${id}`}>   <i class="iconcourse fa fa-bell" aria-hidden="true"></i></Link>
                  </div>
                                </div>
                            </div>
            <hr />
          </div>
          <Box m="20px">
    
    <Box display="flex" justifyContent="space-between">
     
      <Box flex="1 1 100%" ml="15px">
      {renderCalendar && (
        <FullCalendar
          height="75vh"
          plugins={[
            dayGridPlugin,
            timeGridPlugin,
            interactionPlugin,        
          ]}
          headerToolbar={{
            left: "prev,next today",
            center: "title",
            right: "timeGridWeek,timeGridDay",
          }}
          eventClassNames={(arg) => {
            return 'custom-event'; // Lớp CSS tùy chỉnh bạn muốn áp dụng cho sự kiện
          }}
          editable={false} 
          selectable={false} 
          selectMirror={false}
          dayMaxEvents={false}
         
          eventClick={handleEventClick}
          slotEventOverlap={false}
          initialView="timeGridWeek"
          slotDuration="02:00:00" 
          slotMinTime="07:00:00" 
          slotMaxTime="23:00:00" 
          
          initialEvents={classinfoschedule.map((val) => ({
            id: val.staus,
            idsche : val.id,
            title: `${val.className}
            (${val.staus === 0 ? 'Chưa điểm danh' : val.staus === 1 ? 'Đã điểm danh' : val.staus === 2 ? 'Kết thúc'  : val.staus === 3 ? 'Đang chờ' : val.staus === 4 ? 'Đã đóng' : val.staus === 5 ? 'Đổi lịch' : ''})
            ${val.roomName}
          
             Slot ${val.slotNum} / ${val.totalSlot}`,
                date: formatDates(val.dateOffSlot),
                start: `${formatDates(val.dateOffSlot)} ${val.timeStart}`,
                end: `${formatDates(val.dateOffSlot)} ${val.timeEnd}`,
                color :getStatusColor(val.staus)
          }))}
        />
        )}
      </Box>
    </Box>
  </Box>
  <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Chuyển đổi ca học</Modal.Title>
                  </Modal.Header>
                  <form onSubmit={handleChangeSlotClass}>
                  <Modal.Body>
                    <label className="form-label">Ca học</label>
                    <select
                        className="form-control"
                        name="slotId"
                        onChange={handleInputChange}
                    
                      >
                       
                       
                        {slot?.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.timeStart} - {option.timeEnd} {option.day}
                          </option>
                        ))}
                       
                      </select>
                      <label className="form-label">Phòng học </label>
                      <select
                        className="form-control"
                        name="roomIds"
                        onChange={handleInputChange}
                        
                      >
                          
                        {room?.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.name}
                          </option>
                        ))}
                      </select>
                      <label  style={{width:'10%',marginTop:'auto'}} className="form-label">Ngày</label>
                      <DatePicker
                                  value={(values.date)}
                                  dateFormat="dd/MM/yyyy"
                                  className="form-control"
                                  showMonthDropdown
                                  showYearDropdown
                                  scrollableYearDropdown
                                  yearDropdownItemNumber={25}
                                  required
                                  minDate={new Date()}
                                  onChange={(date) => {
                                    const selectedDate = new Date(date);
                                    setValues({
                                      ...values,
                                      date: formatDateString(selectedDate),
                                    });

                                  }}
                                />
                     
                  </Modal.Body>
                  <Modal.Footer>
                  <button className='btn btn-success m-btn' variant="primary"  type="submit">
                      Lưu thay đổi
                    </button>
                  </Modal.Footer>
                  </form>
                </Modal>
   
  </main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
     </>     
  )
}
