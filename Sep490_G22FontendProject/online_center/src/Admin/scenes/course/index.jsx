import { Box,  useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { clean } from 'diacritic';
import Header from "../../components/Header";
import { useEffect, useState } from "react";

import { ToastContainer, toast } from "react-toastify"  
import 'react-toastify/dist/ReactToastify.css';
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link, useNavigate } from "react-router-dom";
import { convertDateFormat } from "../../../constants/constant";
import Cookies from "js-cookie";
import { DeleteCourse, getAllCourseHome, getAllViewCourse } from "../../../api/apiClient/Courses/CoursesAPI";
import FooterAdmin from "../../FooterAdmin";
const Course = () => {
  const [course, setCourse] = useState([]);
  const [originalClasses, setOriginalClasses] = useState([]);
  const [searchCourse, setSearchCourse] = useState("");

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const apiRef = useGridApiRef();
  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
   
    getCourse();
   
  }, [apiRef])
  const getCourse = async() => {  
    try{
    const data = await getAllViewCourse();
   if(data.result.length > 0){
    const convertedCourse = data.result.map((item) => {

      item.dateCreate = convertDateFormat(item.dateCreate);
      item.dateUpdate = convertDateFormat(item.dateUpdate)

 
      return item;
    });
    setOriginalClasses(convertedCourse)
    setCourse(convertedCourse)
 
    apiRef.current.setPageSize(7);
  
  } else{}
}catch(error){}
}
  const dataWithCourse = course.map((item, index) => ({
    ...item,
    CourseID: index + 1,
  }))

  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const removeCourse = async (id) => {
    try{
    if (window.confirm("Bạn có muốn xóa khóa học này ?")) {
      const response = await DeleteCourse(id)
      if (response.statusCode === 200) {
        toast.success('Thành công');
        getCourse();
      } else {
        toast.error(response.errorMessages)
      }
     
    }
  }catch(error){
    toast.error("Có lỗi xảy ra")
  }
  }
  const filterNameCourse = (event) => {
    event.preventDefault();
    if (searchCourse) {
      const filteredClasses = originalClasses.filter((val) =>
        clean(val.name.toLowerCase()).includes(clean(searchCourse.toLowerCase()))
      );
      setCourse(filteredClasses);
    } else {
      setCourse(originalClasses);
    }
      
  };
  const columns = [
    {
      headerName: "STT",
      field: "CourseID",
      flex: 0.5,
      align:'center'
    },
    {
      field: "name",
      headerName: "Tên khóa học",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "code",
      headerName: "Mã khóa học",
      headerAlign: "left",
      align: "left",
      flex: 1,
    },
   
    {
      field: "dateCreate",
      headerName: "Ngày tạo",
      align: "center",
      flex: 0.5,
    },
    {
      field: "dateUpdate",
      headerName: "Ngày cập nhật",
      align: "center",
      flex: 0.5,
    },
 
    {
      headerName: "Chỉnh sửa",
      flex: 0.5,
      renderCell: (params) => (
        <div style={{ display: 'flex', alignItems: 'center', marginLeft:'15px' }}>
          <a style={{ cursor: 'pointer', fontSize: '25px' }} onClick={() => removeCourse(`${params.row.id}`)}>
            <DeleteOutlineIcon />
          </a>
          <a style={{ marginLeft: '10px' }} href={`/coursedetail/${params.row.id}`}>
            <UilEdit />
          </a>
        </div>
      ),
    }
  ];
  return (

    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main
              className="content">
              <div>
                <Topbar setIsSidebar={setIsSidebar} />
                <Box m="20px">
                  <ToastContainer />
                  <Header title="Khóa Học" subtitle="Quản lý khóa học" />
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Box>
                             
          <Link to='./addcourse' class="btn btn-success m-btn ">
                    <i class="fa fa-plus"></i> Thêm mới
                  </Link>
              
                  </Box>
                  
                  <form className="filter-search" onSubmit={filterNameCourse}>
                        <input
                          type="text"
                          className="form-control m-input"
                          placeholder="Tìm kiếm..."
                          onChange={(e) => setSearchCourse(e.target.value)}
                          value={searchCourse}

                        />
                      </form>
                  
                      </Box>
                  <Box
                    m="40px 0 0 0"
                    maxHeight="80vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important"
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                     {dataWithCourse.length === 0 ? (
                    <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                      Không có dữ liệu
                    </div>
                  ) : (
                    <DataGrid pageSizeOptions={[7, 20,25]}  apiRef={apiRef} rows={dataWithCourse} columns={columns} />

                  )}
                   
                  </Box>
                </Box>
              </div>
    
            </main>
          </Box>
          <FooterAdmin/>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default Course;
