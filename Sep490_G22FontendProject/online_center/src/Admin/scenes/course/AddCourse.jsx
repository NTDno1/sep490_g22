import React, { useEffect, useState } from 'react'
// import 'bootstrap/dist/css/bootstrap.css';
import "./coursedetailadmin.css"
import axios from "axios";
import { useNavigate, useParams } from 'react-router-dom';
import Cookies from 'js-cookie';
import { ToastContainer, toast } from "react-toastify"
import { Box, CssBaseline, ThemeProvider, Tooltip } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { addNewCourse, viewAllSyllabus } from '../../../api/apiClient/Courses/CoursesAPI';
import FooterAdmin from '../../FooterAdmin';
import { currencyFormat, generateRandomCode } from '../../../constants/constant';
import CurrencyInput from "react-currency-input-field";
import ReplayIcon from '@mui/icons-material/Replay';
const defaultImageSrc = '/img/image_placeholder.png'
const initialFieldValues = {
  Name: '',
  Code: '',
  Description: '',
  Price: '',
  SyllabusId: 0,
  // imageSrc: defaultImageSrc,
  imageFile: null
}


export default function AddCourse() {

  const [values, setValues] = useState(initialFieldValues);
  const [syllabus, setSyllabus] = useState([]);
  const navigate = useNavigate();

  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);

  let jwttoken = Cookies.get('jwttoken');

  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    ListSyllabus();
  }, []);
  const ListSyllabus = async () => {
    const data = await viewAllSyllabus();
    if(data.result){
    setSyllabus(data.result)
    setValues({
      ...values,
      SyllabusId: data.result[0].id
    })
  }
  }
 
  const addOrEdit = async (formData) => {
    await addNewCourse(formData)
      .then(res => {
        toast.success("Thêm thành công")
        setTimeout(() => {
          navigate("/courseadmin");
        }, 1000)
      }).catch(error => {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      })

  }

  const handleFormSubmit = e => {

    e.preventDefault()
    const formData = new FormData()
    formData.append('Name', values.Name)
    formData.append('Code', values.Code)
    formData.append('Description', values.Description)
    formData.append('Price', values.Price)
    formData.append('SyllabusId', values.SyllabusId)
    // formData.append('imageName', values.imageSrc)
    formData.append('imageFile', values.imageFile)

    addOrEdit(formData)

  }

  const handleChange = (e) => {
    e.preventDefault();
    const { value = "" } = e.target;
    const parsedValue = value.replace(/[^\d.]/gi, "");
    setValues({
      ...values,
      Price: parsedValue
    })

  };



  const showPreview = e => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = x => {
        setValues({
          ...values,
          imageFile,
          imageSrc: x.target.result
        })
      }
      reader.readAsDataURL(imageFile)
    }
    else {
      setValues({
        ...values,
        imageFile: null,
        imageSrc: defaultImageSrc
      })
    }
  }
  const handleInputChange = e => {
    const { name, value } = e.target;
    if (name === "Price") {
      const parsedValue = value.replace(/[^\d.]/gi, "");
      setValues({
        ...values,
        Price: parsedValue
      })
    } else {

      setValues({
        ...values,
        [name]: value
      })
    }
  }
  const generateCode = () => {
    const genCode = generateRandomCode();
    setValues({
      ...values,
      Code: genCode
    })

  }
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Box style={{ display: 'flex' }}>
            <Sidebars isSidebar={isSidebar} />
            <main className="content" >
              <Topbar setIsSidebar={setIsSidebar} />
              <div className="container">
                <ToastContainer />
                <div className="row">
                  <div className="col-12">
                    {/* Page title */}
                    <div className="my-5">

                      <hr />
                    </div>
                    {/* Form START */}
                    <form className="file-upload" onSubmit={handleFormSubmit}>
                      <div className="row mb-5 gx-5">
                        {/* Contact detail */}

                        <div className="col-xxl-8 mb-5 mb-xxl-0">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <div className="col-md-6">
                                <label className="form-label">Mã khóa học <span className='required-field'>*</span></label>
                                <div style={{ position: 'relative', display: 'flex' }}>
                                  <input
                                    value={values.Code}
                                    type="text"
                                    className="form-control"
                                    placeholder=""
                                    name='Code'
                                    aria-label="Last name"
                                    required
                                    onChange={handleInputChange}
                                  />
                                  <Tooltip title="Tự tạo" placement='top'>
                                    <a className='replayCode' onClick={() => generateCode()}><ReplayIcon /></a>
                                  </Tooltip>
                                </div>
                              </div>
                              {/* First Name */}
                              <div className="col-md-6">
                                <label className="form-label">Tên khóa học <span className='required-field'>*</span></label>

                                <input
                                  value={values.Name}
                                  type="text"
                                  className="form-control"
                                  placeholder=""
                                  name='Name'
                                  aria-label="First name"

                                  onChange={handleInputChange}
                                />
                              </div>
                              {/* Last name */}


                              <div className="col-md-6">
                                <label className="form-label">Giáo trình <span className='required-field'>*</span></label>
                                <select
                                  className="form-control"
                                  name="SyllabusId"
                                  value={values.SyllabusId}
                                  onChange={handleInputChange}
                                >
                                  <option value="">Chọn khóa học</option>
                                  {syllabus?.map((option) => (
                                    <option key={option.id} value={option.id}>
                                      {option.syllabusName}
                                    </option>
                                  ))}
                                </select>
                              </div>
                              {/* Phone number */}
                              <div className="col-md-6">
                                <label className="form-label">Giá <span className='required-field'>*</span></label>

                                <CurrencyInput
                                  name="Price"
                                  className="form-control"
                                  id="currencyInput"
                                  data-number-to-fixed="2"
                                  data-number-stepfactor="100"
                                  value={values.Price}
                                  onChange={handleChange}
                                  type='text'
                                  allowDecimals
                                  decimalsLimit="2"
                                  disableAbbreviations
                                />

                              </div>
                              {/* Mobile number */}

                              {/* Email */}
                              <div className="col-md-12">
                                <label htmlFor="inputEmail4" className="form-label">
                                  Mô tả
                                </label>
                                <textarea
                                  value={values.Description}
                                  type="text"
                                  name='Description'
                                  maxLength={255}
                                  className="form-control"
                                  id="inputEmail4"
                                  required
                                  onChange={handleInputChange}
                                />
                              </div>

                            </div>{" "}
                            {/* Row END */}
                          </div>
                        </div>
                        {/* Upload profile */}
                        <div className="col-xxl-4">
                          <div className="bg-secondary-soft px-4 py-5 rounded">
                            <div className="row g-3">
                              <h4 className="mb-4 mt-0">Cập nhật ảnh khóa học</h4>
                              <div className="text-center">
                                {/* Image upload */}
                                <div className="square position-relative display-2 mb-3">
                                  <img style={{ width: '250px', height: '250px' }} src={values.imageSrc} className="fas fa-fw fa-user position-absolute top-50 start-50 translate-middle text-secondary" />

                                </div>
                                {/* Button */}
                                <input type="file" accept="image/*" className={"form-control-file"}
                                  onChange={showPreview} id="image-uploader" />
                                {/* <label
                    className="btn btn-success-soft btn-block"
                    htmlFor="customFile"
                  >
                    Upload
                  </label> */}

                                {/* Content */}
                                <p className="text-muted mt-3 mb-0">
                                  <span className="me-1">Ghi chú:</span>Kích thướng tối thiểu 300px x
                                  300px
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>{" "}

                      <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                        <a className='backtolist' href='/courseadmin'> {'<< Quay về danh sách'}</a>
                        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
                          <i className="addicon fa fa-save"></i>Lưu
                        </button>
                      </div>
                    </form>{" "}
                    {/* Form END */}
                  </div>
                </div>
              </div>

            </main>
          </Box>
          <FooterAdmin />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
