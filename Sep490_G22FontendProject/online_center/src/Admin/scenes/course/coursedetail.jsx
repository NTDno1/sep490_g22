import React, { useEffect, useState } from 'react'
// import 'bootstrap/dist/css/bootstrap.css';
import "./coursedetailadmin.css"

import {ToastContainer,toast} from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from 'react-router-dom';
import Cookies from 'js-cookie';
import fetchData from '../../../api/apiUtils';
import { convertDateFormat, priceSplitter } from '../../../constants/constant';
import CurrencyInput from "react-currency-input-field";

import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebars from "../../scenes/global/Sidebars";
import Topbar from "../../scenes/global/Topbar";
import { CourseDetailbyAdmin, UpdateCourse, viewAllSyllabus } from '../../../api/apiClient/Courses/CoursesAPI';
import FooterAdmin from '../../FooterAdmin';
export default function CourseDetailAdmin() {
const [values, setValues] = useState({
  id: 0,
  name: '',
  description: '',
  price: '',
  syllabusId: 0,
  dateUpdate : '',
  imageFile:File
});
  const [srcImage, setSrcImage] = useState({
    oldSrc: ''
  });
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  
  const [syllabus,setSyllabus] = useState([]);
  const { id } = useParams(); 
  console.log(id);
  const jwttoken = Cookies.get('jwttoken');

  
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
      
        navigate('/login_admin');
      }
    };
  
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    CourseDetail();
    GetSyllabus();
  }, [id])
    const CourseDetail = async() => {
      try{
      const data = await CourseDetailbyAdmin(id)
      setValues(data);
      setSrcImage({
        oldSrc: data.image
      }); 
    }catch(error){
      
    }
    }
    const GetSyllabus = async() => {
      const data = await viewAllSyllabus();
      
      setSyllabus(data.result);
   
    }
    const handleFormSubmit = async (e) => {
    
    e.preventDefault();
    //user.image = user.image.split('/')[4];
    const formSubmit = new FormData();
    formSubmit.append("id",values.id);
    formSubmit.append("name",values.name);
    formSubmit.append("description",values.description);
    formSubmit.append("price",values.price);
    formSubmit.append("syllabusId",values.syllabusId);
    formSubmit.append("imageFile",values.imageFile);
    addOrEdit(formSubmit)
}
  const addOrEdit = async (formSubmit) => {
    await UpdateCourse(formSubmit)
      .then(res => {
        toast.success("Cập nhật thành công");
      }).catch(error => {
        if (error.response && error.response.data && error.response.data.errorMessages) {
          const errorMessages = error.response.data.errorMessages;
          if (errorMessages && errorMessages.length > 0) {
            // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
            const errorMessage = errorMessages[0];
            toast.error(errorMessage); // Hiển thị thông điệp lỗi
            console.error('Error:', error);
          }
        }
      })

  }

  const handleInputChange = e => {
    const { name, value } = e.target;
    if(name === "price") { 
      const inputValue = parseFloat(value).toLocaleString('en-US');
      setValues({
        ...values,
        price: inputValue
    })  
    }
    setValues({
        ...values,
        [name]: value
    })
}

const handleRemove = () => {
  const img = document.querySelector('#preview-box');
  console.log(srcImage.image);
  img.src = srcImage.src;
};
const handleChange = (event) => {
  setValues({
    ...values,
    imageFile: event.target.files[0]
})
  const fileUploader = document.querySelector('#customFile');
  const getFile = fileUploader.files;
  if (getFile.length !== 0) {
    const uploadedFile = getFile[0];
    readFile(uploadedFile);
  }
};
const handleChangePrice = (e) => {
  e.preventDefault();
  const { value = "" } = e.target;
  const parsedValue = value.replace(/[^\d.]/gi, "");
  setValues({
    ...values,
    price: parsedValue
  })
  
};

const readFile = (uploadedFile) => {
  if (uploadedFile) {
    const reader = new FileReader();
    console.log(reader);
    reader.onload = (x) => {
      const img = document.querySelector('#preview-box');


      img.src = reader.result;




    };
    reader.readAsDataURL(uploadedFile);
  }
};
  return (
    <ColorModeContext.Provider value={colorMode}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="app">
              <Box style={{ display: 'flex' }}>
                <Sidebars isSidebar={isSidebar} />
                <main className="content" >
                  <Topbar setIsSidebar={setIsSidebar} />
                  <ToastContainer/>
    <div  className="container">
  <div className="row">
    <div className="col-12">
      {/* Page title */}
      <div className="col-3 my-5">
        <h3>Chi tiết khóa học</h3>
      </div>
      <hr />
      {/* Form START */}
      <form className="file-upload" onSubmit={handleFormSubmit}>
        <div className="row mb-5 gx-5">
          <div className="col-xxl-8 mb-5 mb-xxl-0">
            <div className="bg-secondary-soft px-4 py-5 rounded">
              <div className="row g-3">
                {/* First Name */}
                <div className="col-md-12">
                <input style={{display:'none'}}
                   value={values.id}
                    name='id'  
                  />  
                  <label className="form-label">Tên khóa học <span className='required-field'>*</span></label> 
                  <input
                   value={values.name}
                    type="text"
                    className="form-control"
                    placeholder=""
                    name='name'
                    aria-label="First name"
                    defaultValue="Scaralet"
                    onChange={handleInputChange}
                  />
                </div>
                {/* Last name */}
            
                <div className="col-md-6">
                      <label className="form-label">Giáo trình <span className='required-field'>*</span> </label>
                      <select
                        className="form-control"
                        name="syllabusId"
                        value={values.syllabusId}
                        onChange={handleInputChange}
                      >
                        <option value="">Chọn giáo trình</option>
                        {syllabus?.map((option) => (
                          <option key={option.id} value={option.id} selected={option.id == values.syllabusId}>
                            {option.syllabusName}
                          </option>
                        ))}
                      </select>
                    </div>
                {/* Phone number */}
                <div className="col-md-6">
                  <label className="form-label">Giá tiền <span className='required-field'>*</span></label>
                  <CurrencyInput
                                  name="price"
                                  className="form-control"
                                  id="currencyInput"
                                  data-number-to-fixed="2"
                                  data-number-stepfactor="100"
                                  value={values.price}
                                  onChange={handleChangePrice}
                                  type='text'
                                  allowDecimals
                                  decimalsLimit="2"
                                  disableAbbreviations
                                />
                </div>
                {/* Mobile number */}
             
                {/* Email */}
                <div className="col-md-12">
                  <label htmlFor="inputEmail4" className="form-label">
                    Mô tả
                  </label>
                  <textarea
                  style={{height:'200px'}}
                    value={values.description}
                    type="text"
                    name='description'
                    className="form-control"
                    id="inputEmail4"
                   
                    onChange={handleInputChange}
                  />
                </div>
              
              </div>{" "}
              {/* Row END */}
            </div>
          </div>
          {/* Upload profile */}
          <div className="col-xxl-4">
                <div className="bg-secondary-soft px-4 py-5 rounded">
                  <div className="row g-3">
                    <div className="mb-4 mt-0 text-center" style={{ fontSize: '20px', fontWeight: 'bolder' }}>Cập nhật ảnh </div>
                    <div className="text-center">
                      {/* Image upload */}
                      <div className="square position-relative display-2 mb-3">
                        <img style={{ width: '250px', height: '250px' }} src={values.imageSrc} id='preview-box'></img>
                      </div>
                      {/* Button */}
                      <input type="file" id="customFile" name="file" accept="image/*" onChange={handleChange} hidden="true" />
                      <label
                        className="btn btn-success-soft btn-block"
                        htmlFor="customFile"
                      >
                        Chọn file
                      </label>
                      <button style={{marginTop:'0'}} type="button" className="btn btn-danger-soft" onClick={handleRemove}>
                        Xóa
                      </button>
                      {/* Content */}

                    </div>
                  </div>
                </div>
              </div>
        </div>{" "}
      
        <div className="col-md-12" style={{display:'flex',justifyContent:'space-between'}} align="right">
        <a className='backtolist' href='/courseadmin'> {'<< Quay về danh sách'}</a>
        <div style={{display:'flex',justifyContent:'space-between'}}>
        <button style={{ background: '#2746C0' }} className='btn btn-success m-btn' type="submit" >
        <i className="addicon fa fa-save"></i>Lưu
    </button>
          
          <button>
            <a style={{textDecoration:'underline',marginLeft:'20px'}} href={`/manageExam/${id}`}>
            {' Quản lý bài thi >>'}
            </a>
          </button>
          </div>
        </div>
      </form>{" "}
      {/* Form END */}
    </div>
  </div>
</div>

</main>
              </Box>
              <FooterAdmin/>
            </div>
          </ThemeProvider>
        </ColorModeContext.Provider>
  )

}
