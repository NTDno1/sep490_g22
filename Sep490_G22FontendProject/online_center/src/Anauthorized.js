import React from 'react'
import { useNavigate } from 'react-router-dom';
import "./anauthorize.css"
export default function Anauthorized() {
    const navigate = useNavigate();

    const goBack = () => navigate(-1);

    return (
        <section className="page_404">
        <div>
          <div className="row">	
            <div className="col-sm-12 ">
              <div className="col-sm-10 col-sm-offset-1  text-center">
                <div className="four_zero_four_bg">
                  <h1 className="text-center ">404</h1>
                </div>
                <div className="contant_box_404">
                  <h3 className="h2">
                   Có vẻ có nhầm lẫn
                  </h3>
                  <p>Trang mà bạn truy cập không tồn tại</p>
                  <a style={{cursor:'pointer'}} onClick={goBack} href className="link_404">Quay về </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}
