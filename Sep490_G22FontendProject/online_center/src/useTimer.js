import React, { createContext, useContext, useEffect, useState } from 'react';

const TimerContext = createContext();

export const TimerProvider = ({ children }) => {
  const initialRemainingTime = parseInt(localStorage.getItem('remainingTime')) || 5 * 60;
  const [remainingTime, setRemainingTime] = useState(initialRemainingTime);
      
  useEffect(() => {
    const intervalId = setInterval(() => {
      setRemainingTime((prevTime) => Math.max(0, prevTime - 1));
    

    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);
  
  const updateRemainingTime = (newTime) => {
    setRemainingTime(newTime);
    localStorage.setItem('remainingTime', newTime.toString()); 
  };

  return (
    <TimerContext.Provider value={{ remainingTime, updateRemainingTime }}>
      {children}
    </TimerContext.Provider>
  );
};

export const useTimer = () => {
  const context = useContext(TimerContext);
  if (!context) {
    throw new Error('useTimer must be used within a TimerProvider');
  }
  return context;
};