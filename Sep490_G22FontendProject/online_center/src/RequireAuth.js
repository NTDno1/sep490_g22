import Cookies from "js-cookie";
import { Navigate, Outlet, useLocation } from "react-router-dom";


const RequireAuth = ({allowRoutes}) => {
   
    const location = useLocation();
    const decoded =  Cookies.get('decoded');
    const roles =  Cookies.get('roleName');
   
    return  allowRoutes.includes(roles) ? <Outlet/> 
    : 
    decoded ?
    <Navigate to="/unauthorized" state={{from: location}} replace /> 
    
    :<Navigate to="/login" state={{from: location}} replace /> 
}
export default RequireAuth;