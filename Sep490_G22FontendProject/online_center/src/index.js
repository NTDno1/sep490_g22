import React from "react"
import ReactDOM from "react-dom/client"

// import AppClient from "./Client/AppClient" 
import { BrowserRouter } from "react-router-dom"
// import AppAdmin from "./Admin/AppAdmin" 
import App from "./App"
import { TimerProvider } from "./useTimer"



const root = ReactDOM.createRoot(document.getElementById("root"))

root.render(
  <React.StrictMode>
  <BrowserRouter>
  <TimerProvider>
    <App/>
    </TimerProvider>
  </BrowserRouter>
</React.StrictMode>
)
