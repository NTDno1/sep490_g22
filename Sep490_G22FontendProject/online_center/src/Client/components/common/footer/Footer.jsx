import React, { useEffect, useRef, useState } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ViewContact, ViewFooter } from "../../../../api/apiClient/Banner/BannerAPI";
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import "./footer.css"

const Footer = () => {
  const [blogs, setBlog] = useState([]);
  const [contact, setContact] = useState([]);
  useEffect(() => {
    fetchDataBlog();
  }, []);

  const fetchDataBlog = async () => {
    try {
      const data = await ViewFooter();
      if (data.result.length > 0) {
        setBlog(data.result);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  };
  useEffect(() => {
    fetchDataContact();
  }, []);
  const fetchDataContact = async () => {
    try {
      const data = await ViewContact();
      if (data.result.length > 0) {
        setContact(data.result);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  };

  return (
    <>

<div class="container-fluid bg-dark text-light footer pt-5 " >
        <div className="container py-5">
          <div className="row g-5">
            <div className="col-lg-4 col-md-6">
              <h4 className="text-white mb-3 custom-h4" >Liên kết nhanh</h4>
              {blogs.map((val)=>
              <a className="btn btn-link " href={val.url}>
              <FontAwesomeIcon icon={faAngleRight} stack="2px" className="fa-angle" />
              {val.title}
              </a>
                )}
            </div>
            <div className="col-lg-4 col-md-6">
              <h4 className="text-white mb-3 custom-h4">Liên hệ</h4>
              {contact.map((val)=>
              <p className="mb-2">
                {val.title}: {val.shortDescription}
              </p>
              )}
            </div>
            <div className="col-lg-3 col-md-6">
              
            </div>
          </div>
        </div>

      </div>
    </>
  )
}

export default Footer
