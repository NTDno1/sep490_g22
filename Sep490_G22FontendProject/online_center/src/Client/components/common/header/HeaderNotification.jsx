import Cookies from 'js-cookie';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import { formatDateString } from "../../../../constants/constant";
import { getHeaderNotification } from '../../../../api/apiClient/Banner/BannerAPI';
export default function HeaderNotification() {
  const [notired, setNotired] = useState(true);
  const [notification, setNotification] = useState([]);
  const box = document.getElementById('box');
  const [down, setDown] = useState(false);
  useEffect(() => {
    getHeadNotification();
  }, []);


  const getHeadNotification = async () => {
    try{
    const data = await getHeaderNotification();
    if(data.result.length > 0){
    setNotification(data.result);
    }}catch (error) {
      console.error("Error fetching data:", error.message);
    }
  }


  const handleIconClick = () => {
    setNotired(false);
    if (down) {
      box.style.height = '0px';
      box.style.display = 'none';
      setDown(false);
    } else {
      box.style.height = '510px';
      box.style.display = 'block';
      setDown(true);
    }

  };


  return (
    <li className="services noti-header" onClick={handleIconClick}>
      <Link to="#" className={`services-link ${notired ? 'notired' : ''}`}>
        <NotificationsNoneIcon />
      </Link>
      <div className="notifi-box" id="box">
        <div className="purple-noti" ></div>
        <h1 className="notification-title">Thông báo</h1>
        <div className="content-noti">
          {notification.map((val, index) => (
            <div className="notifi-item">
              <div className="text">
                <div style={{ color: '#0960a0' }}> {val.title} <br /> <span style={{ fontSize: '15px', color: '#0960a0' }}> {val.className}</span> </div> <div className="notification-time">{formatDateString(val.dateUpdate)}</div>

              </div>
              <div className="notification-descreption">
                {val.description}
              </div>
            </div>
          ))}
        </div>
      </div>

    </li>
  )
}
