import React, { useEffect, useState } from "react"
import "./header.css"
import { getBanner4API, getEventsAPI } from "../../../../api/apiClient/Banner/BannerAPI"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

const Header = () => { 
 
const [banner1,setBanner1] = useState([]);
useEffect(()=> {
  fetchData();
},[])
  const fetchData = async() => {
    try{
    const data = await getEventsAPI();
  
    if(data.result.results){
    setBanner1(data.result.results);
    }}catch (error) {
      console.error("Error fetching data:", error.message);
    } 
  };
  const CustomArrow = ({ onClick, icon, direction }) => (
    <div
    role="button" // Điều này giúp bảo đảm tuân thủ các quy tắc về truy cập (accessibility)
    tabIndex={0} // Điều này giúp kích hoạt sự kiện khi người dùng sử dụng bàn phím
    onClick={onClick}
    className={`custom-arrow ${direction === "left" ? "custom-arrow-left" : "custom-arrow-right"}`}
  >
    <FontAwesomeIcon icon={icon} />
  </div>
  );
  
  const settings = {
    dots: false,
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: true,
    autoplaySpeed: 7000,
    arrows: true,
    prevArrow: <CustomArrow icon={faChevronLeft} direction="left" />,
    nextArrow: <CustomArrow icon={faChevronRight} direction="right" />,
  };
  return (
    
    <>
    <section style={{ border: '1px solid gray',zIndex:'8' }}>
      <Slider {...settings}>
        {banner1.slice(0, 3).map((banner, index) => (
          <div key={index}  className="header-banner">
            <header  className="banner1-container"  style={{
              backgroundImage: `linear-gradient(rgba(4, 9, 30, 0.7), rgba(4, 9, 30, 0.7)), url(${banner.img})`,
            }}>
              <div className="text-box">
                <h1 >{banner.title}</h1>
                <p>{banner.shortDescription}</p>
              </div>
            </header>
          </div>
        ))}
      </Slider>
    </section>
    </>
  )
}

export default Header
