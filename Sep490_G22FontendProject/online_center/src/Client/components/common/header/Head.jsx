import React, { useEffect, useRef, useState } from "react"
import { Link, NavLink } from "react-router-dom"
import "./header.css"
import Cookies from 'js-cookie';
import HeaderNotification from "./HeaderNotification";
import { getHeadAPI } from "../../../../api/apiClient/Banner/BannerAPI";
import { getLastPartAfterSlash } from "../../../../constants/constant";


const Head = () => {
  const expirationTime = new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000);
  const defaultImage = "https://pitech.edu.vn:82/Images/Teacher/DefaultAvatar.png"
  const removesession = () => {
    Cookies.remove('decoded');
    Cookies.remove('CenterName');
    Cookies.remove('jwttoken');
    Cookies.remove('username');
    Cookies.remove('roleName');  
    Cookies.remove('Avatar');    
  }
  const roleName =  Cookies.get('roleName');
  const avatar =  Cookies.get('Avatar');
  const [blogs, setBlog] = useState([]);
  useEffect(() => {
   
    if(avatar){
    if(getLastPartAfterSlash(avatar) === "") {
     
      Cookies.set('Avatar', defaultImage, { expires: expirationTime });
    }
  }
    getHead();
  }, [avatar]);
  
  const getHead = async() => {
    try{
    const data = await getHeadAPI();
    if(data.result.length > 0){
    setBlog(data.result);
    
    }
  }catch(error) {
      console.error("Error fetching data:", error.message);
    }
  };
  return (
    <>
      <header style={{position:'fixed'}} className="header">
        <div className="container">
        {roleName && (
      <button className="centerName" ><span><img className="img-attend" src="https://pitech.edu.vn:82/Images/LogoCenter.png"></img></span></button>
        )}
          <input
            className="hamburger-button"
            type="checkbox"
            id="hamburger-button"
          />
          <label htmlFor="hamburger-button">
            <div/>
          </label>
          <div className="menu">
            <nav>
              <ul>
            
              {!roleName ? (
                <>
                  {blogs.map((val) => (
                  <li key={val.id}>
                    <NavLink to={val.url} activeClassName="active" className="inactive">
                      {val.title}
                    </NavLink>
                  </li>
                ))}
              <li>
              <Link to="/login" >
               Đăng nhập
              </Link>
             
            </li>
            </>
          ):  (  roleName === 'Teacher' ? (
            
            <>
             <li>
         <NavLink to="/" >
          Trang chủ
         </NavLink>
       </li> 
            <li>
         <NavLink to="/courses" >
          Lớp học
         </NavLink>
       </li> 
     
       <li >
         <NavLink to="/schedule">
          Thời khóa biểu
         </NavLink>
       </li> 
       <li >
         <NavLink to="/attend" >
          Điểm danh
         </NavLink>
       </li> 
       <li>
         <NavLink to="/exam" >
          Bài thi
         </NavLink>
       </li> 
       <li className="services dropdowns">     
         <Link className="services-link">
                    Báo cáo <i style={{ marginLeft: '3px' }} className="fa fa-caret-down"></i>
                  </Link>
       
         <div className="drop-down">
                    <ul style={{display:'flex',flexDirection:'column'}} >
                      <li style={{width:'100%'}}> 
                      <Link to="/staticteaching" className="dropdown-item">
          Báo cáo điểm danh
           </Link>
                      </li>
                      <li>
                        <Link to="/exam" className="dropdown-item">
                      Báo cáo bài thi
                        </Link>
                      </li>
                     
                     
                    </ul>
                  </div>
        
       </li>
                <HeaderNotification/>
       <li className="services">
         <a className="services-link" >
         <img
           className="topImg"
           src={`${avatar}`}
           alt=""
   />
         </a>
         <div className="drop-down">
                    <ul style={{display:'flex',flexDirection:'column'}} >
                      <li style={{width:'100%'}}>
                      <Link to="/settings"  className="dropdown-item">
             Hồ sơ cá nhân
           </Link>
                      </li>
                      <li>
                        <Link to="/changePassword" className="dropdown-item">
                        Đổi mật khẩu
                        </Link>
                      </li>
                     
                      <li>
                        <a href="/login" className="dropdown-item"  onClick={() => removesession()}>
                        Đăng xuất
                        </a>
                      </li>
                    </ul>
                  </div>
        
       </li>
         </>
         ) : (
           <>
           <li>
         <NavLink to="/" >
          Trang chủ
         </NavLink>
       </li> 
               <li >
         <Link to="/courses" >
          Lớp học
         </Link>
       </li> 
      
       <li>
         <Link to="/schedule" >
          Thời khóa biểu
         </Link>
         
       </li> 
       <li>
         <Link to="/invoicestudent" >
          Hóa đơn
         </Link>
         </li>
       <li className="services dropdowns">     
         <Link className="services-link">
                    Báo cáo <i style={{ marginLeft: '3px' }} className="fa fa-caret-down"></i>
                  </Link>
       
         <div className="drop-down">
                    <ul style={{display:'flex',flexDirection:'column'}} >
                      <li style={{width:'100%'}}> 
                      <Link to="attendreport"  className="dropdown-item">
          Báo cáo điểm danh
           </Link>
                      </li>
                      <li>
                        <Link to="/mark-report" className="dropdown-item">
                      Báo cáo điểm
                        </Link>
                      </li>
                     
                     
                    </ul>
                  </div>
        
       </li>
            <HeaderNotification/>
         <li className="services dropdowns">
         <Link className="services-link">
         <img
           className="topImg"
           src={`${avatar}`}
           alt=""
   />
                  </Link>
         <div className="drop-down">
                    <ul style={{display:'flex',flexDirection:'column'}} >
                      <li style={{width:'100%'}}> 
                      <Link to="/settings"  className="dropdown-item">
           Hồ sơ cá nhân
           </Link>
                      </li>
                      <li>
                        <Link to="/changePassword" className="dropdown-item">
                       Đổi mật khẩu
                        </Link>
                      </li>
                     
                      <li>
                        <a href="/login" className="dropdown-item"  onClick={() => removesession()}>
                        Đăng xuất
                        </a>
                      </li>
                    </ul>
                  </div>
        
       </li>
           </>
            )
            )}


            </ul>
          </nav>
                 </div>
      </div>
    </header>
  </>
  
  
  )
}

export default Head
