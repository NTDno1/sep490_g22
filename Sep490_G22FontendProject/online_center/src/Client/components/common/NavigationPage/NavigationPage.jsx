import React from 'react'
import { Link } from 'react-router-dom'

const NavigationPage = ({ items, className }) => {
    const renderBreadcrumbItem = (item, index) => {
      return (
        <li key={index} style={{ fontSize: '16px' }} className={`breadcrumb-item ${index === items.length - 1 ? 'active' : ''}`} aria-current="page">
          {index === items.length - 1 ? (
            <div style={{ color:'#2746C0',display:'inline-block' }}>{item.title}</div>
          ) : (
            <Link style={{ fontSize: '18px', textDecoration: 'none', display:'inline-block' ,color:'black' }} to={item.url}>{item.title}</Link>
          )}
        </li>
      );
    };
  
    return (
      <>
        <ol className={`page-header-breadcrumb breadcrumb mb-0 ${className}`}>
          {items.map((item, index) => renderBreadcrumbItem(item, index))}
        </ol>
        <br />
      </>
    );
  }
export default NavigationPage