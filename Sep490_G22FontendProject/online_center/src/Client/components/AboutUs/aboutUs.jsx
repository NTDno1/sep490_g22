import React, { useEffect, useState } from "react";
import "./aboutUs.css";
import Head from "../common/header/Head";
import Header from "../common/header/Header"
import Footer from "../common/footer/Footer";
import NavigationPage from "../common/NavigationPage/NavigationPage";
import { Link, useParams } from "react-router-dom"
import Cookies from "js-cookie"
import { getBanner4API } from "../../../api/apiClient/Banner/BannerAPI";
import BackToTop from "../BackToTop/BackToTop";
import { Box } from "@mui/material";
const AboutUs = () => {
  const [data, setData] = useState([]);
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Giới thiệu', url: '/' },

  ];
  useEffect(() => {
    const fetchDataAbout = async () => {
      try {
        const data = await getBanner4API();
        if (data.result.results) {
          setData(data.result.results);
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchDataAbout();
  }, []);

  return (
    <>
      <Head />
      <Header />
      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />
        <section>
          <div>
            {data.slice(0,1).map((val, index) => (
              <div key={index} style={{ margin: '0 10%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <div style={{ marginLeft: 'auto', marginRight: 'auto', padding: 0 }}>
                  <div className="custom">
                    <h1>{val.title}</h1>
                  </div>
                  <div className="post-content-container">
                    <div className="post-content" dangerouslySetInnerHTML={{ __html: val.description }}></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>
      </Box>
      <BackToTop />
      <Footer />

    </>
  );
};

export default AboutUs;
