import React, { useEffect, useState } from "react"
import "./attend.css"
import { SchedualofClass, getAllClassTeacherSide } from "../../../api/apiClient/Class/ClassAPI";
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from "react-toastify";
import { useNavigate, useParams } from "react-router-dom";
import Cookies from "js-cookie";
import NavigationPage from "../common/NavigationPage/NavigationPage";
import { convertDateFormat, getLastPartAfterSlash } from "../../../constants/constant";
import Head from "../common/header/Head";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import BackToTop from "../BackToTop/BackToTop";
import { ListAttendenceBySchedule, TakeAttendence } from "../../../api/apiClient/Attend/Attend";
import { Box } from "@mui/material";
const AttendDetail = () => {
  const [classes, setClases] = useState([]);

  const [student, setStudent] = useState([]);
  const [slot, setSlot] = useState([]);
  const [scheduleId, setScheduleId] = useState();

  const [studentRadioStates, setStudentRadioStates] = useState([]);

  const attendanceData = [];

  const { slotId, classId } = useParams();
  const [selectclassId, setClassID] = useState(classId);
  const [selectslotId, setSlotID] = useState(slotId);
  let jwttoken = Cookies.get('jwttoken');
  const defaultImage = "https://pitech.edu.vn:82/Images/Teacher/DefaultAvatar.png"
  const handleRadioChange = (index, type) => {
    const updatedStudentRadioStates = studentRadioStates.map((radioState, i) => {
      if (i === index) {
        return {
          ...radioState,
          [type]: true,
          [type === 'attend' ? 'absent' : 'attend']: false
        };
      }
      return radioState;
    });
    setStudentRadioStates(updatedStudentRadioStates);
  };

  const navigate = useNavigate();
  useEffect(() => {
   
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login');
      }
    };
    checkTokenBeforeRoute();
  }, [jwttoken]);

  useEffect(() => {
    
    fetchDataTeacher();
    if (student) {
      const initialStudentRadioStates = student.map((val) => ({
        attend: val.status === 1,
        absent: (val.status === 0 || val.status === 2),
      }));
      setStudentRadioStates(initialStudentRadioStates);
    }
  }, [student]);

  const fetchDataTeacher = async () => {
    try {
      const data = await getAllClassTeacherSide();
     if(slotId === undefined){
      setClases(data.result);
      setClassID(data.result[0].id)
      fetchStudentClass(data.result[0].id);
     }else{
      setClases(data.result);
      setClassID(classId)
      fetchStudentClass(classId);
     }
    } catch (error) { }
  }
  const handleSelectChange = (event) => {

    const selectedClassId = event.target.value;
    setClassID(selectedClassId)
    fetchStudentClass(selectedClassId);
  };
  const handleSelectChangeSlot = (event) => {
    const selectSlotId = event.target.value;
    setSlotID(selectSlotId);
  }

  const fetchStudentClass = async (classId) => {
    try {
      const data = await SchedualofClass(classId);
      setSlot(data.result)
      if(slotId === undefined){
      setSlotID(data.result[0].id)
    }
      else {
        setSlotID(slotId)
      }
    } catch (error) { }
  };

  const handleTakeAttend = async (e) => {
    e.preventDefault();
    student?.forEach((val, index) => {
      const rowData = {
        id: val.id,
        studentId: val.studentId,
        status: document.querySelector(`input[name=contact_${index}_absent]`).checked ? 2 : 1,
        schedualId: parseInt(scheduleId),
        note: document.querySelector('#noteInput').value,
      };
      attendanceData.push(rowData);
    });
    try {
      const jsonData = JSON.stringify(attendanceData);
      const response = await TakeAttendence(jsonData);
      toast.success('Cập nhật thành công')
      handleListStudent(e)

    } catch (error) {
      if (error.response && error.response.data && error.response.data.errorMessages) {
        const errorMessages = error.response.data.errorMessages;
        if (errorMessages && errorMessages.length > 0) {
          const errorMessage = errorMessages[0];
          toast.error(errorMessage);
          console.error('Error when send data:', error);
        }
      }
    }
  }

  const handleListStudent = async (e) => {
    e.preventDefault();
    try {
      const data = await ListAttendenceBySchedule(selectslotId);

      setScheduleId(selectslotId);
      data.result = data.result.map((item) => {
        if ((getLastPartAfterSlash(item.studentImage)) === "") {
          item.studentImage = defaultImage
        }

        return item;
      });
      setStudent(data.result);

    } catch (error) {
      console.error("Xảy ra lỗi")
    }
  }
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Điểm danh', url: '/' },

  ];
  console.log(student)
  return (
    <>
      <Head />

      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />
        <div className="course-card-attend">
          <ToastContainer />
          <form onSubmit={(e) => handleListStudent(e)}>
            <div style={{ marginTop: '30px' }} className="col-xl-10 order-3 order-xl-1">
              <div className="form-group m-form__group row align-items-center">
                <div style={{ display: 'flex' }} className="col-md-5 ">
                  <label className="label-select"> Lớp học</label>
                  <select
                    className="form-control"
                    id="select-course"
                    onChange={handleSelectChange}
                    value={selectclassId}
                  >

                    {classes.map((classItem, index) => (
                      <option key={index} value={classItem.id}>
                        {classItem.name}
                      </option>
                    ))}
                  </select>
                </div>
                <div style={{ display: 'flex' }} className="col-md-5">
                  <label className="label-select"> Ca học</label>
                  <select
                    className="form-control"
                    id="select-course"
                    onChange={handleSelectChangeSlot}
                    value={selectslotId}
                  >

                    {slot.map((slotItem, index) => (
                      <option key={index} value={slotItem.id} >
                        Slot {slotItem.slotNum} - {convertDateFormat(slotItem.dateOffSlot)} ({slotItem.timeStart} - {slotItem.timeEnd})
                        {slotItem.staus === 1 && <span>&#10003;</span>}
                      </option>
                    ))}
                  </select>
                </div>
                <div style={{ display: 'flex' }} className="col-md-2 ">
                  <div className="select-student">
                    <button style={{ display: 'flex', background: ' #2746C0', padding: '10px 30px' }} type="submit" className="attended"> <span style={{ display: 'inline-block', lineHeight: 'auto', marginTop: '-2px', marginRight: '4px' }}><RemoveRedEyeIcon /> </span>Xem</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          {student.length > 0 ? (
            <form onSubmit={handleTakeAttend}>

              {/*end: Search Form */}
              <table style={{ width: '100%' }} id="keywords" cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th><span>ID</span></th>
                    <th><span>Hình ảnh</span></th>
                    <th><span>Học sinh</span></th>
                    <th><span>Trạng thái</span></th>
                    <th><span>Điểm danh</span></th>
                    <th style={{ width: '30%' }}><span>Ghi chú</span></th>
                
                  </tr>
                </thead>
                <tbody>
                  {student?.map((val, index) => (
                    <tr key={index}>
                      <td >{index + 1}</td>
                      <td><img className="img-attend" src={val.studentImage}></img></td>
                      <td> <b style={{ color: 'blue', width: '30%' }}>{val.studentName}</b></td>
                      <td style={{ color: val.status === 1 ? 'green' : val.status === 2 ? 'red' : 'black' }}>
                        {val.status === 0 ? 'Chưa điểm danh' : val.status === 1 ? 'Có mặt' : val.status === 2 ? 'Vắng mặt' : ''}
                      </td>
                   
                      <td style={{ display: 'flex', justifyContent: 'space-around', height: '97px' ,alignItems:'center',lineHeight:'auto',marginTop:'10%' }}>
                        <div>
                          <input
                            type="radio"
                            name={`contact_${index}_absent`}
                            value="absent"
                            checked={studentRadioStates[index]?.absent}
                            onChange={() => handleRadioChange(index, 'absent')}
                          />


                          <label>Vắng mặt</label>
                        </div>
                        <div>
                          <input
                            type="radio"
                            name={`contact_${index}_attend`}
                            value="attend"
                            checked={studentRadioStates[index]?.attend}
                            onChange={() => handleRadioChange(index, 'attend')}
                          />
                          <label>Có mặt</label>
                        </div>
                      </td>
                      <td><input
                        type="text"
                        className="form-control"
                        placeholder=""
                        name="note"
                        id="noteInput"
                      /></td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <div style={{ display: 'flex', justifyContent: 'flex-end', marginLeft: '-20px' }} className="select-student">
                <button style={{ display: 'flex', justifyContent: 'flex-end', background: '#2746C0', padding: '10px 30px' }} className="attended"> <span style={{ marginRight: '3px' }}>&#10003; </span>Lưu</button>
              </div>
            </form>
          ) : (
            <>
              <br />
              <div style={{ textAlign: 'center', fontSize: '18px' }}> Không có dữ liệu</div>
            </>
          )}
        </div>
      </Box>
      <BackToTop />




    </>
  )
}

export default AttendDetail
