import { useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import "./schedule.css"
import {
  Box,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import NavigationPage from "../common/NavigationPage/NavigationPage";
import Head from "../common/header/Head";

import { TeacherClassSchedual } from "../../../api/apiClient/Class/ClassAPI";
import { ListAttendenceByStudent } from "../../../api/apiClient/Attend/Attend";

import Header from "../../../Admin/components/Header";

const Schedule = () => {
  const navigate = useNavigate();
  const [classinfoschedule, setClassInfoSchedule] = useState([]);
  const rolename = Cookies.get('roleName');
  const [renderCalendar, setRenderCalendar] = useState(false);
  const jwttoken = Cookies.get('jwttoken');
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };
      
        checkTokenBeforeRoute();
      }, [jwttoken]);
  useEffect(() => {
   
    if (rolename === "Teacher") {
      getScheduleTeacher();
    } else if (rolename === "Student") {
      getScheduleStudent();
    }

  }, [rolename]);

    
  const getScheduleTeacher = async() => {
    try{
    const data = await TeacherClassSchedual();
    if(data.result.length > 0){ 
    setClassInfoSchedule(data.result);

    setRenderCalendar(true);
    }else {}
    }catch(error){

    }
  }
  const getScheduleStudent = async() => {
    try{
    const data = await ListAttendenceByStudent();
    if(data.result.length > 0){
    setClassInfoSchedule(data.result);
    console.log(data.result)
    setRenderCalendar(true);
    }else{}
    }catch(error){

    }
  }
  function formatDates(dateTimeString) {
    return dateTimeString.split('T')[0];
  }
  const handleEventClick = (selected) => {
   
    if (rolename === "Teacher") {
     
    const slotId = selected.event.id
    const classId = selected.event.extendedProps.classId;
    navigate(`/attend/${classId}/${slotId}`)
    }else if(rolename === "Student"){

    }
  };
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Thời khóa biểu', url: '' },
   
  ];

  
  return (
<>
<Head/>
<Box m="80px">
        <NavigationPage items={breadcrumbItems} />
      <Header title="Thời khóa biểu" subtitle="Thời khóa biểu của tôi"/>
      {renderCalendar && (
            <FullCalendar
              height="75vh"
              plugins={[
                dayGridPlugin,
                timeGridPlugin,
                interactionPlugin,
              ]}
              headerToolbar={{
                left: "prev,next today",
                center: "title",
                right: "timeGridWeek,timeGridDay",              
              }}
              eventClassNames={(arg) => {
                return 'custom-event'; 
              }}
              editable={false}
              selectable={false}
              selectMirror={false}
              dayMaxEvents={false}
              eventClick={handleEventClick}
              slotEventOverlap={false}
              initialView="timeGridWeek"
             
              slotDuration="02:00:00"
              slotMinTime="07:00:00"
              slotMaxTime="23:00:00"
              initialEvents={classinfoschedule.map((val) => ({
                id: val.id,
                classId: val.classId,
                title: `${rolename === 'Student' ?
                  (val.status === 0 ? 'Chưa điểm danh' : val.status === 1 ? 'Có mặt' : val.status === 2 ? 'Vắng mặt' : '') :
                  (rolename === 'Teacher' ? (val.staus === 0 ? 'Chưa điểm danh' : val.staus === 1 ? 'Đã điểm danh' : val.staus ===2 ? 'Kết thúc' : val.staus === 3 ? 'Đang chờ' : val.staus === 4 ? 'Đã đóng': val.staus ===5 ? 'Đổi lịch' : '') : '')}
            ${val.roomName}  
            ${val.className} \n Slot ${val.slotNum} / ${val.totalSlot}`,
                date: formatDates(val.dateOffSlot),
                start: `${formatDates(val.dateOffSlot)} ${val.timeStart}`,
                end: `${formatDates(val.dateOffSlot)} ${val.timeEnd}`,
              }))}
              slotLabelFormat={{
            hour: "2-digit",
            minute: "2-digit",
            omitZeroMinute: false,
            hour12: false,
          }}
            />
          )}
    </Box>
   
    </>
  );
};

export default Schedule;
