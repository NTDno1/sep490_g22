import React, { useEffect, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import "./setting.css";
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import Cookies from 'js-cookie';
import { convertDateFormat, convertDatePicker, formatDateString, getLastPartAfterSlash } from '../../../constants/constant';
import { useNavigate } from 'react-router-dom';
import Head from '../common/header/Head';
import Footer from '../common/footer/Footer';
import { ChangePassword, getInfoStudent, getInfoTeacher } from '../../../api/apiClient/Profile/Profile';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { refreshTokenStudent, refreshTokenTeacher } from '../../../api/refershToken/refreshToken';
import { jwtDecode } from 'jwt-decode';
import { UpdateInfoTeacher } from '../../../api/apiClient/Teacher/teacherAPI';
import { UpdateInfoStudent } from '../../../api/apiClient/Student/studentAPI';
import axios from 'axios';
import { Box } from '@mui/material';
import NavigationPage from '../common/NavigationPage/NavigationPage';
const initialFieldValues = {
  id: '',
  firstName: '',
  midName: '',
  lastName: '',
  phoneNumber: '',
  address: '',
  email: '',
  dob: '',
  updateDate: '',
  image: File
}

export default function Setting() {
  const [user, setValues] = useState(initialFieldValues);
  const host = "https://provinces.open-api.vn/api/";
  const [provinces, setProvinces] = useState([]);
  const username = Cookies.get('username');
  const navigate = useNavigate();
  let jwttoken = Cookies.get('jwttoken');
  const rolename = Cookies.get('roleName');
  const expirationTime = new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000);
  const refreshtokens = Cookies.get('refreshToken');
  const defaultImage = "https://pitech.edu.vn:82/Images/Teacher/DefaultAvatar.png"
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {
        navigate('/login');
      }
    };

    checkTokenBeforeRoute();
    axios.get(`${host}?depth=1`)
      .then((response) => {
        setProvinces(response.data);
      })
      .catch((error) => {
        console.error('Error fetching provinces:', error);
      });
  }, [jwttoken]);

  useEffect(() => {
    if (rolename === "Teacher") {
      getInformationTeacher();
    } else if (rolename === "Student") {
      getInformationStudent();
    }
  }, [])

  const getInformationStudent = async () => {
    try{
    const data = await getInfoStudent(username);
    if(data){
      if((getLastPartAfterSlash(data.image)) === "") {data.image = defaultImage}
    setValues({
      id: data.id,
      firstName: data.firstName,
      midName: data.midName,
      lastName: data.lastName,
      description: data.description,
      address: data.address,
      email: data.email,
      image: data.image,
      phoneNumber: data.phoneNumber,
      dob: data.dob.toString().split('T')[0],
      updateDate: data.dateUpdate,
      image: data.image
    });
    Cookies.set('avatar', data.image, { expires: expirationTime });
  }
  }catch(error){}
  };


  const getInformationTeacher = async () => {
    
    try{
    const data = await getInfoTeacher(username);
    if(data){
        
        if((getLastPartAfterSlash(data.image)) === "") {data.image = defaultImage}
      
    setValues({
      id: data.id,
      firstName: data.firstName,
      midName: data.midName,
      lastName: data.lastName,
      description: data.description,
      address: data.address,
      email: data.email,
      image: data.image,
      phoneNumber: data.phoneNumber,
      dob: data.dob.toString().split('T')[0],
      updateDate: data.updateDate,
      image: data.image
    });
  }
}catch(error){}
  };
  const handleFormSubmit = async (e) => {
    e.preventDefault()
    const formSubmit = new FormData()
    formSubmit.append("id", user.id);
    formSubmit.append("firstName", user.firstName);
    formSubmit.append("lastName", user.lastName);
    formSubmit.append("midName", user.midName);

    formSubmit.append("phoneNumber", user.phoneNumber);
    formSubmit.append("address", user.address);
    formSubmit.append("email", user.email);
    formSubmit.append("dob", user.dob);
    formSubmit.append("userName", username);
    formSubmit.append("image", user.image);
    if (rolename === "Teacher") {
      try {
        const response = await UpdateInfoTeacher(formSubmit);

        const res = await refreshTokenTeacher({
          accessToken: jwttoken,
          refreshToken: refreshtokens
        });
        console.log(res)
        Cookies.set("jwttoken", res.data.accessToken)
        Cookies.set("refreshToken", res.data.refreshToken)
        Cookies.set("Avatar", jwtDecode(res.data.accessToken).Avatar)
        toast.success("Update Success")
        window.location.reload();
      } catch (error) {
        toast.error(error.response.data.errorMessages[0]);
      }
    } else if (rolename === "Student") {
      try {
        const response = await UpdateInfoStudent(formSubmit);
        const res = await refreshTokenStudent({
          accessToken: jwttoken,
          refreshToken: refreshtokens
        });
        console.log(res)
        Cookies.set("jwttoken", res.data.accessToken)
        Cookies.set("refreshToken", res.data.refreshToken)
        Cookies.set("Avatar", jwtDecode(res.data.accessToken).Avatar)
        // toast.success("Update Success")
        // window.location.reload();

      } catch (error) {
        toast.error(error.response.data.errorMessages[0]);
      }
    }
  }


  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({
      ...user,
      [name]: value,
    });
  };
  const handleRemove = () => {
    // const img = document.querySelector('#preview-box');
    // img.src = srcImage.src;
  };
  const handleChange = (event) => {
    setValues({
      ...user,
      image: event.target.files[0]
    })
    const fileUploader = document.querySelector('#customFile');
    const getFile = fileUploader.files;
    if (getFile.length !== 0) {
      const uploadedFile = getFile[0];
      readFile(uploadedFile);
    }
  };

  const readFile = (uploadedFile) => {
    if (uploadedFile) {
      const reader = new FileReader();
      console.log(reader);
      reader.onload = (x) => {
        const img = document.querySelector('#preview-box');
        img.src = reader.result;
      };
      reader.readAsDataURL(uploadedFile);
    }
  };
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Cập nhật thông tin', url: '/' },
  ];
  return (
    <>
      <Head />
      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />
        <div className="container">
          <div className="row">
            <div className="col-12">
              {/* Page title */}
              <div className="my-5">
                <h3>Thông tin của tôi</h3>
                <hr />
              </div>
              {/* Form START */}
              <form className="file-upload" onSubmit={handleFormSubmit}>
                <div className="row mb-5 gx-5">
                  {/* Contact detail */}
                  <div className="col-xxl-8 mb-xxl-0 pb-5">
                    <div className="bg-secondary-soft px-4 py-5 rounded">
                      <div className="row g-3">
                        <div className="text-center mt-1"><h4 style={{ fontWeight: 'bolder' }}>Thông tin</h4></div>
                        {/* First Name */}
                        <div className="col-md-6">
                          <label className="form-label">Họ tên </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder=""
                            aria-label="First name"
                            value={user.firstName}
                            onChange={handleInputChange}
                            name='firstName'
                          />
                        </div>

                        {/* Phone number */}
                        <div className="col-md-6">
                          <label className="form-label">Số điện thoại </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder=""
                            aria-label="Phone number"
                            value={user.phoneNumber}
                            onChange={handleInputChange}
                            name='phoneNumber'
                          />
                        </div>
                        {/* Mobile number */}
                        <div className="col-md-6">
                          <label className="form-label">Địa chỉ</label>
                          <select name="address" value={user.address} className='form-control' onChange={handleInputChange}>

                            {provinces.map(province => (
                              <option key={province.code} value={province.name}>{province.name}</option>
                            ))}
                          </select>
                        </div>
                        {/* Email */}
                        <div className="col-md-6">
                          <label htmlFor="inputEmail4" className="form-label">
                            Email
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            id="inputEmail4"
                            value={user.email}
                            onChange={handleInputChange}
                            name='email'
                          />
                        </div>
                        {/* Skype */}
                        <div className="col-md-6">
                          <label className="form-label">Ngày sinh</label>
                          <DatePicker
                            value={(user.dob)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                            required
                            showYearDropdown
                            scrollableYearDropdown
                            yearDropdownItemNumber={25}
                            maxDate={new Date()}
                            onChange={(date) => {
                              const selectedDate = new Date(date);
                              setValues({
                                ...user,
                                dob: formatDateString(selectedDate),
                              });

                            }}
                          />
                        </div>
                        <div className="col-md-6">
                          <label className="form-label">Ngày cập nhật</label>
                          <input
                            style={{ backgroundColor: '#EEEEEE' }}
                            type="text"
                            className="form-control"
                            placeholder=""
                            value={convertDateFormat(user.updateDate)}
                            name='updateDate' readOnly
                          />
                        </div>
                      </div>{" "}
                      {/* Row END */}

                    </div>

                  </div>

                  {/* Upload profile */}
                  <div className="col-xxl-4">
                    <div className="bg-secondary-soft px-4 py-5 rounded">
                      <div className="row g-3">
                        <div className="mb-4 mt-0 text-center" style={{ fontSize: '20px', fontWeight: 'bolder' }}>Cập nhật ảnh của bạn</div>
                        <div className="text-center">
                          {/* Image upload */}
                          <div className="square position-relative display-2 mb-3">
                            <img style={{ width: '250px', height: '250px' }} src={user.image} id='preview-box'></img>
                          </div>
                          {/* Button */}
                          <input type="file" id="customFile" name="file" accept="image/*" onChange={handleChange} hidden="true" />
                          <label
                            className="btn btn-success-soft btn-block"
                            htmlFor="customFile"
                          >
                            Chọn file
                          </label>
                          <button type="button" style={{ marginTop: '0px' }} className="btn btn-danger-soft" onClick={handleRemove}>
                            Xóa
                          </button>
                          {/* Content */}

                        </div>
                      </div>
                    </div>
                  </div>
                </div>{" "}
                <div className='text-center mb-5 mt-5'><button type="submit" className="btn btn-success-soft">
                  Cập nhật
                </button></div>
                <ToastContainer />
                {/* Row END */}
                {/* Social media detail */}
              </form>



              {/* Row END */}
              {/* button */}


              {/* Form END */}
            </div>
          </div>
        </div>
      </Box>
      <Footer />
    </>

  )
}
