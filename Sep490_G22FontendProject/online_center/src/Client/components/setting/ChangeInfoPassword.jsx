import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import { ChangePassword } from '../../../api/apiClient/Profile/Profile';
import Head from '../common/header/Head';
import Footer from '../common/footer/Footer';
import { Box } from '@mui/material';
import NavigationPage from '../common/NavigationPage/NavigationPage';
const changepassword = {
  oldPassword : '',
  password : '',
  resetPassword: '',
}
export default function ChangeInfoPassword() {
  const [verifyPass, setverifyPass] = useState(changepassword);
  const handleChangePassword = e => {
    const { name, value } = e.target;
    setverifyPass({
      ...verifyPass,
      [name]: value,
    });
  };
  const changePassword = async(e) => {
    e.preventDefault()
    const formSubmit = new FormData()
    formSubmit.append("oldPassword ",verifyPass.oldPassword);
    formSubmit.append("password",verifyPass.password);
    formSubmit.append("resetPassword",verifyPass.resetPassword);
    try {
      const response = await ChangePassword(formSubmit);
      toast.success("Update Success");
      window.location.reload();
    }
    catch (error) {
      toast.error(error.response.data.errorMessages[0])
    }
  }
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Đổi mật khẩu', url: '/' },
  ];
  return (
    <>
    <Head/>
    <Box m="80px">
        <NavigationPage items={breadcrumbItems} />

    <div className="container">
    <div className="row">
      <div className="col-12">
    <form onSubmit={changePassword}>
    <div className="row mb-5 gx-5">

      {/* change password */}
      <div className="col-xxl-12">
        <div className="bg-secondary-soft px-4 py-5 rounded">
        <ToastContainer/>
          <div className="row g-3">
            <div className="my-4 text-center"><h4>Đổi mật khẩu</h4></div>
            {/* Old password */}
            <div className="col-md-12">
              <label htmlFor="exampleInputPassword1" className="form-label">
                Mật khẩu cũ
              </label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                name='oldPassword'
                onChange={handleChangePassword}
                required
              />
            </div>
            {/* New password */}
            <div className="col-md-12">
              <label htmlFor="exampleInputPassword2" className="form-label">
                Mật khẩu mới
              </label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword2"
                name='password'
                onChange={handleChangePassword}
                required
              />
            </div>
            {/* Confirm password */}
            <div className="col-md-12">
              <label htmlFor="exampleInputPassword3" className="form-label">
                Xác nhận mật khẩu *
              </label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword3"
                name='resetPassword'
                onChange={handleChangePassword}
                required
              />
            </div>
          </div>
        </div>
      </div>
      <div className='text-center mt-5 mb-2'><button type="submit" className="btn btn-success-soft">
        Thay đổi
      </button></div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </Box>
    <Footer/>
    </>
  )
}
