import React from "react"

import NotificationCard from "./NotificationCard"
import "./notificationCard.css"
import Head from "../common/header/Head"
import Header from "../common/header/Header"
import Footer from "../common/footer/Footer"
import NavigationPage from "../common/NavigationPage/NavigationPage";
import { Box } from "@mui/material"
const Notification = () => {
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Tin tức', url: '/' },
    
  ];
  return (
    <>
     <Head/>
     <Header/>
     <Box m="80px">
  <NavigationPage items={breadcrumbItems} />
      <section className='blog padding'>
        <p style={{fontWeight:700, fontSize:40, marginBottom:30, textAlign: "center"  }}>CÁC TIN TỨC NỔI BẬT</p>
        <div className='container'>
          <NotificationCard />
        </div>
      </section>
      </Box>
      <Footer/>
    </>
  )
}

export default Notification
