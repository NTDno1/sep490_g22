import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom";
import { ViewNotificationCard } from "../../../api/apiClient/Banner/BannerAPI";
import "../modal.css";
import AOS from 'aos';
import 'aos/dist/aos.css';
const BlogCard = () => {
  const [modals, setmodals] = useState(false);
  const [page, setPage] = useState();
  const [id, setId] = useState(1);
  const togglemodals = () => {
    setmodals(!modals);
  };
  if (modals) {
    document.body.classList.add('active-modals')
  } else {
    document.body.classList.remove('active-modals')
  }
  const [blog, setBlog] = useState([]);

  useEffect(() => {
    const fetchCourseDetails = async () => {
      try {
        const data = await ViewNotificationCard(id);

        if (data.result.results.length > 0) {
          setBlog(data.result.results);
          setPage(data.result.totalPages);
        }
      } catch (error) {
        console.error("Error fetching course details:", error);
      }
    };

    fetchCourseDetails();
  }, [id]);
  useEffect(() => {
    window.scrollTo(0, 0); // Cuộn lên đầu trang khi chuyển trang
  }, [id]);
  const handleLinkClick = (newId) => {
    setId(newId);
  };
  useEffect(() => {
    AOS.init({
      duration: 2500,
      easing: 'ease-in-out',
    });
  }, []);
  const links = [];
  for (let i = 1; i <= page; i++) {
    links.push(
      <span
        className={i === id ? "active" : ""}
        key={i}
        onClick={() => handleLinkClick(i)}
      >
        {i}
      </span>
    );
  }
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Câu hỏi thường gặp', url: '/' },
  ];
  return (
    <>
      <div className="grid2" data-aos="fade-up" data-aos-duration="2500">
        {/* <h1>C</h1> */}
        {blog.map((val) => (
          <div className='items shadow'>
            <a href={`/blog/${val.blogCode}`}>
              <div className='img'>
                <img src={val.img} alt='' />
              </div>
            </a>
            <div className='text1'>
              <div className='admin flexSB'>
                <span className="info-blog">
                  <i className='fa fa-user'></i>
                  <label className="blog-status" htmlFor=''>{val.createdBy}</label>
                </span>
                <span className="info-blog">
                  <i className='fa fa-calendar-alt'></i>
                  <label className="blog-status" htmlFor=''>{val.createdAt}</label>
                </span >
              </div>
              <div className="custom-a">
                <Link to={`/blog/${val.blogCode}`}> <a>{val.title}</a> </Link>
                <p>{val.shortDescription}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
      <br />

      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '20px' }} className="pagination">
        {links}
      </div>
    </>
  )
}

export default BlogCard
