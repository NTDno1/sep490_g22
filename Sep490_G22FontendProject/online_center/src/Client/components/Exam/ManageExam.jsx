import { Box, IconButton, InputBase, Typography, useTheme } from "@mui/material";
import {DataGrid,GridEditCellPropsParams} from "@mui/x-data-grid";
import { tokens } from "../../../Admin/theme";
import { clean } from 'diacritic';
import Header from "../../../Admin/components/Header";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import Sidebar from "../../../Admin/scenes/global/Sidebars";
import Topbar from "../../../Admin/scenes/global/Topbar";
import TextField from '@mui/material/TextField';
import {
  UilEdit,
  UilTimesCircle,
} from "@iconscout/react-unicons";
import { Link, useNavigate,useParams} from "react-router-dom";
import { convertDateFormat } from "../../../constants/constant";
import Cookies from "js-cookie";
import { GetExamList,UpdateExam,DeleteExam } from "../../../api/apiClient/Exam/exam";
const ManageExam = () => {
  const [exam, setExam] = useState([]);
  const  {id}  = useParams();
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [updateExams, setUpdateExams] = useState(false);
  const [subtitle, setSubtitle] = useState('');
  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login_admin');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    getExam();
  }, [id])
  const getExam = async() => {  
    try{
    const data = await GetExamList(id);
  
    setExam(data.result.exams)
    setSubtitle('Khóa học : '+data.result.courseName)
    }catch(error){}
  }
  const updateExamMode = () => {
    setEditMode(!editMode);
  };
  const addNewRow = () => {
    let indexMax = 0;
    let maxExam = null;
    if(exam.length != 0){
      maxExam = exam.reduce((max, current) => {
        return current.examID > max.examID ? current : max;
      });
      indexMax = maxExam.examID;
      console.log(2);
    }
    
    const newRow = { examID: indexMax + 1, examName: '', average: '' };
    setExam((prevRows) => [...prevRows, newRow]);
  };
  const handleTextFieldKeyDown = (e) => {
    if (e.key === ' ') {
      e.stopPropagation();
    }
  };
  const  saveUpdate = async (e) => {
    console.log(exam);
    const newData = exam.map(item => ({
      ...item,
      average: parseFloat(item.average)
    }));
    const dtoRequest = {
      "courseID":parseInt(id),
      "listExam":newData
    }
    console.log(JSON.stringify(dtoRequest))
    const response = await UpdateExam(JSON.stringify(dtoRequest));
    
    if(response.errorMessages.length > 0){
      for(let i = 0;i <  response.errorMessages.length;i++){
        toast.error(response.errorMessages[i], {
          autoClose: 10000, 
        });
      }
    }else{
      setEditMode(!editMode);
      toast.success("Cập nhật thành công");
    }
  };
  const cancel = (e) => {
    setEditMode(!editMode);
    getExam();
  };
  
  const handleTextFieldChange = (id, field, value) => {
    const updateRow = exam.map((exam) =>
    exam.examID === id ? { ...exam, [field]: value } : exam
    );
    setExam(updateRow);
  };

  const themes = useTheme();
  const colors = tokens(themes.palette.mode);
  const removeExam = async (id) => {
    if (window.confirm("Bạn có muốn xóa mục này không ?")) {
      let dataList = exam.filter(exam => exam.examID != id);
      setExam(dataList);
      
     
    }
  }
 
  const columns = [
    {
      headerName: "ID",
      field: "examID",
      align:'center',
      flex: 0.5,
    },
    {
      field: "examName",
      headerName: "Tên bài thi",
      flex: 1,
      cellClassName: "name-column--cell",
      renderCell: (params) =>
        editMode ? (
          <input
          defaultValue={params.value}
          type="text"
          className="form-control"
          disabled={!editMode}
          onChange={(e) => handleTextFieldChange(params.id, params.field, e.target.value)}
        />
        ) : (
          params.value
        )
    },
    {
      field: "average",
      headerName: "Trọng số",
      align:'center',
      flex: 1,
      cellClassName: "name-column--cell",
      renderCell: (params) =>
        editMode ? (
          <input
          defaultValue={params.value}
          type="text"
          className="form-control"
          disabled={!editMode}
          onChange={(e) => handleTextFieldChange(params.id, params.field, e.target.value)}
        />
        ) : (
          params.value
        )
    },
    {
      field: "Action",
      headerName: "Hành động",
      align:'center',
      flex: 1,
      renderCell: (params) =>
        editMode ? (
          <div style={{ position: 'relative' }}>
          <a style={{ cursor: 'pointer' }} onClick={() => removeExam(`${params.row.examID}`)}><UilTimesCircle /> </a>
        </div>
        ) : (
          <div></div>
        )
    }
  ];
  return (

    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <div style={{ display: 'flex' }}>
            <Sidebar isSidebar={isSidebar} />
            <main
              className="content"
              style={{
                minWidth: '170.3vh',
                height: '100vh',
                position: 'relative',
                overflowY: 'auto', // Thêm overflowY: 'auto' để thêm thanh cuộn khi nội dung vượt quá chiều cao màn hình
              }}
            >
              <div>
                <Topbar setIsSidebar={setIsSidebar} />
                <Box m="20px">
                  <ToastContainer />
                  <Header title="Quản lý bài thi" subtitle={subtitle} />
                  <div className="container-fluid">
                       
                  {!editMode ? (<div className="row"><button className="btn btn-success m-btn col-2" onClick={updateExamMode}><i className ="fa fa-pen"></i>Cập nhật bài thi</button></div> ):(
                    <div className="row">
                          <button className="btn btn-success m-btn col-2" onClick={saveUpdate}><i className="fa fa-save"></i>Cập nhật<ToastContainer/></button>
                      
                          <button className="btn btn-danger m-btn col-2" onClick={cancel}><i className="fa fa-window-close"></i>Thoát</button>
                          <div className="col-6"></div>
                          <button className="btn btn-success col-2" onClick={addNewRow}><i className="fa fa-plus"></i>Thêm hàng mới</button>
                          </div>
                       
                   
                  )}
                  </div>
                      
                      
                     
                 
                 
                  
                  <Box>

                    <Box marginTop="20px"
                      width="20%"
                      marginRight="20px"

                      borderRadius="3px"
                    >

                    </Box>
                    <Box marginTop="20px"
                      width="20%"

                      borderRadius="3px"
                    >

                      
                    </Box>
                  </Box>
                  <Box
                    m="40px 0 0 0"
                    height="75vh"
                    sx={{
                      "& .MuiDataGrid-root": {
                        border: "none",
                      },
                      "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                        borderRight: "0.5px solid #E0E0E0 !important"
                      },
                      "& .name-column--cell": {
                        color: colors.greenAccent[300],
                      },
                      "& .MuiDataGrid-columnHeaderTitle":{
                        color:"white"
                      },
                      "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: "#2746C0",
                        borderBottom: "none",
                      },
                      "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: colors.primary[400],
                      },
                      "& .MuiDataGrid-footerContainer": {
                        borderTop: "none",
                        backgroundColor: "#2746C0",
                      },
                      "& .MuiCheckbox-root": {
                        color: `${colors.greenAccent[200]} !important`,
                      },
                    }}
                  >
                    <DataGrid checkboxSelection={editMode} rows={exam} columns={columns}  getRowId={(exam) => exam.examID} />
                  </Box>
                </Box>
              </div>
            </main>
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};


export default ManageExam;
