
import React, { useRef } from 'react';
import "./exam.css"
import { Link, useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer,toast} from "react-toastify"
import Cookies from 'js-cookie';
import NavigationPage from '../common/NavigationPage/NavigationPage';

import { EvaluateExam, GetFilterOfClassAndExam, GetFilterOfCourse, UpdateEvaluateExam } from '../../../api/apiClient/Exam/exam';
import Head from '../common/header/Head';
import { Box } from '@mui/material';

export const Exam = () => {
  const formRef = useRef(null);

  const [isInitialMount, setIsInitialMount] = useState(true);
  const [dataClass, setDataClass] = useState([]
  );
  const [dataCourse, setDataCourse] = useState([]
  );

  const [dataExam, setDataExam] = useState([
  ]);

  const [dataList, setDataList] = useState([
  ]);

  const[stateUpdateButton,setStateUpdateButton] = useState(2);

  const [selectedOptionExam, setSelectedOptionExam] = useState();
  const [selectedOptionClass, setSelectedOptionClass] = useState();
  const [selectedOptionCourse, setSelectedOptionCourse] = useState();

  const username = Cookies.get('username');
  let jwttoken = Cookies.get('jwttoken');

 
      const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };

        checkTokenBeforeRoute();
      }, [jwttoken]);
  useEffect( () => {
    GetFilterCourse();
  }, isInitialMount)
  const GetFilterCourse = async () => {
    try {
      const data = await GetFilterOfCourse(username);
      if (data) {
        setDataCourse(data);
      }
      
      if (isInitialMount) {
        
        showClassAndExamFilterOfCourse(data[0].courseID);
        // showList(dataClass[0].classID, dataExam[0].examID);
        setIsInitialMount(false);      
      }
    } catch (error) {
      console.error(error.message);
    }
  };



  const showClassAndExamFilterOfCourse = async(courseID) => {
    const data = await GetFilterOfClassAndExam(courseID,username);
    if(data){
    setDataClass(data.listClass);
    setDataExam(data.listExam);
    console.log(data)
    if (data.listClass.length > 0 && data.listExam.length > 0) {
    setSelectedOptionClass(data.listClass[0].classID);
    setSelectedOptionExam(data.listExam[0].examID);
    if(isInitialMount){
      setStateUpdateButton(2);
    }
    }
    }
  }

  const showList = async(examID, classID) => {
    try{
    const data = await EvaluateExam(examID,classID);
    console.log(data)
    if(data.length > 0){
      setStateUpdateButton(1);
      setDataList(data);
    }else{
      console.log(1222)
      setStateUpdateButton(2);
      setDataList([]);
    }
  }catch(error) {}
  }
  const handleSelectChangeExam = event => {

    setSelectedOptionExam(event.target.value); // Cập nhật state khi option được chọn
    // showList(event.target.value, selectedOptionClass)

  };
  const handleSelectChangeCourse = event => {

    setSelectedOptionCourse(event.target.value); // Cập nhật state khi option được chọn
    showClassAndExamFilterOfCourse(event.target.value);
    // showList(selectedOptionExam, selectedOptionClass)
  };
  const handleSelectChangeClass = event => {

    setSelectedOptionClass(event.target.value);
    // showList(selectedOptionExam, event.target.value)

  };


  const handleClickUpdate = () => {
    
    setStateUpdateButton(0);
  };
  const filter = () => {
    
    showList(selectedOptionExam, selectedOptionClass)
  };

  const handleExternalSubmit = async () => {
    
    setStateUpdateButton(1);
      try {
        const dataSubmit = dataList.map(item => ({
          ...item,
          mark: parseFloat(item.mark)
        }));
        let jwttoken = Cookies.get('jwttoken');
        console.log(JSON.stringify(dataSubmit))
        const response = await UpdateEvaluateExam(JSON.stringify(dataSubmit),jwttoken);
        // const response = await fetch('https://localhost:7241/api/Exam/UpdateEvaluate', {
        //   method: 'POST',
        //   headers: {
        //     'Content-Type': 'application/json',
        //     'Authorization': 'Bearer ' + jwttoken
        //   },
        //   body: JSON.stringify(dataList),
        console.log(response)
        if (response.isSuccess) {
          toast.success("Cập nhật thành công")
        } else {
          toast.error("Cập nhật thất bại")
        }
      } catch (error) {
        toast.error("Cập nhật không đúng")
      }
  };
  const handleClickCancel = () => {   
    setStateUpdateButton(1);
  };
  const handleInputChange = (event, examID,studentClassID) => {
    const { name, value } = event.target;
    setDataList((prevExams) =>
    prevExams.map((exam) => (exam.examID === examID && exam.studentClassID === studentClassID ? { ...exam, [name]: value } : exam))
    );
  };

  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Bài thi', url: '/' },
    
  ];
  return (
    <>
    <>
    <Head/>
     
<Box m="80px">
<div className="m-grid m-grid--hor m-grid--root m-page">
        
        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
          <div className="m-grid__item m-grid__item--fluid m-wrapper">
            <div className="m-content">
            <NavigationPage items={breadcrumbItems} />
              <div className="m-portlet m-portlet--head-sm m-portlet--rounded">
                <div className="m-portlet__body">

                  <div style={{ marginBottom: '40px' }} className="row align-items-center">

                    <div className="form-group m-form__group row align-items-center">
                      <div className="col-md-3">
                        <span style={{ fontWeight: 'bold' }}>
                          Khóa học
                        </span>
                        {dataCourse.length > 0 ? (<select value={selectedOptionCourse} onChange={handleSelectChangeCourse} className='form-control'>
                          {dataCourse.map(item => (
                            <option key={item.courseID} value={item.courseID}>
                              {item.courseName}
                            </option>
                          ))}
                        </select>):(
                          <select  className='form-control'>
                          
                          <option >
                            Bạn chưa được phân công dạy khóa học nào
                          </option>
                       
                      </select>
                        )}
                        
                      </div>
                      <div className="col-md-3">
                        <span style={{ fontWeight: 'bold' }}>
                          Bài thi - Điểm trung bình
                        </span>
                        {dataExam.length > 0 ? (
                          <select value={selectedOptionExam} onChange={handleSelectChangeExam} className='form-control'>
                          {dataExam.map(item => (
                            <option key={item.eaxmID} value={item.examID}>
                              {item.examName} - {item.average} %
                            </option>
                          ))}
                        </select>
                        ):(
                          <select  className='form-control'>
                          
                            <option >
                              Chưa có bài thi nào cho khóa học
                            </option>
                         
                        </select>
                        )}
                        
                      </div>
                      <div className="col-md-3">
                        <span style={{ fontWeight: 'bold' }}>
                          Lớp học
                        </span>
                        {dataClass.length > 0 ? (<select value={selectedOptionClass} onChange={handleSelectChangeClass} className='form-control'>
                          {dataClass.map(item => (
                            <option key={item.classID} value={item.classID}>
                              {item.className}
                            </option>
                          ))}
                        </select>):(
                          <select  className='form-control'>                          
                            <option>
                              Chưa có lớp học cho khóa học 
                            </option>
                        </select>
                        )

                        }
                        
                      </div>
                      {dataExam.length > 0 && dataClass.length > 0 && dataCourse.length > 0 ? (
                        <div className="col-md-1">
                          <button className='btn btn-success mt-4' onClick={filter}>
                            Lọc
                        </button>
                        
                       

                        
                        
                      </div>
                      
                      ):(
                        <span></span>
                      )}
                      {stateUpdateButton == 1 ? (
                        <div className="col-md-4">
                          <button className='btn btn-success mt-4' onClick={handleClickUpdate}>
                            Cập nhật
                            <ToastContainer/>
                        </button>
                        
                        
                      </div>
                      
                      ):(
                        <span></span>
                      )}
                      {stateUpdateButton == 0 ? (
                       <div className="col-md-2">
                       
                       <button className='btn btn-success mt-4' onClick={handleExternalSubmit}>
                           Lưu
                       </button>&ensp;
                       
                       <button className='btn btn-danger mt-4' onClick={handleClickCancel}>
                           Thoát
                       </button>
                       
                      
                     </div>
                      
                      ):(
                        <span></span>
                      )}
                      

                    </div>

                  </div>

                  {/*end: Search Form */}
                  {dataList[0] == null ? (
                    <div style={{ textAlign: 'center' }}>
                      <h3>Không có dữ liệu học sinh</h3>
                    </div>

                  ) : (
                    <div className="table-responsive" id="table-content">
                      <form>
                      <table
                        className="table table-bordered table-hover"
                        id="class-table"
                        style={{ minHeight: "250px !important" }}
                      >
                        <thead>
                          <tr>

                            <th style={{ width: "5%" }}>STT</th>
                            <th>Tên học sinh</th>
                            <th>Điểm</th>
                            <th>Đánh giá</th>

                          </tr>
                        </thead>
                        <tbody>



                          {dataList.map((item, index) => (
                            <tr>

                              <td key={index}>{index + 1}</td>
                              <td>{item.studentName}</td>
                              {stateUpdateButton == true ? (item.mark === null ? (
                                <td className='text-center'> 
                                  Chưa chấm điểm
                                </td>

                              ) : (
                                <td>{item.mark}</td>
                              )):(<td ><input name='mark' style={{border:'none',outline:'none'}} type='number' onChange={(event) => handleInputChange(event, item.examID,item.studentClassID)} className='text-center' step={0.1} min={0} max={10} defaultValue={item.mark} readOnly={false}/></td>)}
                              


                              {stateUpdateButton == true ? (item.evaluate === null ? (
                                <td className='text-center'> 
                                  Chưa đánh giá
                                </td>

                              ) : (
                                <td>{item.evaluate}</td>
                              )):(<td ><input type='text' name='evaluate' style={{border:'none',outline:'none'}} className='text-center' defaultValue={item.evaluate} onChange={(event) => handleInputChange(event,item.examID,item.studentClassID)} readOnly={false}/></td>)}

                            </tr>
                          ))}


                        </tbody>
                      </table>
                      {stateUpdateButton == 0 ? (
                       <div style={{float:'right'}}>
                       
                       <button className='btn btn-success mt-4' onClick={handleExternalSubmit}>
                           Lưu
                       </button>&ensp;
                       
                       <button className='btn btn-danger mt-4' onClick={handleClickCancel}>
                           Thoát
                       </button>
                       
                      
                     </div>
                      
                      ):(
                        <span></span>
                      )}
                      </form>
                      
                     

                    </div>
                  )}

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Box>
    
    </>

  </>
  )
}
