import React, { useRef } from 'react';
import "./exam.css"
import { Link, useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from "react-toastify"
import Cookies from 'js-cookie';
import NavigationPage from '../common/NavigationPage/NavigationPage';


import { GetAllCourseOfStudent, GetMarkOfStudent } from '../../../api/apiClient/Exam/exam';
import Head from '../common/header/Head';
import { Box, styled } from '@mui/material';


export const MarkReport = () => {
  const formRef = useRef(null);


  const [isInitialMount, setIsInitialMount] = useState(true);



  const [dataFilter, setDataFilter] = useState([
  ]);


  const [dataList, setDataList] = useState([
  ]);
  const [markEvaluate, setMarkEvaluate] = useState(
  );
  const [status, setStatus] = useState(false
  );



  const [selectedOption, setSelectedOption] = useState();


  const username = Cookies.get('username');
  let jwttoken = Cookies.get('jwttoken');



  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    GetFilterCourse();
  }, isInitialMount)
  const GetFilterCourse = async () => {
    try {
      const data = await GetAllCourseOfStudent(username);
      if (data) {
        setDataFilter(data.result);
      }
      console.log(data.result);

      if (isInitialMount) {
        const list = await GetMarkOfStudent(username, data.result[0].courseID, data.result[0].classID);
        if (list.result.length > 0) {
          
          setDataList(list.result);
          let averageMark = 0;
          for (let i = 0; i < list.result.length; i++) {

            if (list.result[i].mark === null) {
              setStatus(true);
            } else {
              averageMark += (list.result[i].mark * list.result[i].average) / 100;

            }
          }
          setMarkEvaluate(averageMark)
        }else{
          setDataList([]);
        }
        setIsInitialMount(false);
      }
    } catch (error) {
      console.error(error.message);
    }
  };









  const showList = async (username, courseID, classID) => {
    const data = await GetMarkOfStudent(username, courseID, classID);
    if (data) {
      setDataList(data.result);
    }
  }
  const handleSelectChange = async (event) => {


    setSelectedOption(event.target.value); // Cập nhật state khi option được chọn
    console.log(event.target.value.split(' ')[0]);
    console.log(event.target.value.split(' ')[1]);
    showList(username, event.target.value.split(' ')[1], event.target.value.split(' ')[0]);
    


  };


  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
{ title: 'Báo cáo điểm', url: '/' },

  ];
  return (
    <>
      <>
        <Head />

        <Box m="80px">
          <NavigationPage items={breadcrumbItems} />
          <div className="m-grid m-grid--hor m-grid--root m-page">

            <div className="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
              <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                  <div className="m-portlet m-portlet--head-sm m-portlet--rounded">
                    <div className="m-portlet__body">


                      <div style={{ marginBottom: '40px' }} className="row align-items-center">


                        <div className="form-group m-form__group row align-items-center">
                          <div className="col-md-3">
                            <span style={{ fontWeight: 'bold' }}>
                              Khóa học - Lớp
                            </span>
                            {dataFilter.length > 0 ? (<select value={selectedOption} onChange={handleSelectChange} className='form-control'>
                              {dataFilter.map(item => (
                                <option key={item.classID} value={item.classID + ' ' +item.courseID} >
                                  {item.courseName} - {item.className}
                                </option>
                              ))}
                            </select>) : (
                              <select className='form-control'>

                                <option >
                                  Bạn chưa được học khóa học nào
                                </option>

                              </select>
                            )}

                          </div>








                        </div>


                      </div>


                      {/*end: Search Form */}
                      {dataList[0] == null ? (
                        <div style={{ textAlign: 'center' }}>
                          <h3>Không có dữ liệu bài kiểm tra</h3>
                        </div>


                      ) : (
                        <div className="table-responsive" id="table-content">
                          <form>
                            <table
                              className="table table-bordered table-hover"
                              id="class-table"
                              style={{ minHeight: "250px !important" }}
                            >
                              <thead>
                                <tr>


                                  <th style={{ width: "5%" }}>STT</th>
                                  <th>Bài kiểm tra</th>
                                  <th>Điểm</th>
                                  <th>Trọng số</th>
                                  <th>Đánh giá</th>


                                </tr>
                              </thead>
<tbody>






                                {dataList.map((item, index) => (
                                  <tr>
                                    <td key={index}>{index + 1}</td>
                                    <td>{item.examName}</td>
                                    <td>{item.mark}</td>
                                    <td>{item.average}</td>
                                    <td>{item.evaluate}</td>



                                  </tr>
                                ))}
                                <tr>
                                  <td colSpan={2} style={{ minHeight: "300px !important" }}>Tổng kết</td>
                                  {status == true ? (<td ></td>) : (markEvaluate >= 5 ? (<td style={{ fontWeight: "bold", color: "green" }}>{markEvaluate}</td>) : (<td style={{ fontWeight: "bold", color: "red" }}> {markEvaluate}</td>))}
                                  <td colSpan={2} style={{ fontWeight: "bold" }}>Điểm trung bình </td>
                                </tr>
                                <tr>
                                  <td colSpan={2}></td>
                                  {status == true ? (<td style={{ fontWeight: "bold", color: "yellowgreen" }}>Đang học</td>) : (markEvaluate >= 5 ? (<td style={{ fontWeight: "bold", color: "green" }}>Qua</td>) : (<td style={{ fontWeight: "bold", color: "red" }}> Học lại</td>))}

                                  <td colSpan={2} style={{ fontWeight: "bold" }}>Tình trạng </td>



                                </tr>




                              </tbody>
                            </table>
                          </form>



                        </div>
                      )}


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </Box>
    </>


  </>
  )
}
