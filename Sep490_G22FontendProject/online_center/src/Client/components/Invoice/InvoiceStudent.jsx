import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../../Admin/theme";
import { Link, useNavigate } from 'react-router-dom'
import { useEffect, useState } from "react";
import ViewListIcon from '@mui/icons-material/ViewList';
import NavigationPage from "../common/NavigationPage/NavigationPage";
import Cookies from "js-cookie";
import Head from "../common/header/Head";
import { listOrderbyAdmin } from "../../../api/apiClient/Order/Order";
import { convertDateFormat } from "../../../constants/constant";
const InvoiceStudent = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [order, setOrder] = useState([]);
  const jwttoken = Cookies.get('jwttoken');
  const apiRef = useGridApiRef();
  const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };    
        checkTokenBeforeRoute();
      }, [jwttoken]);
      useEffect(() => {
        const getOrderAdmin = async () => {
          try {
            const data = await listOrderbyAdmin();
            if (data.result.length > 0) {
              data.result = data.result.map((item) => {
                item.createDate = convertDateFormat(item.createDate);
                item.updateDate = convertDateFormat(item.updateDate);
                return item;
              });
              setOrder(data.result)
             
                apiRef.current.setPageSize(7);
               
            } else { }
          } catch (error) { }
        }
        getOrderAdmin();
    
    
      }, [apiRef])
      const dataWithOrder = order.map((item, index) => ({
        ...item,
        OrderID: index + 1,
      }))
      const breadcrumbItems = [
        { title: 'Trang chủ', url: '/' },
        { title: 'Hóa đơn', url: '' },
     
      ];

  const columns = [
    {
      field: "name",
      headerName: "Tên hóa đơn",
      flex: 0.5,
      cellClassName: "name-column--cell",
    },
    {
      field: "orderCode",
      headerName: "Mã hóa đơn",
      flex: 0.5,
    },
    {
      field: "createDate",
      headerName: "Ngày thanh toán",
      flex: 0.5,
    },
    {
      field: "note",
      headerName: "Ghi chú",
      flex: 1,
    },
    {
        field: "",
        headerName: "Xem chi tiết",
        flex: 0.25,
        cellClassName: "name-column--cell",
        renderCell: (params) => (
          <Link style={{ textDecoration: 'none' }} to={`/orderStudentdetail/${params.row.id}`}>
            <ViewListIcon/>
          </Link>
        )
      },
  ];
  return (
    <>
    <Head/>
   
<Box m="80px">
<NavigationPage items={breadcrumbItems} />
<Box
            m="40px 0 0 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .name-column--cell": {
                color: colors.greenAccent[300],
              },
              "& .MuiDataGrid-columnHeaderTitle":{
                color:"white"
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: "#2746C0",
                borderBottom: "none",
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: colors.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: "#2746C0",
              },
              "& .MuiCheckbox-root": {
                color: `${colors.greenAccent[200]} !important`,
              },
            }}
          >
            <DataGrid pageSizeOptions={[7, 10,15]}  apiRef={apiRef} rows={dataWithOrder} columns={columns} />
          </Box>
   
    </Box>
   
    </>
  );
};

export default InvoiceStudent;
