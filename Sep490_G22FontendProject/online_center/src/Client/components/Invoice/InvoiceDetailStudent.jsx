import React, { useEffect, useState } from 'react'

import 'react-toastify/dist/ReactToastify.css';
import { useNavigate, useParams } from 'react-router-dom';

import Cookies from 'js-cookie';
import { convertDateFormat, formatNumberWithDecimalSeparator } from '../../../constants/constant';
import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../../Admin/theme";
import { listOrderDetailbyID, listOrderbyID } from '../../../api/apiClient/Order/Order';

import axios from 'axios';
import Head from '../common/header/Head';
import NavigationPage from '../common/NavigationPage/NavigationPage';


const initialFieldValuesOrder = {
    name: '',
    title: '',
    orderCode: '',
    createDate: '',
    updateDate: '',
    studentName: '',
    status: '',
    employeeName: '',
    note: '',

}
export default function InvoiceDetailStudent() {
    const [order, setOrder] = useState([]);

    const [valuesOrder, setValuesOrder] = useState(initialFieldValuesOrder);
    let totalPrice = 0;

    let jwttoken = Cookies.get('jwttoken');
    const [course, setCourse] = useState([]);
    const { id } = useParams();
    const navigate = useNavigate();
    const [theme, colorMode] = useMode();
    const [isSidebar, setIsSidebar] = useState(true);


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login_admin');
            }
        };
        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        getOrderAdmin();
        getOrderDetailbyId();
    }, []);
    const getOrderDetailbyId = async () => {

        try {

            const data = await listOrderDetailbyID(id)
            const result = data.result;
            setValuesOrder({
                name: result.name,
                title: result.title,
                orderCode: result.orderCode,
                createDate: convertDateFormat(result.createDate),
                updateDate: convertDateFormat(result.updateDate),
                studentName: result.studentName,
                status: result.status,
                employeeName: result.employeeName,
                note: result.note,

            });
        } catch (error) {

        }

    }

    const getOrderAdmin = async () => {
        try {
            const data = await listOrderbyID(id);
            console.log(data)
            if (data.result) {
                data.result = data.result.map((item) => {
                    item.createDate = convertDateFormat(item.createDate);
                    item.updateDate = convertDateFormat(item.updateDate);
                    return item;
                });
                setOrder(data.result)

            } else { }
        } catch (error) { }
    }
    order.forEach((val) => {
        totalPrice += val.price;
    });
    const PDFGenerate = async (e) => {
        try {
            e.preventDefault()
            const response = await axios.get(`https://localhost:7241/api/Order/generatepdf?orderId=${id}`, {
                responseType: 'arraybuffer',
                headers: {
                    'Authorization': `Bearer ${jwttoken}`,
                },
            });
            const blob = new Blob([response.data], { type: 'application/pdf' });
            const url = URL.createObjectURL(blob);

            // Mở trang mới hiển thị PDF
            window.open(url, '_blank');
            console.log(url)
        } catch (error) {
            console.error('Error generating PDF:', error);
        }
    }
    const breadcrumbItems = [
        { title: 'Trang chủ', url: '/' },
        { title: 'Hóa đơn', url: '/invoicestudent' },
        { title: 'Chi tiết', url: '' },

    ];
    return (
        <>
            <Head />

            <Box m="80px">
                <NavigationPage items={breadcrumbItems} />
                <div className="row">

                    <div className="col-12">

                        <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '40px' }}>
                            <div className="m-portlet__head-tools m--pull-right">

                            </div>

                            <div
                                className="m-portlet__head-tools m--pull-right"
                                style={{ marginTop: 8, marginRight: 10, display: 'flex' }}
                            >
                                <form onSubmit={PDFGenerate}>
                                    <button style={{ background: '#2746C0', padding: '10px 20px' }} type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="save-btn" >
                                        Xem dưới dạng PDF
                                    </button>
                                </form>
                            </div>

                        </div>

                        <div className="table-responsive" id="table-content">
                            <table
                                className="table table-bordered table-hover"
                                id="class-table"
                                style={{ minHeight: "250px !important" }}
                            >
                                <thead>
                                    <tr>

                                        <th>STT</th>
                                        <th>Tên khóa học</th>
                                        <th>Giảm giá</th>
                                        <th>Giá</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày cập nhật</th>

                                        <th>Ghi chú</th>



                                    </tr>
                                </thead>
                                <tbody>
                                    {order.map((val, index) => (
                                        <tr>
                                            <td>
                                                <div>{index + 1}</div>
                                            </td>
                                            <td>
                                                <span
                                                >
                                                    {val.courseName}
                                                </span>
                                            </td>
                                            <td>{val.discount}%</td>
                                            <td>
                                                {val.price}
                                            </td>
                                            <td>
                                                {val.createDate}
                                            </td>
                                            <td>
                                                {val.updateDate}
                                            </td>
                                            <td>
                                                {val.note}
                                            </td>

                                        </tr>
                                    ))}
                                    <tr>
                                        <td colSpan="5">Tổng giá trị hóa đơn</td>
                                        <td>{formatNumberWithDecimalSeparator(totalPrice)}VNĐ</td>
                                        <td colSpan="2"></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>


                        {/* Form END */}
                    </div>
                    <h3> Chi tiết hóa đơn</h3>
                    <div className="table-responsive" id="table-content">
                        <table
                            className="table table-bordered table-hover"
                            id="class-table"
                            style={{ minHeight: "250px !important" }}
                        >
                            <thead>
                                <tr>

                                    <th>Tên hóa đơn</th>
                                    <th>Mã hóa đơn</th>

                                    <th>Học viên</th>
                                    <th>Ghi chú</th>
                                    <th>Ngày tạo</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        {valuesOrder.name}
                                    </td>
                                    <td>{valuesOrder.orderCode}</td>
                                    <td>
                                        {valuesOrder.studentName}
                                    </td>

                                    <td>{valuesOrder.note}</td>
                                    <td>{valuesOrder.createDate}</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                    <div class="col-md-12" align="right">


                        <div className="col-md-12" style={{ display: 'flex', justifyContent: 'space-between' }} align="right">
                            <a className='backtolist' href='/invoicestudent'> {'<< Quay về danh sách'}</a>

                        </div>

                    </div>
                </div>

            </Box>

        </>
    )
}
