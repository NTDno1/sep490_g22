import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../../Admin/theme";
import { useNavigate } from 'react-router-dom'
import { useEffect, useState } from "react";

import NavigationPage from "../common/NavigationPage/NavigationPage";
import Cookies from "js-cookie";

import Head from "../common/header/Head";

import { ListTeacherTeachingHour } from '../../../api/apiClient/TeachingHours/teachinghours';
const Statisofteaching = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const[teachinghour,setTeachingHour] = useState([]);
  const jwttoken = Cookies.get('jwttoken');
  const apiRef = useGridApiRef();
  const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };    
        checkTokenBeforeRoute();
      }, [jwttoken]);
  useEffect(() => {
    TeachingHour();
    setTimeout(() => {
    apiRef.current.setPageSize(15);
    },10)
  }, []);
  const TeachingHour = async() => {
    try{
    const data = await ListTeacherTeachingHour();
    if(data.result.length > 0){
    const newData = data.result.map((item, index) => {
      return {
        ...item,
        dateOffSlot: convertDateFormat(item.dateOffSlot)
      };
    });
    setTeachingHour(newData)
  }else{}
  }catch(error) {}
  }
  const convertDateFormat = (inputDate) => {
    const dateTime = new Date(inputDate);
    const day = dateTime.getDate();
    const month = dateTime.getMonth() + 1;
    const year = dateTime.getFullYear();

    const formattedDate = `${day < 10 ? '0' + day : day}/${month < 10 ? '0' + month : month}/${year}`;
    return formattedDate;
  }
  let totalTimeTeaching = 0;
  teachinghour?.map((val, index) => {
    if (!isNaN(val.timeTeaching)) {
      totalTimeTeaching += val.timeTeaching; 
    }
  });
  const totalHours = Math.floor(totalTimeTeaching / 60);
      const totalMinutes = totalTimeTeaching % 60;
      const displayTime = `${totalHours} hours ${totalMinutes} minutes`;
      const breadcrumbItems = [
        { title: 'Trang chủ', url: '/' },
        { title: 'Báo cáo thời gian dạy', url: '' },
     
      ];

  const columns = [
    {
      field: "className",
      headerName: "Lớp học",
      flex: 1.5,
      cellClassName: "name-column--cell",
    },

    {
      field: "dateOffSlot",
      headerName: "Ca học",
      flex: 1,
    },
    {
      field: "slotNum",
      headerName: "Buổi học",
      flex: 1,
    },
    {
      field: "timeTeaching",
      headerName: "Thời gian dạy",
      flex: 1,
    },
    {
      field: "timeStart",
      headerName: "Bắt đầu",
      flex: 1,
    },
    {
      field: "timeEnd",
      headerName: "Kết thúc",
      flex: 1,
    },
    {
      field: "day",
      headerName: "Ngày",
      flex: 1,
    }
  ];
  return (
    <>
    <Head/>
   
<Box m="80px">
<NavigationPage items={breadcrumbItems} />
<Box
            m="40px 0 0 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .name-column--cell": {
                color: colors.greenAccent[300],
              },
              "& .MuiDataGrid-columnHeaderTitle":{
                color:"white"
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: "#2746C0",
                borderBottom: "none",
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: colors.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: "#2746C0",
              },
              "& .MuiCheckbox-root": {
                color: `${colors.greenAccent[200]} !important`,
              },
            }}
          >
            <DataGrid pageSizeOptions={[15, 20,25]}  apiRef={apiRef} rows={teachinghour} columns={columns} />
          </Box>
    <div style={{display:'flex',justifyContent:'flex-end'}}><h3>Tổng số giờ dạy: </h3>&ensp; <h4>{`${displayTime}`}</h4></div>
    </Box>
   
    </>
  );
};

export default Statisofteaching;
