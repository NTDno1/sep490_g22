import Cookies from 'js-cookie';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { convertDateFormat } from '../../../constants/constant';
import Head from '../common/header/Head';
import Footer from '../common/footer/Footer';
import { ListAttendenceByClassId } from '../../../api/apiClient/Attend/Attend';
import { getAllClassStudentSide } from '../../../api/apiClient/Class/ClassAPI';
import { ToastContainer, toast } from 'react-toastify';
import { Box } from '@mui/material';
import NavigationPage from '../common/NavigationPage/NavigationPage';

export default function AttendReport() {
  const [attend, setAttend] = useState([]);
  const [classId, setClassId] = useState([]);
  const [classes, setClasses] = useState([]);
  const [clicked, setClicked] = useState(false);
  const [clickedItem, setClickedItem] = useState(null);

  useEffect(() => {
    getListAttened();
    getClasses();
  }, [classId]);
  const getListAttened = async () => {
    try {
      const data = await ListAttendenceByClassId(classId);
      console.log(data);
      if (data && data.result) {
        setAttend(data.result);
      } else {
        toast.error("Không có dữ liệu hoặc dữ liệu không hợp lệ.");
      }
    } catch (error) {
      // toast.error("Đã xảy ra lỗi khi lấy danh sách điểm danh:", error.message);
    }
  };
  const getClasses = async () => {
    try{
    const data = await getAllClassStudentSide();
    setClasses(data.result);
    }catch(error){
      
    }
  }
  const handleItemClick = (id) => {
    setClassId(id);
    setClicked(!clicked);
    setClickedItem(id);
  };
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Báo cáo điểm danh', url: '/' },
  ];
  return (
    <>
      <ToastContainer />
      <Head />
      
<Box m="80px">
        <NavigationPage items={breadcrumbItems} />
      <div className="m-content">
        <ol class="page-header-breadcrumb breadcrumb mb-0">
          <li class="breadcrumb-item">
            <Link style={{ fontSize: '24px' }} to="/">Trang chủ</Link>
          </li>

          <li style={{ fontSize: '24px' }} class="breadcrumb-item active" aria-current="page">Báo cáo điểm danh</li>
        </ol>
        <div style={{ height: '80vh', overflow: 'auto' }} className="course-card-title">


          {/*end: Search Form */}
          <div style={{ display: 'flex', alignItems: 'flex-start' }} >
            <table style={{ flex: 0.125 }} id="keywords" cellspacing="0" cellpadding="0">
              <thead >
                <tr>
                  <th><span>Lớp học</span></th>
                </tr>
              </thead>
              <tbody>
                {classes.map((val, index) => (
                  <tr key={index}>
                    <td>
                      <a
                        style={{
                          cursor: 'pointer',
                          color: clickedItem === val.id ? 'blue' : 'black',
                        }}
                        onClick={() => handleItemClick(val.id)}
                      >
                        {val.name}
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <table style={{ flex: 0.875 }} id="keywords" cellspacing="0" cellpadding="0">
              <thead >
                <tr>
                  <th><span>STT</span></th>
                  <th><span>Ngày</span></th>
                  <th><span>Ca học</span></th>
                  <th><span>Phòng học</span></th>
                  <th><span>Lớp học</span></th>
                  <th><span>Trạng thái</span></th>

                </tr>
              </thead>
              <tbody>
                {attend?.map((val, index) => (
                  <tr>
                    <td >{val.slotNum}</td>
                    <td>{convertDateFormat(val.dateOffSlot)}</td>
                    <td>{val.timeStart} - {val.timeEnd}</td>
                    <td>  {val.roomName}</td>
                    <td>  {val.className}-{val.classCode}</td>
                    <td style={{ color: val.status === 1 ? 'green' : val.status === 2 ? 'red' : 'black' }}>
                    {val.status === 1 ? 'Có mặt' : val.status === 2 ? 'Vắng mặt' : 'Chưa điểm danh'}
                    </td>

                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

      </div>
      </Box>
      <Footer />
    </>
  )
}
