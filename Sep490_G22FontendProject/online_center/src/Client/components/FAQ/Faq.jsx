import { Box, useTheme } from "@mui/material";
import Header from "../common/header/Header"
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { tokens } from "../../../Admin/theme";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import fetchData from "../../../api/apiUtils";
import NavigationPage from "../common/NavigationPage/NavigationPage";
import Head from "../common/header/Head";
import Footer from "../common/footer/Footer";
import { GetFAQs } from "../../../api/apiClient/FAQ/faq";
const Faq = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [faq, setFaq] = useState([]);
  useEffect(() => {
    fetchDataFAQ();
  }, []);
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Câu hỏi thường gặp', url: '/' },

  ];

  const fetchDataFAQ = async () => {
    try {
      const data = await GetFAQs();
      if (data.result.length > 0) {
        setFaq(data.result);
      }
    } catch (error) { }
  };
  return (
    <>
      <Head />
      <Header />
      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />
        <Box mb="10px">
          <Typography
            variant="h2"
            color="#2746C0"
            fontWeight="bold"
            sx={{
              m: "0 0 5px 0",
              fontFamily: "Arial", // Thêm thuộc tính fontFamily để đặt font chữ là Arial
            }}
          >
            Câu hỏi
          </Typography>
          <Typography variant="h5" sx={{
            fontFamily: "Arial", // Thêm thuộc tính fontFamily để đặt font chữ là Arial
          }} color="#2746C0">
            Câu hỏi thường gặp
          </Typography>
        </Box>
        {faq.map((val, index) => (
          <Accordion defaultExpanded={false}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography color="#2746C0" variant="h5">
                {val.title}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                {val.desc}
              </Typography>
            </AccordionDetails>
          </Accordion>
        ))}
      </Box>
      <Footer />
    </>
  );
};

export default Faq;
