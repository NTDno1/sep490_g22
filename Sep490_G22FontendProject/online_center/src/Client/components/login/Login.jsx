import React, { useState } from 'react'
import './login.css'
import user_icon from '../Assets/person.png'
import password_icon from '../Assets/password.png'
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { jwtDecode } from 'jwt-decode';
import { Button, CardActions, CardContent, Divider, TextField } from "@mui/material"
import RefreshIcon from "@mui/icons-material/Refresh"
import Cookies from 'js-cookie'
import Head from '../common/header/Head'
import { LoginStudent, LoginTeacher } from '../../../api/apiClient/Authentication/Authentication'
export default function Login() {
    const [selectedRole, setSelectedRole] = useState('Teacher');
    const randomString = Math.random().toString(36).slice(2, 8);
    const [capcha, setCapcha] = useState(randomString);
    const [text, setText] = useState("");
    const expirationTime = new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000);
    const SwitchRoleStudent = () => {
       
        setSelectedRole('Student');

    };
    const SwitchRoleTeacher = () => {
       
        setSelectedRole('Teacher');
    };
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || "/";

    const ProceedLogin = async(e) => {
        e.preventDefault();
        if(text === capcha){
            if (selectedRole === 'Teacher' && validate()) {
              let inputObj = {
                "username": username,
                "password": password,
                "role": "Teacher"
              };
      
              try {
                const response = await LoginTeacher(inputObj);
                const data = response;
                const token = data.tokenInformation.accessToken;
                const refreshToken = data.tokenInformation.refreshToken
                if (token) {
      
                  try {
                    // Phân tích token
                    const decoded = jwtDecode(token);
      
                    Cookies.set('jwttoken', token, { expires: expirationTime });
                    Cookies.set('refreshToken', refreshToken, { expires: expirationTime });
                    Cookies.set('decoded', decoded, { expires: expirationTime });
                    Cookies.set('username', decoded.name, { expires: expirationTime });
                    Cookies.set('CenterName', decoded.CenterName, { expires: expirationTime });
                    Cookies.set('roleName', decoded.RoleID, { expires: expirationTime });
                    Cookies.set('Avatar', decoded.Avatar, { expires: expirationTime });
                    Cookies.set('empId', decoded.ID, { expires: expirationTime });
                    navigate("/");
      
                  } catch (error) {
                    if (error.response && error.response.data && error.response.data.errorMessages) {
                        const errorMessages = error.response.data.errorMessages;
                        if (errorMessages && errorMessages.length > 0) {
                          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                          const errorMessage = errorMessages[0];
                          toast.error(errorMessage); // Hiển thị thông điệp lỗi
                          console.error('Error:', error);
                        }
                      }
                  }
                }
              }
              catch (error) {
                if (error.response && error.response.data && error.response.data.errorMessages) {
                    const errorMessages = error.response.data.errorMessages;
                    if (errorMessages && errorMessages.length > 0) {
                      // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                      const errorMessage = errorMessages[0];
                      toast.error(errorMessage); // Hiển thị thông điệp lỗi
                      console.error('Error:', error);
                    }
                  }
              }
              }  
               if (selectedRole === 'Student' && validate()) {
                let inputObj = {
                  "username": username,
                  "password": password,
                  "role": "Student"
                };
        
                try {
                  const response = await LoginStudent(inputObj);
                  const data = response;
                  const token = data.tokenInformation.accessToken;
                  const refreshToken = data.tokenInformation.refreshToken
                  if (token) {
        
                    try {
                      // Phân tích token
                      const decoded = jwtDecode(token);
                      Cookies.set('jwttoken', token, { expires: expirationTime });
                      Cookies.set('refreshToken', refreshToken, { expires: expirationTime });
                      Cookies.set('decoded', decoded, { expires: expirationTime });
                      Cookies.set('username', decoded.name, { expires: expirationTime });
                      Cookies.set('CenterName', decoded.CenterName, { expires: expirationTime });
                      Cookies.set('roleName', decoded.RoleID, { expires: expirationTime });
                      Cookies.set('Avatar', decoded.Avatar, { expires: expirationTime });
                      Cookies.set('empId', decoded.ID, { expires: expirationTime });
                      navigate("/");
        
                    } catch (error) {
                        if (error.response && error.response.data && error.response.data.errorMessages) {
                            const errorMessages = error.response.data.errorMessages;
                            if (errorMessages && errorMessages.length > 0) {
                              // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                              const errorMessage = errorMessages[0];
                              toast.error(errorMessage); // Hiển thị thông điệp lỗi
                              console.error('Error:', error);
                            }
                          }
                    }
                  }
                }
                catch (error) {
                    if (error.response && error.response.data && error.response.data.errorMessages) {
                        const errorMessages = error.response.data.errorMessages;
                        if (errorMessages && errorMessages.length > 0) {
                          // Lấy thông điệp lỗi đầu tiên trong mảng errorMessages
                          const errorMessage = errorMessages[0];
                          toast.error(errorMessage); // Hiển thị thông điệp lỗi
                          console.error('Error:', error);
                        }
                      }
                }
              }
    } else{
        toast.error("Vui lòng nhập lại thông tin ")
    }
    }
    const validate = () => {
        let result = true;
        if (username === '' || username === null) {
            result = false;
            alert('Vui lòng nhập lại thông tin');
        }
        if (password === '' || password === null) {
            result = false;
            alert('Vui lòng nhập lại thông tin');
        }
        return result;
    }
    const refreshString = () => {
        const randomString = Math.random().toString(36).slice(2, 8); 
        setCapcha(randomString);
    }
    return (
        <>
       
        <body className='body'>
       

            <div className='login-switch'>
                <div className='login-switch-role'>
                    <div style={{ margin: '0' }} className='login-header'>
                        <div style={{ display: 'flex' }} className='login-text'>
                            <div className={`role-title ${selectedRole === 'Teacher' ? 'role-selected' : ''}`} onClick={SwitchRoleTeacher}>Giáo viên</div>
                            <div className={`role-title ${selectedRole === 'Student' ? 'role-selected' : ''}`} onClick={SwitchRoleStudent} >Học sinh</div>
                        </div>

                    </div>
                </div>

                {/* <button className="button_backtoHome-title">
            <span className="button_backtoHome">X</span>
                </button> */}
            </div>

            <div className='login-container'>

                <form onSubmit={ProceedLogin} className='login-form-teacher'>
                    <div className='login-header'>
                        <div className='login-text'>POST</div>
                        <div className='login-underline'></div>
                    </div>
                    <div className='login-inputs'>
                        <div className='login-input'>
                            <img src={user_icon} alt='' />
                            <input value={username} onChange={(e) => setUsername(e.target.value)} type='text' placeholder='Tên người dùng' required></input>
                        </div>

                        <div className='login-input'>
                            <img src={password_icon} alt='' />
                            <input value={password} onChange={(e) => setPassword(e.target.value)} type='password' placeholder='Mật khẩu' required></input>
                        </div>
                    </div>
                    <div >
                        <Divider />
                        <CardContent >
                            <CardActions className='validate-capcha'>
                                <div className='h3-capcha'>
                                    {capcha}
                                </div>
                                <Button style={{marginLeft:'13px'}} startIcon={<RefreshIcon />} onClick={() => refreshString()}></Button>
                            </CardActions>                         
                                <TextField
                                    label="Nhập CAPTCHA"
                                    focused
                                    fullWidth
                                    value={text}
                                    onChange={(e) => setText(e.target.value)}
                                />                      
                        </CardContent>

                    </div>       
                    <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around'}}>
                    <Link to="/forgetpassword" className='forget-password'>Quên mật khẩu?</Link>
                    <Link to="/" className='forget-password'>Quay về trang chủ</Link>
                    </div>         
                    <div className='login-submit-container'>
                        <button style={{marginTop:'50px'}} type='submit' className={"login-submit"}>Đăng nhập</button>
                    </div>
                  
                    <ToastContainer />
                </form>


            </div>
        </body>
        </>
    )
}
