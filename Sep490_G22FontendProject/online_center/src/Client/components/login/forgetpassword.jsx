import React, { useEffect, useState } from 'react'
import './login.css'
import user_icon from '../Assets/person.png'
import password_icon from '../Assets/password.png'
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom'

export default function ForgetPassword() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const navigate = useNavigate();

    const [selectedRole, setSelectedRole] = useState('Teacher');
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [remainingTime, setRemainingTime] = useState(5 * 60); // Initial time in seconds

    const [timeoutId, setTimeoutId] = useState(null);
    const SwitchRoleStudent = () => {

        setSelectedRole('Student');

    };
    const SwitchRoleTeacher = () => {

        setSelectedRole('Teacher');
    };



    const ProceedLogin = (e) => {
        e.preventDefault();
        if (isSubmitting) {
            // If already submitting, do nothing
            return;
        }
        setIsSubmitting(true);
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        const newTimeoutId = setTimeout(() => {
            setIsSubmitting(false);
        }, 5 * 60 * 1000); // 5 minutes in milliseconds

        setTimeoutId(newTimeoutId);
        if (selectedRole === 'Teacher') {

            let inputObj = {
                "userName": username,
                "email": email,
                "role": selectedRole
            };
            fetch(`https://pitech.edu.vn:82/api/Email/ForgotPassWord?userName=${username}&email=${email}&roleName=${selectedRole}`, {
                method: 'POST',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(inputObj)
            }).then((res) => {
                if (!res.ok) {
                    return res.json().then((errorData) => {
                        toast.error(errorData.errorMessages[0]);
                    });
                } else {
                    return res.json().then((errorData) => {
                        toast.success(errorData.errorMessages[0]);
                    });
                }
                return res.json();
            })
                .then((data) => {



                }).catch((err) => {

                })

        } if (selectedRole === 'Student') {

            let inputObj = {
                "userName": username,
                "email": email,
                "role": selectedRole
            };
            fetch(`https://pitech.edu.vn:82/api/Email/ForgotPassWord?userName=${username}&email=${email}&roleName=${selectedRole}`, {
                method: 'POST',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(inputObj)
            }).then((res) => {
                if (!res.ok) {
                    return res.json().then((errorData) => {
                        toast.error(errorData.errorMessages[0]);
                    });
                }
                return res.json();
            })
                .then((data) => {



                }).catch((err) => {


                })

        }
    }
    useEffect(() => {
        // Update the remaining time every second
        const intervalId = setInterval(() => {
            setRemainingTime((prevTime) => Math.max(0, prevTime - 1));
        }, 1000);

        // Clear the interval when the component unmounts
        return () => {
            clearInterval(intervalId);

            // Cleanup the timeout when the component unmounts
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
        };
    }, [remainingTime, timeoutId]);
    const formatTime = (seconds) => {
        const minutes = Math.floor(seconds / 60);
        const remainingSeconds = seconds % 60;
        return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
    };

    return (
        <body className='body'>

            <div className='login-switch'>
                <div className='login-switch-role'>
                    <div style={{ margin: '0' }} className='login-header'>
                        <div style={{ display: 'flex' }} className='login-text'>
                            <div className={`role-title ${selectedRole === 'Teacher' ? 'role-selected' : ''}`} onClick={SwitchRoleTeacher}>Giáo viên</div>
                            <div className={`role-title ${selectedRole === 'Student' ? 'role-selected' : ''}`} onClick={SwitchRoleStudent} >Học sinh</div>
                        </div>

                    </div>
                </div>


            </div>

            <div className='login-container'>

                <form onSubmit={ProceedLogin} className='login-form-teacher'>
                    <div className='login-header'>
                        <div className='login-text'>Tìm tài khoản của bạn</div>
                        <div className='login-underline'></div>
                    </div>
                    <div class="forget-title">Vui lòng nhập tên tài khoản và email của bạn.</div>
                    <div className='login-inputs'>
                        <div className='login-input'>
                            <img src={user_icon} alt='' />
                            <input value={username} onChange={(e) => setUsername(e.target.value)} type='text' placeholder='Name' required></input>
                        </div>

                        <div className='login-input'>
                            <img src={password_icon} alt='' />
                            <input value={email} onChange={(e) => setEmail(e.target.value)} type='text' placeholder='Email' required></input>
                        </div>
                      
                    </div>

                    <div style={{ marginTop: '20px' }} className='login-submit-container'>


                        <button onClick={() => navigate("/login")} type='button' className={"login-submit"}>Thoát</button>
                        <button type='submit' style={{ color: `${isSubmitting ? 'gray' : ''}` }} className={"login-submit"}>Lưu</button>
                    </div>

                    <ToastContainer />
                </form>


            </div>
        </body>
    )
}
