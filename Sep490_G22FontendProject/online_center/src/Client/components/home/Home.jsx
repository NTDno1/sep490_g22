import React, { useEffect, useState } from "react"
import { listEmployee } from "../../../api/apiClient//ServiceEmployee"
import Hblog from "./blog/HBlog"
import HCourse from "./Homecourse/HCourse"
import Intro from "./Intro/Intro"
import Event from "./Intro/Event"
import HotNew from "./HotNew"
import Header from "../common/header/Header"
import BackToTop from "../BackToTop/BackToTop"
import Head from "../common/header/Head"
import Footer from "../common/footer/Footer"
import Teacher from "../../../Admin/components/Teacher"
import Cookies from "js-cookie"
import Member from "../../../Admin/scenes/team"
import ChatBot from "../../../ChatBot"
import TeacherClient from "./Teacher/teacher"
import HomeSuperAdmin from "../../../Admin/components/HomeSuperAdmin/HomeSuperAdmin"






const Home = () => {
  const [isAuthenticated, setIssAuthenticated] = useState(Cookies.get('roleName'));
  if (isAuthenticated === undefined) { setIssAuthenticated('Guest'); }

  return (
    <>
      {((isAuthenticated === 'Teacher') || (isAuthenticated === "Student") || (isAuthenticated === "Guest")) ? (
        <>
          <Head />
          <Header />
          <div style={{position:'fixed',top:'50%'}}>
          <ChatBot />
          </div>
          <Intro />
          <HCourse />
          <Event />
          <TeacherClient />
          <Hblog />
          <BackToTop />
          <Footer />
        </>
      ) : (
        isAuthenticated === 'Employee' ? (
          <Teacher />
        ) : (
          isAuthenticated === 'Super Admin' ? (
            <HomeSuperAdmin/>
          ) : (
            null
          ))
      )}
    </>
  )
}

export default Home
