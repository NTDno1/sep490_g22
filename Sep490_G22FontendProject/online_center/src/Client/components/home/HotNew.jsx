import React, { useEffect } from "react"
import "./Homecourse/Hcourse&blog.css"
import { useState } from "react"



import { Link } from "react-router-dom"
import Cookies from "js-cookie"

const HotNew = () => {
  const [hotNew,setHotNew] = useState([]);
  useEffect(()=> {
    let jwttoken = Cookies.get('jwttoken');
  
    fetch("https://pitech.edu.vn:82/api/Blog?page=1&limit=10&cateId=2", {
      headers: {
        'Authorization': 'Bearer ' + jwttoken
      }
    }).then((res) => {
      return res.json();
    }).then((data)=> {
      setHotNew(data.result.results);
    
      
    }).catch((err)=> {
      console.log(err.message);
    })
  },[])
  return (
    <>
    <section className="section course" id="courses" aria-label="course">
  <div className="container">
    <p className="section-subtitle">Khóa học phổ biến</p>
    <h2 className="h2 section-title">Chọn một khóa học để bắt đầu</h2>
    <ul className="grid-list">
    {hotNew.slice(0,3).map((val) => (
      <li>
        <div style={{width:'400px',height:'650px'}} className="course-card">
          <figure className="card-banner img-holder">
            <img
              src={val.img}
            style={{width:'415px',height:'300px'}}
              alt="Build Responsive Real- World Websites with HTML and CSS"
              className="img-cover"
            />
          </figure>
          <div className="card-content">
            <h3 style={{marginTop:'-35px',marginLeft:'auto'}} className="h3">
              <a style={{fontSize:'20px'}} href="#" className="card-title">
             {val.title}
              </a>
            </h3>
            <ul className="card-meta-list">
              <li className="card-meta-item">
                <ion-icon name="library-outline" aria-hidden="true" />
                <span style={{marginLeft:'-20px'}} className="span">{val.shortDescription}</span>
              </li>
            </ul>
          </div>
        </div>
      </li>
    ))}
    </ul>
    <a href="notification" className="btncourse has-before">
      <span className="span">Xem thêm các khóa học</span>
      <ion-icon name="arrow-forward-outline" aria-hidden="true" />
    </a>
  </div>
</section>

    </>
  )
}

export default HotNew
