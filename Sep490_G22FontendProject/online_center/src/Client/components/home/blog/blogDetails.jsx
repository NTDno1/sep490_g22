import React from "react"

// import "blogdetail.css"
import { Link, useParams } from "react-router-dom"
import { useEffect } from "react"
import { useState } from "react"
import "./blogdetail.css"
import Cookies from "js-cookie"
import Head from "../../common/header/Head"
import Footer from "../../common/footer/Footer"
import { ViewBlog, ViewBlogDetail, ViewRelatedBlog } from "../../../../api/apiClient/Banner/BannerAPI"
import NavigationPage from "../../common/NavigationPage/NavigationPage"
import { Box } from "@mui/material"


const BlogDetail = () => {
  const [blog, setBlog] = useState({});
  const [description, setDescription] = useState();
  const { blogCode } = useParams();
  const [anotherBlog, setAnother] = useState([]);
  useEffect(() => {
    fetchDataBlog(blogCode);
    fetchDataBlogRecent(blogCode);
  }, [blogCode]);

  const fetchDataBlog = async (blogCode) => {
    try{
    const data = await ViewBlogDetail(blogCode);
    if (data.result) {
      setBlog(data.result);
    }}catch (error) {
      console.error("Error fetching data:", error.message);
    }

  };

  const fetchDataBlogRecent = async (blogCode) => {
    try{
    const data = await ViewRelatedBlog(blogCode);
    if (data.result.length>0) {
      setAnother(data.result);
    }
  }catch(error){
    console.log(error);
  }
  };
 const handleLinkClick = (blogCode) => {
   
    window.location.href = `/blog/${blogCode}`;
  };
  useEffect(() => {
    window.scrollTo(0, 0); // Cuộn lên đầu trang khi chuyển trang
  }, []);
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Tin tức', url: '/' },   
  ];
  return (
    <>
      <Head />
      <Box m="80px">
<NavigationPage items={breadcrumbItems} />
     
      <div className="page page-home">
        <div className="page-main">
          <div className="container">
            <div className="row">
              <div className="col-main col-lg-8 mb-5">
                <article>
                  <div class="d-flex flex-column text-left mb-3">
                    <p class="section-title2 pr-2">
                      <span class="pr-2">Nội dung chi tiết</span>
                    </p>
                    <h1 className="post-title">
                      {blog.title}
                    </h1>
                    <div class="d-flex">
                      <p class="mr-3"><i class="fa fa-user text-primary"></i> {blog.createdBy}</p>
                      <p class="mr-3"><i class="fa fa-comments text-primary"></i>{blog.createdAt}</p>
                    </div>
                  </div>
                  <div className="post-body">
                    <hr className="divider mt-3" />
                    <div className="post-content mb-4" id="post-content-detail">
                      <div dangerouslySetInnerHTML={{ __html: blog.description }} />
                    </div>
                    <hr />
                  </div>
                  <div className="post-related">
                    <div className="flex-center-y justify-content-between p-2 bg-light border-bottom mb-3">
                      <h4 className="fs-18px mb-0">Có thể bạn quan tâm</h4>
                    </div>
                    <div className="row g-4">
                      {anotherBlog.map((val) => (
                        <div className="col-sm-4" key={val.blogCode}>
                          <article style={{height:'370px'}}  className="card card-post-2">
                            <Link className="thumbnail thumbnail-hover mb-3" onClick={() => handleLinkClick(val.blogCode)}>
                              <img style={{height:'250px',width:'100%'}}
                                className="thumbnail-img"
                                src={val.img}
                                alt={`Thumbnail for ${val.title}`}
                              />
                              <div style={{margin:'10px'}} className="card-body p-0">
                                <h6 className="card-title mb-0">{val.title}</h6>
                              </div>
                            </Link>
                          </article>
                        </div>
                      ))}
                    </div>
                  </div>
                </article>
              </div>
              {/* <div className="col-aside col-lg-4 mb-5">
                <aside className="sidebar">
                  <section className="d-grid gap-3">
                    <a
                      className="lt lt-dark"
                      href="/tin-tuc/Lo-trinh-hoc-Ielts-online-level-5-0-cho-nguoi-moi-bat-dau_mt1462866581.html
  "
                      style={{
                        backgroundImage:
                          "url(https://ielts-fighter.com/images/ex/home/home-s2-lt-1.jpg)"
                      }}
                    >
                      <h6 className="lt-title text-decoration-underline text-uppercase mb-0">
                        Lộ trình tự học 5.0 IELTS{" "}
                      </h6>
                    </a>
                    <a
                      className="lt lt-dark"
                      href="/tin-tuc/Lo-trinh-luyen-thi-IELTS-online-free-level-6-5_mt1464170289.html
  "
                      style={{
                        backgroundImage:
                          "url(https://ielts-fighter.com/images/ex/home/home-s2-lt-2.jpg)"
                      }}
                    >
                      <h6 className="lt-title text-decoration-underline text-uppercase mb-0">
                        Lộ trình tự học 6.0 IELTS{" "}
                      </h6>
                    </a>
                    <a
                      className="lt lt-dark"
                      href="/tin-tuc/Lo-trinh-hoc-IELTS-cho-nguoi-bat-dau-tu-0-len-7-0-IELTS_mt1533596000.html"
                      style={{
                        backgroundImage:
                          "url(https://ielts-fighter.com/images/ex/home/home-s2-lt-3.jpg)"
                      }}
                    >
                      <h6 className="lt-title text-decoration-underline text-uppercase mb-0">
                        Lộ trình tự học 7.0 IELTS{" "}
                      </h6>
                    </a>
                  </section>
                  <section>
                    <h6 className="fs-18px mb-3">Liên kết nhanh</h6>
                    <ul className="list list-bullets list-bullets-chevron mb-0">
                      <li className="list-item">
                        <a
                          href="/ielts-master.html
  "
                        >
                          Khóa học IELTS cam kết đầu ra{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="/lich-khai-giang.html
  "
                        >
                          Lịch khai giảng lớp IELTS{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="https://ieltsonlinetest.ielts-fighter.com/
  "
                        >
                          Thi thử IELTS miễn phí{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="https://khoahoc.ielts-fighter.com/dang-ky-tu-van
  "
                        >
                          Tư vấn khóa học theo yêu cầu{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="/phuong-phap-dao-tao-ripl.html
  "
                        >
                          Phương pháp đào tạo RIPL{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="/tin-tuc/vinh-danh-hoc-vien_c1472178013.html
  "
                        >
                          Học viên điểm cao xuất sắc{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a
                          href="/tin-tuc/Thang-diem-IELTS-Cach-tinh-diem-IELTS-chuan-nhat_mt1509365381.html
  "
                        >
                          Thang điểm IELTS{" "}
                        </a>
                      </li>
                      <li className="list-item">
                        <a href="/tai-lieu.html">Tài liệu IELTS mới nhất </a>
                      </li>
                    </ul>
                  </section>
                </aside>
              </div> */}
            </div>
          </div>
        </div>
      </div>
      </Box>
      <Footer />
    </>

  )
}

export default BlogDetail
