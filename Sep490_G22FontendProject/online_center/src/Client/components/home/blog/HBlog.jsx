import React, { useEffect, useState } from "react"
// import { blog } from "../../dummydata"
import { Link } from "react-router-dom";
import { getBlogAPI } from "../../../../api/apiClient/Banner/BannerAPI";
import "./HBlog_css.css"
import AOS from 'aos';
import 'aos/dist/aos.css';
// copy code of blog => blogCard
const HBlog = () => {
  const [blogs, setBlogs] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const data = await getBlogAPI();
      if (data.result.length>0) {
        setBlogs(data.result);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  };

  return (
    <>
      <section
        className="section blog has-bg-image"
        id="blog"
        aria-label="blog"
       
        data-aos="fade-up"
      >
        <div class="latest-news pt-150 pb-150">
          <div class="container">

            <div class="row">
              <div class="col-lg-8 offset-lg-2 text-center">
                <div class="section-title">
                  <h3>
                    <span class="orange-text">Tin</span> Mới
                  </h3>
                </div>
              </div>
            </div>

            <div class="row">
              {blogs.slice(0, 3).map((val) => (
                <div class="col-lg-4 col-md-6">
                  <div style={{ height:'600px' }} class="single-latest-news">
                    <a href={`/blog/${val.blogCode}`}>
                      <img style={{ width: '400&Ypx', height: '300px' }} src={val.img} alt='' />
                      <div class="latest-news-bg news-bg-1"></div></a>
                    <div class="news-text-box">
                      <Link style={{ textDecoration: 'none' }} to={`blog/${val.blogCode}`}> <a>{val.title}</a> </Link>
                      <p class="blog-meta">
                        <span class="author"><i class="fas fa-user"></i> {val.createdBy}</span>
                        <span class="date"><i class="fas fa-calendar"></i> {val.createdAt}</span>
                      </p>
                      <p  class="excerpt">{val.shortDescription}</p>
                      <Link className="read-more-link" style={{ textDecoration: 'none' }} to={`blog/${val.blogCode}`}>Đọc thêm <i class="fas fa-angle-right"></i></Link>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div class="row">
              <div class="col-lg-12 text-center">
                <a href="/notification" class="boxed-btn">Xem tất cả</a>
              </div>
            </div>
          </div>
        </div>

      </section>

    </>
  )
}

export default HBlog
