import React from "react"

// import "blogdetail.css"
import { Link, useParams } from "react-router-dom"
import { useEffect } from "react"
import { useState } from "react"
import Sidebarcourse from "../Homecourse/Sidebarcourse"
import "./coursedetail.css"
import Head from "../../common/header/Head"
import Footer from "../../common/footer/Footer"
import { GetCourseDetail } from "../../../../api/apiClient/Courses/CoursesAPI"
import NavigationPage from "../../common/NavigationPage/NavigationPage"
import { Box } from "@mui/material"
import { formatNumberWithDecimalSeparator } from "../../../../constants/constant";
const CourseDetail = () => {
  const [course, setCourse] = useState({});
  const { id } = useParams();

  useEffect(() => {
    fetchCourseDetails(id);
  }, [id]);

  const fetchCourseDetails = async (id) => {
    try {
      const data = await GetCourseDetail(id);
      if (data) {
        setCourse(data);
      } else { }
    } catch (error) { }
  };  useEffect(() => {
    window.scrollTo(0, 0); // Cuộn lên đầu trang khi chuyển trang
  }, []);
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Khóa học', url: '/' },
  ];
  return (
    <>
      <Head />
      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />

        <div className="site-section">
          <div className="container">
            <div className="row">
              <div className="col-md-6 mb-4">
                <p className="cover-fit-image-container">
                  <img src={course.imageSrc} alt="Image" className="cover-fit-image" />
                </p>
              </div>
              <div className="col-lg-6 ml-auto align-self-center" style={{ paddingLeft: '100px' }}>
                <h2 className="section-title-underline mb-5">
                  <span>Chi tiết khóa học</span>
                </h2>
                <p className="custom-p" >
                  <strong className="text-black d-block">Tên khóa học:</strong> {course.name}
                </p>
                <p className="mb-5 custom-p" >
                  <strong className="text-black d-block">Giá khóa học:</strong> <span>{formatNumberWithDecimalSeparator(course.price)} VNĐ</span>
                </p>
                <p className="custom-p"><strong className="text-black d-block">Thông tin miêu tả:</strong> {course.description}</p>
                {/* <p>
                  <a href="#" className="btn btn-primary rounded-0 btn-lg px-5">
                    Đọc thêm
                  </a>
                </p> */}
              </div>
            </div>
          </div>
        </div>
      </Box>
      <Footer />
    </>
  )
}

export default CourseDetail
