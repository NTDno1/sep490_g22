import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getAllViewCourse } from "../../../../api/apiClient/Courses/CoursesAPI";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import "./Hcourse&blog.css";
import "./course_css.css";
import AOS from 'aos';
import 'aos/dist/aos.css';
import { formatNumberWithDecimalSeparator } from "../../../../constants/constant";
const HCourse = () => {
  const [course, setCourse] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const data = await getAllViewCourse();

      if (data.result.length > 0) {
        setCourse(data.result);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  
  };

  const CustomPrevArrow = (props) => (
    <div
      {...props}
      className={`custom-prev-arrow `}
      style={{
        left: "-50px",
      }}
    >
      <i className="fas fa-chevron-left"></i>
    </div>
  );

  const CustomNextArrow = (props) => (
    <div
      {...props}
      className={`custom-next-arrow `}
      style={{
        right: "-50px",
      }}
    >
      <i className="fas fa-chevron-right"></i>
    </div>
  );

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    prevArrow: <CustomPrevArrow />,
    nextArrow: <CustomNextArrow />,
  };

  return (
    <section className="our-courses" id="courses" aria-label="course">
      <div className="container" data-aos="fade-left">
        <div className="row">
          <div className="col-lg-12">
            <h2 className="section-heading">KHÓA HỌC PHỐ BIẾN</h2>
          </div>
          <div className="col-lg-14">
            <div class="owl-courses-item owl-carousel">
              <Slider {...settings}>
                {course.slice(0, 6).map((course, index) => (
                  <div class="item" key={index}>
                    <a href={`/course/${course.id}`}>
                      <img style={{ height: '259px' }} src={course.imageSrc} />
                    </a>
                    <div class="down-content">
                      <h4><Link to={`/course/${course.id}`} className="custom-link">
                        {course.name}
                      </Link></h4>
                      <div class="info">
                        <div class="row">
                          <div class="col-7">
                            <ul>
                              {Array.from({ length: course.rating }, (_, i) => (
                                <li key={i}>
                                  <i className="fa fa-star"></i>
                                </li>
                              ))}
                            </ul>
                          </div>
                          <div class="col-5">
                          <span>{formatNumberWithDecimalSeparator(course.price)} VNĐ</span>
  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
export default HCourse;
