import React, { useEffect, useState } from 'react';
import 'react-bootstrap/dist/react-bootstrap'; // Import Bootstrap components
import AOS from 'aos';
import 'aos/dist/aos.css';
import { Row, Col,Button  } from 'react-bootstrap'; // Import Bootstrap grid system components
import "./teacher.css";
import { GetTeacherAdminSide } from '../../../../api/apiClient/Teacher/teacherAPI';
import { convertDateFormat } from '../../../../constants/constant';
import { ToastContainer, toast } from 'react-toastify';

export default function TeacherClient() {

  const [teacher, setTeacher] = useState([]);
  useEffect(() => {
    ListTeacherAdminSide();
    AOS.init({
      duration: 1000,
    });
  }, []);
  const ListTeacherAdminSide = async () => {
    try{
    const data = await GetTeacherAdminSide();
    data.result = data.result.map((item) => {
      if (item.lastName === null) { item.lastName = "" }
      item.dob = convertDateFormat(item.dob);
      item.createDate = convertDateFormat(item.createDate);
      return item;
    });
    setTeacher(data.result)
  }catch(error){
    toast.error("Không tìm thấy dữ liệu")
  }
  }
  return (
        <section className="section about" id="about" aria-label="about" data-aos="fade-up" style={{marginBottom:'-100px'}}>
            <ToastContainer/>
          <div className="container-xxl py-5">
            <div className="custom-container">
              <div className="text-center wow fadeInUp" >
                <h6 className="section-title-3 bg-white text-center text-primary px-3">
                  Giảng viên
                </h6>
                <h1 className="mb-5"> Giảng viên nổi bật</h1>
              </div>
              
              <div className="row g-4">
                  {teacher.slice(0,4).map((value, index) => (
                  <div key={index} className={`col-lg-3 col-md-6 wow fadeInUp`} >
                    <div className="team-item bg-light">
                      <div className="overflow-hidden">
                        <img className="img-fluid" src={value.image} alt="" />
                      </div>
                      <div className="text-center p-4">
                      <h5 className="mb-0">{value.firstName} {value.midName} {value.lastName}</h5>
                        <small>{value.certificateName}</small>
                      </div>
                    </div>
                  </div>
                  ))}
               
              </div>
            
            </div>
          </div>
        </section>
      );
    };
