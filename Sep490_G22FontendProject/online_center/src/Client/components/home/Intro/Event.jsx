import React, { useEffect, useState } from 'react'
import "./event.css"
import AOS from 'aos';
import 'aos/dist/aos.css';
import { getAllCourseHome } from '../../../../api/apiClient/Courses/CoursesAPI';
export default function Event() {
  const [banner3, setBanner3] = useState([]);
  useEffect(() => {
    getCourseHome();
  }, [])
  
  const getCourseHome = async () => {
    try{
    const data = await getAllCourseHome();
    if(data.result){
    setBanner3(data.result);
    }}catch (error) {
      console.error("Error fetching data:", error.message);
    }
  };

  useEffect(() => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
    });
  }, []);
  return (
    <>
      <section
      style={{height:'70%'}}
        className="section hero has-bg-image"
        id="home"
        aria-label="home"
        data-aos="fade-right"
      >
        <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'column' }} className="container">
          <div>
          <div className="col-lg-12">
            <h2 className="section-heading">KHÓA HỌC ĐƯỢC TIN DÙNG</h2>
          </div>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row-reverse' }} className='LL'>
            <div className="hero-content" style={{ width: '50%', float: 'left', paddingLeft:30, textAlign: 'center' }}>
              <h1 style={{ fontSize: '20px', color: "#2746C0", justifyContent:'center', }} className="h1 section-title">
                {banner3.name}
              </h1>
              <p className="hero-text cus-des">
                {banner3.description}
              </p>
              <a href={`course/${banner3.id}`} className="btn has-before">
                <span className="span">Tìm khóa học</span>
                <ion-icon name="arrow-forward-outline" aria-hidden="true" />
              </a>
            </div>
            {/* <figure className="hero-banner" style={{ overflow: 'hidden', borderRadius: '10px' }}> */}
            <div className="img-holder one" style={{ width: '50%', float: 'left', backgroundColor:'transparent',paddingRight:30}}>
              <img
                src={banner3.imageSrc}
                width={270}
                height={300}
                alt="hero banner"
                className="img-cover"
                style={{ borderRadius: '100px 100px 100px 100px'}}
              />
            </div>
            {/* <div style={{ width: '50%', float: 'left' }}></div>
           </figure> */}
          </div>
        </div>
      </section>
    </>
  )
}
