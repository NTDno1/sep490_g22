import React, { useEffect, useState } from 'react';
import 'react-bootstrap/dist/react-bootstrap'; // Import Bootstrap components
import "./intro.css";
import { getBanner4API, getInformationApi } from '../../../../api/apiClient/Banner/BannerAPI';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { Row, Col, Button } from 'react-bootstrap'; // Import Bootstrap grid system components


export default function Intro() {
  const [banner2, setBanner2] = useState({});
  const [infor, setInfo] = useState([]);
  useEffect(() => {
    fetchData();
    getInfoApi();
  }, [])

  const fetchData = async () => {
    try {
      const data = await getBanner4API();
      if (data) {
        setBanner2(data.result.results[0]);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  };
  const getInfoApi = async () => {
    try {
      const data2 = await getInformationApi();
      if (data2) {
        setInfo(data2.result.results);
      }
    } catch (error) {
      console.error("Error fetching data:", error.message);
    }
  }
  useEffect(() => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
    });
  }, []);
  return (
    <section className="section about" id="about" aria-label="about" data-aos="fade-left">
      <div className="custom-container">
        <div className="title-show">
          <h1 >Tại sao chọn chúng tôi?</h1>
        </div>

        {/* New sections */}
        <Row xs={1} md={2} lg={4} className="g-4">
          {/* Skilled Instructors */}
          {infor.slice(0, 4).map((infor, index) => (
            <Col data-aos="fade-up" data-wow-delay="0.1s">
              <div class="service-item text-center pt-3" >
                <div class="p-4">
                  <div class="rounded-image-container">
                    <img src={infor.img} alt="Skilled Instructors" class="service-icon" />
                  </div>
                  <h5 class="mb-3">{infor.title}</h5>
                  <div className='cus-title'><p>{infor.shortDescription}</p></div>
                </div>
              </div>
            </Col>
          ))}
        </Row>

        <div className="container-xxl py-5" data-aos="fade-up" style={{ backgroundImage: 'https://drive.google.com/uc?export=view&id=1vwKHkJD33UCr9hylqHEFxbambCTzhk-7' }}>
          <Row className="g-5">
            <Col lg={6} data-aos="fade-up" data-wow-delay="0.1s" style={{ minHeight: '400px' }}>
              <div className="position-relative h-100">
                <img
                  className="img-fluid position-absolute w-100 h-100"
                  src={banner2.img}
                  alt=""
                  style={{ objectFit: 'cover' }}
                />
              </div>
            </Col>
            <Col lg={6} data-aos="fade-up" data-aos-delay="300">
              <h6 className="section-title1 bg-white text-start text-primary pe-3">{banner2.title}</h6>
              {/* <h1 className="mb-4">{banner2.title}</h1> */}
              <p className="mb-4 cus-title" style={{textAlign:'justify'}}>
                {banner2.shortDescription}
              </p>
              <Row className="gy-2 gx-4 mb-4">
                {infor.slice(0, 4).map((infor, index) => (
                  <Col sm={6}>
                    <p className="mb-0">
                      <i className="fa fa-arrow-right text-primary me-2"></i>{infor.title}
                    </p>
                  </Col>
                ))}
              </Row>
              <Button variant="primary" className="py-3 px-5 mt-2" href="/aboutus">
                Đọc thêm
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    </section>

  )
}
