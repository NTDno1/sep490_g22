import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom";
import "../modal.css";
import "./AllCourse.css"
import AOS from 'aos';
import 'aos/dist/aos.css';
import { getAllViewCourse, ViewCourseFillter } from "../../../api/apiClient/Courses/CoursesAPI";
import Head from "../common/header/Head";
import Header from "../common/header/Header";
import Footer from "../common/footer/Footer";
import { ListCenterByClient } from "../../../api/apiClient/Center/CenterAPI";
import { formatNumberWithDecimalSeparator } from "../../../constants/constant";
const AllTeacher = () => {
    const [modals, setmodals] = useState(false);
    const [page, setPage] = useState(1);
    const [id, setId] = useState(1);
    const [center, setCenter] = useState([]);
    const [selectedCategoryId, setSelectedCategoryId] = useState("");
    const togglemodals = () => {
        setmodals(!modals);
    };
    if (modals) {
        document.body.classList.add('active-modals')
    } else {
        document.body.classList.remove('active-modals')
    }
    //   const [blog, setBlog] = useState([]);

    //   useEffect(() => {
    //     const fetchCourseDetails = async () => {
    //       try {
    //         const data = await ViewNotificationCard(id);

    //         if (data.result.results.length > 0) {
    //           setBlog(data.result.results);
    //           setPage(data.result.totalPages);
    //         }
    //       } catch (error) {
    //         console.error("Error fetching course details:", error);
    //       }
    //     };

    const [course, setCourse] = useState([]);
    useEffect(() => {
        fetchData();
    }, [id,selectedCategoryId]);

    const fetchData = async () => {
        try {
            const data = await ViewCourseFillter(id,selectedCategoryId);
            if (data.result.results.length > 0) {
                setCourse(data.result.results);
                setPage(data.result.totalPages);
            }
        } catch (error) {
            console.error("Error fetching data:", error.message);
            setCourse([]);
        }
    }
    const handleCategoryChange = (e) => {
        const selectedCategoryId = e.target.value;
        setSelectedCategoryId(selectedCategoryId);
        handleLinkClick(1);
      };
    useEffect(() => {
        fetchDataCenter();
    }, []);

    const fetchDataCenter = async () => {
        try {
            const data = await ListCenterByClient();
            if (data.result.length > 0) {
                setCenter(data.result);
                setSelectedCategoryId(data.result[0].id);
            }
        } catch (error) {
            console.error("Error fetching data:", error.message);
        }
    }
    
    const handleLinkClick = (newId) => {
        setId(newId);
    };
    useEffect(() => {
        window.scrollTo(0, 0); // Cuộn lên đầu trang khi chuyển trang
      }, [id]);
    const links = [];
    for (let i = 1; i <= page; i++) {
        links.push(
            <span
                className={i === id ? "active" : ""}
                key={i}
                onClick={() => handleLinkClick(i)}
            >
                {i}
            </span>
        );
    }
    const breadcrumbItems = [
        { title: 'Trang chủ', url: '/' },
        { title: 'Khóa học', url: '/' },
    ];
    return (
        <>
            <Head />
            <Header />
            <section className='course padding '>
                <p style={{ fontWeight: 700, fontSize: 40, marginBottom: 30, textAlign: "center" }}>DANH SÁCH KHÓA HỌC</p>
                <div style={{ textAlign: "center", marginBottom: "20px", marginRight: "20px" }}>
                    <label htmlFor="categorySelect" style={{ marginRight: "20px" }}>Chọn cơ sở:</label>
                    <div class="custom-select">
                        <select id="categorySelect" value={selectedCategoryId}
                        onChange={handleCategoryChange} className="categorySelect">
                        {center.map((category) => (
                          <option key={category.id} value={category.id}>
                            {category.name}
                          </option>
                        ))}
                        </select>
                    </div>
                </div>
                <div className='container'>
                    <div className="grid2" >
                    {course.length > 0 ?(course.slice(0, 6).map((course, index) => (
                            <div class="item" key={index}>
                                <div className="fix">
                                    <div className="cus-a">
                                        <a href={`/course/${course.id}`}>
                                            <img style={{ height: '259px' }} src={course.imageSrc} />
                                        </a>
                                    </div>
                                    <div class="down-content">
                                        <h4><Link to={`/course/${course.id}`} className="custom-link">
                                            {course.name}
                                        </Link></h4>
                                        <div class="info">
                                            <div class="row">
                                                <div class="col-7">
                                                    <ul>
                                                        {Array.from({ length: course.rating }, (_, i) => (
                                                            <li key={i}>
                                                                <i className="fa fa-star"></i>
                                                            </li>
                                                        ))}
                                                    </ul>
                                                </div>
                                                <div class="col-5">
                                                <span>{formatNumberWithDecimalSeparator(course.price)} VNĐ</span>
                                                </div>
                                            </div>
                                            <button class="view-details-button">
                                            <Link to={`/course/${course.id}`} className="link-style">Xem chi tiết</Link></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))): <p>Không có dữ liệu</p>}
                    </div>
                    <br />

                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '20px' }} className="pagination">
                        {links}
                    </div>
                </div>

            </section>
            <Footer />
        </>
    )
}

export default AllTeacher
