import React, { useState, useEffect } from 'react'

import axios from "axios";
const defaultImageSrc = '/img/image_placeholder.png'

const initialFieldValues = {
    name: '',
    imageName: '',
    imageSrc: defaultImageSrc,
    imageFile: null
}

export default function CourseList() {
    const [employeeList, setEmployeeList] = useState([])
    const [recordForEdit, setRecordForEdit] = useState(null)
    const [values, setValues] = useState(initialFieldValues)
    const [errors, setErrors] = useState({})
    useEffect(() => {
        refreshEmployeeList();
    }, [])
    useEffect(() => {
        if (recordForEdit != null)
            setValues(recordForEdit);
    }, [recordForEdit])
    const employeeAPI = (url = 'https://localhost:7241/api/Course/ConvertImageToBase64') => {
        return {
            fetchAll: () => axios.get('https://localhost:7241/api/Course/ListCourse'),
            create: newRecord => axios.post(url, newRecord),
            update: (id, updatedRecord) => axios.put(url + id, updatedRecord),
            delete: id => axios.delete(url + id)
        }
    }
    const handleInputChange = e => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }
    const showPreview = e => {
        if (e.target.files && e.target.files[0]) {
            let imageFile = e.target.files[0];
            const reader = new FileReader();
            reader.onload = x => {
                setValues({
                    ...values,
                    imageFile,
                    imageSrc: x.target.result
                })
            }
            reader.readAsDataURL(imageFile)
        }
        else {
            setValues({
                ...values,
                imageFile: null,
                imageSrc: defaultImageSrc
            })
        }
    }

    const validate = () => {
        let temp = {}
        temp.employeeName = values.employeeName == "" ? false : true;
        temp.imageSrc = values.imageSrc == defaultImageSrc ? false : true;
        setErrors(temp)
        return Object.values(temp).every(x => x == true)
    }

    const resetForm = () => {
        setValues(initialFieldValues)
        document.getElementById('image-uploader').value = null;
        setErrors({})
    }

    const handleFormSubmit = e => {
      
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('name', values.name)
            formData.append('imageName', values.imageName)
            formData.append('imageFile', values.imageFile)
            addOrEdit(formData, resetForm)
        }
    }
    function refreshEmployeeList() {
        employeeAPI().fetchAll()
            .then(res => {
                setEmployeeList(res.data.result)
                // console.log(res.data.result)
            })
            .catch(err => console.log(err))
    }
    

    const addOrEdit = (formData, onSuccess) => {
        if (formData.get('name') !== "")
            employeeAPI().create(formData)
                .then(res => {
                    const imageName = formData.get('imageName');
                    const name = formData.get('name');
                    onSuccess();
                    console.log(name,imageName);
                    refreshEmployeeList();

                  
                   
                    
                    // Sử dụng các giá trị này theo nhu cầu của bạn
                   
                })
             
                .catch(err => console.log(err))
        else
            employeeAPI().update(formData.get('employeeID'), formData)
                .then(res => {
                    onSuccess();
                    refreshEmployeeList();
                })
                .catch(err => console.log(err))

    }
    const showRecordDetails = data => {
        console.log(data)
        setRecordForEdit(data)
    }

    const onDelete = (e, id) => {
        e.stopPropagation();
        if (window.confirm('Are you sure to delete this record?'))
            employeeAPI().delete(id)
                .then(res => refreshEmployeeList())
                .catch(err => console.log(err))
    }

    const imageCard = data => (
        <div className="card" >
            <img onClick={() => { showRecordDetails(data) }} style={{width:'100px',height:'100px',marginRight:'300px',marginLeft:'300px'}} src={data.imageSrc} className="card-img-top rounded-circle" />
            <div className="card-body">
                <h5>{data.employeeName}</h5>
                <span>{data.occupation}</span> <br />
                {/* <button className="btn btn-light delete-button" onClick={e => onDelete(e, parseInt(data.employeeID))}>
                    <i className="far fa-trash-alt"></i>
                </button> */}
            </div>
        </div>
    )


    return (
        <div className="row">
            <div >
                <div className="jumbotron jumbotron-fluid py-4">
                    <div className="container text-center">
                        <h1 className="display-4">Employee Register</h1>
                    </div>
                </div>
            </div>
            <div >
            <div className="container text-center">
                <p className="lead">An Employee</p>
            </div>
            <form autoComplete="off" noValidate onSubmit={handleFormSubmit}>
                <div  className="card">
                    <img style={{width:'50px',height:'50px'}} src={values.imageSrc} className="card-img-top" />
                    <div className="card-body">
                        <div className="form-group">
                            <input type="file" accept="image/*" className={"form-control-file"}
                                onChange={showPreview} id="image-uploader" />
                        </div>
                        <div className="form-group">
                            <input value={values.name} className={"form-control"} placeholder="Employee Name" name="name"                            
                                onChange={handleInputChange} />
                        </div>
                       
                        <div className="form-group text-center">
                            <button type="submit" className="btn btn-light">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
             <div >
                <table>
                    <tbody>
                        {
                            //tr > 3 td
                            [...Array(Math.ceil(employeeList?.length / 3))].map((e, i) =>
                                <tr key={i}>
                                    <td >{imageCard(employeeList[3 * i])}</td>
                                    <td >{employeeList[3 * i + 1] ? imageCard(employeeList[3 * i + 1]) : null}</td>
                                    <td >{employeeList[3 * i + 2] ? imageCard(employeeList[3 * i + 2]) : null}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            
        </div>
    )
}
