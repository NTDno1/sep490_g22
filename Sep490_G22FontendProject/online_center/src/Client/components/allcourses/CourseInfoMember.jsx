import { Box, useTheme } from "@mui/material";
import { DataGrid, useGridApiRef } from "@mui/x-data-grid";
import { tokens } from "../../../Admin/theme";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useEffect, useState } from "react";
import NavigationPage from "../common/NavigationPage/NavigationPage";
import Cookies from "js-cookie";
import { convertDateFormat } from "../../../constants/constant";
import Head from "../common/header/Head";
import { StudentofClass } from "../../../api/apiClient/Class/ClassAPI";
import NotificationsIcon from '@mui/icons-material/Notifications';
const CourseInfoMember = () => {
  const { id } = useParams();
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [className, setClassName] = useState('');
  const [classinfoMember, setClassInfoMember] = useState([]);
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Lớp học', url: '/courses' },
    { title: className, url: '' },
    { title: 'Học viên', url: '' },
  ];

  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  const apiRef = useGridApiRef();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
   
    getStudentofClass();
    apiRef.current.setPageSize(7);
  }, [apiRef]);

  const getStudentofClass = async () => {
    const data = await StudentofClass();
    console.log(data)
    if (data.length > 0) {
      const convertedCourse = data.data.result.map((item) => {

        item.startDate = convertDateFormat(item.startDate);

        return item;

      });

      setClassInfoMember(convertedCourse);

    }
  }
  const columns = [
    { field: "MemberID", headerName: "STT",align:'center' },
    {
      field: "studentName",
      headerName: "Học sinh",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "startDate",
      headerName: "Ngày bắt đầu",
      align:'center',
      flex: 0.5,
    },
    {
      field: "endDate",
      headerName: "Ngày kết thúc",
      align:'center',
      flex: 0.5,
    },
    {
      field: "note",
      headerName: "Ghi chú",
      flex: 1,
    },
  ];
  return (
    <>
      <Head />

      <Box m="80px">
        <NavigationPage items={breadcrumbItems} />
        <div className="container">

          <div className="row">

            <div className="col-12">

              <div className="col-12 my-5">

                <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                  <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
                  <div style={{ flex: '1' }}></div>

                  <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                    <div class="icon-container">
                      <Link className='icon-class' to={`/../courses/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                    </div>
                    <div class="icon-container">
                      <Link className='icon-class' to={`/../courses/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                    </div>
                    <div class="icon-container">
                      <Link className='icon-class' to={`/../courses/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Giáo trình</span></Link>
                    </div>
                    <div class="icon-container">
                      <Link className='icon-class' ><FontAwesomeIcon className='iconcourse-active' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                    </div>
                    <div class="icon-container">
                      <Link className='icon-class' to={`/../courses/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                    </div>
                    <div class="icon-container">
                      <Link className='icon-class' to={`/../courses/notification/${id}`}>    <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                    </div>
                  </div>
                </div>
                <hr />
              </div>
              {/* Form START */}

              <Box
                m="40px 0 0 0"
                height="75vh"
                sx={{
                  "& .MuiDataGrid-root": {
                    border: "none",
                  },
                  "& .MuiDataGrid-columnHeaderTitle":{
                    color:"white"
                  },
                  "& .MuiDataGrid-cell": {
                    borderBottom: "none",
                  },
                  "& .name-column--cell": {
                    color: colors.greenAccent[300],
                  },
                  "& .MuiDataGrid-columnHeaders": {
                    backgroundColor: colors.blueAccent[700],
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-virtualScroller": {
                    backgroundColor: colors.primary[400],
                  },
                  "& .MuiDataGrid-footerContainer": {
                    borderTop: "none",
                    backgroundColor: colors.blueAccent[700],
                  },
                  "& .MuiCheckbox-root": {
                    color: `${colors.greenAccent[200]} !important`,
                  },
                }}
              >
                {classinfoMember.length === 0 ? (
                  <div style={{ textAlign: 'center', padding: '20px', color: '#333' }}>
                    Không có dữ liệu
                  </div>
                ) : (
                  <DataGrid pageSizeOptions={[7, 20, 25]} apiRef={apiRef} checkboxSelection rows={classinfoMember} columns={columns} />

                )}

              </Box>


              {/* Form END */}
            </div>
          </div>
        </div>
      </Box>
    </>
  );
};

export default CourseInfoMember;
