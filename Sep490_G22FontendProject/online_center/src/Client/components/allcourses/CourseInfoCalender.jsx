import React, { useEffect, useState } from 'react'
import "./courses.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import NotificationsIcon from '@mui/icons-material/Notifications';
import fetchData from '../../../api/apiUtils'
import NavigationPage from '../common/NavigationPage/NavigationPage'
import Cookies from 'js-cookie'
import Head from '../common/header/Head'

import { ClassDetail } from '../../../api/apiClient/Class/ClassAPI'
import { Box } from '@mui/material'
export default function CourseInfoCalender() {
    const { id } = useParams();
    const [classinfoClendar, setClassInfoCalendar] = useState([]);
    const [teacher, setTeaher] = useState('');
    const [className, setClassName] = useState('');
    const breadcrumbItems = [
        { title: 'Home', url: '/' },
        { title: 'Classes', url: '/courses' },
        { title: className, url: '' },
        { title: 'Calendar', url: '' },
    ];

    const jwttoken = Cookies.get('jwttoken');
    const navigate = useNavigate();
    useEffect(() => {
        const checkTokenBeforeRoute = () => {
            if (!jwttoken) {

                navigate('/login');
            }
        };

        checkTokenBeforeRoute();
    }, [jwttoken]);
    useEffect(() => {
        getInfoClass();
    }, [id]);
    const getInfoClass = async () => {
        try {
            const data = await ClassDetail(id);
            data.result.startDate = data.result.startDate.toString().split('T')[0];
            setClassInfoCalendar(data.result.classSlotDates);
            setClassName(data.result.name);
            setTeaher(data.result.teacherName)
        } catch (error) {

        }
    }
    return (
        <>
            <Head />

            <Box m="80px">
                <NavigationPage items={breadcrumbItems} />
                <div className="container">
                    <div className="row">

                        <div className="col-12">
                            {/* Page title */}
                            <div className="col-12 my-5">


                                <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                                    <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
                                    <div style={{ flex: '1' }}></div>

                                    <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                                        <div class="icon-container">
                                            <Link className='icon-class' to={`/../courses/${id}`}><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                                        </div>
                                        <div class="icon-container">
                                            <Link className='icon-class'><i class=" iconcourse-active fa fa-calendar"></i>  <span class="hover-text">Lịch học</span> </Link>
                                        </div>
                                        <div class="icon-container">
                                            <Link className='icon-class' to={`/../courses/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Khung chương trình</span></Link>
                                        </div>
                                        <div class="icon-container">
                                            <Link className='icon-class' to={`/../courses/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                                        </div>
                                        <div class="icon-container">
                                            <Link className='icon-class' to={`/../courses/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                                        </div>
                                        <div class="icon-container">
                                            <Link className='icon-class' to={`/../courses/notification/${id}`}>     <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                                        </div>
                                    </div>
                                </div>
                                <hr />

                                {/* Form START */}
                                <table id="keywords" cellspacing="0" cellpadding="0">
                                    <thead >
                                        <tr>
                                            <th><span>ID</span></th>
                                            <th><span>Ca học</span></th>
                                            <th><span>Phòng học</span></th>
                                            <th><span>Giáo viên</span></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {classinfoClendar?.map((val, index) => (
                                            <tr>
                                                <td> <div>{index + 1}</div></td>
                                                <td> <span

                                                >
                                                    {val.timeStart} - {val.timeEnd} {val.day}
                                                </span></td>
                                                <td>{val.roomName}</td>
                                                <td>     {teacher}</td>

                                            </tr>
                                        ))}
                                    </tbody>
                                </table>

                            </div>

                            {/* Form END */}
                        </div>
                    </div>
                </div>
            </Box>
        </>

    )
}
