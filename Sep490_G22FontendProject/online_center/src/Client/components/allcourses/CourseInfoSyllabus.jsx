
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useEffect, useState } from "react";
import Cookies from 'js-cookie'
import NavigationPage from '../common/NavigationPage/NavigationPage'
import NotificationsIcon from '@mui/icons-material/Notifications';
import Head from '../common/header/Head'

import { viewSyllabus } from '../../../api/apiClient/Courses/CoursesAPI'
import { Box } from '@mui/material'
const CourseInfoSyllabus = () => {
  const [classinfoSyllabus, setClassInfoSyllabus] = useState({});
  const { id } = useParams(); 
  const [className, setClassName] = useState('');
  function splitAndRenderDescription(description) {
    return description.split('_').map((item, index) => {
      return <div style={{ textAlign: 'left' }} key={index}>{item.trim()}</div>;
    });
  }

  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  useEffect(() => {
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {

        navigate('/login');
      }
    };

    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
    getSyllabus();
  }, [id]);
  const getSyllabus = async () => {
    try{
    const data = await viewSyllabus(id);
      console.log(data.result)
      if (data.result.description !== null && data.result.studentTasks !== null && data.result.exam !== null) {
        data.result.description = splitAndRenderDescription(data.result.description);
        data.result.studentTasks = splitAndRenderDescription(data.result.studentTasks);
        data.result.exam = splitAndRenderDescription(data.result.exam);
        setClassInfoSyllabus(data.result);
        setClassName(data.result.className);
      }
    }catch(error){}
  }
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Lớp học', url: '/courses' },
    { title: className, url: '' },
    { title: 'Giáo trình', url: '' },
  ];
  return (
    <>
      <Head />
      
<Box m="80px">
        <NavigationPage items={breadcrumbItems} />
      <div className="container">

        <div className="row">

          <div className="col-12">

            {/* Page title */}
            <div className="col-12 my-5">
             

              <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
                  <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

                <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                  <div class="icon-container">
                    <Link to={`/../courses/${id}`} className='icon-class' href='#'><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../courses/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class'  ><FontAwesomeIcon className='iconcourse-active' icon={faFileLines} /><span class="hover-text">Giáo trình</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../courses/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../courses/schedule/${id}`}><i class="iconcourse fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                  </div>
                  <div class="icon-container">
                    <Link className='icon-class' to={`/../courses/notification/${id}`}>     <i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                  </div>
                </div>
              </div>
              <hr />

              {/* Form START */}

              <h1 style={{ margin: '3%', marginLeft: '2%' }}>Syllabus Details</h1>
              <table style={{ border: "1px solid #ccc" }}>
                <tbody>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right">
                      Syllabus ID
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSyllabusID">{classinfoSyllabus.id}</span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style3" align="right" >
                      Tên bài học
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style4">
                      <span id="lblSyllabusName" style={{ fontWeight: "bold" }}>
                        {classinfoSyllabus.syllabusName}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style3" align="right" >
                      Nội dung bài học
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style4">
                      <span id="lblSyllabusNameEnglish">
                        {(classinfoSyllabus.description)}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Nhiệm vụ
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {(classinfoSyllabus.studentTasks)}
                      </span>
                    </td>
                  </tr>

                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Bài thi
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {classinfoSyllabus.exam}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Chủ đề chính
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span style={{float:'left'}} id="lblSubjectCode" >
                      {classinfoSyllabus.mainTopics}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Kiến thức cần đạt
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {classinfoSyllabus.requiredKnowledge}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Phân bổ thời gian
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {classinfoSyllabus.timeAllocation}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Liên hệ
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {(classinfoSyllabus.contact)}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Kết quả khóa học
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {classinfoSyllabus.courseLearningOutcome}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ width: '15%', border: '1px solid #ddd' }} classname="auto-style2" align="right" >
                      Mục tiêu khóa học
                    </td>
                    <td style={{ border: '1px solid #ddd' }} classname="auto-style6">
                      <span id="lblSubjectCode" >
                        {classinfoSyllabus.courseObjectives}
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            {/* Form END */}
          </div>
        </div>
      </div>
  </Box>
    </>

  );
};

export default CourseInfoSyllabus;
