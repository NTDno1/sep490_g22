import React, { useEffect, useState } from "react"
import "./courses.css"
import { Link, useNavigate } from "react-router-dom";
import { getAllClassTeacherSide, getAllClassStudentSide } from "../../../api/apiClient/Class/ClassAPI";
import Cookies from "js-cookie";

import NavigationPage from "../common/NavigationPage/NavigationPage";
import { convertDateFormat } from "../../../constants/constant";
import { getAllViewCourse } from "../../../api/apiClient/Courses/CoursesAPI";
import { Box } from "@mui/material";


const CoursesCard = () => {
  const [originalClasses, setOriginalClasses] = useState([]);
  const [classes, setClases] = useState([]);
  const rolename = Cookies.get('roleName');
  const [course, setCourse] = useState([]);
  const [searchClass, setSearchClass] = useState("");
  const jwttoken = Cookies.get('jwttoken');
  const navigate = useNavigate();
  useEffect(() => {
    
    const checkTokenBeforeRoute = () => {
      if (!jwttoken) {    
        navigate('/login');
      }
    }; 
    checkTokenBeforeRoute();
  }, [jwttoken]);
  useEffect(() => {
  
    const fetchDataTeacher = async () => {
      try{
      const data = await getAllClassTeacherSide();
      if(data.result.length > 0){
      setOriginalClasses(data.result);
      setClases(data.result);
      }else{
        
      }
       }catch{

       }
    }
    const fetchDataStudent = async () => {
      try{
      const data = await getAllClassStudentSide();  
      if(data.result.length >0){
      setOriginalClasses(data.result);
      setClases(data.result);
      }else{}
      }catch{}
    }
    getCourse();
    if (rolename === "Teacher") {  
      fetchDataTeacher();
    } else if (rolename === "Student") {
      fetchDataStudent();
    }
  }, []);
  const getCourse = async() => {
    try{
    const data = await getAllViewCourse();
   if(data.result.length > 0){
    setCourse(data.result );
   }
    }catch(error){}
  }
  const filterCourseType = (event) => {
    const selectedClassType = event.target.value;
    if (selectedClassType) {
      const filtered = originalClasses.filter((val) => val.status == selectedClassType);
      setClases(filtered);
    } else {
      // Reset to the original classes if no course is selected
      setClases(originalClasses);
    }
  };
  const filterCourseClass = (event) => {
    const selectedClassId = event.target.value;

    if (selectedClassId) {
      const filtered = originalClasses.filter((val) => val.courseName === selectedClassId);
      setClases(filtered);
    } else {
      // Reset to the original classes if no course is selected
      setClases(originalClasses);
    }
  };
  const filterNameClass = (event) => {
    event.preventDefault();
    if (searchClass) {
      const filteredClasses = originalClasses.filter((val) =>
        val.name.toLowerCase().includes(searchClass.toLowerCase())
      );
      setClases(filteredClasses);
    } else {
      setClases(originalClasses);
    }

  };
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Lớp học', url: '/' },
   
  ];
  return (
    <>

<Box m="80px">
<NavigationPage items={breadcrumbItems} />
        <div className="course-card-title">
          <div style={{ marginBottom: '50px', marginTop: '50px' }} className="col-xl-8 order-2 order-xl-1">
            <div style={{ marginLeft: '20px' }} className="form-group m-form__group row align-items-center">
            <div className="col-sm-4">
              <label>Trạng thái</label>
              {((rolename === "Teacher")&& (
                <select
                  className="form-control"
                  onChange={filterCourseType}
                >
                <option value=""> Tất cả</option>
                <option value={0}> Chưa mở</option>
                <option value={1} > Đang dạy</option>
                <option value={2}> Đã dạy</option>
                </select>
                ))}
                  {((rolename === "Student")&& (
                <select
                  className="form-control"
                  onChange={filterCourseType}
                >
                <option value=""> Tất cả</option>
                <option value={0}> Chưa mở</option>
                <option value={1} > Đang học</option>
                <option value={2}> Đã học</option>
                </select>
                ))}
              </div>
              <div className="col-sm-4">
                <label>Lớp học</label>
                <select
                  className="form-control"
                  onChange={filterCourseClass}
                >
                   <option value=""> Tất cả</option>
                  {course?.map((classItem, index) => (
                    <option key={index} value={classItem.name}>
                      {classItem.name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="col-sm-4">
                <div className="m-input-icon m-input-icon--left">
                  <label></label>
                  <form onSubmit={filterNameClass}>
                    <input
                      type="text"
                      className="form-control m-input"
                      placeholder="Tìm kiếm lớp học ..."
                      value={searchClass}
                      onChange={(e) => setSearchClass(e.target.value)}
                    />
                  </form>
                  <span className="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i className="la la-search" />
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>


          {/*end: Search Form */}

          <table  style={{width:'100%'}} id="keywords" cellspacing="0" cellpadding="0">
            <thead >
              <tr>
                <th><span>Lớp học</span></th>
                <th><span>Khóa học</span></th>
                <th><span>Học sinh</span></th>
                <th><span>Thời gian</span></th>
                <th><span>Giáo viên</span></th>
                <th><span>Số buổi</span></th>
              </tr>
            </thead>
            <tbody>
              {classes?.map((val, index) => (
                <tr>
                  <td ><Link to={`./${val.id}`}><div>{val.name}</div></Link></td>
                  <td>{val.courseName}</td>
                  <td>{val.studentNumber}</td>
                  <td>   <div>{convertDateFormat(val.startDate)}</div>
                    <div>đến {convertDateFormat(val.endDate)}</div></td>
                  <td>{val.teacherName}</td>
                  <td>     <span className="m-widget11__sub m--font-metal">
                    {val.status === 0 ? 'Chưa mở lớp' : val.status === 1 ? 'Đang học' : val.status === 2 ? 'Đã học' : ''}
                    <span>{val.slotProcess}/{val.slotNumber}</span>
                  </span>
                    <div className="progress m-progress--sm">
                      <div
                        className="progress-bar m--bg-metal progress-bar-striped progress-bar-animated"
                        style={{ width: `${(val.slotProcess / val.slotNumber) * 100}%` }}
                        aria-valuenow={1}
                        aria-valuemin={0}
                        aria-valuemax={100}
                        role="progressbar"
                      ></div>
                    </div></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
    </Box>




    </>
  )
}

export default CoursesCard
