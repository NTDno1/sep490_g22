import React, { useEffect, useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faChartSimple } from '@fortawesome/free-solid-svg-icons'
import NotificationsIcon from '@mui/icons-material/Notifications';
import "../schedule/schedule.css"

import {
  Box,
 
} from "@mui/material";
import Cookies from 'js-cookie';
import fetchData from '../../../api/apiUtils';
import NavigationPage from '../common/NavigationPage/NavigationPage';
import Head from '../common/header/Head';

import { SchedualofClass } from '../../../api/apiClient/Class/ClassAPI';
import { formatDates } from '../../../constants/constant';

export default function CourseInfoSchedule() {
  const { id } = useParams();
  const [classinfoschedule, setClassInfoSchedule] = useState([]);
  const [renderCalendar, setRenderCalendar] = useState(false);
  const rolename = Cookies.get('roleName');
  const [className, setClassName] = useState('');
  const jwttoken = Cookies.get('jwttoken');
      const navigate = useNavigate();
      useEffect(() => {
        const checkTokenBeforeRoute = () => {
          if (!jwttoken) {
          
            navigate('/login');
          }
        };
      
        checkTokenBeforeRoute();
      }, [jwttoken]);
  useEffect(() => {
    getSchedual();
  }, [id]);
  const getSchedual = async() => {
    try{
    const data = await SchedualofClass(id);
    
    
    setClassInfoSchedule(data.result);
    setClassName(data.result[0].className)
        setRenderCalendar(true);
  }catch(error){
    
  }
}
  function formatDate(dateString) {
    const formattedDate = dateString.split('T')[0];
    return formattedDate;
  }
  const handleEventClick = (selected) => {
    

  };
  const breadcrumbItems = [
    { title: 'Trang chủ', url: '/' },
    { title: 'Lớp học', url: '/courses' },
    { title: className, url: '' },
    { title: 'Lịch học', url: '' },
  ];

  return (
    <>
      <Head/>
      
<Box m="80px">
        <NavigationPage items={breadcrumbItems} />
      <div className="container">

        <div className="row">

          <div className="col-12 my-5">
          
            <div style={{ display: 'flex', alignItems: 'center', textAlign: 'center', marginTop: '30px' }}>
            <h1 style={{ flex: '1.5', fontSize: '19px' }}>{className}</h1>
              <div style={{ flex: '1' }}></div>

              <div style={{ display: 'flex', flex: '1', float: 'right' }}>
                <div class="icon-container">
                  <Link to={`/../courses/${id}`} className='icon-class' href='#'><i class="iconcourse fa fa-graduation-cap"></i>  <span class="hover-text">Thông tin chung</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../courses/calender/${id}`}><i class=" iconcourse fa fa-calendar-check "></i> <span class="hover-text">Lịch học</span> </Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../courses/syllabus/${id}`}  ><FontAwesomeIcon className='iconcourse' icon={faFileLines} /><span class="hover-text">Giáo trình</span></Link>
                </div>
               <div class="icon-container">
                  <Link className='icon-class' to={`/../courses/member/${id}`}><FontAwesomeIcon className='iconcourse' icon={faUserGroup} /><span class="hover-text">Học sinh</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' ><i class="iconcourse-active fa fa-calendar" aria-hidden="true"></i><span class="hover-text">Thời khóa biểu</span></Link>
                </div>
                <div class="icon-container">
                  <Link className='icon-class' to={`/../courses/notification/${id}`}><i class="iconcourse fa fa-bell" aria-hidden="true"></i><span class="hover-text">Thông báo</span></Link>
                </div>
              </div>
            </div>
            <hr />
          </div>
          {/* Form START */}

          <Box m="20px">

            <Box display="flex" justifyContent="space-between">

              <Box flex="1 1 100%" ml="15px">
                {renderCalendar && (
                  <FullCalendar
                    height="75vh"
                    plugins={[
                      dayGridPlugin,
                      timeGridPlugin,
                      interactionPlugin,
                    ]}
                    headerToolbar={{
                      left: "prev,next today",
                      center: "title",
                      right: "timeGridWeek,timeGridDay",
                    }}
                    eventClassNames={(arg) => {
                      return 'custom-event'; // Lớp CSS tùy chỉnh bạn muốn áp dụng cho sự kiện
                    }}
                    editable={false}
                    selectable={false}
                    selectMirror={false}
                    dayMaxEvents={false}

                    eventClick={handleEventClick}
                    slotEventOverlap={false}
                    initialView="timeGridWeek"
                    slotDuration="02:00:00"
                    slotMinTime="07:00:00"
                    slotMaxTime="23:00:00"

                    initialEvents={classinfoschedule.map((val) => ({
                      id: val.id,
                      classId: val.classId,
                      title: `${rolename === 'Student' ?
                        (val.status === 0 ? 'Chưa điểm danh' : val.status === 1 ? 'Có mặt' : val.status === 2 ? 'Vắng mặt' : '') :
                        (rolename === 'Teacher' ? (val.staus === 0 ? 'Chưa điểm danh' : val.staus === 1 ? 'Đã điểm danh' : val.staus ===2 ? 'Kết thúc' : val.staus === 3 ? 'Đang chờ' : val.staus === 4 ? 'Đã đóng': val.staus ===5 ? 'Đổi lịch' : '') : '')}
                  ${val.roomName}  
                  ${val.className} \n Slot ${val.slotNum} / ${val.totalSlot}`,
                      date: formatDates(val.dateOffSlot),
                      start: `${formatDates(val.dateOffSlot)} ${val.timeStart}`,
                      end: `${formatDates(val.dateOffSlot)} ${val.timeEnd}`,
                    }))}
                    slotLabelFormat={{
                  hour: "2-digit",
                  minute: "2-digit",
                  omitZeroMinute: false,
                  hour12: false,
                }}
                  />
                )}
              </Box>
            </Box>
          </Box>


          {/* Form END */}
        </div>
      </div>
           </Box>       
    </>
  )
}
