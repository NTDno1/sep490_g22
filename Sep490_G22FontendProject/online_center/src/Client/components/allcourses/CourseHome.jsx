import React from "react"

import CoursesCard from "./CoursesCard"
import Head from "../common/header/Head"
import BackToTop from "../BackToTop/BackToTop"
import Footer from "../common/footer/Footer"



const CourseHome = () => {
  return (
    <>
    <Head/>
      <CoursesCard />
      <BackToTop />
      <Footer/>
    </>
  )
}

export default CourseHome
