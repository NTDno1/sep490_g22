import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp } from '@fortawesome/free-solid-svg-icons'


export default function BackToTop() {
    const [backToTop,setBackToTop] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll",() => {
            if(window.scrollY > 100) {
                setBackToTop(true)
            }else{
                setBackToTop(false)
            }
        })
    })
    const scrollUp = () => {
        window.scrollTo({
            top:0,
            behavior:'smooth',
        });
    }
  return (
    <div className='App'>
        {backToTop && (
            <button   style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'fixed',
                bottom: '20%',
                right: '50px',
                width: '40px',
                height: '40px',
                lineHeight: '0.1',
                overflow: 'hidden',
                textAlign: 'center',
                border: '1px solid #ddd',
                color: '#fff',  // Set text color to white
                borderRadius: '50%',  // Make it a circle
                backgroundColor: '#2746C0',  // Set background color
                zIndex: '19',
                fontSize: '16px',  // Adjust font size
              }}
              onClick={scrollUp}>
                 <FontAwesomeIcon icon={faAngleUp} style={{fontSize:'24px'}} />

            </button>
        )}
    </div>
  )
}
