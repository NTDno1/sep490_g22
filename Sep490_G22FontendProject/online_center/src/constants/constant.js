export const convertDateFormat = (inputDate) => {
    const dateTime = new Date(inputDate);
    const day = dateTime.getDate();
    const month = dateTime.getMonth() + 1;
    const year = dateTime.getFullYear();

    const formattedDate = `${day < 10 ? '0' + day : day}/${month < 10 ? '0' + month : month}/${year}`;
    return formattedDate;
  }
  export function formatDates(dateTimeString) {
    return dateTimeString.split('T')[0];
  }
 export function formatStartDate(Date) {
    if (Date) {
        return `${Date.substring(8, 10)}/${Date.substring(5, 7)}/${Date.substring(0, 4)}`;
    } else {
        return '';
    }
}

export function formatDateString(inputDateString) {
  
    const inputDate = new Date(inputDateString);
  
    const day = inputDate.getDate().toString();
    const month = (inputDate.getMonth() + 1).toString(); // Months are zero-based
    const year = inputDate.getFullYear().toString();
  
    return `${day}/${month}/${year}`;
  
}


export function convertDatePicker(inputDateString) {
  const inputDate = new Date(inputDateString);

  const month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
  const day = inputDate.getDate().toString().padStart(2, '0');
  const year = inputDate.getFullYear();
  const hours = inputDate.getHours().toString().padStart(2, '0');
  const minutes = inputDate.getMinutes().toString().padStart(2, '0');
  const ampm = inputDate.getHours() >= 12 ? 'PM' : 'AM';

  return `${month}/${day}/${year} ${hours}:${minutes} ${ampm}`;
}


export function convertDateFormattoAm(inputDateString) {
const inputDate = new Date(inputDateString);
const day = inputDate.getDate().toString().padStart(2, '0');
const month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
const year = inputDate.getFullYear().toString();
return `${month}/${day}/${year}`;
}


export function padDate(n) {
  return n.toString().padStart(2, '0');
}
export function convertYearMonthDay(inputDateString) {
  const inputDate = new Date(inputDateString);

  // Lấy thông tin về ngày, tháng và năm
  const year = inputDate.getFullYear();
  const month = (inputDate.getMonth() + 1).toString().padStart(2, '0'); // Tháng bắt đầu từ 0
  const day = inputDate.getDate().toString().padStart(2, '0');
  
  // Tạo chuỗi định dạng "yyyy/mm/dd"
  return `${year}-${month}-${day}`;
  
}
export function generateRandomCode() {
  const getRandomCharacter = (characters) => {
    const randomIndex = Math.floor(Math.random() * characters.length);
    return characters[randomIndex].toUpperCase();
  };

  const letters = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789';

  const randomLetter1 = getRandomCharacter(letters);
  const randomLetter2 = getRandomCharacter(letters);
  const randomNumber1 = getRandomCharacter(numbers);
  const randomNumber2 = getRandomCharacter(numbers);

  const randomCode = `${randomLetter1}${randomLetter2}${randomNumber1}${randomNumber2}`;
  return randomCode;
}

export const priceSplitter = (number) => (number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));

export function currencyFormat(num) {
  return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

export function getLastPartAfterSlash(url) {
  const parts = url.split('/');
  return parts[parts.length - 1];
}

export function convertoDate(inputDate) {
  // Tạo đối tượng Date từ chuỗi đầu vào
  const dateParts = inputDate.split('/');
  const formattedDate = new Date(
    parseInt(dateParts[2], 10),   
    parseInt(dateParts[1], 10) - 1, 
    parseInt(dateParts[0], 10)  
  );

  const year = formattedDate.getFullYear();
  const month = String(formattedDate.getMonth() + 1).padStart(2, '0'); 
  const day = String(formattedDate.getDate()).padStart(2, '0');

  return `${year}-${month}-${day}`;
}
export function formatNumberWithDecimalSeparator(number) {
  const parsedNumber = parseFloat(number);
  if (isNaN(parsedNumber)) {
    return '';
  }
  const roundedNumber = Math.round(parsedNumber);
  return roundedNumber.toLocaleString('vi-VN', { style: 'decimal' });
}