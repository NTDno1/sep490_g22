
import apiAuth from '../apiAuth'
const URL_Report = "api/Report";
const endPoint ={
    ReportCol: "ReportCol", 
}
export const  ReportCol= () => {
    return apiAuth.get(`${URL_Report}/${endPoint.ReportCol}`);
}
export const ReportMember= (year) => {
    return apiAuth.get(`${URL_Report}/ReportMember?date=${year}`);
}
export const ReportTeacher= (year) => {
    return apiAuth.get(`${URL_Report}/ListTeacher`);
}
export const TotalMember= (year) => {
    return apiAuth.get(`${URL_Report}/TotalMember`);
}