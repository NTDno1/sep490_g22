
import apiAuth from '../apiAuth'
const URL_Exam = "api/Exam";
export const  GetFilterOfCourse= (username) => {
    return apiAuth.get(`${URL_Exam}/GetFilterOfCourse?teacherName=${username}`);
}
export const GetFilterOfClassAndExam = (courseID,username) => {
    return apiAuth.get(`${URL_Exam}/GetFilterOfClassAndExam?CourseID=${courseID}&teacherName=${username}`);

}
export const EvaluateExam = (examID,classID) => {
    return apiAuth.get(`${URL_Exam}/EvaluateExam?examID=${examID}&classID=${classID}`);

}

export const UpdateEvaluateExam = (data,jwttoken) => {
    
    return apiAuth.put(`${URL_Exam}/UpdateEvaluate`,data,
    {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + jwttoken
          }
    }
      
      )
}
export const GetExamList = (courseID) => {
    return apiAuth.get(`${URL_Exam}/ListExam?courseID=${courseID}`);

}
export const GetAllCourseOfStudent = (username) => {
    return apiAuth.get(`${URL_Exam}/GetAllCourseOfStudent?username=${username}`);

}
export const GetMarkOfStudent = (username,courseID,classID) => {
    return apiAuth.get(`${URL_Exam}/GetMarkOfStudent?username=${username}&courseID=${courseID}&classID=${classID}`);

}
export const UpdateExam = (data) => {
    return apiAuth.put(`${URL_Exam}/EditExam`,data,{
        headers: {
            'Content-Type': 'application/json',
        },
    })
}
export const DeleteExam = (id) => {
    return apiAuth.delete(`${URL_Exam}/DeleteExam?id=${id}`)
}