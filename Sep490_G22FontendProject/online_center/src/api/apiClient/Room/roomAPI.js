import axiosRoom from './axiosRoom'
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Room = "api/Room";

const endPoint ={
    ListRoom: "listRoom?page=1&limit=100"
}
export const  GetAllRoom= () => {
    return apiAuth.get(`${URL_Room}/${endPoint.ListRoom}`);
}
export const  GetAllRoomAdminSide= () => {
    return apiAuth.get(`${URL_Room}/listRoomAdminSide`);
}
export const  addRoom= (data) => {
    return apiAuthPost.post(`${URL_Room}/addRoom`,data);
}
export const  addClassRoom= (data) => {
    return apiAuthPost.post(`${URL_Room}/addClassRoom`,data);
}
export const  GetRoomById= (id) => {
    return apiAuth.get(`${URL_Room}/viewById?id=${id}`);
}
export const  GetClassRoomById= (id) => {
    return apiAuth.get(`${URL_Room}/viewClassRoomById?id=${id}`);
}
export const  UpdateRoom= (id,data) => {
    return apiAuth.put(`${URL_Room}/updateRoom?id=${id}`,data);
}
export const  updateClassRoom= (id,data) => {
    return apiAuth.put(`${URL_Room}/updateClassRoom?id=${id}`,data);
}
export const DeleteRoom = (blogCodes) => {
    return apiAuth.delete(`${URL_Room}/deleteRoom`, { data: blogCodes });
};
export const DeleteClassRoom = (id) => {
    return apiAuth.delete(`${URL_Room}/deleteClassRoom?id=${id}`);
};