import axios from "axios";
import Cookies from "js-cookie";

import { refreshTokenTeacher } from "../../refershToken/refreshToken";
const tokens = Cookies.get('jwttoken');
const refreshtokens = Cookies.get('refreshToken'); 
const instance = axios.create({
    baseURL: process.env.REACT_APP_URL_Room,
    timeout: 300000
})
instance.interceptors.request.use(function (config) {
    const jwttoken =  Cookies.get('jwttoken');
    config.headers.Authorization =  jwttoken ? `Bearer ${jwttoken}` : '';
    return config;
  });
  instance.interceptors.response.use(
    (response) => {
      return response.data;
    },
    async (error) => {
      try {
        if (error.response && (error.response.status === 401 || error.response.status === 403)) {
          const res = await refreshTokenTeacher({
            accessToken : tokens,
            refreshToken : refreshtokens
        });
        console.log(res)
        Cookies.set("jwttoken",res.data.accessToken)   
          return instance(error.config);
        }
      } catch (err) {
        console.error("Error during token refresh:", err);
      }
      return Promise.reject(error);
    }
  );
export default instance