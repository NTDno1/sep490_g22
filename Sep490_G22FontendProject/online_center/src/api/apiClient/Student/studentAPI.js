
import Cookies from 'js-cookie';
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Student = "api/Student";
const endPoint ={
    AddStudent: "AddStudent",
    DeleteListStudent:"DeleteListStudent",
    UpdateStudent:"UpdateStudent"
   
}
export const  GetStudentAdminSide= () => {
    return apiAuth.get(`${URL_Student}/ListAllStudentAdminSide`);
}

export const  GetStudentDetail= (code) => {
    return apiAuth.get(`${URL_Student}/StudentDetail?id=${code}`);
}

export const AddNewStudent = (data) => {
    return apiAuth.post(`${URL_Student}/${endPoint.AddStudent}`,data)
}

export const UpdateInfoStudent = (data) => {
    return apiAuth.put(`${URL_Student}/${endPoint.UpdateStudent}`,data)
}

export const DeleteStudent = (id) => {
    return apiAuth.delete(`${URL_Student}/DeleteStudent?id=${id}`)
}

export const ActivateStudent = (id) => {
    return apiAuth.put(`${URL_Student}/ActivateStudent?id=${id}`)
}
export const DeactivateStudent = (id) => {
    return apiAuth.put(`${URL_Student}/DeactivateStudent?id=${id}`)
}


export const DeleteListStudent = async (data) => {
    try {
      const response = await apiAuth.delete(`${URL_Student}/${endPoint.DeleteListStudent}`, {
        data: data,
        headers: {
          'Content-Type': 'application/json',
        },
      });
      return response;
    } catch (error) {
      // Xử lý lỗi nếu cần thiết
      throw error;
    }
  };