
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'

const URL_Class ="api/Class";
const URL_StudentClass ="api/StudentClass";
const URL_Schedual ="api/Schedual";
const URL_ClassSlotDate ="api/ClassSlotDate";
const URL_SlotDate ="api/SlotDate";
const URL_Classroom ="api/ClassRoom";



const endPoint ={
    Teacherside: "ListClassTeacherSide",
    Studentside: "ListClassStudentSide",
    TeacherClassSchedual:"TeacherClassSchedual",
    ListClassAdmin: "ListClassAdminSide",
    AddClass:"AddClass",
    AddClassSlotDate:"AddClassSlotDate",
    DeleteListStudentOffClass:"DeleteListStudentOffClass",
    UpdateClass:"UpdateClass",
    ListSlotDate:"ListSlotDate",
    ListAllRoom: "ListAllRoom"
    
}
// export const  getAllCourse= () => {
//     return axiosClass.get();
// }

export const getAllClassTeacherSide = () => {
    return apiAuth.get(`${URL_Class}/${endPoint.Teacherside}`);
}

export const getAllClassStudentSide =() => {
    return apiAuth.get(`${URL_Class}/${endPoint.Studentside}`)
}

export const getAllClassAdminSide =() => {
    return apiAuth.get(`${URL_Class}/${endPoint.ListClassAdmin}`)
}

export const ClassDetail = (id) => {
    return apiAuth.get(`${URL_Class}/ClassDetail?id=${id}`)
}
export const StudentofClass = (id) => {
    return apiAuth.get(`${URL_StudentClass}/ListStudentOffClass?classId=${id}`)
}
export const SchedualofClass = (id) => {
    return apiAuth.get(`${URL_Schedual}/ClassSchedual?classId=${id}`)
}
export const TeacherClassSchedual = (id) => {
    return apiAuth.get(`${URL_Schedual}/${endPoint.TeacherClassSchedual}`)
}
export const ClassSlotDate = (classId) => {
    return apiAuth.get(`${URL_ClassSlotDate}/ListClassSlotDateByClass?id=${classId}`)
}
export const SlotDate = () => {
    return apiAuth.get(`${URL_SlotDate}/${endPoint.ListSlotDate}`)
}
export const ClassRoom = () => {
    return apiAuth.get(`${URL_Classroom}/${endPoint.ListAllRoom}`)
}

export const AddNewClass = (data) => {
    return apiAuth.post(`${URL_Class}/${endPoint.AddClass}`, data, {
        headers: {
            'Content-Type': 'application/json',
        },
    });
}

export const AddStudentInClass = (studentId,classId,note) => {
    return apiAuthPost.post(`${URL_StudentClass}/AddStudentClass?studentId=${studentId}&classId=${classId}&note=$${note}`)
}
export const GenerateSchedual = (classId) => {
    return apiAuthPost.post(`${URL_Schedual}/GenerateSchedual?classId=${classId}`)
}
export const AddClassSlotDate = (data) => {
    return apiAuth.post(`${URL_ClassSlotDate}/${endPoint.AddClassSlotDate}`, data, {
        headers: {
            'Content-Type': 'application/json',
        },
    });
}

export const DeleteMemberClass = (classId,id) => {
    return apiAuth.delete(`${URL_StudentClass}/DeletedStudentOffClass?classId=${classId}&studentId=${id}`)
}

export const DeleteListMemberClass = async (data) => {
    try {
      const response = await apiAuth.delete(`${URL_StudentClass}/${endPoint.DeleteListStudentOffClass}`, {
        data: data,
        headers: {
          'Content-Type': 'application/json',
        },
      });
      return response;
    } catch (error) {
      // Xử lý lỗi nếu cần thiết
      throw error;
    }
  };

 
export const DeleteClassSlotDate = async(id) => {
    return apiAuthPost.delete(`${URL_ClassSlotDate}/DeleteClassSlotDate?id=${id}`)
    }


export const UpdateInfoClass = (data) => {
    return apiAuthPost.put(`${URL_Class}/${endPoint.UpdateClass}`,data)
}

export const DeleteClass = (id) => {
    return apiAuth.delete(`${URL_Class}/DeleteClass?classId=${id}`)
}

export const CloseClass = (id) => {
    return apiAuth.put(`${URL_Class}/ClosedClass?classId=${id}`)
}
export const OpenClass = (id) => {
    return apiAuth.put(`${URL_Class}/OpenClass?classId=${id}`)
}
export const UpdateSchedule = (scheduleId,slotId,date,roomIds) => {
    return apiAuthPost.post(`${URL_Schedual}/UpdateSchedule?scheduleId=${scheduleId}&slotDate=${slotId}&dateChange=${date}&roomId=${roomIds}`)
}

export const ScheduleAdmin = (month,year) => {
    return apiAuthPost.get(`${URL_Schedual}/ListSchedual?MM_yyyy=${month}-${year}`)
}


export const AddNotificationClass = (data) => {
    return apiAuth.post(`api/NotificationClass/AddNotificationClass`, data, {
        headers: {
            'Content-Type': 'application/json',
        },
    });
}