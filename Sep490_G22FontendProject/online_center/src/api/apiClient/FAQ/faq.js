
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_FAQ = "api/SAQ";
const endPoint ={
    ListSAQ: "ListSAQ",
    AddFaq : "AddFaq",
    UpdateFaq:"UpdateFaq"
}

export const  GetFAQs= () => {
    return apiAuth.get(`${URL_FAQ}/${endPoint.ListSAQ}`);
}
export const  GetFAQsDetail= (code) => {
    return apiAuth.get(`${URL_FAQ}/FaqDetail?id=${code}`);
}
export const AddFAQs = (data) => {
    return apiAuthPost.post(`${URL_FAQ}/${endPoint.AddFaq}`,data)
}

export const DeleteFAQs = (id) => {
    return apiAuth.delete(`${URL_FAQ}/DeleteFaq?id=${id}`)
}
export const UpdateInfoFAQs = (data) => {
    return apiAuthPost.put(`${URL_FAQ}/${endPoint.UpdateFaq}`,data)
}

