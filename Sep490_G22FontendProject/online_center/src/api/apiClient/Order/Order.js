
import Cookies from 'js-cookie';
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Order = "api/Order";
const endPoint ={
    AddOrderDetail: "AddOrderDetail",
    AddOrder: "AddOrder",
    generatepdf:"generatepdf",
    ListOrder:"ListOrder",
    Payment:"Payment",
    DeleteOrderDetail:"DeleteOrderDetail",
}

export const AddNewOrderDetail = (data) => {
    return apiAuthPost.post(`${URL_Order}/${endPoint.AddOrderDetail}`,data)
}
export const AddNewOrder = (data) => {
    return apiAuthPost.post(`${URL_Order}/${endPoint.AddOrder}`,data)
}
export const generatepdf = (data) => {
    return apiAuthPost.get(`${URL_Order}/${endPoint.generatepdf}?InvoiceNo=1`)
}
export const listOrderbyAdmin= () => {
    return apiAuthPost.get(`${URL_Order}/${endPoint.ListOrder}`)
}
export const listOrderbyID= (id) => {
    return apiAuthPost.get(`${URL_Order}/ListOrderDetail?orderDetailId=${id}`)
}
export const listOrderDetailbyID= (id) => {
    return apiAuthPost.get(`${URL_Order}/Order?orderId=${id}`)
}
export const Payment = (data) => {
    return apiAuthPost.post(`${URL_Order}/${endPoint.Payment}`,data)
}
export const DeleteOrder = (id) => {
    return apiAuthPost.delete(`${URL_Order}/${endPoint.DeleteOrderDetail}?orderDetailId=${id}`)
}
