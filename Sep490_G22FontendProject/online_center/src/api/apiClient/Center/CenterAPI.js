
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Center = "api/Centers";

const endPoint ={
    Center: "ListCenter",
    AddCenter:"AddCenter",
    UpdateCenter:"UpdateCenter",
    ListCenter:"ListCenterClientSide"
}
export const  getCenterAPI= () => {
    return apiAuth.get(`${URL_Center}/${endPoint.Center}`);
}
export const  getCenterByIdAPI= (code) => {
    return apiAuth.get(`${URL_Center}/CenterDetail?id=${code}`);
}
export const  AddCenterByAdmin= (data) => {
    return apiAuthPost.post(`${URL_Center}/${endPoint.AddCenter}`,data);
}

export const DeleteCenter= (id) => {
    return apiAuth.delete(`${URL_Center}/DeleteCenter?id=${id}`);
}

export const OpenCenter= (id) => {
    return apiAuth.put(`${URL_Center}/OpenCenter?id=${id}`);
}

export const CloseCenter= (id) => {
    return apiAuth.put(`${URL_Center}/CloseCenter?id=${id}`);
}

export const UpdateInfoCenter = (data) => {
    return apiAuthPost.put(`${URL_Center}/${endPoint.UpdateCenter}`,data)
}
export const ListCenterByClient = (selectedCategoryId) => {
    return apiAuth.get(`${URL_Center}/${endPoint.ListCenter}?cateId=${selectedCategoryId}`)
}