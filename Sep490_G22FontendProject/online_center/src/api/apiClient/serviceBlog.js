import { Axios } from 'axios'
import axiosBLog from'./axiosBlog'

const endPoint ={
    LISTBLOG: "viewBlog",
}

export const viewBlog=()=>{ return axiosBLog.get(`${endPoint.LISTBLOG}`)}