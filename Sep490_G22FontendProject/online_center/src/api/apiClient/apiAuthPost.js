import axios from "axios";
import Cookies from "js-cookie";
import { refreshTokenTeacher } from "../refershToken/refreshToken";



const tokens = Cookies.get('jwttoken');
const refreshtokens = Cookies.get('refreshToken');  

const instance = axios.create({
    baseURL: process.env.REACT_APP_URL_LOCALHOST,
    timeout: 300000
})
instance.interceptors.request.use(function (config) {
    const jwttoken =  Cookies.get('jwttoken');
    config.headers.Authorization =  jwttoken ? `Bearer ${jwttoken}` : '';
    config.headers['Content-Type'] = 'application/json';  
    return config;
  });
  instance.interceptors.response.use(
    (response) => {
      return response.data;
    },
    async (error) => {
      console.log(error)
      try {
        if (error.response && (error.response.status === 401 || error.response.status === 403)) {
          const res = await refreshTokenTeacher({
            accessToken : tokens,
            refreshToken : refreshtokens
        });
      
        Cookies.set("jwttoken",res.data.accessToken)
        Cookies.set("jwttoken",res.data.refreshToken)   

          return instance(error.config);
        }
      } catch (err) {
        console.error("Error during token refresh:", err);
      }
      return Promise.reject(error);
    }
  );
export default instance