
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Account = "api/Account";


const endPoint = {
    Teacher: `TeacherProfile?username`,
    Student: "StudentProfile?username", 
    ListEmployee : "ListEmployee",
    DeleteListEmployee:"DeleteListEmployee",
    UpdateEmployee:"UpdateEmployee"
}
export const  getInfoTeacher= (username) => {
    return apiAuth.get(`${URL_Account}/${endPoint.Teacher}=${username}`);
}
export const  getInfoStudent= (username) => {
    return apiAuth.get(`${URL_Account}/${endPoint.Student}=${username}`);
}
export const  AddEmployee= (data) => {
    return apiAuthPost.post(`/AddEmployee`,data);
}
export const  ListEmployee= (centerId,name) => {
    return apiAuth.get(`ListEmployee?centerId=${centerId}&name=${name}`);
}
export const  DeleteEmployee= (id) => {
    return apiAuth.delete(`${URL_Account}/DeleteEmployee?id=${id}`);
}
export const ActiveEmployee= (id) => {
    return apiAuth.put(`ActivateEmployee?id=${id}`);
}
export const DeactivateEmployee= (id) => {
    return apiAuth.put(`DeactivateEmployee?id=${id}`);
}
export const DeleteListEmployee= (id) => {
    return apiAuth.get(`${endPoint.DeleteListEmployee}`);
}
export const EmployeeDetail= (code) => {
    return apiAuth.get(`EmployeeDetail?id=${code}`);
}
export const UpdateInfoEmployee= (code,Name,Adress,Email,PhoneNumber,Dob,RoleId) => {
    return apiAuth.put(`UpdateEmployee?Id=${code}&Name=${Name}&Adress=${Adress}&Email=${Email}&PhoneNumber=${PhoneNumber}&Dob=${Dob}&RoleId=${RoleId}`);
}
export const UpdateInfosEmployee = (id,name,adress,email,phonenumber,dob) => {
    return apiAuth.put(`UpdateEmployee?Id=${id}&Name=${name}&Adress=${adress}&Email=${email}&PhoneNumber=${phonenumber}&Dob=${dob}`);
}

export const ChangePassword = (data) => {
    return apiAuthPost.put(`${URL_Account}/ChangePassword`,data);
}

