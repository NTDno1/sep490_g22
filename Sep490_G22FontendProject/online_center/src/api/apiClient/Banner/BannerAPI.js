
import apiAuth from '../apiAuth'
const URL_Blog = "api/Blog";
const URL_Notification ="api/NotificationClass";
const endPoint ={
    Banner: "page=1&limit=3&cateId=4",
    Blog: "page=1&limit=10&cateId=1",
    Notification: "&limit=6&cateId=2",
    BannerAbout: "page=1&limit=10&cateId=9",
    Head: "viewBlogByCate?cateId=3",
    Contact:"viewBlogByCate?cateId=11",
    Information: "page=1&limit=10&cateId=10",
    viewCate:"viewCate",
    AddNotificationClass: "AddNotificationClass",
    Footer:"viewBlogByCate?cateId=2",
    BlogDetail:"viewBlogById",
    ViewBlog:"viewBlog",
    Another:"viewRelatedBlog",
}
export const  getBanner4API= () => {
    return apiAuth.get(`${URL_Blog}/viewBlogClientSide?${endPoint.BannerAbout}`);
}
export const  getEventsAPI= () => {
    return apiAuth.get(`${URL_Blog}/viewBlogClientSide?${endPoint.Banner}`);
}
export const  getBlogAPI= () => {
    return apiAuth.get(`${URL_Blog}/${endPoint.ViewBlog}`);
}
export const getNotificationAPI = (id) => {
    
    return apiAuth.get(`${URL_Blog}/viewBlogClientSide?page=${id}${endPoint.Notification}`)
}
export const getHeadAPI =() => {   
    return apiAuth.get(`${URL_Blog}/${endPoint.Head}`)
}

export const getInformationApi = () => {
    return apiAuth.get(`${URL_Blog}/viewBlogClientSide?${endPoint.Information}`)
}

export const getHeaderNotification = () => {   
    return apiAuth.get(`${URL_Notification}/ListNotificationClass`)
}

export const ListBlogAdmin = (selectedCategoryId) => {   
    return apiAuth.get(`${URL_Blog}?cateId=${selectedCategoryId}`)
}

export const ViewBlogCate = () => {   
    return apiAuth.get(`${URL_Blog}/${endPoint.viewCate}`)
}
export const ViewContact=() => {
    return apiAuth.get(`${URL_Blog}/${endPoint.Contact}`)
}

export const ViewFooter=() => {
    return apiAuth.get(`${URL_Blog}/${endPoint.Footer}`)
}
export const ViewBlogDetail=(blogCode) => {
    return apiAuth.get(`${URL_Blog}/${endPoint.BlogDetail}?code=${blogCode}`)
}
export const ViewBlog=() => {
    return apiAuth.get(`${URL_Blog}/${endPoint.ViewBlog}`)
}
export const ViewRelatedBlog=(blogCodes) => {
    return apiAuth.get(`${URL_Blog}/${endPoint.Another}?blogCode=${blogCodes}`)
}
export const DeleteBlog = (blogCodes) => {
    return apiAuth.delete(`${URL_Blog}`, { data: blogCodes });
};
export const AddBlogAdmin = (data) => {
    return apiAuth.post(`${URL_Blog}`,data);
};
export const UpdateBlogAdmin = (code, dataSend) => {
    return apiAuth.put(`${URL_Blog}?code=${code}`, dataSend);
};

export const ViewNotificationCard = (id) => {
    return apiAuth.get(`${URL_Blog}/viewBlogClientSide?page=${id}&limit=6&cateId=1`);
};
export const ChangeStatus = (code,status) => {
    return apiAuth.put(`${URL_Blog}/changeStatus?code=${code}&status=${status}`);
};
