
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'

const URL_Attendence = "api/Attendence";
const endPoint ={
    ListAttendenceByStudent: "ListAttendenceByStudent",
    TakeAttendenceAdmin:"TakeAttendenceAdmin",
    TakeAttendence:"TakeAttendence",

}
export const  ListAttendenceByClassId= (classId) => {
    return apiAuth.get(`${URL_Attendence}/ListAttendenceByClassId?ClassId=${classId}`);
}
export const ListAttendenceByStudent = () => {
    return apiAuth.get(`${URL_Attendence}/${endPoint.ListAttendenceByStudent}`);
  };

export const  ListAttendenceBySchedule= (slotId) => {
    return apiAuth.get(`${URL_Attendence}/ListAttendenceBySchedule?scheduleID=${slotId}`)
}


export const  TakeAttendenceAdmin= (data) => {
    return apiAuthPost.post(`${URL_Attendence}/${endPoint.TakeAttendenceAdmin}`,data)
}
export const  TakeAttendence= (data) => {
    return apiAuthPost.post(`${URL_Attendence}/${endPoint.TakeAttendence}`,data)
}
export const GenerateAttendence = (classId) => {
    return apiAuth.post(`${URL_Attendence}/GenerateAttendence?classId=${classId}`)
}