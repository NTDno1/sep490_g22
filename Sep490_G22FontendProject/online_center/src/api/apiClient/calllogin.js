import axiosClient from './axiosClient'
export const getTodosAPI = () => {
    return axiosClient.get();
}
export const getCodeDetail = (id) => {
    return axiosClient.get(`https://localhost:7241/api/Course/${id}`)
}