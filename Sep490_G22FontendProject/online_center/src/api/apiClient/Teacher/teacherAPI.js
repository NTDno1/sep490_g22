
import apiAuth from '../apiAuth'

const URL_Teacher = "api/Teacher";
const endPoint ={
    AddTeacher: "AddTeacher",
    DeleteListTeacher:"DeleteListTeacher",
    UpdateTeaher : "UpdateTeaher"
   
}

export const  GetTeacherAdminSide= (username) => {
    return apiAuth.get(`${URL_Teacher}/ListTeacherAdminSide`);
}
export const  GetTeacherDetail= (code) => {
    return apiAuth.get(`${URL_Teacher}/TeacherDetail?id=${code}`);
}

export const AddNewTeacher=(data) => {
  return apiAuth.post(
    `${URL_Teacher}/${endPoint.AddTeacher}`,data
  );
}
export const UpdateInfoTeacher = (data) => {
  return apiAuth.put(
    `${URL_Teacher}/${endPoint.UpdateTeaher}`,data
  );
};

export const DeleteTeacher=(id) => {
    return apiAuth.delete(`${URL_Teacher}/DeleteTeacher?id=${id}`)
}
export const DeleteListTeacher= async(data) => {
    try {
        const response = await  apiAuth.delete(`${URL_Teacher}/${endPoint.DeleteListTeacher}`, {
          data: data,
          headers: {
            'Content-Type': 'application/json',
          },
        });
        return response;
      } catch (error) {
        // Xử lý lỗi nếu cần thiết
        throw error;
      }
}

export const ActivateTeacher=(id) => {
    return apiAuth.put(`${URL_Teacher}/ActivateTeacher?id=${id}`)
}
export const DeactivateTeacher=(id) => {
    return apiAuth.put(`${URL_Teacher}/DeactivateTeacher?id=${id}`)
}

