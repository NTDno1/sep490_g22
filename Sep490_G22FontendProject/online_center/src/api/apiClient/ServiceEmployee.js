import { Axios } from 'axios'
import axiosEmployeeApi from'./axiosEmployeeApi'
const endPoint ={
    LISTEMPLOYEE: "ListEmployee",
    DELETEEMPLOYEE: "DeleteEmployee",
    EDITEmployee: ""
}
export const listEmployee=()=>{ return axiosEmployeeApi.get(`${endPoint.LISTEMPLOYEE}`)}
export const deleteEmployee=(id)=>{ return axiosEmployeeApi.delete(`${endPoint.DELETEEMPLOYEE}${"?id="}${id}`)}
export const editEmployee=(id)=>{ return axiosEmployeeApi.editEmployee(`${endPoint.EDITEmployee}${"?id="}${id}`)}