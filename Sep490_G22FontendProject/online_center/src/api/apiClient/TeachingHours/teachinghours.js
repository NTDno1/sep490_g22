
import apiAuth from '../apiAuth'
const URL_TeacherTeachingHour = "api/TeacherTeachingHour";
const endPoint ={
    ListTeacherTeachingHour: "ListTeacherTeachingHour",   
}
export const  ListTeacherTeachingHour= () => {
    return apiAuth.get(`${URL_TeacherTeachingHour}/${endPoint.ListTeacherTeachingHour}`);
}

