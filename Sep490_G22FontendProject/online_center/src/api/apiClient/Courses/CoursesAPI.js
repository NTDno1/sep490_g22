
import apiAuth from '../apiAuth'
import apiAuthPost from '../apiAuthPost'
const URL_Course ="api/Course";
const URL_Syllabus ="api/Syllabus";

const endPoint ={
    ListSyllabus: "ListSyllabus",
    AddSyllabus : "AddSyllabus",
    AddCourse: "AddCourse",
    UpdateSyllabus:"UpdateSyllabus"
}
export const GetCourseDetail = (id) => {
    return apiAuth.get(`${URL_Course}/DetailCourse?courseId=${id}`)
}
export const  getAllViewCourse= () => {
    return apiAuth.get(`${URL_Course}/ListCourse`);
}
export const  getAllCourseHome= () => {
    return apiAuth.get(`${URL_Course}/getCourse`);
}
export const  viewSyllabus= (id) => {
    return apiAuth.get(`${URL_Syllabus}/ListSylabusByClassId?classId=${id}`);
}
export const  viewAllSyllabus= (id) => {
    return apiAuth.get(`${URL_Syllabus}/${endPoint.ListSyllabus}`);
}

export const addNewSyllabus = (data) => {
    return apiAuthPost.post(`${URL_Syllabus}/${endPoint.AddSyllabus}`,data)
}
export const UpdateCourse = (data) => {
    return apiAuth.put(`${URL_Course}/UpdateCourse`, data);
  };

export const addNewCourse = (data) => {
    return apiAuth.post(`${URL_Course}/${endPoint.AddCourse}`,data)

}

export const CourseDetailbyAdmin = (id) => {
    return apiAuth.get(`${URL_Course}/DetailCourse?courseId=${id}`)
}
export const DeleteSyllabus = (id) => {
    return apiAuth.delete(`${URL_Syllabus}/DeleteSyllabus?id=${id}`)
}

export const SyllabusDetail = (code) => {
    return apiAuth.get(`${URL_Syllabus}/SyllabusDetail?id=${code}`)
}
export const UpdateInfoSyllabus = (data) => {
    return apiAuthPost.put(`${URL_Syllabus}/${endPoint.UpdateSyllabus}`,data)
}

export const DeleteCourse = (id) => {
    return apiAuth.delete(`${URL_Course}/DeleteCourse?courseId=${id}`)
}
export const ViewCourseFillter = (id,selectedCategoryId) => {
    return apiAuth.get(`${URL_Course}/viewCourseClientSide?page=${id}&limit=6&centerCode=${selectedCategoryId}`);
};