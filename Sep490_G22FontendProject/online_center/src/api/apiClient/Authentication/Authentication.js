
import apiAuth from '../apiAuth'

const URL_Authen ="api/Authentication";
const endPoint ={
    LoginTeacher: "LoginTeacher",
    LoginStudent:"LoginStudent",
    LoginAdmin:"LoginEmployee",
    LoginSuperAdmin:"Login"
}
export const  LoginTeacher= (data) => {
    return apiAuth.post(`${URL_Authen}/${endPoint.LoginTeacher}`,data);
}
export const  LoginStudent= (data) => {
    return apiAuth.post(`${URL_Authen}/${endPoint.LoginStudent}`,data);
}
export const  LoginAdminCenter= (data) => {
    return apiAuth.post(`${URL_Authen}/${endPoint.LoginAdmin}`,data);
}
export const  LoginSuperAdmin= (data) => {
    return apiAuth.post(`${URL_Authen}/${endPoint.LoginSuperAdmin}`,data);
}
