import axios from "axios";
import Cookies from "js-cookie";
const instance = axios.create({
    baseURL: process.env.REACT_APP_URL_Token,
    timeout: 300000
})
instance.interceptors.request.use(function (config) {
    
    return config;
  }, (error) => {
    return Promise.reject(error);
  });
export default instance