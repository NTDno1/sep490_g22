import axiosrefreshToken from './axiosrefreshToken'

const endPoint ={
    Teacherside: "RefreshTeacher",
    Studentside: "RefreshStudent",
    Adminside: "RefreshEmployee"
}
export const refreshTokenTeacher = async (data) => {
    try {
      const resp = await axiosrefreshToken.post(`${endPoint.Teacherside}`,data);
      return resp;
    } catch (e) {
      console.log("Error",e);   
    }
  };
export const refreshTokenStudent =async (data) => {
  try {
    const resp = await axiosrefreshToken.post(`${endPoint.Studentside}`,data);
    return resp;
  } catch (e) {
    console.log("Error",e);   
  }
   
}
export const refreshTokenAdmin = (id) => {
    return axiosrefreshToken.post(`${endPoint.Adminside}`)
}

