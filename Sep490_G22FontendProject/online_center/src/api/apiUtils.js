import Cookies from "js-cookie";

const  jwttoken =  Cookies.get('jwttoken');
const fetchData = async (apiEndpoint) => {
    try {
      const response = await fetch(apiEndpoint, {
        headers: {
          'Authorization': 'Bearer ' + jwttoken,
        },
      });
  
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      return data.result;
    } catch (error) {
      console.error('Error fetching data:', error.message);
      throw error; // You can handle the error in the calling code if needed
    }
  };
  
  export default fetchData;