import React, { Component} from 'react';
import { FacebookProvider, CustomChat } from 'react-facebook';

export default class ChatBot extends Component {
  render() {
    return (
      <div style={{ position: 'fixed' , bottom:'20%' , textAlign:'center' }}>
      <FacebookProvider appId="718809049834599" chatSupport>
        <CustomChat pageId="188199927706380" minimized={false}/>
      </FacebookProvider>   
      </div> 
    );
  }
}