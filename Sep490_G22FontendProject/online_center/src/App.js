import { BrowserRouter as Router, Routes, Route, useNavigate, Navigate } from "react-router-dom"
import CourseHome from "./Client/components/allcourses/CourseHome"
import Home from "./Client/components/home/Home"
import { Exam } from "./Client/components/Exam/Exam"
import Login from "./Client/components/login/Login"
import "./Client/AppClient.css"
import CourseDetail from "./Client/components/home/Homecourse/CourseDetail"
import RequireAuth from "./RequireAuth"
import Anauthorized from "./Anauthorized"
import Setting from "./Client/components/setting/Setting"
import Faq from "./Client/components/FAQ/Faq"
import BlogDetail from "./Client/components/home/blog/blogDetails"
import Schedule from "./Client/components/schedule/Schedule"
import AddCourse from "./Admin/scenes/course/AddCourse"
import CourseInfo from "./Client/components/allcourses/CourseInfo"
import CourseInfoCalender from "./Client/components/allcourses/CourseInfoCalender"
import CourseInfoSyllabus from "./Client/components/allcourses/CourseInfoSyllabus"
import CourseInfoMember from "./Client/components/allcourses/CourseInfoMember"
import CourseInfoSchedule from "./Client/components/allcourses/CourseInfoSchedule"
import Notification from "./Client/components/Notification/Notification"

//Import admin-side
import LoginAdmin from "./Admin/components/LoginAdmin/LoginAdmin"
import Dashboard from "./Admin/scenes/dashboard";
import Member from "./Admin/scenes/team";
import Invoices from "./Admin/scenes/invoices";
import Contacts from "./Admin/scenes/contacts";

import Geography from "./Admin/scenes/geography";
import Calendar from "./Admin/scenes/calendar/calendar";
import Course from "./Admin/scenes/course"
import CourseDetailAdmin from "./Admin/scenes/course/coursedetail"
import AddMember from "./Admin/scenes/team/AddMember"
import Class from "./Admin/components/Class"
import AddClass from "./Admin/components/Class/Addclass"
import AddClassSchedule from "./Admin/components/Class/AddClassSchedule"
import AddClassMember from "./Admin/components/Class/AddClassMember"
import BlogAdmin from "./Admin/scenes/blog"
import AddBlog from "./Admin/scenes/blog/addBlog"
import UpdateBlog from "./Admin/scenes/blog/updateBlog"
import Statisofteaching from "./Client/components/Report/Statisofteaching"
import AttendDetail from "./Client/components/Attend/AttendDetail"
import CourseScheduleAdmin from "./Admin/scenes/course/CourseScheduleAdmin"
import EditClassInfo from "./Admin/components/Class/EditClassInfo"
import AttendAdmin from "./Admin/components/AttendAdmin/AttendAdmin"
import FAQAdmin from "./Admin/components/FAQ"


import SyllabusAdmin from "./Admin/components/Syllabus"
import AddFaq from "./Admin/components/FAQ/AddFaq"
import UpdateFaq from "./Admin/components/FAQ/UpdateFaq"
import AddSyllabus from "./Admin/components/Syllabus/AddSyllabus"
import UpdateSyllabus from "./Admin/components/Syllabus/UpdateSyllabus"
import AboutUs from "./Client/components/AboutUs/aboutUs"
import Center from "./Admin/components/Center"
import AddCenter from "./Admin/components/Center/AddCenter"
import UpdateCenter from "./Admin/components/Center/UpdateCenter"
import UpdateMember from "./Admin/scenes/team/UpdateMember"
import Teacher from "./Admin/components/Teacher"
import AddTeacher from "./Admin/components/Teacher/AddTeacher"
import UpdateTeacher from "./Admin/components/Teacher/UpdateTeacher"
import Student from "./Admin/components/Student"
import AddStudent from "./Admin/components/Student/AddStudent"
import UpdateStudent from "./Admin/components/Student/UpdateStudent"
import AttendReport from "./Client/components/Report/AttendReport"
import CourseInfoNotification from "./Client/components/allcourses/CourseInfoNotification"

import ForgetPassword from "./Client/components/login/forgetpassword"
import Cookies from "js-cookie"
import { MarkReport } from "./Client/components/Exam/MarkReport"
import ManageExam from "./Client/components/Exam/ManageExam"
import AdminClassInfo from "./Admin/components/Class/AdminClassInfo"
import AdminClassInfoSyllabus from "./Admin/components/Class/AdminClassInfoSyllabus"
import AdminClassInfoMember from "./Admin/components/Class/AdminClassInfoMember"
import AdminClassInfoCalender from "./Admin/components/Class/AdminClassInfoCanlender"
import RoomAdmin from "./Admin/scenes/room"
import AddRoom from "./Admin/scenes/room/addRoom"
import UpdateRoom from "./Admin/scenes/room/updateRoom"
import AdminClassInfoNotification from "./Admin/components/Class/AdminClassInfoNotification"
import ForgotPasswordAdmin from "./Admin/components/LoginAdmin/ForgotPasswordAdmin"
import ShowStudent from "./Admin/components/Student/ShowStudent"
import AdminProfile from "./Admin/components/Profile/AdminProfile"
import ClassRoomAdmin from "./Admin/scenes/classroom/ViewClassRoom"
import AddClassRoom from "./Admin/scenes/classroom/AddClassRoom"
import UpdateClassRoom from "./Admin/scenes/classroom/UpdateClassRoom"
import ChangeInfoPassword from "./Client/components/setting/ChangeInfoPassword"
import Invoice from "./Admin/components/Profile/Invoice"
import NewOrderDetail from "./Admin/components/Profile/NewOrderDetail"
import NewOrder from "./Admin/components/Profile/NewOrder"

import AllTeacher from "./Client/components/Course/AllCourse"
import InvoiceDetailStudent from "./Client/components/Invoice/InvoiceDetailStudent"
import InvoiceStudent from "./Client/components/Invoice/InvoiceStudent"



function App() {
  const isAuthenticated = Cookies.get('username');

  const ROLES = {
    SuperAdmin: 'Super Admin',
    AdminCenter: 'Employee',
    Teacher: 'Teacher',
    Student: 'Student'
  }
  return (
    <>
      {/* ClientSide */}
      <Routes>
        {/* Public Routes */}
        {!isAuthenticated && (
          <>
            <Route path='/login' element={<Login />} />
            <Route path='/login_admin' element={<LoginAdmin />} />
            <Route path='/forgetpassword' element={<ForgetPassword />} />
            <Route path='/forgetpasswordadmin' element={<ForgotPasswordAdmin />} />

          </>
        )}
        <Route path='/' element={<Home />} />
        <Route path="unauthorized" element={<Anauthorized />} />
        <Route path="notification" element={<Notification />} />
        <Route path="aboutus" element={<AboutUs />} />
        <Route path="course" element={<AllTeacher />} />
        <Route path='/faq' element={<Faq />} />
        <Route path='/course/:id' element={<CourseDetail />}></Route>
        <Route path='/blog/:blogCode' element={<BlogDetail />}></Route>
        <Route path='/updateroom/:id' element={<UpdateClassRoom />}></Route>
        <Route element={<RequireAuth allowRoutes={[ROLES.Teacher, ROLES.Student]} />}>
          {/* Protect Route */}
          <Route path='/settings' element={<Setting />}></Route>
          <Route path='/changePassword' element={<ChangeInfoPassword />}></Route>
          <Route path='/courses' element={<CourseHome />} />
          <Route path='/courses/:id' element={<CourseInfo />}></Route>
          <Route path='/courses/calender/:id' element={<CourseInfoCalender />}></Route>
          <Route path='/courses/syllabus/:id' element={<CourseInfoSyllabus />}></Route>
          <Route path='/courses/schedule/:id' element={<CourseInfoSchedule />}></Route>
          <Route path='/courses/notification/:id' element={<CourseInfoNotification />}></Route>
          <Route path='/courses/member/:id' element={<CourseInfoMember />}></Route>
          <Route path='/schedule' element={<Schedule />} />
        </Route>
        <Route element={<RequireAuth allowRoutes={ROLES.Student} />}>
          <Route path='/mark-report' element={<MarkReport />}></Route>
          <Route path='/attendreport' element={<AttendReport />}></Route>
        </Route>
        <Route element={<RequireAuth allowRoutes={ROLES.SuperAdmin} />}>
          <Route path="/manageteam" element={<Member />} />
          <Route path="/addmember" element={<AddMember />} />
          <Route path="/updateMember/:code" element={<UpdateMember />} />
          <Route path="/center" element={<Center />} />
          <Route path="/center/addCenter" element={<AddCenter />} />
          <Route path="/updateCenter/:code" element={<UpdateCenter />} />


        </Route>
        <Route element={<RequireAuth allowRoutes={[ROLES.SuperAdmin, ROLES.AdminCenter]} />}>
          <Route path="/dashboard" element={<Dashboard />} />
        </Route>

        <Route element={<RequireAuth allowRoutes={ROLES.AdminCenter} />}>
          <Route path="/AdminProfile" element={<AdminProfile />} />
          <Route path="/courseadmin" element={<Course />} />
          <Route path='/coursesadmin/:id' element={<AdminClassInfo />}></Route>
          <Route path='/coursesadmin/calender/:id' element={<AdminClassInfoCalender />}></Route>
          <Route path='/coursesadmin/syllabus/:id' element={<AdminClassInfoSyllabus />}></Route>
          <Route path='/coursesadmin/schedule/:id' element={<CourseScheduleAdmin />}></Route>
          <Route path='/coursesadmin/member/:id' element={<AdminClassInfoMember />}></Route>
          <Route path='/coursesadmin/notification/:id' element={<AdminClassInfoNotification />}></Route>
          <Route path="/classadmin" element={<Class />} />
          <Route path="/roomadmin" element={<ClassRoomAdmin />} />
          <Route path="/courseadmin/addcourse" element={<AddCourse />} />
          <Route path="/classadmin/editclass/:newclassId" element={<EditClassInfo />} />
          <Route path="/classadmin/addclass" element={<AddClass />} />
          <Route path="/addclassschedule/:classId" element={<AddClassSchedule />} />
          <Route path="/addclasssmember/:classId" element={<AddClassMember />} />

          <Route path='/coursedetail/:id' element={<CourseDetailAdmin />}></Route>
          <Route path="/contacts" element={<Contacts />} />
          <Route path="/order" element={<NewOrder />} />

          <Route path="/orderdetail/:envoiceid" element={<NewOrderDetail />} />

          <Route path="/calendar" element={<Calendar />} />
          <Route path="/geography" element={<Geography />} />
          <Route path='/roomadmin/addroom' element={<AddClassRoom />} ></Route>
          <Route path="/attendadmin" element={<AttendAdmin />} />
          <Route path="/Syllabusadmin" element={<SyllabusAdmin />} />
          <Route path='/Syllabusadmin/addSyllabus' element={<AddSyllabus />} ></Route>
          <Route path='/updateSyllabusadmin/:code' element={<UpdateSyllabus />}></Route>

          <Route path="/addteacher" element={<AddTeacher />} />
          <Route path="/updateTeacher/:code" element={<UpdateTeacher />} />
          <Route path="/student" element={<Student />} />
          <Route path="/addstudent" element={<AddStudent />} />
          <Route path="/updateStudent/:code" element={<UpdateStudent />} />
          <Route path="/ShowStudent/:code" element={<ShowStudent />} />
          <Route path="/manageExam/:id" element={<ManageExam />} />
        </Route>
        <Route element={<RequireAuth allowRoutes={ROLES.Teacher} />}>
          <Route path='/attend' element={<AttendDetail />} />
          <Route path='/attend/:classId/:slotId' element={<AttendDetail />} />
          <Route path='/staticteaching' element={<Statisofteaching />} />
          <Route path='/exam' element={<Exam />} />
        </Route>
        <Route element={<RequireAuth allowRoutes={ROLES.Student} />}>
          <Route path='/mark-report' element={<MarkReport />}></Route>
          <Route path='/attendreport' element={<AttendReport />}></Route>
          <Route path='/invoicestudent' element={<InvoiceStudent />}></Route>
          <Route path='/orderStudentdetail/:id' element={<InvoiceDetailStudent />}></Route>
        </Route>

        <Route element={<RequireAuth allowRoutes={ROLES.SuperAdmin} />}>

          <Route path="/manageteam" element={<Member />} />
          <Route path="/addmember" element={<AddMember />} />
          <Route path="/updateMember/:code" element={<UpdateMember />} />
          <Route path="/center" element={<Center />} />
          <Route path="/center/addCenter" element={<AddCenter />} />
          <Route path="/updateCenter/:code" element={<UpdateCenter />} />
          <Route path="/blogadmin" element={<BlogAdmin />} />
          <Route path='/blogadmin/addBlog/:cateId' element={<AddBlog />} ></Route>
          <Route path='/updateblog/:code' element={<UpdateBlog />}></Route>
          <Route path="/FAQadmin" element={<FAQAdmin />} />
          <Route path='/FAQadmin/addFAQ' element={<AddFaq />} ></Route>
          <Route path='/updateFAQ/:code' element={<UpdateFaq />}></Route>

        </Route>
        <Route element={<RequireAuth allowRoutes={[ROLES.SuperAdmin, ROLES.AdminCenter]} />}>
          <Route path="/dashboard" element={<Dashboard />} />
        </Route>

        <Route element={<RequireAuth allowRoutes={ROLES.AdminCenter} />}>

          <Route path="/invoice" element={<Invoice />} />

          <Route path="/AdminProfile" element={<AdminProfile />} />
          <Route path="/courseadmin" element={<Course />} />
          <Route path='/coursesadmin/:id' element={<AdminClassInfo />}></Route>
          <Route path='/coursesadmin/calender/:id' element={<AdminClassInfoCalender />}></Route>
          <Route path='/coursesadmin/syllabus/:id' element={<AdminClassInfoSyllabus />}></Route>
          <Route path='/coursesadmin/schedule/:id' element={<CourseScheduleAdmin />}></Route>
          <Route path='/coursesadmin/member/:id' element={<AdminClassInfoMember />}></Route>
          <Route path='/coursesadmin/notification/:id' element={<AdminClassInfoNotification />}></Route>
          <Route path="/classadmin" element={<Class />} />
          <Route path="/blogadmin" element={<BlogAdmin />} />
          <Route path="/roomadmin" element={<ClassRoomAdmin />} />
          <Route path="/courseadmin/addcourse" element={<AddCourse />} />
          <Route path="/classadmin/editclass/:newclassId" element={<EditClassInfo />} />
          <Route path="/classadmin/addclass" element={<AddClass />} />
          <Route path="/addclassschedule/:classId" element={<AddClassSchedule />} />
          <Route path="/addclasssmember/:classId" element={<AddClassMember />} />

          <Route path='/coursedetail/:id' element={<CourseDetailAdmin />}></Route>
          <Route path="/contacts" element={<Contacts />} />
          <Route path="/invoices" element={<Invoices />} />
          <Route path="/calendar" element={<Calendar />} />
          <Route path="/geography" element={<Geography />} />
          <Route path='/blogadmin/addBlog/:cateId' element={<AddBlog />} ></Route>
          <Route path='/updateblog/:code' element={<UpdateBlog />}></Route>
          <Route path='/roomadmin/addroom' element={<AddClassRoom />} ></Route>
          <Route path="/attendadmin" element={<AttendAdmin />} />
          <Route path="/FAQadmin" element={<FAQAdmin />} />
          <Route path='/FAQadmin/addFAQ' element={<AddFaq />} ></Route>
          <Route path='/updateFAQ/:code' element={<UpdateFaq />}></Route>
          <Route path="/Syllabusadmin" element={<SyllabusAdmin />} />
          <Route path='/Syllabusadmin/addSyllabus' element={<AddSyllabus />} ></Route>
          <Route path='/updateSyllabusadmin/:code' element={<UpdateSyllabus />}></Route>

          <Route path="/addteacher" element={<AddTeacher />} />
          <Route path="/updateTeacher/:code" element={<UpdateTeacher />} />
          <Route path="/student" element={<Student />} />
          <Route path="/addstudent" element={<AddStudent />} />
          <Route path="/updateStudent/:code" element={<UpdateStudent />} />
          <Route path="/ShowStudent/:code" element={<ShowStudent />} />


          <Route path="/manageExam/:id" element={<ManageExam />} />
        </Route>
        <Route path="*" element={<Navigate to='/' replace />} />
      </Routes>


    </>
  )
}

export default App